<?php

class frontendConfiguration extends sfApplicationConfiguration
{
  public function configure()
  {
    //Passport listeners
    $this->dispatcher->connect('passport.new.application', array('PassportListener', 'newApplication'));
    $this->dispatcher->connect('passport.application.payment', array('PassportListener', 'newPayment'));
    $this->dispatcher->connect('passport.application.vetter', array('PassportListener', 'newVetter'));
    $this->dispatcher->connect('passport.application.approver', array('PassportListener', 'newApprover'));

    //Visa listeners
    $this->dispatcher->connect('visa.new.application', array('VisaListener', 'newApplication'));
    $this->dispatcher->connect('visa.application.payment', array('VisaListener', 'newPayment'));
    $this->dispatcher->connect('visa.application.vetter', array('VisaListener', 'newVetter'));
    $this->dispatcher->connect('visa.application.approver', array('VisaListener', 'newApprover'));

  }
}
