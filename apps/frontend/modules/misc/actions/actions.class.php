<?php

/**
 * misc actions.
 *
 * @package    ama
 * @subpackage misc
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class miscActions extends sfActions
{
  public function executeNisFeeDetails(sfWebRequest $request)
  {
      /**
       * [WP:081] => CR:118
       * Open NIS url in iframe...
       */
      $this->nisUrl = settings::getNisUrl();
      $this->setLayout('popupLayout');
  }

}
