  <!--
  [WP:081] => CR:118
  Code Start Here
  Adding iframe which will populate Visa fees from NIS URL...
  -->
  <div class="comBox" style="padding:10px;">
    <h2>Passport Application Fee</h2>
    <p class="body_txtp">
      NIS Passport Fee is $65
    </p>
    <h2>Visa Application Fee</h2>
    <p class="body_txtp" id="fee_details">
      Following table lists Visa fee for each country:
    <table class="bdr">
      <thead>
        <tr>
          <td align="center" colspan="5"><h3>Embassy VISA Fees </h3></td>
        </tr>
        <tr height="30">
          <td width="256" height="30"><h4>Country Name</h4></td>
          <td width="206"><h4>SINGLE ENTRY</h4></td>
          <td width="206"><h4>MULTIPLE ENTRY</h4></td>
          <td width="233"><h4>TWP FEES</h4></td>
          <td width="200"><h4>STR VISA FEES</h4></td>
        </tr>
      </thead>
      <tbody>
        <tr>
         <td colspan="5" style="padding-left:2px;height:100%;">            
           <iframe src="<?php echo $nisUrl;?>/pages/visaguidelinesInPopup/popup/true" height="500" width="100%"></iframe>
         </td>
        </tr>
      </tbody>
      </table>
    </p>
  </div>
  <div style="text-align: center">
  <input value="Close Window" type="button" class="normalbutton"
         onclick="window.close();" /></div>
<!--
  [WP:081] => CR:118
  Code End Here
  -->