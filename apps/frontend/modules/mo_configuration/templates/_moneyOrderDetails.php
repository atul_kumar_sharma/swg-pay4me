<?php
if($moneyOrderDetails['currency'] == '5'){
    $moneyOrderAmount = $moneyOrderDetails['convert_amount'];    
}else{
    $moneyOrderAmount = $moneyOrderDetails['amount'];    
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($moneyOrderDetails['currency']);
?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
           <tr>
              <td class="blbar" colspan="11" align="left">Money order information</td>
            </tr>
            <tr>
            <td width="50%"><table width="100%">
                  <tr>
                  <td width="30%"> Money Order Number </td>
                  <td width="30%"><?= $moneyOrderDetails['moneyorder_number']?>
                    </td>
                  </tr>
                  <tr>
                  <td> Money Order Date </td>
                  <td><?= $moneyOrderDetails['moneyorder_date']?>
                    </td>
                  </tr>
              </table></td>
            <td width="50%"><table width="100%">
                  <tr>
                  <td width="30%"v> Amount(<?php echo html_entity_decode($currencySymbol); ?>) </td>
                  <td width="30%"><?php echo $moneyOrderAmount;?></td>
                  </tr>
                  <tr>
                  <td colspan="2">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
           </table>
		   <br/>
           <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
            <tr>
              <td class="blbar" colspan="11" align="left">Tracking Information</td>
            </tr>
            <tr>
              <td width="15%"><span class="txtBold">Tracking Number</span></td>
              <td width="15%"><span class="txtBold">Order Number</span></td>
              <td width="15%" align="center"><span class="txtBold">Application Detail</span></td>
              <td width="10%" align="right"><span class="txtBold">Amount(<?php echo html_entity_decode($currencySymbol); ?>)</span></td>
            </tr>
            <tbody>
            <?php if($record) { ?>
              <?php  foreach ($applicationDetails as $records){ ?>
              <tr>
              <td valign="top"><?= $records['tracking_number']?>
                </td>
              <td valign="top"><?= $records['order_number']?>
                </td>
              <td><table>
                        <tr style="background-color: rgb(229, 236, 249);">
                            <td width="15%"><span class="txtBold">Application Id</span></td>
                            <td width="15%"><span class="txtBold">Application Type</span></td>
                            <td width="15%"><span class="txtBold">Reference Number</span></td>
                        </tr>
                        <?php foreach($records['app_detail'] as $detail){?>
                            <tr>
                                <td width="15%"><?= $detail['app_id']?></td>
                                <td width="15%"><?php if($detail['app_type'] == 'vap'){echo sfConfig::get('app_visa_arrival_title');}else{echo ucfirst($detail['app_type']);}?></td>
                                <td width="15%"><?= $detail['ref_no']?></td>
                            </tr>
                        <?php } ?>
                </table></td>
              <td align="right"><?= $records['cart_amount']?>
                </td>
              </tr>
              <?php } ?>
              <?php } else{ ?>
              <tr>
              <td colspan="3"> No tracking number found. </td>
              </tr>
              <?php } ?>
            </tbody>
           </table>