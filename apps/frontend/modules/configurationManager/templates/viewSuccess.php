
<style>

#1popupContent {
background-color:#bca;
width:100px;
border:1px solid green;
}
</style> 

<div id="result_div" style="width:500px;display:none" align="center"></div><br/>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading', array('heading' =>'Country Wise Register Card Configuration'));
?>
<div class="clear"></div>
    <div class="tmz-spacer"></div>
<div id="nxtPage">
<form  name = "myForm" id="myForm"  method="Get" onsubmit="return false">
<table border="99%" width="99%" id = "tableId">
   <tr>
    <td class="blbar" colspan="5" align="right">
    <div style="float:left">Country Wise Register Card Configuration</div>
    <span>Showing <b><span id ="firstRecord"><?php echo $pager->getFirstIndice() ?></span></b> - <b><span id = "lastRecord"><?php echo $pager->getLastIndice() ?></span></b> of total  <b><span id = "totalRecord"><?php echo $pager->getNbResults(); ?></span></b>  results</span></td>
   </tr>
    <tr>    
    <td width ="10%">
    <input type="checkbox" value="" name="chk_all" id="chk_all" />
    <input type="hidden" value="<?php echo $page;?>" name="page" id="page" />    
    </td>
    <td width="25%"><span class = "txtBold"> Country</span></td>
    <td width="25%"><span class = "txtBold"> Register Card </span> </td>
    <td width="50"><span class = "txtBold"> Action </span></td>
    </tr>
    <?php if($pager->getNbResults() > 0) {
        $i = $pager->getFirstIndice();
        foreach($pager->getResults() as $report):   ?>
   <tr id="removeRowHideId_<?php echo $report['id']; ?>">   
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
       <td>&nbsp;</td>
   <?php }else { ?>
   <td><input type = "checkbox" class="chk_fee" name = "chk_fee[]" value="<?php echo $report['id']; ?>" id ="chk_<?php echo $report['id']; ?>"></td>
   <?php } ?>
   <td><?php echo $report['country_code'];?> </td>
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
   <td id ="statusId" width="25"><?php echo strtoupper($report['is_registered']);?></td>
   <?php   } else { ?>
   <td><?php echo strtoupper($report['is_registered']);?></td>
   <?php } ?>
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
   <td id="removeRowId"><a href="javascript:void(0);" onclick = "changeStatus('<?php echo $report['id'];?>')"><span class="red"><b><?php if(strtolower($report['is_registered']) == 'yes') { echo " Change Status to NO"; }  if(strtolower($report['is_registered']) == 'no')  {  echo "Change Status to YES";  }  ?></b></span></a></td>
   <?php   } else { ?>
   <td id="removeRowId"><a href="javascript:void(0);" onclick = "removeRowDataFromDB('<?php echo $report['id'];?>','<?php echo $deletedReocord = '1'; ?>')">Remove</a></td>
   <?php } ?>
   </tr>    
  <?php $i++; 
  endforeach;
  ?>
  <?php } else { ?>  
  <tr id="noRecordFound" ><td align="center" colspan="4">No Record Found </td></tr>
<?php }  ?>
  <tr id ="lastRow">
    <td class="blbar" colspan="5" height="25px" align="right">
    <div class = "paging pagingFoot" id = "footPaging">
   <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/viewAjax'), 'nxtPage') ?></div></td>
   </tr>
</table>
</form><br/>
</div>
<div align="center">
<input type="button" class="normalbutton" id = "removeAllButton" value = "Remove All" onclick="removeAllSelected();" title = "Please click on checkboxes to remove multiple row">
<?php if(($pager->getNbResults() < 1) ) { ?>
<script> $('#chk_all').attr('disabled',true);
$('#removeAllButton').hide();
</script>
<?php } ?>

<?php if($pager->getNbResults() == 1) { if(($report['country_code']) == 'ALL') {?>
<script> $('#removeAllButton').hide();
$('#chk_all').attr('disabled',true);
</script> <?php }} ?>



<input type="button" class = "normalbutton" value = "&nbsp;Add New&nbsp;" id="addnew" onclick="showNewRow();" title="Add New Country Register Card">
</div>
<div id="popupContent"></div>

<div id="backgroundPopup"></div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>

function popitup(url) {
	newwindow=window.open(url,'name','height=300,width=200');
	if (window.focus) {newwindow.focus()}
	return false;
}


   function removeRowDataFromDB(id,deletedRecord){     
      var deletedRecord = parseInt(deletedRecord);
      if(confirm('Do you permanently want to delete this entry')){
          $('#result_div').html('<b><font color ="green">Please Wait While Data is deleting...</font></b>');
          $("#result_div").show();
          var url = "<?php echo url_for('configurationManager/removeCountryCardRow');?>" ;
          $.post(url,{id : id},function(data){
              if(data){
              $("#result_div").html('<b><font color ="green">Data Removed Successfully</font></b>');
              $("#result_div").hide(2500);
              $("#removeRowHideId_"+id).hide();
              addRowsForPager(deletedRecord);              
              } else {
                  alert('Row Not Deleted');
              }              
          });
      }else {
          return false;
      }
  }

$('#chk_all').bind('click',function(){
    if($('#chk_all').attr('checked') == true){
        $("#myForm input:checkbox").attr('checked',true);
    }else{ 
        $("#myForm input:checkbox").attr('checked',false);
    }
    
})

$('#myForm .chk_fee').click(function(){
    var totalChk = ($("#myForm input:checkbox").length) - 1;
    
    var totalCheckedChk = 0;
    $("#myForm input:checkbox:checked").each(function(){
         if(this.value != ''){
            totalCheckedChk = parseInt(totalCheckedChk) + parseInt(1);
         }
    })

    if(totalChk == totalCheckedChk){
        $('#chk_all').attr('checked',true);
    }else{
        $('#chk_all').attr('checked',false);
    }
    
})


  
 function removeAllSelected() {    
    var chkid   = '';
    var i = 0;
    var idArray = new Array();
    $("#myForm input:checkbox:checked").each(function(){
         if(this.value != ''){
            chkid = chkid + ',' + this.value;
            idArray.push(this.value);
         }
    });
    var deletedReocord = idArray.length;
        
   if(idArray.length == 0){
       alert('Please select atleast one row');
       return false;
    }else {
    if(confirm('Do you permanently want to delete selected entry')){    
    $('#result_div').html('<b><font color ="green">Please Wait While Data is deleting...</font></b>');
    $("#result_div").show();
    var url = "<?php echo url_for('configurationManager/removeMultipleRow') ?>";
    $.post(url,{chkid : chkid},function(data) {
        if(data){
        $("#result_div").html('<b><font color ="green">Data Removed Successfully</font></b>');
        $("#result_div").hide(2500);
        var len = idArray.length;
        for(i=0;i<len;i++){
          $("#removeRowHideId_"+idArray[i]).hide();
          
        }
        addRowsForPager(deletedReocord);
        } else {
            alert('Data not Deleted');
            return false;
        }
      })
     }
    }
   }
 function showNewRow(){     
     var url = "<?php echo url_for('configurationManager/addNewCountry') ?>";
     $.post(url,{},function(data){
        $('#popupContent').html(data);
         //centerPopup();
        var windowWidth = document.documentElement.clientWidth;
        var windowHeight = document.documentElement.clientHeight;
        var popupHeight = $("#popupContent").height();
        var popupWidth = $("#popupContent").width();

        //only need force for IE6
        $("#backgroundPopup").css({
            "height": windowHeight
        });

        var elementPos = findElementPosition('addnew');
        elementPos = elementPos.split(",");
        var browserName =  navigator.appName ;
        
        if(browserName == 'Microsoft Internet Explorer'){
              //centering
         $("#popupContent").css({
            "position": "absolute",
            "left": elementPos[0]-100,
            "top": elementPos[1]-350
            });
            
        } else {
         //centering
        $("#popupContent").css({
            "position": "absolute",
            "left": elementPos[0],
            "top": elementPos[1]-350
        });
        }
            
		//load popup
		loadPopup();
     
     });
     
   }

     function closeDiv(){
         $("#popupContent").animate({
           width: "0%",
           opacity: 0.0,
           marginLeft: "0.6in",
           fontSize: "5em",
           borderWidth: "0px"
         }, 400 );
         $("#popupContent").html('');
     }
     function changeStatus(id){
         if(confirm('Do you want to change Register Card status')){
             $('#result_div').html('<b><font color ="green">Please Wait While Status is changing...</font></b>');
             $("#result_div").show();
             var newStatus;
             var newLinkName;
             var status = $('#statusId').text();             
             if((status.toUpperCase()) == 'YES'){
                 newStatus = 'NO';
                 newLinkName = '<a href="javascript:void(0);" onclick = "changeStatus(<?php echo $report['id'];?>)"><span class="red"><strong>Change Status to YES</strong></span></a>';
             }else {
                 newStatus = 'YES';
                 newLinkName = '<a href="javascript:void(0);" onclick = "changeStatus(<?php echo $report['id'];?>)"><span class="red"><strong>Change Status to NO</strong></span></a>';
             }
             var url = "<?php echo url_for('configurationManager/changeAllStatus');?>"
             $.post(url,{id : id, isRegister :newStatus},function(data){
                 $("#result_div").html('<b><font color ="green">Status Changed Successfully</font></b>');
                 $("#result_div").hide(2500);
                 $('#statusId').html(newStatus);
                 $('#removeRowId').html(newLinkName);
             })            

         }
     }

     function addRowsForPager(deletedRecord){
         var page = $('#page').val();
         var totalRecord = document.getElementById("totalRecord").innerHTML;
         var recordPerPage = '<?php echo sfConfig::get('app_records_per_page');?>';
         var totalRecordLeftAfterDeleting = parseInt(totalRecord) - parseInt(deletedRecord);
         var specialFlag = parseInt(totalRecordLeftAfterDeleting)%parseInt(recordPerPage);
         var pagenoUri = '<?php echo url_for('configurationManager/viewAjax');?>';

         if(!specialFlag){              ;
                ajax_paginator('nxtPage', pagenoUri+'/page/'+(parseInt(page)-1));
              } else {
                  ajax_paginator('nxtPage', pagenoUri+'/page/'+(parseInt(page)));
              }
        
         getPaginBar(page);
         if(totalRecordLeftAfterDeleting == 1){
             $('#removeAllButton').hide();
             $('#chk_all').attr('disabled',true);
            }
       
     }

     function getPaginBar(page) {
         pagingBar = 'pagingBar';
         var dataRecord = ''
         var url = '<?php echo url_for('configurationManager/view');?>';
         $.post(url,{ page : page ,pagingBar :pagingBar},function(data){
             
             dataRecord = data.split('####');
             var firstRecord = dataRecord[0];
             var lastRecord = dataRecord[1];
             var totaltRecord = dataRecord[2];
             var footPaging = dataRecord[3];
             $('#firstRecord').html(firstRecord);
             $('#lastRecord').html(lastRecord);
             $('#totalRecord').html(totaltRecord);
             $('#footPaging').html(footPaging);
             
         })
     }
     
</script>
