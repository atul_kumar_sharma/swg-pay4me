   <div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
   
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
use_helper('Form');
use_helper('Pagination');
$sessionMsg = sfContext::getInstance()->getUser()->getAttribute('sessMsg');
sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sessMsg');
?>



  <div class="content_container">

    

    <?php include_partial('global/innerHeading',array('heading' =>'Country Payment Mode Validation(s) ')); ?>
    <div class="clear"></div>
    <div id="flash_notice" class="alertBox" style="display:<?php echo ($sessionMsg!='')?'':'none'?>"><?php echo $sessionMsg;?></div>
    
    
   <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <table width="100%" border="100" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">

    <tr>
    <td class="blbar" colspan="10" align="right">
    <div style="float:left">Support Request List</div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
    </tr>
    <tr>
        <td width ="5%"> <span class = "txtBold"> Sr No</span></td>
        <td width ="11%"> <span class = "txtBold"> Country</span></td>
        <td width ="24%"> <span class = "txtBold"> Card Type </span></td>
        <td width ="7%"> <span class = "txtBold"> Service </span></td>
        <td width ="17%"> <span class = "txtBold"> Validation </span></td>
        <td width ="7%"> <span class = "txtBold">Cart Capacity</span></td>
        <td width ="7%"> <span class = "txtBold">Cart Amount Capacity</span></td>
        <td width ="5%"> <span class = "txtBold"> Number of Transaction </span></td>
        <td width ="7%"> <span class = "txtBold"> Transaction Period </span></td>
        <td width ="10%"> <span class = "txtBold"> Action </span></td>
    </tr>
    <style>
        .global_content3 input{
        width:80px;
        }
    </style>

<?php $i = $pager->getFirstIndice();
if($pager->getNbResults()>0){
    foreach($pager->getResults() as $list):?>
        <tr id="tr_id_<?php echo $list['id']; ?>">
            <td width ="5%">  <?php echo $i; ?></td>
            <?php $countryName =  Doctrine::getTable('Country')->findBy('id', $list['country_code']);
            ?>
            <td ><?php if($countryName[0]['country_name'] == ''){echo 'ALL';} else { echo ucwords($countryName[0]['country_name']);  } ?></td>
            <td ><span id="cardtype_span_<?php echo $list['id']; ?>"><?php echo $list['card_type']; ?></span></td>
            <td ><span id="service_span_<?php echo $list['id']; ?>"><?php echo $list['service']; ?></span></td>
            <td ><span id="validation_span_<?php echo $list['id']; ?>"><?php echo $list['validation']; ?></span></td>
            <td ><span id="cartcapacity_span_<?php echo $list['id']; ?>"><?php echo $list['cart_capacity']; ?></span></td>
            <td > <span id="cartamountcapacity_span_<?php echo $list['id']; ?>"><?php echo $list['cart_amount_capacity']; ?></span></td>
            <td ><span id="numberoftransaction_span_<?php echo $list['id']; ?>"><?php echo $list['number_of_transaction']; ?></span></td>
            <td ><span id="transactionperiod_span_<?php echo $list['id']; ?>"><?php echo $list['transaction_period']; ?></span></td>
       <input id="total_records" type="hidden" name="total_records" value="<?php echo $pager->getNbResults();?>" />
            <td valign="top">
            <table width="100%" cellspacing="0" cellpadding="0" style="border:0px;" >
                <tr>
                    <td id="edit_<?php echo $list['id']; ?>" style="border:0px;"><span><a  href="javascript:void(0);" onclick="addNewCountryValidation('edit','<?php echo $list['id']; ?>','<?php echo $page;?>');">Edit</a></span></td>
                   
                    <?php if($countryName[0]['country_name'] != ''){ ?><td id="remove_<?php echo $list['id']; ?>" style="border:0px;"><span><a href="javascript:void(0);" onclick="remove('<?php echo $list['id']; ?>','<?php echo $countryName[0]['country_name']; ?>'); return false;">Remove</a></span></td><?php } ?>
                </tr>
            </table>
            </td>
        </tr>

    <?php $i++;
    endforeach;?>
    <tr>
    <td class="blbar" colspan="10" height="25px" align="right">
    <div class = "paging pagingFoot">
   <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td>
    </tr>

    <?php }else {?>
    <tr><td  align='center' class='error' colspan="10">No Country Payment Mode Validation Available</td></tr>
   <?php } ?>
</table>


   <div class="mgauto">
              <input type="button" class = "loginbutton" value = "Add New&nbsp;&nbsp;" id="add" onclick="addNewCountryValidation('add','','<?php echo $page;?>')" title="Add New Country Validations">
   </div>

</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
//function edit(id){
//    document.getElementById("cardtype_input_"+id).type = 'text';
//    document.getElementById("cardtype_span_"+id).innerHTML = '';
//    document.getElementById("validation_input_"+id).type = 'text';
//    document.getElementById("validation_span_"+id).innerHTML = '';
//    document.getElementById("service_input_"+id).type = 'text';
//    document.getElementById("service_span_"+id).innerHTML = '';
//    document.getElementById("cartcapacity_input_"+id).type = 'text';
//    document.getElementById("cartcapacity_span_"+id).innerHTML = '';
//    document.getElementById("cartamountcapacity_input_"+id).type = 'text';
//    document.getElementById("cartamountcapacity_span_"+id).innerHTML = '';
//    document.getElementById("numberoftransaction_input_"+id).type = 'text';
//    document.getElementById("numberoftransaction_span_"+id).innerHTML = '';
//    document.getElementById("transactionperiod_input_"+id).type = 'text';
//    document.getElementById("transactionperiod_span_"+id).innerHTML = '';
//
//        $('#tr_id_'+id+ '> td' ).children('input').attr('class','txt-input');
//        $('#tr_id_'+id+ '> td').children('input').attr('readonly','');
//        $('#save_'+id).show();
//        $('#edit_'+id).hide();
//    }
//
//
//    function save(id){
//        if(confirm("Do you realy want to change these values?") == true){
//        var cardtype = $('#cardtype_'+id+' > input').val();
//        var service = $('#service_'+id+' > input').val();
//        var validation = $('#validation_'+id+' > input').val();
//        var cartcapacity = $('#cartcapacity_'+id+' > input').val();
//        var cartamountcapacity = $('#cartamountcapacity_'+id+' > input').val();
//        var numberoftransaction = $('#numberoftransaction_'+id+' > input').val();
//        var transactionperiod = $('#transactionperiod_'+id+' > input').val();
//        <?php
//         $saveUrl = url_for('configurationManager/saveCountryPaymentModeValidation');
//        ?>
//
//       var err  = 0;
//       var reg = /^[\-0-9a-zA-Z\,_]+$/;
//       if(cardtype == "" || reg.test(cardtype) == false)
//            {
//                alert('Please enter a valid value for Card Type');
//                err = err+1;
//            }
//       if(service == "" || reg.test(service) == false)
//            {
//                alert('Please enter a valid value for Service');
//                err = err+1;
//            }
//       if(validation == "" || reg.test(validation) == false)
//            {
//                alert('Please enter a valid value for Validation');
//                err = err+1;
//            }
//       if(isNaN(cartcapacity) || cartcapacity == ""){
//               alert('Please enter a valid numeric value for Cart Capacity');
//                err = err + 1;
//            }
//       if(isNaN(cartamountcapacity) || cartamountcapacity == ""){
//               alert('Please enter a valid numeric value for Cart Amount Capacity');
//                err = err + 1;
//            }
//       if(isNaN(numberoftransaction) || numberoftransaction == ""){
//               alert('Please enter a valid numeric value for Number of Transaction');
//                err = err + 1;
//            }
//       if(isNaN(transactionperiod) || transactionperiod == ""){
//               alert('Please enter a valid numeric value for Transaction Period');
//                err = err + 1;
//            }
//            if(err != 0)
//            {
//                return false;
//            }
//            else{
//       var saveUrl = "<?php //echo $saveUrl;?>"
//         $.post(saveUrl,{id:id,cardtype :cardtype,service:service,validation:validation,cartcapacity:cartcapacity,cartamountcapacity:cartamountcapacity,numberoftransaction:numberoftransaction,transactionperiod:transactionperiod}, function(data){
//             });
//        document.getElementById('flash_notice').style.display = 'block';
//        document.getElementById('flash_notice').innerHTML = 'Validation(s) successfully changed.';
//        document.getElementById("cardtype_span_"+id).innerHTML = document.getElementById("cardtype_input_"+id).value;
//        document.getElementById("cardtype_input_"+id).type='hidden';
//        document.getElementById("validation_span_"+id).innerHTML = document.getElementById("validation_input_"+id).value;
//        document.getElementById("validation_input_"+id).type='hidden';
//        document.getElementById("service_span_"+id).innerHTML = document.getElementById("service_input_"+id).value;
//        document.getElementById("service_input_"+id).type='hidden';
//        document.getElementById("cartcapacity_span_"+id).innerHTML = document.getElementById("cartcapacity_input_"+id).value;
//        document.getElementById("cartcapacity_input_"+id).type='hidden';
//        document.getElementById("cartamountcapacity_span_"+id).innerHTML = document.getElementById("cartamountcapacity_input_"+id).value;
//        document.getElementById("cartamountcapacity_input_"+id).type='hidden';
//        document.getElementById("numberoftransaction_span_"+id).innerHTML = document.getElementById("numberoftransaction_input_"+id).value;
//        document.getElementById("numberoftransaction_input_"+id).type='hidden';
//        document.getElementById("transactionperiod_span_"+id).innerHTML = document.getElementById("transactionperiod_input_"+id).value;
//        document.getElementById("transactionperiod_input_"+id).type='hidden';
//        $('#save_'+id).hide();
//        $('#edit_'+id).show();
//        return false;
//        }
//      }
//      else
//        {
//            $('#tr_id_'+id+ '> td' ).children('input').attr('class','');
//            $('#tr_id_'+id+ '> td').children('input').attr('readonly','readonly');
//            $('#save_'+id).hide();
//            $('#edit_'+id).show();
//        }
//    }

    function addNewCountryValidation(mode,id,page){
      
      var url = "<?php echo url_for('configurationManager/addCountry?totalRecords='.$pager->getNbResults()) ?>";
      if(id !=''){
        url = url + "/id/"+ id + "/mode/" + mode + "/page/" + page;
      }else {
        url = url + "/mode/" + mode + "/page/" + page;
      }
      emailwindow = dhtmlmodal.open('EmailBox', 'iframe', url ,'Country Validation', ' width=620,height=600,center=1,border=0, scrolling=no')


  }

  function remove(id,countryName)
  {

    if(confirm('Do you want to remove validations for '+countryName+' ?')){
          <?php
            $removeUrl = url_for('configurationManager/deleteCountryPaymentModeValidation');
          ?>
         var removeUrl = "<?php echo $removeUrl;?>"
         $.post(removeUrl,{id: id}, function(data){              
               location.reload();
         });
    }
  }
</script>