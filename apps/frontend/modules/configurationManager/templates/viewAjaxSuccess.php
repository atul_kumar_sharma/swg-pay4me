<?php
use_helper('Pagination');
use_helper('Form');
?>


<form  name = "myForm" id="myForm"  method="Get" onsubmit="return false">
<table border="99%" width="99%" id = "tableId">
   <tr>
    <td class="blbar" colspan="5" align="right">
    <div style="float:left">Country Wise Register Card Configuration</div>
    <span>Showing <b><span id ="firstRecord"><?php echo $pager->getFirstIndice() ?></span></b> - <b><span id = "lastRecord"><?php echo $pager->getLastIndice() ?></span></b> of total  <b><span id = "totalRecord"><?php echo $pager->getNbResults(); ?></span></b>  results</span></td>
   </tr>
    <tr>    
    <td width ="10%">
    <input type="checkbox" value="" name="chk_all" id="chk_all" />
    <input type="hidden" value="<?php echo $page;?>" name="page" id="page" />
    
    </td>
    <td width="25%"><span class = "txtBold"> Country</span></td>
    <td width="25%"><span class = "txtBold"> Register Card </span> </td>
    <td width="50"><span class = "txtBold"> Action </span></td>
    </tr>
    <?php if(count($pager->getNbResults()) > 0) {
        $i = $pager->getFirstIndice();
        foreach($pager->getResults() as $report):   ?>
   <tr id="removeRowHideId_<?php echo $report['id']; ?>">   
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
       <td>&nbsp;</td>
   <?php }else { ?>
   <td><input type = "checkbox" class="chk_fee" name = "chk_fee[]" value="<?php echo $report['id']; ?>" id ="chk_<?php echo $report['id']; ?>"></td>
   <?php } ?>
   <td><?php echo $report['country_code'];?> </td>
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
   <td id ="statusId" width="25"><?php echo strtoupper($report['is_registered']);?></td>
   <?php   } else { ?>
   <td><?php echo strtoupper($report['is_registered']);?></td>
   <?php } ?>
   <?php if(strtoupper($report['country_code']) == 'ALL'){ ?>
   <td id="removeRowId"><a href="javascript:void(0);" onclick = "changeStatus('<?php echo $report['id'];?>')"><span class="red"><b><?php if(strtolower($report['is_registered']) == 'yes') { echo " Change Status to NO"; }  if(strtolower($report['is_registered']) == 'no')  {  echo "Change Status to YES";  }  ?></b></span></a></td>
   <?php   } else { ?>
   <td id="removeRowId"><a href="javascript:void(0);" onclick = "removeRowDataFromDB('<?php echo $report['id'];?>','<?php echo $deletedReocord = '1'; ?>')">Remove</a></td>
   <?php } ?>
   </tr>    
  <?php $i++; 
  endforeach;
  ?>
  <tr id ="lastRow">
    <td class="blbar" colspan="5" height="25px" align="right">
    <div class = "paging pagingFoot" id = "footPaging">
   <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/viewAjax'), 'nxtPage') ?></div></td>
   </tr>
<?php }?>

</table><br/>

</form><br/>

<script>

$('#chk_all').bind('click',function(){
    if($('#chk_all').attr('checked') == true){
        $("#myForm input:checkbox").attr('checked',true);
    }else{
        $("#myForm input:checkbox").attr('checked',false);
    }

})

$('#myForm .chk_fee').click(function(){
    var totalChk = ($("#myForm input:checkbox").length) - 1;

    var totalCheckedChk = 0;
    $("#myForm input:checkbox:checked").each(function(){
         if(this.value != ''){
            totalCheckedChk = parseInt(totalCheckedChk) + parseInt(1);
         }
    })

    if(totalChk == totalCheckedChk){
        $('#chk_all').attr('checked',true);
    }else{
        $('#chk_all').attr('checked',false);
    }

})

</script>
