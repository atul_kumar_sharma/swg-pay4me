   <div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
   
<?php
use_helper('Form');
use_helper('Pagination');
$sessionMsg = sfContext::getInstance()->getUser()->getFlash('notice');
$colspan = '6';
?>

  <div class="content_container">

    <?php include_partial('global/innerHeading',array('heading' =>'Credit Card Country')); ?>
    <div class="clear"></div>
    <div id="flash_notice" class="alertBox" style="display:<?php echo ($sessionMsg!='')?'':'none'?>"><?php echo $sessionMsg;?></div>
    
    
   <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <table width="100%" border="100" cellpadding="0" cellspacing="0" class="dataTable">

    <tr>
        <td colspan="<?php echo $colspan; ?>" >
            <form name="frmImport" id="frmImport" method="post" action="" enctype="multipart/form-data" >
                <table width="100%" cellspacing="0" cellpading="0">
                    <tr>
                        <td colspan="2"><div style="float:left;"><strong>Add Credit Card Country</strong></div>                        
                        <div style="float:right;"><?php echo link_to('Download Sample Data File', 'configurationManager/DownloadSampleBinData'); ?></div></td>
                    </tr>
                    <tr>
                        <td width="30%">
                            <input type="file" name="countryList" value="Browse" class="txt-input" /><br />
                            <strong>Note:</strong> Only .xls file allowed. File size should not greater than 1 MB.(around 10,000 entries)

                        </td>
                        <td valign="top">
                            <input type="submit" name="btnSubmit" value="Import" class="loginbutton" />
                        </td>
                    </tr>
                </table>
            </form>
       </td>
    </tr>    
    <tr>
    <td class="blbar" colspan="<?php echo $colspan; ?>" align="right">
    <div style="float:left">Support Request List</div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
    </tr>
    <tr>
        <td width ="5%"> <span class = "txtBold"> Sr No</span></td>
        <td width ="11%"> <span class = "txtBold"> BIN</span></td>
        <td width ="11%"> <span class = "txtBold"> Processor BIN</span></td>
        <td width ="11%"> <span class = "txtBold"> Region</span></td>
        <td width ="11%"> <span class = "txtBold"> Created On</span></td>
        <td width ="11%"> <span class = "txtBold"> Updated On</span></td>
    </tr>
    <style>
        .global_content3 input{
        width:80px;
        }
    </style>

<?php $i = $pager->getFirstIndice();
if($pager->getNbResults()>0){
    $tempCountryName = '';
    foreach($pager->getResults() as $list):
        $countryName =  Doctrine::getTable('Country')->findBy('id', $list['country']);
        $countryName = (count($countryName) > 0)?$countryName[0]['country_name']:$list['country'];
        if($countryName != $tempCountryName){ ?>

        <tr>
            <td colspan="<?php echo $colspan; ?>" bgcolor="#EEEFFF">
                <i>Country Name:</i> <strong><?php echo ucwords($countryName); ?></strong>
            </td>
        </tr>


        <?php
            $tempCountryName = $countryName;
        }
        

    ?>

        


        <tr>
            <td width ="5%">  <?php echo $i; ?></td>            
            <td><?php echo $list['bin']; ?></td>
            <td><?php echo $list['processor_bin']; ?></td>
            <td><?php echo $list['region']; ?></td>
            <td><?php echo $list['created_at']; ?></td>
            <td><?php echo $list['updated_at']; ?></td>
        </tr>

    <?php $i++;
    endforeach;?>
    <tr>
    <td class="blbar" colspan="<?php echo $colspan; ?>" height="25px" align="right">
    <div class = "paging pagingFoot">
   <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></div></td>
    </tr>

    <?php }else {?>
    <tr><td  align='center' class='error' colspan="<?php echo $colspan; ?>">No Records Available.</td></tr>
   <?php } ?>
</table>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
//function edit(id){
//    document.getElementById("cardtype_input_"+id).type = 'text';
//    document.getElementById("cardtype_span_"+id).innerHTML = '';
//    document.getElementById("validation_input_"+id).type = 'text';
//    document.getElementById("validation_span_"+id).innerHTML = '';
//    document.getElementById("service_input_"+id).type = 'text';
//    document.getElementById("service_span_"+id).innerHTML = '';
//    document.getElementById("cartcapacity_input_"+id).type = 'text';
//    document.getElementById("cartcapacity_span_"+id).innerHTML = '';
//    document.getElementById("cartamountcapacity_input_"+id).type = 'text';
//    document.getElementById("cartamountcapacity_span_"+id).innerHTML = '';
//    document.getElementById("numberoftransaction_input_"+id).type = 'text';
//    document.getElementById("numberoftransaction_span_"+id).innerHTML = '';
//    document.getElementById("transactionperiod_input_"+id).type = 'text';
//    document.getElementById("transactionperiod_span_"+id).innerHTML = '';
//
//        $('#tr_id_'+id+ '> td' ).children('input').attr('class','txt-input');
//        $('#tr_id_'+id+ '> td').children('input').attr('readonly','');
//        $('#save_'+id).show();
//        $('#edit_'+id).hide();
//    }
//
//
//    function save(id){
//        if(confirm("Do you realy want to change these values?") == true){
//        var cardtype = $('#cardtype_'+id+' > input').val();
//        var service = $('#service_'+id+' > input').val();
//        var validation = $('#validation_'+id+' > input').val();
//        var cartcapacity = $('#cartcapacity_'+id+' > input').val();
//        var cartamountcapacity = $('#cartamountcapacity_'+id+' > input').val();
//        var numberoftransaction = $('#numberoftransaction_'+id+' > input').val();
//        var transactionperiod = $('#transactionperiod_'+id+' > input').val();
//        <?php
//         $saveUrl = url_for('configurationManager/saveCountryPaymentModeValidation');
//        ?>
//
//       var err  = 0;
//       var reg = /^[\-0-9a-zA-Z\,_]+$/;
//       if(cardtype == "" || reg.test(cardtype) == false)
//            {
//                alert('Please enter a valid value for Card Type');
//                err = err+1;
//            }
//       if(service == "" || reg.test(service) == false)
//            {
//                alert('Please enter a valid value for Service');
//                err = err+1;
//            }
//       if(validation == "" || reg.test(validation) == false)
//            {
//                alert('Please enter a valid value for Validation');
//                err = err+1;
//            }
//       if(isNaN(cartcapacity) || cartcapacity == ""){
//               alert('Please enter a valid numeric value for Cart Capacity');
//                err = err + 1;
//            }
//       if(isNaN(cartamountcapacity) || cartamountcapacity == ""){
//               alert('Please enter a valid numeric value for Cart Amount Capacity');
//                err = err + 1;
//            }
//       if(isNaN(numberoftransaction) || numberoftransaction == ""){
//               alert('Please enter a valid numeric value for Number of Transaction');
//                err = err + 1;
//            }
//       if(isNaN(transactionperiod) || transactionperiod == ""){
//               alert('Please enter a valid numeric value for Transaction Period');
//                err = err + 1;
//            }
//            if(err != 0)
//            {
//                return false;
//            }
//            else{
//       var saveUrl = "<?php //echo $saveUrl;?>"
//         $.post(saveUrl,{id:id,cardtype :cardtype,service:service,validation:validation,cartcapacity:cartcapacity,cartamountcapacity:cartamountcapacity,numberoftransaction:numberoftransaction,transactionperiod:transactionperiod}, function(data){
//             });
//        document.getElementById('flash_notice').style.display = 'block';
//        document.getElementById('flash_notice').innerHTML = 'Validation(s) successfully changed.';
//        document.getElementById("cardtype_span_"+id).innerHTML = document.getElementById("cardtype_input_"+id).value;
//        document.getElementById("cardtype_input_"+id).type='hidden';
//        document.getElementById("validation_span_"+id).innerHTML = document.getElementById("validation_input_"+id).value;
//        document.getElementById("validation_input_"+id).type='hidden';
//        document.getElementById("service_span_"+id).innerHTML = document.getElementById("service_input_"+id).value;
//        document.getElementById("service_input_"+id).type='hidden';
//        document.getElementById("cartcapacity_span_"+id).innerHTML = document.getElementById("cartcapacity_input_"+id).value;
//        document.getElementById("cartcapacity_input_"+id).type='hidden';
//        document.getElementById("cartamountcapacity_span_"+id).innerHTML = document.getElementById("cartamountcapacity_input_"+id).value;
//        document.getElementById("cartamountcapacity_input_"+id).type='hidden';
//        document.getElementById("numberoftransaction_span_"+id).innerHTML = document.getElementById("numberoftransaction_input_"+id).value;
//        document.getElementById("numberoftransaction_input_"+id).type='hidden';
//        document.getElementById("transactionperiod_span_"+id).innerHTML = document.getElementById("transactionperiod_input_"+id).value;
//        document.getElementById("transactionperiod_input_"+id).type='hidden';
//        $('#save_'+id).hide();
//        $('#edit_'+id).show();
//        return false;
//        }
//      }
//      else
//        {
//            $('#tr_id_'+id+ '> td' ).children('input').attr('class','');
//            $('#tr_id_'+id+ '> td').children('input').attr('readonly','readonly');
//            $('#save_'+id).hide();
//            $('#edit_'+id).show();
//        }
//    }

    function addNewCountryValidation(mode,id,page){
      
      var url = "<?php echo url_for('configurationManager/addCountry?totalRecords='.$pager->getNbResults()) ?>";
      if(id !=''){
        url = url + "/id/"+ id + "/mode/" + mode + "/page/" + page;
      }else {
        url = url + "/mode/" + mode + "/page/" + page;
      }
      emailwindow = dhtmlmodal.open('EmailBox', 'iframe', url ,'Country Validation', ' width=620,height=600,center=1,border=0, scrolling=no')


  }

  function remove(id,countryName)
  {

    if(confirm('Do you want to remove validations for '+countryName+' ?')){
          <?php
            $removeUrl = url_for('configurationManager/deleteCountryPaymentModeValidation');
          ?>
         var removeUrl = "<?php echo $removeUrl;?>"
         $.post(removeUrl,{id: id}, function(data){              
               location.reload();
         });
    }
  }
</script>