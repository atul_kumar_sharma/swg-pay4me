<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">  
  <?php include_partial('global/innerHeading',array('heading' =>'Currency Rate')); ?>
  <div class="clear"></div>
    <div class="tmz-spacer"></div>

    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br/>
    <?php }

     if($sf->hasFlash('error')){ ?>
    <div id="flash_error" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('error'));
      ?>
    </div><br/>
    <?php } ?>


   <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
   <tr>
   <td class="blbar" colspan="12" align="right"><div class="float_left">Application Rate List</div>
   </tr>
   <tr>
   <td width="5%" class="txtBold">Sr.No</td>
   <td width="5%" class="txtBold">Country Name</td>
   <td width="5%" class="txtBold">Currency</td>
   <td width="5%" class="txtBold">Passport Fees</td>
   <td width="5%" class="txtBold">Visa Fees</td>
   <td width="5%" class="txtBold">Visa Arrival Fees</td>
   <td width="5%" class="txtBold">Additional Charges for Visa Arrival</td>
   <td width="5%" class="txtBold">Fees Update Date(Y/M/D)</td>
   <td width="5%" class="txtBold">Action</td>
   </tr>
   <?php
   $currencyRateArray = $sf_data->getRaw('currencyRateArray');
//   echo "<pre>";print_r($currencyRateArray);die;
   if(count($currencyRateArray) > 0)  {
   $srNo = 1;
   foreach($currencyRateArray as $details){ 
       ?>


   <tr id="tr_id_<?php echo $details['id']; ?>">
   <td width="5%"><?php echo $srNo; ?>  </td>
   <td><?php $countryName = Doctrine::getTable('Country')->getCountryName($details['country_id']);
   echo $countryName ; ?> </td>
   <td width="5%"><?php echo $details['currency']; ?>  </td>
   <td width="10%" id="passport_<?php echo $details['id'];?>">
   <input style="width:50px" type ="text" name="passport" value="<?php echo $details['passport']; ?>" readonly="readonly">
   </td>
   <td width="10%" id="visa_<?php echo $details['id'];?>">
   <input style="width:50px" type ="text" name="visa" value="<?php echo $details['visa']; ?>" readonly="readonly">
   </td>
   <td width="10%" id="visa_arrival_<?php echo $details['id'];?>">
   <input style="width:50px" type ="text" name="visa_arrival" value="<?php echo $details['visa_arrival']; ?>" readonly="readonly">
   </td>
   <td width="10%" id="additional_vap_<?php echo $details['id'];?>">
   <input style="width:50px" type ="text" name="additional_vap" value="<?php echo $details['additional_charges_for_vap']; ?>" readonly="readonly">
   </td>
   <td width="10%"><?php echo date_format(date_create($details['updated_at']),'Y-m-d');  ?> </td>
   <td width="20px"valign="top">

   <table width="99%" cellspacing="0" cellpadding="0" style="border:0px;" >
   <tr>
   <td width ="20%"id="edit_<?php echo $details['id']; ?>" style="border:0px;"> <a href="javascript:void(0);" onclick="editCurrencyRate(<?php echo $details['id']; ?>)"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></a> </td>
   <td width="10%" id="save_<?php echo $details['id']; ?>" class="no_display no_border_padding" style="border:0px;"><a  href="javascript:void(0);" onclick="updateCurrencyRate(<?php echo $details['id'] ?>)" >Save</a></td>
   <td width="10%"id="cancel_<?php echo $details['id']; ?>" class="no_display no_border_padding" style="border:0px;"><a  href="javascript:void(0);" onclick="cancelRow(<?php echo $details['id'] ?>)" >Cancel</a></td>
  <!-- <td id="delete_<?php echo $details['id']; ?>" style="border:0px;"> <a href="javascript:void(0);" onclick="deleteRow(<?php echo $details['id']; ?>)">Delete</a> </td> -->

   </tr>
   </table>

   </td>


   </tr>
   <?php $srNo++; }} else { die('aa'); ?>
   <tr>
   <td>No Record Found</td>

   </tr>

 <?php }?>
   </table>

   </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    function editCurrencyRate(id){
        
        $('#tr_id_'+id+ '> td' ).children('input').attr('class','txt-input');
        $('#tr_id_'+id+ '> td').children('input').attr('readonly','');
        $('#edit_'+id).hide();
        $('#save_'+id).show();
//        $('#delete_'+id).hide();
        $('#cancel_'+id).show();

        
    }
    function updateCurrencyRate(id){
       passportFees = jQuery.trim($('#passport_'+id+' > input').val());
       visaFees = jQuery.trim($('#visa_'+id+' > input').val());
       visaArrivalFees = jQuery.trim($('#visa_arrival_'+id+' > input').val());
       additionalForVap = jQuery.trim($('#additional_vap_'+id+' > input').val());
       if(passportFees == ''){
           alert('Please enter Passport Fees value');
           return false;
       }else if(visaFees == ''){
           alert('Please enter Visa Fees value');
           return false;
       }else if(visaArrivalFees == ''){
           alert('Please enter Visa Arrival Fees value');
           return false;

       }else if(additionalForVap == ''){
           alert('Plese enter Additional Charges for Visa Arrival value');
           return false;
       }

//       alert(passportFees+visaFees+visaArrivalFees+additionalForVap);

       if(isNaN(passportFees) || isNaN(visaFees) || isNaN(visaArrivalFees) || isNaN(additionalForVap)) {
            alert('Please enter numeric value only');
            return false
        }
        
       if(confirm("Do you realy want to change the fees ?") == true){
       
        var url = "<?php echo url_for('configurationManager/updateCurrencyRate')?>";
        $.post(url,{id:id,passportFees:passportFees,visaFees:visaFees,visaArrivalFees:visaArrivalFees,additionalForVap:additionalForVap},function(data){
            location.reload();
            
          });
        }
      
    }
//    function deleteRow(id){
//        if(confirm("Do you realy want to delete this row") == true){
//        var url = "<?php echo url_for('configurationManager/deleteCurrencyRateRow')?>";
//        $.post(url,{id:id},function(data){
//            location.reload();
//
//        });
//       }
//    }

    function cancelRow(id){
        location.reload();
//        $('#save_'+id).hide();
//        $('#cancel_'+id).hide();
//        $('#edit_'+id).show();
//        $('#delete_'+id).show();
//        $('#tr_id_'+id+ '> td' ).children('input').attr('class','');
//        $('#tr_id_'+id+ '> td').children('input').attr('readonly','readonly');
    }
    </script>
