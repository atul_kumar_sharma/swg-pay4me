<?php use_helper('Form');
      use_helper('Pagination');
?>

<div class="global_content4 clearfix">
<div class="brdBox">
    <div class="clearfix"/>
        <?php echo form_tag('report/merchantPaymentReport',array('name'=>'pfm_report_form','class'=>'dlForm', 'method'=>'post','id'=>'pfm_report_form')) ?>
            <fieldset class="multiform wd90">
                <legend>Payment Report</legend>
                <div class="wrapForm2">

                    <?php echo $splitForm ?>
                    <div class="mgauto">
                        <?php   echo submit_tag('Search',array('class' => 'paymentbutton','onClick'=>'validate_merchant_form()')); ?>

                    </div>
                </div>
            </fieldset>
        </form>
        <div id="result_div" style="width:500px" align="center"></div>
    </div>

</div>

