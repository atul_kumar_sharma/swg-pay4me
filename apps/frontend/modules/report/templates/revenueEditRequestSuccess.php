<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Pagination');
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');

include_partial('global/innerHeading',array('heading'=>'Revenue Edit Request'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<div id="nxtPage">
<table width="99%">
  <tr>
    <td class="blbar" colspan="8" align="right">
    <div style="float:left">Revenue Edit Request Details</div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
  </tr>
  <tr>
      <td width="6%"><span class="txtBold">S. No.</span></td>
      <td width="16%"><span class="txtBold">Current Amount</span></td>
      <td width="10%"><span class="txtBold">Date</span></td>
      <td width="15%"><span class="txtBold">Project</span></td>
      <td width="14%"><span class="txtBold">Account</span></td>
      <td width="31%"><span class="txtBold">Support Request</span></td>
      <td width="16%"><span class="txtBold">Status</span></td>
      <td width="16%"><span class="txtBold">Action</span></td>

  </tr>
    <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
    ?>
    <?php
      foreach ($pager->getResults() as $result):     
//      echo "<pre>";print_r($result->toArray());die;
        $i++;
    ?>
    <?php if($result['status']=='Requested'){ ?>

    <tr>
      <td><?php echo $i;?></td>
      <td width="16%"><span class="txtBold"><?php echo "$".number_format($result['current_amount'],2);?></span></td>
      <td width="10%"><span class="txtBold"><?php echo $result['date'];?></span></td>
      <td width="15%"><span class="txtBold"><?php echo $result['TblProjectName']['name'];?></span></td>
      <td width="14%"><span class="txtBold"><?php echo $result['TblBankDetails']['account_name']; ?></span></td>
      <td width="31%"><span class="txtBold"><?php echo $result['comments']?></span></td>
      <td width="16%"><span class="txtBold"><?php echo $result['status']; ?></span></td>
      <td width="16%" align="center">
                <u><span style="cursor:pointer" onclick="openRequestEditWindow('<?php echo $result['id'];?>','<?php echo $result['TblBankDetails']['account_name'];?>','<?php echo $result['TblProjectName']['name'];?>')"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></span></u>
            </td>
    </tr>
    <?php }else{ ?>
     <tr>
      <td><?php echo $i;?></td>
      <td width="16%"><span class="txtRight"><?php echo "$".number_format($result['current_amount'],2);?></span></td>
      <td width="10%"><span class="txtRight"><?php echo $result['date'];?></span></td>
      <td width="15%"><span class="txtRight"><?php echo $result['TblProjectName']['name'];?></span></td>
      <td width="14%"><span class="txtRight"><?php echo $result['TblBankDetails']['account_name']; ?></span></td>
      <td width="31%"><span class="txtRight"><?php echo $result['comments']?></span></td>
      <td width="16%"><span class="txtRight"><?php echo $result['status']; ?></span></td>
      <td width="16%" align="center">
                <u><span class="hand" onclick="openRequestEditWindow('<?php echo $result['id'];?>','<?php echo $result['TblBankDetails']['account_name'];?>','<?php echo $result['TblProjectName']['name'];?>')"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></span></u>
            </td>
    </tr>
    <?php } ?>
    
    <?php endforeach; ?>
    <tr>
        <td class="blbar" colspan="8" height="25px" align="right">
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></td>
    </tr>
    <?php
    }else { ?>
    <tr><td  colspan="8" align='center' ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>
  </table>
  </div>
</div>
</div><div class="content_wrapper_bottom"></div>
<script>
    function openRequestEditWindow(id,accountId,projectId){
    
      accountId = '&accountId='+accountId;
      projectId = '&projectId='+projectId;
      

    emailwindow = dhtmlmodal.open('EmailBox', 'iframe', '<?php echo url_for('report/revenueEdit').'?id='?>' +id +accountId + projectId, 'Admin Comments', 'width=620px,height=250px,center=1,border=0, scrolling=no')
  }

    function reloadWindow() {
      window.location.reload();
    }
</script>