<div id="flash_notice" class="alertBox" style="display:none;">
</div>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
use_helper('Form');
use_helper('Pagination');

include_partial('global/innerHeading',array('heading'=>'Revenue Management'));?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
  <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tbody>
        <tr>
            <td width="20%"> Project Name<font color="red">*</font>: </td>
            <td width="20%" class="border_none"><?php
            if(!empty($projectId)){
              $projectId = $projectId;
            }else{
              $projectId = '';
            }

            echo select_tag('project_id', options_for_select($project,$projectId, array('include_custom' => '-- Please Select --')),array('onchange'=>'getAccount()','style'=>'width:150px')); ?>
            <input type="hidden" name="hdn_project" id="hdn_project" value="<?php echo $projectId;?>">
              <div class="red" id="sel_project_err"> </div></td>
            <td width="20%"> Account Name<font color="red">*</font>: </td >
            <td width="20%" class="border_none"><?php
            $array = array(''=>'-- Please Select --');
            echo select_tag('account_id', options_for_select($array),array('style'=>'width:150px')); ?>
            <input type="hidden" name="hdn_account" id="hdn_account" value="<?php echo $accountId;?>">
              <div class="red" id="sel_account_err"> </div></td>
            <td width="20%" class="border_none" align="right"><span class="hand bluelink" onclick="openBankAccouontWindow('add')"><u>Add New Account</u></span> </td>
        </tr>
        <tr id='month_tr' >
            <td width="20%">Year of Transaction<font color="red">*</font> : </td>
            <td width="20%" class="border_none"><?php
            if(!empty($yearId)){
              $yearId = $yearId;
            }else{
              $yearId = '';
            }
            $arrYear = array('2010'=>'2010','2011'=>'2011','2012'=>'2012','2013'=>'2013','2014'=>'2014','2015'=>'2015','2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020');
            echo select_tag('sel_year', options_for_select($arrYear, $yearId, array('include_custom' => '-- Please Select --')),array('onchange'=>'getMonth()','style'=>'width:150px')); ?>
            <input type="hidden" name="hdn_year" id="hdn_year" value="<?php echo $yearId;?>">
            <!-- <select name="sel_year" id="sel_year" style="width:150px;" onchange="getMonth()">
<option value="">-- Please Select --</option>
<option value="2010">2010</option>
<option value="2011">2011</option>
<option value="2012">2012</option>
<option value="2013">2013</option>
<option value="2014">2014</option>
<option value="2015">2015</option>
<option value="2015">2016</option>
<option value="2015">2017</option>
<option value="2015">2018</option>
<option value="2015">2019</option>
<option value="2015">2020</option>
</select>
            -->
            <br />
              <div class="red" id="sel_year_err"> </div></td>
            <td width="20%"  > Month of Transaction<font color="red">*</font>: </td>
            <td width="20%" class="border_none" colspan="2"><select name="sel_mon" id="sel_mon" align="left" style="width:150px;">
              <option value="">-- Please Select --</option>
            </select>
            <input type="hidden" name="hdn_month" id="hdn_month" value="<?php echo $monthId;?>">
            <br />
              <div class="red" style="width:45%" id="sel_mon_err"> </div></td>
        <tr>
            <td colspan="5"><center>
                <input type="button" name="btn_search" id="btn_search" value="Search" class="normalbutton" onclick="searchReport();">
              </center></td>
        </tr>
      </tbody>
    </table>
  </form>
  <br />
  <div id="result_div"  align="center"></div>
    <div id="entry"> </div>
  <div >
    <center>
      <h5>Note: The reports will be displayed per project and per account</h5>
    </center>
  </div>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script type="text/javascript">
  window.onload = loadAllFunction();


  function loadAllFunction(){
    getAccount();
    getMonth();
    showReportList();
  }
  function getAccount(){
    var project_id = $('#project_id').val();
    var hdn_account = '';
    if($('#hdn_account').val() !='' ){

      var hdn_account = $('#hdn_account').val();

    }
    var url = "<?php echo url_for('report/projectAccount'); ?>";
    $.post(url, {project_id: project_id,account_id: hdn_account},function(data){
      $('#account_id').html(data);
    });
  }

  function searchReport(){
    var err = 0;

    if($('#project_id').val() ==''){
      $("#sel_project_err").html('Please select Project Name');
      err = err+1;
    }else{
      $("#sel_project_err").html('');
    }


    if($('#account_id').val() ==''){
      $("#sel_account_err").html('Please select Account Name');
      err = err+1;
    }else{
      $("#sel_account_err").html('');
    }


    if($('#sel_mon').val() ==''){
      $("#sel_mon_err").html('Please select Month');
      err = err+1;
    }else{
      $("#sel_mon_err").html('');
    }


    if($('#sel_year').val() ==''){
      $("#sel_year_err").html('Please select Year');
      err = err+1;
    }else{
      $("#sel_year_err").html('');
    }

    if(err == 0){
      var selmonth = $('#sel_mon').val();
      var selyear = $('#sel_year').val();
      var project_id = $('#project_id').val();
      var account_id = $('#account_id').val();
      var url = "<?php echo url_for('report/nisDollarReportList'); ?>";
      $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
      $("#result_div").show();
      $('#entry').html('');
      $.post(url, {sel_month: selmonth,sel_year: selyear,account_id: account_id,project_id: project_id},function(data){
        $('#entry').html(data);
        $("#result_div").hide();
      });
    }else{
      return false;
    }

  }

  function reloadBankWindow() {
    var project_id = parent.document.getElementById('project_id').value;
    var account_id = '';
    if(parent.document.getElementById('account_id').value !=''){
      account_id = parent.document.getElementById('account_id').value;
    }
    var url = "<?php echo url_for('report/projectAccount'); ?>";
    $.post(url, {project_id: project_id,account_id: account_id},function(data){
      $('#account_id').html(data);
    });
  }

  function openBankAccouontWindow(mode){
    var project_id = $('#project_id').val();
    if(mode == 'edit'){
      var account_id = $('#account_id').val();
      account_id = '&account_id='+account_id;
      var action_type = 'Edit Account';
    }else{
      var account_id = '';
      var action_type = 'Add New Account';
    }
    mode = '&mode='+mode;
    if(project_id == ''){
      alert('Please select project for adding new account!!');
    }else{
      emailwindow = dhtmlmodal.open('EmailBox', 'iframe', '<?php echo url_for('report/addBankAccount').'?project_id='?>' + project_id+account_id+mode, action_type, 'width=620px,height=350px,center=1,border=0, scrolling=no')
    }
  }
  function getMonth(){
    var sel_year = $('#sel_year').val();
    var sel_month = '';
    if($('#hdn_month').val() !=''){
      sel_month = $('#hdn_month').val();
    }
    //alert(sel_month);
    var url = "<?php echo url_for('report/getMonth'); ?>";
    $.post(url, {sel_year: sel_year,hdn_month: sel_month},function(data){
      $('#sel_mon').html(data);
    });
  }

  function showReportList(){

    if($('#hdn_month').val() !='' && $('#hdn_year').val() !='' && $('#hdn_project').val() !='' && $('#hdn_account').val() !=''){
      var selmonth = $('#hdn_month').val();
      var selyear = $('#hdn_year').val();
      var project_id = $('#hdn_project').val();
      var account_id = $('#hdn_account').val();
      var url = "<?php echo url_for('report/nisDollarReportList'); ?>";

      $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
      $("#result_div").show();
      $('#entry').html('');
      $.post(url, {sel_month: selmonth,sel_year: selyear,account_id: account_id,project_id: project_id},function(data){
        $('#entry').html(data);
        $("#result_div").hide();
      });

    }


  }

</script>