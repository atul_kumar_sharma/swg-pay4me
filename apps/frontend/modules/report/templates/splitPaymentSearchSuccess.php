<?php use_helper('Pagination');

?>
<div class="clearfix">
    <div id="nxtPage">
        <table class="innerTable_content">
        <?php if($count>0){?>
            <tr>
                <td class="blbar"  colspan="2" align="left"><div style="float:left">Summary</div>
                <div style="float:right">
                    <form name="detail" id="detail" method="post">
                        <input type="hidden" name="startdate" id="startdate" value="<?php echo $fromDate?>">
                        <input type="hidden" name="enddate" id="enddate" value="<?php echo $toDate?>">
                        <input type="button" onclick="validate_form();" value="Detail report">
                    </form>
               </div>
             </td>
            </tr>
            <tr>
                <td>Transaction Fee</td>
                <td>6.00%</td>
            </tr>
            <tr>
                <td>Total Transactions</td>
                <td><?php echo $totalTransaction?></td>
            </tr>
            <tr>
                <td>Total Amount</td>
                <td><?php echo $totalAmount;?></td>
            </tr>
            <tr>
                <td>Total Payment</td>
                <td><?php echo $totalPayment;?></td>
            </tr>
            <tr>
                <td>Total Fee</td>
                <td><?php echo $totalFee;?></td>
            </tr>
            <?php }else{?>
            <tr>
                <td class="blbar"  colspan="2" align="left"><div style="float:left">Summary</div>                
             </td>
            </tr>
            <tr><td  colspan="2" align='center' ><div style="color:red"> No record found</div></td></tr>
            <?php }?>
            
        </table>
    </div>
    <div>&nbsp;</div>
    <div id="detail_div"  class="clearfix"></div>
</div>
 <script>
  function validate_form(){
    var err = 0;
    if(err == 0){
       $.post('swGlobalReportdetail',$("#detail").serialize(), function(data){
         if(data == "logout"){
        location.reload();
       }else{          
        $("#detail_div").html(data);
       }
    });
    }
  }
  </script>