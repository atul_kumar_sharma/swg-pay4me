<?php use_stylesheet('style.css');
use_javascript('calendar.js');
echo include_stylesheets();
echo include_javascripts(); ?>

<div class="clearfix">
  <div id="nxtPage">
  <form action='#' method="post" name="frm_tra_detail" id="frm_tra_detail" onsubmit="return false;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
          <td width="200px;"><?php echo $form['account_name']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['account_name']->render(); ?>
              <br>
              <div class="red" id="account_name_error">
                  <?php echo $form['account_name']->renderError(); ?>
              </div>
          </td>
      </tr>

      <tr>
          <td width="200px;"><?php echo $form['account_number']->renderLabel(); ?><span class="red"></span></td>
          <td><?php echo $form['account_number']->render(); ?>
              <br>
              <div class="red" id="account_error">
                  <?php echo $form['account_number']->renderError(); ?>
              </div>


          </td>

      </tr>
      <tr id="orderNumber_row" >
          <td ><?php echo $form['bank_name']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['bank_name']->render(); ?>
              <br>
              <div class="red" id="bank_error">
                  <?php echo $form['bank_name']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr id="orderNumber_row" >
          <td ><?php echo $form['city']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['city']->render(); ?>
              <br>
              <div class="red" id="city_error">
                  <?php echo $form['city']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr id="orderNumber_row" >
          <td ><?php echo $form['state']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['state']->render(); ?>
              <br>
              <div class="red" id="state_error">
                  <?php echo $form['state']->renderError(); ?>
              </div>

          </td>
      </tr>
      <tr id="orderNumber_row" >
          <td ><?php echo $form['country']->renderLabel(); ?><span class="red"></span></td>
          <td colspan="2"><?php echo $form['country']->render(); ?>
              <br>
              <div class="red" id="country_error">
                  <?php echo $form['country']->renderError(); ?>
              </div>
<input type="hidden" id="hdn_id" name="hdn_id" value="<?php echo $projectId;?>">
<input type="hidden" id="hdn_account" name="hdn_account" value="<?php echo $account_id;?>">
<input type="hidden" id="hdn_mode" name="hdn_mode" value="<?php echo $mode;?>">
          </td>

      </tr>


      <tr>
        <td align="center" colspan="2">
          <div class="divBlock">
            <center><input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Save" onclick="validateForm();">
            &nbsp;&nbsp;
            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="parent.emailwindow.hide();">

            </center>
          </div>
        </td>
      </tr>
    </table>
    </form>
    <br>
  </div>
</div>

<script type="text/javascript">


  function validateForm(){

     var err  = 0;

    if($('#bank_account_name').val() == "")
    {
      $('#account_name_error').html("Please enter account name");
      err = err+1;
    }else
    {
      $('#account_name_error').html(" ");
    }

    if($('#bank_account_number').val() == "")
    {
      $('#account_error').html("Please enter account number");
      err = err+1;
    }else
    {
      $('#account_error').html(" ");
    }

      if($('#bank_bank_name').val() == "" )
      {
        $('#bank_error').html("Please enter bank name");
        err = err+1;
      }else
      {
        $('#bank_error').html(" ");
      }

      if($('#bank_city').val() == "" )
      {
        $('#city_error').html("Please enter bank city");
        err = err+1;
      }else
      {
        $('#city_error').html(" ");
      }

      if($('#bank_state').val() == "" )
      {
        $('#state_error').html("Please enter bank state");
        err = err+1;
      }else
      {
        $('#state_error').html(" ");
      }

      if($('#bank_country').val() == "" )
      {
        $('#country_error').html("Please enter bank country");
        err = err+1;
      }else
      {
        $('#country_error').html(" ");
      }


      if(err == 0)
      {

        // Call ajax
        var hdn_id = $('#hdn_id').val();
        var hdn_account = '';
        if($('#hdn_mode').val() == 'edit'){

            var hdn_account = $('#hdn_account').val();

        }
        var account_name = $('#bank_account_name').val();
        var account_num = $('#bank_account_number').val();

        var bank_name = $('#bank_bank_name').val();
        var city = $('#bank_city').val();
        var state = $('#bank_state').val();
        var country = $('#bank_country').val();

        $.post("<?php echo url_for('report/savebankAccountDeatil') ?>", {hdn_id: hdn_id,account_num: account_num,account_name: account_name,
            bank_name: bank_name,city: city,state: state,
            country: country,hdn_account: hdn_account},
          function(data){
            if($('#hdn_mode').val() == 'edit'){
              parent.reloadWindow();
              parent.reloadBankWindow();
              parent.document.getElementById('flash_notice').style.display = 'block';
              parent.document.getElementById('flash_notice').innerHTML = 'Account has been updated successfully';
            }else{
              parent.reloadBankWindow();
              parent.document.getElementById('flash_notice').style.display = 'block';
              parent.document.getElementById('flash_notice').innerHTML = data;
            }

            parent.emailwindow.hide();

            //alert(data);


          }
        );
      }else{
          return false;
      }
  }



  </script>