<?php use_helper('Pagination');

?>
<div class="clearfix">
    <div id="nxtPage">
    <?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
        <table class="innerTable_content" >
            <tr>

                <td align="right" class="blbar">
                    Showing <b><?php echo ( $pager->getNbResults()>0 ? $pager->getFirstIndice() : 0 ); ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results
                </td>
            </tr>
             <!--tr>
                <td>
                    <div id = "csvDiv" style="cursor:pointer;" onclick="javascript:progBarDiv('reportStatementCsv', 'csvDiv', 'progBarDiv')"><span  class="floatLeft">Click here to download data in <font color="red"><b><u>CSV</u></b></font> Format</span></div><div id ="progBarDiv"  style="display:none;"></div><br class="pixbr" />
                </td>
            </tr-->
        </table>
        <div class="clearfix"></div>
    </div>

    <table class="innerTable_content" >

        <tr>
            <td width = "2%"><b>S.No.</b></td>
            <td align="center"><b>Order No.</b></td>
            <td align="center"><b>Response Code</b></td>
            <td align="center"><b>Response Text</b></td>
            <td align="center"><b>Status</b></td>
            <td align="center"><b>Date</b></td>
            <td align="center"><b>Action</b></td>
        </tr>

        <?php
        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $i = max(($page-1),0)*$limit ;
            foreach ($pager->getResults() as $result):
            $i++;
            if($gateway =='')
            $gateway = "BLANK";
            if($orderNo =='')
            $orderNo = "BLANK";
            if($from_date =='')
            $from_date = "BLANK";
            if($to_date =='')
            $to_date = "BLANK";
            if($status =='')
            $status = "BLANK";
            
            ?>
        <tr>
            <td align="center"><?php echo $i ?></td>
            <td align="center"><?php echo $result->getOrderNumber();?></td>
            <td align="center"><?php echo $result->getResponseCode();?></td>
            <td align="center"><?php echo  $result->getResponseText(); ?></td>
            <td align="center"><?php echo ( $result->getAuthorizeStatus() ? 'Authorize' : 'Capture' )?></td>
            <td align="center"><?php echo $result->getTransDate(); ?></td>
            <td align="center">
            <?php if($result->getAuthorizeStatus()){?>
                           <a class="approveInfo" href="#" onclick="sendReq(<?php echo $result->getId();?>,'Capture');" title="Capture" alt="Capture"></a>
                        <?php }else{echo '--';}?></td>
        </tr>
        <?php endforeach; ?>

        <tr>
            <td colspan="7" class="blbar" height="25" align="right">
                <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date.'&order_number='.$orderNo.'&gateway='.$gateway.'&status='.$status), 'result_div')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>
                </div>
            </td>
        </tr>

            <?php }
        else { ?>
        <tr><td  align='center' colspan="7" ><div style="color:red"> No record found</div></td></tr>
        <?php } ?>



    </table>




    <div>&nbsp;</div>

</div>

<script>
function progBarDiv(url, referenceDivId, targetDivId){
    $('#'+targetDivId).html('<?php echo image_tag('../images/ajax-loader_1.gif'); ?>');
    $('#'+referenceDivId).css("display","none");
    $('#'+targetDivId).css("display","inline");
    $('#'+targetDivId).load(url, {byPass:1 },function (data){

                            if(data=='logout'){
                                $('#'+targetDivId).html(data);
                                location.reload();
                              } });

}
function sendReq(userid,state){
        var activate_url = "<?php echo url_for('report/setAuthorizeStatus');?>";
        $.post(activate_url,{id:userid,status:state}, function(data){
            if(data){
                validate_search_form();
            }});

    }
</script>