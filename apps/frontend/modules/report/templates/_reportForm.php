<?php use_helper('Form');
use_helper('Pagination');?>
<div class="global_content4 clearfix">
    <div class="brdBox"> <?php //echo ePortal_pagehead('Change Password'); ?>
        <div class="clearfix"/>
        <?php echo form_tag('report/paymentReport',array('name'=>'pfm_report_form','class'=>'', 'method'=>'post','id'=>'pfm_report_form')) ?>
        <div class="wrapForm2">
            <table width="50%">

                <?//php echo $splitForm;?>
                <tbody>
                <?php if($splitForm->renderGlobalErrors()){?>
                    <tr>
                        <td colspan="2">
                            <?php echo $splitForm->renderGlobalErrors() ?>
                        </td>
                    </tr>
                    <?}?>
                    <tr>
                        <th><?php echo $splitForm['from_date']->renderLabel(); ?><sup><font color="red">* </font></sup></th>
                        <td>
                            <?php echo $splitForm['from_date']->render(); ?>
                            <?php echo $splitForm['from_date']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $splitForm['to_date']->renderLabel(); ?><sup><font color="red">* </font></sup></th>
                        <td>
                            <?php echo $splitForm['to_date']->render(); ?>
                            <?php echo $splitForm['to_date']->renderError(); ?>
                        </td>
                    </tr>

                    <tr>
                        <th><?php echo $splitForm['bill_number']->renderLabel(); ?></th>
                        <td><?php echo $splitForm['bill_number']->render(); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo $splitForm['order_number']->renderLabel(); ?></th>
                        <td>
                        <?php echo $splitForm['order_number']->render(); ?></td>
                    </tr>

                </tbody><tr>
                    <td> </td>
                    <td><div class="lblButton">
                        <input type="submit" class="button" value="Search" name="commit"/>                        </div>
                        <div class="lblButtonRight">
                        <div class="btnRtCorner"/>
                    </div></td>
            </tr></table>

            <br />
            <div id="result_div" style="width:500px" align="center"></div>
        </div>
        </form>
    </div>
</div>

