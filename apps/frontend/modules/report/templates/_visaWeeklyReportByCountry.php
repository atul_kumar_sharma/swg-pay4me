<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($reportArr)) { ?>
<table width="99%" border="0">
    <tr>
        <td class="blbar" align="left" colspan="9">
            <div align="left" style="float:left">Country Wise Visa Weekly Report</div>
            <div align="right" style="float:right" id="excelDivId"><a href="javascript:void(0);" onclick="exportExcel();" title="Export to Excel"><?php echo image_tag('excel_icon.gif'); ?></a>
        </td>
    </tr>
    <tr>
        <td class="txtBold">Country</td>
        <td class="txtBold">Week Start</td>
        <td class="txtBold">Week End</td>
        <td class="txtBold" width="170">Total Applications</td>
        <td class="txtBold">Application Amount($)</td>
        <td class="txtBold"><?php echo ucwords(sfConfig::get('app_visa_additional_charges_text'));?>($)</td>
        <td class="txtBold">Total Amount($)</td>
    </tr>
    <?php

    if(count($reportArr) > 0) { ?>

        <?php foreach($reportArr AS $key => $report) { ?>

    <tr bgcolor="#EEEEEE">
        <td colspan="9" class="txtBold"><?php echo $countryListObj[$key]; ?></td>
    </tr>

                <?php
                $tmpYear = '';
                foreach($report AS $value){

                    
                        $week_start = date('j M, Y', strtotime($value['week_start']));
                        $week_end = date('j M, Y', strtotime($value['week_end']));
                    
                    
                    ?>
    <td>&nbsp;</td>
    <td><?php echo $week_start; ?></td>
    <td><?php echo $week_end; ?></td>
    <td><?php echo ($value['count'] > 0)?$value['count']:'--'; ?></td>
    <td><?php
        //$currencySymbol = PaymentModeManager::currencySymbol(4);
        echo ($value['app_amount'] > 0)?number_format($value['app_amount'],2):'--';
        //echo ($value['app_convert_amount'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['app_convert_amount'],2).')':'';
        ?></td>
    <td><?php
        echo ($value['service_charge'] > 0)?number_format($value['service_charge'],2):'--';
        //echo ($value['app_convert_service_charge'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['app_convert_service_charge'],2).')':'';
        ?></td>
    <td><?php
        echo ($value['total_amount'] > 0)?number_format($value['total_amount'],2):'--';
        //echo ($value['total_convert_amount'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['total_convert_amount'],2).')':'';
        ?></td>
    </tr>



                    <?php } } ?>




            <?php } else { ?>

    <td align="center" colspan="9">No Record Found</td>

    <script>
        $('#excelDivId').hide();
    </script>

            <?php } ?>
</table>

    <?php }

?>
