<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Payment Transactions'));
?>
<script type="text/javascript">
function disableForm(frm){

        if(validate_frm(frm)){
            $('#submit').attr('disabled',true);
            return true;
        }else{
            return false;
        }
    }
</script>
<div class="global_content4 clearfix">
<div class="brdBox">
 <div class="clearfix"/>
<?php echo form_tag('report/paymentTransactionsReport',array('name'=>'paymentTransaction_search_form','class'=>'', 'method'=>'post','id'=>'paymentTransaction_search_form')) ?>

<div class="wrapForm2">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }?>
            <table width="50%">
                <?php echo $paymentTransactionReportform;?>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php   echo submit_tag('Search',array('class' => 'button')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            <br />
            <div id="result_div" style="width:500px" align="center"></div>
        </div>


</form>
</div></div>
</div>
<script>
<?php
if("valid" == $formValid )
{
    ?>
        $(document).ready(function()
        {
            validate_search_form();
        });
    <?php
}
?>

   function validate_search_form(){
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;

        if(err == 0){
            $.post('transactionReportGraph',$("#paymentTransaction_search_form").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
        }
   }
    function setDurationForDate(){

        var from_date = $('#paymentTransaction_startDate').val();
        var to_date = $('#paymentTransaction_endDate').val();
        
        
        if(from_date !=  to_date){alert('come');

           // $("#paymentTransaction_startDate option[value='1']").attr("disabled", true);
           // $("#paymentTransaction_duration option [value='45']").remove();
            $("#paymentTransaction_duration option['value=45']").attr("disabled",true);
        }else{
             $("#paymentTransaction_duration").attr("disabled", false);
        }
    }
</script>
