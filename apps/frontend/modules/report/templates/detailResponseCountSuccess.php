<?php use_helper('Pagination'); ?>
<div class="clearfix">
    <div id="nxtPage">
        <table class="innerTable_content">
            <tr>
                <td  width="100px" align="left"><span class="txtBold">Gateway</span></td>
                <td  align="left"><span ><?php echo $gatewayName->getFirst()->getName(); ?></span></td>
            </tr>
        </table>
        <br>

        <table class="innerTable_content">
            <tr>
                <td class="blbar" colspan="7" align="right">
                <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
            </tr>
            <tr>
                <td width="5%"><span class="txtBold">S. No. </span></td>
                <td width="10%"><span class="txtBold">Order Number</span></td>
                <td width="10%"><span class="txtBold">Item Number</span></td>
                <td width="15%"><span class="txtBold">Last Updated</span></td>
                <td width="8%"><span class="txtBold">Card Type</span></td>
                <td width="30%"><span class="txtBold">User Name</span></td>
                <td width="8%"><span class="txtBold">Amount</span></td>
            </tr>

            <?php
            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                ?>

                <?php
                foreach ($pager->getResults() as $key=>$result):
                $i++;

                ?>

            <tr>

                <td ><?php  echo $i;?></td>
                <td ><?php  echo $result->getOrderNumber(); ?></td>
                <td ><?php  echo $result->getOrderRequestDetails()->getOrderRequest()->getTransactionNumber(); ?></td>
                <td ><?php  echo $result->getUpdatedAt();  ?>
                <td ><?php  echo $result->getCardType(); ?>
                <td ><?php  echo $result->getOrderRequestDetails()->getSfGuardUser()->getUserName();  ?>
                <td ><?php  echo $result->getOrderRequestDetails()->getOrderRequest()->getAmount(); ?>

                </td>
            </tr>
            <?php endforeach; ?>

            <tr>
                <td class="blbar" colspan="8" height="25px" align="right">

            <?php  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?startDate='.Settings::encryptInput($startDate).'&endDate='.Settings::encryptInput($endDate).'&gateway='.Settings::encryptInput($gateway).'&responseCode='.Settings::encryptInput($responseCode).'&usePager=true');?></td>
            </tr>
            <?php
        }else { ?>
            <tr><td  colspan="8" align='center' ><div style="color:red"> No record found</div></td></tr>
            <?php } ?>
            <tr>
        <td align="center" colspan="8">
            <center>
            <input style="" type="button" class="loginbutton"  id="multiFormSubmit" value="Close" onclick="window.close();">
            </center>
        </td>
      </tr>

        </table>

    </div>
</div>

