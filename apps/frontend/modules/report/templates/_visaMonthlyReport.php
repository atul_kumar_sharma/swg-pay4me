<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if(isset($reportArr)) { ?>

    

    
    <table width="99%" border="0">



    <tr>
        <td class="blbar" align="left" colspan="6">
            <div align="left" style="float:left">Visa Monthly Report</div>
            <div align="right" style="float:right" id="excelDivId"><a href="javascript:void(0);" onclick="exportExcel();" title="Export to Excel"><?php echo image_tag('excel_icon.gif'); ?></a>
          </div>

         </td>
        
    </tr>
    <tr>
    <td class="txtBold">Month</td>
    <td class="txtBold" width="170">Total Applications</td>
    <td class="txtBold">Application Amount($)</td>
    <td class="txtBold"><?php echo ucwords(sfConfig::get('app_visa_additional_charges_text'));?>($)</td>
    <td class="txtBold">Total Amount($)</td>
    </tr>
    <?php

    $tmpYear = '';
    if(count($reportArr) > 0) {
    $tmp_country = '';
    foreach($reportArr AS $value) {

    if($value['processing_country'] != $tmp_country){ ?>

        <tr bgcolor="#EEEEEE">
            <td class="txtBold" colspan="5">Country: <?php echo $countryListObj[$value['processing_country']]; ?> </td>
        </tr>



        
    <?php }

    $tmp_country = $value['processing_country'];
 

?>

    <tr>
    <td><?php echo $value['month'].' ('.$value['year'].') '; ?></td>
    <td><?php echo ($value['count'] > 0)?$value['count']:'--'; ?></td>
    <td><?php
            //$currencySymbol = PaymentModeManager::currencySymbol(4);
            echo ($value['app_amount'] > 0)?number_format($value['app_amount'],2):'--';
            //echo ($value['app_convert_amount'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['app_convert_amount'],2).')':'';
     ?></td>
    <td><?php
        echo ($value['service_charge'] > 0)?number_format($value['service_charge'],2):'--';
        //echo ($value['app_convert_service_charge'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['app_convert_service_charge'],2).')':'';
        ?></td>
    <td><?php
        echo ($value['total_amount'] > 0)?number_format($value['total_amount'],2):'--';
        //echo ($value['total_convert_amount'] > 0)?' ('.html_entity_decode($currencySymbol).number_format($value['total_convert_amount'],2).')':'';
        ?></td>
    </tr>



    <?php } ?>    


    <?php } else { ?>
    
    <td align="center" colspan="5">No Record Found</td>
    <script>
        $('#excelDivId').hide();
    </script>
    
    <?php } ?>
   </table>

<?php }



?>

