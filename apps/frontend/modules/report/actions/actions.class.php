<?php

/**
*  order actions.
*
* @package    mysfp
* @subpackage  order
* @author     Your name here
* @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
*/
class reportActions extends sfActions {

/**
* Executes index actionvalidated params
*
* @param sfRequest $request A request object
*/
    public static $MERCHANT_REPORT = "/report/merchantPaymentReport";
    public static $SWGLOBAL_REPORT = "/report/swGlobalPaymentReport";

    public function executePaymentHistory(sfWebRequest $request) {
        $userId = $this->getUser()->getGuardUser()->getId();
        $version = 'V1';
        $orderManagerObj = orderServiceFactory::getService($version);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $purchaseHistory = $orderManagerObj->getPurchaseHistory($userId);

        $this->pager = new sfDoctrinePager('OrderRequest', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($purchaseHistory);
        $this->pager->setPage($this->page);
        $this->pager->init();
        $this->setTemplate('PaymentHistory');
    }

    public function executeGetReceipt(sfWebRequest $request) {

        $this->receiptId = $request->getParameter('receiptId');
        if(!is_numeric($this->receiptId)){
            $this->receiptId = Settings::decryptInput($this->receiptId);
        }
        $ipayNisDetailsQuery = Doctrine::getTable('CartItemsTransactions')->getCartDetailsQuery($this->receiptId);
        ## If there is no query set then user redirected to welcome page...
        if(!empty($ipayNisDetailsQuery)){
            $this->ipayNisDetailsQuery = $ipayNisDetailsQuery->execute()->toArray();
            if(count($this->ipayNisDetailsQuery) > 0){
                $orderObj = Doctrine::getTable('ipay4meOrder')->getReceipt($this->receiptId);
                if (!$orderObj) {
                    //        /print "fd";exit;
                    $this->getUser()->setFlash('notice', 'You are not authorized for this receipt.');
                }
                $orderDetailObj = $orderObj->getFirst();
                $ordUID = $orderDetailObj->getOrderRequestDetails()->getUserId();
                $curUId = $this->getUser()->getGuardUser()->getId();
                if (!isset($ordUID) || $curUId != $ordUID) {
                    $this->getUser()->setFlash('notice', 'You are not authorized for this receipt.');
                }
                $this->cardData = $orderDetailObj;
                $this->getDetails = $orderDetailObj->getOrderRequestDetails();
                $this->merchantDetails = $this->getDetails->getOrderRequest()->getMerchant();
            }else{
                $this->getUser()->setFlash('notice', 'You are not authorized for this receipt.');
                $this->redirect('welcome/index');
            }
        }else{
            $this->getUser()->setFlash('notice', 'You are not authorized for this receipt.');
            $this->redirect('welcome/index');
        }

    }

    public function executePaymentReport(sfWebRequest $request) {
        $this->splitform = new userPaymentreportForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {
            $this->splitform->bind($request->getParameter('report'));
            if ($this->splitform->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executePaymentReportSearch(sfWebRequest $request) {

        $this->splitform = new userPaymentreportForm();
        $this->splitform->bind($request->getParameter('report'));
        $usePager = $request->getParameter('page');
        if ($this->splitform->isValid() || (isset($usePager) )) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['report']))
            $postDataArray = $postDataArray['report'];

            if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
                $this->from_date = $postDataArray['from_date'];
                $dateArray = explode("-", $this->from_date);
                $this->fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
            } else {
                $this->fromDate = "";
            }
            if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
                $this->to_date = $postDataArray['to_date'];
                $dateArray = explode("-", $this->to_date);
                $this->toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
            } else {
                $this->toDate = "";
            }
            if ($postDataArray['order_number'] != "" && $postDataArray['order_number'] != "BLANK") {
                $this->orderNo = $postDataArray['order_number'];
            } else {
                $this->orderNo = "";
            }
            if ($postDataArray['bill_number'] != "" && $postDataArray['bill_number'] != "BLANK") {
                $this->billNo = $postDataArray['bill_number'];
            } else {
                $this->billNo = "";
            }
            $userObj = Doctrine::getTable('Ipay4meOrder')->getPaymentReport($this->fromDate, $this->toDate, $this->orderNo, $this->billNo);
            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }

            $this->pager = new sfDoctrinePager('OrderRequest', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($userObj);
            $this->pager->setPage($this->page);
            $this->pager->init();
        } else {

            foreach ($this->splitform->getFormFieldSchema() as $name => $formField) {
                echo "<div id=error_list class=error_list >" . $formField->renderError() . "</div>";
            }
            echo $this->splitform->renderGlobalErrors();
            die;
        }
    }

    public function executeReportSearch(sfWebRequest $request) {
        $this->reportForm = new reportSearchForm();
    }

    public function executeReportStatement(sfWebRequest $request) {
        $this->reportForm = new reportSearchForm();
        $postDataArray = $request->getParameterHolder()->getAll();
        if (isset($postDataArray['report']))
        $postDataArray = $postDataArray['report'];

        if ($postDataArray['gateway'] != "" && $postDataArray['gateway'] != "BLANK") {
            $this->gateway = $postDataArray['gateway'];
        } else {
            $this->gateway = '';
        }
        if ($postDataArray['order_number'] != "" && $postDataArray['order_number'] != "BLANK") {
            $this->orderNo = $postDataArray['order_number'];
        } else {
            $this->orderNo = "";
        }
        if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
            $this->from_date = $postDataArray['from_date'];
            $dateArray = explode("-", $this->from_date);
            $this->fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
        } else {
            $this->from_date = '';
            $this->fromDate = "";
        }
        if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
            $this->to_date = $postDataArray['to_date'];
            $dateArray = explode("-", $this->to_date);
            $this->toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
        } else {
            $this->toDate = "";
            $this->to_date = "";
        }
        if ($postDataArray['status'] != "" && $postDataArray['status'] != "BLANK") {
            $this->status = $postDataArray['status'];
        } else {
            $this->status = '';
        }

        $reportObj = Doctrine::getTable('OrderAuthorize')->getReport($this->gateway, $this->orderNo, $this->fromDate, $this->toDate, $this->status);

        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['report']);
            $_SESSION['pfm']['report'] = array('to_date' => $this->toDate, 'from_date' => $this->fromDate, 'order_number' => $this->orderNo, 'status' => $this->status);
            $this->postDataArray = $_SESSION['pfm']['report'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['report'];
        }
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('OrderAuthorize', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($reportObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeFraudPrevention() {

    }

    public function executeFraudPreventionSearch(sfWebRequest $request) {
        $this->pfsrule = Doctrine::getTable('FpsRule')->getPfsRule();
        $postDataArray = $request->getParameterHolder()->getAll();

        if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
            $this->from_date = $postDataArray['from_date'];
            $dateArray = explode("-", $this->from_date);
            $this->fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
        } else {
            $this->from_date = '';
            $this->fromDate = "";
        }
        if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
            $this->to_date = $postDataArray['to_date'];
            $dateArray = explode("-", $this->to_date);
            $this->toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
        } else {
            $this->toDate = "";
            $this->to_date = "";
        }

        $this->pfSdetail = Doctrine::getTable('FpsDetail')->getFpsDeatil($this->fromDate, $this->toDate);
    }

    public function executeSetAuthorizeStatus(sfWebRequest $request) {
        $id = $request->getParameter('id');
        $getStatus = $request->getParameter('status');
        if ($getStatus == 'Capture')
        $staus = 0;

        $authorizeDetail = Doctrine::getTable('OrderAuthorize')->find($id);
        $authorizeDetail->set('authorize_status', $staus);
        $authorizeDetail->save();

        $this->getUser()->setFlash('notice', sprintf('Order captured successfully'));
        $this->forward($this->getModuleName(), 'fraudPreventionSearch');
    }

    public function executeSwGlobalPaymentReport(sfWebRequest $request) {

        $this->splitform = new splitForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {

            $this->splitform->bind($request->getParameter('split'));
            if ($this->splitform->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeSwGlobalPaymentSearch(sfWebRequest $request) {
        $this->splitform = new splitForm();
        $this->splitform->bind($request->getParameter('split'));
        $usePager = $request->getParameter('usePager');
        if ($this->splitform->isValid() || (isset($usePager) && 'true' == $usePager )) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['split']))
            $postDataArray = $postDataArray['split'];

            if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
                $fromDate = $postDataArray['from_date'];
                $dateArray = explode("-", $fromDate);
                $fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
            } else {
                $fromDate = "";
            }
            if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
                $toDate = $postDataArray['to_date'];
                $dateArray = explode("-", $toDate);
                $toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
            } else {
                $toDate = "";
            }

            $gatewayObj = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_active_paymentgateway'));
            $gateway_id = $gatewayObj->getFirst()->getId();

            //              $userId1 =$this->getUser()->getGuardUser()->getId();
            //              $merchantOBj = Doctrine::getTable('MerchantUser')->findByUserId($userId1)  ;
            //              $merchnat_id = $merchantOBj->getfirst()->getMerchantId();
            $transctionChargeObj = Doctrine::getTable('TransactionCharges')->findByGatewayId($gateway_id);
            $this->transactionCharge = $transctionChargeObj->getfirst()->getTransactionCharges();

            $this->fromDate = $postDataArray['from_date'];
            $this->toDate = $postDataArray['to_date'];


            $splitQry = Doctrine::getTable('OrderRequestDetails')->getPaymentReport($fromDate, $toDate);
            $this->totalPayment = 0;
            $this->totalAmount = 0;
            $this->totalFee = 0;
            $amount = 0;
            $this->count = 0;
            foreach ($splitQry as $val) {
                if ($val['merchant_code'] == sfConfig::get('app_swglobal_merchant_code'))
                $this->totalPayment = $val['sum'];
                else
                $amount+= $val['sum'];
                $this->totalTransaction = $splitQry[0]['totaltransaction'];
                $this->count++;
            }

            $this->totalAmount = $this->totalPayment + $amount;
            $this->totalFee = $this->totalAmount - $this->totalPayment;
        }
        else {
            $this->splitform->renderGlobalErrors();
            die;
        }
    }

    public function executeSwGlobalReportdetail(sfWebRequest $request) {

        $startdate = $request->getParameter('startdate');
        $enddate = $request->getParameter('enddate');
        if ($startdate != "" && $startdate != "BLANK") {
            $dateArray = explode("-", $startdate);
            $fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
        } else {
            $fromDate = "";
        }
        if ($enddate != "" && $enddate != "BLANK") {

            $dateArray = explode("-", $enddate);
            $toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
        } else {
            $toDate = "";
        }
        $this->fromDate = $startdate;
        $this->toDate = $enddate;
        $splitQry = Doctrine::getTable('OrderRequestDetails')->getPaymentDetailReport($fromDate, $toDate);

        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['swGlobalReport']);
            $_SESSION['pfm']['swGlobalReport'] = array('startdate' => $startdate, 'enddate' => $enddate);
            $this->postDataArray = $_SESSION['pfm']['swGlobalReport'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['swGlobalReport'];
        }

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }


        $this->pager = new sfDoctrinePager('OrderRequestSplit', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($splitQry);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeMerchantPaymentReport(sfWebRequest $request) {
        $this->splitform = new splitForm();
        $this->formValid = "";
        if ($request->isMethod('post')) {
            $this->splitform->bind($request->getParameter('split'));
            if ($this->splitform->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeMerchantPaymentSearch(sfWebRequest $request) {
        $this->splitform = new splitForm();
        $this->splitform->bind($request->getParameter('split'));
        $usePager = $request->getParameter('usePager');
        if ($this->splitform->isValid() || (isset($usePager) && 'true' == $usePager )) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['split']))
            $postDataArray = $postDataArray['split'];

            if ($postDataArray['from_date'] != "" && $postDataArray['from_date'] != "BLANK") {
                $fromDate = $postDataArray['from_date'];
                $dateArray = explode("-", $fromDate);
                $fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
            } else {
                $fromDate = "";
            }
            if ($postDataArray['to_date'] != "" && $postDataArray['to_date'] != "BLANK") {
                $toDate = $postDataArray['to_date'];
                $dateArray = explode("-", $toDate);
                $toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
            } else {
                $toDate = "";
            }

            $this->fromDate = $postDataArray['from_date'];
            $this->toDate = $postDataArray['to_date'];

            $splitQry = Doctrine::getTable('OrderRequestDetails')->getPaymentReport($fromDate, $toDate);
            $this->totalPayment = 0;
            $this->totalAmount = 0;
            $this->totalFee = 0;
            $amount = '';
            $this->count = 0;
            foreach ($splitQry as $val) {
                if ($val['merchant_code'] == sfConfig::get('app_swglobal_merchant_code'))
                $this->totalPayment = $val['sum'];
                else
                $amount+= $val['sum'];
                $this->totalTransaction = $splitQry[0]['totaltransaction'];
                $this->count++;
            }
            $this->totalPayment = $amount;
            $this->totalAmount = $amount;
            $this->totalFee = $this->totalAmount - $amount;
        }
        else {
            $this->splitform->renderGlobalErrors();
            die;
        }
    }

    public function executeMerchantReportdetail(sfWebRequest $request) {

        $startdate = $request->getParameter('startdate');
        $enddate = $request->getParameter('enddate');
        if ($startdate != "" && $startdate != "BLANK") {
            $dateArray = explode("-", $startdate);
            $fromDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 00:00:00"));
        } else {
            $fromDate = "";
        }
        if ($enddate != "" && $enddate != "BLANK") {

            $dateArray = explode("-", $enddate);
            $toDate = date('Y-m-d H:i:s', strtotime($dateArray[2] . "-" . $dateArray[1] . "-" . $dateArray[0] . " 23:59:59"));
        } else {
            $toDate = "";
        }
        $this->fromDate = $startdate;
        $this->toDate = $enddate;
        $splitQry = Doctrine::getTable('OrderRequestDetails')->getPaymentDetailReport($fromDate, $toDate);

        if ($request->isMethod('post')) {
            unset($_SESSION['pfm']['merchantReport']);
            $_SESSION['pfm']['merchantReport'] = array('startdate' => $startdate, 'enddate' => $enddate);
            $this->postDataArray = $_SESSION['pfm']['merchantReport'];
        } else {
            $this->postDataArray = $_SESSION['pfm']['merchantReport'];
        }

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('OrderRequestSplit', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($splitQry);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeMerchantReportCsv(sfWebRequest $request) {
        $this->postDataArray = $_SESSION['pfm']['merchantReport'];
        $from_date = "";
        $to_date = "";

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $start_date = $this->postDataArray['startdate'];
        if ($start_date != "") {
            $from_date = date('Y-m-d H:i:s', strtotime($start_date . " 00:00:00"));
        }

        $end_date = $this->postDataArray['enddate'];
        if ($end_date != "") {
            $to_date = date('Y-m-d H:i:s', strtotime($end_date . " 23:59:59"));
        }
        $merchantRecord = Doctrine::getTable('OrderRequestDetails')->getPaymentDetailReport($start_date, $to_date);
        $res = $merchantRecord->execute();
        $headerNames = array(0 => array('S. No.', 'Transaction No.', 'Description', 'Paid Date', "Total Amount($)", 'Payment($)', 'Fee($)'));
        $headers = array('transaction_number', 'description', 'paid_date', 'total_amount', 'payment', 'fee');
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $totalAmount = 0.00;
        $totalPayment = 0.00;
        $totalFee = 0.00;
        foreach ($res as $val) {
            $amount = 0;
            $payment = 0;
            for ($index = 0; $index < count($headers); $index++) {

                if ($headers[$index] == 'transaction_number') {
                    $recordsDetails[$i][$headers[$index]] = $val['OrderRequest']['transaction_number'];
                } elseif ($headers[$index] == 'total_amount') {
                    foreach ($val['OrderRequestSplit'] as $result) {
                        if ($result['merchant_code'] == sfConfig::get('app_swglobal_merchant_code'))
                        $payment = $result['item_fee'] / 100;
                        else
                        $amount+= $result['item_fee'] / 100;
                    }

                    $recordsDetails[$i][$headers[$index]] = format_amount($amount);
                }elseif ($headers[$index] == 'payment') {
                    $payment = $amount;
                    $recordsDetails[$i][$headers[$index]] = format_amount($payment);
                } elseif ($headers[$index] == 'fee') {
                    $fee = ($amount - $payment) / 100;
                    $recordsDetails[$i][$headers[$index]] = format_amount($fee);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $val[$headers[$index]];
                }
            }$i++;
            $totalAmount+=$amount;
            $totalPayment+=$amount;
            $totalFee+=$fee;
        }
        $grandAmount = format_amount($totalAmount);
        $grandPayment = format_amount($totalPayment);
        $grandFee = format_amount($totalFee);
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $arrList[] = array('Total', '', '', '', $grandAmount, $grandPayment, $grandFee);
            $file_array = $this->saveExportListFile($arrList, 'merchantPaymentReport/', "merchantPaymentReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "MERCHANT_REPORT";
        } else {
            $this->redirect_url('index.php/report/merchantPaymentReport');
        }$this->setTemplate('csv');
    }

    public function executeSwGlobalReportCsv(sfWebRequest $request) {
        $this->postDataArray = $_SESSION['pfm']['swGlobalReport'];
        $from_date = "";
        $to_date = "";

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $start_date = $this->postDataArray['startdate'];
        if ($start_date != "") {
            $from_date = date('Y-m-d H:i:s', strtotime($start_date . " 00:00:00"));
        }

        $end_date = $this->postDataArray['enddate'];
        if ($end_date != "") {
            $to_date = date('Y-m-d H:i:s', strtotime($end_date . " 23:59:59"));
        }
        $merchantRecord = Doctrine::getTable('OrderRequestDetails')->getPaymentDetailReport($start_date, $to_date);
        $res = $merchantRecord->execute();
        $headerNames = array(0 => array('S. No.', 'Transaction No.', 'Description', 'Paid Date', "Total Amount($)", 'Payment($)', 'Fee($)'));
        $headers = array('transaction_number', 'description', 'paid_date', 'total_amount', 'payment', 'fee');
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $totalAmount = 0.00;
        $totalPayment = 0.00;
        $totalFee = 0.00;
        foreach ($res as $val) {
            $amount = 0;
            $payment = 0;
            for ($index = 0; $index < count($headers); $index++) {

                if ($headers[$index] == 'transaction_number') {
                    $recordsDetails[$i][$headers[$index]] = $val['OrderRequest']['transaction_number'];
                } elseif ($headers[$index] == 'total_amount') {
                    foreach ($val['OrderRequestSplit'] as $result) {
                        if ($result['merchant_code'] == sfConfig::get('app_swglobal_merchant_code'))
                        $payment = $result['item_fee'] / 100;
                        else
                        $amount+= $result['item_fee'] / 100;
                    }
                    $amount+= $payment;
                    $recordsDetails[$i][$headers[$index]] = format_amount($amount);
                }elseif ($headers[$index] == 'payment') {

                    $recordsDetails[$i][$headers[$index]] = format_amount($payment);
                } elseif ($headers[$index] == 'fee') {
                    $fee = ($amount - $payment);
                    $recordsDetails[$i][$headers[$index]] = format_amount($fee);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $val[$headers[$index]];
                }
            }$i++;
            $totalAmount+=$amount;
            $totalPayment+=$payment;
            $totalFee+=$fee;
        }
        $grandAmount = format_amount($totalAmount);
        $grandPayment = format_amount($totalPayment);
        $grandFee = format_amount($totalFee);
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $arrList[] = array('Total', '', '', '', $grandAmount, $grandPayment, $grandFee);
            $file_array = $this->saveExportListFile($arrList, 'swGlobalPaymentReport/', "swGlobalPaymentReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "SWGLOBAL_REPORT";
        } else {
            $this->redirect_url('index.php/report/merchantPaymentReport');
        }$this->setTemplate('csv');
    }

    public function executeReportStatementCsv(sfWebRequest $request) {

        $this->postDataArray = $_SESSION['pfm']['report'];
        $from_date = "";
        $to_date = "";

        ini_set('memory_limit', '1024M');
        ini_set("max_execution_time", "64000");

        $fromDate = $this->postDataArray['from_date'];

        $toDate = $this->postDataArray['to_date'];
        $orderNo = $this->postDataArray['order_number'];
        $status = $this->postDataArray['status'];


        $reportObj = Doctrine::getTable('OrderAuthorize')->getReport($orderNo, $fromDate, $toDate, $status);
        $res = $reportObj->execute();

        $headerNames = array(0 => array('S. No.', 'Transaction No', 'Description', 'Paid Date', "Total Amount", 'Payment', 'Fee'));
        $headers = array('transaction_number', 'description', 'paid_date', 'total_amount', 'payment', 'fee');
        $i = 0;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $totalAmount = 0.00;
        $totalPayment = 0.00;
        $totalFee = 0.00;
        foreach ($res as $val) {
            $amount = 0;
            $payment = 0;
            for ($index = 0; $index < count($headers); $index++) {

                if ($headers[$index] == 'transaction_number') {
                    $recordsDetails[$i][$headers[$index]] = $val['OrderRequest']['transaction_number'];
                } elseif ($headers[$index] == 'total_amount') {
                    foreach ($val['OrderRequestSplit'] as $result) {
                        if ($result['merchant_code'] == sfConfig::get('app_swglobal_merchant_code'))
                        $payment = $result['item_fee'] / 100;
                        else
                        $amount+= $result['item_fee'] / 100;
                    }
                    $amount+= $payment;
                    $recordsDetails[$i][$headers[$index]] = format_amount($amount);
                }elseif ($headers[$index] == 'payment') {

                    $recordsDetails[$i][$headers[$index]] = format_amount($payment);
                } elseif ($headers[$index] == 'fee') {
                    $fee = ($amount - $payment);
                    $recordsDetails[$i][$headers[$index]] = format_amount($fee);
                } else {
                    $recordsDetails[$i][$headers[$index]] = $val[$headers[$index]];
                }
            }$i++;
            $totalAmount+=$amount;
            $totalPayment+=$payment;
            $totalFee+=$fee;
        }
        $grandAmount = format_amount($totalAmount);
        $grandPayment = format_amount($totalPayment);
        $grandFee = format_amount($totalFee);
        if (count($recordsDetails) > 0) {
            $arrList = $this->getSimplifiedXLSExportList($headerNames, $headers, $recordsDetails);
            $arrList[] = array('Total', '', '', '', $grandAmount, $grandPayment, $grandFee);
            $file_array = $this->saveExportListFile($arrList, 'swGlobalPaymentReport/', "swGlobalPaymentReport");
            $file_array = explode('#$', $file_array);
            $this->fileName = $file_array[0];
            $this->filePath = $file_array[1];
            $this->folder = "SWGLOBAL_REPORT";
        } else {
            $this->redirect_url('index.php/report/merchantPaymentReport');
        }$this->setTemplate('csv');
    }

    public function getSimplifiedXLSExportList($arrHeader, $headers, $arrList) {
        $arrTotalList = array();
        $counterVal = count($arrHeader);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$i] = $arrHeader[$i];
        }

        $total = count($arrList) + count($arrTotalList);
        $length = count($arrTotalList);
        $j = $length;
        $counterVal = count($arrList);
        $counterValHeader = count($headers);
        for ($k = 0; $k < $counterVal; $k++) {
            $arrExPortList[$k][] = $k + 1;
            for ($index = 0; $index < $counterValHeader; $index++) {
                $arrExPortList[$k][] = $arrList[$k][$headers[$index]];
            }
        }
        $counterVal = count($arrExPortList);
        for ($i = 0; $i < $counterVal; $i++) {
            $arrTotalList[$j + $i] = $arrExPortList[$i];
        }

        return $arrTotalList;
    }

    public function saveExportListFile($arrList, $filePath, $fileName) {

        try {
            $upload_path = sfConfig::get('sf_upload_dir') . '/report/';
            $downold_Path = $upload_path . $filePath;
            $strFile = $fileName . "_" . date("Ymdhis");
            $filepath = $downold_Path . $strFile . ".csv";
            // $final_path = sfConfig::get('sf_upload_dir').$filePath;
            $final_path = $upload_path . $filePath;
            //               chmod('/var/www/p4meInt/web/uploads/report/merchantPaymentReport', 0777);
            if (!is_dir($upload_path)) {
                mkdir($upload_path);
                chmod($upload_path, 0777);
            }
            if (!is_dir($final_path)) {
                mkdir($final_path);
                chmod($final_path, 0777);
            }
            $strFileName = $this->makeCsvFile($arrList, $filepath, ",", "\"");
            $arrFileInfo = pathinfo($strFileName);
            $path = "../../uploads/report/" . $filePath . $arrFileInfo['basename'];
            return $strFile . "#$" . $path;
            //                $this->redirect_url($path);
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

/*     * *****************common functions for save the reports data********************** */

    public function makeCsvFile($dataArray, $filename, $delimiter=",", $enclosure="\"") {
        // Build the string

        $fh = fopen($filename, "a");
        // for each array element, which represents a line in the csv file...
        foreach ($dataArray as $line) {
            $string = "";
            // No leading delimiter
            $writeDelimiter = FALSE;

            foreach ($line as $dataElement) {
                // Replaces a double quote with two double quotes
                $dataElement = str_replace("\"", "\"\"", $dataElement);
                $dataElement = str_replace("\n", "", $dataElement);
                $dataElement = str_replace("\r", "", $dataElement);

                // Adds a delimiter before each field (except the first)
                if ($writeDelimiter)
                $string .= $delimiter;

                // Encloses each field with $enclosure and adds it to the string
                $string .= $enclosure . $dataElement . $enclosure;

                // Delimiters are used every time except the first.
                $writeDelimiter = TRUE;
            }
            // Append new line
            $string .= "\n";

            fwrite($fh, $string);
        } // end foreach($dataArray as $line)
        fclose($fh);
        return $filename;
    }

/*     * *****************end the common function********************** */

    public function executeDownloadCsv(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName') . '.csv';
        $folder = $request->getParameter('folder');
        #sfLoader::loadHelpers('Asset');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
        //        print $$folder;
        $folder_path = self::$$folder;
        $filePath = addslashes(sfConfig::get('sf_web_dir') . '/uploads' . $folder_path . '/' . $fileName);
        $this->setLayout(false);
        sfConfig::set('sf_web_debug', false);

        // Adding the file to the Response object
        // $this->getResponse()->setHttpHeader("Content-Length: ",filesize($filePath),TRUE);
        // $this->getResponse()->setHttpHeader('Content-type','application/csv',TRUE);
        $this->getResponse()->setHttpHeader('Content-type', 'application/force-download');
        $this->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileName}", TRUE);
        $this->getResponse()->sendHttpHeaders();
        $this->getResponse()->setContent(file_get_contents($filePath));
        return sfView::NONE;
    }

    public function executeGatewayRequestReport(sfWebRequest $request) {

        $this->gatewayRequestReportForm = new GatewayRequestReportForm();

        $this->formValid = "";
        if ($request->isMethod('post')) {


            $this->gatewayRequestReportForm->bind($request->getParameter('gatewayRequestReport'));
            if ($this->gatewayRequestReportForm->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeGatewayRequestSearch(sfWebRequest $request) {

        $this->gatewayRequestReportForm = new GatewayRequestReportForm();
        $this->gatewayRequestReportForm->bind($request->getParameter('gatewayRequestReport'));

        $from_date = "";
        $to_date = "";


        $start_date = $request->getParameter('startdate');
        $end_date = $request->getParameter('enddate');
        if ($start_date != "" && $start_date != "BLANK") {
            $dateArray = explode("-", $start_date);
            $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));
        } else {
            $from_date = "";
        }
        if ($end_date != "" && $end_date != "BLANK") {

            $dateArray = explode("-", $end_date);
            $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));
        } else {
            $to_date = "";
        }

        if ($this->gatewayRequestReportForm->isValid()) {
            $postDataArray = $request->getParameterHolder()->getAll();
            if (isset($postDataArray['gatewayRequestReport']))
            $postDataArray = $postDataArray['gatewayRequestReport'];
            if ($postDataArray['from_date']) {
                $start_date = $postDataArray['from_date'];
                if ($start_date != "") {
                    $dateArray = explode("-", $start_date);
                    $from_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 00:00:00"));
                }
            }
            if ($postDataArray['to_date']) {
                $end_date = $postDataArray['to_date'];
                if ($end_date != "") {
                    $dateArray = explode("-", $end_date);
                    $to_date = date('Y-m-d H:i:s', strtotime($dateArray[0] . "-" . $dateArray[1] . "-" . $dateArray[2] . " 23:59:59"));
                }
            }
        }

        $requestQry = Doctrine::getTable('EpPayEasyRequest')->getRequestDetails($from_date, $to_date);



        $this->from_date = $start_date;
        $this->to_date = $end_date;

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('EpPayEasyRequestTable', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($requestQry);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeGatewayResponseRequest(sfWebRequest $request) {

        $this->form = new ActionGatewayResponseForm();
        $this->formValid = "";
        $this->gatewayArray = array();
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('report'));
            if ($this->form->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeDisplayGatewayResponseRequest(sfWebRequest $request) {


        $this->form = new ActionGatewayResponseForm();

        $report = $request->getParameter('report');
        $this->gateway = $report['gateway'];
        if($this->gateway == ''){
            $this->gateway = $request->getParameter('gateway');
        }
        $this->startDate = $request->getParameter('startDate');
        $this->endDate = $request->getParameter('endDate');

        $responseGateway = array();
        $responseObj = Doctrine::getTable('GatewayResponseDetails')->findByGatewayId($this->gateway);

        foreach ($responseObj as $res) {
            $responseGateway[$res->getResponseCode()] = $res->getResponseText();
        }
        $this->responseArr = $responseGateway;



        if (!isset($this->endDate) || $this->endDate == '') {
            $this->endDate = $this->startDate;
        }



        $dataResult = Doctrine::getTable('Ipay4meOrder')->getAllGatewayPaymentOrderDetails($this->gateway, $this->startDate, $this->endDate);
        //this below mentioned code is for executing the sqlrecd & get the data in paging order
        $this->page = 1;

        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('ipay4meOrder', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($dataResult);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
    }

    public function executeDetailResponseCount(sfWebRequest $request) {

        $this->gateway = Settings::decryptInput($this->getRequestParameter('gateway'));
        $this->startDate = Settings::decryptInput($this->getRequestParameter('startDate'));
        $this->endDate = Settings::decryptInput($this->getRequestParameter('endDate'));
        $this->responseCode = Settings::decryptInput($this->getRequestParameter('responseCode'));
        $this->gatewayName = Doctrine::getTable('Gateway')->getGatewayMerchantDetails($this->gateway);
        //convert the date into the proper format
        $dataResult = Doctrine::getTable('Ipay4meOrder')->getAllGatewayPaymentHistory($this->gateway, $this->startDate, $this->endDate, $this->responseCode);
        //this below mentioned code is for executing the sqlrecd & get the data in paging order
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('ipay4meOrder', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($dataResult);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
    }

    public function executePaymentTransactionsReport(sfWebRequest $request) {

        $this->paymentTransactionReportform = new paymentTransactionsReportForm();

        $this->formValid = "";

        if ($request->isMethod('post')) {
            $this->paymentTransactionReportform->bind($request->getParameter('paymentTransaction'));
            if ($this->paymentTransactionReportform->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeTransactionReportGraph(sfWebRequest $request) {

        $this->paymentTransactionReportform = new paymentTransactionsReportForm();
        $this->paymentTransactionReportform->bind($request->getParameter('paymentTransaction'));

        if ($this->paymentTransactionReportform->isValid()) {

            if ($request->getMethod() == sfRequest::POST) {
                $post = $request->getPostParameters();

                $chartData = array();

                $result = ReportManager::getPaymentTransactionsGraphQuery($post)->execute();

                if ($result->count()) {
                    $this->showGraph = 2;
                    $this->post = $post;
                    $this->file = ReportManager::setPaymentTransactionsGraph($result, $post);
                } else {
                    $this->showGraph = 1;
                }
            } else {
                $this->showGraph = 0;
            }
        }
    }

    public function executeNisDollarReport(sfWebRequest $request) {

        $userId = $this->getUser()->getGuarduser()->getId();
        $arrUserPreviousRecord = Doctrine::getTable('nisDollarRevenue')->getPreviousRecord($userId);
        if (!empty($arrUserPreviousRecord) && count($arrUserPreviousRecord) > 0) {
            $this->accountId = $arrUserPreviousRecord[0]['account_id'];
            $yearMonthId = explode('-', $arrUserPreviousRecord[0]['date']);
            $this->yearId = $yearMonthId[0];
            $this->monthId = abs($yearMonthId[1]);
            $accountDetail = Doctrine::getTable('TblBankDetails')->FindById($this->accountId)->toArray();
            $this->projectId = $accountDetail[0]['project_id'];
        }

        $projectName = Doctrine::getTable('tblProjectName')->findAll();
        $arrProject = array();
        foreach ($projectName as $key => $value) {
            $arrProject[$value['id']] = $value['name'];
        }
        $this->project = $arrProject;
    }

    public function executeNisDollarReportList(sfWebRequest $request) {
        $this->setLayout(NULL);

        $project_id = $request->getParameter('project_id');
        $account_id = $request->getParameter('account_id');
        $month = $request->getParameter('sel_month');
        $year = $request->getParameter('sel_year');
        $arrRevDetail = array();
        if (($year > '2010') || ($month >= 12 && $year >= '2010')) {
            $month = ($month < 10) ? '0' . $month : $month;

            $startDate = $year . '-' . $month . '-01';
            $currDate = date('Y-m-d');
            $lastDayOfMonth = date('Y-m-d', mktime(0, 0, 0, (date($month) + 1), 0, date($year)));

            $endDate = ($currDate < $lastDayOfMonth) ? $currDate : $lastDayOfMonth;

            $nisArr = Doctrine::getTable('nisDollarRevenue')->getDollarDetails($month, $year, $account_id);
            $this->accountDetail = Doctrine::getTable('TblBankDetails')->FindById($account_id)->toArray();

            $i = 1;
            $j = 0;
            $k = 1;
            for ($i = $startDate; $i <= $endDate; $i++) {

                if (isset($nisArr[$j]) && $nisArr[$j]['date'] == date("Y-m-d", mktime(0, 0, 0, $month, $k, $year))) {
                    $arrRevDetail[$k]['id'] = $nisArr[$j]['id'];
                    $arrRevDetail[$k]['amount'] = $nisArr[$j]['amount'];
                    $arrRevDetail[$k]['date'] = $nisArr[$j]['date'];
                    $arrRevDetail[$k]['remark'] = $nisArr[$j]['remark'];
                    $j++;
                } else {
                    $arrRevDetail[$k]['id'] = '';
                    $arrRevDetail[$k]['amount'] = '0.00';
                    $arrRevDetail[$k]['date'] = date("Y-m-d", mktime(0, 0, 0, $month, $k, $year));
                    $arrRevDetail[$k]['remark'] = '';
                }

                $arrEditRequest = TblRevenueEditRequestTable::getInstance()->getPreviousRequest($arrRevDetail[$k]['date'], $account_id, $project_id);
                if (!empty($arrEditRequest) && $arrEditRequest[0]['status'] == 'Approved') {
                    $arrRevDetail[$k]['action'] = $arrEditRequest[0]['status'];
                } else if (!empty($arrEditRequest) && $arrEditRequest[0]['status'] == 'Declined') {
                    $arrRevDetail[$k]['action'] = $arrEditRequest[0]['status'];
                } else {
                    $arrRevDetail[$k]['action'] = '';
                }

                $k++;
            }
        } else {
            $this->accountDetail = Doctrine::getTable('TblBankDetails')->FindById($account_id)->toArray();
        }

        $this->arrRevDetail = $arrRevDetail;

        $this->isFound = true;
    }

    public function executeNisDollarUpdate(sfWebRequest $request) {
        $postDataArray = $request->getParameterHolder()->getAll();

        $amount = $request->getParameter('amount_update');
        $remark = $request->getParameter('remark_update');
        $date = $request->getParameter('date_update');
        $hdn_account = $request->getParameter('hdn_account');

        $nisDollarUpdate = new NisDollarRevenue();

        $nisDollarUpdate->setAmount($amount);
        $nisDollarUpdate->setRemark($remark);
        $nisDollarUpdate->setDate($date);
        $nisDollarUpdate->setAccountId($hdn_account);
        $nisDollarUpdate->save();

        $this->getUser()->setFlash('notice', sprintf('Updated'));

        $this->setTemplate('nisDollarReport');
    }

    public function executeNisDollarRevenue(sfWebrequest $request) {
        $projectName = Doctrine::getTable('tblProjectName')->findAll();
        $arrProject = array();
        foreach ($projectName as $key => $value) {
            $arrProject[$value['id']] = $value['name'];
        }

        $this->project = $arrProject;
        $month = $request->getParameter("month");
        $this->project_id = $request->getParameter('project_id');
        $this->account_id = $request->getParameter('account_id');
        $year = $request->getParameter("year");
        $this->year = $year;
        $this->month = $month;
        $this->isfound = false;

        if (isset($month) && $month != '' && isset($year) && $year != '') {
            $month = str_pad($month, 2, "0", STR_PAD_LEFT);
            $startdate = $year . "-" . $month . "-01";
            $enddate = $year . "-" . $month . "-31";

            $currDate = date('Y-m-d');
            $lastDayOfMonth = date('Y-m-d', mktime(0, 0, 0, (date($month) + 1), 0, date($year)));

            $lastDate = ($currDate < $lastDayOfMonth) ? $currDate : $lastDayOfMonth;

            if ($this->account_id == 0) {
                //get All Account
                $arrAllAccount = Doctrine::getTable('TblBankDetails')->getAllAccount($this->project_id);

                for ($i = 0; $i < count($arrAllAccount); $i++) {
                    $arrAccountId[$i] = $arrAllAccount[$i]['id'];
                }

                $this->accountDetail = $arrAllAccount;
                $dataRevenue = Doctrine::getTable("NisDollarRevenue")->getRevenueData($startdate, $enddate, $arrAccountId);
            } else {
                $this->accountDetail = Doctrine::getTable('TblBankDetails')->FindById($this->account_id)->toArray();
                $dataRevenue = Doctrine::getTable("NisDollarRevenue")->getRevenueData($startdate, $enddate, $this->account_id);
            }

            if (isset($dataRevenue) && is_array($dataRevenue) && count($dataRevenue) > 0) {


                $pojectname = Doctrine::getTable('TblProjectName')->Find($this->project_id)->getName();
                $this->isfound = true;

                $this->dataRevenue = $dataRevenue;
                //Data for Excel and PDF
                $accountDetail = $this->accountDetail;
                //Excel
                if ($this->account_id == 0) {
                    $this->filename = $pojectname . " General Dollar Revenue Report - " . date("F d, Y", strtotime($startdate)) . " - " . date("F d, Y", strtotime($lastDate)) . ".xls";
                } else {
                    $this->filename = $pojectname . " Dollar Revenue Report - " . $accountDetail[0]['bank_name'] . ", " . $accountDetail[0]['city'] . ' ' . $accountDetail[0]['state'] .
                ' ' . $accountDetail[0]['country'] . " - " . date("F d, Y", strtotime($startdate)) . " - " . date("F d, Y", strtotime($lastDate)) . ".xls";
                }

                $excel = new ExcelWriter("uploads/excel/" . $this->filename);
                if ($excel == false)
                echo $excel->error;
                $month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                $title = "<b>Revenue Report for the Month of " . $month[$request->getParameter("month") - 1] . " " . $request->getParameter("year") . "</b>";
                $myArr = array($title);
                $excel->writeLine($myArr);
                foreach ($this->accountDetail as $valueBank) {

                    foreach ($dataRevenue as $value) {
                        if ($valueBank['id'] == $value['account_id']) {
                            $blank = array();
                            $excel->writeLine($blank);
                            $bankName = array("Bank Name: ", $valueBank['bank_name']);
                            $excel->writeLine($bankName);
                            $accountName = array("Account Name: ", $valueBank['account_name']);
                            $excel->writeLine($accountName);
                            $accountNumber = array("Account Number: ", $valueBank['account_number']);
                            $excel->writeLine($accountNumber);
                            $bankLocation = array("Bank Location: ", $valueBank['city'] . ', ' . $valueBank['state'] . ', ' . $valueBank['country']);
                            $excel->writeLine($bankLocation);
                            $excel->writeLine($blank);
                            $details = array("<b>Details:</b> ");
                            $excel->writeLine($details);

                            $dataArray = array();
                            $myArr = array("<b>S.No.</b>", "<b>Date</b>", "<b>Amount</b>", "<b>Commulative Amount</b>", "<b>Remark</b>");
                            $dataArray[] = $myArr;
                            $i = 1;
                            $commAmount = 0;

                            $myArr = array();
                            $dateArr = explode(",", $value['total_date']);
                            $amountArr = explode(",", $value['total_amount']);
                            $remarkArr = explode(",", $value['total_remark']);
                            for ($j = 0; $j < count($dateArr); $j++) {
                                $myArr = array();
                                $myArr[] = $i;
                                $myArr[] = date_format(date_create($dateArr[$j]), "F d,Y");
                                $myArr[] = "$" . $amountArr[$j];
                                $myArr[] = "";
                                if ($remarkArr[$j] != "0") {
                                    $myArr[] = $remarkArr[$j];
                                } else {
                                    $myArr[] = '';
                                }
                                $i++;
                                $dataArray[] = $myArr;
                            }
                            $commAmount = number_format($commAmount + $value['sum'], 2);
                            $test = array("", "<b>Total</b>", "<b>$" . $value['sum'] . "</b>", "<b>$" . $commAmount . "</b>");
                            $dataArray[] = $test;

                            foreach ($dataArray as $excelArr) {
                                $excel->writeLine($excelArr);
                            }
                        }
                    }
                }

                $excel->close();
            }
        }
    }

    public function executeNisRevDetail(sfWebRequest $request) {
        $this->setLayout(false);
        $this->revDate = $request->getParameter('date');
        $this->accountId = $request->getParameter('accountId');
        $this->mode = $request->getParameter('mode');

        $editRequest = Doctrine::getTable('TblRevenueEditRequest')->getAccountRecord($this->accountId, $this->revDate);
        $this->showComments = false;
        $this->status = true;
        if (count($editRequest)) {
            $this->showComments = true;
            $this->comments = $editRequest[0]['comments'];
            $this->adminComments = $editRequest[0]['admin_comments'];
            $this->status = ($editRequest[0]['status'] == 'Declined') ? false : true;
        }
        $arrRevDetail = Doctrine::getTable('NisDollarRevenue')->getRevDetail($this->revDate, $this->accountId);
        if (!empty($arrRevDetail)) {
            $request->setAttribute('revDate', $this->revDate);
            $request->setAttribute('revAmount', $arrRevDetail[0]['amount']);
            $request->setAttribute('remark', $arrRevDetail[0]['remark']);
            $this->remark = $arrRevDetail[0]['remark'];
        } else {
            $request->setAttribute('revDate', $this->revDate);
            $request->setAttribute('revAmount', 0);
            $this->remark = '';
        }



        $this->form = new NisRevenueForm();
    }

    public function executeUpdateRevDetail(sfWebRequest $request) {
        $this->setLayout(false);
        $this->setTemplate(NULL);
        $postArray = $request->getPostParameters();
        $date = $postArray['yearVal'] . '-' . $postArray['monthVal'] . '-' . $postArray['dayVal'];
        $amount = $postArray['amount'];
        $hdnId = $postArray['hdn_id'];
        $hdnAccountId = $postArray['hdn_account'];
        if (isset($postArray['chkId']) && !empty($postArray['chkId'])) {
            if ($postArray['chkId'] == 'H') {
                $remark = 'Holiday';
            }
        } else {
            $remark = '';
        }
        if ($hdnId != '') {
            $arrRevDetail = Doctrine::getTable('NisDollarRevenue')->getRevDetail($hdnId, $hdnAccountId);
            if (!empty($arrRevDetail)) {
                $nisDollarUpdate = Doctrine::getTable('NisDollarRevenue')->updateDetail($date, $amount, $remark, $arrRevDetail[0]['id'], $hdnAccountId);
            } else {
                $nisDollarUpdate = new NisDollarRevenue();
                $nisDollarUpdate->setAmount($amount);
                $nisDollarUpdate->setRemark($remark);
                $nisDollarUpdate->setDate($date);
                $nisDollarUpdate->setAccountId($hdnAccountId);
                $nisDollarUpdate->save();
            }
            //return $nisDollarUpdate;
        }
        $str = 'Amount has been updated successfully';
        return $this->renderText($str);
    }

    public function executeProjectAccount(sfWebRequest $request) {
        $projectId = $request->getParameter('project_id');
        $accountId = $request->getParameter('account_id');
        $mode = $request->getParameter('mode');
        $arrAccountNumber = Doctrine::getTable('TblBankDetails')->findByProjectId($projectId)->toArray();

        $str = '<option value="">-- Please Select --</option>';
        if (count($arrAccountNumber) > 0) {
            if (!empty($projectId) && !empty($mode)) {
                if ($accountId == 0) {
                    $str .= '<option value="0" selected="' . $accountId . '">All</option>';
                } else {
                    $str .= '<option value="0">All</option>';
                }
            }
            foreach ($arrAccountNumber as $key => $value) {
                if (!empty($accountId) && ($value['id'] == $accountId )) {
                    $str .= '<option value="' . $value['id'] . '" selected="' . $accountId . '">' . $value['account_name'] . '</option>';
                } else {
                    $str .= '<option value="' . $value['id'] . '" >' . $value['account_name'] . '</option>';
                }
            }
        }
        return $this->renderText($str);
    }

    public function executeAddBankAccount(sfWebRequest $request) {
        $this->setLayout(false);
        $this->projectId = $request->getParameter('project_id');
        $this->account_id = $request->getParameter('account_id');
        $this->mode = $request->getParameter('mode');
        if (!empty($this->account_id)) {

            $accountDetail = Doctrine::getTable('TblBankDetails')->FindById($this->account_id)->toArray();
            $request->setAttribute('account_name', $accountDetail['0']['account_name']);
            $request->setAttribute('account_number', $accountDetail['0']['account_number']);
            $request->setAttribute('bank_name', $accountDetail['0']['bank_name']);
            $request->setAttribute('city', $accountDetail['0']['city']);
            $request->setAttribute('state', $accountDetail['0']['state']);
            $request->setAttribute('country', $accountDetail['0']['country']);
        } else {
            $this->account_id = '';
        }
        $this->form = new NewBankAccountForm();
    }

    public function executeSavebankAccountDeatil(sfWebRequest $request) {
        $this->setLayout(false);
        $this->setTemplate(NULL);
        $postArray = $request->getPostParameters();
        $accountName = $postArray['account_name'];
        $accountNumber = $postArray['account_num'];
        $bankName = $postArray['bank_name'];
        $city = $postArray['city'];
        $state = $postArray['state'];
        $country = $postArray['country'];
        $hdnId = $postArray['hdn_id'];
        $hdnAccountId = $postArray['hdn_account'];

        if (!empty($hdnAccountId)) {
            $nisDollarUpdate = TblBankDetailsTable::getInstance('TblBankDetails')->updateBAnkRecord($accountNumber, $bankName, $city, $state, $country, $hdnId, $hdnAccountId, $accountName);
        } else {
            $nisDollarUpdate = new TblBankDetails();
            $nisDollarUpdate->setAccountName($accountName);
            $nisDollarUpdate->setAccountNumber($accountNumber);
            $nisDollarUpdate->setBankName($bankName);
            $nisDollarUpdate->setCity($city);
            $nisDollarUpdate->setState($state);
            $nisDollarUpdate->setCountry($country);
            $nisDollarUpdate->setProjectId($hdnId);
            $nisDollarUpdate->save();
        }
        $str = 'Account has been added successfully';
        return $this->renderText($str);
    }

    public function executeGetMonth(sfWebRequest $request) {
        $yearId = $request->getParameter('sel_year');
        $hdnMonth = $request->getParameter('hdn_month');
        $arrMonth = array('1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April', '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        $str = '<option value="">-- Please Select --</option>';
        if ($yearId != '') {
            if ($yearId == '2010') {
                for ($i = 11; $i <= count($arrMonth); $i++) {
                    if (!empty($hdnMonth) && ($i == $hdnMonth)) {
                        $str .= '<option value="' . $i . '" selected="' . $hdnMonth . '">' . $arrMonth[$i] . '</option>';
                    } else {
                        $str .= '<option value="' . $i . '" >' . $arrMonth[$i] . '</option>';
                    }
                }
            } else if ($yearId == date('Y')) {
                $currentDate = date('Y-m-d');
                $explode = explode('-', $currentDate);
                $currentMonth = $explode[1];
                for ($i = 1; $i <= $currentMonth; $i++) {
                    if (!empty($hdnMonth) && ($i == $hdnMonth)) {
                        $str .= '<option value="' . $i . '" selected="' . $hdnMonth . '">' . $arrMonth[$i] . '</option>';
                    } else {
                        $str .= '<option value="' . $i . '" >' . $arrMonth[$i] . '</option>';
                    }
                }
            } else if ($yearId <= date('Y')) {
                for ($i = 1; $i <= count($arrMonth); $i++) {
                    if (!empty($hdnMonth) && ($i == $hdnMonth)) {
                        $str .= '<option value="' . $i . '" selected="' . $hdnMonth . '">' . $arrMonth[$i] . '</option>';
                    } else {
                        $str .= '<option value="' . $i . '" >' . $arrMonth[$i] . '</option>';
                    }
                }
            }
        }

        return $this->renderText($str);
    }

    public function executeNisRequestEdit(sfWebRequest $request) {
        $this->setLayout(false);
        $this->revDate = $request->getParameter('date');
        $this->accountId = $request->getParameter('accountId');
        $this->projectId = $request->getParameter('projectId');
        $arrRevDetail = Doctrine::getTable('NisDollarRevenue')->getRevDetail($this->revDate, $this->accountId);
        if (!empty($arrRevDetail)) {
            $this->revDate = $arrRevDetail[0]['date'];
            $this->revAmount = $arrRevDetail[0]['amount'];
            $this->remark = $arrRevDetail[0]['remark'];
        } else {
            $this->revDate = $this->revDate;
            $this->revAmount = 0;
            $this->remark = '';
        }
    }

    public function executeSaveEditRequest(sfWebRequest $request) {
        $this->setLayout(false);
        $this->setTemplate(NULL);
        $postArray = $request->getPostParameters();
        $amount = $postArray['amount'];
        $remark = $postArray['remark'];
        $comments = $postArray['comments'];
        $hdnDate = $postArray['hdn_date'];
        $hdnAccountId = $postArray['hdn_account'];
        $hdnProjectId = $postArray['project'];
        //find previous record
        $arrEditRequest = TblRevenueEditRequestTable::getInstance()->getPreviousRequest($hdnDate, $hdnAccountId, $hdnProjectId);
        if (!empty($arrEditRequest)) {
            $nisDollarUpdate = TblRevenueEditRequestTable::getInstance()->updateEditRequest($amount, $hdnDate, $hdnAccountId, $comments, $hdnProjectId, $arrEditRequest[0]['id']);
        } else {
            $nisDollarUpdate = new TblRevenueEditRequest();
            $nisDollarUpdate->setCurrentAmount($amount);
            $nisDollarUpdate->setDate($hdnDate);
            $nisDollarUpdate->setAccountId($hdnAccountId);
            $nisDollarUpdate->setComments($comments);
            $nisDollarUpdate->setProjectId($hdnProjectId);
            $nisDollarUpdate->save();
        }
        //return $nisDollarUpdate;
        $str = 'Request for edit has been sent successfully';
        return $this->renderText($str);
    }

    public function executeRevenueEditRequest(sfWebRequest $request) {
        $this->page = 1;

        $revenueEditRequestObj = Doctrine::getTable('TblRevenueEditRequest')->getRecord();

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }



        $this->pager = new sfDoctrinePager('RevenueEditRequest', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($revenueEditRequestObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeRevenueEdit(sfWebRequest $request) {
        $this->setLayout(false);
        $this->id = $request->getParameter('id');

        $this->accountName = $request->getParameter('accountId');
        $this->projectName = $request->getParameter('projectId');
        $arrRevDetail = Doctrine::getTable('TblRevenueEditRequest')->getEditRecord($this->id);

        $this->remark = $arrRevDetail[0]['comments'];
        $this->adminComments = $arrRevDetail[0]['admin_comments'];
        $this->date = $arrRevDetail[0]['date'];
        $this->account_id = $arrRevDetail[0]['account_id'];
        $this->project_id = $arrRevDetail[0]['project_id'];
    }

    public function executeSaveAdminEditRequest(sfWebRequest $request) {
        $this->setLayout(false);
        $this->setTemplate(NULL);
        $postArray = $request->getPostParameters();
        $hdn_id = $postArray['hdn_id'];
        if ($postArray['mode'] == 'approve') {
            $mode = 'Approved';
        } else if ($postArray['mode'] == 'decline') {
            $mode = 'Declined';
        }
        $comments = $postArray['comments'];
        $nisDollarUpdate = TblRevenueEditRequestTable::getInstance()->updateAdminComments($mode, $comments, $hdn_id);
    }

/**
* [WP: 082] => CR: 120
* @param <type> $request
* This function show dashboard for transaction reports...
* Required date range...
*/
    public function executeTransactionReport(sfWebRequest $request){

    }



/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * This function return graph report by gateway and amount...
 * Chart Type will decide about the Bar chart or Pie chart...
 */
    public function executeTransactionReportByGatewayAndAmount(sfWebRequest $request){

        $startDate = $request->getParameter('fdate');
        $endDate = $request->getParameter('tdate');
        $chartType = $request->getParameter('chartType');
        $currency = $request->getParameter('currency');
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $url = 'report/transactionReportByGatewayAndAmountData?start_date='.$startDate.'&end_date='.$endDate.'&chart_type='.$chartType.'&currency='.$currency;
        stOfc::createChart( 445, 200, $url, false );
        exit;
    }

/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * This function will call by TransactionReportByGatewayAndAmount function...
 * Chart Type will decide about the Bar chart or Pie chart...
 */
    public function executeTransactionReportByGatewayAndAmountData(sfWebRequest $request){
        //    $startDate = '2012-06-20';
        //    $endDate = '2012-06-27';
        //    $chartType = 'bar';
        //    $currency = 'dollar';

        $startDate = $request->getParameter('start_date');
        $endDate = $request->getParameter('end_date');
        $chartType = $request->getParameter('chart_type');
        $currency = $request->getParameter('currency');

        $successArray = array();
        $query = Doctrine::getTable('Ipay4meOrder')->getTransactionStatusGraphReport($startDate, $endDate, $paymentStatus='0');
        $queryArray = $query->toArray();
        if(!empty($queryArray)){
            foreach($queryArray as $details){
                //if($details['payment_status'] == 0){
                $successArray[] = $details['gateway_id'];
                //}
            }
            $gatewaySuccessArray = array_count_values($successArray);

            ksort($gatewaySuccessArray);

            $amountArray = array();
            foreach($gatewaySuccessArray as $key=>$value){
                $amountCount = 0;
                $orderCount = count($queryArray);
                for($j=0;$j<$orderCount;$j++){
                    if($queryArray[$j]['gateway_id'] == $key){
                        $orderReqObj  = Doctrine::getTable('OrderRequestDetails')->find($queryArray[$j]['order_request_detail_id']);
                        if($orderReqObj->getCurrency() == $currency){
                            if($currency == 'dollar' && $orderReqObj->getCurrency() == 'dollar'){
                                $amount = $orderReqObj->getAmount();
                                $amountCount += $amount;
                                $amountArray[$key] = $amountCount;
                            }else if($currency == 'pound' && $orderReqObj->getCurrency() == 'pound'){
                                $amount = $orderReqObj->getConvertAmount();
                                $amountCount += $amount;
                                $amountArray[$key] = $amountCount;
                            }

                        }
                    }
                }

            }
            if(count($amountArray) >0){

                $finalAmountArray = array();
                if(isset($amountArray[1]) || isset($amountArray[8])){
                    $finalAmountArray['graypay'] = (isset($amountArray[1])?$amountArray[1]:0)+(isset($amountArray[8])?$amountArray[8]:0);
                }else{
                    $finalAmountArray['graypay'] = 0;
                }
                if(isset($amountArray[5]) || isset($amountArray[9])){
                    $finalAmountArray['nmi'] = (isset($amountArray[5])?$amountArray[5]:0)+(isset($amountArray[9])?$amountArray[9]:0);
                }else{
                    $finalAmountArray['nmi'] = 0;
                }
                if(isset($amountArray[6])) {
                    $finalAmountArray['mo'] =$amountArray[6];
                }else{
                    $finalAmountArray['mo'] = 0;
                }
                if(isset($amountArray[10])) {
                    $finalAmountArray['jna'] = $amountArray[10];
                }else{
                    $finalAmountArray['jna'] = 0;
                }
                $gatewayNameArray = array();
                foreach($finalAmountArray as $key=>$value){
                    switch($key){
                        case 'graypay':
                            $gatewayName = 'Graypay';
                            break;
                        case 'nmi':
                            $gatewayName = 'NMI';
                            break;
                        case 'mo':
                            $gatewayName = 'Money Order';
                            break;
                        default:
                            $gatewayName = 'JNA';
                        }
                        $gatewayNameArray[$key] = $gatewayName;
                    }
                }else{
                    $finalAmountArray = array('No Data Available');
                    $gatewayNameArray = array('No Data Available');
                }}else{
                $finalAmountArray = array('No Data Available');
                $gatewayNameArray = array('No Data Available');
            }
            $key = 'Payment Gateway Amount';
            $xAxisHeading = 'Payment Gateway ->';

            ## Creating currency symbol...
            if($currency == 'pound'){
                $currencySymbol = '£';
            }else{
                $currencySymbol = '$';
            }

            $yAxisHeading = 'Collection ('.$currencySymbol.') ->';
            $title = 'Collection ('.$currencySymbol.') by Payment Gateway';


            if($chartType == 'pie'){
                return $this->createPieChart($finalAmountArray, $title, $gatewayNameArray);
            }else{
                return $this->createBarChart($key, $finalAmountArray, $title, $gatewayNameArray, $xAxisHeading, $yAxisHeading);
            }
        }



/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * This function return graph report of transactions by status...
 */
        public function executeTransactionReportByStatus(sfWebRequest $request){
            $startDate = $request->getParameter('fdate');
            $endDate = $request->getParameter('tdate');
            $chartType = $request->getParameter('chartType');
            $currency = $request->getParameter('currency');
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $url = 'report/TransactionReportByStatusData?start_date='.$startDate.'&end_date='.$endDate.'&chart_type='.$chartType.'&currency='.$currency;
            stOfc::createChart( 900, 200, $url, false );
            exit;
        }

/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * This function is being called by executeTransactionReportByStatus function...
 * Chart Type will decide weather Bar chart will be displayed or Pie chart...
 */
        public function executeTransactionReportByStatusData(sfWebRequest $request){

            $startDate = $request->getParameter('start_date');
            $endDate = $request->getParameter('end_date');
            $chartType = $request->getParameter('chart_type');
            $currency = $request->getParameter('currency');
            $chatData = array();

            $newArray = array();
            $query = Doctrine::getTable('Ipay4meOrder')->getTransactionStatusGraphReport($startDate,$endDate);
            $queryArray = $query->toArray();
            if(!empty($queryArray)){
                foreach($queryArray as $details){
                    if($details['payment_status'] != 4){
                        $orderReqObj = Doctrine::getTable('OrderRequestDetails')->find($details['order_request_detail_id']);
                        if($orderReqObj->getCurrency() == $currency){
                            $newArray[] = $details['payment_status'];
                        }
                    }
                }
                if(count($newArray) > 0 ){
                    $statusArray = array_count_values($newArray);

                    $completeStatusArray = array(0,1,2,3);
                    $diffArr = array_diff_key($completeStatusArray, $statusArray);
                    foreach($diffArr as $key=>$value){
                        $statusArray[$key] = 0;
                    }

                    ksort($statusArray);
                    $statusTitle = array();
                    foreach($statusArray as $key=>$value){
                        $title = '';


                        switch($key){
                            case 0:
                                $title = 'Success' ;
                                break;
                            case 1:
                                $title = 'Fail' ;
                                break;
                            case 2:
                                $title = 'Refund' ;
                                break;
                            default:
                                $title = 'Charge-back' ;
                            }
                            $statusTitle[$key] = $title;
                        }

                    }else{
                        $statusArray = array('No Data Available');
                        $statusTitle = array('No Data Available');
                    }}else{
                    $statusArray = array('No Data Available');
                    $statusTitle = array('No Data Available');
                }

                $key = 'Transaction by Payment Status';
                $title = 'Transaction by Payment Status';
                $xAxisHeading = 'Payment Status ->';
                $yAxisHeading = 'Transaction ->';

                if($chartType == 'pie'){
                    $this->createPieChart($statusArray, $title, $statusTitle);
                }else{
                    $this->createBarChart($key, $statusArray, $title, $statusTitle, $xAxisHeading, $yAxisHeading);
                }

            }

/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * This function return graph Gateway and status...
 */
            public function executeTransactionReportByGatewayAndStatus(sfWebRequest $request){

                $startDate = $request->getParameter('fdate');
                $endDate = $request->getParameter('tdate');
                $chartType = $request->getParameter('chartType');
                $paymentStatus = $request->getParameter('paymentStatus');
                $currency = $request->getParameter('currency');
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = 'report/transactionReportByGatewayAndStatusData?start_date='.$startDate.'&end_date='.$endDate.'&chart_type='.$chartType.'&paymentStatus='.$paymentStatus.'&currency='.$currency;
                stOfc::createChart( 445, 200, $url, false );
                exit;
            }

/**
 * [WP: 082] => CR:120
 * @param <type> $request
 * @return <type>
 * This function is being called by executeTransactionReportByGatewayAndStatus function...
 * Chart type will decide weather bar chart will be displayed or pie chart...
 */
            public function executeTransactionReportByGatewayAndStatusData(sfWebRequest $request){
                $startDate = $request->getParameter('start_date');
                $endDate = $request->getParameter('end_date');
                $chartType = $request->getParameter('chart_type');
                $paymentStatus = $request->getParameter('paymentStatus');
                $currency = $request->getParameter('currency');
                $successArray = array();
                $reportObj = Doctrine::getTable('Ipay4meOrder')->getTransactionStatusGraphReport($startDate,$endDate,$paymentStatus);
                //    echo "<pre>";print_r($reportObj->toArray());die;
                $queryArray = $reportObj->toArray();
                if(!empty($queryArray)){
                    foreach($queryArray as $details){ //echo $details['order_request_detail_id'];die;
                        $orderReqObj = Doctrine::getTable('OrderRequestDetails')->find($details['order_request_detail_id']);
                        $orderCurrency = $orderReqObj->getCurrency();
                        if($orderCurrency == $currency){
                            $successArray[] = $details['gateway_id'];
                        }
                    }
                    $gatewaySuccessArray = array_count_values($successArray);
                    if(!empty($gatewaySuccessArray)){
                        ksort($gatewaySuccessArray);
                        $transCountArray = array();
                        foreach($gatewaySuccessArray as $key=>$value){
                            $transCount = 0;
                            for($j=0;$j<count($queryArray);$j++){
                                if($queryArray[$j]['gateway_id'] == $key){
                                    $transCount += 1;
                                }
                            }
                            $transCountArray[$key] = $transCount;
                        }
                        $finalCounttArray = array();
                        if(isset($transCountArray[1]) || isset($transCountArray[8])){
                            $finalCounttArray['graypay'] = (isset($transCountArray[1])?$transCountArray[1]:0)+(isset($transCountArray[8])?$transCountArray[8]:0);
                        }else{
                            $finalCounttArray['graypay'] = 0;
                        }
                        if(isset($transCountArray[5]) || isset($transCountArray[9])){
                            $finalCounttArray['nmi'] = (isset($transCountArray[5])?$transCountArray[5]:0)+(isset($transCountArray[9])?$transCountArray[9]:0);
                        }else{
                            $finalCounttArray['nmi'] = 0;
                        }
                        if(isset($transCountArray[6])) {
                            $finalCounttArray['mo'] =$transCountArray[6];
                        }else{
                            $finalCounttArray['mo'] = 0;
                        }
                        if(isset($transCountArray[10])) {
                            $finalCounttArray['jna'] = $transCountArray[10];
                        }else{
                            $finalCounttArray['jna'] = 0;
                        }
                        $gatewayNameArray = array();
                        foreach($finalCounttArray as $key=>$value){
                            switch($key){
                                case 'graypay':
                                    $gatewayName = 'Graypay';
                                    break;
                                case 'nmi':
                                    $gatewayName = 'NMI';
                                    break;
                                case 'mo':
                                    $gatewayName = 'Money Order';
                                    break;
                                default:
                                    $gatewayName = 'JNA';
                                }
                                $gatewayNameArray[$key] = $gatewayName;
                            }
                        }else{
                            $finalCounttArray = array('No Data Available');
                            $gatewayNameArray = array('No Data Available');
                        }
                    }else{
                        $finalCounttArray = array('No Data Available');
                        $gatewayNameArray = array('No Data Available');
                    }

                    switch($paymentStatus){
                        case 0:
                            $status = 'Success';
                            break;
                        case 1:
                            $status = 'Fail';
                            break;
                        case 2:
                            $status = 'Refunded';
                            break;
                        default:
                            $status = 'Charge-back';
                            break;
                    }

                    $key = $status.' Transaction';
                    $xAxisHeading = 'Payment Gateway ->';
                    $yAxisHeading = 'Transactions ->';
                    $title = 'Transaction by Payment Gateway';
                    if($chartType == 'pie'){
                        return $this->createPieChart($finalCounttArray, $title, $gatewayNameArray);
                    }else{
                        return $this->createBarChart($key, $finalCounttArray, $title, $gatewayNameArray, $xAxisHeading, $yAxisHeading);
                    }
                }

                protected function createBarChart($key,$chartArrayData,$title,$titleArrayData,$xAxisHeading,$yAxisHeading,$xAxisHeadingRotation=false){

                    //To create a bar chart we need to create a stBarOutline Object
                    $bar = new stBarOutline( 80, '#78B9EC', '#3495FE' );
                    $bar->key( $key, 10 );

                    //Passing the random data to bar chart
                    $bar->data = $chartArrayData;

                    //Creating a stGraph object
                    $g = new stGraph();
                    $g->title( $title, '{font-size: 20px;}' );
                    $g->bg_colour = '#E4F5FC';
                    $g->set_inner_background( '#E3F0FD', '#CBD7E6', 90 );

                    $g->y_axis_colour( '#8499A4', '#E4F5FC' ,'#CBD7E6');

                    //Pass stBarOutline object i.e. $bar to graph
                    $g->data_sets[] = $bar;

                    //Setting labels for X-Axis
                    $g->set_x_labels($titleArrayData);

                    $g->set_tool_tip( 'Label: #x_label#<br>Value: #val#' );

                    // to set the format of labels on x-axis e.g. font, color, step
                    //$g->set_x_label_style( 12, '#18A6FF', 0);
                    //$g->set_x_label_style( 11, '#303030', 2 );
                    if($xAxisHeadingRotation){
                        $g->set_x_label_style( 12, '#18A6FF', 2);
                    }else{
                        $g->set_x_label_style( 12, '#18A6FF', 0);
                    }

                    // To tick the values on x-axis
                    // 2 means tick every 2nd value
                    $g->set_x_axis_steps( 2 );

                    //set maximum value for y-axis
                    //we can fix the value as 20, 10 etc.
                    //but its better to use max of data
                    $g->set_y_max( max($chartArrayData) );
                    $g->y_label_steps( 4 );
                    $g->set_x_legend($xAxisHeading, 12, '#18A6FF' );
                    $g->set_y_legend($yAxisHeading, 12, '#18A6FF' );

                    echo $g->render();

                    return sfView::NONE;

                }
                protected function createPieChart($chartArrayData,$title,$titleArrayData){

                    //Creating a stGraph object
                    $g = new stGraph();

                    //set background color
                    $g->bg_colour = '#E4F5FC';

                    //Set the transparency, line colour to separate each slice etc.
                    $g->pie(80,'#78B9EC','{font-size: 12px; color: #78B9EC;');
                    //array two arrray one containing data while other contaning labels
                    $g->pie_values($chartArrayData, $titleArrayData);

                    //Set the colour for each slice. Here we are defining three colours
                    //while we need 7 colours. So, the same colours will be
                    //repeated for the all remaining slices in the same order
                    $g->pie_slice_colours( array('#d01f3c','#c79810', '#cc3399', '#0066cc', '#9933cc', '#639f45'));

                    //To display value as tool tip
                    //$g->set_tool_tip( '#val#' );
                    $g->set_tool_tip( 'Label: #x_label#<br>Value: #val#' );

                    $g->title($title, '{font-size:18px; color: #18A6FF}' );
                    echo $g->render();
                    return sfView::NONE;

                }
    /**
     * [WP: 0820 => CR:120
     * @param <type> $request
     * This function return country wise collection/amount...
     */
                public function executeCountryWiseCollection(sfWebRequest $request){

                    $startDate = $request->getParameter('fdate');
                    $endDate = $request->getParameter('tdate');
                    $chartType = $request->getParameter('chartType');
                    $currency = $request->getParameter('currency');
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $url = 'report/countryWiseCollectionData?start_date='.$startDate.'&end_date='.$endDate.'&chart_type='.$chartType.'&currency='.$currency;
                    stOfc::createChart( 900, 200, $url, false );
                    exit;
                }

    /**
     * [WP: 082] => CR:120
     * @param <type> $request
     * This function is being called by executeCountryWiseCollection function ...
     * Chart type will decide weather bar chart or pie chart will be displayed...
     */
                public function executeCountryWiseCollectionData(sfWebRequest $request){

                    //        $startDate = '2012-06-20';
                    //        $endDate = '2012-06-27';
                    //        $chartType = 'bar';
                    //        $currency = 'dollar';
                    $startDate = $request->getParameter('start_date');
                    $endDate = $request->getParameter('end_date');
                    $chartType = $request->getParameter('chart_type');
                    $currency = $request->getParameter('currency');
                    $status = '0';
                    $orderObj = Doctrine::getTable('ipay4meOrder')->getTransactionStatusGraphReport($startDate, $endDate, $status);
                    $reportData = array();

                    ## Getting report country...
                    $transactionReportCountry = sfConfig::get('app_transactionReportCountry');
                    $transactionReportCountry = array_flip($transactionReportCountry);

                    if(count($orderObj) > 0){
                        foreach($orderObj AS $order){
                            $orderAmountObj = Doctrine::getTable('OrderRequestDetails')->find($order->getOrderRequestDetailId());
                            if(count($orderAmountObj) > 0){

                                if($orderAmountObj->getCurrency() == $currency){
                                    if($currency == 'dollar' && $orderAmountObj->getCurrency() == 'dollar'){
                                        $amount = $orderAmountObj->getAmount();
                                    }else if($currency == 'pound' && $orderAmountObj->getCurrency() == 'pound'){
                                        $amount = $orderAmountObj->getConvertAmount();
                                    }else{
                                        $amount = 0;
                                    }

                                    $appDetailObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetailsByORDId($order->getOrderRequestDetailId());
                                    if(count($appDetailObj->toArray()) > 0){
                                        $appId = $appDetailObj->getFirst()->getAppId();
                                        $appType = $appDetailObj->getFirst()->getAppType();

                                        $arrProcessingCountry = '';
                                        if($appType == 'Passport') {
                                            ## get Application details
                                            $arrProcessingCountry = Doctrine::getTable('PassportApplication')->getProcessingCountry($appId);
                                        }elseif ($appType == 'Freezone') {
                                            $arrProcessingCountry = 'NG';
                                        }elseif ($appType == 'Visa') {
                                            $arrProcessingCountry = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($appId);
                                        }elseif ($appType == 'Vap') {
                                            $arrProcessingCountry = Doctrine::getTable('VapApplication')->getProcessingCountry($appId);
                                        }
                                        ## Only defined processing country will be displayed...
                                        if(isset($transactionReportCountry[$arrProcessingCountry])){
                                            $reportData[$arrProcessingCountry][] = $amount;
                                        }
                                    }//End of if(count($appDetailObj->toArray()) > 0){...

                                }


                            }//End of if(count($orderAmountObj) > 0){...
                        }//End of foreach($orderObj AS $order){...
                    }//End of if(count($orderObj) > 0){.............

                    $countryNameArray = array();
                    $amountArray = array();

                    if(count($reportData) > 0){
                        ksort($reportData);

                        foreach($reportData AS $key => $value){
                            $amount = 0;
                            foreach($value AS $val){
                                $amount = $amount + $val;
                            }
                            if($amount > 0){
                                $amountArray[$key] = $amount;

                                $countryName = Doctrine::getTable('Country')->getCountryName($key);
                                $countryNameArray[] = $countryName;
                            }
                        }

                    }else{
                        $countryNameArray = array('No Data Available');
                        $amountArray = array('No Data Available');
                    }

                    ## Creating currency symbol...
                    if($currency == 'pound'){
                        $currencySymbol = '£';
                    }else{
                        $currencySymbol = '$';
                    }

                    $title = 'Collection ('.$currencySymbol.') by Country' ;
                    $key = 'Collection ('.$currencySymbol.')' ;
                    $xAxisHeading = 'Country -> ';
                    $yAxisHeading = 'Collection ('.$currencySymbol.') ->';
                    if($chartType == 'pie'){
                        $this->createPieChart($amountArray, $title, $countryNameArray) ;
                    }else{
                        $this->createBarChart($key, $amountArray, $title, $countryNameArray, $xAxisHeading, $yAxisHeading, true);
                    }
                }

    /**
     * [WP: 082] => CR:120
     * @param <type> $request
     * This function return country wise transactions...
     */
                public function executeCountryWiseTransaction(sfWebRequest $request){

                    $startDate = $request->getParameter('fdate');
                    $endDate = $request->getParameter('tdate');
                    $chartType = $request->getParameter('chartType');
                    $currency = $request->getParameter('currency');
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $url = 'report/countryWiseTransactionData?start_date='.$startDate.'&end_date='.$endDate.'&chart_type='.$chartType.'&currency='.$currency;
                    stOfc::createChart( 900, 200, $url, false );
                    exit;
                }

    /**
     * [WP: 082] => CR:120
     * @param <type> $request
     * This function is being called by executeCountryWiseTransaction function...
     * Chart type will decide weather bar chart or pie chart will be displayed...
     */
                public function executeCountryWiseTransactionData(sfWebRequest $request){

                    $startDate = $request->getParameter('start_date');
                    $endDate = $request->getParameter('end_date');
                    $chartType = $request->getParameter('chart_type');
                    $currency = $request->getParameter('currency');
                    $status = '0';
                    $orderObj = Doctrine::getTable('ipay4meOrder')->getTransactionStatusGraphReport($startDate, $endDate, $status);
                    $reportData = array();

                    ## Getting report country...
                    $transactionReportCountry = sfConfig::get('app_transactionReportCountry');
                    $transactionReportCountry = array_flip($transactionReportCountry);

                    if(count($orderObj) > 0){
                        foreach($orderObj AS $order){
                            $orderAmountObj = Doctrine::getTable('OrderRequestDetails')->find($order->getOrderRequestDetailId());
                            if(count($orderAmountObj) > 0){

                                if($orderAmountObj->getCurrency() == $currency){

                                    $amount = $orderAmountObj->getAmount();

                                    $appDetailObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetailsByORDId($order->getOrderRequestDetailId());
                                    if(count($appDetailObj->toArray()) > 0){
                                        $appId = $appDetailObj->getFirst()->getAppId();
                                        $appType = $appDetailObj->getFirst()->getAppType();

                                        $arrProcessingCountry = '';
                                        if($appType == 'Passport') {
                                            ## get Application details
                                            $arrProcessingCountry = Doctrine::getTable('PassportApplication')->getProcessingCountry($appId);
                                        }elseif ($appType == 'Freezone') {
                                            $arrProcessingCountry = 'NG';
                                        }elseif ($appType == 'Visa') {
                                            $arrProcessingCountry = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($appId);
                                        }elseif ($appType == 'Vap') {
                                            $arrProcessingCountry = Doctrine::getTable('VapApplication')->getProcessingCountry($appId);
                                        }
                                        ## Only defined processing country will be displayed...
                                        if(isset($transactionReportCountry[$arrProcessingCountry])){
                                            $reportData[] = $arrProcessingCountry;
                                        }//End of if(isset($transactionReportCountry[$arrProcessingCountry])){...
                                    }//End of if(count($appDetailObj->toArray()) > 0){...
                                }
                            }//End of if(count($orderAmountObj) > 0){...
                        }//End of foreach($orderObj AS $order){...
                    }//End of if(count($orderObj) > 0){...

                    if(count($reportData) > 0){
                        $finalArray = array_count_values($reportData);
                        ksort($finalArray);
                        $countryNameArray = array();

                        foreach($finalArray AS $key => $value){
                            $countryName = Doctrine::getTable('Country')->getCountryName($key);
                            $countryNameArray[] = $countryName;
                        }

                    }else{
                        $countryNameArray = array('No Data Available');
                        $finalArray = array('No Data Available');
                    }

                    $title = 'Transaction by Country' ;
                    $key = 'Transaction' ;
                    $xAxisHeading = 'Country ->';
                    $yAxisHeading = 'Transaction ->';
                    if($chartType == 'pie'){
                        $this->createPieChart($finalArray, $title, $countryNameArray) ;
                    }else{
                        $this->createBarChart($key='', $finalArray, $title, $countryNameArray, $xAxisHeading, $yAxisHeading, true);
                    }
                }


                public function executeVisaArrivalReport(sfWebRequest $request){

                    if($request->isMethod('POST')){
                        $this->startDate = $request->getParameter('fdate');
                        $this->endDate = $request->getParameter('tdate');
                        $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getVisaArrivalReport($this->startDate,$this->endDate);
                    }
                }

                /**
                 * [WP: 092] => CR:132
                 * @param <type> $request
                 */

                public function executePosPaymentReport(sfWebRequest $request){

                    if($request->isMethod('POST')){
                        $this->startDate = $request->getParameter('fdate');
                        $this->endDate = $request->getParameter('tdate');
                        $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getPOSPaymentReport($this->startDate,$this->endDate);
                    }
                }


                /**
                 * [WP: 102] => CR: 145
                 * [WP: 112] => CR: 158
                 * @param <type> $request
                 * Showing Visa weekly reprot...
                 */
                public function executeVisaReport(sfWebRequest $request){


                    $userId = $this->getUser()->getGuardUser()->getId();

                    $this->excelFileName = 'visa_report_'.$userId.'.xls';

                    $this->reported_months = sfConfig::get('app_visa_reported_months');

                    if($request->isMethod('POST')){
                        $this->startDate = $request->getParameter('fdate');
                        $this->endDate = $request->getParameter('tdate');
                        $this->reportType = $request->getParameter('reportType');
                        $this->countryWise = $request->getParameter('countryWise');
                        $this->selectedCountries = array_flip($request->getParameter('selectedCountries'));

                        /*
                         * Country wise report will be displayed if and only if country wise checkbox is selected...
                         */
                        if($this->countryWise != ''){
                            $countries = $request->getParameter('selectedCountries');
                            if($this->reportType == 'weekly'){
                                ## Converting date range into week...
                                $weeklyArray = Functions::get_week_from_daterange($this->startDate, $this->endDate);
                                $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getVisaWeeklyReportByCountry($weeklyArray, $countries);
                            }else{ 
                                $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getVisaMonthlyReportByCountry($this->startDate, $this->endDate, $countries);
                            }

                        }else{
                            if($this->reportType == 'weekly'){
                                ## Converting date range into week...
                                $weeklyArray = Functions::get_week_from_daterange($this->startDate, $this->endDate);
                                $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getVisaWeeklyReport($weeklyArray);
                            }else{
                                $this->reportArr = Doctrine::getTable('TransactionServiceCharges')->getVisaMonthlyReport($this->startDate, $this->endDate);
                            }
                        }
                    }else{
                        $this->startDate = '';
                        $this->endDate = '';
                        $this->reportType = '';
                        $this->countryWise = '';
                        $this->selectedCountries = array();
                    }


                    $this->countryListObj = Doctrine::getTable('Country')->getCountryListOption();//->orderBy('country_name ASC');


                }

                /**
                 * [WP: 112] => CR: 158
                 * @param <type> $request
                 * This method generate excel sheet...
                 */
                public function executeDownloadVisaReportInExcel(sfWebRequest $request){

                    $filename = 'visa_report_'.$this->getUser()->getGuardUser()->getId().'.xls';

                    $reportArr = json_decode($request->getParameter('reportArr'));
                    
                    $countryListObj = json_decode($request->getParameter('countryListObj'));
                    $reportType = $request->getParameter('reportType');
                    $countryWise = $request->getParameter('countryWise');

                    $isExcelShow = 'no';
                    $excel = new ExcelWriter('uploads/excel/' . $filename);
                    if ($excel == false) {
                        $isExcelShow = 'error'; //$excel->error;
                    } else {
                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));

                        if($reportType == 'monthly'){
                            if($countryWise == 'countryWise'){
                                $this->_createMonthlyByCountryExcel($excel, $isExcelShow, $reportArr, $countryListObj);
                            }else{ 
                                $this->_createMonthlyExcel($excel, $isExcelShow, $reportArr);
                            }
                        }else if($reportType == 'weekly'){                            
                            if($countryWise == 'countryWise'){ 
                                $this->_createWeeklyByCountryExcel($excel, $isExcelShow, $reportArr, $countryListObj);
                            }else{
                                $this->_createWeeklyExcel($excel, $isExcelShow, $reportArr);
                            }
                            
                        }

                        $excel->close();
                    }
                    return $this->renderText($isExcelShow);

                }

                /**
                 * [WP: 112] => CR: 158
                 * @param <type> $request 
                 */
                public function executeDownloadVisaReport(sfWebRequest $request){

                    $filename = $request->getParameter('filename');
                    
                    // Modify this line to indicate the location of the files you want people to be able to download
                    // This path must not contain a trailing slash. ie. /temp/files/download
                    $download_path = "";
                    // Make sure we can't download files above the current directory location.
                    if(eregi("\.\.", $filename)) die("I'm sorry, you may not download that file.");
                        $file = str_replace("..", "", $filename);
                    // Make sure we can't download .ht control files.
                    if(eregi("\.ht.+", $filename)) die("I'm sorry, you may not download that file.");
                    // Combine the download path and the filename to create the full path to the file.

                    $download_path = '../web/uploads/excel/';

                    $file = "$download_path$file";
                    // Test to ensure that the file exists.
                    if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");
                    // Extract the type of file which will be sent to the browser as a header
                    $type = filetype($file);
                    // Get a date and timestamp
                    $today = date("F j, Y, g:i a");
                    $time = time();


                    $fsize = filesize($file);
                    header("Pragma: public"); // required
                    header("Expires: 0");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Cache-Control: private",false); // required for certain browsers
                    header("Content-Type: application/xls");
                    header("Content-Disposition: attachment; filename=\"".basename($file)."\";" );
                    header("Content-Transfer-Encoding: binary");
                    header("Content-Length: ".$fsize);
                    ob_clean();
                    flush();
                    readfile( $file );
                    exit;
                    
                }//End of public function executeDownloadVisaReport(sfWebRequest $request){...

                
                /**
                 * [WP: 112] => CR: 158
                 * @param <type> $excel
                 * @param <type> $isExcelShow
                 * @param <type> $reportArr 
                 */
                private function _createMonthlyExcel($excel, & $isExcelShow, $reportArr){

                    $excel->writeLine(array('<b>VISA MONTHLY REPORT</b>'), NULL, '5');
                    $excel->writeLine(array());

                    $myArr = array("<b>Month</b>", "<b>Total Applications</b>", "<b>Application Amount($)</b>", "<b>".ucwords(sfConfig::get('app_visa_additional_charges_text'))."($)</b>", "<b>Total Amount($)</b>");
                    $excel->writeLine($myArr);
                    $tmp_country = '';

                    foreach($reportArr AS $value) {

                        if($value->processing_country != $tmp_country){
                            $excel->writeLine(array('Country: '.$countryListObj->$value->processing_country));
                        }
                        $tmp_country = $value->processing_country;

                        //$currencySymbol = PaymentModeManager::currencySymbol(4);
                        $month = $value->month.' ('.$value->year.') ';
                        $count = ($value->count > 0)?$value->count:'--';
                        $app_amount = ($value->app_amount > 0)?number_format($value->app_amount,2):'--';
                        //$app_convert_amount = ($value->app_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_amount,2).')':'';


                        $service_charge = ($value->service_charge > 0)?number_format($value->service_charge,2):'--';
                        //$app_convert_service_charge = ($value->app_convert_service_charge > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_service_charge,2).')':'';

                        $total_amount = ($value->total_amount > 0)?number_format($value->total_amount,2):'--';
                        //$total_convert_amount = ($value->total_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->total_convert_amount,2).')':'';

                        $dataArr = array($month, $count, $app_amount, $service_charge, $total_amount);
                        //$dataArr = array($month, $count, $app_amount.$app_convert_amount, $service_charge.$app_convert_service_charge, $total_amount.$toal_convert_amount);
                        
                        $excel->writeLine($dataArr);

                    }

                    $isExcelShow = 'yes';
                }

                /**
                 * [WP: 112] => CR: 158
                 * @param <type> $excel
                 * @param <type> $isExcelShow
                 * @param <type> $reportArr
                 * @param <type> $countryListObj
                 */
                private function _createMonthlyByCountryExcel($excel, & $isExcelShow, $reportArr, $countryListObj){ 

                    $excel->writeLine(array('<b>COUNTRY WISE VISA MONTHLY REPORT</b>'), NULL, '6');
                    $excel->writeLine(array());

                    $myArr = array("<b>Country</b>", "<b>Month</b>", "<b>Total Applications</b>", "<b>Application Amount($)</b>", "<b>".ucwords(sfConfig::get('app_visa_additional_charges_text'))."($)</b>", "<b>Total Amount($)</b>");
                    $excel->writeLine($myArr, array('text-align'=>'center', 'color'=> '#EEEEEE'));
                    

                    foreach($reportArr AS $countryKey => $countryValue) {
                        
                        $excel->writeLine(array($countryListObj->$countryKey), NULL, '6');

                        foreach($countryValue AS $value) {

                            //$currencySymbol = PaymentModeManager::currencySymbol(4);
                            $month = $value->month.' ('.$value->year.') ';
                            $count = ($value->count > 0)?$value->count:'--';
                            $app_amount = ($value->app_amount > 0)?number_format($value->app_amount,2):'--';
                            //$app_convert_amount = ($value->app_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_amount,2).')':'';


                            $service_charge = ($value->service_charge > 0)?number_format($value->service_charge,2):'--';
                            //$app_convert_service_charge = ($value->app_convert_service_charge > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_service_charge,2).')':'';

                            $total_amount = ($value->total_amount > 0)?number_format($value->total_amount,2):'--';
                            //$total_convert_amount = ($value->total_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->total_convert_amount,2).')':'';

                            $dataArr = array('', $month, $count, $app_amount, $service_charge, $total_amount);
                            //$dataArr = array('', $month, $count, $app_amount.$app_convert_amount, $service_charge.$app_convert_service_charge, $total_amount.$toal_convert_amount);
                            $excel->writeLine($dataArr);

                        }//End of foreach($countryValue AS $value) {...
                        
                    }//End of foreach($reportArr AS $countryKey => $countryValue) {...                    

                    $isExcelShow = 'yes';
                }

                /**
                 * [WP: 112] => CR: 158
                 * @param <type> $excel
                 * @param <type> $isExcelShow
                 * @param <type> $reportArr
                 */
                private function _createWeeklyExcel($excel, & $isExcelShow, $reportArr){

                    $excel->writeLine(array('<b>VISA WEEKLY REPORT</b>'), NULL, '6');
                    $excel->writeLine(array());
                    
                    $myArr = array("<b>Start Week</b>", "<b>End Week</b>", "<b>Total Applications</b>", "<b>Application Amount($)</b>", "<b>".ucwords(sfConfig::get('app_visa_additional_charges_text'))."($)</b>", "<b>Total Amount($)</b>");
                    $excel->writeLine($myArr);

                    foreach($reportArr AS $value) {
                        
                        $week_start = date('j M, Y', strtotime($value->week_start));
                        $week_end = date('j M, Y', strtotime($value->week_end));
                        //$excel->writeLine(array('WEEK: '.$week_start. ' - '.$week_end));
                        
                        

                        //$currencySymbol = PaymentModeManager::currencySymbol(4);
                        
                        $count = ($value->count > 0)?$value->count:'--';
                        $app_amount = ($value->app_amount > 0)?number_format($value->app_amount,2):'--';
                        //$app_convert_amount = ($value->app_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_amount,2).')':'';


                        $service_charge = ($value->service_charge > 0)?number_format($value->service_charge,2):'--';
                        //$app_convert_service_charge = ($value->app_convert_service_charge > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_service_charge,2).')':'';

                        $total_amount = ($value->total_amount > 0)?number_format($value->total_amount,2):'--';
                        //$total_convert_amount = ($value->total_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->total_convert_amount,2).')':'';

                        $dataArr = array($week_start, $week_end, $count, $app_amount, $service_charge, $total_amount);
                        //$dataArr = array($week_start, $week_end, $count, $app_amount.$app_convert_amount, $service_charge.$app_convert_service_charge, $total_amount.$toal_convert_amount);
                        
                        $excel->writeLine($dataArr);

                    }

                    $isExcelShow = 'yes';
                }

                private function _createWeeklyByCountryExcel($excel, & $isExcelShow, $reportArr, $countryListObj){

                    $excel->writeLine(array('<b>COUNTRY WISE VISA WEEKLY REPORT</b>'), NULL, '7');
                    $excel->writeLine(array());
                    
                    $myArr = array("<b>Country</b>", "<b>Week Start</b>", "<b>Week End</b>", "<b>Total Applications</b>", "<b>Application Amount($)</b>", "<b>".ucwords(sfConfig::get('app_visa_additional_charges_text'))."($)</b>", "<b>Total Amount($)</b>");
                    $excel->writeLine($myArr);
                    
                    foreach($reportArr AS $key => $report) {
                                            
                            $excel->writeLine(array($countryListObj->$key), NULL, '7');
                            
                            $tmpYear = '';
                            foreach($report AS $value) {

                                
                                $week_start = date('j M, Y', strtotime($value->week_start));
                                $week_end = date('j M, Y', strtotime($value->week_end));
                                            
                                
                                //$currencySymbol = PaymentModeManager::currencySymbol(4);

                                $count = ($value->count > 0)?$value->count:'--';
                                $app_amount = ($value->app_amount > 0)?number_format($value->app_amount,2):'--';
                                //$app_convert_amount = ($value->app_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_amount,2).')':'';


                                $service_charge = ($value->service_charge > 0)?number_format($value->service_charge,2):'--';
                                //$app_convert_service_charge = ($value->app_convert_service_charge > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->app_convert_service_charge,2).')':'';

                                $total_amount = ($value->total_amount > 0)?number_format($value->total_amount,2):'--';
                                //$total_convert_amount = ($value->total_convert_amount > 0)?' ('.html_entity_decode($currencySymbol).number_format($value->total_convert_amount,2).')':'';

                                $dataArr = array('',$week_start, $week_end, $count, $app_amount, $service_charge, $total_amount);
                                //$dataArr = array('',$week_start, $week_end, $count, $app_amount.$app_convert_amount, $service_charge.$app_convert_service_charge, $total_amount.$toal_convert_amount);
                                
                                $excel->writeLine($dataArr);

                            }
                        
                    }


                    

                    $isExcelShow = 'yes';
                }


                /**
                 * [WP: 102] => CR: 146
                 * @param <type> $request
                 */
                public function executeRegisteredCardReport(sfWebRequest $request){

                    $this->email = $request->getParameter('email');
                    $this->card_first = $request->getParameter('card_first');
                    $this->card_last = $request->getParameter('card_last');

                    if ($request->isMethod('POST')) {
                        $this->showDataFlag = true;
                    }else{
                        $this->searchFlag = false;
                        $this->email = ($this->email != '')?Settings::decryptInput($this->email):'';
                        $this->card_first = ($this->card_first != '')?Settings::decryptInput($this->card_first):'';
                        $this->card_last = ($this->card_last != '')?Settings::decryptInput($this->card_last):'';
                    }

                    $applicantVaultQuery = Doctrine::getTable('ApplicantVault')->getRegisteredCardDetails($this->card_first, $this->card_last, '', '', $this->email);



                    //$applicantVaultQuery = Doctrine::getTable('ApplicantVault')->getDetailsByCardNumber($this->card_first, $this->card_last, '', '', $this->email, $this->userId);

                    $page = 1;
                    if ($request->hasParameter('page')) {
                        $page = $request->getParameter('page');
                    }

                    $this->pager = new sfDoctrinePager('ApplicantVault', sfConfig::get('app_records_per_page'));
                    $this->pager->setQuery($applicantVaultQuery);
                    $this->pager->setPage($page);
                    $this->pager->init();

                    $this->showDataFlag = true;
                }

                /**
                 * [WP: 102] => CR: 146
                 * @param <type> $request
                 */
                public function executeCardDetails(sfWebRequest $request){
                    $this->setLayout(false);
                    $this->cardReportArray = ipay4meOrderTable::getInstance()->getPaymentDetailsForReport($request->getParameter('card_first'), $request->getParameter('card_last'), $request->getParameter('card_hash'),  $request->getParameter('user_id'), $request->getParameter('start_date'));

                }


            }
