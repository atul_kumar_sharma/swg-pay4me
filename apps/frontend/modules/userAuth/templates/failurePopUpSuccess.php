<html><head><title></title>
  <style type="text/css">

h3{
  background-color:#eee7e7;
  -moz-border-radius: 5px;
  height:22px;
}
ol li{
  padding-bottom:10px;
}

  </style>
</head><body bgcolor="#eceee9">
<div class="global_content4 clearfix" style="border:solid 1px #CCCCCC;">
<div class="comBox">
<h3><a name="PayOption"><span style="padding-left:5px;">Failure Description</span></a></h3>
<a name="PayOption"> </a>
<p style="padding:15px">In order to minimize and/or prevent the fraudulent use of Credit/Debit cards on this site, SW Global LLC DOES NOT allow usage of a single card more than once on our platform. If you intend to use your card multiple times on this site, you may request permission to do so by following the procedure outlined below.

<!--
<br><br><b>NOTE THAT THESE RULES DO NOT APPLY TO VERIFY BY VISA OR MASTERCARD SECURECODE ENABLED CARDS.  THERE ARE NO LIMITS TO THE USE OF CARDS THAT ARE VERIFIED BY VISA OR MASTERCARD SECURECODE ENABLED.</b>
</p>
<p style="padding:15px"><b>PLEASE CONTACT YOUR BANK TO ENABLE VERIFIED BY VISA AND/OR MASTERCARD SECURECODE ON YOUR CARD FOR MULTIPLE USE ON THIS SITE (OR VISIT <a target="_blank" href="http://usa.visa.com/personal/security/index.html">http://usa.visa.com/personal/security/index.html</a> OR <a target="_blank" href="http://www.mastercard.us/securecode.html">http://www.mastercard.us/securecode.html</a>).
  PLEASE CONTACT US TO REQUEST AND REGISTER YOUR CARD FOR MULTIPLE USE ON THIS SITE, AS DESCRIBED BELOW, ONLY IF YOU ARE UNABLE TO SECURE YOUR CARD VIA VERIFIED BY VISA OR MASTERCARD SECURECODE..</b>
</p>
-->
<br/>
<h3 ><a name="PayOption"><span style="padding-left:5px;">How to Request/register</span></a></h3>

<p class="body_txtp">
  <ol style="padding-left:25px;"><li>Fill out the request form on the next page. You are required to produce proof of your name and billing address as associated with your credit card. A scanned copy of your latest credit card statement (clearly indicating address) shall suffice for this purpose.<br><br>
      If your credit card statement carries the full credit card numbers on it, please ensure to hide (by overwriting it with dark black pen) the 8 middle digits of your credit card leaving only first four and last four digits visible. For example, if your credit card number is 1234-5678-5678-9876, please mask  5678-5678 so that it appears as 1234-xxxx-xxxx-9876 in the scanned copy that you upload for our review.
    </li>
    <li>Submit it for our review and approval. You'll also be sent a notification email confirming your submission.</li>
  <li>Once your request is processed, a notification will be sent to you for confirmation and next steps.</li></ol>

  <br />


  <strong> &nbsp; &nbsp; &nbsp; &nbsp; On successful approval you will be able to use your card for multiple payments on SW Global LLC platform.</strong>
</p>
</div>

<div style="padding-left:400px;">
  <div class="lblButton">
    <!-- parent.document.confirmViewApp(); window.close(); -->
    <?php $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/userAuth/newContectDetails'; ?>
  <input type="button" value="Continue" onClick="window.opener.location.href='<?=$url ?>'; window.close();" class="normalbutton" style=""></div>
  <div class="lblButtonRight">
    <div class="btnRtCorner"></div>
  </div>
  <br />
</div>
</div>
</body></html>
<script>
  function confirmViewApp(){
    window.location = '<?php echo url_for('userAuth/newContectDetails'); ?>';
  }
</script>
