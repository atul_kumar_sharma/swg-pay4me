<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Volt Report'));
?>
<?php //use_javascript('jquery.form.js'); ?>
<?php //use_javascript('jquery.datePicker.js'); ?>
<?php //use_javascript('date.js'); ?>
<?php //use_stylesheet('datePicker.css'); ?>
<div class="global_content4 clearfix">
<div class="brdBox">
 <div class="clearfix"/>
<?php echo form_tag($sf_context->getModuleName().'/vaultreport',array('name'=>'volt_form','class'=>'', 'onsubmit'=>'valid_form()','method'=>'post','id'=>'gateway_request_form')) ?>

  <div class="wrapForm2">

            <table width="50%">
                <?php echo $voltReportForm;?>
                <tr>
                    <th>
                     <ul id="error_volt" class="error_list">
                         
                    </ul>
                    <label for="max_amount">Max Amount<font color="red">*</font></label>
                     </th>
                    <td><input type="text" name="maxAmount" class="txt-input" id="maxAmount" readonly="true" maxlength="10"/></td>
                </tr>
                <tr>
                    <th><label for="max_amount">Volt Type<font color="red">*</font></label></th>
                    <td>
                        <select name="voltType">
                            <option value="0">Not Attempted</option>
                            <option value="1">Attempted </option>
                            <option value="2">Paid</option>
                            <option value="3">failure</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php   echo submit_tag('Search',array('class' => 'button')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            <br />
            <div id="result_div" style="width:500px" align="center"></div>
        </div>


</form>
</div></div>
</div>
<script>


 function valid_form(){
     if( !document.getElementById('maxAmount').value )
     {
         
         $('#error_volt').html('Please enter the amount');
         return false;
     }
     else
        $('#error_volt').html('');

    return true;
 }
<?php
if("valid" == $formValid )
{
    ?>
        $(document).ready(function()
        {
            if( valid_form())
            validate_search_form();
        });
    <?php
}
?>
   function validate_search_form(){
        $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;

        if(err == 0){
            $.post('voltreportSearch',$("#volt_form").serialize(), function(data){

                if(data == "logout"){
                    location.reload();
                }else{
                    $("#result_div").html(data);
                }
            });
        }


    }


</script>
