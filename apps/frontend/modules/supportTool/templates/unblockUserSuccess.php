<?php
$userDetailArray = $sf_data->getRaw('userDetailArray');
?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Blocked User'));
?>
<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>

      <br>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <br/>
      <?php }?>
    <?php echo form_tag('supportTool/unblockUser',array('name'=>'user_search_form','class'=>'', 'method'=>'POST','id'=>'user_search_form','onsubmit'=>'return validateForm()')) ?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
        <?php

        echo formRowComplete('Email Id<span class="red">*</span>',input_tag('email', $email, array('size' => 40, 'maxlength' => 50,'class'=>'txt-input')));
        echo "<tr>";?>
        <tr>
          <td>&nbsp;</td>
        <td><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </td>
        </tr>
      </table>
     
      <div class="clearfix">
      <div id="nxtPage">
       <?php if(count($userDetailArray) > 0){ ?>
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
            <td class="blbar" colspan="8" align="right"><div class="float_left">User List</div></td>
          </tr>
          
          <tr>
            <td width="7%" ><span class="txtBold">S. No.</span></td>
            <td width="20%"><span class="txtBold">First Name</span></td>
            <td width="20%"><span class="txtBold">Last Name</span></td>
            <td width="20%"><span class="txtBold">Email</span></td>
            <td width="20%"><span class="txtBold">Address</span></td>
            <td width="13%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php $i=0; foreach ($userDetailArray as $userDetails): ?>
            <tr>
              <td><?php echo ++$i;?></td>
              <td><span><?php echo $userDetails->getFirstName();?></a></span></td>
              <td><span><?php echo $userDetails->getLastName(); ?></span></td>
              <td><span><?php echo $userDetails->getEmail(); ?></span></td>
              <td><span><?php echo nl2br($userDetails->getAddress());?></span></td>              
              <td><span><?php if($userDetails->getPaymentRights() == 1){ ?><a href ="javascript:void(0);" onclick="unblockUser('<?php echo base64_encode($userDetails->getUserId());?>');" id="order_no">Unblock</a><?php } else { ?>----<?php } ?></span></td></tr>
            <?php endforeach; ?>
            <tr>
              <td class="blbar" colspan="6" height="25px" align="right"><div class="paging pagingFoot"></div></td>
            </tr>
          </tbody>
        </table>
        <?php } ?>
        <br>
      </div>
    </div>
    
  </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    
 function validateForm(){
     var email = $.trim($('#email').val());
     if(email == ''){
         alert('Please enter email address.');
         $('#email').focus();
         return false;
     }
     var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
     if(reg.test(email) == false){         
         alert('Please enter valid email address.');
         $('#email').focus();
         return false;
     }
 }
 $(document).ready(function(){
     $('#email').focus();
 })
 function unblockUser(userId){
     var url = '<?php echo url_for('supportTool/doUnblockUser?id='); ?>'+userId;     
     if(confirm('Are you sure?')){
         location.href=url;
     }
 }
</script>