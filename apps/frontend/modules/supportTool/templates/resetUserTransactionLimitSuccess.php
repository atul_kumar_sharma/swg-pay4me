<!--
New Template added to list user transaction details
-->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Reset User Transaction Limit'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
  <div id="nxtPage"> <?php echo form_tag('supportTool/resetUserTransactionLimit',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form', 'onsubmit' => 'return frmValidate()')) ?>
            <?php
            $sf = sfContext::getInstance()->getUser();
            if($sf->hasFlash('notice')){ ?>
            <div id="flash_notice" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('notice'));
                ?>
    </div>
    <br/>
            <?php }?>
                <?php
                $sf = sfContext::getInstance()->getUser();
                if($sf->hasFlash('error')){ ?>
                <div id="flash_error" class="alertBox" >
                    <?php
                    echo nl2br($sf->getFlash('error'));
                    ?>
    </div>
    <br/>
                <?php }?>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <?php
                    echo formRowComplete('Card First',input_tag('card_first',$card_first , array('size' => 4, 'maxlength' => 4,'class'=>'txt-input')));
                    echo "</tr>";
                    echo formRowComplete('Card Last',input_tag('card_last', $card_last, array('size' => 4, 'maxlength' => 4,'class'=>'txt-input')));
                    echo "</tr>";
                    echo "<tr><td colspan='2' style='text-align:center'><strong>OR</strong></td></tr>";
                    echo formRowComplete('Email Address',input_tag('email', $email, array('size' => 30, 'maxlength' => 50,'class'=>'txt-input')));
                    echo "</tr>";
                    ?>
                    

                    
                    <tr valign="top" >
        <td height="30" valign="top">&nbsp;</td>
        <td height="30" valign="top"><?php echo submit_tag('Search',array('class' => 'normalbutton')); ?> </td>
                    </tr>
                </table>
        </form>
	<div class="clear">&nbsp;</div>
        <table width="100%">


            <?php if($dataFlag){ ?>

            <tr>
        <td class="blbar" colspan="9" align="right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
            </tr>
            <tr>
                <td width="5%"><span class="txtBold">S. No.</span></td>
                <td width="15%"><span class="txtBold">First Name</span></td>
                <td width="15%"><span class="txtBold">Last Name</span></td>
                <td width="20%"><span class="txtBold">Email</span></td>
                <td width="15%"><span class="txtBold">Mobile Number</span></td>
                <td width="15%"><span class="txtBold">Card First</span></td>
                <td width="15%"><span class="txtBold">Card Last</span></td>
                <td width="10%"><span class="txtBold">Action</span></td>
            </tr>
            <?php

            
                
            


            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                $arrActiveUser =array();
                if(!empty($arrActiveUserId)) {
                    foreach ($arrActiveUserId as $val) {
                        $arrActiveUser[] = $val['user_id'];
                    }
                }

                ?>
                <?php
                foreach ($pager->getResults() as $result):               

                
                $i++;
                
                
                ?>
            <tr>
                <td><?php echo $i; ?></td>
        <td ><span>
          <?php if($result->getFirstName() !='') {echo $result->getFirstName();}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->getLastName() !=''){echo $result->getLastName();}else{echo "--";}?>
          </span></td>
        <td ><span >
          <?php if($result->getEmail() !=''){echo $result->getEmail();}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->getPhone() !=''){echo $result->getPhone();}else{echo "--";}?>
          </span></td>
          <td><span >
          <?php if($result->getCardFirst() !=''){echo $result->getCardFirst();}else{echo "--";}?>
          </span></td>
          <td><span >
          <?php if($result->getCardLast() !=''){echo $result->getCardLast();}else{echo "--";}?>
          </span></td>
        
        
        <td><?php
                            $user_id = $result->getUserId();
                            $encriptedUser_id = SecureQueryString::ENCRYPT_DECRYPT($user_id);
                            $encriptedUser_id = SecureQueryString::ENCODE($encriptedUser_id);
                    ?>
                  
                                 
          
                <a href="javascript:void(0);" onclick="resetLimit('<?php echo $encriptedUser_id; ?>', '<?php echo $result->getCardHash(); ?>');">
                  <img src="<?php echo image_path('reset_icon.png'); ?>" alt="Reset Limit" title="Reset User Transaction Limit" id="resetLimitId" />
                </a>
                    
                    
                </td>
            </tr>
            <?php endforeach; ?>
            <tr>
        <td class="blbar" colspan="9" height="25px" align="right"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?card_first='.$card_first.'&card_last='.$card_last.'&email='.$email.'&usePager=true'.'&back=1') ?></td>
            </tr>
            <?php
        }else { ?>
      <tr>
        <td  colspan="9" align='center' ><div class="red"> No record found</div></td>
      </tr>
            <?php } } ?>
        </table>
    </div>
</div>
</div><div class="content_wrapper_bottom"></div>

<div id="popupContent"></div>
<div id="backgroundPopup"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>

<script>
    function frmValidate(){
        var card_first = $.trim($('#card_first').val());
        var card_last = $.trim($('#card_last').val());
        var email = $.trim($('#email').val());


        if(card_first != '' && card_last != '' && email != ''){
            alert("Please input either card first and card last or email address.");
            return false;
        }else if(card_first == '' && card_last == '' && email == ''){
            alert("Please input either card first and card last or email address.");
            return false;
        }else if( (card_first != '' || card_last != '') && email != ''){
            alert("Please input either card first and card last or email address.");
            return false;
        }else if( (card_first != '' && card_last == '') || (card_first == '' && card_last != '')){
            alert("Please input both card first and card last.");
            return false;
        }else{
            if(card_first != '' && card_last != ''){
                if(isNaN(card_first) || isNaN(card_last) ){
                    alert("Please input numeric value only.");
                    return false;
                }
            }else{
                if(validateEmail(email) != 0){
                    alert("Please enter valid Email Address.");
                    return false;                    
                }
            }
        }

        return true;
    }

    function validateEmail(email) {

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(reg.test(email) == false) {

          return 1;
        }

        return 0;
    }



     function resetLimit(userId, cardHash) {

        
        
        var url = "<?php echo url_for('supportTool/resetTransactionLimit/'); ?>";

        centerPopup();
        var elementPos = findElementPosition('resetLimitId');
        elementPos = elementPos.split(',');
        $("#popupContent").css({
                "position": "absolute",
                "top": (parseInt(elementPos[1]-200))+'px',
                "left":(parseInt(elementPos[0]-600))+'px',
                "width":'600px'
        });
        $("#popupContent").html($('#loading').html());
        loadPopup();

        $.ajax({
            type: "POST",
            url: url,
            data: {'uid': userId, 'cardHash': cardHash},
            success: function (data) {
                data = jQuery.trim(data);
//                if(data == 'notfound'){
//                    $('#appStatusFlag').val('new');
//                    document.frm.submit();
//                }else if(data == 'error'){
//                    $('#popupContent').hide('slow');
//                    alert('Oops! Some technical problem occur. Please try it again.');
//                }else{
                    $('#popupContent').html(data);
                    //alert(data);
                    $("#popupContent").css({
                        "width": '600px'
                    });

                   
              
                //}
            }//End of success: function (data) {...
        });

    }//End of function checkForm() {...  


</script>