<?php
use_helper('Form');
$ipayNisDetailsQuery = $sf_data->getRaw('ipayNisDetailsQuery'); ?>

<script type="text/javascript">
function checkAll(fmobj, chkAll){
    for (var i=0;i<fmobj.elements.length;i++){
        var e = fmobj.elements[i];
        if ((e.name != 'chk_all') && (e.type=='checkbox') && (!e.disabled)){
            e.checked = fmobj.chk_all.checked;
        }
    }
}//End of function checkAll(fmobj, chkAll){...

function checkOrderCheckBox(fmobj, chkAll, orderNumber){    
    for (var i=0;i<fmobj.elements.length;i++){
        var e = fmobj.elements[i];        
        if ((e.name == 'chk_apps_'+orderNumber+'[]') && (e.type=='checkbox') && (!e.disabled)){
            e.checked = document.getElementById('chk_order_'+orderNumber).checked;            
        }
    }
    validateOrderCheckBox();    
}//End of function checkOrderCheckBox(fmobj, chkAll, orderNumber){...

function checkAppsCheckBox(fmobj, orderNumber, appId){
    
    var appCheckbox = document.getElementsByName('chk_apps_'+orderNumber+'[]');
    var appLength = appCheckbox.length
    var count = 0;
    for(var i=0;i< appLength;i++){         
         if(appCheckbox[i].checked){
             count = count + 1;
         }
    }
    if(appLength == count){
        $('#chk_order_'+orderNumber).attr('checked','checked');
    }else{
        $('#chk_order_'+orderNumber).attr('checked','');
    }
    validateOrderCheckBox();
}//End of function checkAppsCheckBox(fmobj, orderNumber, appId){...

function validateOrderCheckBox(){
    var appCheckbox = document.getElementsByName('chk_order[]');
    var appLength = appCheckbox.length;
    var count = 0;
    for(var i=0;i< appLength;i++){
         if(appCheckbox[i].checked){
             count = count + 1;
         }
    }
    if(appLength == count){
        $('#chk_all').attr('checked','checked');
    }else{
        $('#chk_all').attr('checked','');
    }
}

function validateForm(fmobj, chkAll){
 $("#err_comment").html('');
 $('#err_chkbx').html("");
 var orderNo = $('#order_no').val();
 if(orderNo == ''){
    alert("Please enter SW Global LLC order number(s)");
    $('#order_no').focus();
    return false;
 }

if($.trim($('#comment').val())=='')
{
  $("#err_comment").html('Please enter Reason for Chargeback.');
  document.getElementById('comment').focus();
  return false;
}

}

 var actionUrl = '<?php echo url_for('supportTool/multipleOrderChargeBack'); ?>';
        $("#chargeBack").click(function(){
            alert("A");
            document.order_search_form.action =  actionUrl;
            document.order_search_form.submit();
        });


</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Search NIS Application By SW Global LLC Order Number(s)'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>

      
    <form name='order_search_form' id='order_search_form' action='<?php echo url_for('supportTool/chargeBackOrder');?>' method='post' class="dlForm" onsubmit="return validateForm(document.order_search_form,'chk_fee[]')">

        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('notice')){ ?>
        <div id="flash_notice" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('notice'));
          ?>
        </div><br/>
        <?php }?>


          <?php
          $sf = sfContext::getInstance()->getUser();
          if($sf->hasFlash('error')){ ?>
          <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
          </div><br/>
          <?php }?>

          <div class="clear"></div>
          <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <?php
               $val = '';
              echo formRowComplete('Order Number<span style="color:red">*</span>',input_tag('order_no', $ipayOrderNo, array('size' => 80, 'maxlength' => 80,'class'=>'txt-input'))); 

              echo "";
              echo formRowComplete('Reason For Chargeback'.'<span style="color:red">*</span>',textarea_tag('comment', '', array('class=FieldInput')),'','','err_comment');
              echo "";
              ?>
              <tr>
              <td>&nbsp;</td>
              <td><div class="lblButton">
                  <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                </div>
                <div class="lblButtonRight">
                  <div class="btnRtCorner"></div>
              </div></td>
            </tr>
          </table>
          <?php if($isFound) { 
        $i = 1;
        ?>
           <?php if($notRefundMessage != '') {?>
        <div class ="alertBox">
        <?php echo "Note: ".$notRefundMessage; ?>
        </div>
        <?php } ?>
        <br/>
      <div class="clear"></div>

        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
          <tr>
            <td class="blbar" colspan="13" align="right">
            <div style="float:left">NIS Application Details By SW Global LLC Order Number(s)</div>
            
          </tr>
          <tr>
            <td width="2%" ><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.order_search_form,'chk_order[]');"></span></td>
            
            
            <td colspan ="2" width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
            <td width="12%"><span class="txtBold">Reference No</span></td>
            <td width="14%"><span class="txtBold">Applicant Name</span></td>
            <td width="9%"><span class="txtBold">Date of Birth</span></td>
            <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
            <td width="6%"><span class="txtBold">Paid On</span></td>
            <td width="10%"><span class="txtBold">Email Id</span></td>
            <td width="10%"><span class="txtBold">Order Number</span></td>
            <td width="10%"><span class="txtBold">Payment Gateway</span></td>
            <td width="10%"><span class="txtBold">Country</span></td>
          </tr>
          
          <tbody>
            <?php $showButtons = false;
            $i =1;
            $isVettedFlag = false ;
            if(count($ipayNisDetailsQuery)>0){
               $j = 0;
                foreach ($ipayNisDetailsQuery as $result){
                  

                    foreach($result AS $ordernumber => $data){

                        $orderData = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($ordernumber);
                        if(count($orderData) > 0){
                            $orderStatus = $orderData[0]['payment_status'];
                        }else{
                            $orderStatus = 0;
                        }

                        $flag = false;
                        $app = array();?>
                        <tr>
                            <td width="2%" ><?php if($orderStatus == 0) { ?><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_order[]" id="chk_order_<?php echo $ordernumber;?>" value="<?php echo $ordernumber;?>" onclick="checkOrderCheckBox(document.order_search_form, 'chk_fee[]', '<?php echo $ordernumber; ?>');"   ></span><?php } ?></td>
                            <td width="10%" colspan="12"><span class="txtBold" id ="order_no">Order No: <?php echo $ordernumber;?></span></td>
                        </tr>
                        <?php  
                        $j = 1;
                        foreach($data AS $values){

                            $app['passport_id'] = $values['passport_id'];
                            $app['visa_id'] = $values['visa_id'];
                            $app['freezone_id'] = $values['freezone_id'];
                            $app['visa_arrival_program_id'] = $values['visa_arrival_program_id'];


                            ## Fetching application details...
                            $appDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($app);
                            
                            
                            ## Fetching application processing country...
                            $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails['id'], $appDetails['app_type']);
                            //$appDetails['country_id']
                            if(strtolower($appDetails['app_type']) != 'vap' && $processingCountry == 'CN'){
                                $currency_id = CurrencyManager::getCurrency($processingCountry);
                                if(isset($appDetails['convert_amount'])){
                                    $appAmount = $appDetails['convert_amount'];
                                }else{
                                    $appAmount = $appDetails['amount'];
                                }
                            }else{
                                $appAmount = $appDetails['amount'];
                                $currency_id = 1; // means dollar...
                            }

                              ?>
                              <tr>
                                <td></td>
                                <td width="2%"><span><?php if($appDetails['status']!="New") { ?> <input type="checkbox" name="chk_apps_<?php echo $ordernumber; ?>[]" id="<?php echo "chk_fee".$appDetails['id']; ?>" value="<?= $appDetails['id']."_".$appDetails['app_type']."_".$appAmount."_".$currency_id;?>" OnClick="checkAppsCheckBox(document.order_search_form, '<?php echo $ordernumber; ?>',    '<?php echo $appDetails['id']; ?>');" > <?php } ?></span></td>
                                <td width="9%"><span><?php if($appDetails['app_type'] == 'vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($appDetails['app_type']);} ?></span></td>
                                <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
                                <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
                                <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
                                <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'Y-m-d');?></span></td>
                                <?php if(isset ($fStatus) && $fStatus!=null){ ?>
                                <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
                                <?php } else{ $showButtons = true;?>
                                <td width="9%"><span><?php echo $appDetails['status'];?></span></td>
                                <?php  }  ?>
                                <td width="9%"><span><?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'Y-m-d'); else echo "--"; ?></span></td>
                                <td width="10%"><span><?php echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
                                <td width="10%"><span><?php echo $ordernumber;?></span></td>
                                <td width="10%"><span><?php echo $gatewayName;?></span></td>
                                <td width="10%"><span><?php echo ($processingCountry != '')?$processingCountry:'--';?></span></td>
                            </tr>

                    <?php $j++;
                            }////End of foreach($data AS $values){...
                            
                            
                        }//End of foreach($result AS $ordernumber => $data){...

                $i++;
                
                        
               }//End of foreach ($ipayNisDetailsQuery as $result){...
              
            
             ?>
            <tr>
              <td class="blbar" colspan="13" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  //echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?order_no='.$ipayOrderNo.'&usePager=true','detail_div');
                  ?>
                </div>
              </td>
            </tr><?php } else { ?>
            <tr><td  align='center' class='red' colspan="13">No Results found</td></tr>
            <tr><td class="blbar" colspan="13" height="25px" align="right"></td></tr>
            <?php } ?>
          </tbody>

        </table>
        <br>

        <?php if($showButtons)
  {
     if(count($ipayNisDetailsQuery)>0)
    {?>
        <div align="center">
            <input style="" type="button" class="normalbutton" id="chargeBack" name="chargeBack" value="Chargeback" onclick="FormSubmit();"/>
        </div>
         <?php }

          }
   } ?>

      <br>
      <br>
      <br>

      <div id="result_div" style="width:500px;" align="center"></div>
      <div id="application_div" style="display:none;"></div>
</form>
</div>
</div>

<div class="content_wrapper_bottom"></div>

<script>
 

function FormSubmit()
{

    var appCheckbox = document.getElementsByName('chk_order[]');
    var appLength = appCheckbox.length;
    var count = 0;
    for(var i=0;i< appLength;i++){
         if(appCheckbox[i].checked){
             count = count + 1;
         }
    }
    if(count == 0){
        alert("Please Select atleast one Order Number to Chargeback");
        return false;
    }

    var actionUrl = '<?php echo url_for('supportTool/multipleOrderChargeBack'); ?>';                    
    document.order_search_form.action =  actionUrl;
    document.order_search_form.submit();


}
</script>



