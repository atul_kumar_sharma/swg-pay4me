
<?php  //echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>

<script>
  function getOrderDetails(orderNo){
    // alert('<?= url_for("supportTool/searchOrderNo?order_no="); ?>'+orderNo);
    window.location = '<?= url_for("supportTool/searchOrderNo?order_no="); ?>'+orderNo;
  }
</script>

<br>
<div class="wrapTable">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
            <tr>
                <td class="blbar" colspan="12" align="right">
                <div style="float:left">Duplicate NIS Application</div>
                <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td> 
            </tr>
            <tr>
                <td width="4%" ><span class="txtBold">S. No.</span></td>
                <td width="9%"><span class="txtBold">Application type</span></td>
                <td width="9%"><span class="txtBold">App Id</span></td>
                <td width="12%"><span class="txtBold">Reference No</span></td>
                <td width="14%"><span class="txtBold">Applicant Name</span></td>
                <td width="9%"><span class="txtBold">Date of Birth</span></td>
                <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
                <td width="6%"><span class="txtBold">Paid On</span></td>
                <td width="10%"><span class="txtBold">Email Id</span></td>
                <td width="6%"><span class="txtBold">Order Number</span></td>
                <td width="6%"><span class="txtBold">Payment Gateway</span></td>
                <td width="6%"><span class="txtBold">Country</span></td>
            </tr>
          <tbody>
            <?php
            $i = 1;
            $ipay4meOrderDetails = array();
            //$url = url_for("supportTool/searchOrderNo?order_no=");
            if($pager->getNbResults()>0)
            {
            foreach ($pager->getResults() as $appDetails):
            if($appDetails['order_no']!=0)
            {
            $index = strpos($appDetails['order_no'], ",");
            if($index){
              $appOrderDetails = substr($appDetails['order_no'], 0,$index);
            }else if($appDetails['order_no']!=0){
              $appOrderDetails = $appDetails['order_no'];
            }
            if($appOrderDetails!=0)
            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
            }else{
              $appOrderDetails=0;
            }

            $finalStatus = $nisHelper->isAllReadyRefunded($appOrderDetails);
            $finalStatus= $finalStatus->getRawValue();
            $fStatus = null;
            if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
              $fStatus = $finalStatus['action'];
            }


            ## Fetching application processing country...
            $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails['app_id'], $appType);
//            echo $appDetails['order_no'];die;
//            echo "<pre>";print_r($appDetails);die;
////            echo "<pre>";print_r(get_class_methods($result));die;
//            $app['passport_id'] = $result['passport_id'];
//            $app['visa_id'] = $result['visa_id'];
//            $app['freezone_id'] = $result['freezone_id'];
//            $appDetails = $appObj->getApplicationDetails($app);
////            echo "<pre>";print_R($appDetails);die;
//            $i = 1;
//            foreach ($duplicateAppArr as $k=>$appDetails):

            ?>
            <tr>
                <td width="4%"><?php echo $i;?></td>
                <td width="9%"><span><?php  if($appType=='vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($appType) ; }?></span></td>
                <td width="10%"><span><?php echo $appDetails['app_id'];?></span></td>
                <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
                <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
                <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'd-m-Y');?></span></td>
                <?php if(isset ($fStatus) && $fStatus!=null){ ?>

                <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
                <?php } else{ ?>

                 <td width="9%"><span><?php echo $appDetails['status'];?></span></td>

                <?php  } ?>

                <td width="9%"><span><?php if($appDetails['paid_on']!='') echo date_format(date_create($appDetails['paid_on']), 'd-m-Y'); else echo "--"; ?></span></td>
                <?php if(isset ($appOrderDetails) && $appOrderDetails!=0) { ?>
                <td width="10%"><span><?php  echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
                <td width="9%" align="center"><span><a href="<?= 'searchOrderNo?order_no='.Settings::encryptInput($appOrderDetails); ?>" id="order_detail" title="Show Order Details"><?php  echo $appOrderDetails;?></a></span></td>
                <?php $gatewayName = $gatewayObj->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);     ?>

                <td width="10%"><span><?php  echo $gatewayName ; ?></span></td>                
                <?php } else { ?>
                <td width="10%"><span>Not Applicable</span></td>
                <td width="10%"><span>Not Applicable</span></td>
                <td width="10%"><span>Not Applicable</span></td>
                <?php } ?>
                <td width="10%"><span><?php  echo $processingCountry ; ?></span></td>



            </tr>
            <?php
            $i++;
            endforeach; ?>

            <tr>
                <td class="blbar" colspan="12" height="25px" align="right">
                <div class="paging pagingFoot">
                    <?php
                    echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?id='.$sf_params->get('id').'&appType='.$sf_params->get('appType').'&usePager=true&refNo='.$refNo,'application_div');
                    ?>
                    </div>
                </td>
            </tr>
            <?php } else { ?>
                    <tr><td  align='center' class='error' colspan="12">No Results found</td></tr>
            <?php }?>
          </tbody>
        </table>
    <?php
    
    if(!isset($backVar))
    {?>
	<div class="clear">&nbsp;</div>
    <center>
     <input type="button" value="Back" id="Back" name="Back" class="normalbutton" onclick="window.location.href='<?php echo url_for('supportTool/searchByAppId?app_id='.$sf_params->get('id').'&ref_no='.$refNo.'&commit=Search')?>'" />
    </center>
     <?php } ?>
</div>



