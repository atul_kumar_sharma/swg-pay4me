<?php use_stylesheet('style.css');
echo include_stylesheets(); ?>
<h2 style="font-size:18px;"> View Details </h2>
<div class="clearfix">
  <div id="nxtPage">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
        <td class="blbar" colspan="5" height="25px" align="right"></td>
      </tr>
      <tr><?php echo formRowComplete('<b>Order Number</b>', $recordDetails[0]['order_number'],'','ord','','ord_row'); ?></tr>
      <tr><?php echo formRowComplete('<b>Amount</b>', $recordDetails[0]['amount'],'','ord','','ord_row'); ?></tr>
      <tr><?php echo formRowComplete('<b>Refund Type</b>', $recordDetails[0]['refund_type'],'','ord','','ord_row'); ?></tr>
      <tr><?php echo formRowComplete('<b>Refund Description</b>', $recordDetails[0]['refund_desc'],'','ord','','ord_row'); ?></tr>
      <tr><?php echo formRowComplete('<b>Message</b>', $recordDetails[0]['message'],'','ord','','ord_row'); ?></tr>
      <tr>
        <td class="blbar" colspan="5" height="25px" align="right"></td>
      </tr>
    </table>
    <br>
  </div>
</div>

<script>
  window.opener.refreshParent();
</script>