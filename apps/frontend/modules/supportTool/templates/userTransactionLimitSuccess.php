<!--
New Template added to list user transaction details
-->
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'User Transaction Limit'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
  <div id="nxtPage"> <?php echo form_tag('supportTool/userTransactionLimit',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form', 'onsubmit' => 'return frmValidate()')) ?>
            <?php
            $sf = sfContext::getInstance()->getUser();
            if($sf->hasFlash('notice')){ ?>
            <div id="flash_notice" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('notice'));
                ?>
    </div>
    <br/>
            <?php }?>
                <?php
                $sf = sfContext::getInstance()->getUser();
                if($sf->hasFlash('error')){ ?>
                <div id="flash_error" class="alertBox" >
                    <?php
                    echo nl2br($sf->getFlash('error'));
                    ?>
    </div>
    <br/>
                <?php }?>
                <table width="100%">
                    <?php
                    echo formRowComplete('First Name',input_tag('fname',$fname , array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                    echo "</tr>";
                    echo formRowComplete('Last Name',input_tag('lname', $lname, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                    echo "</tr>";
                    echo formRowComplete('Email',input_tag('email', $email, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                    echo "</tr>";
                    ?>
                    <tr>
                        <td class="ltgray">User List</td>
                        <td>
                            <input type="radio" class="txt-input" name="userList" id="userList" value="utl" checked="checked">Updated Users&nbsp;&nbsp;&nbsp;
                            <input type="radio" class="txt-input" name="userList" id="userList" value="nutl" <?php if($userList == 'nutl'){ echo "checked"; } else { echo ""; } ?> >All Users
                        </td>
                    </tr>

                    
                    <tr valign="top" >
        <td height="30" valign="top">&nbsp;</td>
        <td height="30" valign="top"><?php echo submit_tag('Search',array('class' => 'normalbutton')); ?> </td>
                    </tr>
                </table>
        </form>
	<div class="clear">&nbsp;</div>
        <table width="100%">
            <tr>
        <td class="blbar" colspan="9" align="right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
            </tr>
            <tr>
                <td width="5%"><span class="txtBold">S. No.</span></td>
                <td width="15%"><span class="txtBold">First Name</span></td>
                <td width="15%"><span class="txtBold">Last Name</span></td>
                <td width="20%"><span class="txtBold">Email</span></td>
                <td width="15%"><span class="txtBold">Mobile Number</span></td>
                <td width="10%"><span class="txtBold">Last Login</span></td>
                <td width="10%"><span class="txtBold">Login Type</span></td>
                <td width="5%"><span class="txtBold">Action</span></td>
            </tr>
            <?php
            if(($pager->getNbResults())>0) {
                $limit = sfConfig::get('app_records_per_page');
                $page = $sf_context->getRequest()->getParameter('page',0);
                $i = max(($page-1),0)*$limit ;
                $arrActiveUser =array();
                if(!empty($arrActiveUserId)) {
                    foreach ($arrActiveUserId as $val) {
                        $arrActiveUser[] = $val['user_id'];
                    }
                }

                ?>
                <?php
                foreach ($pager->getResults() as $result):

                
                $i++;
                ## Fetching login type...
                $userType = Settings::getLoginType($result->getUsername());
                ?>
            <tr>
                <td><?php echo $i; ?></td>
        <td ><span>
          <?php if($result->sfGuardUser->UserDetail->getFirstName() !='') {echo $result->sfGuardUser->UserDetail->getFirstName();}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->sfGuardUser->UserDetail->getLastName() !=''){echo $result->sfGuardUser->UserDetail->getLastName();}else{echo "--";}?>
          </span></td>
        <td ><span >
          <?php if($result->sfGuardUser->UserDetail->getEmail() !=''){echo $result->sfGuardUser->UserDetail->getEmail();}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->sfGuardUser->UserDetail->getMobilePhone() !=''){echo $result->sfGuardUser->UserDetail->getMobilePhone();}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->getLastLogin() !=''){echo date_format(date_create($result->getLastLogin()),'Y-m-d');}else{echo "--";}?>
          </span></td>
        <td><span >
          <?php if($result->getUsername() !=''){echo $userType;}else{echo "--";}?>
          </span></td>
        <td><?php
                            $user_id = $result->getUserId();
                            $encriptedUser_id = SecureQueryString::ENCRYPT_DECRYPT($user_id);
                            $encriptedUser_id = SecureQueryString::ENCODE($encriptedUser_id);
                    ?>
                    <?php 
                    if($result->getUserIdUserTransaction()) {?>                    
          <a href="<?php echo url_for('supportTool/editUserTransactionLimit?uid='.$encriptedUser_id.'&page='.$page) ?>"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></a>          
                    <?php }else{ ?>
          <a href="<?php echo url_for('supportTool/addUserTransactionLimit?uid='.$encriptedUser_id.'&page='.$page) ?>"><img src="<?php echo image_path('add_icon.png'); ?>" alt="Add" title="Add" /></a>
                    <?php } ?>
                    
                </td>
            </tr>
            <?php endforeach; ?>
            <tr>
        <td class="blbar" colspan="9" height="25px" align="right"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?fname='.$fname.'&lname='.$lname.'&email='.$email.'&usePager=true'.'&back=1') ?></td>
            </tr>
            <?php
        }else { ?>
      <tr>
        <td  colspan="9" align='center' ><div class="red"> No record found</div></td>
      </tr>
            <?php } ?>
        </table>
    </div>
</div>
</div><div class="content_wrapper_bottom"></div>

<div id="popupContent"></div>
<div id="backgroundPopup"></div>
<div id="loading" class="no_display"><?php echo image_tag('/images/ajax-loader_1.gif',array('alt'=>'Loading ...', 'align' => 'absmiddle'));?><br/>&nbsp;&nbsp;Please wait...</div>

<script>
    function frmValidate(){

        var radioVal = $("input[type='radio'][name='userList']:checked").val();
        if(radioVal == 'nutl'){

            var fname = $.trim($('#fname').val());
            var lname = $.trim($('#lname').val());
            var email = $.trim($('#email').val());

            if(fname == '' && lname == '' && email == ''){
                alert("Please input data to search in user list.")
                return false;
            }
        }
    }
</script>