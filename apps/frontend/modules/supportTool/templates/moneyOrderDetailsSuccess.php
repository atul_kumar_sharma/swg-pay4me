<?php
if($moneyOrderDetails['currency'] == '5'){
    $moneyOrderAmount = $moneyOrderDetails['convert_amount'];
}else{
    $moneyOrderAmount = $moneyOrderDetails['amount'];
}
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($moneyOrderDetails['currency']);
?>
<div class="clear"></div>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Money Order Details'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div>
    <br/>
    <?php }?>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
    </div>
    <br/>
      <?php }?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
        <tr>
          <td class="blbar" colspan="11" align="left">Money order information</td>
        </tr>
        <tr>
        <td width="50%"><table width="99%" cellpadding="0" cellspacing="0" border="0">
              <tr>
              <td width="30%"> Money Order Serial Number </td>
              <td width="30%"><?= $moneyOrderDetails['moneyorder_number']?>
                </td>
              </tr>
              <tr>
              <td> Money Order Date </td>
              <td><?= $moneyOrderDetails['moneyorder_date']?>
                </td>
              </tr>
          </table></td>
        <td width="50%"><table width="99%" cellpadding="0" cellspacing="0" border="0">
              <tr>
              <td width="30%"v> Money Order Amount(<?php echo html_entity_decode($currencySymbol); ?>) </td>
              <td width="30%"><?php echo $moneyOrderAmount; ?>
                </td>
              </tr>
              <tr>
              <td colspan="2">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
      <br/>
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
        <tr>
          <td class="blbar" colspan="11" align="left">Tracking Information</td>
        </tr>
        <tr>
          <td width="15%"><span class="txtBold">Tracking Number</span></td>
          <td width="15%"><span class="txtBold">Order Number</span></td>
          <td width="15%" align="center"><span class="txtBold">Application Detail</span></td>
          <td width="10%" align="right"><span class="txtBold">Amount(<?php echo html_entity_decode($currencySymbol); ?>)</span></td>
        </tr>
        <tbody>
          <?php if(count($applicationDetails)>0) { ?>
          <?php  foreach ($applicationDetails as $records){ ?>
          <tr>
          <td valign="top"><?= $records['tracking_number']?>
            </td>
          <td valign="top"><?php if($order_number == $records['order_number']) {?>
            <span class="highlight yellow" >
            <?= $records['order_number']?>
            </span>
              <?php }else{ ?>
              <?= $records['order_number']?>
              <?php }?>
            </td>
          <td><table>
                <tr style="background-color: rgb(229, 236, 249);">
                  <td width="15%"><span class="txtBold">Application Id</span></td>
                  <td width="15%"><span class="txtBold">Application Type</span></td>
                  <td width="15%"><span class="txtBold">Reference Number</span></td>
                </tr>
                <?php foreach($records['app_detail'] as $detail){?>
                <tr>
                <td width="15%"><a href="#" onclick="findAppDetail('<?php echo $detail['app_id']; ?>','MO','<?= $records['order_number']?>', '<?=$detail['app_type'] ?>')">
                  <?= $detail['app_id']?>
                  </a></td>
                  <td width="15%"><?php if($detail['app_type']=='vap')
                  { echo sfConfig::get('app_visa_arrival_title') ; } else { echo $detail['app_type']; }?></td>
                  <td width="15%"><?= $detail['ref_no']?></td>
                </tr>
                <?php } ?>
            </table></td>
          <td align="right"><?php echo $records['cart_amount']; ?>
            </td>
          </tr>
          <?php } ?>
        <?php } else{ ?>
                              <tr>
          <td align="center" class="red" colspan="4"> No tracking number found. </td>
                              </tr>
                              <?php } ?>
        </tbody>
      </table>
  <br>
  <br>
    <div id="result_div" class="no_display" align="center"></div>
  <div id="application_div"></div>
	<div class="clear">&nbsp;</div>
    <div align="center">
      <?php if($back !=1) {?>
      <?php //echo url_for('supportTool/searchByMoneyOrder');?>
      <a href ="javascript:void(0);" class="normalbutton" onclick="history.go(-1);">Back</a>
      <?php }else {?>
      <a href ="<?php echo url_for('supportTool/moneyOrderReport?back=1&page='.$page);?>" class="normalbutton noPrint">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
      <?php } ?>
      <input type="button" class="normalbutton noPrint" onclick="javaScript:window.print();" name="Print" value="&nbsp;&nbsp;Print&nbsp;&nbsp;">
    </div>
    </div>
    </div>
<div class="content_wrapper_bottom"></div>
<!--
</div>-->
<script>
  function   findAppDetail(appId,MO,orderNumber, appType){
    var url = "<?= url_for("supportTool/searchApplication"); ?>";

    $("#application_div").hide();
    $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $("#result_div").show();

    $.get(url, { app_id: appId,template: MO,orderNumber: orderNumber, apptype : appType},
    function(data){
      if(data == "logout"){
        location.reload();
      }else
      {
        var trimdata = jQuery.trim(data);
        if(trimdata.substring(0, 7) == '<style>')
        {
          location.reload();
        }else{
          //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
          $("#application_div").html(data);
          $("#application_div").show();
          $("#application_div").focus();
          $("#result_div").hide();
        }
      }
      //window.location.href = "URL";
      //setTimeout( "refresh()", 2*1000 );
    });

  }
</script>
