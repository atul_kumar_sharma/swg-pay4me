
<script>
  function validateForm()
  {

    if(document.getElementById('apptype').value == '')
        {
            alert('Please select Application Type');
            document.getElementById('apptype').focus();
            return false;
        }
     
    if(document.getElementById('app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('app_id').focus();
      return false;
    }

    if(document.getElementById('app_id').value != "")
    {
      if(isNaN(document.getElementById('app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('app_id').value = "";
        document.getElementById('app_id').focus();
        return false;
      }

    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search Application by Application Id'));
?>
  <div class="clear"></div>
<div class="tmz-spacer"></div>
<?php echo form_tag('supportTool/searchApplication',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php

}
$app_type = array('' => ' Please Select Application Type ','visa' => 'Visa/Free Zone', 'passport' => 'Passport','vap'=>sfConfig::get('app_visa_arrival_title'));
?>


            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <?php
               
                $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
                
                echo formRowComplete('NIS Application Type<span class="red">*</span>',select_tag('apptype', options_for_select($app_type, array('class'=>'txt-input'))));
                echo formRowComplete('NIS Application Id<span class="red">*</span>',input_tag('app_id', $app_id, array('size' => 24, 'maxlength' => 20, 'class'=>'txt-input'))); ?>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>

<?php /*
<div class="multiForm dlForm">
  <form name='editForm' action='<?php echo url_for('supportTool/searchApplication');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      
      <dl>
        <dt><label>Application ID<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['app_id']))?$_POST['app_id']:"";
          echo input_tag('app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autoComplete'=>"off")); ?>
        </dd>
      </dl>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'></center>
    </div>
  </form>
</div>
*/ ?>
</div></div>
<div class="content_wrapper_bottom"></div>