<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
  <?php use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Update Application Details'));
?>

 <?php if(isset($data) && $data != ''){   if($data['status'] == 'Vetted' || $data['status'] == 'Approved' || $data['status'] == 'Rejected'){  echo '<div class="alertBox">'; echo "This Application can not be edit due  to its ".$data['status']." status." ; echo '</div>'; } } ?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<?php echo form_tag('supportTool/nisApplicationDetails',array('name'=>'search_form','class'=>'', 'method'=>'post','id'=>'search_form','onSubmit'=>'return validateForm()')) ?>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
<?php
    echo nl2br($sf->getFlash('notice'));
?>
</div><br/>
<?php }
          $app_array = array('' => ' Please Select Application Type ','visa' => 'Visa/Free Zone', 'passport' => 'Passport','vap'=>sfConfig::get('app_visa_arrival_title')); ?>
          <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <?php
//                $applicationType = array();

                $selected = (isset($appType)?$appType:'');
                $app_id = (isset($appType)?$appId:"");
                $ref_no = (isset($refNo)?$refNo:"");

                echo formRowComplete('NIS Application Type<span class="red">*</span>',select_tag('app_type', options_for_select($app_array,$selected, array('class'=>'txt-input'))));
                echo formRowComplete('NIS Application Id<span class="red">*</span>',input_tag('app_id', $app_id, array('size' => 24, 'maxlength' => 20, 'class'=>'txt-input'))); 
                echo formRowComplete('NIS Reference Number <span class="red">*</span>',input_tag('ref_no', $ref_no, array('size' => 24, 'maxlength' => 20, 'class'=>'txt-input'))); ?>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                            <?php echo submit_tag('Search',array('class' => 'normalbutton')); ?>
                        </div>
                    <div class="lblButtonRight">
                      <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table><br/>
    <?php if(isset($data) && $data != '') { ?>
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
             <tr>
                <td class="blbar" colspan="14" align="left">Application Details
             </tr>               
                 <tr>
                 <td><span class="txtBold">Application Type</span></td>
                 <td><span class="txtBold">Application Id</span></td>
                 <td><span class="txtBold">Reference Number</span></td>
                 <td><span class="txtBold">First Name</span></td>
                 <td><span class="txtBold">Middle Name</span></td>
                 <td><span class="txtBold">Last Name</span></td>
                 <td><span class="txtBold">Date of Birth(DD/MM/YY)</span></td>
                 <td><span class="txtBold">Processing Country</span></td>                 
                 <td><span class="txtBold"><?php if($data['app_type'] == 'vap') { ?>Processing Center<?php } else { ?> Embassy<?php } ?> </span></td>
                 <?php if($data['app_type'] != 'vap'){ ?>
                 <td><span class="txtBold">Interview Date(DD/MM/YY)</span></td>
                 <?php }  ?>
                 <td><span class="txtBold">Eye Color</span></td>
                 <td><span class="txtBold">Hair Color</span></td>
                 <td><span class="txtBold">Payment Status</span></td>
                 <?php if($data['status'] == 'New' || $data['status'] == 'Paid'){  ?>
                 <td><span class="txtBold">Action</span></td>
                 <?php } ?>
                 </tr>
                 <tr>
                 <?php if(!empty($data)){ ?>
                 <td><?php if($data['app_type'] == 'vap') { echo sfConfig::get('app_visa_arrival_title'); } else { echo ucwords($data['app_type']); } ?> </td>
                 <td><?php echo $data['id'];?> </td>
                 <td><?php echo $data['ref_no'];?> </td>
                 <td><?php echo $data['first_name'];?> </td>
                 <td><?php echo $data['mid_name'];?> </td>
                 <td><?php echo $data['last_name'];?> </td>
                 <td><?php echo date_format(date_create($data['date_of_birth']),'d-m-Y');?> </td>
                 <td><?php echo $data['country_name'];?> </td>
                 <td><?php echo $data['embassy_name'];?> </td>
                 <?php if($data['app_type'] != 'vap'){
                 $interviewDate = isset($data['interview_date'])?date_format(date_create($data['interview_date']),'d-m-Y'):'N/A'; ?>
                 <td><?php echo $interviewDate; ?> </td>
                 <?php } ?>
                 <td><?php echo  $data['color_eyes_id'];?> </td>
                 <td><?php echo $data['color_hair_id'];?> </td>
                 <td><?php echo $data['status'];?> </td>
                 <?php if($data['status'] == 'New' || $data['status'] == 'Paid'){  ?>
                 <td><a href="<?php echo url_for('supportTool/editApplicationDetails?app_id='.Settings::encryptInput($data['id']).'&app_type='.Settings::encryptInput($data['app_type']).'&ref_no='.Settings::encryptInput($data['ref_no']))?>"><img title="Edit" alt="Edit" src="<?php echo image_path('edit_icon.png')?>"></a></td>
                 <?php } } else {?>
                 <td colspan="12" align="center" class="red"> No Result Found </td>
                 <?php } ?>
                 </tr>
       </table>
        
        
        
     <?php  } ?>

 </div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
  function validateForm()
  {

    if(document.getElementById('app_type').value == '')
        {
            alert('Please select Application Type');
            document.getElementById('app_type').focus();
            return false;
        }

    if(document.getElementById('app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('app_id').focus();
      return false;
    }else if (isNaN(document.getElementById('app_id').value)){
      alert('Please insert numeric Application Id.');
      document.getElementById('app_id').focus();
      return false;
    }

    if(document.getElementById('ref_no').value == "")
    {    
        alert('Please insert Reference Number');
        document.getElementById('ref_no').value = "";
        document.getElementById('ref_no').focus();
        return false;
    }else if(isNaN(document.getElementById('ref_no').value)){
      alert('Please insert numeric Reference Number');
      document.getElementById('ref_no').focus();
      return false;

    }
  }

</script>
