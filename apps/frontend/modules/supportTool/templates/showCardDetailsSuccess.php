<?php
/**
 * Template to display Application Listing for the order number selected.
 */

use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Card Details '));

?>
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="global_content4 clearfix">

  <div class="clearfix" style="padding-bottom:20px">
    <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
      <tr>
        <td class="blbar" colspan="9" align="right">
        <div style="float:left">Summary</div>

      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Card First</span></td>
        <td width="9%"><span><?php echo $orderRequestDetailByOrder[0]['card_first'];?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Card Last</span></td>
        <td width="9%"><span><?php echo $orderRequestDetailByOrder[0]['card_last'];?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Card Hash</span></td>
        <td width="9%"><span><?php echo "<font color ='red'>".$orderRequestDetailByOrder[0]['card_hash']."<font>";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Successive Failure</span></td>
        <td width="9%"><span><?php echo $successiveFailure;?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Last Decline Transaction</span></td>
        <td width="9%"><span><?php echo $lastFailureDate;?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is Verified</span></td>

        <?php  if($epVbvResponse[0]['three_ds_verificaion']=='y')
        { ?>

        <td width="9%"><span><?php echo "Verified";?></span></td>
        <?php }
       else if($epVbvResponse[0]['three_ds_verificaion']=='N') { ?>
        <td width="9%"><span><?php echo "N/A"; ?></span></td>
        <?php }
      else { ?>
        <td width="9%"><span><?php echo "Not Verified"; ?></span></td>
        <?php } ?>
      </tr>


      <tr>
        <td width="4%" ><span class="txtBold">Last Transaction</span></td>
        <td width="9%"><span><?php echo $arrTransactionDetail['last_transaction'];?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Next Allowed Transaction</span></td>
        <?php
        if($date_flg == 0)
        {
          $date = $arrTransactionDetail['last_transaction'];
          $dd = date('d',strtotime($date));
          $mm = date('m',strtotime($date));
          $yyyy = date('Y',strtotime($date));
          $hh = date('h',strtotime($date));
          $min = date('i',strtotime($date));
          $ss = date('s',strtotime($date));
          $nextDate = date('Y-m-d h:i:s',mktime($hh,$min+$time,$ss,$mm,$dd,$yyyy));
        }
        if($date_flg == 1)
        {
          $nextDate = $arrTransactionDetail['last_transaction'];
        }
        else if($date_flg == 2)
        {
            $nextDate = 'N/A';
        }
        
        else
        {
          $nextDate = $arrTransactionDetail['last_transaction'];
        }
        ?>
        <td width="9%"><span><?php echo $nextDate;?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Gateway</span></td>
        <td width="9%"><span><?php echo $arrTransactionDetail['gateway_name']?></span></td>
      </tr>

      <tr>
        <td width="4%" ><span class="txtBold">Is Registered</span></td>
        <td width="9%"><span>
            <?php if($date_flg == 0)
            {
              echo "No";
            }else if($date_flg == 2){
              echo "N/A";
            }
            else
            {
                echo 'Yes';
            }
            ?>
        </span></td>
      </tr>
      <?php

      $fpsId =  $arrTransactionDetail['fps_rule_id'];
      //      $reason = fps_error($fpsId);
      ?>

      <?php if($fpsId=='4' || $fpsId=='5')
      { ?>
      <tr>
        <td width="4%" ><span class="txtBold">Is Blocked</span></td>
        <td width="9%"><span><?php echo "<font color='red'>Yes</font>";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is allowed for transaction</span></td>
        <td width="9%"><span><?php echo "No";?></span></td>
      </tr>
      <?php }
     else if ($fpsId=='N')
    {
      ?>
      <tr>
        <td width="4%" ><span class="txtBold">Is Blocked</span></td>
        <td width="9%"><span><?php echo "N/A";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is allowed for transaction</span></td>
        <td width="9%"><span><?php echo 'N/A';?></span></td>
      </tr>
      <?php }

    else
    {
      ?>
      <tr>
        <td width="4%" ><span class="txtBold">Is Blocked</span></td>
        <td width="9%"><span><?php echo "<font color='green'>No</font>";?></span></td>
      </tr>
      <tr>
        <td width="4%" ><span class="txtBold">Is allowed for transaction</span></td>
        <td width="9%"><span><?php echo "Yes";?></span></td>
      </tr>
      <?php } ?>

      <tr><td colspan="2" align="center"><center>

       <div class="lblButton" style="text-align:center;padding-left:350px">
          <a href ="<?php echo url_for('supportTool/searchByAppId?app_id='.$appid.'&ref_no='.$ref_no);?>" class="button" style="text-decoration:none">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
       </div>
       <div class="lblButtonRight">
       <div class="btnRtCorner"></div>
       </div>

        </center>
      </td></tr>



    </table>

  </div>
</div>

