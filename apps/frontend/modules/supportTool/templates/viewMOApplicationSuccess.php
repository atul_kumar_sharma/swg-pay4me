<?php use_helper('Form');

?>

    <div id="nxtPage">
  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable">
              <tr>
                <td class="blbar" colspan="11" align="left">NIS Applications1
              </tr>
              <tr>
                <td><span class="txtBold">S.No.</span></td>
                <td><span class="txtBold">Application Type</span></td>
                <td><span class="txtBold">App Id</span></td>
                <td><span class="txtBold">Reference No</span></td>
                <td><span class="txtBold">Applicant Name</span></td>
                <td><span class="txtBold">Date Of Birth</span></td>
                <td><span class="txtBold">NIS Payment Status</span></td>
                <td><span class="txtBold">Paid On</span></td>
                <td><span class="txtBold">Order Number</span></td>
              </tr>
            <tbody>
              <?php
              $i=0;
              foreach ($detailinfo as $k=>$v):
              $i++;
              ?>
              <tr>
                <td><?php echo $i;//$embassyList->getid() ?></td>
                <td><?php
                echo ($k == 'vap')?sfConfig::get('app_visa_arrival_title'):strtoupper($k) ;//$embassyList->getid() ?></td>
                <td><?php echo $v['appid'] ?></td>
                <td><?php echo $v['refno'] ?></td>
                <td><?php echo $v['name'] ?></td>
                <td><?php echo date_format(date_create($v['date_of_birth']),'Y-m-d'); ?></td>
                <td><?php echo $v['status'] ?></td>
                <td><?php if($v['paid_at']!='') echo date_format(date_create($v['paid_at']),'Y-m-d'); else echo "--"; ?></td>
                <td><?php  echo $orderNumber;?></td>
              </tr>
              <?php
              endforeach;
              
              ?>
            </tbody>
          </table>
        </div>
