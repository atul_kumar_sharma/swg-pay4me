




  
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?>
    <div id="flash_notice" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br/>
    <?php }?>


      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('error')){ ?>
      <div id="flash_error" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('error'));
        ?>
      </div><br/>
      <?php }?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
      <?php
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Wire Transfer Details'));
?>
 <div class="clear"></div>
        <div class="tmz-spacer"></div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
        <tr>
          <td class="blbar" colspan="11" align="left">Wire Transfer information</td>
        </tr>
        <tr>
          <td width="50%">
            <table style="margin:auto;" width="100%">
              <tr>
                <td width="30%">
                  Wire Transfer Serial Number
                </td>
                <td width="30%">
                  <?= $wireTransferDetails['wire_transfer_number']?>
                </td>
              </tr>
              <tr>
                <td>
                  Wire Transfer Date
                </td>
                <td>
                  <?= $wireTransferDetails['wire_transfer_date']?>
                </td>
              </tr>
            </table>
          </td>
          <td width="50%">
            <table  style="margin:auto;" width="100%">
              <tr>
                <td width="30%"v>
                  Wire Transfer Amount
                </td>
                <td width="30%">
                  <?= "$".$wireTransferDetails['amount']?>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  &nbsp;
                </td>

              </tr>
            </table>
          </td>
        </tr>
      </table>
      <br/>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable show_table" >
        <tr>
          <td class="blbar" colspan="11" align="left">Tracking Information</td>
        </tr>
        <tr>
          <td width="15%"><span class="txtBold">Tracking Number</span></td>
          <td width="15%"><span class="txtBold">Order Number</span></td>
          <td width="15%"><span class="txtBold">Application Detail</span></td>
          <td width="10%"><span class="txtBold">Amount</span></td>
        </tr>
        <tbody>
          <?php //if($record) { ?>
          <?php  foreach ($applicationDetails as $records){ ?>
          <tr>
            <td valign="top">
              <?= $records['tracking_number']?>
            </td>
            <td valign="top">
              <?php if($order_number == $records['order_number']) {?>
              <span style="font-weight:bold; background-color: yellow" ><?= $records['order_number']?> </span>
              <?php }else{ ?>
              <?= $records['order_number']?>
              <?php }?>
            </td>
            <td>
              <table>
                <tr bgcolor="#eeeeee">
                  <td width="15%"><span class="txtBold">Application Id</span></td>
                  <td width="15%"><span class="txtBold">Application Type</span></td>
                  <td width="15%"><span class="txtBold">Reference Number</span></td>
                </tr>
                <?php foreach($records['app_detail'] as $detail){?>
                <tr>
                  <td width="15%"><a href="#" onclick="findAppDetail('<?php echo $detail['app_id']; ?>','MO','<?= $records['order_number']?>','<?php echo $detail['app_type']; ?>')"><?= $detail['app_id']?></a></td>
                  <td width="15%"><?= $detail['app_type']?></td>
                  <td width="15%"><?= $detail['ref_no']?></td>
                </tr>
                <?php } ?>
              </table>

            </td>
            <td valign="top">
              <?= "$".$records['cart_amount']?>
            </td>
          </tr>
          <?php } ?>
        <?php /*} else{ ?>
                                              <tr>
                                                <td colspan="3">
                                                  No tracking number found.
                                                </td>
                                              </tr>
                                              <?php } */?>
        </tbody>
      </table>

   


  <br>
  <br>
  <div id="result_div" style="width:500px;display:none" align="center"></div>
  <div id="application_div"></div>
  <br />
  <div class="noPrint" align="center">
   
    
      <?php if($back !=1) {?>
      <a href ="<?php echo url_for('supportTool/searchByWireTransfer');?>" class="normalbutton" style="text-decoration:none">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
      <?php }else {?>
      <a href ="<?php echo url_for('supportTool/wireTransferReport?back=1&page='.$page);?>" class="normalbutton" style="text-decoration:none">&nbsp;&nbsp;Back&nbsp;&nbsp;</a>
      <?php } ?>
   
 

      <input type="button" class="normalbutton noPrint" onclick="javaScript:window.print();" name="Print" value="&nbsp;&nbsp;Print&nbsp;&nbsp;">

  </div>


  </div>
  </div>
  <div class="content_wrapper_bottom"></div>


<script>
  function   findAppDetail(appId,MO,orderNumber, appType){
    var url = "<?= url_for("supportTool/searchApplication"); ?>";

    $("#application_div").hide();
    $('#result_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    $("#result_div").show();

    $.post(url, { app_id: appId,template: MO,orderNumber: orderNumber, apptype : appType},
    function(data){

      if(data == "logout"){
        location.reload();
      }else
      {
        var trimdata = jQuery.trim(data);
        if(trimdata.substring(0, 7) == '<style>')
        {
          location.reload();
        }else{
          //$("#flash_notice").html('<span>Bank '+status+' successfully</span>');
          $("#application_div").html(data);
          $("#application_div").show();
          $("#application_div").focus();
          $("#result_div").hide();
        }
      }
      //window.location.href = "URL";
      //setTimeout( "refresh()", 2*1000 );
    });

  }
</script>
