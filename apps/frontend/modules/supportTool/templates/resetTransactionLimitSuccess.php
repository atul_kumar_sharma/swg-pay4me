<div class="NwPopUp">
    <div class="floatRight"><strong>press [Esc] to close</strong></div> <!-- Bug Id:33770 -->
    <div class="NwHeading"><?php echo image_tag('/images/reset_icon.png',array('alt'=>'SWGlobal LLC', 'align' => 'absmiddle'));?> Reset User Transaction Limit</div>
    
    <div class="NwMessage">
        <div class="red noscreen" id="confirmDiv" style="text-align:center;"></div>
        <form>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="150">First Name: </td>
                    <td><?php echo $firstName; ?></td>
                </tr>
                <tr>
                    <td>Last Name: </td>
                    <td><?php echo $lastName; ?></td>
                </tr>
                <tr>
                    <td>Email: </td>
                    <td><?php echo $email; ?></td>
                </tr>
                <tr>
                    <td>Assigned Transaction Limit: </td>
                    <td><?php echo $numberOfTransaction; ?>
                    <input type="hidden" name="hidenNumberOfTransaction" id="hidenNumberOfTransaction" value="<?php echo $numberOfTransaction; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Transaction Used: </td>
                    <td><?php echo $transactionUsed; ?>
                    <input type="hidden" name="hidenTransactionUsed" id="hidenTransactionUsed" value="<?php echo $transactionUsed; ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Reset Transaction Limit: </td>
                    <td>
                        <input type="text" name="txtTransLimit" id="txtTransLimit" class="txt-input" />
                        <div class="red" id="errorDiv"></div>
                    </td>
                </tr>
                
            </table>
        </form>
    </div>
    <br />
    <div align="center">
        <input type="button" class="normalbutton" value="Reset" id="btnReset"  />
        &nbsp;&nbsp;
        <input type="button" class="normalbutton" id="btnCancel" value="Cancel" onclick = "disablePopup();" />
        <input type="hidden" name="userId" id="userId" value="<?php echo Settings::encryptInput($userId); ?>"  />
        <input type="hidden" name="cardHash" id="cardHash" value="<?php echo $cardHash; ?>"  />
    </div>
    
</div>
<script>
    $('document').ready(function(){
        if($('#txtTransLimit').length > 0){
            $('#txtTransLimit').focus();
        }


        $('#btnReset').click(function(){
            var txtVal = jQuery.trim($('#txtTransLimit').val());
            var userId = jQuery.trim($('#userId').val());
            var cardHash = jQuery.trim($('#cardHash').val());

            /* Resetting confirmation div text */
            $('#confirmDiv').hide();

            var objRegExp = /^\d+$/;     
            if(txtVal == ''){
                $('#errorDiv').html("Please enter digit for reset transaction.");
                $('#txtTransLimit').focus();                
            } else if(!objRegExp.test(txtVal)){
                $('#errorDiv').html("Please enter numeric value only.");
                $('#txtTransLimit').focus();
            } else {
                $('#errorDiv').html("");
                
                if(parseInt(txtVal) > parseInt($('#hidenNumberOfTransaction').val()) ){
                    $('#errorDiv').html("Reset limit can not be greater than assigned transaction limit.");
                    $('#txtTransLimit').focus();
                    return false;
                }
                if(parseInt($('#hidenTransactionUsed').val()) < 1 ){
                    $('#errorDiv').html("Sorry!! You can't reset transaction limit as there is no transaction used till now.");
                    $('#txtTransLimit').focus();
                    return false;
                }

                $('#btnCancel').attr('disabled', 'disabled');
                $('#btnReset').attr('disabled', 'disabled');
                $('#btnReset').val('Please Wait...');


                var resetUrl = "<?php echo url_for('supportTool/resetTransactionLimit/'); ?>";
                $.ajax({
                    type: "POST",
                    url: resetUrl,
                    data: {'txtValue': txtVal, 'uid': userId, 'cardHash': cardHash},
                    success: function (data) {
                        data = jQuery.trim(data);
                        $('#confirmDiv').show();
                        if(data == 'done'){                            
                            $('#confirmDiv').html('User has been reset successfully.');
                        }else{
                            $('#confirmDiv').html('Oops!! There are some error while reseting transactions.');
                        }                      

                        $('#btnCancel').attr('disabled', '');
                        $('#btnReset').attr('disabled', '');
                        $('#btnReset').val('Reset');
                        
                    }//End of success: function (data) {...
                });




                
            }
        })


        
    })
</script>