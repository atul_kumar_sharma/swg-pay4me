<script type="text/javascript">
    $(document).ready(function()
    {
        var fmobj = document.order_search_form;
        var count = 0;
        for (var i=0;i<fmobj.elements.length;i++)
        {
            var e = fmobj.elements[i];
            if ((e.type=='checkbox'))
            {
                count = parseInt(count) +1;
            }
        } 

        var actionUrl = '<?php echo url_for('supportTool/unpayNisIpay4me'); ?>';
        $("#unpaidIpay4meNIS").click(function(){
            document.order_search_form.action =  actionUrl;
            $('#actionType').val('ipay4me_nis');
            document.order_search_form.submit();
        });

        $("#unpaidNIS").click(function(){
            document.order_search_form.action = actionUrl;
            $('#actionType').val('nis');
            document.order_search_form.submit();
        });

    });

    function validateForm(fmobj, chkAll){
        $("#err_comment").html('');
        $('#err_chkbx').html("");
        var orderNo = $('#order_no').val();
        if(orderNo == ''){
            alert("Please enter SW Global LLC order number");
            $('#order_no').focus();
            return false;
        }
        if(orderNo != ''){
            if(isNaN(orderNo)){
                alert("Please enter only numeric value.");
                $('#order_no').val('') ;
                $('#order_no').focus();
                return false;
            }
        }

        var refunded = validateMultipleRadio('order_search_form');        
        if(refunded == ''){
            alert("Please select from refunded/non-refunded.");
            return false;
        }        

    }

    function checkAll(fmobj, chkAll)
    {
        for (var i=0;i<fmobj.elements.length;i++)
        {
            var e = fmobj.elements[i];
            if ((e.name == chkAll) && (e.type=='checkbox') && (!e.disabled))
            {
                e.checked = fmobj.chk_all.checked;
                
            }
        }
    }

    function UncheckMain(fmobj, objChkAll, chkElement, obj)
    {
        var boolCheck = true;        
        
        if(objChkAll.checked==false)
        {
            for(var i=0;i<fmobj.elements.length;i++)
            {
                if(fmobj.elements[i].name.indexOf(""+chkElement+"")!="-1" && fmobj.elements[i].checked==false)
                {
                    boolCheck = false;
                    break;
                }
            }
            if(boolCheck==true)
                objChkAll.checked=true;
        }
        else
        {
            objChkAll.checked=false;
        }
    }


</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Unpaid Applications(s)'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>

<form name='order_search_form' id='order_search_form' action='<?php echo url_for('supportTool/unpaidApplications');?>' method='post' class="dlForm" onsubmit="return validateForm(document.order_search_form,'chk_fee[]')">
<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
<div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
</div><br/>
<?php }?>

<div class="clear"></div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">



    <tr>
        <td>&nbsp;</td>
        <td>
            <?php
            echo formRowComplete('Order Number<span style="color:red">*</span>',input_tag('order_no', $ipayOrderNo, array('class'=>'txt-input'))); ?></td>
    </tr>
    <tr>
        <td></td>
        <td> <input type="radio" name="refunded" id="refunded_true" value="true" <?php if($refunded == 'true'){ echo "checked"; } ?>> Refunded&nbsp;&nbsp;&nbsp;
        <input type="radio" name="refunded" id="refunded_false" value="false" <?php if($refunded == 'false'){ echo "checked"; } ?>> Non-Refunded</td>
    </tr>

    <td>&nbsp;</td>
    <td><div class="lblButton">
            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </div>
        <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
    </div></td>
    </tr>
</table>
<br>


<?php if($isFound) {
    $i = 1;
    ?>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td class="blbar" colspan="14" align="right">
            <div style="float:left">Unpaid Application(s)</div>
        <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
    </tr>
    <tr>
        <td width="2%" ><span class="txtBold" id="maincheck"> <input type="checkbox" name="chk_all" id="chk_all" onclick="checkAll(document.order_search_form,'chk_fee[]');"></span></td>
        <td width="4%" ><span class="txtBold">S. No.</span></td>
        <td width="9%"><span class="txtBold">Application type</span></td>
        <td width="9%"><span class="txtBold">App Id</span></td>
        <td width="12%"><span class="txtBold">Reference No</span></td>
        <td width="14%"><span class="txtBold">Applicant Name</span></td>
        <td width="9%"><span class="txtBold">Date of Birth</span></td>
        <td width="6%"><span class="txtBold">NIS Payment Status</span></td>
        <td width="6%"><span class="txtBold">Paid On</span></td>
        <td width="10%"><span class="txtBold">Email Id</span></td>
        <td width="10%"><span class="txtBold">Order Number</span></td>
        <td width="10%"><span class="txtBold">Payment Gateway</span></td>
        <td width="10%"><span class="txtBold">Country</span></td>

    </tr>
    <tbody>
        <?php $showButtons = false;
        $i=1;
        $appCount = 0;
        if($pager->getNbResults()>0)
        {
            //              $finalStatus = $nisHelper->isAllReadyRefunded($sf_params->get('order_no'));
            //              $finalStatus= $finalStatus->getRawValue();
            //              $fStatus = null;
            //              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
            //                $fStatus = $finalStatus['action'];
            //              }
            foreach ($pager->getResults() as $result):
            //            echo "<pre>";print_r(get_class_methods($result));die;
            $app['passport_id'] = $result['passport_id'];
            $app['visa_id'] = $result['visa_id'];
            $app['freezone_id'] = $result['freezone_id'];
            $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];

            ## Fetching application details...
            $appDetails = $appObj->getApplicationDetails($app);

            ## Fetching application processing country...
            $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails['id'], $appDetails['app_type']);

            $finalStatus = $nisHelper->isAllAppReadyRefunded($ipayOrderNo,$appDetails['id']);
            $finalStatus= $finalStatus->getRawValue();
            $fStatus = null;
            if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                $fStatus = $finalStatus['action'];
            }

            if($appDetails['status']!="New"){
                $appCount = $appCount + 1;
            }

            ?>
        <tr>
            <td width="2%"><span><?php if($appDetails['status']!="New") { ?> <input type="checkbox" name="chk_fee[]" id="<?= "chk_fee".$i; ?>" value="<?= $appDetails['id']."_".$appDetails['app_type'];?>" OnClick="UncheckMain(document.order_search_form,document.order_search_form.chk_all,'chk_fee',<?= "chk_fee".$i; ?>);" > <?php } ?></span></td>

            <td width="4%"><?php echo $i;?></td>
            <td width="9%"><span><?php if($appDetails['app_type'] == 'vap'){ echo sfConfig::get('app_visa_arrival_title');} else { echo strtoupper($appDetails['app_type']);} ?></span></td>
            <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
            <td width="9%"><span><?php echo $appDetails['ref_no'];?></span></td>
            <td width="14%"><span><?php echo $appDetails['name'];?></span></td>
            <td width="9%"><span><?php echo date_format(date_create($appDetails['dob']), 'Y-m-d');?></span></td>
            <?php if(isset ($fStatus) && $fStatus!=null){ ?>

            <td width="9%"><span><?php echo strtoupper($fStatus);?></span></td>
            <?php } else{ $showButtons = true;?>

            <td width="9%"><span><?php echo $appDetails['status'];?></span></td>

                        <?php  }  ?>

            <td width="9%"><span><?php if($appDetails['status'] != 'New') echo date_format(date_create($ipay4meOrderDetails[0]['paid_at']), 'Y-m-d'); else echo "--"; ?></span></td>
            <td width="10%"><span><?php echo str_replace('@',' @',$ipay4meOrderDetails[0]['email']);?></span></td>
            <td width="10%"><span><?php echo $ipayOrderNo;?></span></td>
            <td width="10%"><span><?php echo $gatewayName;?></span></td>
            <td width="10%"><span><?php echo ($processingCountry != '')?$processingCountry:'--';?></span></td>




        </tr>
        <?php
        $i++;
        endforeach; ?>

        <tr>
            <td class="blbar" colspan="14" height="25px" align="right">
                <div class="paging pagingFoot">
                    <?php
                    echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?order_no='.$ipayOrderNo.'&usePager=true','detail_div');
                    ?>
                </div>
            </td>
        </tr>
        <?php } else { ?>
        <tr><td  align='center' class='red' colspan="13">No Results found</td></tr>
        <tr><td class="blbar" colspan="13" height="25px" align="right"></td></tr>
        <?php } ?>
    </tbody>

</table>
<br>

    <?php if($showButtons)
    {
        if($pager->getNbResults()>0)
        {?>
<div align="center">
    <input type="button" class="normalbutton" id="unpaidIpay4meNIS" name="ipay4meButton" value="Unpaid (Ipay4me + NIS)"/>
    <?php  // echo submit_tag('Unpaid (Ipay4me + NIS)',array('class' => 'normalbutton','onsubmit'=>'return formsubmit()')); ?>

    <input type="button" class="normalbutton" id="unpaidNIS" name="nisButton" value="Unpaid NIS"/>

</div>
<?php }

}} ?>

<br>
<br>
<br>

<input type="hidden" name="actionType" id="actionType" value="" />
<input type="hidden" name="hdnOrderNumber" id="hdnOrderNumber" value="<?php echo $ipayOrderNo; ?>" />
<input type="hidden" name="hdnRefunded" id="hdnRefunded" value="<?php echo $refunded; ?>" />
<input type="hidden" name="hdnPaidApplicationCount" id="hdnPaidApplicationCount" value="<?php echo $appCount; ?>" />

<div id="result_div" style="width:500px;" align="center"></div>
<div id="application_div" style="display:none;"></div>
</form>
</div>
</div>
<div class="content_wrapper_bottom"></div>



