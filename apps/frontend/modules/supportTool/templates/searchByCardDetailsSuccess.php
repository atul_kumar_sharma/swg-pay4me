<script>
  function validateForm()
  {
    //    if(document.getElementById('card_type').value == '')
    //    {
    //      alert('Please Select Card Type.');
    //      document.getElementById('card_type').focus();
    //      return false;
    //    }
    if(document.getElementById('first_digit').value == '')
    {
      alert('Please Enter Card First Digit.');
      document.getElementById('first_digit').focus();
      return false;
    }else{
      var lastD = $('#first_digit').val();
//      if(isNaN(lastD)){
//        alert("Card First Digit should be numeric value.");
//        $('#first_digit').val('');
//        $('#first_digit').focus();
//        return false;
//      }
    }

    if(document.getElementById('last_digit').value == '')
    {
      alert('Please Enter Card Last 4 Digits.');
      document.getElementById('last_digit').focus();
      return false;
    }else{
      var lastD = $('#last_digit').val();
      if(isNaN(lastD)){
        alert("Card Last 4 Digits should be numeric value.");
        $('#last_digit').val('');
        $('#last_digit').focus();
        return false;
      }
      if(lastD.length<4){
        alert("Please Enter Card Last 4 Digits.");
        $('#last_digit').focus();
        return false;
      }
    }
  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Search SW Global LLC order number by card details'));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php echo form_tag('supportTool/searchByCardDetails',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
    </div>
    <br/>
        <?php }?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
          <?php

          $cardFirst = (isset($_POST['first_digit']))?$_POST['first_digit']:"";
          $cardLast = (isset($_POST['last_digit']))?$_POST['last_digit']:"";

          //                echo "<tr><td>";
          echo formRowComplete('Card First Digit<span class="red">*</span>',input_tag('first_digit', $cardFirst, array('size' => 20, 'maxlength' => 4,'class'=>'txt-input')));
          echo "<tr>";
          echo formRowComplete('Card Last 4 Digits<span class="red">*</span>',input_tag('last_digit', $cardLast, array('size' => 20, 'maxlength' => 4,'class'=>'txt-input')));
          echo "</tr>";?>
          <tr>
            <td>&nbsp;</td>
        <td>
            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>

          </td>
          </tr>
        </table>
  <br>
  <?php if($isFound):

  if($pager->getNbResults()>0) {

  if(isset($cardReportArray)) {
       ?>
<table>
<tr>
<td width=60%">
  <table align="center" border="1px solid black" cellpadding="0" cellspacing="0" >
  <tr>
  <td class="blbar" colspan="2" align="right"><div class="float_left">Card Transaction Summary</div></td>
  </tr>
    <tr>
    <td><span class="txtBold">Success Transaction:</span></td>
    <td width="25%"><?php echo $cardReportArray['0'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Failed Transaction:</span></td>
    <td><?php echo $cardReportArray['1'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Success Transaction in one month:</span></td>
    <td><?php echo $cardReportArray['4'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Failed Transaction in one month:</span></td>
    <td><?php echo $cardReportArray['5'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Total Refund on this Card:</span></td>
    <td><?php echo $cardReportArray['2'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Total Charge-Back on this Card:</span></td>
    <td><?php echo $cardReportArray['3'];?></td>
    </tr>
    <tr>
    <td><span class="txtBold">Total Transaction(Including All):</span></td>
    <td><span class="txtBold"><?php echo $pager->getNbResults();?></span></td>
    </tr>
    <tr>
    <td class="blbar" colspan="2" height="25px" align="right"></td>
    </tr>
  </table>
  </td>
  
  <td width="50%" valign="top" height="20px" >
  <table align="center"  cellpadding="0" cellspacing="0">
  <tr>
  <td class="blbar" colspan="2" align="right"><div class="float_left">Card Registeration Summary</div></td>
  </tr>
  <?php if(count($registerDetails) > 1) { ?>
   <tr>
   <td colspan="2" align="center" class="txtBold">This card first and last four digits search has <?php echo count($registerDetails);?> records.</td>
   </tr>
  <?php } ?>
  <tr>
  <td>Is Register</td>
  <?php if(count($registerDetails) > 0){
        $isRegister  = 'Yes'; }
        else{
            $isRegister  = 'No';
        } ?>
  <td><?php echo $isRegister;?></td>
  </tr>
  <?php if(count($registerDetails) > 0) {
   foreach($registerDetails as $details) {
      ?>

  <tr>
  <td>Register with Login Email</td>
  <td><?php echo $details?></td>
  </tr>
  <?php } } ?>
    <tr style="border:0px">
    <td class="blbar" colspan="2" height="25px" align="right">
    </tr>
  </table>  
  </td>
  </tr>
  </table>

  <?php } } ?>

    <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
            <td class="blbar" colspan="10" align="right"><div class="float_left">SW Global LLC Order Numbers By Card Details</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
        </tr>
        <tr>
          <td width="4%" ><span class="txtBold">S. No.</span></td>
          <td width="9%"><span class="txtBold">Order Number</span></td>          
          <td width="9%"><span class="txtBold">Card First</span></td>
          <td width="9%"><span class="txtBold">Card Last</span></td>
          <td width="12%"><span class="txtBold">Transaction Amount</span></td>
          <td width="9%"><span class="txtBold">Card Holder Name</span></td>
          <td width="14%"><span class="txtBold">Transaction Status</span></td>
           <td width="9%"><span class="txtBold">Transaction Date(Y/M/D)</span></td>
        </tr>
        <tbody>
          <?php
          $i = $pager->getFirstIndice();

          if($pager->getNbResults()>0)
          {
            foreach ($pager->getResults() as $appDetails):

            if(isset ($appDetails['pay_status']) && $appDetails['pay_status']!='' && $appDetails['pay_status'] != 1)
            {
              $url = url_for("supportTool/searchOrderNo");
            }else{
              $url = url_for("supportTool/searchByRetryid");
            }

            ?>
          <tr>
            <td width="4%"><?php echo $i;?></td>
            <?php if(isset ($appDetails['pay_status']) && $appDetails['pay_status']!='' && $appDetails['pay_status'] != 1) {?>
            <td width="9%"><span><a href="<?= $url."?order_no=".Settings::encryptInput($appDetails['order_no']) ;?>" >
                <?= $appDetails['order_no'] ?>
                </a></span></td>
            <?php }else{ ?>
              <td width="9%"><span><a href="<?= $url."?id=".Settings::encryptInput($appDetails['retry_id']).'&order_no='.Settings::encryptInput($appDetails['order_no']) ;?>" >
                <?= $appDetails['order_no'] ?>
                </a></span></td>
            <?php } ?>             
            <td width="5%"><span><?php echo $appDetails['card_first'];?></span></td>
            <td width="5%"><span><?php echo $appDetails['card_last'];?></span></td>

            <?php
               $currencyId =  $appDetails['currency'];
               if($currencyId == '1'){
                   $amount = $appDetails['amount'];
               }else{
                   $amount = $appDetails['convert_amount'];
               }
               $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);

               ?>

            <td width="10%"><?php echo format_amount($amount,1,0,$currencySymbol);?></td>
            <td width="9%"><span>
                <?= ucwords($appDetails['holder_name']); ?></span>
            </td>
            <?php
            $status = '';
            if(isset ($appDetails['pay_status']) && $appDetails['pay_status']!='')
            {
              if($appDetails['pay_status'] == 0){
                $status = "Paid";
              }else if($appDetails['pay_status'] == 1){
                $status = "Notpaid";
              }else if($appDetails['pay_status'] == 2){
                $status = "Refunded";
              }else if($appDetails['pay_status'] == 3){
                $status = "Charge-Back";
              }
            }

            ?>
            <td width="9%"><span><?php echo $status;?></span></td>
            <td width="9%"><span><?php echo date_format(date_create($appDetails['trans_date']), 'Y-m-d');?></span></td>
          </tr>
          <?php
          $i++;
          endforeach; ?>
          <tr>
              <td class="blbar" colspan="10" height="25px" align="right"><div class="paging pagingFoot">
                <?php
                echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?first_digit='.Settings::encryptInput($firstDigit).'&last_digit='.Settings::encryptInput($lastDigit).'&usePager=true','detail_div');
                ?>
                </div></td>
          </tr>
          <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="10">No Results found</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <br>
    </div>
  <?php endif; ?>
</div>
</div>
<div class="content_wrapper_bottom"></div>
