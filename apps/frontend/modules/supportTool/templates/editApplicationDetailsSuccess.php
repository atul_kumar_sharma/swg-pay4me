<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Form');
if($appType == 'vap'){
    $appTypeTitle = sfConfig::get('app_visa_arrival_title');
}else{
    $appTypeTitle = $appType ;
}
include_partial('global/innerHeading',array('heading'=>'Update '.ucwords($appTypeTitle).'  Application'));
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>

<?php echo form_tag('supportTool/updateApplicationDetails',array('name'=>'edit_application_form','class'=>'dlForm multiForm','method'=>'post','id'=>'edit_application_form','onSubmit'=>'return validateForm()')) ?>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
          <td width="200px;">Application Type</td>
          <td><input type ="text" value = "<?php echo ucwords($appTypeTitle); ?>" name="app_type" readonly="true"></td>
        </tr>
        <tr>
          <td width="200px;">Application ID</td>
          <td><input type ="text" value = "<?php echo $appId; ?>" name="app_id" readonly="true"></td>
        </tr>
        <tr>
          <td width="200">Reference Number</td>
          <td><input type ="text" value = "<?php echo $refNo; ?>" name="ref_no" readonly="true"></td>
        </tr>
        
        <?php if(strtolower($appType) == 'passport') { ?>
        <tr>
          <td width="200px;"><?php echo $form['title_id']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['title_id']->render(); ?>
              <div class="red" id="title_id_error">
                  <?php echo $form['title_id']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['first_name']->render(); ?>
              <div class="red" id="first_name_error">
                  <?php echo $form['first_name']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['mid_name']->renderLabel(); ?></td>
          <td><?php echo $form['mid_name']->render(); ?>
              <div class="red" id="mid_name_error">
                  <?php echo $form['mid_name']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['last_name']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['last_name']->render(); ?>
              <div class="red" id="last_name_error">
                  <?php echo $form['last_name']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['gender_id']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['gender_id']->render(); ?>
              <div class="red" id="gender_id_error">
                  <?php echo $form['gender_id']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['date_of_birth']->render(); ?>
              <div class="red" id="date_of_birth_error">
                  <?php echo $form['date_of_birth']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['color_eyes_id']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['color_eyes_id']->render(); ?>
              <div class="red" id="color_eyes_id_error">
                  <?php echo $form['color_eyes_id']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['color_hair_id']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['color_hair_id']->render(); ?>
              <div class="red" id="color_hair_id_error">
                  <?php echo $form['color_hair_id']->renderError(); ?>
              </div>
          </td>
        </tr>
         <tr>
          <td width="200px;"><?php echo $form['marital_status_id']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['marital_status_id']->render(); ?>
              <div class="red" id="marital_status_id_error">
                  <?php echo $form['marital_status_id']->renderError(); ?>
              </div>
          </td>
        </tr>       


        <?php } else{ ?>

        <tr>
          <td width="200px;"><?php echo $form['title']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['title']->render(); ?>
              <div class="red" id="title_error">
                  <?php echo $form['title']->renderError(); ?>
              </div>
          </td>
        </tr>
        <?php if(strtolower($appType) == 'visa' || strtolower($appType) == 'freezone' || strtolower($appType) == 'reentry freezone') { ?>
        <tr>
          <td width="200px;"><?php echo $form['other_name']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['other_name']->render(); ?>
              <div class="red" id="other_name_error">
                  <?php echo $form['other_name']->renderError(); ?>
              </div>
          </td>
        </tr>
        <?php } else { ?>
        <tr>
          <td width="200px;"><?php echo $form['first_name']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['first_name']->render(); ?>
              <div class="red" id="first_name_error">
                  <?php echo $form['first_name']->renderError(); ?>
              </div>
          </td>
        </tr>
        <?php } ?>
        <tr>
          <td width="200px;"><?php echo $form['middle_name']->renderLabel(); ?></td>
          <td><?php echo $form['middle_name']->render(); ?>
              <div class="red" id="middle_name_error">
                  <?php echo $form['middle_name']->renderError(); ?>
              </div>
          </td>
        </tr>
        <tr>
          <td width="200px;"><?php echo $form['surname']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['surname']->render(); ?>
              <div class="red" id="surname_error">
                  <?php echo $form['surname']->renderError(); ?>
              </div>
          </td>
        </tr>
        <tr>
          <td width="200px;"><?php echo $form['gender']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['gender']->render(); ?>
              <div class="red" id="gender_error">
                  <?php echo $form['gender']->renderError(); ?>
              </div>
          </td>
        </tr>
        <tr>
          <td width="200px;"><?php echo $form['date_of_birth']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['date_of_birth']->render(); ?>
              <div class="red" id="date_of_birth_error">
                  <?php echo $form['date_of_birth']->renderError(); ?>
              </div>
          </td>
        </tr>
        <?php if(strtolower($appType) != 'reentry freezone') {  ?>
        <tr>
          <td width="200px;"><?php echo $form['eyes_color']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['eyes_color']->render(); ?>
              <div class="red" id="eyes_color_error">
                  <?php echo $form['eyes_color']->renderError(); ?>
              </div>
          </td>
        </tr>
        <tr>
          <td width="200px;"><?php echo $form['hair_color']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['hair_color']->render(); ?>
              <div class="red" id="hair_color_error">
                  <?php echo $form['hair_color']->renderError(); ?>
              </div>
          </td>
        </tr>
        <tr>
          <td width="200px;"><?php echo $form['marital_status']->renderLabel(); ?><span class="red">*</span></td>
          <td><?php echo $form['marital_status']->render(); ?>
              <div class="red" id="marital_status_error">
                  <?php echo $form['marital_status']->renderError(); ?>
              </div>
          </td>
        </tr>       
        
       <?php } } ?>
        <tr>        
        <td colspan="2" align ="center">
            <input type="button" value="Back" onclick='window.location = "<?php echo url_for('supportTool/nisApplicationDetails?app_id='.$appId.'&app_type='.$appType.'&ref_no='.$refNo); ?>"; return false;' class="normalbutton">
            <input type="submit" value="Update" onclick="return confirm('Are you sure you want to update details ?')" class="normalbutton">
        </td>
        </tr>                 
     </table>
</div>
</div>
<div class="content_wrapper_bottom"></div>

