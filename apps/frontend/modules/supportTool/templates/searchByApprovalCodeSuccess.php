<script>
  function validateForm()
  {
    var err  = 0;
    if(jQuery.trim($('#approval_code').val()) == "")
    {
      $('#err_approval_code').html("Please enter Approval Code");
      $('#approval_code').focus();
      err = err+1;
    }

    if(err != 0)
    {
      return false;
    }

  }
</script>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
    <?php
use_helper('Pagination');
use_helper('Form');
use_helper('EPortal');
if(empty($print)){
  include_partial('global/innerHeading',array('heading'=>'Search by Approval Code'));
}
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php echo form_tag('supportTool/searchByApprovalCode',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));        
        ?>
    </div>
    <br/>
      <?php }?>
      <?php if( isset($approvalCodeNotFound) && count($approvalCodeNotFound) > 0){ ?>
    <div id="flash_notice" class="alertBox" > Following approval code(s) are not found:<br />
        <ul>
        <?php foreach($approvalCodeNotFound AS $value) { if(trim($value) == '')continue; ?>
            <li><?php echo $value; ?></li>
        <?php } ?>
        </ul>
    </div>
    <br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
    </div>
    <br/>
        <?php }?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
          <?php



          $approval_code = (isset($_GET['approval_code']))?$_GET['approval_code']:"";          


          echo formRowComplete('Approval Code<span style="color:red;">*</span>',input_tag('approval_code', $approval_code, array('size' => 20, 'class'=>'txt-input')),'','approval_code','err_approval_code','username_row');          
          ?>
          <tr>
            <td>&nbsp;</td>
        <td colspan="2">
            <?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>

          </td>
          </tr>
        </table>
  <br>
  <?php if($isFound){ ?>
  <?php
  $arrOrderCount = count($arrOrderDetails);
  foreach($arrOrderDetails AS $arrOrderDetails) { ?>
  <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
    <tr>
        <td><table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0">
            <tbody>
            <tr>
                <td style="font-size: 83%;">&nbsp;Order date: <b>
                  <?php  echo $arrOrderDetails['paid_date'];?>
                  </b><br>
                  &nbsp;SW Global LLC order number: <b>
                  <?php  echo $arrOrderDetails['order_number'];?>
                  </b></td>
                <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td>
    </tr>
            </tbody>
          </table></td>
      </tr>
    <tr>
        <td><table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
          <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
            <td width="100" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Payment Status</b>&nbsp;</td>
            <td colspan="2" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; text-align: left;"><b>Item</b></td>
            <td width="52" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black;" align="right"><b>Price</b></td>
          </tr>
          <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
              <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left">&nbsp;&nbsp;<b>Successful</b></td>
              <td colspan="2" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="left"><b><span style="font-weight: bold; color: black;">
                <?php  echo $arrOrderDetails['mername'];?>
                </span></b> &nbsp;-&nbsp;
              <?php
              if(is_numeric($arrOrderDetails['name']))
              {
                ?>
                <span style="color: rgb(102, 102, 102);">Number of appplications for which payment has been made is <?php echo $arrOrderDetails['name'];?>.<br>
                Your applications will be processed further on <?php echo $arrOrderDetails['mername']; ?> Portal.</span>
              <?php
            }
            else
            {
              ?>
                <span style="color: rgb(102, 102, 102);">Payment for Service
                <?php //echo $orderDetail['description']; ?>
                for customer <?php echo $arrOrderDetails['name'];?>.<br>
                Your application will be processed further on <?php echo $arrOrderDetails['mabbr']; ?> Portal.</span>
              <?php
            }
            ?>
            </td>            
            <td style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249);" align="right"><?php echo format_amount($arrOrderDetails['amount'],1); ?></td>
          </tr>
          <tr style="background-color: rgb(229, 236, 249);">
            <td></td>
            <td colspan="2" style="font-weight: bold; font-size: 105%; text-align: right; vertical-align: text-top;">Total:</td>
              <td style="font-weight: bold; font-size: 105%;" align="right"><?php echo format_amount($arrOrderDetails['amount'],1); ?></td>
    </tr>
          </table></td>
      </tr>
    <?php
    $card_type = $arrOrderDetails['card_type'];
    if('V' == strtoupper($arrOrderDetails['card_type']))
    $card_type = "VISA";    
    ?>
    <tr style="color:#336600;background-color:#EFFFEF;">
        <td colspan="3" class="txtBold" align="left"><b>Paid with:</b> <?php echo $card_type;?>&nbsp;
          <?php  echo $arrOrderDetails['card_first']; ?>
          -<?php echo getNoOfX($arrOrderDetails['card_len'] - 8);?>-
          <?php  echo $arrOrderDetails['card_last']; ?></td>
    </tr>
    <tr style="color:#336600;background-color:#EFFFEF;">
      <td colspan="3" class="txtBold" align="left"><b>Approval Code:</b> <?php echo $arrOrderDetails['approvalCode'];?></td>
    </tr>
    <tr style="color:#336600;background-color:#EFFFEF;">
      <td colspan="3" class="txtBold" align="left"><b>Email:</b> <?php echo $arrOrderDetails['email']?></td>
    </tr>
  </table>
  <?php }//End of foreach($arrOrderDetails AS $arrOrderDetails) {... ?>
  <br />
    <div align="center">
      <?php
      $fileName = $doc_File_title.$extension;      
      if($multiple){
              $fileName = $zipFileName;
      }      
      
      ?>
       <a href="<?php echo url_for('supportTool/downloadPdf').'?fileName='.$fileName  ?>" class="normalbutton" style="text-decoration:none;">Download To PDF</a>
   
  </div>
    <?php }
  ?>
</div>
</div>
<div class="content_wrapper_bottom"></div>
<script>
    if($('#approval_code').length > 0){
        $('#approval_code').focus();
    }
</script>
