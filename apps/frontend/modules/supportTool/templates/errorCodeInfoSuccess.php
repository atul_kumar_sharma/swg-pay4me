<script>
  function validateForm()
  {
    $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
    var err  = 0;
    
//    if(document.getElementById('order_detail_id').value){
//      var creditcardD = $('#order_detail_id').val();
//      if(isNaN(creditcardD)){
//        alert("Order Detail Id should be numeric value.");
//        $('#order_detail_id').val('');
//        $('#order_detail_id').focus();
//        $('#hdn_datatable').hide();
//        return false;
//      }
//
//    }

    if($('#error_code').val() =="")
    {

     // document.getElementById('error_td').style.display=  '';
      $('#detail_error').html("<font color='red'>Please enter error code.</font>");

      err = err+1;
    }else
    {
      $('#detail_error').html(" ");
    }
    
    var a = $('#error_code').val();
    var b = a.toUpperCase();
    if(b.search("X") ==-1)
    
      {
        $('#detail_error').html("<font color='red'>Please enter correct error code.</font>");
        err = err+1;
      }
      else
    {
      $('#detail_error').html(" ");
    }

    if(err != 0)
    {
      $('#hdn_datatable').hide();
      return false;
    }else{

    }
    
    }
</script>
    <?php echo form_tag('supportTool/errorCodeInfo',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form','onSubmit'=>'return validateForm()')) ?>

<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
      <?php
use_helper('Pagination');
use_helper('Form');
include_partial('global/innerHeading',array('heading'=>'Error Code Info'));
?>
    <div class="clear"></div>
    <div class="tmz-spacer"></div>
    <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>
      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
    </div>
    <br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
    </div>
    <br/>
        <?php }?>
    <table width="99%" cellpadding="0" cellspacing="0" border="0">
          <?php         

          //                echo "<tr><td>";
          echo formRowComplete('Error Code<font color="red">*</font>',input_tag('error_code', $errorCode, array('size' => 27, 'maxlength' => 11,'class'=>'txt-input')),'(Eg: 0000000X000)','error_code','detail_error','error_code_row');
          echo "</tr>";
          ?>
          <tr valign="top">
            <td height="30" valign="top" style="border-right-style:none; vertical-align:top;">&nbsp;</td>
        <td height="30" valign="top" style="border-left-style:none; vertical-align:top;"><?php   echo submit_tag('Search',array('class' => 'normalbutton')); ?>
        </td>
          </tr>
        </table>
  <br>
  <?php
  if(!empty($error_message))
  {
    echo '<center><span class="red" size="14px">'.$error_message.'</span></center><br>';
  }
  ?>
  <?php if($appDetail == 1) {?>
    <div id="nxtPage">
      <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
            <td class="blbar" colspan="11" align="right"><div style="float:left">NIS Application Details By SW Global LLC Order Number</div>
        </tr>
        <tr>
          <td width="4%" ><span class="txtBold">S. No.</span></td>
          <td width="9%"><span class="txtBold">Application type</span></td>
          <td width="9%"><span class="txtBold">App Id</span></td>
          <td width="12%"><span class="txtBold">Reference No</span></td>
          <td width="14%"><span class="txtBold">Applicant Name</span></td>
          <td width="9%"><span class="txtBold">Date of Birth</span></td>
          <td width="16%"><span class="txtBold">NIS Payment Status</span></td>
        </tr>
        <tbody>
        <?php for($i=0;$i<count($ipayNisAppDetails);$i++){ ?>
          <tr>
            <td width="4%"><?php echo $i+1; ?></td>
            <td width="9%"><span><?php echo strtoupper($ipayNisAppDetails[$i]['app_type']);?></span></td>
            <td width="10%"><span><a href="<?php echo url_for('supportTool/applicationDetailsInfo?appId='.$ipayNisAppDetails[$i]['id'].'&type='.$ipayNisAppDetails[$i]['app_type']) ?>"><?php echo $ipayNisAppDetails[$i]['id'];?></a></span></td>
            <td width="9%"><span><?php echo $ipayNisAppDetails[$i]['ref_no'];?></span></td>
            <td width="14%"><span><?php echo $ipayNisAppDetails[$i]['name'];?></span></td>
            <td width="9%"><span><?php echo date_format(date_create($ipayNisAppDetails[$i]['dob']), 'd-m-Y');?></span></td>
            <td width="9%"><span><?php echo strtoupper($ipayNisAppDetails[$i]['status']);?></span></td>
          </tr>
          <?php } ?>
            <tr>
              <td class="blbar" colspan="11" height="25px" align="right"></td>
            </tr>
        </tbody>
      </table>
    </div>
  <?php } else { ?>
  <?php if($isFound): ?>
    <div id="nxtPage">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" id="hdn_datatable">
        <tr>
            <td class="blbar" colspan="6" align="right"><div style="float:left">Order Number</div>
              <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
        </tr>
        <tr>
          <td width="5%" ><span class="txtBold">S. No.</span></td>
          <td width="20%"><span class="txtBold">Order Number</span></td>
          <td width="20%"><span class="txtBold">Card Holder Name</span></td>
          <td width="20%"><span class="txtBold">Card Number</span></td>
          <td width="15%"><span class="txtBold">Phone Number</span></td>
          <td width="20%"><span class="txtBold">Processing Gateway</span></td>
        </tr>
        <tbody>
          <?php
          $i = $pager->getFirstIndice();
          // $checkedVal = $sf_request->getParameter('checkedVal');
            
          if($pager->getNbResults()>0)
          {
            foreach ($pager->getResults() as $details):
            $cardLen = $details['card_len'];
            $middlenum = '';
            $cardDisplay = '';
            if($cardLen == 13){
              $middlenum = "-XXXXX-";
            }else if($cardLen == 16){
              $middlenum = "-XXXX-XXXX-";
            }else if($cardLen == 15){
              $middlenum = "-XXXXXXX-";
            }
            $cardDisplay = $details['card_first'].$middlenum.$details['card_last'];

           ?>
          <tr>
            <td width="5%"><?php echo $i;?></td>
            <td width="20%"><span> <a href="<?php echo url_for('supportTool/applicationDetails?orderNumber='.Settings::encryptInput($details['order_number']).'&errorMessageId='.Settings::encryptInput($errorMessageId)) ?>" title="View Application Details"><?php echo $details['order_number'];?></a></span></td>
              <td width="20%"><span>
                <?= ucwords($details['card_holder']) ?>
                </span></td>
              <td width="20%"><span>
                <?= "<b>".$cardDisplay."</b>"; ?>
                </span></td>
              <td width="15%"><span>
                <?= $details['phone'] ?>
                </span></td>
              <td width="20%"><span>
                <?= $details['Gateway']['display_name'] ?>
                </span></td>
            <input type ="hidden" name="errorMsg" id="errorMsg" value="<?php echo $errorMsg;?>">
          </tr>
          <?php
          $i++;
          endforeach; ?>
          <tr>
              <td class="blbar" colspan="6" align="right"><div class="paging pagingFoot">
                <?php 
                echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?error_code='.Settings::encryptInput($errorCode).'&usePager=true','detail_div');
                ?>
                </div></td>
          </tr>
          <?php } else { ?>
            <tr>
              <td  align='center' class='red' colspan="6">No order has been generated for this transaction.</td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <br>
    </div>
  <?php endif;
} ?>
</div>
</div>
<div class="content_wrapper_bottom"></div>
