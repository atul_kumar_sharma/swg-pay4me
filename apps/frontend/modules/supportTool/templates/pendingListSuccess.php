<?php
use_helper('Pagination');
use_helper('Form');

?>
      <?php
      $sf = sfContext::getInstance()->getUser();
      if($sf->hasFlash('notice')){ ?>

      <div id="flash_notice" class="alertBox" >
        <?php
        echo nl2br($sf->getFlash('notice'));
        ?>
</div>
<br/>
      <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
          <?php
          echo nl2br($sf->getFlash('error'));
          ?>
</div>
<br/>
        <?php }?>
    <br>
      <div id="nxtPage">
  <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
          <tr>
      <td class="blbar" colspan="7" align="right"><div class="float_left">Credit Card Pending List</div>
        <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
          </tr>
          <tr>
            <td width="8%" ><span class="txtBold">S. No.</span></td>
            <td width="18%"><span class="txtBold">Credit Card number</span></td>
            <td width="18%"><span class="txtBold">Card Holder Name</span></td>
            <td width="20%"><span class="txtBold">Card Registration Date</span></td>
            <td width="12%"><span class="txtBold">Credit Card Statement</span></td>
            <td width="12%"><span class="txtBold">Photo Identity</span></td>
            <td width="15%"><span class="txtBold">Action</span></td>
          </tr>
          <tbody>
            <?php
            $i = $pager->getFirstIndice();
         
            $checkedVal = $sf_request->getParameter('checkedVal');
            $concatUrl = '?checkedVal='.$checkedVal;
            $concatUrl .= ($sf_request->getParameter('email') != '')? '&email='.$sf_request->getParameter('email') : "";
            $concatUrl .= ($sf_request->getParameter('fdate') != '')? '&fdate='.$sf_request->getParameter('fdate') : "";
            $concatUrl .= ($sf_request->getParameter('tdate') != '')? '&tdate='.$sf_request->getParameter('tdate') : "";
            
            if($pager->getNbResults()>0)
            {
              foreach ($pager->getResults() as $details):
              
              $cardLen = $details['card_len'];
              $middlenum = '';
              $cardDisplay = '';
              if($cardLen == 13){
                $middlenum = "-XXXXX-";
              }else if($cardLen == 16){
                $middlenum = "-XXXX-XXXX-";
              }else if($cardLen == 15){
                $middlenum = "-XXXXXXX-";
              }
              $cardDisplay = $details['card_first'].$middlenum.$details['card_last']
              ?>
            <tr>
              <td width="8%"><?php echo $i;?></td>
        <td width="18%"><span>
          <?= "<b>".$cardDisplay."</b>"; ?>
          </span></td>
        <td width="18%"><span>
          <?= ucwords($details['card_holder']) ?>
          </span></td>
        <td width="20%"><span>
          <?= $details['created_at'] ?>
          </span></td>
        <td width="12%"><span>
          <?php if($details['address_proof'] !=''){echo "Yes";}else{echo 'No';}?>
          </span></td>
        <td width="12%"><span>
          <?php if($details['user_id_proof'] !=''){echo "Yes";}else{echo 'No';}?>
          </span></td>
        <?php  
       $encryptedId = Settings::encryptInput($details['id']); ?>
        <td width="15%"><span><a href="javascript:void(0);" onclick="gotoAuthDetails('<?php echo $page;?>', '<?php echo $encryptedId; ?>'); return false;"><img src="<?php echo image_path('duplicate_app.gif'); ?>" alt="Details" title="Details" /></a>
          <?php //echo link_to("Details", url_for("supportTool/authDetails?id=".$details['id']))?>
          
          </span> <span><a href="javascript:void(0);" onclick="gotoEditContent('<?php echo $page;?>', '<?php echo $encryptedId; ?>'); return false;"><img src="<?php echo image_path('edit_icon.png'); ?>" alt="Edit" title="Edit" /></a>
          <?php //echo link_to("Edit", url_for("supportTool/editContectDetails?id=".$details['id']."&mode=1"))?>
          </span> </td>
            </tr>
            <?php
            $i++;
            endforeach;           

            ?>
            <tr>
        <td class="blbar" colspan="7" height="25px" align="right"><div class="paging pagingFoot">
            <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().$concatUrl), 'nxtPage') ?>
          </div></td>
            </tr>
            <?php } else { ?>
      <tr>
        <td  align='center' class='error' colspan="7">No Results found</td>
      </tr>
            <?php } ?>
          </tbody>
        </table>
        <br>
      </div>
  <script>
      function gotoAuthDetails(page, id){
        var url = '<?php echo url_for('supportTool/authDetails');?>';        
        document.frm_tra_detail.action = url+ '/id/'+id;
        $('#page').val(page);
        document.frm_tra_detail.submit();
      }
      function gotoEditContent(page, id){
        var url = '<?php echo url_for('supportTool/editContectDetails');?>';
        document.frm_tra_detail.action = url+ '/id/'+id+'/mode/1';
        $('#page').val(page);
        document.frm_tra_detail.submit();
      }
  </script>
