<?php

/**
 * supportTool actions.
 *
 * @package    ama
 * @subpackage supportTool
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class supportToolActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeOrderSearch(sfWebRequest $request) {
        $ErrorMsg = new ErrorMsg();
        $this->getUser()->setFlash("error", $ErrorMsg->displayErrorMessage("E048", "001000", array("msg" => "Hi Navin")));
        $this->redirect("supportTool/searchOrderNo");
        //    Doctrine::getTable("CartItemsTransactions")->getRetryIdDetailsOnNis("20128627009887888");
        $this->orderSearchForm = new OrderSearchForm();

        $this->formValid = "";
        if ($request->isMethod('post')) {
            $this->orderSearchForm->bind($request->getParameter('order'));
            if ($this->orderSearchForm->isValid()) {
                $this->formValid = "valid";
            }
        }
    }

    public function executeOrderPayment(sfWebRequest $request) {
        $this->orderSearchForm = new OrderSearchForm();
        $this->orderSearchForm->bind($request->getParameter('order'));

        if ($this->orderSearchForm->isValid()) {
            $postDataArray = $request->getParameterHolder()->getAll();
            $orderNumber = $postDataArray['order']['order_number'];

            # Get Order Request Details
            $OrdeReqDetails = Doctrine::getTable('ipay4meOrder')->getOrderDetails($orderNumber);
            if ($OrdeReqDetails) {
                $orderDetailObj = $OrdeReqDetails->getFirst();

                $this->getDetails = $orderDetailObj->getOrderRequestDetails();
                $this->orderDetails = $this->getDetails->getOrderRequest();
                $this->gatewayDetails = $orderDetailObj->getGateway();
                $this->userName = $orderDetailObj->getOrderRequestDetails()->getSfGuardUser()->getUsername();
                $this->orderNumber = $orderNumber;
            }
        }
    }

    public function executeMakePayment(sfWebRequest $request) {
        $orderNumber = $request->getParameter('orderNumber');

        # Get Order Details
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);
        $requestId = $ipay4meOrder->getFirst()->getOrderRequestDetailId();

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        if ($request_details) {
            $orderPaymentStatus = $request_details->getPaymentStatus();
        }
        $gatewayId = $ipay4meOrder->getFirst()->getGatewayId();

        $payOrderObj = new PaymentManager();
        $status = $payOrderObj->doPayment($orderNumber, $orderPaymentStatus, $requestId, $gatewayId);
        if ($status['status'] == 'success') {

            #Insert transaction in support tool payment table to keep a track for the payment updated by admin.
            $payOrderObj = new SupportToolPayment();
            $payOrderObj->setOrderId($orderNumber);
            $payOrderObj->setGatewayId($gatewayId);
            $payOrderObj->save();

            //display error message with error code
            $ErrorMsgObj = new ErrorMsg();
            $ErrorMsg = $ErrorMsgObj->displayErrorMessage("E016", "001000");
            $this->getUser()->setFlash('notice', $ErrorMsg);
            $this->redirect($this->getModuleName() . '/orderSearch');
        }
    }

    //search order no
    public function executeSearchOrderNo(sfWebRequest $request) {
        $this->isFound = false;
        $this->notRefundMessage = '';
        $template = $request->getParameter('template');
        if ($request->isMethod('POST')) {
            $this->ipayOrderNo = $request->getParameter("order_no");
        } else {
            $this->ipayOrderNo = Settings::decryptInput($request->getParameter("order_no"));
        }
        if (isset($this->ipayOrderNo) && $this->ipayOrderNo != '') {
            if (!is_numeric($this->ipayOrderNo)) {
                $this->getUser()->setFlash("error", "SW Global LLC order number is invalid.");
                $this->redirect("supportTool/searchOrderNo");
            }
            //get order details on ipay4me
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($this->ipayOrderNo);
            $gatewayId = $this->ipay4meOrderDetails[0]['gateway_id'];
            $this->gatewayName = Doctrine::getTable('Gateway')->getDisplayName($gatewayId);
            if ($gatewayId == '2' || $gatewayId == '3' || $gatewayId == '4' || $gatewayId == '6' || $gatewayId == '7') {
                $this->notRefundMessage = "Refund is not allowed here for this Payment Gateway";
            }

            $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($this->ipayOrderNo);

            $this->nisHelper = new NisHelper();
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($ipayNisDetailsQuery);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
            $this->appObj = CartItemsTransactionsTable::getInstance();
            $this->isFound = true;
            if ($template == 'blackListedReport') {
                $this->appDetailArray = $ipayNisDetailsQuery->execute()->toArray();
                $this->setTemplate('applicationBlackListedReport');
            }
        }
    }

    public function executeFindDuplicateNisApplication(sfWebRequest $request) {
        $applicationId = $request->getParameter("id");
        $applicationTYype = $request->getParameter("appType");
        $this->refNo = $request->getParameter('refNo');
        $this->backVar = $request->getParameter('hideBack');
        $this->setTemplate("appDetails");
        $this->nisHelper = new NisHelper();
        if (isset($applicationId) && $applicationId != '' && isset($applicationTYype) && is_string($applicationTYype) && $applicationTYype != '') {
            switch ($applicationTYype) {
                case "passport" :
                    $passportAppObj = PassportApplicationTable::getInstance();
                    $appObj = $passportAppObj->find($applicationId);
                    $firstName = $appObj->getFirstName();
                    $dob = $appObj->getDateOfBirth();
                    $duplicateAppQuery = $passportAppObj->findDuplicatePassportApplication($firstName, $dob);
                    //          echo $duplicateAppQuery->getSqlQuery();die;
                    //          echo "<pre>";print_r($duplicateAppQuery->execute(array(),Doctrine::HYDRATE_ARRAY));die;
                    $page = 1;
                    if ($request->hasParameter('page')) {
                        $page = $request->getParameter('page');
                    }
                    $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
                    $this->pager->setQuery($duplicateAppQuery);
                    $this->pager->setPage($this->getRequestParameter('page', $page));
                    $this->pager->init();
                    $this->appObj = CartItemsTransactionsTable::getInstance();
                    $this->ipayObj = ipay4meOrderTable::getInstance();
                    $this->gatewayObj = GatewayTable::getInstance();
                    $this->appType = "passport";
                    break;

                case "visa":
                case "freezone":
                    $visaAppObj = VisaApplicationTable::getInstance();
                    $appObj = $visaAppObj->find($applicationId);
                    $firstName = $appObj->getOtherName();
                    $dob = $appObj->getDateOfBirth();
                    $duplicateAppQuery = $visaAppObj->findDuplicateVisaApplication($firstName, $dob, $applicationTYype);
                    //          echo "<pre>";print_r($duplicateAppQuery->execute(array(),Doctrine::HYDRATE_ARRAY));die;
                    $page = 1;
                    if ($request->hasParameter('page')) {
                        $page = $request->getParameter('page');
                    }
                    $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
                    $this->pager->setQuery($duplicateAppQuery);
                    $this->pager->setPage($this->getRequestParameter('page', $page));
                    $this->pager->init();
                    $this->appObj = CartItemsTransactionsTable::getInstance();
                    $this->ipayObj = ipay4meOrderTable::getInstance();
                    $this->gatewayObj = GatewayTable::getInstance();
                    $this->appType = $applicationTYype;
                    break;

                case "vap":
                    $vapAppObj = VapApplicationTable::getInstance();
                    $appObj = $vapAppObj->find($applicationId);
                    $firstName = $appObj->getFirstName();
                    $dob = $appObj->getDateOfBirth();
                    $duplicateAppQuery = $vapAppObj->findDuplicateVapApplication($firstName, $dob);
                    //          echo "<pre>";print_r($duplicateAppQuery->execute(array(),Doctrine::HYDRATE_ARRAY));die;
                    $page = 1;
                    if ($request->hasParameter('page')) {
                        $page = $request->getParameter('page');
                    }
                    $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
                    $this->pager->setQuery($duplicateAppQuery);
                    $this->pager->setPage($this->getRequestParameter('page', $page));
                    $this->pager->init();
                    $this->appObj = CartItemsTransactionsTable::getInstance();
                    $this->ipayObj = ipay4meOrderTable::getInstance();
                    $this->gatewayObj = GatewayTable::getInstance();
                    $this->appType = 'vap';
                    break;
            }
        }
    }

    public function executeSearchApplication(sfWebRequest $request) {
        $apptype = ($request->hasParameter('apptype')) ? $request->getParameter('apptype') : '';
        $appID = $request->getParameter('app_id');
        $template = $request->getParameter('template');
        $this->orderNumber = $request->getParameter('orderNumber');
        if (isset($appID) && $appID != '') {
            $this->detailinfo = array();
            if ($apptype != 'passport' && $apptype != 'vap') {
                $appRecord = Doctrine::getTable('VisaApplication')->getVisaInfo($appID);
                if (isset($appRecord) && count($appRecord[0])) {
                    $ipayObj = ipay4meOrderTable::getInstance();
                    $ipay4meOrderDetails = null;
                    $czoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();
                    if ($appRecord[0]['zone_type_id'] == '' || $appRecord[0]['zone_type_id'] == $czoneTypeId) {
                        $this->detailinfo['visa']['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['other_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                        $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'v');
                        if (count($isRefunded) > 0) {
                            $this->detailinfo['visa']['status'] = $isRefunded[0]['action'];
                        } else {
                            $this->detailinfo['visa']['status'] = $appRecord[0]['status'];
                        }

                        $this->detailinfo['visa']['paid_at'] = $appRecord[0]['paid_at'];
                        $this->detailinfo['visa']['appid'] = $appRecord[0]['id'];
                        $this->detailinfo['visa']['refno'] = $appRecord[0]['ref_no'];
                        $this->detailinfo['visa']['date_of_birth'] = $appRecord[0]['date_of_birth'];
                        $duplicateAppData = VisaApplicationTable::getInstance()->findDuplicateVisaApplication(null, null, "visa", $appID);
                        $orderNumber = $duplicateAppData[0]['order_no'];
                        if ($orderNumber != 0) {
                            $index = strpos($orderNumber, ",");
                            if ($index) {
                                $appOrderDetails = substr($orderNumber, 0, $index);
                            } else if ($orderNumber != 0) {
                                $appOrderDetails = $orderNumber;
                            }
                            if ($appOrderDetails != 0)
                                $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                            $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                        }
                        if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                            $this->detailinfo['visa']['gatewayName'] = $gatewayName;
                            $this->detailinfo['visa']['order_no'] = $appOrderDetails;
                            $this->detailinfo['visa']['email'] = $ipay4meOrderDetails[0]['email'];
                        } else {
                            $this->detailinfo['visa']['gatewayName'] = "Not Applicable";
                            $this->detailinfo['visa']['order_no'] = "Not Applicable";
                            $this->detailinfo['visa']['email'] = "Not Applicable";
                        }
                    } else {
                        $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'v');
                        if (count($isRefunded) > 0) {
                            $this->detailinfo['freezone']['status'] = $isRefunded[0]['action'];
                        } else {
                            $this->detailinfo['freezone']['status'] = $appRecord[0]['status'];
                        }
                        $this->detailinfo['freezone']['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['other_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                        $this->detailinfo['freezone']['paid_at'] = $appRecord[0]['paid_at'];
                        $this->detailinfo['freezone']['appid'] = $appRecord[0]['id'];
                        $this->detailinfo['freezone']['refno'] = $appRecord[0]['ref_no'];
                        $this->detailinfo['freezone']['date_of_birth'] = $appRecord[0]['date_of_birth'];
                        $duplicateAppData = VisaApplicationTable::getInstance()->findDuplicateVisaApplication(null, null, "freezone", $appID);
                        $orderNumber = $duplicateAppData[0]['order_no'];
                        if ($orderNumber != 0) {
                            $index = strpos($orderNumber, ",");
                            if ($index) {
                                $appOrderDetails = substr($orderNumber, 0, $index);
                            } else if ($orderNumber != 0) {
                                $appOrderDetails = $orderNumber;
                            }
                            if ($appOrderDetails != 0)
                                $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                            $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                        }
                        if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                            $this->detailinfo['freezone']['gatewayName'] = $gatewayName;
                            $this->detailinfo['freezone']['order_no'] = $appOrderDetails;
                            $this->detailinfo['freezone']['email'] = $ipay4meOrderDetails[0]['email'];
                        } else {
                            $this->detailinfo['freezone']['gatewayName'] = "Not Applicable";
                            $this->detailinfo['freezone']['order_no'] = "Not Applicable";
                            $this->detailinfo['freezone']['email'] = "Not Applicable";
                        }
                        //            echo "<pre>";print_r($duplicateAppQuery);
                    }
                }
            }
            if ($apptype == 'vap') {
                $appRecord = Doctrine::getTable('VapApplication')->getVisaInfoById($appID);
                if (isset($appRecord) && count($appRecord[0])) {
                    $ipayObj = ipay4meOrderTable::getInstance();
                    $ipay4meOrderDetails = null;
                    $this->detailinfo['vap']['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['first_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                    $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'vap');


                    if (count($isRefunded) > 0) {
                        $this->detailinfo['vap']['status'] = $isRefunded[0]['action'];
                    } else {
                        $this->detailinfo['vap']['status'] = $appRecord[0]['status'];
                    }

                    $this->detailinfo['vap']['paid_at'] = $appRecord[0]['paid_date'];
                    $this->detailinfo['vap']['appid'] = $appRecord[0]['id'];
                    $this->detailinfo['vap']['refno'] = $appRecord[0]['ref_no'];
                    $this->detailinfo['vap']['date_of_birth'] = $appRecord[0]['date_of_birth'];
                    $duplicateAppData = VapApplicationTable::getInstance()->findDuplicateVapApplication(null, null, "vap", $appID);

                    $orderNumber = $duplicateAppData[0]['order_no'];
                    if ($orderNumber != 0) {
                        $index = strpos($orderNumber, ",");
                        if ($index) {
                            $appOrderDetails = substr($orderNumber, 0, $index);
                        } else if ($orderNumber != 0) {
                            $appOrderDetails = $orderNumber;
                        }
                        if ($appOrderDetails != 0)
                            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                        $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                    }
                    if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                        $this->detailinfo['vap']['gatewayName'] = $gatewayName;
                        $this->detailinfo['vap']['order_no'] = $appOrderDetails;
                        $this->detailinfo['vap']['email'] = $ipay4meOrderDetails[0]['email'];
                    } else {
                        $this->detailinfo['vap']['gatewayName'] = "Not Applicable";
                        $this->detailinfo['vap']['order_no'] = "Not Applicable";
                        $this->detailinfo['vap']['email'] = "Not Applicable";
                    }
                }
            }
            if ($apptype == 'passport') {
                $appRecord = Doctrine::getTable('PassportApplication')->getPassportInfo($appID);
                if (isset($appRecord) && count($appRecord[0])) {
                    $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'p');
                    // echo '<pre>';print_r(count($isRefunded));die;
                    $ipayObj = ipay4meOrderTable::getInstance();
                    $this->detailinfo['passport']['name'] = ucwords($appRecord[0]['title_id'] . " " . $appRecord[0]['first_name'] . " " . $appRecord[0]['mid_name'] . " " . $appRecord[0]['last_name']);
                    if (count($isRefunded) > 0) {
                        $this->detailinfo['passport']['status'] = $isRefunded[0]['action'];
                    } else {
                        $this->detailinfo['passport']['status'] = $appRecord[0]['status'];
                    }
                    $this->detailinfo['passport']['paid_at'] = $appRecord[0]['paid_at'];
                    $this->detailinfo['passport']['appid'] = $appRecord[0]['id'];
                    $this->detailinfo['passport']['refno'] = $appRecord[0]['ref_no'];
                    $this->detailinfo['passport']['date_of_birth'] = $appRecord[0]['date_of_birth'];
                    $duplicateAppData = PassportApplicationTable::getInstance()->findDuplicatePassportApplication(null, null, $appID);
                    $orderNumber = $duplicateAppData[0]['order_no'];
                    if ($orderNumber != 0) {
                        $index = strpos($orderNumber, ",");
                        if ($index) {
                            $appOrderDetails = substr($orderNumber, 0, $index);
                        } else if ($orderNumber != 0) {
                            $appOrderDetails = $orderNumber;
                        }
                        if ($appOrderDetails != 0)
                            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                        $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                    }
                    if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                        $this->detailinfo['passport']['gatewayName'] = $gatewayName;
                        $this->detailinfo['passport']['order_no'] = $appOrderDetails;
                        $this->detailinfo['passport']['email'] = $ipay4meOrderDetails[0]['email'];
                    } else {
                        $this->detailinfo['passport']['gatewayName'] = "Not Applicable";
                        $this->detailinfo['passport']['order_no'] = "Not Applicable";
                        $this->detailinfo['passport']['email'] = "Not Applicable";
                    }
                    //          echo "<pre>";print_r($duplicateAppData);
                }
            }
            if ($apptype == 'vap') {
                $appRecord = Doctrine::getTable('VapApplication')->getVisaArrivalInfo($appID);

                if (isset($appRecord) && count($appRecord[0])) {
                    //            $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'p');
                    $ipayObj = ipay4meOrderTable::getInstance();
                    $this->detailinfo['vap']['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['first_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                    //            if (count($isRefunded) > 0) {
                    //              $this->detailinfo['passport']['status'] = $isRefunded[0]['action'];
                    //            } else {
                    $this->detailinfo['vap']['status'] = $appRecord[0]['status'];
                    //            }
                    $this->detailinfo['vap']['paid_at'] = $appRecord[0]['paid_date'];
                    $this->detailinfo['vap']['appid'] = $appRecord[0]['id'];
                    $this->detailinfo['vap']['refno'] = $appRecord[0]['ref_no'];
                    $this->detailinfo['vap']['date_of_birth'] = $appRecord[0]['date_of_birth'];
                    $duplicateAppData = VapApplicationTable::getInstance()->findDuplicateVapApplication(null, null, null, $appID);
                    $orderNumber = $duplicateAppData[0]['order_no'];
                    //            $orderNumber = '132807890588224';
                    if ($orderNumber != 0) {
                        $index = strpos($orderNumber, ",");
                        if ($index) {
                            $appOrderDetails = substr($orderNumber, 0, $index);
                        } else if ($orderNumber != 0) {
                            $appOrderDetails = $orderNumber;
                        }
                        if ($appOrderDetails != 0)
                            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                        $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                    }
                    if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                        $this->detailinfo['vap']['gatewayName'] = $gatewayName;
                        $this->detailinfo['vap']['order_no'] = $appOrderDetails;
                        $this->detailinfo['vap']['email'] = $ipay4meOrderDetails[0]['email'];
                    } else {
                        $this->detailinfo['vap']['gatewayName'] = "Not Applicable";
                        $this->detailinfo['vap']['order_no'] = "Not Applicable";
                        $this->detailinfo['vap']['email'] = "Not Applicable";
                    }
                }
            }


            if ($template == 'MO') {
                $this->setTemplate('viewMOApplication');
            } else {
                $this->setTemplate('viewAllApplication');
            }

            if (count($this->detailinfo) > 0) {

            } else {
                $this->getUser()->setFlash('notice', 'Application not found.', false);
            }
        }
    }

    public function executeSearchBySwLlcAccount(sfWebRequest $request) {
        $this->setTemplate("searchApplicationBySwLlc");
        $this->emailId = $request->getParameter("acoount_email");
        $this->isFound = false;
        if (isset($this->emailId) && $this->emailId != '') {
            $userDetails = UserDetailTable::getInstance()->findByEmail($this->emailId);
            $userCount = $userDetails->count();
            if ($userCount) {
                $userDetails = $userDetails->toArray();
                $userId = $userDetails[0]['user_id'];
                $this->name = $userDetails[0]['first_name'] . " " . $userDetails[0]['last_name'];
                $this->email = $userDetails[0]['email'];
                $this->address = $userDetails[0]['address'];
                $this->phone = $userDetails[0]['mobile_phone'];
                $detailsQuery = ipay4meOrderTable::getInstance()->getPurchaseHistoryByEmailAddress($userId);

                $page = 1;
                if ($request->hasParameter('page')) {
                    $page = $request->getParameter('page');
                }
                $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($detailsQuery);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
                $this->isFound = true;
            } else {
                $this->getUser()->setFlash("error", "No record found");
            }
        }
    }

    public function executeOrderAction(sfWebRequest $request) {
        $this->action = Settings::decryptInput($request->getParameter("pAction"));

        $ErrorMsgObj = new ErrorMsg();
        $orderNumber = Settings::decryptInput($request->getParameter("order_no"));
        $comment = $request->getParameter("comment");
        $arrAppId = $request->getParameter("chk_fee");
        //      echo "<pre>";print_r($arrAppId);die;
        $isVlid = ipay4meOrderTable::getInstance()->findByOrderNumber($orderNumber)->count();
        if ($isVlid) {

            //get order details on ipay4me
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);
            $price = ipay4meOrderTable::getInstance()->getOrderNumberPrice($orderNumber);
            if ($price[0]['currency'] == '1') {
                $this->totAmount = $price[0]['amount'];
            } else {
                $this->totAmount = $price[0]['convert_amount'];
            }
            $this->currencySymbol = CurrencyManager::currencySymbolByCurrencyId($price[0]['currency']);
            $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);
            $this->nisHelper = new NisHelper();
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PassportApplication', 40);
            $this->pager->setQuery($ipayNisDetailsQuery);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
            $this->appObj = CartItemsTransactionsTable::getInstance();
            if ($comment) {
                $paymentManager = new PaymentManager();

                $isValidDetails = $this->nisHelper->isValidRefund($orderNumber);

                if ($isValidDetails['nisStatus'] && $isValidDetails['ipay4meStatus']) {
                    $gatewayId = ipay4meOrderTable::getInstance()->getGatewayId($orderNumber);
                    switch ($this->action) {
                        case "Refund" :
                            if ($arrAppId) {
                                $this->isRefundAllowed($orderNumber, $gatewayId, $this->action, $paymentManager);
                                $refundReq = $paymentManager->refundPayment($orderNumber, $comment, '', $arrAppId, $gatewayId);
                                if ($refundReq) {
                                    //revert NIS Application
                                    for ($i = 0; $i < count($arrAppId); $i++) {
                                        $explode = explode('_', $arrAppId[$i]);
                                        $revertStatus = $this->nisHelper->revertStatus($explode[0], $explode[1]);

                                        /**
                                         * [WP: 112] => CR: 158
                                         * Updating payment status set to 2 for refund...
                                         */
                                        Functions::updateApplicationPaymentStatus($explode[0], $explode[1], $orderNumber, $status = 2);

                                    }

                                    $this->getUser()->setFlash("notice", "SW Global LLC order number " . $orderNumber . " has successfully refunded.");
                                    //$this->redirect("supportTool/orderAction?pAction=".$this->action);
                                    $this->redirect("supportTool/searchOrderNo");
                                } else {
                                    $this->getUser()->setFlash("error", "Due to some problem refund request can not process.");
                                    $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                                }
                            } else {
                                $this->getUser()->setFlash("error", "All application of this order number " . $orderNumber . " has already been refunded.");
                                $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                            }
                            break;

                        case "Refund and Block" :
                            if ($arrAppId) {
                                $refundStatus = $this->nisHelper->isAllReadyRefunded($orderNumber);

                                if (isset($refundStatus) && is_array($refundStatus) && count($refundStatus) > 0) {
                                    $this->getUser()->setFlash("error", "Some of application of this order number " . $orderNumber . " has already been " . ucfirst($refundStatus['action']) . "ed. so this order number can not be refund and blocked");
                                    $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                                } else {
                                    $this->isRefundAllowed($orderNumber, $gatewayId, $this->action, $paymentManager);
                                    $refundBlockReq = $paymentManager->refundAndBlockPayment($orderNumber, $comment, '', $arrAppId, $gatewayId);

                                    if ($refundBlockReq) {
                                        //revert NIS Application
                                        $revertStatus = $this->nisHelper->revertNisApplicationStatus($orderNumber);
                                        $allOrderNumber = $paymentManager->getAllPaymentByCard($orderNumber);
                                        if (!empty($allOrderNumber)) {
                                            foreach ($allOrderNumber as $key => $value) {
                                                $revertStatus = $this->nisHelper->revertNisApplicationStatus($value['order_number']);
                                            }
                                        }

                                        $this->getUser()->setFlash("notice", "SW Global LLC order number " . $orderNumber . " has successfully refunded and user has blocked");
                                        $this->redirect("supportTool/searchOrderNo");
                                    } else {
                                        //                      $this->getUser()->setFlash("error","Due to some problem refund request can not process.");
                                        $ErrorMsg = $ErrorMsgObj->displayErrorMessage("E025", "001000");
                                        $this->getUser()->setFlash("error", $ErrorMsg);
                                        $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                                    }
                                }
                            } else {
                                $this->getUser()->setFlash("error", "All application of this order number " . $orderNumber . " has already been refunded.");
                                $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                            }
                            break;
                        case "Chargeback" :
                            if ($arrAppId) {
                                $refundStatus = $this->nisHelper->isAllReadyRefunded($orderNumber);
                                if (isset($refundStatus) && is_array($refundStatus) && count($refundStatus) > 0) {
                                    $this->getUser()->setFlash("error", "Some of application of this order number " . $orderNumber . " has already been " . ucfirst($refundStatus['action']) . "ed. so this order number can not be chargebacked");
                                    $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                                } else {
                                    $chargeBackReq = $paymentManager->chargebackPayment($orderNumber, $comment, $arrAppId);
                                    if ($chargeBackReq) {
                                        //revert NIS Application
                                        $revertStatus = $this->nisHelper->revertNisApplicationStatus($orderNumber);
                                        $allOrderNumber = $paymentManager->getAllPaymentByCard($orderNumber);
                                        if (!empty($allOrderNumber)) {
                                            foreach ($allOrderNumber as $key => $value) {
                                                $revertStatus = $this->nisHelper->revertNisApplicationStatus($value['order_number']);
                                            }
                                        }
                                        /**
                                         * [WP: 112] => CR: 158
                                         * Updating payment status set to 3...
                                         */
                                        Functions::updateTransactionServiceChargesPaymentStatus($orderRequestDetailId, $status = 3);

                                        $this->getUser()->setFlash("notice", "SW Global LLC order number " . $orderNumber . " has successfully chargedback");
                                        $this->redirect("supportTool/searchOrderNo");
                                    } else {
                                        //                      $this->getUser()->setFlash("error","Due to some problem chargeback request can not process.");
                                        $ErrorMsg = $ErrorMsgObj->displayErrorMessage("E029", "001000");
                                        $this->getUser()->setFlash("error", $ErrorMsg);
                                        $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                                    }
                                }
                            } else {
                                $this->getUser()->setFlash("error", "All application of this order number " . $orderNumber . " has already been refunded.");
                                $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($this->action) . "&order_no=" . Settings::encryptInput($orderNumber));
                            }
                            break;
                    }
                } else {
                    if (!$isValidDetails['ipay4meStatus']) {
                        $this->getUser()->setFlash("error", "SW Global LLC Order Number is not paid");
                    } else if (!$isValidDetails['nisStatus']) {
                        $this->getUser()->setFlash("error", "Order can not refund or chargeback, one or more NIS Application associated with this order number has already process.");
                    }
                }
            }
            $this->orderNumber = $orderNumber;
        } else {
            $this->getUser()->setFlash("error", "SW Global LLC Order Number is invalid");
        }
    }

    public function executeGetIpay4OrderPrice(sfWebRequest $request) {
        $price = ipay4meOrderTable::getInstance()->getOrderNumberPrice($request->getParameter("orderNo"));
        if (isset($price) && is_array($price) && count($price) > 0)
            $this->price = $price[0]['unit_price'];
        $this->setTemplate('getPrice');
    }

    public function executeSearchByCardDetails(sfWebRequest $request) {
        $this->setTemplate("searchByCardDetails");
        if ($request->isMethod('POST')) {
            $this->firstDigit = $request->getParameter("first_digit");
            $this->lastDigit = $request->getParameter("last_digit");
        } else {
            $this->firstDigit = Settings::decryptInput($request->getParameter("first_digit"));
            $this->lastDigit = Settings::decryptInput($request->getParameter("last_digit"));
        }
        $this->isFound = false;
        if (isset($this->firstDigit) && $this->firstDigit != '' && isset($this->lastDigit) && $this->lastDigit != '') {

            $detailsQuery = ipay4meOrderTable::getInstance()->getOrderNumberDetailsByCardDetails($this->firstDigit, $this->lastDigit);
            if (isset($this->firstDigit[3])) {
                $this->cardReportArray = ipay4meOrderTable::getInstance()->getReportByCardNumber($this->firstDigit, $this->lastDigit);
                $aplicantVaultArray = Doctrine::getTable('ApplicantVault')->getCardDetailsByFirstAndSecond($this->firstDigit, $this->lastDigit);
                $this->registerCardCount = count($aplicantVaultArray);
                $this->registerDetails = array();
                if (!empty($aplicantVaultArray)) {
                    foreach ($aplicantVaultArray as $details) {
                        $userEmail = Doctrine::getTable('UserDetail')->findByUserId($details['user_id'])->getLast()->toArray();
                        $this->registerDetails[] = $userEmail['email'];
//                        echo "<pre>";print_r($userEmail);die;
                    }
//                    $registerDetails =
                }
            }
//            print_r($this->registerDetails);

            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new rawDocPager(null, 10);
            $this->pager->setStatement($detailsQuery);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
            $this->isFound = true;
        } else {
            if ($request->hasParameter("first_digit"))
                $this->getUser()->setFlash("error", "Please insert card first or last digits", false);
        }
    }

    public function executeBlackListedCards(sfWebRequest $request) {
        $this->showDataFlag = false;
        if ($request->isMethod('POST')) {
            $this->email = $request->getParameter('email');
            $this->card_first = $request->getParameter('card_first');
            $this->card_last = $request->getParameter('card_last');
            $this->showDataFlag = true;
            if ($this->email != '') {
                $detailsQuery = FpsDetailTable::getInstance()->getBlackListCardDetails(trim($this->email));
            } else if ($this->card_first != '' && $this->card_last != '') {
                $cardHash = Ipay4meOrderTable::getInstance()->getCardHashByCardDetails($this->card_first, $this->card_last);
                if (count($cardHash) > 0) {
                    $detailsQuery = FpsDetailTable::getInstance()->getBlackListCardDetails('', $cardHash);
                } else {
                    $this->getUser()->setFlash('notice', 'No record exist by this card number.');
                    $this->redirect('supportTool/blackListedCards');
                }
            }
            $this->pager = new rawDocPager(null, 10);
            $this->pager->setStatement($detailsQuery);
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
        }
    }

    public function executeUnblockCards(sfWebRequest $request) {
        $orderNumber = $request->getParameter('order');
        $cardNumber = $request->getParameter('card');
        $detailsQuery = FpsDetailTable::getInstance()->UnblockCards($orderNumber);
        $this->getUser()->setFlash('notice', 'Card: ' . $cardNumber . ' has been unblocked');
        $this->redirect('supportTool/blackListedCards');
    }

    public function executeUnblockUndefinedCards(sfWebRequest $request) {
        $fps_id = trim($request->getParameter('fps_id'));
        $fpsObj = Doctrine::getTable('FpsDetail')->find($fps_id);
        if (!empty($fpsObj)) {
            Doctrine::getTable('FpsDetail')->UnblockCardsByCardNum($fpsObj->getCardNum());
            $msg = 'Card has been unblocked successfully.';
        } else {
            $msg = 'Card has not been unblocked.';
        }
        $this->getUser()->setFlash('notice', $msg);
        $this->redirect('supportTool/blackListedCards');
    }

    public function executeViewComments(sfWebRequest $request) {
        $orderNumber = Settings::decryptInput($request->getParameter('order'));
        $this->recordDetails = RollbackPaymentTable::getInstance()->getAllRecordsByOrderNumber($orderNumber);
        $this->setLayout(false);
    }

    public function executeViewReportDetail(sfWebRequest $request) {
        $orderNumber = $request->getParameter('order');
        $this->recordDetails = SupportRefundListTable::getInstance()->getAllRecordsByOrderNumber($orderNumber);
        $this->setLayout(false);
    }

    public function executeRefundReport(sfWebRequest $request) {
        $detailsQuery = SupportRefundListTable::getInstance()->getAllRecords();

        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('SupportRefundList', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($detailsQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
    }

    function isRefundAllowed($orderNumber, $gatewayId, $action, $paymentManager) {
        $isRefundAllowed = $paymentManager->isRefundAllowed($gatewayId);
        if (!$isRefundAllowed) {
            $gatewayName = GatewayTable::getInstance()->find($gatewayId)->getDisplayName();
            $this->getUser()->setFlash("error", "Refund is not allowed for this order as it has been paid using $gatewayName gateway.");
            $this->redirect("supportTool/orderAction?pAction=" . Settings::encryptInput($action) . "&order_no=" . Settings::encryptInput($orderNumber));
        }
    }

    public function executeUserListAuth(sfWebRequest $request) {

        if ($request->getParameter('mode')) {
            $this->mode = $request->getParameter('mode');
        } else {
            $this->mode = '';
        }
        $back = $request->getParameter('back');

        if ($back) {
            ## Getting search data...
            $this->checkedVal = $this->getUser()->getAttribute('checkedVal');
            $this->email = $this->getUser()->getAttribute('email');
            $this->fdate = $this->getUser()->getAttribute('fdate');
            $this->tdate = $this->getUser()->getAttribute('tdate');
            $this->page = $this->getUser()->getAttribute('page');
        } else {
            $this->checkedVal = '';
            $this->email = '';
            $this->fdate = '';
            $this->tdate = '';
            $this->page = '';

            ## Removing search data...processAction
            $this->getUser()->getAttributeHolder()->remove('checkedVal');
            $this->getUser()->getAttributeHolder()->remove('email');
            $this->getUser()->getAttributeHolder()->remove('fdate');
            $this->getUser()->getAttributeHolder()->remove('tdate');
            $this->getUser()->getAttributeHolder()->remove('page');
        }
    }

    public function executePendingUserAuthList(sfWebRequest $request) {

        ## Getting paging number to maintain paging data...
        //      $checkedVal = $request->getParameter('checkedVal');
        $email = $request->getParameter('email');
        $fdate = $request->getParameter('fdate');
        $tdate = $request->getParameter('tdate');

        //      $this->getUser()->setAttribute('checkedVal', $checkedVal);
        $this->getUser()->setAttribute('email', $email);
        $this->getUser()->setAttribute('fdate', $fdate);
        $this->getUser()->setAttribute('tdate', $tdate);

        $this->setLayout(NULL);

        //      if ($checkedVal == 'normal') {
        $userAuthListQuery = ApplicantVaultTable::getInstance()->getUserAuthList($email, $fdate, $tdate);
        //      }
        //       else {
        //        $userAuthListQuery = ApplicantVaultTable::getInstance()->getFilteredUserAuthList($email, $fdate, $tdate);
        //      }

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('ApplicantVault', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userAuthListQuery);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
        $this->setTemplate("pendingList");
    }

    public function executeAuthDetails(sfWebRequest $request) {

        ## Getting paging number to maintain paging data...
        $page = $request->getParameter('page');
        $this->getUser()->setAttribute('page', $page);


        $userAuthId = Settings::decryptInput($request->getParameter("id"));
        $this->userAuthDetailsObj = Doctrine::getTable("ApplicantVault")->find($userAuthId);
        //check if user details already defined
        if ($this->userAuthDetailsObj->getTransactionType() == 'Yes') {
            $user_limit = Doctrine::getTable('UserTransactionLimit')->findByUserId($this->userAuthDetailsObj->getCreatedBy());
            //        echo  $user_limit->getFirst();die;
            if ($user_limit->getFirst()) {
                $this->cartCapacity = $user_limit->getfirst()->getCartCapacity();
                $this->CartAmountCapacity = $user_limit->getfirst()->getCartAmountCapacity();
                $this->NumberOfTransaction = $user_limit->getFirst()->getNumberOfTransaction();
                $this->TransactionPeriod = $user_limit->getfirst()->getTransactionPeriod();
            }
        }

        if ($this->userAuthDetailsObj->getStatus() == 'New') {
            $approvedCard = Doctrine::getTable("ApplicantVault")->getApprovedCardByHash($this->userAuthDetailsObj->getCardHash());
            if (!empty($approvedCard)) {
                $this->isApproval = false;
                $userEmail = Doctrine::getTable("UserDetail")->findByUserId($approvedCard[0]['user_id'])->toArray();
                $this->email = $userEmail[0]['email'];
                $this->msg = "This card is already approved and associated with " . $this->email;
            }
        } else if ($this->userAuthDetailsObj->getStatus() == 'Approved') {
            $this->isApproval = true;
            $this->msg = "This card is already approved.";
        } elseif ($this->userAuthDetailsObj->getStatus() == 'Rejected') {
            $this->isApproval = false;
            $this->msg = "This request is already rejected.";
        }

        $this->status = $this->userAuthDetailsObj->getStatus();
        $this->user_id = $this->userAuthDetailsObj->getCreatedBy();

        $this->isApprove = ApplicantVaultTable::getInstance()->getDeplicateCardDetails($this->userAuthDetailsObj->getCardHash());
        $this->userAuthDetails = $this->userAuthDetailsObj->toArray();
        $this->setTemplate("userAuthDetails");
    }

    // Function added to call a template after clicking on detail link after credit card number search
    public function executeAuthDetailsCredit(sfWebRequest $request) {
        $userAuthId = Settings::decryptInput($request->getParameter("id"));
        $this->userAuthDetailsObj = Doctrine::getTable("ApplicantVault")->find($userAuthId);

        if ($this->userAuthDetailsObj->getStatus() == 'New') {
            $approvedCard = Doctrine::getTable("ApplicantVault")->getApprovedCardByHash($this->userAuthDetailsObj->getCardHash());
            if (!empty($approvedCard)) {
                $this->isApproval = false;
                $userEmail = Doctrine::getTable("UserDetail")->findByUserId($approvedCard[0]['created_by'])->toArray();
                $this->email = $userEmail[0]['email'];
                $this->msg = "This card is already approved and associated with " . $this->email;
            } else {
                $this->isApproval = false;
                $this->msg = '';
            }
        } else if ($this->userAuthDetailsObj->getStatus() == 'Approved') {
            $this->isApproval = true;
            //$this->comments = $this->userAuthDetailsObj->getComments();
            $this->msg = "This card is already approved.";
        } elseif ($this->userAuthDetailsObj->getStatus() == 'Rejected') {
            $this->isApproval = false;
            $this->msg = "This request is already rejected.";
        }

        $this->status = $this->userAuthDetailsObj->getStatus();

        $this->isApprove = ApplicantVaultTable::getInstance()->getDeplicateCardDetails($this->userAuthDetailsObj->getCardHash());
        $this->userAuthDetails = $this->userAuthDetailsObj->toArray();

        $this->setTemplate("userAuthDetailsCredit");
    }

    public function executeViewStatement(sfWebRequest $request) {
        $userAuthId = $request->getParameter("id");
        $userAuthDetails = Doctrine::getTable("ApplicantVault")->find($userAuthId)->toArray();
        $fileNme = $userAuthDetails['uploaded_file'];
        $path = _compute_public_path($fileNme, 'uploads/applicant_profile', '', true);
        $output = "<img src='" . $path . "'></img>";
        $this->renderText($output);
    }

    public function executeProcessAction(sfWebRequest $request) {
        // echo "<pre>";print_r($_REQUEST);die;
        $authId = $request->getParameter("auth_id");
        $transaction_type = $request->getParameter("transaction_type");
        if (!isset($authId) || $authId == '') {
            //        $this->getUser()->setFlash("error", "Invalid Request.");
            $ErrorMsgObj = new ErrorMsg();
            $ErrorMsg = $ErrorMsgObj->displayErrorMessage("E040", "001000");
            $this->getUser()->setFlash("error", $ErrorMsg);
            $this->redirect("supportTool/userListAuth/back/1");
        }
        $comments = $request->getParameter("approver_comments");
        $userAuthDetails = Doctrine::getTable("ApplicantVault")->find($authId);
        $userAuthId = $userAuthDetails->getCreatedBy();
        $addressProof = $userAuthDetails->getAddressProof();
        $user_id_proof = $userAuthDetails->getUserIdProof();
        $agent_proof = $userAuthDetails->getAgentProof();


        if (!isset($comments) || $comments == '') {
            $this->getUser()->setFlash("error", "Please provide comments.");
            $this->redirect("supportTool/authDetails?id=" . $authId);
        }
        $action = '';

        if ($request->hasParameter("approve")) {
            $action = "Approved";
        } else if ($request->hasParameter("reject")) {
            $action = "Rejected";
        }
        if (isset($action) && $action != '') {
            //ready to chenge status of auth
            $userAuthDetails->setStatus($action);
            $userAuthDetails->setComments($comments);
            $userAuthDetails->save();

            $app_Var = sfConfig::get('app_delete_useruploaded_document');
            if ($action == "Approved" && $app_Var == "ON") {
                if (file_exists(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $addressProof) && !empty($addressProof)) {
                    unlink(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $addressProof);
                }
                if (file_exists(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $user_id_proof) && !empty($user_id_proof)) {
                    unlink(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $user_id_proof);
                }
                if (file_exists(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $agent_proof) && !empty($agent_proof)) {
                    unlink(sfConfig::get('sf_upload_dir') . '/applicant_profile/' . $agent_proof);
                }
            }

            ## If transaction type is 'no' then no need to change user transaction limit table...
            if (strtolower('Yes') == strtolower($transaction_type)) {
                $user_limit = Doctrine::getTable('UserTransactionLimit')->findByUserId($request->getParameter('user_id'));
                if ($user_limit->getFirst()) {
                    $updateIp4mOrder = Doctrine::getTable('UserTransactionLimit')->updateUserTransactionLimitRegisterCard($request->getParameter('user_id'), $request->getParameter('cart_capacity'), $request->getParameter('cart_amount_capacity'), $request->getParameter('number_of_transaction'), '', '');
                } else {
                    $save = Doctrine::getTable('UserTransactionLimit')->addUserTransactionLimit($request->getParameter('user_id'), $request->getParameter('cart_capacity'), $request->getParameter('cart_amount_capacity'), $request->getParameter('number_of_transaction'), '', '');
                }
            }
            $this->getUser()->setFlash("notice", "Request successfully " . $action . ".");
            //send mail to the customer, for application submission
            $mailUrl = "notification/creditCardProcessMail";
            $mailTaskId = EpjobsContext::getInstance()->addJob('CreditCardProcessRequestMail', $mailUrl, array('request_id' => $authId, "paction" => $action, "comments" => $comments, "user_id" => $userAuthId));

            $this->redirect("supportTool/userListAuth?back=1&mode=" . $action);
        }
    }

    // Function added to redirect page back to Credit Card list search after appropriate action taken
    public function executeProcessActionCredit(sfWebRequest $request) {

        $authId = $request->getParameter("auth_id");
        if (!isset($authId) || $authId == '') {
            $this->getUser()->setFlash("error", "Invalid Request.");
            $this->redirect("supportTool/searchByCardNumber");
        }
        $comments = $request->getParameter("approver_comments");
        $userAuthDetails = Doctrine::getTable("ApplicantVault")->find($authId);
        $userAuthId = $userAuthDetails->getCreatedBy();

        if (!isset($comments) || $comments == '') {
            $this->getUser()->setFlash("error", "Please provide comments.");
            $this->redirect("supportTool/authDetailsCredit?id=" . $authId);
        }
        $action = '';

        if ($request->hasParameter("approve")) {
            $action = "Approved";
        } else if ($request->hasParameter("reject")) {
            $action = "Rejected";
        }
        if (isset($action) && $action != '') {
            //ready to chenge status of auth
            $userAuthDetails->setStatus($action);
            $userAuthDetails->setComments($comments);
            $userAuthDetails->save();
            $this->getUser()->setFlash("notice", "Request successfully " . $action . ".");
            //send mail to the customer, for application submission
            $mailUrl = "notification/creditCardProcessMail";
            $mailTaskId = EpjobsContext::getInstance()->addJob('CreditCardProcessRequestMail', $mailUrl, array('request_id' => $authId, "paction" => $action, "comments" => $comments, "user_id" => $userAuthId));

            $this->redirect("supportTool/searchByCardNumber");
        }
    }

    // Function added to list the search results after Searching via credit number or first or last four digits
    public function executeSearchByCardNumber(sfWebRequest $request) {
        $this->setTemplate("searchByCardNumber");
        $this->firstDigit = $request->getParameter("first_digit");
        $this->lastDigit = $request->getParameter("last_digit");
        $this->digits = $request->getParameter("digits");
        $this->name = $request->getParameter("name");
        if ($this->digits) {
            $this->creditCard = md5($this->digits);
        }
        $this->isFound = false;
        if (isset($this->firstDigit) && $this->firstDigit != '' || isset($this->lastDigit) && $this->lastDigit != '' || isset($this->digits) && $this->digits != '' || isset($this->name) && $this->name != '') {

            $detailsQuery = ApplicantVaultTable::getInstance()->getDetailsByCardNumber($this->firstDigit, $this->lastDigit, $this->creditCard, $this->name);

            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('ApplicantVault', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($detailsQuery);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
            $this->isFound = true;
        }//else{
        //        if($request->hasParameter("first_digit"))
        //        $this->getUser()->setFlash("error", "Please insert card first or last digits",false);
        //      }
    }

    ##Print Receipt After Payment for VISA...

    public function printVisaRecipt($visa_app_id, $visa_app_refId) {
        $VisaAppID = SecureQueryString::DECODE($visa_app_id);
        $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
        $VisaRef = SecureQueryString::DECODE($visa_app_refId);
        $VisaRef = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
        //$VisaReportType = $request->getParameter('printType');

        $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId($VisaAppID, $VisaRef);
        if (!$visaFound) {
            // TODO redirect to not found error page
            //$this->setVar('errMsg','Application Not Found!! Please try again.');
            //$this->setTemplate('VisaStatus');
            return;
        }
        $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaAppID);
        $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaAppID);

        $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
        $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        $freezoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();

        if ($isFreshEntry != '' || $isFreezoneFreshEntry != '') {
            //For show the Information
            $this->visa_application = $this->getVisaFreshRecord($VisaAppID);
            $cat_id = $this->visa_application[0]['visacategory_id'];
            $country_id = $this->visa_application[0]['present_nationality_id'];
            $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
            $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
            $zone_id = $this->visa_application[0]['zone_type_id'];
            $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];

            $visa_count_id = $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'];
            //Get Processing Country Name
            $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);
        } else {
            $this->visa_application = $this->getVisaReEntryRecord($VisaAppID);
            $cat_id = $this->visa_application[0]['visacategory_id'];
            $country_id = $this->visa_application[0]['present_nationality_id'];
            $visa_id = $this->visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
            $entry_id = $this->visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
            $no_of_entry = $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
            $zone_id = $this->visa_application[0]['zone_type_id'];
            //$this->forward404Unless($this->visa_application);
        }
        if ($cat_id == $freshEntry) {
            $this->appType = 'Visa';
        } else if ($cat_id == $reEntry) {
            $this->appType = 'Visa';
        } else if ($cat_id == $freezoneFreshEntry) {
            $this->appType = 'Free Zone';
        } else if ($cat_id == $freezoneReEntry) {
            $this->appType = 'Free Zone';
        }
        $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id, $cat_id, $visa_id, $entry_id, $no_of_entry, $zone_id, true);
        //Get Payment status
        $isGratis = false; // if application is gratic
        if ($this->visa_application[0]['ispaid'] == 1) {
            //        if (($this->visa_application[0]['paid_naira_amount'] == 0) && ($this->visa_application[0]['paid_dollar_amount'] == 0)) {
            //          $payment_status = "This Application is Gratis (Requires No Payment)";
            //          $isGratis = true;
            //        } else {
            $payment_status = "Payment Done";
            //        }
        }


        ##############//Get payment gateway type and Paid amount ###########################
        $this->paidAmount = 0;
        if ($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id'])) {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
            if ($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)') {
                //          $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                if ($this->paidAmount != 0.00) {
                    $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                } else {
                    $this->paidAmount = "Not Applicable";
                }
            } else {
                $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID, 'VISA');
                if ($this->data['pay4me']) {
                    if ($this->data['currency'] == 'naira') {
                        //              $this->paidAmount = $this->visa_application[0]['paid_naira_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "NGN&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    } else {
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                        if ($this->paidAmount != 0.00) {
                            $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                        } else {
                            $this->paidAmount = "Not Applicable";
                        }
                    }
                } else {
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'];
                    if ($this->paidAmount != 0.00) {
                        $this->paidAmount = "USD&nbsp;" . $this->paidAmount;
                    } else {
                        $this->paidAmount = "Not Applicable";
                    }
                }
            }
        }
        ################### End of Code ############################################################

        if ($this->visa_application[0]['VisaCategory']['var_value'] != "Fresh" && $this->visa_application[0]['VisaCategory']['var_value'] != "Fresh Freezone") {
            //Naira Amount
            $nairaAmt = "0";
            if ($this->visa_application[0]['VisaZoneType']['var_value'] != 'Free Zone') {
                if ($this->visa_application[0]['ispaid'] == 1) {
                    //            $nairaAmt = $this->visa_application[0]['paid_naira_amount'];
                } else {
                    if (isset($this->payment_details['naira_amount'])) {
                        $nairaAmt = $this->payment_details['naira_amount'];
                    }
                }
            }
            //Dollar Amount
            $dollarAmt = "0";
            if ($this->visa_application[0]['VisaZoneType']['var_value'] == 'Free Zone') {
                if ($this->visa_application[0]['ispaid'] == 1) {
                    $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
                } else {
                    if (isset($this->payment_details['dollar_amount'])) {
                        $dollarAmt = $this->payment_details['dollar_amount'];
                    }
                }
                $VisaTypeId = Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($this->visa_application[0]['id']);
                $FreezoneSingleVisaId = Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
                $FreezoneMultipleVisaId = Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
                if ($VisaTypeId == $FreezoneSingleVisaId) {
                    $app_type = 'Single Re-entry';
                } else if ($VisaTypeId == $FreezoneMultipleVisaId) {
                    $app_type = 'Multiple Re-entry';
                }
                $app_category = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa/Permit&nbsp";
                $processing_centre = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($this->visa_application[0]['ReEntryVisaApplication']['processing_centre_id']);
            } else {
                $app_type = $this->visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value'];
                $app_category = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa/Permit&nbsp;(" . $this->visa_application[0]['VisaZoneType']['var_value'] . ")";
                $processing_centre = '';
            }

            if (isset($this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']) && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != "" && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] != 0) {
                $no_of_entry = "-&nbsp;[&nbsp;" . $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'] . "&nbsp;Multiple Re Entries ]";
            }
            else
                $no_of_entry = "";

            $request_type = $this->visa_application[0]['VisaCategory']['var_value'] . "&nbsp;Visa" . $no_of_entry;

            if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'])) {
                $state = $this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];
            } else {
                $state = '';
            }
            if (isset($this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'])) {
                $visaOffice = $this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];
            } else {
                $visaOffice = '';
            }

            $data = array(
                "profile" => array(
                    "title" => $this->visa_application[0]['title'],
                    "first_name" => $this->visa_application[0]['other_name'],
                    "middle_name" => $this->visa_application[0]['middle_name'],
                    "last_name" => $this->visa_application[0]['surname'],
                    "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                    "sex" => $this->visa_application[0]['gender'],
                    "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                    "state_origin" => "",
                    "occupation" => $this->visa_application[0]['ReEntryVisaApplication']['profession']),
                "app_info" => array(
                    "application_category" => $app_category,
                    "application_type" => $app_type . "&nbsp;Visa",
                    "request_type" => $request_type,
                    "application_date" => $this->visa_application[0]['created_at'],
                    "application_id" => $this->visa_application[0]['id'],
                    "reference_no" => $this->visa_application[0]['ref_no'],
                    "isGratis" => $isGratis,
                ),
                "process_info" => array(
                    "country" => "Nigeria",
                    "processing_centre" => $processing_centre,
                    "state" => $state,
                    "embassy" => "",
                    "office" => $visaOffice,
                    "interview_date" => $this->visa_application[0]['interview_date'],
                ),
                "payment_info" => array(
                    "naira_amount" => $nairaAmt,
                    "dollor_amount" => $dollarAmt,
                    "payment_status" => $payment_status,
                    "payment_gateway" => $PaymentGatewayType,
                    "paid_amount" => $this->paidAmount
                ),
            );
        } else {
            if (isset($no_of_entry) && $no_of_entry != "" && $no_of_entry != 0) {
                $no_of_entry = "-&nbsp;[&nbsp;" . $no_of_entry . "&nbsp;Multiple Entries ]";
            }
            else
                $no_of_entry = "";
            $request_type = "Entry Visa" . $no_of_entry;

            //Dollar Amount
            $dollarAmt = 0;
            $nairaAmt = 0;
            if ($this->visa_application[0]['ispaid'] == 1) {
                $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
            } else {
                if (isset($this->payment_details['dollar_amount'])) {
                    $dollarAmt = $this->payment_details['dollar_amount'];
                }
            }

            //Get Embassy Name
            if ($this->applying_country != "Nigeria") {
                $embassyName = $this->visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
            } else {
                $embassyName = "Not Applicable";
            }
            if ($cat_id == $freshEntry) {
                $category_type = 'Entry Visa/Permit';
            } else if ($cat_id == $freezoneFreshEntry) {
                $category_type = 'Fresh Free Zone Visa/Permit';
            }

            $data = array(
                "profile" => array(
                    "title" => $this->visa_application[0]['title'],
                    "first_name" => $this->visa_application[0]['other_name'],
                    "middle_name" => $this->visa_application[0]['middle_name'],
                    "last_name" => $this->visa_application[0]['surname'],
                    "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                    "sex" => $this->visa_application[0]['gender'],
                    "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                    "state_origin" => "",
                    "occupation" => $this->visa_application[0]['profession']),
                "app_info" => array(
                    "application_category" => $category_type,
                    "application_type" => $this->visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value'] . "&nbsp;Visa",
                    "request_type" => $request_type,
                    "application_date" => $this->visa_application[0]['created_at'],
                    "application_id" => $this->visa_application[0]['id'],
                    "reference_no" => $this->visa_application[0]['ref_no'],
                    "isGratis" => $isGratis,
                ),
                "process_info" => array(
                    "country" => $this->applying_country,
                    "state" => "",
                    "embassy" => $embassyName,
                    "office" => "",
                    "interview_date" => $this->visa_application[0]['interview_date']),
                "payment_info" => array(
                    "naira_amount" => $nairaAmt,
                    "dollor_amount" => $dollarAmt,
                    "payment_status" => $payment_status,
                    "payment_gateway" => $PaymentGatewayType,
                    "paid_amount" => $this->paidAmount
                ),
            );
        }

        $paymentSlip = $this->generatePaymentSlipHTML($data);
    }

    /**
     *
     * @param <type> $id
     * @return <type>
     * Added by ashwani kumar for visa payment receipt...
     */
    protected function getVisaReEntryRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getVisaReEntryRecord($id);
        return $visa_application;
    }

    public function passportPaymentSlip($appID) {

        //$this->passport_application = Doctrine::getTable('PassportApplication')->find(array($request->getParameter('id')));
        $getAppId = SecureQueryString::DECODE($appID);
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $this->passport_application = $this->getPassportRecord($getAppId);
        $passport_fee = Doctrine::getTable('PassportFee')
                        ->createQuery('a')
                        ->where('a.passporttype_id = ?', $this->passport_application[0]['passporttype_id'])
                        ->execute()->toArray(true);
        $this->passport_fee = $passport_fee;
        //      if (($this->passport_application[0]['paid_naira_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
        //        $datetime = "Check the Passport Office / Embassy / High Commission";
        //      } else {
        $datetime = $this->passport_application[0]['interview_date'];
        //      }
        //Get Payment status
        $isGratis = false;
        if ($this->passport_application[0]['ispaid'] == 1) {
            if (($this->passport_application[0]['paid_local_currency_amount'] == 0) && ($this->passport_application[0]['paid_dollar_amount'] == 0)) {
                $payment_status = "This Application is Gratis (Requires No Payment)";
                $paidAmount = 'Not Applicable';
                $isGratis = true;
            } else {
                $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);

                if ($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' || $PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)') {
                    $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                    if ($paidAmount != 0.00) {
                        $paidAmountInNiara = $paidAmount;
                        $paidAmountInDollar = 'Not Applicable';
                        $paidAmount = "NGN&nbsp;" . $paidAmount;
                    } else {
                        $paidAmountInNiara = "Not Applicable";
                        $paidAmountInDollar = 'Not Applicable';
                        $paidAmount = 'Not Applicable';
                    }
                } else {

                    $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId, 'PASSPORT');
                    if (isset($this->data['pay4me']) && isset($this->data['currency']) && $this->data['currency'] == 'naira') {
                        $paidAmount = $this->passport_application[0]['paid_local_currency_amount'];
                        if ($paidAmount != 0.00) {
                            $nairaAmt = $paidAmount;
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = "NGN&nbsp;" . $paidAmount;
                        } else {
                            $nairaAmt = "Not Applicable";
                            $dollarAmt = 'Not Applicable';
                            $paidAmount = 'Not Applicable';
                        }
                    } else {
                        $paidAmount = $this->passport_application[0]['paid_dollar_amount'];
                        if ($paidAmount != 0.00) {
                            $paidAmountInDollar = $paidAmount;
                            $paidAmountInNiara = "Not Applicable";
                            $paidAmount = "USD&nbsp;" . $paidAmount;
                        } else {
                            $paidAmountInDollar = "Not Applicable";
                            $paidAmountInNiara = "Not Applicable";
                            $paidAmount = "Not Applicable";
                        }
                    }
                }

                //$paidAmount = ($this->passport_application[0]['paid_dollar_amount']!="")?"USD ".$this->passport_application[0]['paid_dollar_amount']:($this->passport_application[0]['paid_naira_amount']!="")?"NGN ".$this->passport_application[0]['paid_naira_amount']:0;
                $payment_status = "Payment Done";
            }
        }

        //Get payment gateway type
        // $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->passport_application[0]['payment_gateway_id']);
        //Pass data to view
        $data = array(
            "profile" => array(
                "title" => $this->passport_application[0]['title_id'],
                "first_name" => $this->passport_application[0]['first_name'],
                "middle_name" => $this->passport_application[0]['mid_name'],
                "last_name" => $this->passport_application[0]['last_name'],
                "date_of_birth" => $this->passport_application[0]['date_of_birth'],
                "sex" => $this->passport_application[0]['gender_id'],
                "country_origin" => $this->passport_application[0]['cName'],
                "state_origin" => $this->passport_application[0]['sName'],
                "occupation" => $this->passport_application[0]['occupation']),
            "app_info" => array(
                "application_category" => "",
                "application_type" => $this->passport_application[0]['passportType'],
                "request_type" => $this->passport_application[0]['PassportApplicationDetails']['request_type_id'],
                "application_date" => $this->passport_application[0]['created_at'],
                "application_id" => $this->passport_application[0]['id'],
                "reference_no" => $this->passport_application[0]['ref_no'],
                "isGratis" => $isGratis),
            "process_info" => array(
                "country" => $this->passport_application[0]['passportPCountry'],
                "state" => $this->passport_application[0]['passportPState'],
                "embassy" => $this->passport_application[0]['passportPEmbassy'],
                "office" => $this->passport_application[0]['passportPOffice'],
                "interview_date" => $datetime,
            ),
            "payment_info" => array(
                "naira_amount" => $paidAmountInNiara,
                "dollor_amount" => $paidAmountInDollar,
                "payment_status" => $payment_status,
                "payment_gateway" => $PaymentGatewayType,
                "paid_amount" => $paidAmount
            ),
        );

        $paymentSlip = $this->generatePaymentSlipHTML($data);

        return $paymentSlip;
    }

    //End of public function passportPaymentSlip()...

    public function generatePaymentSlipHTML($data) {

        $payMentSlip = '<div>';
        $payMentSlip .= '<table width="100%" cellspacing="2" cellpadding="2" border="1" style="padding:2px;" >';
        $payMentSlip .= '<tr><td>';
        $payMentSlip .= '<table width="100%" cellspacing="0" cellpadding="0" >';
        $payMentSlip .= '<tr bgcolor="#CCC"><td colspan="2" style="color:#FFF"><strong>Profile Information</strong></td></tr>';
        //      $payMentSlip .= '<tr><td width="300">Full Name: </td><td>' . ePortal_displayName($data['profile']['title'], $data['profile']['first_name'], $data['profile']['middle_name'], $data['profile']['last_name']) . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Date of Birth: </td><td>' . date_format(date_create($data['profile']['date_of_birth']), 'd/F/Y') . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Gender: </td><td>' . $data['profile']['sex'] . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Country of Origin: </td><td>' . $data['profile']['country_origin'] . '</td></tr>';
        $state_origin = ($data['profile']['state_origin']) ? $data['profile']['state_origin'] : 'Not Applicable';
        $payMentSlip .= '<tr><td width="300">State of Origin: </td><td>' . $state_origin . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Occupation: </td><td>' . $data['profile']['occupation'] . '</td></tr>';
        if (isset($data['profile']['LGA'])) {
            $payMentSlip .= '<tr><td width="300">LGA: </td><td>' . $data['profile']['LGA'] . '</td></tr>';
        }
        $payMentSlip .= '</table>';
        $payMentSlip .= '</td>';
        $payMentSlip .= '<td valign="top">';
        $payMentSlip .= '<table width="100%" cellspacing="0" cellpadding="0" >';
        $payMentSlip .= '<tr bgcolor="#CCC"><td colspan="2" style="color:#FFF"><strong>Application Information</strong></td></tr>';
        if ($data['app_info']['application_category'] != '') {
            $payMentSlip .= '<tr><td width="300">Category: </td><td>' . $data['app_info']['application_category'] . '</td></tr>';
        }
        $payMentSlip .= '<tr><td width="300">Type: </td><td>' . $data['app_info']['application_type'] . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Application Date: </td><td>' . date_format(date_create($data['app_info']['application_date']), 'd/F/Y') . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Application ID: </td><td>' . $data['app_info']['application_id'] . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Reference No.: </td><td>' . $data['app_info']['reference_no'] . '</td></tr>';
        $payMentSlip .= '</table>';
        $payMentSlip .= '</td></tr>';

        $payMentSlip .= '<tr><td valign="top">';
        $payMentSlip .= '<table width="100%" cellspacing="0" cellpadding="0" >';
        $payMentSlip .= '<tr bgcolor="#CCC"><td colspan="2" style="color:#FFF"><strong>Processing Information</strong></td></tr>';
        $payMentSlip .= '<tr><td width="300">Country: </td><td>' . $data['process_info']['country'] . '</td></tr>';
        if (isset($data['process_info']['processing_centre']) && $data['process_info']['processing_centre'] != '') {
            $processing_center = ($data['process_info']['processing_centre']) ? $data['process_info']['processing_centre'] : 'Not Applicable';
            $payMentSlip .= '<tr><td width="300">Processing Centre</td><td>' . $processing_centre . '</td></tr>';
        }
        if ($data['process_info']['state'] != '') {
            $state = (trim($data['process_info']['state']) != '') ? $data['process_info']['state'] : 'Not Applicable';
            $payMentSlip .= '<tr><td width="300">State</td><td>' . $state . '</td></tr>';
        }
        if ($data['process_info']['embassy'] != '') {
            $embassy = (trim($data['process_info']['embassy']) != '') ? $data['process_info']['embassy'] : 'Not Applicable';
            $payMentSlip .= '<tr><td width="300">Embassy</td><td>' . $embassy . '</td></tr>';
        }
        if ($data['process_info']['office'] != '') {
            $office = (trim($data['process_info']['office']) != '') ? $data['process_info']['office'] : 'Not Applicable';
            $payMentSlip .= '<tr><td width="300">Office</td><td>' . $office . '</td></tr>';
        }

        if (!$data['app_info']['isGratis']) {
            if (!empty($data['process_info']['interview_date'])) {
                $payMentSlip .= '<tr><td width="300">Interview Date</td><td>' . date_format(date_create($data['process_info']['interview_date']), 'd/F/Y') . '</td></tr>';
            }
        } else {
            $payMentSlip .= '<tr><td width="300">Interview Date</td><td>Check the Passport Office / Embassy / High Commission</td></tr>';
        }
        $payMentSlip .= '</table>';
        $payMentSlip .= '</td>';
        $payMentSlip .= '<td valign="top">';
        $payMentSlip .= '<table width="100%" cellspacing="0" cellpadding="0" >';
        $payMentSlip .= '<tr bgcolor="#CCC"><td colspan="2" style="color:#FFF"><strong>Payment Information</strong></td></tr>';

        if (!$data['app_info']['isGratis']) {
            //        if(!empty ($data['payment_info']['naira_amount']) && $data['payment_info']['naira_amount'] != 'Not Applicable'){
            //          if($data['payment_info']['naira_amount']!=0){
            //            $naira_amount = 'NGN&nbsp;'.number_format($data['payment_info']['naira_amount'],2);
            //          }else{
            //            $naira_amount = 0;
            //          }
            //          $payMentSlip .= '<tr><td width="300">Naira Amount</td><td>'.$naira_amount.'</td></tr>';
            //        }
            if (!empty($data['payment_info']['dollor_amount']) && $data['payment_info']['dollor_amount'] != 'Not Applicable') {
                if ($data['payment_info']['dollor_amount'] != 0) {
                    $dollor_amount = 'USD&nbsp;' . number_format($data['payment_info']['dollor_amount'], 2);
                } else {
                    $dollor_amount = 0;
                }
                $payMentSlip .= '<tr><td width="300">Dollar Amount</td><td>' . $dollor_amount . '</td></tr>';
            }
        }
        $payMentSlip .= '<tr><td width="300">Payment Status</td><td>' . $data['payment_info']['payment_status'] . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Payment Gateway</td><td>' . $data['payment_info']['payment_gateway'] . '</td></tr>';
        $payMentSlip .= '<tr><td width="300">Paid Amount</td><td>' . $data['payment_info']['paid_amount'] . '</td></tr>';
        $payMentSlip .= '</table>';
        $payMentSlip .= '</td></tr>';
        $payMentSlip .= '</table>';
        $payMentSlip .= '<br /></div>';

        return $payMentSlip;
    }

    // Function added to list the search results after Searching via Approval code
    public function executeSearchByApprovalCode(sfWebRequest $request) {
        $configuration = sfProjectConfiguration::getActive();
        $configuration->loadHelpers('EPortal', '');

        $this->setTemplate("searchByApprovalCode");

        $approval_code = trim($request->getParameter("approval_code"));
        $approval_code = ltrim($approval_code, ',');
        $approval_code = rtrim($approval_code, ',');

        $this->isFound = false;
        if (isset($approval_code) && $approval_code != '') {
            //$approvalCode = $this->approval_code .' a';
            ## exploding approval code with comma...
            $approvalCodeArray = explode(',', $approval_code);

            ## Getting count of given approval code...
            $approvalCodeCount = count($approvalCodeArray);

            $errorCount = 0;
            $approvalCode = array();
            $approvalCodeNotFound = array();
            $this->folderPath = sfConfig::get('sf_upload_dir') . '/pdf/';
            if ($approvalCodeCount > 1) {
                $newFolderName = time() . '/';
                ## Creating new directory which will contain multiple pdf files...
                mkdir($this->folderPath . $newFolderName);
                chmod($this->folderPath . $newFolderName, 0777);
                $this->folderPath = $this->folderPath . $newFolderName;
            }
            $this->extension = ".pdf";
            $config = sfTCPDFPluginConfigHandler::loadConfig();
            foreach ($approvalCodeArray AS $appCode) {

                $this->approval_code = trim($appCode);

                ## putting ' a' in at the last of given approval code so that can be matched from database...
                $approvalCode = trim($appCode) . ' a';

                $detailsQuery = EpVbvResponseTable::getInstance()->getDetailsByApprovalCode($approvalCode);

                $detailsQueryCount = count($detailsQuery);
                if ($detailsQueryCount > 0) {
                    $strContentToWrite = '';
                    for ($kj = 0; $kj < $detailsQueryCount; $kj++) {

                        //echo $detailsQuery[$kj]['order_id'];
                        $amount = $detailsQuery[$kj]['purchase_amount_scr'];
                        $orderDetail = ipay4meOrderTable::getInstance()->getOrderDetailByApprovalCode($detailsQuery[$kj]['order_id']);

                        //echo $orderDetail['order_number'];
                        //echo '<br />';
                        //$orderDetail['order_number'] = '129949026597125';
                        //$orderDetail['order_number'] = '129440049140850';
                        ## Getting order details...
                        //$ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($detailsQuery['order_id']);

                        unset($result);
                        if (count($orderDetail) > 0) {
                            ## Fetiching Application id on the basis of order id...
                            $result = CartItemsTransactionsTable::getInstance()->getCartDetails($orderDetail['order_number']);
                            //echo $ipayNisDetailsQuery->
                            //$result = $ipayNisDetailsQuery->execute()->toArray();
                            $ipayNisDetailsCount = count($result);

                            ## Fetching application details...
                            $appDetailsArray = array();
                            for ($i = 0; $i < $ipayNisDetailsCount; $i++) {
                                $app['passport_id'] = $result[$i]['passport_id'];
                                $app['visa_id'] = $result[$i]['visa_id'];
                                $app['freezone_id'] = $result[$i]['freezone_id'];
                                $appDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($app);

                                $appDetailsArray[$i]['appType'] = $appDetails['app_type'];
                                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appDetails['id']);
                                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                                $appDetailsArray[$i]['id'] = $encriptedAppId;
                                $ref_no = SecureQueryString::ENCRYPT_DECRYPT($appDetails['ref_no']);
                                $ref_no = SecureQueryString::ENCODE($ref_no);
                                $appDetailsArray[$i]['ref_no'] = $ref_no;

                                unset($appDetails);
                            }//End of for($i=0;$i<$ipayNisDetailsCount;$i++){...
                            ## Fetching payment slip on the basis of application id, application type and reference number...
                            $appDetailsArrayCount = count($appDetailsArray);
                            $paymentReceipt = array();
                            if ($appDetailsArrayCount > 0) {
                                for ($i = 0; $i < $appDetailsArrayCount; $i++) {
                                    if ($appDetailsArray[$i]['appType'] == 'passport') {
                                        $paymentReceipt[$i] = $this->passportPaymentSlip($appDetailsArray[$i]['id']);
                                    } else {
                                        ## commented for the time being...
                                        ## uncommnet in next release...
                                        $paymentReceipt[$i] = $this->printVisaRecipt($appDetailsArray[$i]['id'], $appDetailsArray[$i]['ref_no']);
                                    }
                                }//End of for($i=0;$i<$appDetailsArrayCount;$i++){...
                            }//End of if($appDetailsArrayCount > 0){...

                            $finalArray['email'] = '';
                            if (!empty($orderDetail)) {
                                $userDetail = UserDetailTable::getInstance()->findByUserId($orderDetail['created_by'])->toArray();
                                $finalArray['email'] = $userDetail[0]['email'];
                            }

                            $this->isFound = true;

                            ## Array for PDF...
                            $finalArray['order_number'] = $orderDetail['order_number'];
                            $finalArray['name'] = $orderDetail['name'];
                            $finalArray['mabbr'] = $orderDetail['mabbr'];
                            $finalArray['paid_date'] = $orderDetail['paid_date'];
                            $finalArray['mername'] = $orderDetail['mername'];
                            $finalArray['amount'] = $amount;
                            $finalArray['card_type'] = $orderDetail['card_type'];
                            $finalArray['card_first'] = $orderDetail['card_first'];
                            $finalArray['card_len'] = $orderDetail['card_len'];
                            $finalArray['card_last'] = $orderDetail['card_last'];
                            $finalArray['approvalCode'] = $this->approval_code;


                            $arrOrderDetails[] = $finalArray;
                            $strContentToWrite .= $this->contentToWrite($finalArray);

                            $paymentReceiptCount = count($paymentReceipt);
                            if ($paymentReceiptCount > 0) {
                                for ($i = 0; $i < $paymentReceiptCount; $i++) {
                                    $strContentToWrite .= $paymentReceipt[$i];
                                }
                            }//End of if($paymentReceiptCount > 0){...

                            unset($orderDetail);
                            unset($userDetail);
                            unset($finalArray);
                        }//End of if(count($orderDetail) > 0){...
                    }//End of for($i=0;$i<$detailsQueryCount;$i++){...


                    $this->arrOrderDetails = $arrOrderDetails;
                    $this->doc_File_title = $this->approval_code . "_Search_By_Approval_Code";
                    $this->path = $this->folderPath . $this->doc_File_title;

                    ## Creating PDF files...
                    $this->writeMultipleFile($this->extension, $this->path, $strContentToWrite, $this->doc_File_title);

                    //echo $strContentToWrite;
                } else {
                    $errorCount++;
                    $approvalCodeNotFound[] = $appCode;
                }//End of if($detailsQueryCount > 0){...
            }//End of foreach($approvalCodeArray AS $value){...
            ## Assigning approval codes which are not found...
            $this->approvalCodeNotFound = $approvalCodeNotFound;
            ## Getting count of approvals which are not found...
            $approvalCodeNotFoundCount = count($approvalCodeNotFound);

            $this->approvalCodeCount = $approvalCodeCount;
            $this->multiple = false;

            if ($approvalCodeCount > 1 && ($approvalCodeCount - $approvalCodeNotFoundCount) > 1) {
                $this->multiple = true;
            }

            if ($approvalCodeCount === $errorCount) {
                $errorMsg = 'Approval Code is not valid.';
                if ($approvalCodeNotFoundCount > 1) {
                    $errorMsg = 'Approval Codes are not valid.';
                }//End of if($approvalCodeNotFoundCount > 1){...

                $this->getUser()->setFlash("notice", $errorMsg);
                $this->redirect('supportTool/searchByApprovalCode');
            } else {
                if ($this->multiple) {
                    $zipFileName = rtrim($this->folderPath, "/") . '.zip';
                    exec('zip -rj ' . $zipFileName . ' ' . $this->folderPath);
                    if (file_exists($zipFileName)) {
                        $this->zipFileName = basename($zipFileName);
                        // To Do...
                        // original folder will be deleted..
                    }//End of if(file_exists($zipFileName)){...
                }//End of if($approvalCodeCount > 1){...
            }//End of if($approvalCodeCount == $errorCount){...
        }//End of if(isset ($this->approval_code) && $this->approval_code != ''){...
    }

    public function executePrintCommand(sfWebRequest $request) {
        $this->orderId = $request->getParameter('orderId');
        $this->setTemplate('printCommand');
        $this->setLayout('popupLayout');
    }

    public function executePrintNISPaymentReceipt(sfWebRequest $request) {
        $orderId = $request->getParameter('orderId');

        $orderDetail = ipay4meOrderTable::getInstance()->getOrderDetailByApprovalCode($orderId);

        $amount = $orderDetail['amount'];
        unset($result);
        if (count($orderDetail) > 0) {
            ## Fetiching Application id on the basis of order id...
            $result = CartItemsTransactionsTable::getInstance()->getCartDetails($orderDetail['order_number']);
            //echo $ipayNisDetailsQuery->
            //$result = $ipayNisDetailsQuery->execute()->toArray();
            $ipayNisDetailsCount = count($result);


            ## Fetching application details...
            $appDetailsArray = array();
            for ($i = 0; $i < $ipayNisDetailsCount; $i++) {
                $app['passport_id'] = $result[$i]['passport_id'];
                $app['visa_id'] = $result[$i]['visa_id'];
                $app['freezone_id'] = $result[$i]['freezone_id'];
                $appDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($app);

                $appDetailsArray[$i]['appType'] = $appDetails['app_type'];
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appDetails['id']);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $appDetailsArray[$i]['id'] = $encriptedAppId;
                $ref_no = SecureQueryString::ENCRYPT_DECRYPT($appDetails['ref_no']);
                $ref_no = SecureQueryString::ENCODE($ref_no);
                $appDetailsArray[$i]['ref_no'] = $ref_no;

                unset($appDetails);
            }//End of for($i=0;$i<$ipayNisDetailsCount;$i++){...

            $strContentToWrite = '';
            ## Fetching payment slip on the basis of application id, application type and reference number...
            $appDetailsArrayCount = count($appDetailsArray);
            $paymentReceipt = array();
            if ($appDetailsArrayCount > 0) {
                for ($i = 0; $i < $appDetailsArrayCount; $i++) {
                    if ($appDetailsArray[$i]['appType'] == 'passport') {
                        $paymentReceipt[$i] = $this->passportPaymentSlip($appDetailsArray[$i]['id']);
                    } else {
                        ## commented for the time being...
                        ## uncommnet in next release...
                        $paymentReceipt[$i] = $this->printVisaRecipt($appDetailsArray[$i]['id'], $appDetailsArray[$i]['ref_no']);
                    }
                }//End of for($i=0;$i<$appDetailsArrayCount;$i++){...
            }//End of if($appDetailsArrayCount > 0){...


            $finalArray['email'] = '';
            if (!empty($orderDetail)) {
                $userDetail = UserDetailTable::getInstance()->findByUserId($orderDetail['created_by'])->toArray();
                $finalArray['email'] = $userDetail[0]['email'];
            }

            $this->isFound = true;

            ## Array for PDF...
            $finalArray['order_number'] = $orderDetail['order_number'];
            $finalArray['name'] = $orderDetail['name'];
            $finalArray['mabbr'] = $orderDetail['mabbr'];
            $finalArray['paid_date'] = $orderDetail['paid_date'];
            $finalArray['mername'] = $orderDetail['mername'];
            $finalArray['amount'] = $amount;
            $finalArray['card_type'] = $orderDetail['card_type'];
            $finalArray['card_first'] = $orderDetail['card_first'];
            $finalArray['card_len'] = $orderDetail['card_len'];
            $finalArray['card_last'] = $orderDetail['card_last'];
            $finalArray['approvalCode'] = $this->approval_code;


            $arrOrderDetails[] = $finalArray;
            $strContentToWrite .= $this->contentToWrite($finalArray);

            $paymentReceiptCount = count($paymentReceipt);
            if ($paymentReceiptCount > 0) {
                for ($i = 0; $i < $paymentReceiptCount; $i++) {
                    $strContentToWrite .= $paymentReceipt[$i];
                }
            }//End of if($paymentReceiptCount > 0){...

            unset($orderDetail);
            unset($userDetail);
            unset($finalArray);
        }//End of if(count($orderDetail) > 0){...
        echo $strContentToWrite;
        die;
        $this->setLayout(false);
    }

    private function writeFile($extension, $path, $strDocument, $title) {
        //echo $strDocument;die;
        //ini_set("memory_limit", "999999M");

        $config = sfTCPDFPluginConfigHandler::loadConfig();
        //sfTCPDFPluginConfigHandler::includeLangFile($this->getUser()->getCulture());
        $pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        $pdf->SetFont("FreeSerif", "", 8);
        $pdf->SetHeaderData("", "", "", "Kumar");
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); //set image scale factor
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetBarcode(date("Y-m-d H:i:s", time()));
        $pdf->writeHTML($strDocument, true, 0);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $fileLocation = $path . $extension;
        $pdf->Output($fileLocation, 'F');
        unset($config);
        unset($pdf);
        return;
    }

    private function writeMultipleFile($extension, $path, $strDocument, $title) {
        //echo $strDocument;die;
        //ini_set("memory_limit", "999999M");
        //sfTCPDFPluginConfigHandler::includeLangFile($this->getUser()->getCulture());
        $pdf = new sfTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true);
        $pdf->SetFont("FreeSerif", "", 8);
        $pdf->SetHeaderData("", "", "", "Ashwani");
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.'', PDF_HEADER_STRING);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); //set image scale factor
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetBarcode(date("Y-m-d H:i:s", time()));
        $pdf->writeHTML($strDocument, true, 0);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $fileLocation = $path . $extension;
        $pdf->Output($fileLocation, 'F');
        // unset($config);
        unset($pdf);
        return;
    }

    private function contentToWrite($arrOrderDetails) {


        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Tag'));

        //$path = _compute_public_path('logo_swg.png', 'images', '', true);

        $str = "";
        $str .="<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
        $str .= "<tr><td style='font-size: 83%;'><img src='" . image_tag('/images/logo_swg.png', array('alt' => 'SWGLOBAL LLC')) . "' /> </td></tr>";

        $str .= "<tr><td height'100px;'></td></tr>";
        $str .="</table>";
        $str .="<table style='background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);' width='100%' border='0' cellpadding='4' cellspacing='0'>";
        $str .="<tr><td>";
        $str .="<table style='background-color: rgb(229, 236, 249);' width='100%' border='0' cellpadding='2' cellspacing='0'><tbody>";
        $str .= "<tr><td style='font-size: 83%;'>&nbsp;Order date: <b>" . $arrOrderDetails['paid_date'] . "</b></td></tr>";
        $str .= "<tr><td style='font-size: 83%;'>SW Global LLC order number: <b>" . $arrOrderDetails['order_number'] . "</b></td>";
        $str .= "<td style='padding-right: 15px;' nowrap='nowrap' align='right'></td></tr></tbody></table></td></tr>";
        $str .= "<tr><td>";
        $str .= "<table style='background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);' width='100%' border='0' cellpadding='4' cellspacing='0'><tr style=' margin-left: 5px;' valign='top'>";
        $str .="<td  style='font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;' nowrap='nowrap'>&nbsp;<b>Payment Status</b>&nbsp;</td>
            <td  style='font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 65%; text-align: left;'>&nbsp;<b>Item</b></td>

            <td  style='font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 20%; text-align: center; padding-right: 1em;'>&nbsp;<b>Price</b></td>

        </tr>
        <tr style='margin-right: 15px; margin-left: 5px;' valign='top'>
            <td  style='font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;' align='left'>
            &nbsp;&nbsp;<b>Successful</b></td>
            <td  style='font-family: Arial,Sans-Serif; font-size: 73%; border-bottom: 1px solid rgb(229, 236, 249); width: 65%;' align='left'>
                <b><span style='font-weight: bold; color: black;'>&nbsp;" . $arrOrderDetails['mername'] . "</span></b>
                &nbsp;-&nbsp;";

        if (is_numeric($arrOrderDetails['name'])) {

            $str .="<span style='color: rgb(102, 102, 102);'>Number of appplications for which payment has been made is " . $arrOrderDetails['name'] . " . </n>Your applications will be processed further on " . $arrOrderDetails['mername'] . " Portal.</span>";
        } else {

            $str .="<span style='color: rgb(102, 102, 102);'>Payment for Service for customer " . $arrOrderDetails['name'] . " . </n>Your application will be processed further on " . $arrOrderDetails['mabbr'] . " Portal .</span>";
        }
        $str .="</td><td  style='font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 20%; text-align: center; padding-right: 1em;'>" . html_entity_decode('$' . number_format($arrOrderDetails['amount'], 2, '.', '')) . "</td>


        </tr>
        <tr style='background-color: rgb(229, 236, 249);'>

            <td  style='font-weight: bold; font-size: 105%; width: 70%; text-align: right; vertical-align: text-top;'>Total:</td>
            <td  style='font-weight: bold; font-size: 105%; width: 30%; text-align: center; white-space: nowrap; padding-right: 1em;'>" . html_entity_decode('$' . number_format($arrOrderDetails['amount'], 2, '.', '')) . "</td>";
        $str .= "</tr>";
        $str .= "</table></td></tr>";

        if ('V' == strtoupper($arrOrderDetails['card_type'])) {
            $card_type = "VISA";
        } else {
            $card_type = "";
        }
        $str .="<tr style='color:#336600;background-color:#EFFFEF;'>
        <td  class='txtBold' align='left'><b>Paid with:</b>" . $card_type . "&nbsp; " . $arrOrderDetails['card_first'] . "-" . ($arrOrderDetails['card_len'] - 8) . "-" . $arrOrderDetails['card_last'] . "</td>
        </tr>";
        /* <tr style='color:#336600;background-color:#EFFFEF;'>
          <td  class='txtBold' align='left'><b>Approval Code:</b> " . $arrOrderDetails['approvalCode'] . "</td>
          </tr> */
        $str .= "<tr style='color:#336600;background-color:#EFFFEF;'>
            <td  class='txtBold' align='left'><b>Email:</b> " . $arrOrderDetails['email'] . "</td>
        </tr>


        </table>";
        return $str;
    }

    public function executeDownloadPdf(sfWebRequest $request) {
        $fileName = $request->getParameter('fileName');

        $fileFullPath = sfConfig::get('sf_upload_dir') . '/pdf/' . $fileName;
        header("Content-Type: application/pdf");
        header('Content-Disposition: attachment; filename=' . $fileName);
        readfile($fileFullPath);
        die;
    }

    public function executeSearchByRetryid(sfWebRequest $request) {
        $this->isFound = false;
        $retryId = Settings::decryptInput($request->getParameter("id"));
        if (isset($retryId) && $retryId != '') {
            if (!is_numeric($retryId)) {
                $this->getUser()->setFlash("error", "Error.");
                $this->redirect("supportTool/searchByCardDetails");
            }
            //get order details on ipay4me
            //$this->ipay4meOrderDetails  = ipay4meOrderTable::getInstance()->getIpayOrderDetails($ipayOrderNo);

            $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId);
            //        $this->nisHelper = new NisHelper();
            //        $page = 1;
            //        if($request->hasParameter('page')) {
            //          $page = $request->getParameter('page');
            //        }
            //        $this->pager = new sfDoctrinePager('PassportApplication',sfConfig::get('app_records_per_page'));
            //        $this->pager->setQuery($ipayNisDetailsQuery);
            //        $this->pager->setPage($this->getRequestParameter('page',$page));
            //        $this->pager->init();
            //        $this->appObj = CartItemsTransactionsTable::getInstance();
            //
            //        $this->isFound = true;
        }
    }

    /**
     *
     * @param <type> $request
     * Error code info entry and to display order numbers corresponding to that error code
     */
    public function executeErrorCodeInfo(sfWebRequest $request) {
        $configuration = sfProjectConfiguration::getActive();
        $configuration->loadHelpers('FpsError', '');
        $this->appDetail = 0;

        $this->isFound = false;
        $this->error_message = '';
        if ($request->isMethod('POST')) {
            $this->errorCode = $request->getParameter("error_code");
        } else {
            $this->errorCode = Settings::decryptInput($request->getParameter("error_code"));
        }

        $pos = strstr($this->errorCode, 'x');
        $pos1 = strstr($this->errorCode, 'X');
        if ($pos || $pos1) {
            if ($this->errorCode) {
                $this->errorCode = strtoupper($this->errorCode);
                $splitError = explode('X', $this->errorCode);
                $orderDetailId = $splitError[0];
                $this->errorMessageId = $splitError[1];
            }
            if ($orderDetailId == '' || $this->errorMessageId == '') {
                $this->getUser()->setFlash("error", "Error Code is invalid.");
                $this->redirect("supportTool/errorCodeInfo");
            }
            if (isset($orderDetailId) && $orderDetailId != '') {
                if (!is_numeric($orderDetailId)) {
                    $this->getUser()->setFlash("error", "Error Code is invalid.");
                    $this->redirect("supportTool/errorCodeInfo");
                }
                $errObj = new ErrorMsg();
                $msg = $errObj->getErrorMgs("E" . $this->errorMessageId);
                if (!isset($msg) || $msg == '') {
                    $this->getUser()->setFlash("error", "Error Code is invalid.");
                    $this->redirect("supportTool/errorCodeInfo");
                }





                //get order details on ipay4me
                $ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailbyId($orderDetailId);

                $arrRecord = $ipay4meOrderDetails->execute()->toArray();
                if (!($arrRecord)) {
                    $fpsDetails = FpsDetailTable::getInstance()->getFpsDetailbyOrderRequestDetailId($orderDetailId);
                    $fpsDetails = $fpsDetails->execute()->toArray();

                    if (!empty($fpsDetails)) {
                        $cardNum = $fpsDetails[0]['card_num'];
                        $fpsRuleId = $fpsDetails[0]['fps_rule_id'];

                        $ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailbyCardNum($cardNum);


                        $reason = fps_error_admin($fpsRuleId);

                        if ($fpsRuleId == '4' || $fpsRuleId == '5') {
                            $this->error_message = 'This Card is blocked, and associated with email: ' . $fpsDetails[0]['email'];
                        } else if ($fpsRuleId == '0') {
                            $this->error_message = '';
                        } else {
                            $this->error_message = $reason;
                        }
                        $this->nisRetryId = Doctrine::getTable('OrderRequestDetails')->findById($orderDetailId)->toArray();

                        $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->nisRetryId[0]['retry_id']);

                        $this->appDetail = 1;


                        //Your card has been declined by the Processor, please use another card.
                    } else {
                        $this->isFound = false;
                    }
                }

                $page = 1;
                if ($request->hasParameter('page')) {
                    $page = $request->getParameter('page');
                }
                if (count($ipay4meOrderDetails->execute()->toArray()) > 0) {
                    $this->isFound = true;
                }

                $this->pager = new sfDoctrinePager('ipay4meOrder', sfConfig::get('app_records_per_page'));
                $this->pager->setQuery($ipay4meOrderDetails);
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
                $this->isFound = true;
            }
        }
    }

    /**
     *
     * @param <type> $request
     * To display application's listed for a particular order number
     */
    public function executeApplicationDetails(sfWebRequest $request) {
        //        $this->broadcast_message = $this->getBroadcastMessage();

        $this->setTemplate('applicationDetails');
        $error = $request->getPostParameters();
        $orderNumber = Settings::decryptInput($request->getParameter('orderNumber'));
        $this->errorMessageId = Settings::decryptInput($request->getParameter('errorMessageId'));
        $this->date_flg = 0;

        //get FPS detail
        $fpsDetail = FpsDetailTable::getInstance()->findByOrderNumber($orderNumber);
        if (($fpsDetail) > 0) {
            $orderRequestDetailId = $fpsDetail[0]['order_request_detail_id'];
            $fpsRuleId = $fpsDetail[0]['fps_rule_id'];
            $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $gatewayName = Doctrine::getTable('Gateway')->find($fpsDetail[0]['gateway_id']);
            $gatewayName = $gatewayName->getDisplayName();
            $userId = $requestDetails->getUserId();
            $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();

            $this->appVault = ApplicantVaultTable::getInstance()->getApprovedCardByHash($fpsDetail[0]['card_num'], $userId);
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailsChildFPS($orderNumber);
        } else {
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
            $gatewayId = Doctrine::getTable('ipay4meOrder')->getGatewayId($orderNumber);
            $gatewayName = Doctrine::getTable('Gateway')->find($gatewayId);
            $gatewayName = $gatewayName->getDisplayName();
            $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $userId = $requestDetails->getUserId();
            $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailsChildFPS($orderNumber);
            $cardFirst = $this->ipay4meOrderDetails[0]['card_first'];
            $cardLast = $this->ipay4meOrderDetails[0]['card_last'];
            $cardHash = $this->ipay4meOrderDetails[0]['card_hash'];

            $this->appVault = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);
            $fpsRuleId = '';
        }

        if ($this->appVault) {
            $this->date_flg = 1;
        } else {
            $this->date_flg = 0;
        }
        $arrTransactionDetail['last_transaction'] = $requestDetails['updated_at'];
        $arrTransactionDetail['next_transaction'] = $requestDetails['updated_at'];
        $arrTransactionDetail['gateway_name'] = $gatewayName;
        $arrTransactionDetail['fps_rule_id'] = $fpsRuleId;
        $this->arrTransactionDetail = $arrTransactionDetail;



        $this->orderNumber = $orderNumber;
        $this->amount = $requestDetails->getAmount();
        $this->form = new RefundPaymentForm();

        //get retry id for retriving NIS applications
        $this->nisRetryId = ipay4meOrderTable::getInstance()->getIpayOrderRetryId($orderNumber);
        $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsByRetryIdQuery($this->nisRetryId);

        $this->time = sfConfig::get('app_active_maxcard_time_in_min');



        $this->nisHelper = new NisHelper();
        $page = 1;
        if ($request->hasParameter('page')) {
            $page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($ipayNisDetailsQuery);
        $this->pager->setPage($this->getRequestParameter('page', $page));
        $this->pager->init();
        $this->appObj = CartItemsTransactionsTable::getInstance();

        $this->isFound = true;

        //    $this->setLayout(false);
    }

    /**
     *
     * @param <type> $request
     * To display information for a particular application.
     */
    public function executeApplicationDetailsInfo(sfWebRequest $request) {


        $orderNumber = Settings::decryptInput($request->getParameter('orderNumber'));
        $appId = Settings::decryptInput($request->getParameter('appId'));
        $type = Settings::decryptInput($request->getParameter('type'));
        // $appId='2934194';$type='visa';
        $this->orderNumber = $orderNumber;
        $this->appId = $appId;
        $this->type = $type;
        $nisUrl = sfConfig::get("app_nis_portal_url");
        if ($type == 'passport') {
            $passport_application = $this->getPassportRecord($appId);
            $this->passport_application = $passport_application;

            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $this->payReceiptUrl = $nisUrl . 'passport/passportPaymentSlip?id=' . $encriptedAppId;
            $this->ackUrl = $nisUrl . 'passport/passportAcknowledgmentSlip?id=' . $encriptedAppId;
        } else if ($type == 'visa') {
            $visa_application = $this->getVisaFreshRecord($appId);

            $refNo = $visa_application[0]["ref_no"];
            $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
            $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
            $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
            $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

            $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
            $this->ackUrl = $nisUrl . 'visa/visaAcknowledgmentSlip?id=' . $encriptedAppId;
            $this->passport_application = $visa_application;
        } else if ($type == 'freezone') {
            $visa_application = $this->getVisaFreshRecord($appId);
            $visa_application[0]['country'] = 'Nigeria';
            $country = 'NG';
            $embassy = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($appId);
            $embassyName = Doctrine::getTable('VisaProcessingCentre')->findById($embassy)->toArray();
            $visa_application[0]['center'] = $embassyName[0]['centre_name'];

            $refNo = $visa_application[0]["ref_no"];
            $zoneType = $visa_application[0]["zone_type_id"];
            if ($zoneType == 67) {
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

                $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
                $this->ackUrl = $nisUrl . 'visa/visaAcknowledgmentSlip?id=' . $encriptedAppId;
            } else {
                $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
                $encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                $encriptedRefId = SecureQueryString::ENCODE($encriptedRefId);

                $this->payReceiptUrl = $nisUrl . 'visa/PrintRecipt?visa_app_id=' . $encriptedAppId . '&visa_app_refId=' . $encriptedRefId;
                $this->ackUrl = $nisUrl . 'visa/visaReEntryAcknowledgmentSlip?id=' . $encriptedAppId;
            }
            $this->passport_application = $visa_application;
        }
    }

    protected function getPassportRecord($id) {
        $passport_application = Doctrine_Query::create()
                        ->select("pa.*,pa.processing_country_id as processing_passport_id, pd.*, pci.*, c.country_name as cName, s.state_name as sName, pc.country_name as passportPCountry, ps.state_name as passportPState, em.embassy_name as passportPEmbassy, po.office_name as passportPOffice, pt.id, pt.var_value as passportType,cs.state_name as contact_state")
                        ->from('PassportApplication pa')
                        ->leftJoin('pa.PassportApplicationDetails pd')
                        ->leftJoin('pa.PassportApplicantContactinfo pci')
                        ->leftJoin('pci.Country c', 'c.id=pci.nationality_id')
                        ->leftJoin('pci.State cs', 'cs.id=pci.state_of_origin')
                        ->leftJoin('pd.State s', 's.id=pd.stateoforigin')
                        ->leftJoin('pa.Country pc', 'pc.id=pa.processing_country_id')
                        ->leftJoin('pa.State ps', 'ps.id=pa.processing_state_id')
                        ->leftJoin('pa.EmbassyMaster em', 'em.id=pa.processing_embassy_id')
                        ->leftJoin('pa.PassportOffice po', 'po.id=pa.processing_passport_office_id')
                        ->leftJoin('pa.PassportAppType pt')
                        ->Where('pa.id=' . $id)
                        ->execute()->toArray(true);

        if (isset($passport_application) && is_array($passport_application) && count($passport_application) > 0) {
            if ($passport_application[0]['sName'] == '') {
                $passport_application[0]['sName'] = $passport_application[0]['contact_state'];
            }
        }
        return $passport_application;
    }

    public function executeSummaryDetails(sfWebRequest $request) {
        $orderNumber = $request->getParameter('order_number');
        $cardFirst = $request->getParameter('card_first');
        $cardLast = $request->getParameter('card_last');
        $this->summary = ipay4meOrderTable::getInstance()->getSummaryDetails($orderNumber, $cardFirst, $cardLast);
        $cardHash = $this->summary[0]['OrderRequestDetails']['FpsDetail'][0]['card_num'];
        $this->appVault = ApplicantVaultTable::getInstance()->getDetailsByCardHash($cardHash, $cardFirst, $cardLast);
        $userId = $this->summary[0]['OrderRequestDetails']['FpsDetail'][0]['user_id'];
        $this->userDetail = UserDetailTable::getInstance()->getDetailByUserId($userId);
    }

    public function executeEditContectDetails(sfWebRequest $request) {
        $this->setTemplate("editContectDetails");
        $this->mode = $request->getParameter('mode');

        ## Getting paging number to maintain paging data...
        $page = $request->getParameter('page');
        $this->getUser()->setAttribute('page', $page);


        if ($request->hasParameter('id')) {
            $id = Settings::decryptInput($request->getParameter('id'));
            $applicantVaultValues = Doctrine::getTable('ApplicantVault')->find($id);
        } else {
            $applicantVaultValues = Doctrine::getTable('ApplicantVault')->find($_REQUEST['applicant_vault']['id']);
        }
        $arrCardDetail['country'] = $applicantVaultValues->getCountry();
        $phonePrefix = $applicantVaultValues->getPhone();

        $arrCardDetail['year'] = $applicantVaultValues->getExpiryYear();
        $arrCardDetail['month'] = $applicantVaultValues->getExpiryMonth();
        $arrCardDetail['card_first'] = $applicantVaultValues->getCardFirst();
        $arrCardDetail['card_last'] = $applicantVaultValues->getCardLast();
        $arrCardDetail['card_len'] = $applicantVaultValues->getCardLen();
        $arrCardDetail['card_type'] = $applicantVaultValues->getCardType();
        $this->arrCardDetail = $arrCardDetail;

        $phone = explode('-', $phonePrefix);
        if (isset($phone[1])) {
            $this->phone = $phone[1];
        } else {
            $this->phone = '';
        }
        $this->form = new ApplicantVaultForm($applicantVaultValues);
        $payBut = $request->getParameter('payBut');
        if ($request->isMethod("post") && isset($payBut)) {
            $formVal = $request->getParameter($this->form->getName());
            $mode = $request->getPostParameter('hdn_mode');
            $this->mode = $mode;
            //            echo '<pre>';print_r($formVal);die;
            $phone = $formVal['phone'];
            $mobile_prefix = $request['mobile_frefix'];
            $phone_number = $mobile_prefix . '' . $phone;
            $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles());
            if ($this->form->isValid()) {
                $formObj = $this->form->getObject();
                $formObj->setExpiryYear($formVal['expiry_date']['year']);
                $formObj->setExpiryMonth($formVal['expiry_date']['month']);
                $formObj->setPhone($phone_number);
                $obj = $this->form->save();
                $request_id = $obj->id;
                $tableobj = ApplicantVaultTable::getInstance()->find($request_id);
                $tableobj->setPhone($phone_number);
                $tableobj->save();
                $this->getUser()->setFlash("notice", "Credit Card details has been updated successfully.");
                if ($mode == 1) {
                    $this->redirect("supportTool/userListAuth?back=1");
                } else {
                    $this->redirect("supportTool/searchByCardNumber");
                }
            } else {
                //die("not");
            }
        }
    }

    protected function getVisaFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getVisaFreshRecord($id);
        return $visa_application;
    }

    protected function getFreezoneFreshRecord($id) {
        $visa_application = Doctrine::getTable('VisaApplication')->getFreezoneFreshRecord($id);
        return $visa_application;
    }

    public function executeSearchByMoneyOrder(sfWebRequest $request) {
        $this->isFound = true;
        $moneyOrder = trim($request->getParameter("money_order"));
        $trackingNuberQuery = Doctrine::getTable('Moneyorder')->getAllTrackingNumber($moneyOrder);
        $this->pager = new rawDocPager(null, 10);
        $this->pager->setStatement($trackingNuberQuery);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();

        if ($request->isMethod('POST')) {

            $this->amountCurrency = $request->getParameter("amount_currency");
            $this->receivedAmount = trim($request->getParameter("received_amount"));
            $this->isSearch = true;
        } else {
            $this->totalMORequest = $this->pager->getNbResults();
            $this->isSearch = false;
        }
    }

    public function executeUpdateMoneyOrder(sfWebRequest $request) {
        $postArray = $request->getPostParameters();
        $arrMoneyOrder = $postArray['hdn_mo_id'];

        for ($k = 0; $k < count($arrMoneyOrder); $k++) {
            $trackingNuberQuery = Doctrine::getTable('Moneyorder')->getAllMODetails($arrMoneyOrder[$k]);
            $MOId = $trackingNuberQuery[0]['id'];
            for ($i = 0; $i < count($trackingNuberQuery[0]['TrackingMoneyorder']); $i++) {
                $orderNumber = $trackingNuberQuery[0]['TrackingMoneyorder'][$i]['CartTrackingNumber']['order_number'];

                $orderReqDetailId = $trackingNuberQuery[0]['TrackingMoneyorder'][$i]['CartTrackingNumber']['order_request_detail_id'];
                $userId = $trackingNuberQuery[0]['TrackingMoneyorder'][$i]['CartTrackingNumber']['user_id'];

                $comments = '';

                //update money order payment status in ipay4me_order
                $updateIp4mOrder = Doctrine::getTable('ipay4meOrder')->updateIP4MOrderStatus($orderNumber);
                //update payment_status in order request detail table
                $updateOrderReqDetail = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderReqDetailId, $userId);
                //update table money order
                $updateMO = Doctrine::getTable('Moneyorder')->updateMODetail($MOId, $comments);
                //update table cart tracking number
                $updatecartTrackingMO = Doctrine::getTable('CartTrackingNumber')->updateCartTrackingDetail($orderNumber, $orderReqDetailId);

                // start -Amit - split amount if payment is successfull
                $gatewayId = 6;
                $payManagerObj = new PaymentManager();
                $updateOrder = $payManagerObj->updateSplitAmount($orderReqDetailId, $gatewayId);
                // end -Amit -split amount

               /**
                 * [WP: 112] => CR: 158
                 * Updating payment status set to 0...
                 */
                Functions::updateTransactionServiceChargesPaymentStatus($orderReqDetailId);


                $notificationUrl = "notification/PaymentNotificationV1";
                $senMailUrl = "notification/moneyOrderPaymentSuccessMail";
                $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication', $notificationUrl, array('order_detail_id' => $orderReqDetailId));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                $this->auditPaymentTransaction($orderReqDetailId, $orderNumber);
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("report/getReceipt", true) . '?receiptId=' . $orderNumber;
                $url1 = url_for("report/paymentHistory", true);

                $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $senMailUrl, array('order_detail_id' => $orderReqDetailId, 'url' => $url, 'url1' => $url1));
                sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
            }
        }


        $this->getUser()->setFlash("notice", "Application(s) associated with this money order has been paid successfully.");
        $this->redirect("supportTool/searchByMoneyOrder");


        // echo '<pre>';print_r($postArray);die;
    }

    public function auditPaymentTransaction($orderDetailId, $cusId) {
        //Log audit Details
        $orderDetailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $amount = $orderDetailsObj->getAmount();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $amount_formatted = format_amount($amount, 1);
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $orderDetailId, $orderDetailId));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $cusId, 'amount' => $amount_formatted)),
                        $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    /**
     *
     * @param <type> $request 
     * Finiding money order report...
     */
    public function executeMoneyOrderReport(sfWebRequest $request) {

        $moneyOrderStatus = ($request->getParameter("mo_status")) ? $request->getParameter("mo_status") : '';
        $currency = ($request->getParameter("currency")) ? $request->getParameter("currency") : '';
        if (!empty($currency)) {            
            if (strtolower($currency) == 'pound') {
                $currency = 5;
            }else{
                $currency = 1;
            }
        }
        $moneyOrder = $request->getParameter("money_order");
        $trackingNumber = $request->getParameter("tracking_number");
        $order_number = $request->getParameter("order_number");
        $fdate = $request->getParameter("fdate");
        $tdate = $request->getParameter("tdate");


        if ($request->isMethod('post')) {
            unset($_SESSION['mo_status']);
            unset($_SESSION['money_order']);
            unset($_SESSION['tracking_number']);
            unset($_SESSION['order_number']);
            unset($_SESSION['currency']);
            unset($_SESSION['fdate']);
            unset($_SESSION['tdate']);


            $_SESSION['mo_status'] = $moneyOrderStatus;
            $_SESSION['money_order'] = $moneyOrder;
            $_SESSION['tracking_number'] = $trackingNumber;
            $_SESSION['order_number'] = $order_number;
            $_SESSION['currency'] = $currency;
            $_SESSION['fdate'] = $fdate;
            $_SESSION['tdate'] = $tdate;
        } else if ($request->hasParameter('back')) {
            $moneyOrderStatus = $_SESSION['mo_status'];
            $moneyOrder = $_SESSION['money_order'];
            $trackingNumber = $_SESSION['tracking_number'];
            $order_number = $_SESSION['order_number'];
            $currency = $_SESSION['currency'];
            $fdate = $_SESSION['fdate'];
            $tdate = $_SESSION['tdate'];
        } else {
            $_SESSION['mo_status'] = NULL;
            $_SESSION['money_order'] = NULL;
            $_SESSION['tracking_number'] = NULL;
            $_SESSION['order_number'] = NULL;
            $_SESSION['currency'] = NULL;
            $_SESSION['fdate'] = NULL;
            $_SESSION['tdate'] = NULL;
        }

        $userGroup = $this->getUser()->getGroups();
        $userId = $this->getUser()->getGuarduser()->getId();

        $this->isFound = true;
        ## find tracking number for money order
        $trackingNuberArray = array();
        $userGruoupId = '';
        if ($userGroup->getFirst()->getName() == 'payment_group') {
            $userGruoupId = $userId;
        }

        $trackingNuberArray = Doctrine::getTable('Moneyorder')->getTrackingNumberByStatus($moneyOrderStatus, $moneyOrder, $trackingNumber, $userGruoupId, $order_number, $currency, $fdate, $tdate);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('Moneyorder', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($trackingNuberArray);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
        $this->filename = 'MoneyOrderReport.xls';
        $this->isExcelShow = false;
        if (isset($fdate) && !empty($fdate) && isset($tdate) && !empty($tdate)) {
            $this->isExcelShow = true;
        }//End of if(isset($fdate) && !empty($fdate) && isset($tdate) && !empty($tdate)){...
    }

//End of public function executeMoneyOrderReport...

    public function executeTrackingInfoBox(sfWebRequest $request) {
        $this->setLayout(false);
        $trackingNumberId = $request->getParameter('tracking_id');
        $this->status = $request->getParameter('status');
        $trackingDetail = Doctrine::getTable('Moneyorder')->getMoneyOrderDetailByTrackingNumber($trackingNumberId);
        $arrMODetail['moneyorder_number'] = $trackingDetail['0']['moneyorder_number'];
        $arrMODetail['phone'] = $trackingDetail['0']['phone'];
        $arrMODetail['address'] = $trackingDetail['0']['address'];
        $arrMODetail['courier_flag'] = $trackingDetail['0']['courier_flag'];
        $arrMODetail['waybill_trackingno'] = $trackingDetail['0']['waybill_trackingno'];
        $arrMODetail['paid_date'] = $trackingDetail['0']['paid_date'];
        $arrMODetail['currency'] = $trackingDetail['0']['currency'];
        if ($trackingDetail['0']['currency'] == '5') {
            $arrMODetail['mo_amount'] = $trackingDetail['0']['convert_amount'];
            $arrMODetail['tn_amount'] = $trackingDetail['0']['TrackingMoneyorder'][0]['CartTrackingNumber']['convert_amount'];
        } else {
            $arrMODetail['mo_amount'] = $trackingDetail['0']['amount'];
            $arrMODetail['tn_amount'] = $trackingDetail['0']['TrackingMoneyorder'][0]['CartTrackingNumber']['cart_amount'];
        }
        $arrMODetail['status'] = $trackingDetail['0']['status'];
        $arrMODetail['tracking_number'] = $trackingDetail['0']['TrackingMoneyorder'][0]['CartTrackingNumber']['tracking_number'];
        //$arrMODetail['tn_amount'] = $trackingDetail['0']['TrackingMoneyorder'][0]['CartTrackingNumber']['cart_amount'];
        $arrMODetail['order_number'] = $trackingDetail['0']['TrackingMoneyorder'][0]['CartTrackingNumber']['order_number'];
        $this->arrMODetail = $arrMODetail;
        // echo '<pre>';print_r($trackingDetail);die;
    }

    public function executeDisAllocate(sfWebRequest $request) {
        $this->setLayout(false);

        //      $moneyOrderNumber = $request->getParameter('moneyorder_number');
        //      $moneyOrderDetail = Doctrine::getTable('MoneyOrder')->getMoneyOrderDetailByMoneyOrderNumber($moneyOrderNumber);

        $moneyorderId = $request->getParameter('moneyorderId');
        //      $moneyorderId = $moneyOrderDetail['0']['id'];
        $trackingMoneyOrderDetail = Doctrine::getTable('TrackingMoneyOrder')->getTrackingMoneyOrderDetailByMoneyOrderId($moneyorderId);
        $trackingMoneyOrderDetailCount = count($trackingMoneyOrderDetail);
        $msg = 'Money Order not found.';
        if ($trackingMoneyOrderDetailCount > 0) {
            $finalArray = array();

            for ($i = 0; $i < $trackingMoneyOrderDetailCount; $i++) {
                $finalArray[$i] = $trackingMoneyOrderDetail[$i]['cart_track_id'];
            }
            if (count($finalArray) > 0) {
                $cartTrackingNumber = Doctrine::getTable('CartTrackingNumber')->setTrackingNumberStatus($finalArray);
                $deleteTrackingMoneyOrder = Doctrine::getTable('TrackingMoneyOrder')->deleteDetailByMoneyOrderId($moneyorderId);
                $deleteMoneyOrder = Doctrine::getTable('MoneyOrder')->deleteDetailById($moneyorderId);
                $msg = 'Your Money Order has been de-associated successfully.'; // Bug id: 29439
            }
        }
        $this->getUser()->setFlash("notice", $msg);
        $this->redirect("supportTool/searchByMoneyOrderNumber");
    }

    public function executeMoneyOrderDetails(sfWebRequest $request) {

        $moneyOrderId = Settings::decryptInput($request->getParameter('money_order_id'));
        $this->back = $request->getParameter('back');
        $this->page = $request->getParameter('page');
        $this->order_number = Settings::decryptInput($request->getParameter('order_number'));
        $trackingDetails = TrackingMoneyorderTable::getInstance()->getMoneyOrderDetails($moneyOrderId);
        $this->moneyOrderDetails = MoneyorderTable::getInstance()->find($moneyOrderId)->toArray();
        $applicationDetails = array();
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                //           echo '<pre>';print_r($this->ipayNisAppDetails);die;
                if (isset($this->ipayNisAppDetails) && is_array($this->ipayNisAppDetails) && count($this->ipayNisAppDetails) > 0) {
                    foreach ($this->ipayNisAppDetails as $key => $app) {

                        $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];
                        if ($orderDetail['currency'] == '5') {
                            $applicationDetails[$i]['cart_amount'] = $orderDetail['convert_amount'];
                        } else {
                            $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                        }
                        $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                        $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                        $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                        $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                    }
                }
                $i++;
            }
            $this->applicationDetails = $applicationDetails;
            $this->trackingDetails = $trackingDetails;
        }
    }

    public function executeSearchByMoneyOrderNumber(sfWebRequest $request) {
        $this->setTemplate('searchByMoneyOrderNumber');
    }

    public function executeMoneyOrderDetailsTrackingNumbers(sfWebRequest $request) {
        $this->form = new MoneyorderForm();
        $this->isValid = false;
        $this->moneyOrderNumber = $request->getParameter('money_order_number');
        $idflag = $request->getParameter('idflag');
        $status = '';
        $moneyOrderDetail = Doctrine::getTable('MoneyOrder')->getMoneyOrderDetailByMoneyOrderNumber($this->moneyOrderNumber);
        if (!empty($moneyOrderDetail)) {
            $status = $moneyOrderDetail['0']['status'];

            if ($status != 'Paid' || $idflag != '') {
                $this->moneyOrderDetail = $moneyOrderDetail;

                $moneyorderId = $moneyOrderDetail['0']['id'];
                $this->moneyorderId = $moneyorderId;
                $trackingMoneyOrderDetail = Doctrine::getTable('TrackingMoneyOrder')->getTrackingMoneyOrderDetailByMoneyOrderId($moneyorderId);
                $finalArray = array();
                for ($i = 0; $i < count($trackingMoneyOrderDetail); $i++) {
                    $finalArray[$i] = $trackingMoneyOrderDetail[$i]['cart_track_id'];
                }

                $this->pendingtrackingNumbers = Doctrine::getTable('CartTrackingNumber')->getTrackingNumberById($finalArray);

                if (isset($this->pendingtrackingNumbers) && is_array($this->pendingtrackingNumbers) && count($this->pendingtrackingNumbers) > 0) {
                    $this->isValid = true;
                } else {
                    $this->getUser()->setFlash("error", "No tracking numbers for associate");
                    $this->redirect("supportTool/searchByMoneyOrderNumber");
                }
            } else {
                $this->getUser()->setFlash("error", "Money Order Number is already Paid");
                $this->redirect("supportTool/searchByMoneyOrderNumber");
            }
        } else {

            $this->getUser()->setFlash("notice", "Money Order Serial Number not found");
            $this->redirect("supportTool/searchByMoneyOrderNumber");
        }
    }

    public function executeUpdateMoneyOrderDetails(sfWebRequest $request) {

        $para = $request->getPostParameters();

        $moneyOrderDetailId = $request->getParameter('moneyOrderDetailId');
        $moneyOrder = $request->getParameter('moneyorder');
        $moneyOrderAmount = $moneyOrder['amount'];
        $moneyOrderNumber = $moneyOrder['moneyorder_number'];
        $moneyOrderNumberExisting = $request->getParameter('moneyOrderNumber1');

        $isMoneyrderNumber = Doctrine::getTable('MoneyOrder')->getMoneyOrderNumberId($moneyOrderNumber, $moneyOrderDetailId);
        $detail = MoneyorderTable::getInstance()->findByMoneyorderNumber($moneyOrderNumberExisting);
        $status = $detail->toArray();
        $statusFlag = ($status[0]['status'] == 'Paid') ? '1' : '';
        if ($isMoneyrderNumber > 0) {
            $this->getUser()->setFlash("notice", "The Money Order Serial Number entered is already used");
            $this->redirect("supportTool/moneyOrderDetailsTrackingNumbers?money_order_number=" . $moneyOrderNumberExisting . "&idflag=" . $statusFlag);
        } else {
            $msg = 'Invalid Tracking number.';
            $moneyOrderDetail = Doctrine::getTable('MoneyOrder')->setMoneyOrderDetailByMoneyOrderId($moneyOrderDetailId, $moneyOrderAmount, $moneyOrderNumber);
            if ($moneyOrderDetail) {
                $trackingMoneyOrderDetail = Doctrine::getTable('TrackingMoneyOrder')->getTrackingMoneyOrderDetailByMoneyOrderId($moneyOrderDetailId);

                $finalArray = array();
                for ($i = 0; $i < count($trackingMoneyOrderDetail); $i++) {
                    $finalArray[$i] = $trackingMoneyOrderDetail[$i]['cart_track_id'];
                }
                $arrayPost = $request->getParameter('chk_fee');
                $arrayFinalPost = array();

                for ($j = 0; $j < count($arrayPost); $j++) {
                    $arrayValue = explode('_', $arrayPost[$j]);
                    $arrayFinalPost[$j] = $arrayValue['0'];
                }
                if (!empty($finalArray) && !empty($arrayFinalPost)) {
                    $resultToBeDeleted = array_diff($finalArray, $arrayFinalPost);
                }
                if (!empty($resultToBeDeleted)) {
                    $resultToBeDeleted = array_values($resultToBeDeleted);
                    Doctrine::getTable('CartTrackingNumber')->setTrackingNumberStatus($resultToBeDeleted);

                    for ($k = 0; $k < count($resultToBeDeleted); $k++) {
                        $tracking_money_order_id = $resultToBeDeleted[$k];
                        $deleteDetail = Doctrine::getTable('TrackingMoneyOrder')->deleteDetailByMoneyOrderIdCartTrackId($moneyOrderDetailId, $tracking_money_order_id);
                        //delete query
                    }
                }
                $msg = 'Money Order Details updated successfully.';
            }//End of if($moneyOrderDetail)...

            $this->getUser()->setFlash("notice", $msg);
            $this->redirect("supportTool/moneyOrderDetailsTrackingNumbers?money_order_number=" . $moneyOrderNumber);
        }
    }

    public function executeSearchByAppId(sfWebRequest $request) {
        $appID = $request->getParameter('app_id');
        $refNo = $request->getParameter('ref_no');

        if (isset($appID) && $appID != '' && isset($refNo) && $refNo != 0) {

            $detailinfo = array();
            // Find application in Visa table
            $appRecord = Doctrine::getTable('VisaApplication')->getVisaInfoByAppId($appID, $refNo);

            if (isset($appRecord) && count($appRecord[0])) {
                $ipayObj = ipay4meOrderTable::getInstance();
                $ipay4meOrderDetails = null;
                $czoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();

                if ($appRecord[0]['zone_type_id'] == '' || $appRecord[0]['zone_type_id'] == $czoneTypeId) {
                    $detailinfo['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['other_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                    $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'v');

                    if (count($isRefunded) > 0) {
                        $detailinfo['status'] = $isRefunded[0]['action'];
                    } else {
                        $detailinfo['status'] = $appRecord[0]['status'];
                    }
                    $detailinfo['paid_at'] = $appRecord[0]['paid_at'];
                    $detailinfo['appid'] = $appRecord[0]['id'];
                    $detailinfo['refno'] = $appRecord[0]['ref_no'];
                    $detailinfo['date_of_birth'] = $appRecord[0]['date_of_birth'];
                    $detailinfo['app_type'] = 'visa';
                    $duplicateAppData = VisaApplicationTable::getInstance()->findDuplicateVisaApplication(null, null, "visa", $appID);

                    $orderNumber = $duplicateAppData[0]['order_no'];
                    if ($orderNumber != 0) {
                        $index = strpos($orderNumber, ",");
                        if ($index) {
                            $appOrderDetails = substr($orderNumber, 0, $index);
                        } else if ($orderNumber != 0) {
                            $appOrderDetails = $orderNumber;
                        }
                        if ($appOrderDetails != 0)
                            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                        $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                    }
                    if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                        $detailinfo['order_no'] = $appOrderDetails;
                        $detailinfo['gatewayName'] = $gatewayName;
                        $detailinfo['email'] = $ipay4meOrderDetails[0]['email'];
                    } else {
                        $detailinfo['order_no'] = "Not Paid on NIS";
                        $detailinfo['gatewayName'] = "Not Applicable";
                        $detailinfo['email'] = "Not Applicable";
                    }
                } else {
                    $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'v');
                    if (count($isRefunded) > 0) {
                        $detailinfo['status'] = $isRefunded[0]['action'];
                    } else {
                        $detailinfo['status'] = $appRecord[0]['status'];
                    }
                    $detailinfo['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['other_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                    $detailinfo['paid_at'] = $appRecord[0]['paid_at'];
                    $detailinfo['appid'] = $appRecord[0]['id'];
                    $detailinfo['refno'] = $appRecord[0]['ref_no'];
                    $detailinfo['date_of_birth'] = $appRecord[0]['date_of_birth'];
                    $detailinfo['app_type'] = 'freezone';
                    $duplicateAppData = VisaApplicationTable::getInstance()->findDuplicateVisaApplication(null, null, "freezone", $appID);
                    $orderNumber = $duplicateAppData[0]['order_no'];
                    if ($orderNumber != 0) {
                        $index = strpos($orderNumber, ",");
                        if ($index) {
                            $appOrderDetails = substr($orderNumber, 0, $index);
                        } else if ($orderNumber != 0) {
                            $appOrderDetails = $orderNumber;
                        }
                        if ($appOrderDetails != 0)
                            $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                        $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                    }
                    if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                        $detailinfo['order_no'] = $appOrderDetails;
                        $detailinfo['gatewayName'] = $gatewayName;
                        $detailinfo['email'] = $ipay4meOrderDetails[0]['email'];
                    } else {
                        $detailinfo['order_no'] = "Not Paid on NIS";
                        $detailinfo['gatewayName'] = "Not Applicable";
                        $detailinfo['email'] = "Not Applicable";
                    }
                }
            }
            // Find application in Passport table if it not exist in Visa table
            $appRecord = Doctrine::getTable('PassportApplication')->getPassportInfoByAppId($appID, $refNo);
            if (isset($appRecord) && count($appRecord[0])) {
                $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'p');
                // echo '<pre>';print_r(count($isRefunded));die;
                $ipayObj = ipay4meOrderTable::getInstance();
                $detailinfo['name'] = ucwords($appRecord[0]['title_id'] . " " . $appRecord[0]['first_name'] . " " . $appRecord[0]['mid_name'] . " " . $appRecord[0]['last_name']);
                if (count($isRefunded) > 0) {
                    $detailinfo['status'] = $isRefunded[0]['action'];
                } else {
                    $detailinfo['status'] = $appRecord[0]['status'];
                }
                $detailinfo['paid_at'] = $appRecord[0]['paid_at'];
                $detailinfo['appid'] = $appRecord[0]['id'];
                $detailinfo['refno'] = $appRecord[0]['ref_no'];
                $detailinfo['date_of_birth'] = $appRecord[0]['date_of_birth'];
                $detailinfo['app_type'] = 'passport';
                $duplicateAppData = PassportApplicationTable::getInstance()->findDuplicatePassportApplication(null, null, $appID);
                $orderNumber = $duplicateAppData[0]['order_no'];
                if ($orderNumber != 0) {
                    $index = strpos($orderNumber, ",");
                    if ($index) {
                        $appOrderDetails = substr($orderNumber, 0, $index);
                    } else if ($orderNumber != 0) {
                        $appOrderDetails = $orderNumber;
                    }
                    if ($appOrderDetails != 0)
                        $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                    $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                }
                if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                    $detailinfo['order_no'] = $appOrderDetails;
                    $detailinfo['gatewayName'] = $gatewayName;
                    $detailinfo['email'] = $ipay4meOrderDetails[0]['email'];
                } else {
                    $detailinfo['order_no'] = "Not Paid on NIS";
                    $detailinfo['gatewayName'] = "Not Applicable";
                    $detailinfo['email'] = "Not Applicable";
                }
            }
            // Find application in VAP table if it not exist in Visa and Passport table
            $appRecord = Doctrine::getTable('VapApplication')->getVisaArrivalInfo($appID, $refNo);

            if (isset($appRecord) && count($appRecord[0])) {
                //          $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($appID, 'p');
                // echo '<pre>';print_r(count($isRefunded));die;
                $ipayObj = ipay4meOrderTable::getInstance();
                $detailinfo['name'] = ucwords($appRecord[0]['title'] . " " . $appRecord[0]['first_name'] . " " . $appRecord[0]['middle_name'] . " " . $appRecord[0]['surname']);
                //          if (count($isRefunded) > 0) {
                //            $detailinfo['status'] = $isRefunded[0]['action'];
                //          } else {
                $detailinfo['status'] = $appRecord[0]['status'];
                //          }
                $detailinfo['paid_at'] = $appRecord[0]['paid_date'];
                $detailinfo['appid'] = $appRecord[0]['id'];
                $detailinfo['refno'] = $appRecord[0]['ref_no'];
                $detailinfo['date_of_birth'] = $appRecord[0]['date_of_birth'];
                $detailinfo['app_type'] = 'vap';
                $duplicateAppData = VapApplicationTable::getInstance()->findDuplicateVapApplication(null, null, null, $appID);
                $orderNumber = $duplicateAppData[0]['order_no'];
                if ($orderNumber != 0) {
                    $index = strpos($orderNumber, ",");
                    if ($index) {
                        $appOrderDetails = substr($orderNumber, 0, $index);
                    } else if ($orderNumber != 0) {
                        $appOrderDetails = $orderNumber;
                    }
                    if ($appOrderDetails != 0)
                        $ipay4meOrderDetails = $ipayObj->getIpayOrderDetails($appOrderDetails);
                    $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderDetails[0]['gateway_id']);
                }
                if (isset($ipay4meOrderDetails) && $ipay4meOrderDetails != '') {
                    $detailinfo['order_no'] = $appOrderDetails;
                    $detailinfo['gatewayName'] = $gatewayName;
                    $detailinfo['email'] = $ipay4meOrderDetails[0]['email'];
                } else {
                    $detailinfo['order_no'] = "Not Paid on NIS";
                    $detailinfo['gatewayName'] = "Not Applicable";
                    $detailinfo['email'] = "Not Applicable";
                }
            }

            if (count($detailinfo) > 0) {
                $appType = $detailinfo['app_type'];
            } else {
                $this->getUser()->setFlash("notice", "Application Not Found.");
                $this->redirect('supportTool/searchByAppId');
            }
            // To find failed order list by app id
            $orderDetailArray = array();
            $orderRequesDetailsArray = array();
            $k = 0;
            $iPaymentRequestArray = Doctrine_Core::getTable('IPaymentRequest')->getId($appID, $refNo, $appType);
            if (!empty($iPaymentRequestArray)) {
                for ($i = 0; $i < count($iPaymentRequestArray); $i++) {
                    $iPaymentRequestId = $iPaymentRequestArray[$i]['id'];
                    $cartDetailsArray = Doctrine_Core::getTable('CartItemsTransactions')->getRetryIdByPaymentRequestId($iPaymentRequestId);
                    if (!empty($cartDetailsArray)) {
                        for ($j = 0; $j < count($cartDetailsArray); $j++) {
                            $transactionNo = $cartDetailsArray[$j]['transaction_number'];
                            $orderRequesDetailsArray[] = Doctrine_Core::getTable('OrderRequestDetails')->getOrderDetailId($transactionNo);
                        }
                    }
                }
            }
            if (!empty($orderRequesDetailsArray)) {
                $j = 0;
                for ($i = 0; $i < count($orderRequesDetailsArray); $i++) {
                    $orderRequesDetailsId = $orderRequesDetailsArray[$i][0]['id'];
                    $currencyId = $orderRequesDetailsArray[$i][0]['currency'];
                    if ($currencyId == '1') {
                        $amount = $orderRequesDetailsArray[$i][0]['amount'];
                    } else {
                        $amount = $orderRequesDetailsArray[$i][0]['convert_amount'];
                    }
                    $currencySymbool = CurrencyManager::currencySymbolByCurrencyId($currencyId);
                    $ipay4meOrderArray = Doctrine_Core::getTable('Ipay4meOrder')->getOrderNumbers($orderRequesDetailsId);
                    if (!empty($ipay4meOrderArray)) {
                        for ($k = 0; $k < count($ipay4meOrderArray); $k++) {
                            $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($ipay4meOrderArray[$k]['gateway_id']);
                            $applicantVaultArray = Doctrine_Core::getTable('ApplicantVault')->getApprovedCardByHash($ipay4meOrderArray[$k]['card_hash'], $ipay4meOrderArray[$k]['created_by']);
                            //                                            echo "<pre>";print_r($applicantVaultArray);
                            $userDetailArray = Doctrine_Core::getTable('UserDetail')->findByUserId($ipay4meOrderArray[$k]['created_by'])->toArray();
                            $fpsDetailObj = Doctrine_Core::getTable('FpsDetail')->findByOrderNumber($ipay4meOrderArray[$k]['order_number']);


                            $orderDetailArray[$j]['order_number'] = $ipay4meOrderArray[$k]['order_number'];
                            $orderDetailArray[$j]['amount'] = $amount;
                            $orderDetailArray[$j]['currency_symbol'] = $currencySymbool;
                            $orderDetailArray[$j]['request_id'] = $ipay4meOrderArray[$k]['order_request_detail_id'];
                            $orderDetailArray[$j]['gateway_name'] = $gatewayName;
                            $orderDetailArray[$j]['payment_status'] = 'Not Paid';
                            $orderDetailArray[$j]['user_id'] = $ipay4meOrderArray[$k]['created_by'];
                            $orderDetailArray[$j]['user_email'] = $userDetailArray[0]['email'];
                            $orderDetailArray[$j]['trans_date'] = $ipay4meOrderArray[$k]['created_at'];
                            $orderDetailArray[$j]['gateway_id'] = $ipay4meOrderArray[$k]['gateway_id'];
                            $orderDetailArray[$j]['payor_name'] = $ipay4meOrderArray[$k]['payor_name'];

                            if (!empty($applicantVaultArray) && $applicantVaultArray != '') {
                                $orderDetailArray[$j]['is_register'] = 'Yes';
                            } else {
                                $orderDetailArray[$j]['is_register'] = 'No';
                            }

                            if (count($fpsDetailObj) > 0) {
                                $fpsDetailArray = $fpsDetailObj;
                                $ruleId = $fpsDetailArray[0]['fps_rule_id'];
                                switch ($ruleId) {
                                    case "1" :
                                        $fpsBlock = 'IP Limit Exceeded';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "2" :
                                        $fpsBlock = 'Credit Card Limit Exceeded';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "3" :
                                        $fpsBlock = 'Same Email Multiple Times';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "4" :
                                        $fpsBlock = 'Card is blocked due to successive failure attempt';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "5" :
                                        $fpsBlock = 'User is black listed by Admin';
                                        $failureReason = 'User is blocked on SWGlobal LLC';
                                        break;
                                    default:
                                        $fpsBlock = 'No';
                                        $failureReason = 'Transaction has been declined by bank. Please see response code';
                                }
                            } else {
                                $fpsBlock = 'No';
                                $failureReason = 'Transaction has been declined by bank. Please see response code';
                            }

                            if ($ipay4meOrderArray[$k]['gateway_id'] != '6') {
                                $orderDetailArray[$j]['card_first'] = $ipay4meOrderArray[$k]['card_first'];
                                $orderDetailArray[$j]['card_last'] = $ipay4meOrderArray[$k]['card_last'];
                                if (!empty($ipay4meOrderArray[$k]['response_code']) && $ipay4meOrderArray[$k]['response_code'] != '') {
                                    $orderDetailArray[$j]['response_code'] = $ipay4meOrderArray[$k]['response_code'];
                                } else {
                                    $orderDetailArray[$j]['response_code'] = 'N/A';
                                }
                                $orderDetailArray[$j]['fps_block'] = $fpsBlock;
                                $orderDetailArray[$j]['failure_reason'] = $failureReason;
                            } else {
                                $orderDetailArray[$j]['card_first'] = 'N/A';
                                $orderDetailArray[$j]['card_last'] = 'N/A';
                                $orderDetailArray[$j]['response_code'] = 'N/A';
                                $orderDetailArray[$j]['fps_block'] = 'N/A';
                                $orderDetailArray[$j]['failure_reason'] = 'N/A';
                                $orderDetailArray[$j]['is_register'] = 'N/A';
                            }
                            $j++;
                        }
                    } else {
                        $fpsDetailArray = Doctrine::getTable('FpsDetail')->getUnpaidRecordByRequestId($orderRequesDetailsId);
                        if (!empty($fpsDetailArray) && !empty($fpsDetailArray)) {
                            for ($k = 0; $k < count($fpsDetailArray); $k++) {
                                $gatewayName = Doctrine_Core::getTable('Gateway')->getDisplayName($fpsDetailArray[$k]['gateway_id']);
                                $applicantVaultArray = Doctrine_Core::getTable('ApplicantVault')->getApprovedCardByHash($fpsDetailArray[$k]['card_num'], $fpsDetailArray[$k]['user_id']);
                                $userDetailArray = Doctrine_Core::getTable('UserDetail')->findByUserId($fpsDetailArray[$k]['user_id'])->toArray();

                                $orderDetailArray[$j]['order_number'] = 'N/A';
                                $orderDetailArray[$j]['amount'] = $amount;
                                $orderDetailArray[$j]['currency_symbol'] = $currencySymbool;
                                $orderDetailArray[$j]['request_id'] = $orderRequesDetailsId;
                                $orderDetailArray[$j]['gateway_name'] = $gatewayName;
                                $orderDetailArray[$j]['payment_status'] = 'Not Paid';
                                $orderDetailArray[$j]['user_id'] = $fpsDetailArray[$k]['user_id'];
                                $orderDetailArray[$j]['user_email'] = $userDetailArray[0]['email'];
                                $orderDetailArray[$j]['trans_date'] = $fpsDetailArray[$k]['created_at'];
                                $orderDetailArray[$j]['gateway_id'] = $fpsDetailArray[$k]['gateway_id'];
                                $orderDetailArray[$j]['card_first'] = 'Not Available';
                                $orderDetailArray[$j]['card_last'] = 'Not Available';
                                $orderDetailArray[$j]['response_code'] = 'N/A';
                                $orderDetailArray[$j]['payor_name'] = 'N/A';

                                $ruleId = $fpsDetailArray[$k]['fps_rule_id'];
                                switch ($ruleId) {
                                    case "1" :
                                        $fpsBlock = 'IP Limit Exceeded';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "2" :
                                        $fpsBlock = 'Credit Card Limit Exceeded';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "3" :
                                        $fpsBlock = 'Same Email Multiple Times';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "4" :
                                        $fpsBlock = 'Card is blocked due to successive failure attempt';
                                        $failureReason = 'Transaction has been stopped from our side';
                                        break;
                                    case "5" :
                                        $fpsBlock = 'User is black listed by Admin';
                                        $failureReason = 'User is blocked on SWGlobal LLC';
                                        break;
                                    default:
                                        $fpsBlock = 'No';
                                        $failureReason = 'Transaction has been declined by bank. Please see response code';
                                }



                                $orderDetailArray[$j]['fps_block'] = $fpsBlock;
                                $orderDetailArray[$j]['failure_reason'] = $failureReason;

                                if (!empty($applicantVaultArray) && $applicantVaultArray != '') {
                                    $orderDetailArray[$j]['is_register'] = 'Yes';
                                } else {
                                    $orderDetailArray[$j]['is_register'] = 'No';
                                }
                                $j++;
                            }
                        }
                    }
                }
            }

            $this->orderDetailArray = $orderDetailArray;
            $this->detailinfo = $detailinfo;
            $this->setTemplate('viewAll');
        }
    }

    public function executeViewAll(sfWebRequest $request) {
        
    }

    public function executeShowCardDetails(sfWebRequest $request) {

        $orderNumber = $request->getParameter('orderNumber');
        $this->appid = $request->getParameter('app_id');
        $this->ref_no = $request->getParameter('ref_no');

        $this->date_flg = 0;

        //get FPS detail
        $fpsDetail = FpsDetailTable::getInstance()->findByOrderNumber($orderNumber);
        if (($fpsDetail) > 0) {
            $orderRequestDetailId = $fpsDetail[0]['order_request_detail_id'];
            $fpsRuleId = $fpsDetail[0]['fps_rule_id'];
            $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $this->orderRequestDetailByOrder = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailByOrder($orderNumber);
            $cardHash = $this->orderRequestDetailByOrder[0]['card_hash'];

            if (!empty($cardHash)) {

                $this->successiveFailure = $this->fpsDetailByCardNum($cardHash);
                if (!empty($cardHash)) {
                    $lastFailureDate = Doctrine::getTable('FpsDetail')->findLastFailureDate($cardHash)->execute()->toArray();
                    if (!empty($lastFailureDate) && isset($lastFailureDate[0]['trans_date'])) {
                        $this->lastFailureDate = $lastFailureDate[0]['trans_date'];
                    } else {
                        $this->lastFailureDate = 'N/A';
                    }
                } else {
                    $this->lastFailureDate = 'N/A';
                }

                $epVbvResponse = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderNumber);
                if (!empty($epVbvResponse)) {
                    $this->epVbvResponse = $epVbvResponse;
                } else {
                    $this->epVbvResponse[0]['three_ds_verificaion'] = 'a';
                }
                $gatewayName = Doctrine::getTable('Gateway')->find($fpsDetail[0]['gateway_id']);
                $gatewayName = $gatewayName->getDisplayName();
                $userId = $requestDetails->getUserId();
                $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();

                $this->appVault = ApplicantVaultTable::getInstance()->getApprovedCardByHash($fpsDetail[0]['card_num'], $userId);
                $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailsChildFPS($orderNumber);


                if ($this->appVault) {
                    $this->date_flg = 1;
                } else {
                    $this->date_flg = 0;
                }

                $arrTransactionDetail['last_transaction'] = $requestDetails['updated_at'];
                $arrTransactionDetail['next_transaction'] = $requestDetails['updated_at'];
                $arrTransactionDetail['gateway_name'] = $gatewayName;
                $arrTransactionDetail['fps_rule_id'] = $fpsRuleId;
                $this->arrTransactionDetail = $arrTransactionDetail;

                $this->time = sfConfig::get('app_active_maxcard_time_in_min');
            } else {
                $this->epVbvResponse = array();
                $this->orderRequestDetailByOrder = array();
                $arrTransactionDetail = array();
                $this->date_flg = '';
                $this->successiveFailure = 'N/A';
                $this->lastFailureDate = 'N/A';
                $this->epVbvResponse[0]['three_ds_verificaion'] = 'N';
                $this->orderRequestDetailByOrder[0]['card_first'] = 'N/A';
                $this->orderRequestDetailByOrder[0]['card_last'] = 'N/A';
                $this->orderRequestDetailByOrder[0]['card_hash'] = 'This Order Number is generated before entering card details.';
                $arrTransactionDetail['last_transaction'] = 'N/A';
                $arrTransactionDetail['next_transaction'] = 'N/A';
                $arrTransactionDetail['gateway_name'] = 'N/A';
                $arrTransactionDetail['fps_rule_id'] = 'N';
                $this->arrTransactionDetail = $arrTransactionDetail;
                $this->date_flg = '2';
            }
        } else {
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
            $this->orderRequestDetailByOrder = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailByOrder($orderNumber);
            $cardHash1 = $this->orderRequestDetailByOrder[0]['card_hash'];
            if (!empty($cardHash1)) {
                $this->successiveFailure = $this->fpsDetailByCardNum($cardHash1);

                $this->lastFailureDate = Doctrine::getTable('FpsDetail')->findLastFailureDate($cardHash1);
                $this->lastFailureDate = $this->lastFailureDate->execute()->toArray();
                if (!empty($this->lastFailureDate)) {
                    $this->lastFailureDate = $this->lastFailureDate['0']['trans_date'];
                } else {
                    $this->lastFailureDate = 'N/A';
                }


                $epVbvResponse = Doctrine::getTable('EpVbvResponse')->findByOrderId($orderNumber);
                if (!empty($epVbvResponse)) {
                    $this->epVbvResponse = $epVbvResponse;
                }
                $gatewayId = Doctrine::getTable('ipay4meOrder')->getGatewayId($orderNumber);
                $gatewayName = Doctrine::getTable('Gateway')->find($gatewayId);
                $gatewayName = $gatewayName->getDisplayName();
                $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
                $userId = $requestDetails->getUserId();
                $userDetailobj = $requestDetails->getSfGuardUser()->getUserDetail();
                $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetailsChildFPS($orderNumber);
                $cardFirst = $this->ipay4meOrderDetails[0]['card_first'];
                $cardLast = $this->ipay4meOrderDetails[0]['card_last'];
                $cardHash = $this->ipay4meOrderDetails[0]['card_hash'];

                $this->appVault = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);

                $fpsRuleId = '';

                if ($this->appVault) {
                    $this->date_flg = 1;
                } else {
                    $this->date_flg = 0;
                }

                $arrTransactionDetail['last_transaction'] = $requestDetails['updated_at'];
                $arrTransactionDetail['next_transaction'] = $requestDetails['updated_at'];
                $arrTransactionDetail['gateway_name'] = $gatewayName;
                $arrTransactionDetail['fps_rule_id'] = $fpsRuleId;
                $this->arrTransactionDetail = $arrTransactionDetail;

                $this->time = sfConfig::get('app_active_maxcard_time_in_min');
            } else {
                $this->epVbvResponse = array();
                $this->orderRequestDetailByOrder = array();
                $arrTransactionDetail = array();
                $this->date_flg = '';
                $this->successiveFailure = 'N/A';
                $this->lastFailureDate = 'N/A';
                $this->epVbvResponse[0]['three_ds_verificaion'] = 'N';
                $this->orderRequestDetailByOrder[0]['card_first'] = 'N/A';
                $this->orderRequestDetailByOrder[0]['card_last'] = 'N/A';
                $this->orderRequestDetailByOrder[0]['card_hash'] = 'This Order Number is generated before entering card details.';
                $arrTransactionDetail['last_transaction'] = 'N/A';
                $arrTransactionDetail['next_transaction'] = 'N/A';
                $arrTransactionDetail['gateway_name'] = 'N/A';
                $arrTransactionDetail['fps_rule_id'] = 'N';
                $this->arrTransactionDetail = $arrTransactionDetail;
                $this->date_flg = '2';
            }
        }
    }

    public function fpsDetailByCardNum($cardHash) {
        $fpsDetailByCarNum = Doctrine::getTable('FpsDetail')->findByCardNum($cardHash);

        $count = count($fpsDetailByCarNum);

        $j = 0;
        for ($i = 0; $i < $count; $i++) {
            $paymentStatus = $fpsDetailByCarNum[$i]['payment_status'];
            if ($paymentStatus == 0) {
                break;
            } else {
                $j++;
            }
        }
        return $j;
    }

    public function executeShowStatusFromBackEnd(sfWebRequest $request) {

        $orderNo = $request->getPostParameter('orderNo');
        $status = '';
        $gatewayName = '';
        if (!empty($orderNo)) {
            $orderRequestDetailId = Doctrine_Core::getTable('Ipay4meOrder')->findByOrderNumber($orderNo);
            $orderRequestId = $orderRequestDetailId[0]['order_request_detail_id'];
            $amountArray = Doctrine_Core::getTable('OrderRequestDetails')->findById($orderRequestId);
            $amountRequired = $amountArray[0]['amount'];
            $gatewayArray = Doctrine_Core::getTable('Ipay4meOrder')->getGatewayIdByOrderNo($orderNo);
            $gatewayId = $gatewayArray[0]['gateway_id'];
            $gatewayNameArray = Doctrine_Core::getTable('Gateway')->findById($gatewayId);
            $gatewayName = $gatewayNameArray[0]['name'];
            if (!empty($gatewayId)) {

                if ($gatewayId == 1 || $gatewayId == 8 || $gatewayId == 5 || $gatewayId == 9) {

                    ## Getting MIDService...
                    $paymentManager = new PaymentManager();
                    $midService = $paymentManager->getMidService($gatewayId);

                    ## Creating EpGrayPayManager class object...
                    $epgrayPayManager = new EpGrayPayManager();
                    $statusGrayPay = $epgrayPayManager->getStatusFromGrayPay($orderNo, $amountRequired, $midService);
                    if (!empty($statusGrayPay)) {
                        if ($statusGrayPay == 'complete') {
                            $status = "Paid at Back-End";
                        } else if ($statusGrayPay == 'Not Found at Back-End') {
                            $status = 'Not Paid at Back-End';
                        } else {
                            $status = 'Refunded';
                        }
                    }
                } else if ($gatewayId == 5) {
                    $epNmiManager = new EpNmiManager();
                    $statusNmi = $epNmiManager->getStatusFromNmi($orderNo, $amountRequired);
                    if (!empty($statusNmi)) {
                        if ($statusNmi == 'complete') {
                            $status = "Paid at Back-End";
                        } else if ($statusNmi == 'Not Found at Back-End') {
                            $status = 'Not Paid at Back-End';
                        } else {
                            $status = 'Refunded';
                        }
                    }
                } else {
                    $status = "Unable to get the payment status";
                }
            }
        }
        return $this->renderText($status);
    }

    public function executeUpdatePaymentAtBackEnd(sfWebRequest $request) {
        $orderNo = $request->getParameter('orderNo');
        $orderDetailsArray = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderNo);
        $orderRequestDetailsId = $orderDetailsArray[0]['order_request_detail_id'];
        $userId = $orderDetailsArray[0]['created_by'];

        //update payment_status in ipay4meOrder table
        $updateIp4mOrder = Doctrine::getTable('Ipay4meOrder')->updateIP4MOrderStatus($orderNo);

        //update payment_status in order request detail table
        $updateOrderReqDetail = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderRequestDetailsId, $userId);
        if (!empty($orderRequestDetailsId)) {
            $notificationUrl = "notification/PaymentNotificationV1";
            $senMailUrl = "notification/paymentSuccessMail";
            $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication', $notificationUrl, array('order_detail_id' => $orderRequestDetailsId));
            sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
            $this->auditPaymentTransaction($orderRequestDetailsId, $orderNo);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $url = url_for("report/getReceipt", true) . '?receiptId=' . $orderNo;
            $url1 = url_for("report/paymentHistory", true);

            $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $senMailUrl, array('order_detail_id' => $orderRequestDetailsId, 'url' => $url, 'url1' => $url1));
            sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
        }
        $flash = "This order number has been paid successfully";
        return $this->renderText($flash);
        //          return $this->getUser()->setFlash("notice","This order number has been paid successfully.");
    }

    public function executeSearchByOrderNo(sfWebRequest $request) {
        $this->isFound = false;
        $ipayOrderNo = $request->getParameter("order_no");
        $this->appid = $request->getParameter('app_id');
        $this->ref_no = $request->getParameter('ref_no');

        if (isset($ipayOrderNo) && $ipayOrderNo != '') {
            if (!is_numeric($ipayOrderNo)) {
                $this->getUser()->setFlash("error", "SW Global LLC order number is invalid.");
                $this->redirect("supportTool/searchOrderNo");
            }
            //get order details on ipay4me
            $ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpay4meOrderDetails($ipayOrderNo);

            $orderRequestId = $ipay4meOrderDetails[0]['order_request_detail_id'];
            $orderDetailsArray = OrderRequestDetailsTable::getInstance()->getOrderRequestId($orderRequestId);
            $retryId = $orderDetailsArray[0]['retry_id'];

            $cartItemTrArray = CartItemsTransactionsTable::getInstance()->getIPRequestId($retryId);
            $applicationArray = array();
            for ($i = 0; $i < count($cartItemTrArray); $i++) {
                $itemId = $cartItemTrArray[$i]['item_id'];
                $appIdArray = Doctrine_Core::getTable('IPaymentRequest')->getAppId($itemId);
                if (!empty($appIdArray)) {
                    $passportId = $appIdArray[0]['passport_id'];
                    $visaId = $appIdArray[0]['visa_id'];
                    $freezoneId = $appIdArray[0]['freezone_id'];
                    if (!empty($passportId)) {
                        $passportArray = Doctrine_Core::getTable('PassportApplication')->getPassportInfoById($passportId);

                        $title_id = $passportArray[0]['title_id'];
                        $last_name = $passportArray[0]['last_name'];
                        $first_name = $passportArray[0]['first_name'];
                        $mid_name = $passportArray[0]['mid_name'];
                        $name = $title_id . ' ' . $first_name . ' ' . $mid_name . ' ' . $last_name;

                        $applicationArray[$i]['app_type'] = 'Passport';
                        $applicationArray[$i]['app_id'] = $passportArray[0]['id'];
                        $applicationArray[$i]['ref_no'] = $passportArray[0]['ref_no'];
                        $applicationArray[$i]['name'] = $name;
                        $applicationArray[$i]['date_of_birth'] = $passportArray[0]['date_of_birth'];
                        $applicationArray[$i]['status'] = $passportArray[0]['status'];
                        $applicationArray[$i]['email'] = $passportArray[0]['email'];
                        $applicationArray[$i]['amount'] = $passportArray[0]['paid_dollar_amount'];
                        $applicationArray[$i]['paid_at'] = $passportArray[0]['paid_at'];
                        $applicationArray[$i]['order_number'] = $ipayOrderNo;
                    }
                    if (!empty($visaId)) {
                        $visaArray = Doctrine_Core::getTable('VisaApplication')->getVisaInfoById($visaId);

                        $title_id = $visaArray[0]['title'];
                        $last_name = $visaArray[0]['surname'];
                        $first_name = $visaArray[0]['other_name'];
                        $mid_name = $visaArray[0]['middle_name'];
                        $name = $title_id . ' ' . $first_name . ' ' . $mid_name . ' ' . $last_name;

                        $applicationArray[$i]['app_type'] = 'Visa';
                        $applicationArray[$i]['app_id'] = $visaArray[0]['id'];
                        $applicationArray[$i]['ref_no'] = $visaArray[0]['ref_no'];
                        $applicationArray[$i]['name'] = $name;
                        $applicationArray[$i]['date_of_birth'] = $visaArray[0]['date_of_birth'];
                        $applicationArray[$i]['status'] = $visaArray[0]['status'];
                        $applicationArray[$i]['email'] = $visaArray[0]['email'];
                        $applicationArray[$i]['amount'] = $visaArray[0]['paid_dollar_amount'];
                        $applicationArray[$i]['paid_at'] = $visaArray[0]['paid_at'];
                        $applicationArray[$i]['order_number'] = $ipayOrderNo;
                    }
                    if (!empty($freezoneId)) {
                        $visaArray = Doctrine_Core::getTable('VisaApplication')->getVisaInfoById($freezoneId);

                        $title_id = $visaArray[0]['title'];
                        $last_name = $visaArray[0]['surname'];
                        $first_name = $visaArray[0]['other_name'];
                        $mid_name = $visaArray[0]['middle_name'];
                        $name = $title_id . ' ' . $first_name . ' ' . $mid_name . ' ' . $last_name;

                        $applicationArray[$i]['app_type'] = 'Visa';
                        $applicationArray[$i]['app_id'] = $visaArray[0]['id'];
                        $applicationArray[$i]['ref_no'] = $visaArray[0]['ref_no'];
                        $applicationArray[$i]['name'] = $name;
                        $applicationArray[$i]['date_of_birth'] = $visaArray[0]['date_of_birth'];
                        $applicationArray[$i]['status'] = $visaArray[0]['status'];
                        $applicationArray[$i]['email'] = $visaArray[0]['email'];
                        $applicationArray[$i]['amount'] = $visaArray[0]['paid_dollar_amount'];
                        $applicationArray[$i]['paid_at'] = $visaArray[0]['paid_at'];
                        $applicationArray[$i]['order_number'] = $ipayOrderNo;
                    }
                }
            }
            $this->applicationArray = $applicationArray;
        }
    }

    /*
     * STARTING WIRE TRANSFER FUNCTIONS...
     */

    public function executeWireTransferReport(sfWebRequest $request) {
        // $this->isFound = false;
        if ($request->getParameter("wt_status"))
            $wireTransferStatus = $request->getParameter("wt_status");
        else
            $wireTransferStatus = '';
        $wireTransfer = $request->getParameter("wire_transfer");
        $trackingNumber = $request->getParameter("tracking_number");
        $order_number = $request->getParameter("order_number");

        if ($request->isMethod('post')) {
            unset($_SESSION['wt_status']);
            unset($_SESSION['wire_transfer']);
            unset($_SESSION['tracking_number']);
            unset($_SESSION['order_number']);

            $_SESSION['wt_status'] = $wireTransferStatus;
            $_SESSION['wire_transfer'] = $wireTransfer;
            $_SESSION['tracking_number'] = $trackingNumber;
            $_SESSION['order_number'] = $order_number;
        } else if ($request->hasParameter('back')) {
            $wireTransferStatus = $_SESSION['wt_status'];
            $wireTransfer = $_SESSION['wire_transfer'];
            $trackingNumber = $_SESSION['tracking_number'];
            $order_number = $_SESSION['order_number'];
        } else {
            $_SESSION['wt_status'] = NULL;
            $_SESSION['wire_transfer'] = NULL;
            $_SESSION['tracking_number'] = NULL;
            $_SESSION['order_number'] = NULL;
        }

        $userGroup = $this->getUser()->getGroups();
        $userId = $this->getUser()->getGuarduser()->getId();


        // if(isset($moneyOrderStatus) && $moneyOrderStatus !=''){
        $this->isFound = true;
        //find tracking number for money order
        $trackingNumberArray = array();
        $userGruoupId = '';
        if ($userGroup->getFirst()->getName() == 'payment_group') {
            $userGruoupId = $userId;
        }

        $trackingNumberArray = Doctrine::getTable('WireTransfer')->getTrackingNumberByStatus($wireTransferStatus, $wireTransfer, $trackingNumber, $userGruoupId, $order_number);
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('Wiretransfer', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($trackingNumberArray);
        $this->pager->setPage($this->getRequestParameter('page', $this->page));
        $this->pager->init();
        // }
    }

    public function executeWireTransferDetails(sfWebRequest $request) {

        $wireTransferId = $request->getParameter('wire_transfer_id');
        $this->back = $request->getParameter('back');
        $this->page = $request->getParameter('page');
        $this->order_number = $request->getParameter('order_number');
        $trackingDetails = WireTrackingNumberTable::getInstance()->getWireTransferDetails($wireTransferId);
        $this->wireTransferDetails = WiretransferTable::getInstance()->find($wireTransferId)->toArray();
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                // echo '<pre>';print_r($this->ipayNisAppDetails);die;
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];
                    $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                }
                $i++;
            }
            $this->applicationDetails = $applicationDetails;
            $this->trackingDetails = $trackingDetails;
        }
    }

    public function executeWtTrackingInfoBox(sfWebRequest $request) {
        $this->setLayout(false);
        $trackingNumberId = $request->getParameter('tracking_id');
        $this->status = $request->getParameter('status');
        $trackingDetail = Doctrine::getTable('Wiretransfer')->getWireTransferDetailByTrackingNumber($trackingNumberId);
        $arrMODetail['wire_transfer_number'] = $trackingDetail['0']['wire_transfer_number'];
        $arrMODetail['paid_date'] = $trackingDetail['0']['paid_date'];
        $arrMODetail['wt_amount'] = $trackingDetail['0']['amount'];
        $arrMODetail['status'] = $trackingDetail['0']['status'];
        $arrMODetail['tracking_number'] = $trackingDetail['0']['WireTrackingNumber'][0]['tracking_number'];
        $arrMODetail['tn_amount'] = $trackingDetail['0']['WireTrackingNumber'][0]['cart_amount'];
        $arrMODetail['order_number'] = $trackingDetail['0']['WireTrackingNumber'][0]['order_number'];
        $this->arrMODetail = $arrMODetail;
    }

    public function executeSearchByWireTransfer(sfWebRequest $request) {
        $this->isFound = true;
        $wireTransfer = $request->getParameter("wire_transfer");
        $trackingNumberQuery = Doctrine::getTable('Wiretransfer')->getAllTrackingNumber($wireTransfer);
        $this->pager = new rawDocPager(null, 10);
        $this->pager->setStatement($trackingNumberQuery);
        $this->pager->setPage($request->getParameter('page', 1));
        $this->pager->init();
    }

    public function executeUpdateWireTransfer(sfWebRequest $request) {
        $postArray = $request->getPostParameters();
        $arrWireTransfer = $postArray['hdn_wt_id'];

        for ($k = 0; $k < count($arrWireTransfer); $k++) {
            $trackingNumberQuery = Doctrine::getTable('Wiretransfer')->getAllWTDetails($arrWireTransfer[$k]);
            $WTId = $trackingNumberQuery[0]['id'];
            for ($i = 0; $i < count($trackingNumberQuery[0]['WireTrackingNumber']); $i++) {
                $orderNumber = $trackingNumberQuery[0]['WireTrackingNumber'][$i]['order_number'];

                $orderReqDetailId = $trackingNumberQuery[0]['WireTrackingNumber'][$i]['order_request_detail_id'];
                $userId = $trackingNumberQuery[0]['WireTrackingNumber'][$i]['user_id'];

                $comments = '';

                //update money order payment status in ipay4me_order
                $updateIp4mOrder = Doctrine::getTable('ipay4meOrder')->updateIP4WTorderStatus($orderNumber);
                //update payment_status in order request detail table
                $updateOrderReqDetail = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderReqDetailId, $userId);
                //update table money order
                $updateWT = Doctrine::getTable('Wiretransfer')->updateWTDetail($WTId, $comments);
                //update table cart tracking number
                $updatecartTrackingWT = Doctrine::getTable('WireTrackingNumber')->updateCartTrackingDetail($orderNumber, $orderReqDetailId);

                // start -Amit - split amount if payment is successfull
                $gatewayId = 7;
                $payManagerObj = new PaymentManager();
                $updateOrder = $payManagerObj->updateSplitAmount($orderReqDetailId, $gatewayId);
                // end -Amit -split amount



                $notificationUrl = "notification/PaymentNotificationV1";
                $senMailUrl = "notification/wireTransferPaymentSuccessMail";
                $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication', $notificationUrl, array('order_detail_id' => $orderReqDetailId));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                $this->auditPaymentTransaction($orderReqDetailId, $orderNumber);
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $url = url_for("report/getReceipt", true) . '?receiptId=' . $orderNumber;
                $url1 = url_for("report/paymentHistory", true);

                $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $senMailUrl, array('order_detail_id' => $orderReqDetailId, 'url' => $url, 'url1' => $url1));
                sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
            }
        }


        $this->getUser()->setFlash("notice", "Application(s) associated with this wire transfer has been paid successfully.");
        $this->redirect("supportTool/searchByWireTransfer");


        // echo '<pre>';print_r($postArray);die;
    }

    public function executeSearchByWireTransferNumber(sfWebRequest $request) {
        $this->setTemplate('searchByWireTransferNumber');
    }

    public function executeWireTransferDetailsTrackingNumbers(sfWebRequest $request) {
        $this->form = new WiretransferForm();
        $this->isValid = false;
        $this->wireTransferNumber = $request->getParameter('wire_transfer_number');
        $idflag = $request->getParameter('idflag');
        $status = '';
        $wireTransferDetail = Doctrine::getTable('Wiretransfer')->getWireTransferDetailByWireTransferNumber($this->wireTransferNumber);
        if (!empty($wireTransferDetail)) {
            $status = $wireTransferDetail['0']['status'];

            if ($status != 'Paid' || $idflag != '') {
                $this->wireTransferDetail = $wireTransferDetail;

                $this->wiretransferId = $wireTransferDetail['0']['id'];
                //          $trackingMoneyOrderDetail = Doctrine::getTable('WireTrackingNumber')->getTrackingMoneyOrderDetailByMoneyOrderId($this->wiretransferId);
                //          $finalArray = array();
                //          for($i=0;$i < count($trackingMoneyOrderDetail);$i++){
                //            $finalArray[$i] = $trackingMoneyOrderDetail[$i]['cart_track_id'];
                //          }

                $this->pendingtrackingNumbers = Doctrine::getTable('WireTrackingNumber')->getTrackingNumberById($this->wiretransferId);

                if (isset($this->pendingtrackingNumbers) && is_array($this->pendingtrackingNumbers) && count($this->pendingtrackingNumbers) > 0) {
                    $this->isValid = true;
                } else {
                    $this->getUser()->setFlash("error", "No tracking numbers for associate");
                    $this->redirect("supportTool/searchByWireTransferNumber");
                }
            } else {
                $this->getUser()->setFlash("error", "Wire Transfer Number is already Paid");
                $this->redirect("supportTool/searchByWireTransferNumber");
            }
        } else {

            $this->getUser()->setFlash("notice", "Wire Transfer Serial Number not found");
            $this->redirect("supportTool/searchByWireTransferNumber");
        }
    }

    public function executeUpdateWireTransferDetails(sfWebRequest $request) {

        $para = $request->getPostParameters();

        $wireTransferDetailId = $request->getParameter('wireTransferDetailId');
        $wireTransfer = $request->getParameter('wiretransfer');
        $wireTransferAmount = $wireTransfer['amount'];
        $wireTransferExisting = $request->getParameter('wireTransferNumber1');
        $wireTransferNumber = $wireTransfer['wiretransfer_number'];

        $isWireTransfer = Doctrine::getTable('Wiretransfer')->getWireTransferNumberId($wireTransferNumber, $wireTransferDetailId);
        $detail = WiretransferTable::getInstance()->findByWireTransferNumber($wireTransferExisting);
        $status = $detail->toArray();
        $statusFlag = ($status[0]['status'] == 'Paid') ? '1' : '';
        if ($isWireTransfer > 0) {
            $this->getUser()->setFlash("notice", "The Wire Transfer Serial Number entered is already used");
            $this->redirect("supportTool/wireTransferDetailsTrackingNumbers?wire_transfer_number=" . $wireTransferExisting . "&idflag=" . $statusFlag);
        } else {
            $msg = 'Invalid Tracking number.';
            $wireTransferDetail = Doctrine::getTable('Wiretransfer')->setWireTransferDetailByWireTransferId($wireTransferDetailId, $wireTransferAmount, $wireTransferNumber);
            if ($wireTransferDetail) {
                $trackingWireTransferDetail = Doctrine::getTable('WireTrackingNumber')->getTrackingWireTransferDetailByWireTransferId($wireTransferDetailId);

                $finalArray = array();
                for ($i = 0; $i < count($trackingWireTransferDetail); $i++) {
                    $finalArray[$i] = $trackingWireTransferDetail[$i]['id'];
                }
                $arrayPost = $request->getParameter('chk_fee');

                $arrayFinalPost = array();

                for ($j = 0; $j < count($arrayPost); $j++) {
                    $arrayValue = explode('_', $arrayPost[$j]);
                    $arrayFinalPost[$j] = $arrayValue['0'];
                }

                if (!empty($finalArray) && !empty($arrayFinalPost)) {
                    $resultToBeDeleted = array_diff($finalArray, $arrayFinalPost);
                }
                if (!empty($resultToBeDeleted)) {
                    $resultToBeDeleted = array_values($resultToBeDeleted);
                    Doctrine::getTable('WireTrackingNumber')->setTrackingNumberStatus($resultToBeDeleted);

                    //            for($k=0;$k<count($resultToBeDeleted);$k++)
                    //            {
                    //              $tracking_money_order_id = $resultToBeDeleted[$k];
                    //              $deleteDetail = Doctrine::getTable('TrackingMoneyOrder')->deleteDetailByMoneyOrderIdCartTrackId($wireTransferDetailId,$tracking_money_order_id);
                    //              //delete query
                    //            }
                }
                $msg = 'Wire Transfer Details updated successfully.';
            }//End of if($moneyOrderDetail)...

            $this->getUser()->setFlash("notice", $msg);
            $this->redirect("supportTool/wireTransferDetailsTrackingNumbers?wire_transfer_number=" . $wireTransferNumber);
        }
    }

    public function executeDisAllocateWT(sfWebRequest $request) {
        $this->setLayout(false);

        //      $moneyOrderNumber = $request->getParameter('moneyorder_number');
        //      $moneyOrderDetail = Doctrine::getTable('MoneyOrder')->getMoneyOrderDetailByMoneyOrderNumber($moneyOrderNumber);

        $wireTransferId = $request->getParameter('wiretransferId');
        //      $moneyorderId = $moneyOrderDetail['0']['id'];
        $trackingWireTransferDetail = Doctrine::getTable('WireTrackingNumber')->getTrackingWireTransferDetailByWireTransferId($wireTransferId);
        $trackingWireTransferDetailCount = count($trackingWireTransferDetail);
        $msg = 'Wire Transfer not found.';
        if ($trackingWireTransferDetailCount > 0) {
            $finalArray = array();

            for ($i = 0; $i < $trackingWireTransferDetailCount; $i++) {
                $finalArray[$i] = $trackingWireTransferDetail[$i]['id'];
            }
            if (count($finalArray) > 0) {
                $cartTrackingNumber = Doctrine::getTable('WireTrackingNumber')->setTrackingNumberStatus($finalArray);
                //$deleteTrackingMoneyOrder = Doctrine::getTable('TrackingMoneyOrder')->deleteDetailByMoneyOrderId($moneyorderId);
                $deleteMoneyOrder = Doctrine::getTable('Wiretransfer')->deleteDetailById($wireTransferId);
                $msg = 'Your Wire Transfer has been de-associated successfully.'; // Bug id: 29439
            }
        }
        $this->getUser()->setFlash("notice", $msg);
        $this->redirect("supportTool/searchByWireTransferNumber");
    }

    /*
     * ENDING WIRE TRANSFER FUNCTIONS...
     */

    /*
     * START USER TRANSACTION LIMIT
     * Author: Varun
     * Date 30/05/2011
     */

    public function executeUserTransactionLimit(sfWebRequest $request) {
        $this->fname = $request->getParameter("fname");
        $this->lname = $request->getParameter("lname");
        $this->email = $request->getParameter("email");
        $this->userList = $request->getParameter("userList");
        

        //creating session
        if ($request->isMethod('post')) {
            ## Removing session variables...
            $this->getUser()->getAttributeHolder()->remove('fname');
            $this->getUser()->getAttributeHolder()->remove('lname');
            $this->getUser()->getAttributeHolder()->remove('email');
            $this->getUser()->getAttributeHolder()->remove('userList');

            ## Setting session varibale for maintain search...
            $this->getUser()->setAttribute('fname', $this->fname);
            $this->getUser()->setAttribute('lname', $this->lname);
            $this->getUser()->setAttribute('email', $this->email);
            $this->getUser()->setAttribute('userList', $this->userList);


        } else if ($request->hasParameter('back')) {
            ## Getting session variables...
            $this->fname = $this->getUser()->getAttribute('fname');
            $this->lname = $this->getUser()->getAttribute('lname');
            $this->email = $this->getUser()->getAttribute('email');
            $this->userList = $this->getUser()->getAttribute('userList');
        } else {
            ## Removing session variables...
            $this->getUser()->getAttributeHolder()->remove('fname');
            $this->getUser()->getAttributeHolder()->remove('lname');
            $this->getUser()->getAttributeHolder()->remove('email');
            $this->getUser()->getAttributeHolder()->remove('userList');
        }
        //get Paymnet Group id from db
        $paymentGroupArray = Doctrine::getTable('sfGuardGroup')->findByName('payment_group');
        $paymentGroupId = $paymentGroupArray->getFirst()->getId();  


        if($this->userList == 'nutl'){
            $arrTotalUser = Doctrine::getTable('UserDetail')->getAllUser(trim($this->fname), trim($this->lname), trim($this->email), $paymentGroupId);            
        }else{
            $arrTotalUser = Doctrine::getTable('UserTransactionLimit')->getAllUsers(trim($this->fname), trim($this->lname), trim($this->email));
        }
        

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }


        $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($arrTotalUser);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function executeAddUserTransactionLimit(sfWebRequest $request) {
        $this->setTemplate("addUserTransactionLimit");
        $user_id = $request->getParameter('uid');
        $this->page = $request->getParameter('page');
        $encriptedUser_id = SecureQueryString::DECODE($user_id);
        $this->user_id = SecureQueryString::ENCRYPT_DECRYPT($encriptedUser_id);

        $this->form = new UserTransactionLimitForm();

        //      $this->cart_limit = $user_limit->getCartLimit();
    }

    public function executeEditUserTransactionLimit(sfWebRequest $request) {
        $this->setTemplate("editUserTransactionLimit");

        $user_id = $request->getParameter('uid');
        $this->page = $request->getParameter('page');

        $encriptedUser_id = SecureQueryString::DECODE($user_id);
        $this->user_id = SecureQueryString::ENCRYPT_DECRYPT($encriptedUser_id);

        $this->form = new UserTransactionLimitForm();
        $user_limit = Doctrine::getTable('UserTransactionLimit')->findByUserId($this->user_id);

        if ($user_limit->getfirst()->getCartCapacity() != '') {
            $this->form->setDefault('cart_capacity', $user_limit->getfirst()->getCartCapacity());
        }
        if ($user_limit->getfirst()->getCartAmountCapacity() != '') {
            $this->form->setDefault('cart_amount_capacity', $user_limit->getfirst()->getCartAmountCapacity());
        }
        if ($user_limit->getFirst()->getNumberOfTransaction() != '') {
            $this->form->setDefault('number_of_transaction', $user_limit->getFirst()->getNumberOfTransaction());
        }
        if ($user_limit->getfirst()->getTransactionPeriod() != '') {
            $this->form->setDefault('transaction_period', $user_limit->getfirst()->getTransactionPeriod());
        }

        //      $this->cart_limit = $user_limit->getCartLimit();
    }

    public function executeUpdateUserTransactionLimit(sfWebRequest $request) {
        $this->user_id = $request->getParameter('user_id');
        $page = $request->getParameter('page');
        $this->form = new UserTransactionLimitForm();
        $this->page = $request->getParameter('page');


        $this->form->bind($request->getPostParameters());

        if ($this->form->isValid()) {
            $user_id = $request->getParameter('user_id');
            $check = false;
            $cart_capacity = $request->getParameter('cart_capacity');
            $cart_amount_capacity = $request->getParameter('cart_amount_capacity');
            $number_of_transaction = $request->getParameter('number_of_transaction');
            $transaction_period = $request->getParameter('transaction_period');
            $ifLimitPresent = Doctrine::getTable('UserTransactionLimit')->checkUserId($user_id);
            // Start Varun
            // code for email on edit of transacation limits
            // Check old data present in database , and compare it with latest current data entered by admin.
            $oldData = Doctrine::getTable('UserTransactionLimit')->getDetailByUserId($user_id);
            $old_cart_capacity = $oldData->getFirst()->getCartCapacity();
            $old_cart_amount_capacity = $oldData->getFirst()->getCartAmountCapacity();
            $old_number_of_transaction = $oldData->getFirst()->getNumberOfTransaction();
            $old_transaction_period = $oldData->getFirst()->getTransactionPeriod();
            $latest_cart_capacity = '';
            $latest_cart_amount_capacity = '';
            $latest_number_of_transaction = '';
            $latest_transaction_period = '';

            $updateIp4mOrder = Doctrine::getTable('UserTransactionLimit')->updateUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent);
            $this->getUser()->setFlash('notice', 'User transaction limit(s) has been updated');
            if ($old_cart_capacity != $cart_capacity) {
                $check = true;
                $latest_cart_capacity = $cart_capacity;
            } else {
                $latest_cart_capacity = $cart_capacity;
            }
            if ($old_cart_amount_capacity != $cart_amount_capacity) {
                $check = true;
                $latest_cart_amount_capacity = $cart_amount_capacity;
            } else {
                $latest_cart_amount_capacity = $cart_amount_capacity;
            }
            if ($old_number_of_transaction != $number_of_transaction) {
                $check = true;
                $latest_number_of_transaction = $number_of_transaction;
            } else {
                $latest_number_of_transaction = $number_of_transaction;
            }
            if ($old_transaction_period != $transaction_period) {
                $check = true;
                $latest_transaction_period = $transaction_period;
            } else {
                $latest_transaction_period = $transaction_period;
            }
            if ($check) {

                if ($latest_cart_capacity != '') {
                    $fcart_capacity = $latest_cart_capacity;
                } else {
                    if ($old_cart_capacity == $latest_cart_capacity) {
                        $fcart_capacity = '';
                    } else {
                        $fcart_capacity = "Limit as per your Processing Country";
                    }
                }
                if ($latest_cart_amount_capacity != '') {
                    $fcart_amount_capacity = $latest_cart_amount_capacity;
                } else {
                    if ($old_cart_amount_capacity == $latest_cart_amount_capacity) {
                        $fcart_amount_capacity = '';
                    } else {
                        $fcart_amount_capacity = "Limit as per your Processing Country";
                    }
                }


                if ($latest_number_of_transaction != '') {
                    $fnumber_of_transaction = $latest_number_of_transaction;
                } else {
                    if ($old_number_of_transaction == $latest_number_of_transaction) {
                        $fnumber_of_transaction = '';
                    } else {
                        $fnumber_of_transaction = "Limit as per your Processing Country";
                    }
                }


                if ($latest_transaction_period != '') {
                    $ftransaction_period = $latest_transaction_period;
                } else {
                    if ($old_transaction_period == $latest_transaction_period) {
                        $ftransaction_period = '';
                    } else {
                        $ftransaction_period = "Limit as per your Processing Country";
                    }
                }
                $mailUrl = "notification/updateUserTransactionLimitMail";
                $arrayEE = array('cart_capacity' => $fcart_capacity, "cart_amount_capacity" => $fcart_amount_capacity, "number_of_transaction" => $fnumber_of_transaction, "transaction_period" => $ftransaction_period, "user_id" => $user_id);
                $mailTaskId = EpjobsContext::getInstance()->addJob('UpdateUserTransactionLimit', $mailUrl, $arrayEE);
                //End Varun
            }
            $this->redirect('supportTool/userTransactionLimit?back=1&page=' . $page);
        } else {
            $this->setTemplate("editUserTransactionLimit");
        }
    }

    public function executeAddUserTransactionLimitDetail(sfWebRequest $request) {
        $this->user_id = $request->getParameter('user_id');
        $page = $request->getParameter('page');
        $this->form = new UserTransactionLimitForm();
        $this->form->bind($request->getPostParameters());
        $this->page = $request->getParameter('page');
        if ($this->form->isValid()) {
            $user_id = $request->getParameter('user_id');
            $cart_capacity = $request->getParameter('cart_capacity');
            $cart_amount_capacity = $request->getParameter('cart_amount_capacity');
            $number_of_transaction = $request->getParameter('number_of_transaction');
            $transaction_period = $request->getParameter('transaction_period');
            $ifLimitPresent = Doctrine::getTable('UserTransactionLimit')->checkUserId($user_id);
            $updateIp4mOrder = Doctrine::getTable('UserTransactionLimit')->addUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent);
            $this->getUser()->setFlash('notice', 'User transaction limit(s) has been added');
            //Start Varun
            //Code for email on add of transaction limits
            if ($cart_capacity != '' || $cart_amount_capacity != '' || $number_of_transaction != '' || $transaction_period != '') {
                $mailUrl = "notification/addUserTransactionLimitMail";
                $mailTaskId = EpjobsContext::getInstance()->addJob('AddUserTransactionLimit', $mailUrl, array('cart_capacity' => $cart_capacity, "cart_amount_capacity" => $cart_amount_capacity, "number_of_transaction" => $number_of_transaction, "transaction_period" => $transaction_period, "user_id" => $user_id));
            }
            //End Varun
            $this->redirect('supportTool/userTransactionLimit?back=1&page=' . $page);
        } else {
            $this->setTemplate("addUserTransactionLimit");
        }
    }

    /*
     * END USER TRANSACTION LIMIT
     */

    /* Start Amit -> To find Black Listed User Report */

    public function executeBlackListedReport(sfWebRequest $request) {
        $this->setTemplate("blackListedReport");
        if ($request->isMethod('POST')) {
            $parm = $request->getPostParameters();
            $this->fdate = $parm['fdate'];
            $this->tdate = $parm['tdate'];
            $this->cardFirst = $parm['card_first'];
            $this->cardLast = $parm['card_last'];
        } else {
            $this->fdate = $request->getParameter('fdate');
            $this->tdate = $request->getParameter('tdate');
            $this->cardFirst = $request->getParameter('card_first');
            $this->cardLast = $request->getParameter('card_last');
        }
        $this->isReport = false;
        $this->isFound = false;
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        if ((isset($this->fdate) && $this->fdate != '' && isset($this->tdate) && $this->tdate != '') || (isset($this->cardFirst) && $this->cardFirst != '' && isset($this->cardLast) && $this->cardLast != '' )) {
            $this->isReport = true;
            $blcakListedReportQuery = FpsDetailTable::getInstance()->getBlackListUserDetails($email = '', $this->fdate, $this->tdate, $this->cardFirst, $this->cardLast);

            $this->pager = new rawDocPager(null, sfConfig::get('app_records_per_page'));
            $this->pager->setStatement($blcakListedReportQuery);
            $this->pager->setPage($this->page);
            $this->pager->init();


            $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");
            //         $x = Doctrine_Manager::connection()->getManager()->getConnectionName($db);
            $blackListedReport = $db->execute($blcakListedReportQuery)->fetchAll();

            $this->filename = 'Black Listed Report.xls';
            $excel = new ExcelWriter('uploads/excel/' . $this->filename);
            if ($excel == false) {
                echo $excel->error;
            }
            //$myArr=array("<b>S.No.</b>","<b>Application Type</b>","<b>App Id</b>","<b>Ref No</b>","<b>Applicant Name</b>","<b>Date Of Birth</b>","<b>Email</b>","<b>Processing Country</b>","<b>Embassy</b>","<b>Order Number</b>","<b>Payment Gateway</b>","<b>Card Number</b>","<b>Card Holder Name</b>","<b>User Name</b>","<b>Email</b>","<b>Blocked Date</b>",);
            $myArr = array("<b>S.No.</b>", "<b>Application Type</b>", "<b>App Id</b>", "<b>Ref No</b>", "<b>Applicant Name</b>", "<b>Date Of Birth</b>", "<b>Email</b>", "<b>Processing Country</b>", "<b>Embassy</b>", "<b>Order Number</b>", "<b>Payment Gateway</b>", "<b>Card Number</b>", "<b>Card Holder Name</b>", "<b>Blocked Date</b>",);
            $excel->writeLine($myArr);

            $count = count($blackListedReport);
            $srNo = 1;
            if ($count > 0) {
                for ($i = 0; $i < $count; $i++) {
                    $orderNumber = $blackListedReport[$i]['order_number'];
                    $cardLen = $blackListedReport[$i]['card_len'];
                    $middlenum = '';
                    $cardDisplay = '';
                    if ($cardLen == 13) {
                        $middlenum = "-XXXXX-";
                    } else if ($cardLen == 16) {
                        $middlenum = "-XXXX-XXXX-";
                    } else if ($cardLen == 15) {
                        $middlenum = "-XXXXXXX-";
                    }
                    $cardDisplay = $blackListedReport[$i]['card_first'] . $middlenum . $blackListedReport[$i]['card_last'];

                    $ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);
                    $gatewayId = $ipay4meOrderDetails[0]['gateway_id'];
                    $gatewayName = Doctrine::getTable('Gateway')->getDisplayName($gatewayId);
                    $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);
                    $appDetailArray = $ipayNisDetailsQuery->execute()->toArray();
                    $nisHelper = new NisHelper();
                    $appObj = CartItemsTransactionsTable::getInstance();
                    $app = array();

                    if (count($appDetailArray) > 0) {
                        foreach ($appDetailArray as $result) {
                            $app['passport_id'] = $result['passport_id'];
                            $app['visa_id'] = $result['visa_id'];
                            $app['freezone_id'] = $result['freezone_id'];
                            $appDetails = $appObj->getApplicationDetails($app);
                            // echo '<pre>';print_r($appDetails);die;
                            if ($appDetails['app_type'] == 'passport') {
                                $countryId = $appDetails['country_id'];
                                $embassyId = $appDetails['embassy_id'];
                            } else {
                                if ($appDetails['app_type'] == 'freezone') {
                                    $countryId = 'NG';
                                    $embassyId = '';
                                } else {
                                    $visaApplicantArray = Doctrine_Core::getTable('VisaApplicantInfo')->findByApplicationId($appDetails['id'])->toArray();
                                    $countryId = $visaApplicantArray[0]['applying_country_id'];
                                    $embassyId = $visaApplicantArray[0]['embassy_of_pref_id'];
                                }
                            }

                            $countryNameArr = Doctrine_Core::getTable('Country')->find($countryId)->toArray();
                            $countryName = $countryNameArr['country_name'];
                            if ($embassyId != '') {
                                $embassyNameArr = Doctrine_Core::getTable('EmbassyMaster')->find($embassyId)->toArray();
                                $embassyName = $embassyNameArr['embassy_name'];
                            } else {
                                $embassyName = 'N/A';
                            }

                            $finalStatus = $nisHelper->isAllAppReadyRefunded($orderNumber, $appDetails['id']);
                            $fStatus = null;
                            $appStatus = '';
                            if (isset($finalStatus) && is_array($finalStatus) && count($finalStatus) > 0) {
                                $fStatus = $finalStatus['action'];
                            }
                            if (isset($fStatus) && $fStatus != null) {
                                $appStatus = $fStatus;
                            } else {
                                $appStatus = $appDetails['status'];
                            }
                            //$arrayData = array($i+1,$appDetails['app_type'],$appDetails['id'],$appDetails['ref_no'],$appDetails['name'],$appDetails['dob'],$appDetails['email'],$countryName,$embassyName,$blackListedReport[$i]['order_number'],$gatewayName,$cardDisplay,$blackListedReport[$i]['payor_name'],$blackListedReport[$i]['fname'].' '.$blackListedReport[$i]['lname'],$blackListedReport[$i]['email'],date_format(date_create($blackListedReport[$i]['blocked_date']),'d-m-Y'),);
                            $arrayData = array($srNo, $appDetails['app_type'], $appDetails['id'], $appDetails['ref_no'], $appDetails['name'], $appDetails['dob'], $appDetails['email'], $countryName, $embassyName, $blackListedReport[$i]['order_number'], $gatewayName, $cardDisplay, $blackListedReport[$i]['payor_name'], date_format(date_create($blackListedReport[$i]['blocked_date']), 'd-m-Y'),);
                            $excel->writeLine($arrayData);
                            $srNo++;
                        }
                    }
                }
            } $excel->close();
        }
    }

    /* End Amit ->Black Listed User Report */
    /*
     * START USER TRANSACTION LIMIT STATUS
     * Author: Varun
     * Date 16/11/2011
     */

    public function executeUserTransactionLimitStatus(sfWebRequest $request) {
        $this->setTemplate("userTransactionLimitStatus");
        $this->emailId = $request->getPostParameter("email");
        $this->isFound = false;
        if (isset($this->emailId) && $this->emailId != '') {
            $userDetails = UserDetailTable::getInstance()->findByEmail($this->emailId);
            ##$userCount = $userDetails->count();
            if (count($userDetails)) {
                ##$userDetails = $userDetails->toArray();
                ##$userId = $userDetails->getFirst()->getUserId();
                $this->name = $userDetails->getFirst()->getFirstName() . " " . $userDetails->getFirst()->getLastName();
                $this->email = $userDetails->getFirst()->getEmail();
                $this->phone = $userDetails->getFirst()->getMobilePhone();
                $this->userId = $userDetails->getFirst()->getUserId();
                $arrCartItems = unserialize($userDetails[0]['cart_items']);
                if ($arrCartItems) {
                    foreach ($arrCartItems as $items) { 
                        if ($items->getType() == 'Passport') {
                            $firstProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
                        } elseif ($items->getType() == 'Freezone') {
                            $firstProcessingCountry[] = 'NG';
                        } elseif ($items->getType() == 'Visa') {
                            $firstProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
                        } elseif ($items->getType() == 'Vap') {
                            $firstProcessingCountry[] = Doctrine::getTable('VapApplication')->getProcessingCountry($items->getAppId());
                        }
                    }
                    $firstProcessingCountry = $firstProcessingCountry[count($firstProcessingCountry) - 1];
                } else {
                    $firstProcessingCountry = 'ALL';
                }
                
                $userTransactionLimit = UserTransactionLimitTable::getInstance()->findByUserId($this->userId);
                
                $countryPaymentMode = CountryPaymentModeTable::getInstance()->findByCountryCode($firstProcessingCountry);
                if(count($countryPaymentMode) < 1){
                    $countryPaymentMode = CountryPaymentModeTable::getInstance()->findByCountryCode('ALL');
                }

                if (isset($userTransactionLimit[0]['cart_amount_capacity']) && $userTransactionLimit[0]['cart_amount_capacity'] != '') {
                    $this->cartAmountCapacity = $userTransactionLimit[0]['cart_amount_capacity'];
                } else {
                    $this->cartAmountCapacity = $countryPaymentMode[0]['cart_amount_capacity'];
                }
                if (isset($userTransactionLimit[0]['number_of_transaction']) && $userTransactionLimit[0]['number_of_transaction'] != '') {
                    $this->numberOfTransaction = $userTransactionLimit[0]['number_of_transaction'];
                } else {
                    $this->numberOfTransaction = $countryPaymentMode[0]['number_of_transaction'];
                }
                if (isset($userTransactionLimit[0]['transaction_period']) && $userTransactionLimit[0]['transaction_period'] != '') {
                    $transactionPeriod = $userTransactionLimit[0]['transaction_period'];
                } else {
                    $transactionPeriod = $countryPaymentMode[0]['transaction_period'];
                }
                $this->timePeriod = date("Y-m-d", mktime(date("H"), date("i") - $transactionPeriod, date("s"), date("m"), date("d"), date('Y')));
                $this->currentDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date('Y')));

                $this->transactionPeriod = Settings::maxcard_time($transactionPeriod);


                $fpsDetails = Doctrine::getTable('FpsDetail')->getCardhash($this->timePeriod, $this->currentDate, $this->userId);
                $this->fpsDetails = $fpsDetails->execute()->toArray();
                $page = 1;
                $this->pager = new sfDoctrinePager('FpsDetail', sfConfig::get('app_records_per_page'));                
                $this->pager->setPage($this->getRequestParameter('page', $page));
                $this->pager->init();
                $this->isFound = true;
            } else {
                $this->getUser()->setFlash("error", "No record found");
            }
        }
    }

    /* End Varun ->User transaction limit status */

    public function executeUpdateApplication(sfWebRequest $request) {
        // $this->isFound = false;
        $AppId = $request->getParameter("app_id");
        $referenceNo = $request->getParameter("reference_no");

        if ($request->isMethod('post')) {
            unset($_SESSION['app_id']);
            unset($_SESSION['reference_no']);

            $_SESSION['app_id'] = $AppId;
            $_SESSION['reference_no'] = $referenceNo;
        } else if ($request->hasParameter('back')) {
            $AppId = $_SESSION['app_id'];
            $referenceNo = $_SESSION['reference_no'];
        } else {
            $_SESSION['app_id'] = NULL;
            $_SESSION['reference_no'] = NULL;
        }
    }

    public function executeNisApplicationDetails(sfWebRequest $request) {

        ## Removing app id from session...
        $this->getUser()->getAttributeHolder()->remove('app_id');

        $this->appType = ($request->hasParameter('app_type')) ? $request->getParameter('app_type') : '';
        $this->appId = trim(($request->hasParameter('app_id')) ? $request->getParameter('app_id') : '');
        $this->refNo = trim(($request->hasParameter('ref_no')) ? $request->getParameter('ref_no') : '');
        $this->getUser()->setAttribute('notice', '');
        if ($request->isMethod('post') || $this->appId != '') {
            if ($this->appType != '' && $this->appId != '' && $this->refNo != '') {
                $data = array();
                if (strtolower($this->appType) == 'visa' || strtolower($this->appType) == 'freezone' || strtolower($this->appType) == 'reentry freezone') {
                    $visaArray = Doctrine::getTable('VisaApplication')->getVisaApplicationDetails($this->appId, $this->refNo);
                    if (count($visaArray) > 0) {
                        $czoneTypeId = Doctrine::getTable('VisaZoneType')->getConventionalZoneId();

                        if ($visaArray[0]['visacategory_id'] == '101') {
                            $data['app_type'] = 'Freezone';
                            $processingCountry = Doctrine::getTable('Country')->getCountryName($visaArray[0]['VisaApplicantInfo']['applying_country_id']);
                            $data['embassy_name'] = $visaArray[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
                        } else if ($visaArray[0]['visacategory_id'] == '102') {
                            $data['app_type'] = 'Reentry FreeZone';
                            $processingCountry = 'N/A';
                            $data['embassy_name'] = 'N/A';
                            $visaArray[0]['eyes_color'] = 'N/A';
                            $visaArray[0]['hair_color'] = 'N/A';
                        } else {
                            $data['app_type'] = $this->appType;
                            $processingCountry = Doctrine::getTable('Country')->getCountryName($visaArray[0]['VisaApplicantInfo']['applying_country_id']);
                            $data['embassy_name'] = $visaArray[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
                        }

                        $data['id'] = $visaArray[0]['id'];
                        $data['ref_no'] = $visaArray[0]['ref_no'];
                        $data['title'] = $visaArray[0]['title'];
                        $data['first_name'] = $visaArray[0]['other_name'];
                        $data['mid_name'] = $visaArray[0]['middle_name'];
                        $data['last_name'] = $visaArray[0]['surname'];
                        $data['date_of_birth'] = $visaArray[0]['date_of_birth'];
                        $data['country_name'] = $processingCountry;
                        $data['interview_date'] = $visaArray[0]['interview_date'];
                        $data['color_eyes_id'] = $visaArray[0]['eyes_color'];
                        $data['color_hair_id'] = $visaArray[0]['hair_color'];
                        $data['status'] = $visaArray[0]['status'];
                    } else {
                        $this->getUser()->setFlash('notice', "Application Not Found");
                        $this->redirect('supportTool/nisApplicationDetails');
                    }
                }
                if (strtolower($this->appType) == 'passport') {
                    $passportArray = Doctrine::getTable('PassportApplication')->getPassportApplicationDetails($this->appId, $this->refNo);
                    if (count($passportArray) > 0) {
                        $processingCountry = Doctrine::getTable('Country')->getCountryName($passportArray[0]['processing_country_id']);

                        $data['app_type'] = $this->appType;
                        $data['id'] = $passportArray[0]['id'];
                        $data['ref_no'] = $passportArray[0]['ref_no'];
                        $data['title'] = $passportArray[0]['title_id'];
                        $data['first_name'] = $passportArray[0]['first_name'];
                        $data['mid_name'] = $passportArray[0]['mid_name'];
                        $data['last_name'] = $passportArray[0]['last_name'];
                        $data['date_of_birth'] = $passportArray[0]['date_of_birth'];
                        $data['country_name'] = $processingCountry;
                        $data['embassy_name'] = $passportArray[0]['EmbassyMaster']['embassy_name'];
                        $data['interview_date'] = $passportArray[0]['interview_date'];
                        $data['color_eyes_id'] = $passportArray[0]['color_eyes_id'];
                        $data['color_hair_id'] = $passportArray[0]['color_hair_id'];
                        $data['status'] = $passportArray[0]['status'];
                    } else {
                        $this->getUser()->setFlash('notice', 'Application Not Found');
                        $this->redirect('supportTool/nisApplicationDetails');
                    }
                }
                if (strtolower($this->appType) == 'vap') {
                    $vapArray = Doctrine::getTable('VapApplication')->getVapApplicationDetails($this->appId, $this->refNo);
                    //                echo "<pre>";print_r($vapArray);die;
                    if (count($vapArray) > 0) {
                        //$processingCountry =  Doctrine::getTable('Country')->getCountryName($vapArray[0]['processing_country_id']);

                        $data['app_type'] = $this->appType;
                        $data['id'] = $vapArray[0]['id'];
                        $data['ref_no'] = $vapArray[0]['ref_no'];
                        $data['title'] = $vapArray[0]['title'];
                        $data['first_name'] = $vapArray[0]['first_name'];
                        $data['mid_name'] = $vapArray[0]['middle_name'];
                        $data['last_name'] = $vapArray[0]['surname'];
                        $data['date_of_birth'] = $vapArray[0]['date_of_birth'];
                        $data['country_name'] = 'Nigeria';
                        $data['embassy_name'] = $vapArray[0]['processing_center_name'];
                        $data['color_eyes_id'] = $vapArray[0]['eyes_color'];
                        $data['color_hair_id'] = $vapArray[0]['hair_color'];
                        $data['status'] = $vapArray[0]['status'];
                    } else {
                        $this->getUser()->setFlash('notice', 'Application Not Found');
                        $this->redirect('supportTool/nisApplicationDetails');
                    }
                }

                $this->getUser()->setAttribute('session_app_id', $this->appId);
                $this->getUser()->setAttribute('session_app_type', $data['app_type']);
                $this->getUser()->setAttribute('session_ref_no', $this->refNo);
                $this->data = $data;
                $this->setTemplate('nisApplicationDetails');
            } else {
                $this->getUser()->setFlash('notice', "Please Enter required information");
                $this->setTemplate('nisApplicationDetails');
            }
        }
    }

    public function executeEditApplicationDetails(sfWebRequest $request) {
        $this->appType = Settings::decryptInput($request->getParameter('app_type'));
        $this->appId = trim(Settings::decryptInput($request->getParameter('app_id')));
        $this->refNo = trim(Settings::decryptInput($request->getParameter('ref_no')));

        $this->getUser()->setAttribute('app_id', $this->appId);

        if (($this->appType == $this->getUser()->getAttribute('session_app_type')) && ($this->appId == $this->getUser()->getAttribute('session_app_id')) && ($this->refNo == $this->getUser()->getAttribute('session_ref_no'))) {
            if (strtolower($this->appType) == 'passport') {
                $passportObj = Doctrine::getTable('PassportApplication')->find($this->appId);
                $this->form = new EditPassportApplicationForm($passportObj);
            } else if (strtolower($this->appType) == 'visa' || strtolower($this->appType) == 'freezone' || strtolower($this->appType) == 'reentry freezone') {
                $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
                $this->form = new EditVisaApplicationForm($visaObj);
            } else if (strtolower($this->appType) == 'vap') {
                $visaObj = Doctrine::getTable('VapApplication')->find($this->appId);
                $this->form = new EditVapApplicationForm($visaObj);
            } else {
                $this->getUser()->setFlash('notice', "Tempering URL is not allowed");
                $this->redirect('supportTool/nisApplicationDetails');
            }
        } else {
            $this->getUser()->getAttributeHolder()->remove('session_app_type');
            $this->getUser()->getAttributeHolder()->remove('session_app_id');
            $this->getUser()->getAttributeHolder()->remove('session_ref_no');
            $this->getUser()->setFlash('notice', "Tempering URL is not allowed");
            $this->redirect('supportTool/nisApplicationDetails');
        }
    }

    public function executeUpdateApplicationDetails(sfWebRequest $request) {
        $this->appType = $request->getParameter('app_type');
        $this->appId = trim($request->getParameter('app_id'));
        $this->refNo = trim($request->getParameter('ref_no'));


        if ($this->appId != $this->getUser()->getAttribute('app_id')) {
            $this->getUser()->setFlash('notice', 'Tempering of data is not allowed');
            $this->redirect('supportTool/nisApplicationDetails');
        }


        if (isset($this->appType) && isset($this->appId) && isset($this->refNo)) {
            $parm = $request->getParameter('editApplicationForm');

            $appType = strtolower($this->appType);
            if ($appType == 'passport') {
                $passportObj = Doctrine::getTable('PassportApplication')->find($this->appId);
                $this->form = new editPassportApplicationForm($passportObj);
            } else if ($appType == 'visa' || $appType == 'freezone' || $appType == 'reentry freezone') {
                $visaObj = Doctrine::getTable('VisaApplication')->find($this->appId);
                $this->form = new EditVisaApplicationForm($visaObj);
            } else {
                $vapObj = Doctrine::getTable('VapApplication')->find($this->appId);
                $this->form = new EditVapApplicationForm($vapObj);
                $appType = 'vap';
            }

            $this->processForm($request, $this->form, $appType, $this->appId, $this->refNo);
            $this->setTemplate('editApplicationDetails');
        } else {
            $this->getUser()->setFlash('notice', 'Tempering URL is not allowed');
            $this->redirect('supportTool/nisApplicationDetails');
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $appType = '', $appId='', $refNo='') {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $appObj = $form->save();
            if ($appObj->getId()) {
                $this->getUser()->setFlash('notice', 'Application Updated Successfully');
                $this->redirect('supportTool/nisApplicationDetails?app_type=' . strtolower($appType) . '&app_id=' . $appId . '&ref_no=' . $refNo);
            }
        } else {

        }
    }

    public function executeDownloadMoneyOrderReportInExcel(sfWebRequest $request) {

        $moneyOrderStatus = ($request->getParameter("mo_status")) ? $request->getParameter("mo_status") : '';
        $currencyText = ($request->getParameter("currency")) ? $request->getParameter("currency") : '';
        $currency = '';
        switch($currencyText){
            case 'dollar':
                $currency = 1;
                break;
            case 'pound':
                $currency = 5;
                break;            
        }

        $moneyOrder = $request->getParameter("money_order");
        $trackingNumber = $request->getParameter("tracking_number");
        $order_number = $request->getParameter("order_number");
        $fdate = $request->getParameter("fdate");
        $tdate = $request->getParameter("tdate");

        $userGroup = $this->getUser()->getGroups();
        $userId = $this->getUser()->getGuarduser()->getId();

        ## find tracking number for money order
        $userGruoupId = '';
        if ($userGroup->getFirst()->getName() == 'payment_group') {
            $userGruoupId = $userId;
        }

        $filename = 'MoneyOrderReport.xls';
        $i = 1;
        $isExcelShow = 'no';

        if (isset($fdate) && !empty($fdate) && isset($tdate) && !empty($tdate)) {

            set_time_limit(0);
            ini_set('memory_limit', '512M');

            $trackingNuberArray = Doctrine::getTable('Moneyorder')->getTrackingNumberByStatus($moneyOrderStatus, $moneyOrder, $trackingNumber, $userGruoupId, $order_number, $currency, $fdate, $tdate);
            $moneyOrderReport = $trackingNuberArray->execute();
            $totalRecord = count($moneyOrderReport);
            if ($totalRecord > 0) {

                $excel = new ExcelWriter('uploads/excel/' . $filename);
                if ($excel == false) {
                    $isExcelShow = 'error'; //$excel->error;
                } else {
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
                    $myArr = array("<b>S.No.</b>", "<b>Money Order Serial #</b>", "<b>Tracking #</b>", "<b>Order #</b>", "<b>Associate Date</b>", "<b>Paid Date</b>", "<b>Currency</b>", "<b>Amount</b>", "<b>Status</b>");
                    $excel->writeLine($myArr);

                    foreach ($moneyOrderReport AS $result) {
                        if ($result['currency'] == '5') {
                            $orderAmount = $result['convert_amount'];
                        } else {
                            $orderAmount = $result['cart_amount'];
                        }
                        $currencySymbol = PaymentModeManager::currencySymbol($result['currency']);

                        if ($result['order_number'] == '' || $result['order_number'] == '0') {
                            $orderNumber = '--';
                        } else {
                            $orderNumber = $result['order_number'];
                        }

                        if ($result['status'] == 'Paid') {
                            $paidDate = $result['updated_at'];
                        } else {
                            $paidDate = '--';
                        }

                        $arrayData = array($i, $result['TrackingMoneyorder'][0]['Moneyorder']['moneyorder_number'], $result['tracking_number'], $orderNumber, $result['associated_date'], $paidDate, html_entity_decode($currencySymbol), format_amount($orderAmount), $result['status']);
                        $excel->writeLine($arrayData);
                        $i++;
                    }//End of foreach($moneyOrderReport AS $result){...
                    $isExcelShow = 'yes';
                }
                $excel->close();
            }//End of if($totalRecord > 0){...
        }//End of if(isset($fdate) && !empty($fdate) && isset($tdate) && !empty($tdate)){...

        return $this->renderText($isExcelShow);
    }

//End of public function executeDownloadMoneyOrderReportInExcel...

    /**
     *
     * @param <type> $request
     * This function provide searching for users who is blocked... ([WP: 079] => CR: 114)
     */
    public function executeUnblockUser(sfWebRequest $request) {
        $this->email = trim($request->getParameter('email'));
        $this->userDetailArray = array();
        if (isset($this->email) && !empty($this->email)) {
            $userDetailArray = Doctrine::getTable('UserDetail')->findByEmail($this->email);
            if (count($userDetailArray) > 0) {
                $this->userDetailArray = $userDetailArray;
            } else {
                $this->getUser()->setFlash('notice', 'No User found by this email.');
                $this->redirect('supportTool/unblockUser');
            }
        }
    }

    public function executeSearchBlackListedApplications(sfWebRequest $request) {
        $orderNumber = $request->getParameter('order_no');
        $ipay4meOrder = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderNumber)->toArray();
        $applicantArray = array();
        $requestId = $ipay4meOrder[0]['order_request_detail_id'];
        $transactionServiceArray = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($requestId)->toArray();
        If (!empty($transactionServiceArray)) {
            for ($i = 0; $i < count($transactionServiceArray); $i++) {
                $applicantArray[$i]['app_type'] = $transactionServiceArray[$i]['app_type'];
                $applicantArray[$i]['app_id'] = $transactionServiceArray[$i]['app_id'];
                $applicantArray[$i]['ref_no'] = $transactionServiceArray[$i]['ref_no'];
            }
        }
        $blockApplicationArray = array();
        for ($i = 0; $i < count($applicantArray); $i++) {
            $tblBlockApplicant = Doctrine::getTable('TblBlockApplicant')->findByAppId($applicantArray[$i]['app_id'])->toArray();

            if (isset($tblBlockApplicant) && !empty($tblBlockApplicant)) {
                if (strtolower($tblBlockApplicant[0]['blocked']) == 'yes') {
                    $processingCountry = Doctrine::getTable('Country')->getCountryName($tblBlockApplicant[$i]['processing_country']);
                    $j = 0;
                    $blockApplicationArray[$j]['app_type'] = $tblBlockApplicant[0]['app_type'];
                    $blockApplicationArray[$j]['app_id'] = $tblBlockApplicant[0]['app_id'];
                    $blockApplicationArray[$j]['ref_no'] = $tblBlockApplicant[0]['ref_no'];
                    $blockApplicationArray[$j]['name'] = $tblBlockApplicant[0]['first_name'] . ' ' . $tblBlockApplicant[0]['middle_name'] . ' ' . $tblBlockApplicant[0]['last_name'];
                    $blockApplicationArray[$j]['dob'] = $tblBlockApplicant[0]['dob'];
                    $blockApplicationArray[$j]['processing_country'] = $processingCountry;
                    $j++;
                }
            }
        }
        $this->blockApplicationArray = $blockApplicationArray;
        $this->setLayout(false);
    }

    /**
     * [WP: 079] => CR: 114
     * This function unblock user...
     * Required user id...
     */
    public function executeDoUnblockUser(sfWebRequest $request) {
        $userId = base64_decode($request->getParameter('id'));
        $unblocked = false;
        $userDetailArray = Doctrine::getTable('UserDetail')->findByUserId($userId);
        if (count($userDetailArray) > 0) {
            if ($userDetailArray->getFirst()->getPaymentRights() == '1') {
                $userDetailArray->getFirst()->setPaymentRights('0');
                $userDetailArray->getFirst()->save();
                $unblocked = true;
            }
        }
        if ($unblocked) {
            $notice = 'User has been unblocked successfully';
        } else {
            $notice = 'There is some problem while unblocking, Please try after some time.';
        }
        $this->getUser()->setFlash('notice', $notice);
        $this->redirect('supportTool/unblockUser');
    }

    public function executeOrderStatusFromProcessor(sfWebRequest $request) {

        if ($request->isMethod('POST')) {
            $noticeFlag = false;
            $this->orderNumber = $request->getParameter('ordernumber');
            $orderDetail = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($this->orderNumber)->toArray();
            if (count($orderDetail) < 1) {
                $noticeFlag = true;
                $notice = 'This Order Number doesn\'t exist on the portal.';
            } else if ($orderDetail[0]['gateway_id'] != 10) {
                $noticeFlag = true;
                $notice = 'This Order Number doesn\'t belong to JNA.';
            } else {

                $epNmiObj = new EpNmiManager();

                $con = mysql_connect('192.168.10.237', 'root', 'root');
                if (!$con) {
                    die('Couldn\'t connect to server');
                }
                mysql_select_db('paip_db', $con);
                $query = "SELECT * FROM order_info where shop_order_id =" . $this->orderNumber;
                $data = mysql_query($query);
                $dataArr = mysql_fetch_assoc($data);

                $jnaOrderId = $dataArr['id'];

                $query = "SELECT * FROM order_detail_info where order_id =" . $jnaOrderId;
                $data2 = mysql_query($query);
                if (!empty($data2)) {
                    $this->responseArray = mysql_fetch_assoc($data2);
                    mysql_close();
                }


//                    $requestXml = $this->createValcardRequestXml($orderNumber,$sessionId);
//                    $url = 'https://127.0.0.1:5555/Exec';
//                    $url = 'https://196.46.20.36/shopindex.jsp';
//                    $header[] = "Content-Type: text/xml;charset=UTF-8";
//                    $curl = curl_init();
//                    curl_setopt($curl,CURLOPT_URL,$url);
//                    curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
//                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
//                    curl_setopt($curl, CURLOPT_POST, 1);
//                    curl_setopt($curl, CURLOPT_POSTFIELDS, $requestXml);
//                    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//
//                    $responseXml = curl_exec($curl);
//                    echo "<pre>";print_r($responseXml);die;
            }
            if ($noticeFlag) {
                $this->getUser()->setFlash('notice', $notice);
                $this->redirect('supportTool/orderStatusFromProcessor');
            }
        }
    }

    protected function createValcardRequestXml($orderNumber, $sessionId) {
        $dom = new DomDocument('1.0');

        $structElem = $dom->createElement('TKKPG');
        $root1 = $dom->appendChild($structElem);

        $structElem = $dom->createElement('Request');
        $root2 = $root1->appendChild($structElem);


        $structElem = $dom->createElement('Operation');
        $root3 = $root2->appendChild($structElem);
        $structElem->appendChild($dom->createTextNode('GetOrderStatus'));

        $structElem = $dom->createElement('Language');
        $root4 = $root2->appendChild($structElem);
        $structElem->appendChild($dom->createTextNode('EN'));

        $structElem = $dom->createElement('Order');
        $root5 = $root2->appendChild($structElem);

        $structElem = $dom->createElement('Merchant');
        $root = $root5->appendChild($structElem);
        $structElem->appendChild($dom->createTextNode('SWGLOBALDOLLAR'));

        $structElem = $dom->createElement('OrderID');
        $root = $root5->appendChild($structElem);
        $structElem->appendChild($dom->createTextNode($orderNumber));

        $structElem = $dom->createElement('SessionID');
        $root = $root2->appendChild($structElem);
        $structElem->appendChild($dom->createTextNode($sessionId));


        $reqXml = $dom->saveXML();
        return $reqXml;
    }

    /**
     * [WP: 100] => CR: 142
     * Follwoing function is being used to unpay applications from Ipay4me and NIS
     */
    public function executeUnpaidApplications(sfWebRequest $request) {
        $this->isFound = false;
        $this->notRefundMessage = '';
        $template = $request->getParameter('template');
        $this->refunded = $request->getParameter("refunded");
        if ($request->isMethod('POST')) {
            $this->ipayOrderNo = $request->getParameter("order_no");
            $search = true;
        } else {
            $this->ipayOrderNo = Settings::decryptInput($request->getParameter("order_no"));
            $search = false;
        }
        if (isset($this->ipayOrderNo) && $this->ipayOrderNo != '' && $search) {
            if (!is_numeric($this->ipayOrderNo)) {
                $this->getUser()->setFlash("error", "SW Global LLC order number is invalid.");
                $this->redirect("supportTool/unpaidApplications");
            }
            //get order details on ipay4me
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($this->ipayOrderNo);



            $gatewayId = $this->ipay4meOrderDetails[0]['gateway_id'];
            $this->gatewayName = Doctrine::getTable('Gateway')->getDisplayName($gatewayId);
            if ($gatewayId == '2' || $gatewayId == '3' || $gatewayId == '4' || $gatewayId == '6' || $gatewayId == '7') {
                $this->notRefundMessage = "Refund is not allowed here for this Payment Gateway";
            }

            $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($this->ipayOrderNo);
            $this->nisHelper = new NisHelper();
            $page = 1;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $this->pager = new sfDoctrinePager('PassportApplication', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($ipayNisDetailsQuery);
            $this->pager->setPage($this->getRequestParameter('page', $page));
            $this->pager->init();
            $this->appObj = CartItemsTransactionsTable::getInstance();
            $this->isFound = true;
            if ($template == 'blackListedReport') {
                $this->appDetailArray = $ipayNisDetailsQuery->execute()->toArray();
                $this->setTemplate('applicationBlackListedReport');
            }
        }
    }

//End of public function executeUnpaidApplications(sfWebRequest $request) {...

    /**
     * [WP: 100] => CR: 142
     * @param <type> $request
     * unpay Nis & Ipay4me Applications
     */
    public function executeUnpayNisIpay4me(sfwebRequest $request) {
        $actionType = $request->getParameter('actionType');
        $order_no = $request->getParameter('hdnOrderNumber');
        $this->refunded = $request->getParameter('refunded');
        $hdnRefunded = $request->getParameter('hdnRefunded');
        //$hdnPaidApplicationCount = $request->getParameter('hdnPaidApplicationCount');



        $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($order_no)->execute();
        $applicationArray = $ipayNisDetailsQuery->toArray();
        $hdnPaidApplicationCount = 0;
        foreach ($applicationArray AS $result) {
            $app['passport_id'] = $result['passport_id'];
            $app['visa_id'] = $result['visa_id'];
            $app['freezone_id'] = $result['freezone_id'];
            $app['visa_arrival_program_id'] = $result['visa_arrival_program_id'];

            ## Fetching application details...
            $appDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($app);
            if ($appDetails['status'] != 'New') {
                $hdnPaidApplicationCount++;
            }
        }

        $checkbox_values = $request->getParameter('chk_fee');
        $no_of_app = count($checkbox_values);
        $count = 0;
        $passport_flow_type = 'PassportWorkflow';
        $visa_flow_type = 'VisaWorkflow';

        ## Creating connction for NIS database...
        $databaseConf = sfYaml::load(sfConfig::get('sf_config_dir') . '/databases.yml');
        $conparamsNIS = $databaseConf ['all']['connection_nis']['param'];
        $nisConn = Doctrine_Manager::connection('dsn: ' . $conparamsNIS['dsn'], 'connection_nis');

        ## Connection for Ipay4me database...
        $conparamsIPAY4ME = $databaseConf ['all']['connection_ipay4me']['param'];
        $ipay4meConn = Doctrine_Manager::connection('dsn: ' . $conparamsIPAY4ME['dsn'], 'connection_ipay4me');

        try {

            if ($no_of_app > 0) {

                $nisConn->beginTransaction();
                $ipay4meConn->beginTransaction();

                foreach ($checkbox_values as $val) {
                    $appDetails = array();
                    $appDetails = explode('_', $val);
                    $app_type = strtolower($appDetails[1]);
                    $appId = (int) $appDetails[0];
                    $actionPerformed = false;
                    if ($app_type != '' && $appId != '') {
                        $order_request_detail_id = $this->VisaAndPassportCommQueries($order_no);
                        if ($app_type == 'passport') {
                            $appObj = Doctrine::getTable('PassportApplication')->find($appId);
                            if (!empty($appObj)) {
                                if ($appObj->getStatus() != 'New') {
                                    ## execute all queries...
                                    $delete = Doctrine::getTable('PassportVettingQueue')->deletePassportRecord($appId);
                                    if ($delete) {
                                        $this->VisaAndPassportWorkpoolQueries($appId, $passport_flow_type);
                                        $update_passport = Doctrine::getTable('PassportApplication')->updatePassportApp($appId);
                                        $actionPerformed = true;
                                    }
                                }
                            }
                        } else if ($app_type == 'visa' || $app_type == 'freezone') {
                            $appObj = Doctrine::getTable('VisaApplication')->find($appId);
                            if (!empty($appObj)) {
                                if ($appObj->getStatus() != 'New') {
                                    $delete = Doctrine::getTable('VisaVettingQueue')->deleteVisaRecord($appId);
                                    if ($delete) {
                                        $this->VisaAndPassportWorkpoolQueries($appId, $visa_flow_type);
                                        $update_visa = Doctrine::getTable('VisaApplication')->updateVisaApp($appId);
                                        $actionPerformed = true;
                                    }
                                }
                            }
                        } else if ($app_type == 'vap') {
                            $appObj = Doctrine::getTable('VapApplication')->find($appId);
                            if (!empty($appObj)) {
                                if ($appObj->getStatus() != 'New') {
                                    $update_vap = Doctrine::getTable('VapApplication')->updateVapApp($appId);
                                    $actionPerformed = true;
                                }
                            }
                        }
                        if ($actionPerformed) {
                            $count++;

                            $orderRequestDetailId = Doctrine::getTable('Ipay4meOrder')->getOrderRequestDetailId($order_no);
                            $transactionDetailsObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appId, $app_type, $orderRequestDetailId);
                            

                            $isOrderExist = Doctrine::getTable('RollbackPayment')->findByOrderNumber($order_no)->toArray() ;

                            if(count($isOrderExist) > 0){                                   
                               $isRollbackDetailsUpdated = Doctrine::getTable('RollbackPaymentDetails')->setRollbackDetail($appId, $app_type, $transactionDetailsObj->getFirst()->getAppAmount(), $isOrderExist[0]['id'], 'refund', 'request sent for refund') ;
                            }else{
                                $comments = 'back-end process';
                                $isRollbackUpdated = Doctrine::getTable('RollbackPayment')->setRollback($order_no, $comments) ;
                                $isRollbackDetailsUpdated = Doctrine::getTable('RollbackPaymentDetails')->setRollbackDetail($appId, $app_type, $transactionDetailsObj->getFirst()->getAppAmount(), $isRollbackUpdated, 'refund', 'request sent for refund') ;
                            }
                            
                            /**
                             * [WP: 112] => CR: 158
                             * Updating payment status set to 2 for refund...
                             */
                            Functions::updateApplicationPaymentStatus($appId, $app_type, $order_no, $status = 2);

                        }
                    }//End of if ($app_type != '' && $appId != '') {...
                }//End of foreach ($checkbox_values as $val) {...

                if ($count == $hdnPaidApplicationCount) {
                    $ipay4me_payment_response = Doctrine::getTable('IPaymentResponse')->deleteRecord($order_no);
                    if ($actionType == "ipay4me_nis") {
                        $orderStatus = ($hdnRefunded == 'true') ? 2 : 1;
                        $update_order = Doctrine::getTable('Ipay4meOrder')->setOrderStatus($order_no, $orderStatus);
                        if ($update_order) {
                            $update_order_req_details = Doctrine::getTable('OrderRequestDetails')->setOrderRequestDetailStatus($order_request_detail_id, $orderStatus);
                        }
                    }
                }//End of if ($count == $hdnPaidApplicationCount){...

                if ($actionPerformed) {
                    $this->getUser()->setFlash('error', 'Application(s) have been unpaid successfully.');
                } else {
                    $this->getUser()->setFlash('error', 'Sorry!! Application(s) have not been unpaid.');
                }


                $this->getUser()->setFlash('error', 'Application(s) has been unpaid successfully.');

                $nisConn->commit();
                $ipay4meConn->commit();
            } else {
                $this->getUser()->setFlash('error', 'Please select atleast one application.');
            }
        } catch (Exception $e) {
            //try {
                $nisConn->rollback();
                $ipay4meConn->rollback();
                //echo("Problem found while Unpay the data" . $e->getMessage()); die;
                $msg = 'Problem found while Unpay the data: ' . $e->getMessage();
                $this->getUser()->setFlash('error', $msg);
            //} catch (Exception $e) {
                //error message will come here...
                //echo("Problem found while Unpay the data" . $e->getMessage());
                //$msg = 'Problem found while Unpay the data: ' . $e->getMessage();
                //$this->getUser()->setFlash('error', $msg);
            //}
        }



        $this->redirect('supportTool/unpaidApplications?order_no=' . Settings::encryptInput($order_no));
    }

    /**
     * [WP: 100] => CR: 142
     * @param <type> $order_no
     * @return <type>
     * Common Function for Unapy Visa & PASSPORT Application
     */
    private function VisaAndPassportCommQueries($order_no) {
        $order_details = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($order_no);
        $order_request_detail_id = $order_details->getFirst()->getOrderRequestDetailId();
        return $order_request_detail_id;
    }

    /**
     * [WP: 100] => CR: 142
     * @param <type> $appId
     * @param <type> $flow_type
     * Common Function for Unapy Visa & PASSPORT Application
     */
    private function VisaAndPassportWorkpoolQueries($appId, $flow_type) {
        $workpoolData = Doctrine::getTable('Workpool')->findByApplicationIdAndFlowName($appId, $flow_type);
        if (count($workpoolData) > 0) {
            $workpool_app_id = $workpoolData->getFirst()->getApplicationId();
            $workpool_execution_id = $workpoolData->getFirst()->getExecutionId();
            $delete_workpool = Doctrine::getTable('Workpool')->deleteRecord($workpool_execution_id);
            if ($delete_workpool) {
                $delete_execution = Doctrine::getTable('Execution')->deleteRecord($workpool_execution_id);
                if ($delete_execution) {
                    $delete_execution_state = Doctrine::getTable('ExecutionState')->deleteRecord($workpool_execution_id);
                }
            }
        }
    }

    /**
     * [WP: 101] => CR: 143
     * @param <type> $request
     * Function to ChargeBack Multiple Orders
     */
    public function executeChargeBackOrder(sfwebRequest $request) {
        $this->isFound = false;
        $this->notRefundMessage = '';
        $template = $request->getParameter('template');
        if ($request->isMethod('POST')) {
            $this->ipayOrderNo = $request->getParameter("order_no");
        } else {
            $this->ipayOrderNo = Settings::decryptInput($request->getParameter("order_no"));
        }
        $ipayOrderNoArr = explode(',', $this->ipayOrderNo);        
        $ipayOrderNoArray = array();
        $i = 0;
        foreach ($ipayOrderNoArr as $val) {
            $val = trim($val);
            if ($val == '') {
                continue;
            }
            $val = str_replace(' ','',$val);
            $val = str_replace("'",'',$val);
            $val = str_replace('"','',$val);
            $ipayOrderNoArray[$i] = $val;
            $i++;
        }
        if(count($ipayOrderNoArray) > 0){
            $ipayOrderNoArray = array_unique($ipayOrderNoArray);
        }        
        $this->ipayNisDetailsQuery = Array();
        if (count($ipayOrderNoArray) > 0) {
            foreach ($ipayOrderNoArray as $order_no) {
                $this->isFound = true;
                $j = 0;
                $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($order_no);                
                if($this->ipay4meOrderDetails > 0){
                    $gatewayId = $this->ipay4meOrderDetails[$j]['gateway_id'];
                    $this->gatewayName = Doctrine::getTable('Gateway')->getDisplayName($gatewayId);
                    if ($gatewayId == '2' || $gatewayId == '3' || $gatewayId == '4' || $gatewayId == '6' || $gatewayId == '7') {
                        $this->notRefundMessage = "Refund is not allowed here for this Payment Gateway";
                    }
                    $CartDetails = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($order_no)->execute()->toArray();
                    $this->ipayNisDetailsQuery[][$order_no] = $CartDetails;
                }
            }
        }
    }

    /**
     *
     * @param sfwebRequest $request
     *
     */
    public function executeMultipleOrderChargeBack(sfwebRequest $request) {
        $this->setTemplate(false);
        $chk_order_arr = $request->getParameter('chk_order');
        $comment = $request->getParameter("comment");
        $count_orders = count($chk_order_arr);
        $orderNotChargeBackArr = array();
        $orderChargeBackArr = array();
        foreach ($chk_order_arr as $orderNumber) {           
            $isValid = ipay4meOrderTable::getInstance()->findByOrderNumber($orderNumber)->count();
            if ($isValid) {

                //get order details on ipay4me
            $this->ipay4meOrderDetails = ipay4meOrderTable::getInstance()->getIpayOrderDetails($orderNumber);
            $price = ipay4meOrderTable::getInstance()->getOrderNumberPrice($orderNumber);
            if ($price[0]['currency'] == '1') {
                $this->totAmount = $price[0]['amount'];
            } else {
                $this->totAmount = $price[0]['convert_amount'];
            }
            $this->currencySymbol = CurrencyManager::currencySymbolByCurrencyId($price[0]['currency']);
            $ipayNisDetailsQuery = CartItemsTransactionsTable::getInstance()->getCartDetailsQuery($orderNumber);
             $this->nisHelper = new NisHelper();
             $this->appObj = CartItemsTransactionsTable::getInstance();
             $paymentManager = new PaymentManager();
              $isValidDetails = $this->nisHelper->isValidRefund($orderNumber,true);
              if ($isValidDetails['nisStatus'] && $isValidDetails['ipay4meStatus']) {
                    $gatewayId = ipay4meOrderTable::getInstance()->getGatewayId($orderNumber);
                    $refundStatus = $this->nisHelper->isAllReadyRefunded($orderNumber);
                    if (isset($refundStatus) && is_array($refundStatus) && count($refundStatus) > 0) {
                        $orderNotChargeBackArr[] = $orderNumber;
                    }else{
                        $arrAppId = $request->getParameter('chk_apps_'.$orderNumber);                        
                        $chargeBackReq = $paymentManager->chargebackPayment($orderNumber, $comment, $arrAppId);
                        if ($chargeBackReq) {
                            ## Revert NIS Application
                            $revertStatus = $this->nisHelper->revertNisApplicationStatus($orderNumber);
                            $allOrderNumber = $paymentManager->getAllPaymentByCard($orderNumber);
                            if (!empty($allOrderNumber)) {
                                foreach ($allOrderNumber as $key => $value) {
                                    $revertStatus = $this->nisHelper->revertNisApplicationStatus($value['order_number']);
                                }
                            }
                            $orderChargeBackArr[] = $orderNumber;
                        } else {
                            $orderNotChargeBackArr[] = $orderNumber; }
                    }

              } else {
                $orderNotChargeBackArr[] = $orderNumber;
            }
          }//End of if ($isVlid) {...
        }//End of foreach ($chk_order_arr as $orderNumber) {...

        
        if(count($orderNotChargeBackArr) > 0){
            $orderNotChargeBack = implode(',', $orderNotChargeBackArr);
            $errorMsg = $orderNotChargeBack. ' can not be charge-back due to some reason.';
        }

        if(count($orderChargeBackArr) == $count_orders){
            $notice = 'All selected order(s) has been charge-back successfully.';
        }else if(count($orderChargeBackArr) > 0){
            $notice = 'Few selected order(s) has been charge-back successfully and '.$errorMsg;
        }else{
            $notice = $errorMsg;
        }
        
        $this->getUser()->setFlash("notice", $notice);
        $this->redirect("supportTool/chargeBackOrder");
        
    }//End of function...

    /**
     * [WP: 103] => CR: 147
     * @param <type> $request
     * This is being used to ublock applicants, user, cards...
     */
    public function executeUnblockApplicants(sfWebRequest $request) {
        $this->isFound = false;        
        $this->refunded = 2;
        $this->ipayOrderNo = '';
        $this->userPaymentRights = '';
        $this->fpsRuleId = '';
        if ($request->isMethod('POST')) {
            $this->ipayOrderNo = $request->getParameter("order_no");
            if (isset($this->ipayOrderNo) && $this->ipayOrderNo != '') {
                if (!is_numeric($this->ipayOrderNo)) {
                    $this->getUser()->setFlash("error", "SW Global LLC order number is invalid.");
                    $this->redirect("supportTool/unblockApplicants");
                    exit;
                }
                ## Fetching order number deatils from rollback payment table...
                $this->rollbackDetails = Doctrine::getTable('RollbackPayment')->findByOrderNumber($this->ipayOrderNo);                
                if(count($this->rollbackDetails)){
                    $this->isFound = true;

                    $ipay4meOrderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($this->ipayOrderNo);
                    if(count($ipay4meOrderObj)){
                        $orderRequestDetailObj = Doctrine::getTable('OrderRequestDetails')->find($ipay4meOrderObj->getFirst()->getOrderRequestDetailId());
                        if(!empty($orderRequestDetailObj)){
                            $userObj = Doctrine::getTable('UserDetail')->findByUserId($orderRequestDetailObj->getUserId());
                            if(count($userObj)){
                                $this->userPaymentRights = $userObj->getFirst()->getPaymentRights();
                            }
                        }
                        $fpsOrderObj = Doctrine::getTable('FpsDetail')->findByOrderNumber($this->ipayOrderNo);                        
                        if(count($fpsOrderObj)){
                            $this->fpsRuleId = $fpsOrderObj[0]['fps_rule_id'];
                        }
                    }
                }else{
                    $this->getUser()->setFlash("error", "SW Global LLC order number does not exist for charge-back.");
                    $this->redirect("supportTool/unblockApplicants");
                    exit;
                }
            }
        } 
        
    }//End of public function executeUnblockApplicants(sfWebRequest $request) {...

    /**
     * [WP: 103] => CR: 147
     * @param <type> $request
     * Fetching applicants details...
     */
    public function executeBlockApplicantDetails(sfWebRequest $request) {

        $this->setLayout(false);

        $appId = $request->getParameter("appId");
        $appType = $request->getParameter("appType");
        $this->divId = $request->getParameter("divId");
        $this->appDetailsObj = Doctrine::getTable('TblBlockApplicant')->findByAppIdAndAppType($appId, $appType);
        
    }//End of public function executeblockApplicantsDetails(sfWebRequest $request) {...


    /**
     * [WP: 103] => CR: 147
     * @param <type> $request
     */
    public function executeMarkUnblockApplicants(sfWebRequest $request) {

        $checkbox_values = $request->getParameter('chk_fee');
        $no_of_app = count($checkbox_values);

        ## Creating connction for NIS database...
        $databaseConf = sfYaml::load(sfConfig::get('sf_config_dir') . '/databases.yml');
        $conparamsNIS = $databaseConf ['all']['connection_nis']['param'];
        $nisConn = Doctrine_Manager::connection('dsn: ' . $conparamsNIS['dsn'], 'connection_nis');

        ## Connection for Ipay4me database...
        $conparamsIPAY4ME = $databaseConf ['all']['connection_ipay4me']['param'];
        $ipay4meConn = Doctrine_Manager::connection('dsn: ' . $conparamsIPAY4ME['dsn'], 'connection_ipay4me');       

        try{

            $nisConn->beginTransaction();
            $ipay4meConn->beginTransaction();

            if($no_of_app > 0){
                $completedAppArray = array();
                $i = 1;
                foreach($checkbox_values AS $values){
                    $appData = explode('_',$values);                    
                    $applicantObj = Doctrine::getTable('TblBlockApplicant')->findByAppIdAndAppType($appData[1], $appData[2]);                    
                    if(count($applicantObj)){                        
                        
                        $ipay4meOrderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($appData[0]);
                        if(count($ipay4meOrderObj)){
                            
                            if($i == 1){
                                ## Setting payment status 2 in ipay4me order table...
                                $ipay4meOrderObj->getFirst()->setPaymentStatus(2);
                                $ipay4meOrderObj->getFirst()->save();


                                ## Unblocking card...
                                $card_hash = $ipay4meOrderObj->getFirst()->getCardHash();
                                $fpsDetailObj = Doctrine::getTable('FpsDetail')->UnblockCardsByCardNum($card_hash);

                                

                                ## Setting order request detail id 2...
                                Doctrine::getTable('OrderRequestDetails')->setBlockStatus($ipay4meOrderObj->getFirst()->getOrderRequestDetailId());

                                

                                $userObj = Doctrine::getTable('OrderRequestDetails')->find($ipay4meOrderObj->getFirst()->getOrderRequestDetailId());
                                if(!empty($userObj)){
                                    $userId = $userObj->getUserId();
                                    Doctrine::getTable('UserDetail')->setUnblockPaymentRights($userId);
                                }//End of if(!empty($userObj)){...

                               

                            }//End of if($i == 1){...

                            ## Setting rollback payment status refund...
                            $rollbackPayemntObj = Doctrine::getTable('RollbackPayment')->findByOrderNumber($appData[0]);
                            if(count($rollbackPayemntObj)){
                                $rollbackStatus = Doctrine::getTable('RollbackPaymentDetails')->setRollbackAction($appData[1], $appData[2], $rollbackPayemntObj->getFirst()->getId(), 'refund');
                            }

                            

                            ## Setting blocked status 'no' to get unblocked...
                            $applicantObj->getFirst()->setBlocked('no');
                            $applicantObj->getFirst()->save();

                             

                            $i++;
                            $completedAppArray[] = $appData[1];
                        }//End of if(count($ipay4meOrderObj)){...
                    }//End of if(count($applicantObj)){...
                }//End of foreach($checkbox_values AS $values){...

                if(count($completedAppArray)){
                    $nisConn->commit();
                    $ipay4meConn->commit();

                    
                    if(count($completedAppArray) == 1){
                        $msg = 'Applicant has been unblocked successfully. ';
                    }else{
                        $msg = 'Applicants have been unblocked successfully. ';
                    }
                }else{
                    $msg = 'Applicant(s) have not been unblocked. ';
                }
                $this->getUser()->setFlash("error", $msg);
                $this->redirect("supportTool/unblockApplicants");
                exit;
            }else{
                $this->getUser()->setFlash("error", "Please select atleast one applicant.");
                $this->redirect("supportTool/unblockApplicants");
                exit;
            }//End of if($no_of_app > 0){...           
            
        }catch (Exception $e) {            
            $nisConn->rollback();
            $ipay4meConn->rollback();
            $msg = 'Problem found while Unblock applicants: ' . $e->getMessage();
            $this->getUser()->setFlash('error', $msg);            
            $this->redirect("supportTool/unblockApplicants");
            exit;            
        }
        
        
    }//End of public function executeMarkUnblockApplicants(sfWebRequest $request) {...

    
    public function executeResetTransactionLimit(sfWebRequest $request) {

        $this->userId = Settings::decryptInput($request->getParameter('uid'));
        $this->cardHash = $request->getParameter('cardHash');

        $transData = Doctrine::getTable('UserTransactionLimit')->findByUserId($this->userId);

        if(count($transData)){
            $this->numberOfTransaction = (int)$transData->getFirst()->getNumberOfTransaction();
            $transactionPeriod = $transData->getFirst()->getTransactionPeriod();
        }

        if($transactionPeriod == 0 || $transactionPeriod == ''){
            $transactionPeriod = '43200';
        }

        if($this->numberOfTransaction == 0 || $this->numberOfTransaction == ''){
            $this->numberOfTransaction = 1;
        }

        $timePeriod = date("Y-m-d", mktime(date("H"), date("i") - $transactionPeriod, date("s"), date("m"), date("d"), date('Y')));
        $currentDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date('Y')));

        
        $txtValue = ceil(trim($request->getParameter('txtValue')));
        if($txtValue != ''){
            $this->setLayout(false);
            $resetTransaction = Doctrine::getTable('FpsDetail')->resetUserTransactions($this->userId, $timePeriod, $currentDate, $txtValue, $this->cardHash);
            if($resetTransaction){
                $result = "done";
            }else{
                $result = "error";
            }
            echo $result;
            exit;
        }

        $userTransactionsObj = Doctrine::getTable('FpsDetail')->getSuccessTransactions($this->userId, $timePeriod, $currentDate, $this->cardHash);        
        $this->transactionUsed = count($userTransactionsObj);

        $userData = Doctrine::getTable('UserDetail')->findByUserId($this->userId);
        $this->firstName = '';
        $this->lastName = '';
        $this->email = '';
        if(count($userData)){
            $this->firstName = $userData->getFirst()->getFirstName();
            $this->lastName = $userData->getFirst()->getLastName();
            $this->email = $userData->getFirst()->getEmail();
        }  
        
    }//End of public function executeResetLimit(sfWebRequest $request) {...


    /**
     * [WP: 110] => CR: 156
     * @param <type> $request
     */
    public function executeResetUserTransactionLimit(sfWebRequest $request) {
        $this->card_first = $request->getParameter("card_first");
        $this->card_last = $request->getParameter("card_last");
        $this->email = $request->getParameter("email");

        $this->dataFlag = false;


        //creating session
        if ($request->isMethod('post')) {
            ## Removing session variables...
            $this->getUser()->getAttributeHolder()->remove('card_first');
            $this->getUser()->getAttributeHolder()->remove('card_last');
            $this->getUser()->getAttributeHolder()->remove('email');
            

            ## Setting session varibale for maintain search...
            $this->getUser()->setAttribute('card_first', $this->card_first);
            $this->getUser()->setAttribute('card_last', $this->card_last);
            $this->getUser()->setAttribute('email', $this->email);
            

            $this->dataFlag = true;

        } else if ($request->hasParameter('back')) {
            ## Getting session variables...
            $this->card_first = $this->getUser()->getAttribute('card_first');
            $this->card_last = $this->getUser()->getAttribute('card_last');
            $this->email = $this->getUser()->getAttribute('email');
            

            $this->dataFlag = true;

        } else {
            ## Removing session variables...
            $this->getUser()->getAttributeHolder()->remove('card_first');
            $this->getUser()->getAttributeHolder()->remove('card_last');
            $this->getUser()->getAttributeHolder()->remove('email');
            
        }

         if($this->dataFlag){
            $userId = NULL;
            if($this->email != ''){
                $userData = Doctrine::getTable('userDetail')->getPaymentUserByEmail($this->email);
                if(count($userData)){
                    $userId = $userData->getFirst()->getUserId();
                }else{
                    $userData = Doctrine::getTable('ApplicantVault')->findByEmail($this->email);
                    if(count($userData)){
                        $userId = $userData->getFirst()->getUserId();
                    }
                }
            }

            $arrTotalUser = Doctrine::getTable('ApplicantVault')->getDetailsByCardNumber($this->card_first, $this->card_last, '', '', $this->email, $userId, $status = 'Approved');


            $this->page = 1;
                if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }

            $this->pager = new sfDoctrinePager('ApplicantVault', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($arrTotalUser);
            $this->pager->setPage($this->page);
            $this->pager->init();
             
         }//End of if($this->dataFlag){...
    }//End of public function executeResetUserTransactionLimit(sfWebRequest $request) {...

}
