<?php use_helper('EPortal'); ?>
<?php include_partial('global/innerHeading',array('heading'=>'Know Your Customer'));?>


<div class="global_content4 clearfix">

<h2 class="successBox">It is required to acquire an eWallet account in SW Global LLC which will enable you to make multiple payments.</h2>

<div class="brdBox">
 <div class="clearfix"/>
    <?php // echo ePortal_highlight('eWallet Secure Certificate'); ?>
<?php echo form_tag($sf_context->getModuleName().'/'.$sf_context->getActionName(),array('name'=>'signup','id'=>'signup','method'=>'post','enctype'=>'multipart/form-data', 'onSubmit'=>'return validateSignForm()')); ?>
 <div class="wrapForm2">
<div class="divBlock" class="passPolicy" align="left">&nbsp;</div>
<?php
    switch($kycStatus)
    {
      case 2:

?>

     <div class="descriptionArea">

         Thank you for applying an SW Global LLC eWallet Secure Certificate.<br><br>

         Now your account is queued for approval.<br>
         You will be notified when an action is recorded on your application.

    </div>

<?php  break; ////////////Key status 2 ////////////
      case 1:

?>

     <div class="descriptionArea">

          Congratulations! Your application for SW Global LLC eWallet Secure Certificate has been approved.<br>
          You can now use your eWallet Payment facility.


    </div>

<?php  break;////////////Key status 1 /////////////
case 3:

?>

     <div class="descriptionArea">

            We regret to inform you that, Your application for SW Global LLC eWallet Secure Certificate is disapproved due to <b><?php echo $disapproveReason;?></b>.
            Please apply for a fresh eWallet account after correcting the reasons mentioned above.


    </div>

<?php  break;////////////Key status 3 /////////////
    case 0:

?>
<table width="100%"  cellpadding="0" cellspacing="0" border="0">
<tbody id="doc_div">
    <tr>
        <td class="ltgray" width="15%" valign="top">Upload Passport Size Self  Image &nbsp; <font color="red">*</font></td>

        <td width="41%">
            <input id="image" class="txt-input" type="file" onchange="uploadImageTemp()" name="image"/>
            <input id="chkImage" type="checkbox" name="chkImage" disabled = "1"/>
            <br/>
            (Image size should be maximum 400kb)
            <br/>
            <div id="error_img" class="red"/>
            <dl id='username_row'>
                <div class="dsTitle4Fields"></div>
                <div class="dsInfo4Fields" align="left"> <img id="file-info" width="100px;" height="120px;" style="display:none;"></img>
                    <?php echo $sf_params->get('file-info');?>
                </div>
            </dl>
        </td>
    </tr>
    <tr>
        <td class="ltgray" width="15%">
            Upload Document &nbsp;
            <font color="red">*</font>
            <br/>
            (Issued by any government body)
        </td>
        <td width="41%">
            <input id="doc1" type="file" onchange="uploadDocTemp(1)" name="doc1" class="txt-input"/>
            <input id="chkDoc1" type="checkbox" disabled="1" name="chkDoc1"/>
            <br/>
            (Total document size should be maximum 1mb)
            <br/>
            <div id="err_alldoc" class="red"/>
           
        </td>
    </tr>
 </tbody>
</table>
    <?php //echo checkbox_tag('chkimage','chkimage');?>
    <input type="hidden" name="byPass" id="byPass" value="1">

    <!--<dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields" align="left"> <img id="file-info" width="100px;" height="120px;" style="display:none;"></img>
            <?php //echo $sf_params->get('file-info');?>
        </div>
    </dl> -->




    <input type="hidden" id="tmpImgName" name="tmpImgName" value=''>
    <input type="hidden" id="tmpDocName" name="tmpDocName" value=''>
    <input type="hidden" id="totDocSize" name="totDocSize" value='0'>



    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <!--<div class="dsInfo4Fields" align="left">(Total document size should be maximum 200kb)<div class="cRed" id="err_alldoc"></div></div>-->
    </dl>
    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields" align="right"><a href="javascript:void(0);" id="addDoc" class="add_document" onClick="addMoreDoc();">add More Document</a></div>
    </dl>


    <?php echo tag('input',array('name'=>'totdoc', 'id'=>'totdoc', 'type'=>'hidden', 'value'=>'1' ))?>
    <?php echo tag('input',array('name'=>'totsize', 'id'=>'totsize','type'=>'hidden', 'value'=>'0' ))?>
<div class="divBlock" class="passPolicy" align="left">

        <h5  class="passPolicy" >
            <?php echo "&nbsp;&nbsp;".tag('input', array('type' => 'checkbox','name' => 'confirmUpload', 'id'=>'confirmUpload'))."  <font color='gray'>I accept and confirm that the uploaded image and documents are correct</font>";?>
        </h5>

    <div class="passPolicy" style="color:red;" id="err_confirmUpload"></div>
</div>
<br>
<div class="divBlock">
    <center>
         <?php echo tag('input', array('name' => 'apply', 'type'=>'submit', 'class'=>'certificatebutton','value'=>'Apply for Certificate'));?>
    </center>
</div>
<iframe id="uploadIframe" name="uploadIframe" height="10px;" width="10px;" style="display:none;"></iframe>



<?php  break;////////////Key status 0 /////////////
    }

?>



</div>
</div>
</form>
</div>

<script>
    function uploadDocTemp(docNum)
    {
        var docSize = document.getElementById('totsize').value;
        var docName = document.getElementById('tmpDocName').value;

        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadDoc');?>?docSize="+docSize+"&docNum="+docNum+"&docName="+docName;
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();

    }
    function uploadImageTemp()
    {
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadImage');?>";
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();
    }
    function addMoreDoc()
    {
        var totdoc = document.getElementById("totdoc").value;
        if(totdoc>=3){
            alert('Your can upload maximum three document.');
            return false;
        }

        if($("#totsize").val() >= 1024){
            alert('Your uploaded size(1mb including image) has been exceeded.');
            return false;
        }

        ++totdoc;
        var divid = "div_"+totdoc;

        var TrTag = document.createElement("tr");
        TrTag.id = divid;

        var tdTag1 = document.createElement("td");
        tdTag1.className = 'ltgray';
        tdTag1.width = "15%";
        tdTag1.innerHTML = 'Upload Document '+totdoc;

        var tdTag2 = document.createElement("td");
        tdTag2.width = "41%";
        tdTag2.innerHTML = '<input id="doc'+totdoc+'"  type="file" value="" class="txt-input" onchange=uploadDocTemp('+totdoc+') name="doc'+totdoc+'"/>\n\
                            <input id="chkDoc'+totdoc+'" type="checkbox" name="chkDoc'+totdoc+'" disabled=true"  />\n\
                <div id="err_alldoc'+totdoc+'" class="red"/>';
    document.getElementById("doc_div").appendChild(TrTag);
        document.getElementById(divid).appendChild(tdTag1);
        document.getElementById(divid).appendChild(tdTag2);
//        divTag.innerHTML = '\n\
//                <td width="15%" class="ltgray">\n\
//                <label for="upload">Upload Document '+totdoc+'</label>\n\
//                </td>\n\
//                <td width="41%">\n\
//                <input id="doc'+totdoc+'"  type="file" value="" onchange=uploadDocTemp('+totdoc+') name="doc'+totdoc+'"/>\n\
//                &nbsp;&nbsp;&nbsp;<input id="chkDoc'+totdoc+'" type="checkbox" value="true" name="chkDoc'+totdoc+'" disabled=true"  />\n\
//                <div id="errorDoc'+totdoc+'" class="cRed"/>\n\
//                </td>\n\
//               ';

        
        document.getElementById("totdoc").value = totdoc;
        if(3==totdoc){
            document.getElementById("addDoc").style.display='none';
        }

    }
    function validateSignForm()
    {
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/'.$sf_context->getActionName());?>";
        document.forms[0].target ='';
        err = 0;
        if(!$('#chkImage').is(':checked'))
        {
            $('#error_img').html("Please  Upload  Image and check the  box");
            err = err+1;
        }
        else
        {
            $('#error_img').html("");
        }

        if( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') )
        {
            $('#err_alldoc').html("Please  Upload Documents  and check the  box");
            err = err+1;
        }
        else
        {
            $('#err_alldoc').html("");
        }
        if( !$('#confirmUpload').is(':checked')  && !( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') ))
        {
            $('#err_confirmUpload').html("Please  confirm the uploaded image and documents ");
            err = err+1;
        }
        else
        {
            $('#err_confirmUpload').html("");
        }
            if(err == 0)
            {
                return true;
            }
            else
                return false;
    }
</script>