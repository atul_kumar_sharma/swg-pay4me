<?php use_helper('EPortal'); ?>
<?php include_partial('global/innerHeading',array('heading'=>'eWallet Secure Certificate'));?>

<div class="global_content4 clearfix">
<div class="brdBox">
 <div class="clearfix"/>
    <?php // echo ePortal_highlight('eWallet Secure Certificate'); ?>
<?php echo form_tag($sf_context->getModuleName().'/'.$sf_context->getActionName(),array('name'=>'signup','id'=>'signup','method'=>'post','enctype'=>'multipart/form-data', 'onSubmit'=>'return validateSignForm()')); ?>
 <div class="wrapForm2">
<div class="divBlock" class="passPolicy" align="left">&nbsp;</div>
<?php
    switch($kycStatus)
    {
      case 2:
        
?>

     <div class="descriptionArea">
        
         Thank you for applying a SW Global LLC eWallet Secure Certificate.<br><br>

         Now your account is queued for approval.<br>
         You will be notified when an action is recorded on your application.
        
    </div>

<?php  break; ////////////Key status 2 ////////////
      case 1:
      
?>

     <div class="descriptionArea">
        
          Congratulations! Your application for SW Global LLC eWallet Secure Certificate has been approved.<br>
          You can now use your eWallet Payment facility.

        
    </div>

<?php  break;////////////Key status 1 /////////////
case 3:

?>

     <div class="descriptionArea">
        
            We regret to inform you that, Your application for SW Global LLC eWallet Secure Certificate is disapproved due to <b><?php echo $disapproveReason;?></b>.
            Please apply for a fresh eWallet account after correcting the reasons mentioned above.

        
    </div>

<?php  break;////////////Key status 3 /////////////
    case 0:
      
?>
    <?php  echo formRowComplete('Upload Passport Size Self  Image<sup>*</sup>',tag('input', array('class'=>'FieldInput','name'=>'image', 'id'=>'image', 'type'=>'file', 'onchange'=>'uploadImageTemp()' )).'&nbsp;&nbsp;&nbsp;'.tag('input', array('type' => 'checkbox', 'name'=> 'chkImage','id'=>'chkImage', 'disabled'=>true)),'(Image size should be maximum 20kb)','image','error_img','username_row'); ?>
    <?php //echo checkbox_tag('chkimage','chkimage');?>
    <input type="hidden" name="byPass" id="byPass" value="1">

    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields" align="left"> <img id="file-info" width="100px;" height="120px;" style="display:none;"></img>
            <?php echo $sf_params->get('file-info');?>
        </div>
    </dl>




    <input type="hidden" id="tmpImgName" name="tmpImgName" value=''>
    <input type="hidden" id="tmpDocName" name="tmpDocName" value=''>
    <input type="hidden" id="totDocSize" name="totDocSize" value='0'>


    <div id="doc_div">
        <div id="div_1">
            <?php echo formRowComplete('Upload Document<sup>*</sup><br>(Issued by any government body)',tag('input'  , array('name'=>'doc1','type'=>'file',  'id'=>'doc1', 'onchange'=>'uploadDocTemp(1)' )).'&nbsp;&nbsp;&nbsp;'.tag('input', array('type' => 'checkbox','name'=> 'chkDoc1',  'id'=>'chkDoc1', 'disabled'=>true)),'','doc1','errorDoc1','username_row'); ?>
                    </div>
    </div>
    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields" align="left">(Total document size should be maximum 200kb)<div class="cRed" id="err_alldoc"></div></div>
    </dl>
    <dl id='username_row'>
        <div class="dsTitle4Fields"></div>
        <div class="dsInfo4Fields" align="right"><a href="javascript:void(0);" id="addDoc" onClick="addMoreDoc();">add More Document</a></div>
    </dl>


    <?php echo tag('input',array('name'=>'totdoc', 'id'=>'totdoc', 'type'=>'hidden', 'value'=>'1' ))?>
    <?php echo tag('input',array('name'=>'totsize', 'id'=>'totsize','type'=>'hidden', 'value'=>'0' ))?>
<div class="divBlock" class="passPolicy" align="left">
    
        <h5  class="passPolicy" >
            <?php echo "&nbsp;&nbsp;".tag('input', array('type' => 'checkbox','name' => 'confirmUpload', 'id'=>'confirmUpload'))."  <font color='gray'>I accept and confirm that the uploaded image and documents are correct</font>";?>
        </h5>

    <div class="passPolicy" style="color:red;" id="err_confirmUpload"></div>
</div>
<div class="divBlock">
    <center>
         <?php echo tag('input', array('name' => 'apply', 'type'=>'submit', 'class'=>'formSubmit','value'=>'Apply for Certificate'));?>
    </center>
</div>
<iframe id="uploadIframe" name="uploadIframe" height="10px;" width="10px;" style="display:none;"></iframe>



<?php  break;////////////Key status 0 /////////////
    }
   
?>




</div>
</div>
</form>
</div>

<script>
    function uploadDocTemp(docNum)
    {
        var docSize = document.getElementById('totsize').value;
        var docName = document.getElementById('tmpDocName').value;

        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadDoc');?>?docSize="+docSize+"&docNum="+docNum+"&docName="+docName;
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();

    }
    function uploadImageTemp()
    { 
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/uploadImage');?>";
        document.forms[0].target = "uploadIframe";
        document.forms[0].submit();
    }
    function addMoreDoc()
    {
        var totdoc = document.getElementById("totdoc").value;
        if(totdoc>=3){
            alert('Your can upload maximum three document.');
            return false;
        }

        if($("#totsize").val() >= 200){
            alert('Your uploaded size(200kb including image) has been exceeded.');
            return false;
        }

        ++totdoc;
        var divid = "div_"+totdoc;

        var divTag = document.createElement("div");
        divTag.id = divid;


        divTag.innerHTML = '<dl id="username_row">\n\
                <div class="dsTitle4Fields">\n\
                <label for="upload">Upload Document '+totdoc+'</label>\n\
                </div>\n\
                <div class="dsInfo4Fields">\n\
                <input id="doc'+totdoc+'"  type="file" value="" onchange=uploadDocTemp('+totdoc+') name="doc'+totdoc+'"/>\n\
                &nbsp;&nbsp;&nbsp;<input id="chkDoc'+totdoc+'" type="checkbox" value="true" name="chkDoc'+totdoc+'" disabled=true"  />\n\
                <div id="errorDoc'+totdoc+'" class="cRed"/>\n\
                </div>\n\
                </dl>';

        document.getElementById("doc_div").appendChild(divTag);
        document.getElementById("totdoc").value = totdoc;
        if(3==totdoc){
            document.getElementById("addDoc").style.display='none';
        }

    }
    function validateSignForm()
    {
        document.forms[0].action = "<?php echo url_for($sf_context->getModuleName().'/'.$sf_context->getActionName());?>";
        document.forms[0].target ='';
        err = 0;
        if(!$('#chkImage').is(':checked'))
        {
            $('#error_img').html("Please  Upload  Image and check the  box");
            err = err+1;
        }
        else
        {
            $('#error_img').html("");
        }

        if( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') )
        {
            $('#err_alldoc').html("Please  Upload Documents  and check the  box");
            err = err+1;
        }
        else
        {
            $('#err_alldoc').html("");
        }
        if( !$('#confirmUpload').is(':checked')  && !( !$('#chkDoc1').is(':checked') && !$('#chkDoc2').is(':checked') && !$('#chkDoc3').is(':checked') ))
        {
            $('#err_confirmUpload').html("Please  confirm the uploaded image and documents ");
            err = err+1;
        }
        else
        {
            $('#err_confirmUpload').html("");
        }
            if(err == 0)
            {             
                return true;
            }
            else
                return false;
    }
</script>