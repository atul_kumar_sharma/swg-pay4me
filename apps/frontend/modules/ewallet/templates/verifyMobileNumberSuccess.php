<?php
include_partial('global/innerHeading',array('heading'=>'Verify Mobile Number'));

use_helper('Form');

$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<div class="wrap">


<div class="global_content4 clearfix">
   <h2 class="successBox">The PIN number will be required to approve your account.</h2>
    <div class="brdBox">
        <div class="clearfix"/>
        <?php echo form_tag('ewallet/verifyMobileNumber',array('name'=>'frm_mobile','class'=>'', 'method'=>'post','id'=>'frm_mobile','onSubmit' => 'return changeMobileNumber();')) ?>
        <div class="wrapForm2">

            <table width="50%">

                <?php //echo $form;?>

                <tr>
                <td colspan="2"><b>Please verify your mobile number to get KYC Pin or change if required.</b></td>
                </tr>

                <tr>
                 <td><?php echo $form['mobile_number']->renderLabel(); ?> </td>
                 <td><?php echo $form['mobile_number']->render(); ?><br>
                 <div id="err_mobile" class="red"><?php echo $form['mobile_number']->renderError() ?></div>
                 </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                    <td><div class="lblButton">
                    <?php  //if ($form->isCSRFProtected()) : ?>
            <?php //echo $form['_csrf_token']->render(); ?>
            <?php //endif; ?>
                            <?php   echo submit_tag('Proceed',array('class' => 'button')); ?>
                        </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>

            </table>
    </div>

        </form>
        </div>
    </div>
</div>

</div>


<script>
    function changeMobileNumber(){
   var mobile = $("#mobile_mobile_number").val();
   var err = 0;

   if(jQuery.trim(mobile) == "")
        {
            if(validatePhone(mobile))
            {
                $('#err_mobile').html("Please enter Mobile number");
                err = err+1;
            }
            else
            {
                $('#err_mobile').html("");
            }
        }

        if(mobile != "")
        {
            if(validatePhone(mobile))
            {
                $('#err_mobile').html("Please enter Valid Mobile number <br/>Mobile Number should be between 10-14 digits, eg: +1234567891");
                err = err+1;
            }
            else
            {
                $('#err_mobile').html("");
            }
        }


  if(err == 0){
     return true;
   }else{
       return false;
   }

}

</script>
