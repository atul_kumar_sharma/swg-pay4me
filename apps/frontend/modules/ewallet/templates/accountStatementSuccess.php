<?php use_helper('Pagination');

?>
<div class="clearfix">
    <div id="nxtPage">
       <table class="innerTable_content" >
                  <tr>
                     
                   <td align="right" class="blbar">
                      Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results
                    </td>
                  </tr>
       </table>
        <div class="clearfix"></div>
    </div>
      <table class="innerTable_content" >
      <tr>
        <td align="center"> <b>Account No:</b> <?php echo $acctDetails->getAccountNumber(); ?>
          <?php echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Account Balance:</b> ".format_amount($acctDetails->getClearBalance(),1,1);?>
          <?php //echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Unclear Amount:</b> ".format_amount($acctDetails->getUnclearAmount(),1,1);?>
        </td>
      </tr>

  <?php if($pager->getNbResults()>0){?><?php

  foreach($statementObj->getFirst()->getEpMasterLedger() as $res) {
    //    echo $res->getAmount();
    echo "<tr><td align='center'>";
    if($res->getEntryType() == "credit"){echo " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Credits:</b> ".format_amount($res->getAmount(),1,1);}
    else echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> Total Debits: </b>".format_amount($res->getAmount(),1,1);}
    echo "</td></tr>";  }
?>
</table>
<table class="innerTable_content" >
  
    <tr>
      <td width = "2%"><b>S.No.</b></td>
      <td align="center"><b>Narration</b></td>
      <td align="center"><b>Transaction Date</b></td>
      <td align="center"><b>Amount</b></td>
      <td align="center"><b>Transaction Type</b></td>
    </tr>
 
    <?php
    if(($pager->getNbResults())>0) {
      $limit = sfConfig::get('app_records_per_page');
      $i = max(($page-1),0)*$limit ;
      foreach ($pager->getResults() as $result):
        $i++;
        $transaction_date = explode(" ",$result->getTransactionDate());
        $amount = format_amount($result->getAmount(),1,1);
        ?>
    <tr>
      <td align="center"><?php echo $i ?></td>
      <td align="center"><?php echo html_entity_decode($result->getDescription());?></td>
      <td align="center"><?php echo $result->getTransactionDate();?></td>
      <td align="right"><?php echo $amount; ?></td>
      <td align="center"><?php echo ucwords($result->getEntryType()); ?></td>
    </tr>
      <?php endforeach; ?>

    <tr>
        <td colspan="5" class="blbar" height="25" align="right">
            <div class="paging pagingFoot"><?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?startdate='.$from_date.'&enddate='.$to_date), 'result_div')  //pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?'.$url.'designation_id='.$sf_request->getParameter('designation_id').'&department_id='.$sf_request->getParameter('department_id'))) ?>
            </div>
        </td>
    </tr>

    <?php }
    else { ?>
    <tr><td  align='center' colspan="5" ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>

  

</table>


        
    
    <div>&nbsp;</div>
    <div id="detail_div"  class="clearfix"></div>
    <div style="display:none;padding-top:20px;" align="center" id="loader">
        <?php echo image_tag('/images/ajax-loader.gif',array()); ?></div>
</div>

<script>
    function validate_form(){
        $('#detail_div').html('<?php echo image_tag('../images/ajax-loader.gif'); ?>');
        var err = 0;
        if(err == 0){          
            $.post('swGlobalReportdetail',$("#detail").serialize(), function(data){
                if(data == "logout"){
                    location.reload();
                }else{                   
                    document.getElementById('show').style.display = "none";
                    document.getElementById('hide').style.display = "block";
                    $("#detail_div").html(data);
                }
            });
        }
    }
    function hide_form(){
        document.getElementById('detail_div').innerHTML = "";
        document.getElementById('show').style.display = "block";
        document.getElementById('hide').style.display = "none";
    }
</script>