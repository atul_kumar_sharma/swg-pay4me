<?php  echo ePortal_pagehead(" ",array('class'=>'_form')); ?>
<?php use_helper('Pagination');  ?>
<div class="wrapTable">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <tr>
            <td class="blbar" colspan="7" align="right">
                <div style="float:left">New eWallet User Details</div>
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
        </tr>
    </table>
    <br class="pixbr" />
</div>


<div id="search_results">
    <div class="wrapTable">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="dataTable" >
        <?php
        $from_date = $sf_request->getParameter('from_date') == '' ? "BLANK" : $sf_request->getParameter('from_date');
        $to_date = $sf_request->getParameter('to_date') == '' ? "BLANK" : $sf_request->getParameter('to_date');

        if(($pager->getNbResults())>0) {
            $limit = sfConfig::get('app_records_per_page');
            $page = $sf_context->getRequest()->getParameter('page',0);
            $i = max(($page-1),0)*$limit ;
            ?>
        <thead>
            <tr>
                <td width = "2%"><b>S.No.</b>
                    <input type="hidden" name="fromDate" id="fromDate" value="<?php echo $from_date;?>">
                    <input type="hidden" name="toDate"   id="toDate" value="<?php echo $to_date;?>">
                </td>
                <td><b>Name</b></td>
                <td><b>Image</b></td>
                <td><b>Documents</b></td>
                <td><b>Last Updated Date</b></td>
                <td><b>Actions</b></td>
            </tr>
        </thead>
        <?php

        foreach ($pager->getResults() as $result):
        $i++;
        $userFolder = sfConfig::get('sf_upload_dir')."/".$result->getUid();
        $handle = opendir($userFolder);
                                                /* This is the correct way to loop over the directory. */
        $user_file = array();
        while (false !== ($file = readdir($handle))) {
            $user_file[]= $file;
        }
        closedir($handle);
       
        ?>
        <tbody>
            <tr class="alternateBgColour">
                <td align="center"><?php echo $i ?></td>
                <td align="center"><?php echo $result->getFirstName();   ?>&nbsp;<?php echo $result->getLastName();   ?></td>
                <td align="left">
                    <?php
                    //       echo "<pre>";
                    //       print_r($_SERVER);
                    $img = $userFolder."/img.jpeg";
                    if (file_exists($img))
                    {
                        echo image_tag('/uploads/'.$result->getUid().'/img.jpeg',array('size' => '100x100'));
                    }
                    else
                    echo "----";
                    ?>

                </td>
                <td align="left">
                    <?php
                    $fileCount=1;
                    foreach($user_file as $file)
                    {
                        $fullpath = $userFolder."/".$file;
                        if("img.jpeg" != $file && $file != "." && $file != "..")
                        {
                            echo $fileCount.". ".$file."     "."<a class='downloadInfo' href='".url_for('ewallet/downloadfile?fileName='.$file.'&folder='.$result->getUid())."' title='download'  alt='download'  ></a>";
                            echo "<br>";
                            ++$fileCount;
                        }
                    }
                    if(1==$fileCount)
                    echo "----";
                    ?>
                </td>
                <td><?php echo $result->getUpdatedAt();   ?></td>
                <td align="center">
                    <a class="approveInfo" href="#" onclick="changeStatus(<?php echo $result->getUid();?>,'approve');" title="Approve" alt="Approve"></a>
                    &nbsp;
                    <!--a class="delInfo" href="#" onclick="changeStatus(<?//php echo $result->getUid();?>,'denial');" title="disapprove" alt="delete"></a-->
                </td>

            </tr>

                <?php endforeach;    ?>

        </tbody>



            <?php if(($pager->getNbResults())>$limit) {
                ?>
        <tr>
        <td class="blbar" colspan="6" height="25px" align="right">
        <?php  echo ajax_pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName().'?from_date='.$from_date.'&to_date='.$to_date), 'result_div') ?>

    </div>
    </td>
    </tr>
    <?php
}
?>


<?php
}else {
?>
    <tr><td colspan="10"  align='center' class='error' >No Record Found</td></tr>
    <?php } ?>
    </table>
</div>
</div>
<script>
    function changeStatus(userid,state){

        var denial;
        if("denial" == state)
        {
            jPrompt('Please enter reason of disapproval (Mandatory):', '', 'Disapprove Reason', function(denial) {
                if( !denial ){
                    jAlert('User can not be disapproved without the reason!');
                    return;
                }
                else {  sendReq(userid,state,denial); }
            }

        );

        }
        else{
            sendReq(userid,state,'');
        }

    }
    function sendReq(userid,state,denial){
        var activate_url = "<?php echo url_for('ewallet/setUserStatus');?>";
        $.post(activate_url,{user_id:userid,status:state,reason:denial,from_date:$("#fromDate").val(),to_date:$("#toDate").val()}, function(data){
            if(data){
                newewalletAccounts();
            }});

    }
</script>