<?php include_partial('global/innerHeading',array('heading'=>'Order Details'));?>

<div class="global_content4 clearfix">
<?php if($result == 'success'){?>
<div class="thanksArea">
<h2 class="successBox">Thanks , your payment is successful.</h2>
			<div class="clear"/>
           <div class="msgArea">
                <p>Your order number : <?php echo $vumber?></p>
           </div>
          </div>
<?php }elseif($result == 'invalid'){?>
<div class="thanksArea">
			<h2 class="warningBox">The payment has already been made against this order »</h2>
      </div>
<?php }else {?>
<div class="thanksArea">
	<h2 class="errorBox">There was an error in proccessing your order. Please try after some time. »</h2>
    </div>
<?php }?>
</div>