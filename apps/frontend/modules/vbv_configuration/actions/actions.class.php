<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author akumar1
 */


class vbv_configurationActions extends sfActions {
  public static $version = 'V1';

//  public function preExecute()
//  {
//    $this->getUser()->setFlash('notice', 'Tampering URL is not allowed!!!');
//    $this->redirect('cart/list');
//
//  }

  public function executeRecharge(sfWebRequest $request) {
    $requestId = base64_decode($request->getParameter('rechargeRequestId'));
    $request_details = Doctrine::getTable('RechargeOrder')->find($requestId);
    $this->payStatus = $request_details->getPaymentStatus();
    if(0==$this->payStatus)
    $this->getUser()->setFlash('notice', 'Recharge has been made.',false);
    if($request_details){
      $this->request_details = $request_details;
      $this->requestId = base64_encode($requestId);
    }else{
      throw New Exception('No Order details found for this request!');
    }
  }
  public function executePayment(sfWebRequest $request) {
    //msg if applicant have some registered cards
    $userId = $this->getUser()->getGuardUser()->getId();
    $ifVaultActive = Doctrine::getTable('GlobalSettings')->findByVarKey('graypay-vault');
    if(!$request->hasParameter('valid'))
    {
      if($ifVaultActive->getFirst()->getVarValue() == "inactive"){
        $applicantVaultArray = ApplicantVaultTable::getInstance()->getCardsAmexCard($userId);
      }else{
        $applicantVaultArray = ApplicantVaultTable::getInstance()->getCardsFromVault($userId);
      }
      if(count($applicantVaultArray) > 0){
        $this->getUser()->setFlash('notice', 'Due to anti fraud reasons, currently approved cards are not available for use. We will be back with the service soon.',false);
      }
    }
    //echo '<pre>';print_r($applicantVaultArray);die;
    $requestId = $request->getParameter('requestId');
    $failure = $request->getParameter('failure');
    $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
    $this->payStatus = $request_details->getPaymentStatus();
    if(0==$this->payStatus)
    $this->getUser()->setFlash('notice', 'Payment has already been made for this Item',false);
    if($failure == 1)
    $this->getUser()->setFlash('notice', 'Payment page is expired. Please try again',false);
    if($request_details){
      $this->request_details = $request_details;
      $this->requestId = $requestId;

      $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
      if (count($appDetails) == 1) {
        $this->application_type = strtolower($appDetails[0]['app_type']);
      }else{
        $this->application_type = '';
      }

    }else{
      throw New Exception('No Order details found for this request!');
    }
  }
  public function executeVbvRecharge(sfWebRequest $request) {

    $rechargeRequestId = base64_decode($request->getParameter('rechargeRequestId'));

    $rechargedetail =  Doctrine::getTable('RechargeOrder')->find($rechargeRequestId);
    $gatewayId = $rechargedetail->getGatewayId();

    $this->payStatus = $rechargedetail->getPaymentStatus();
    if(0!=$this->payStatus )
    {
      //ipay4me recharge order insert
      $orderObj = $this->createRechargeOrder($gatewayId, $rechargeRequestId);
      $orderNumber = $orderObj->getOrderNumber();
      $epVbvManagerObj = new EpVbvManager();
      $epVbvManagerObj->amount = $rechargedetail->getAmount();
      $epVbvManagerObj->orderId = $orderNumber;//order number
      $epVbvManagerObj->currency = Settings::getCurrency();
      $host = $request->getUriPrefix() ;
      $url_root = $request->getPathInfoPrefix();
      $epVbvManagerObj->returnUrlApprove = $host.$url_root."/".Settings::getRechargeApproveurl()."/z/".session_id() ;
      $epVbvManagerObj->returnUrlDecline = $host.$url_root."/".Settings::getRechargeDeclineurl()."/z/".session_id() ;
      $epVbvManagerObj->postUrl = Settings::getPostUrl();
      $epVbvManagerObj->userId = $this->getUser()->getGuardUser()->getId();
      $epVbvManagerObj->merid = Settings::getMerid();
      $epVbvManagerObj->acqBin = Settings::getAcqbin();
      $this->retObj = $epVbvManagerObj->setRequest();
    }
    $this->taskType = 'recharge';

    if(0==$this->payStatus )
    $this->getUser()->setFlash('notice', 'Recharge has been done.',false);
    else
    $this->getUser()->setFlash('notice', NULL);

    $this->setTemplate('vbvForm');
  }


  public function executeVbvForm(sfWebRequest $request) {
    //$time_now=mktime(date('h'),date('i'),date('s'));
    $timeAfter=mktime(date('h'),date('i'),date('s'));
    $currentTime = date('h:i:s',$timeAfter);
    $currentTime = explode(':',$currentTime);
    $sec = sfConfig::get('app_time_out_limit')-7;
    $this->microSec = $sec*1000;
    $requestId = $request->getParameter('requestId');
    $this->requestId = $requestId;
    $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
    $this->payStatus = $request_details->getPaymentStatus();
    $orderRequestObj = OrderRequestDetailsTable::getInstance();

    if($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {

      $this->getUser()->setFlash('notice', 'Payment has already been made for this Item',false);
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'payment','requestId'=>''.$requestId,"valid"=>"yes"))."'</script>");
    }
    /*start payment status from backend*/
    $userId = $this->getUser()->getGuardUser()->getId();
    $vbVManager = new VbVManager();
    $getStatusOfrequestId = $vbVManager->StatusOfrequestId($this->requestId, $userId);
    if($getStatusOfrequestId)
    {
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'paymentGateway',
            'action' => 'thankYou','valid'=>$getStatusOfrequestId,'response_code'=>'001','response_text'=>'APPROVED'))."'</script>");
    }
    /*end payment status from backend*/

    $arrRet = $this->saveIp4mOrder($requestId);

    $epVbvManagerObj = new EpVbvManager();
    $epVbvManagerObj->amount = $arrRet['amount'];
    $epVbvManagerObj->orderId = $arrRet['orderNumber'];//order number
    $epVbvManagerObj->currency = Settings::getCurrency();
    $host = $request->getUriPrefix() ;
    $url_root = $request->getPathInfoPrefix();
    $epVbvManagerObj->returnUrlApprove = $host.$url_root."/".Settings::getPaymentApproveurl()."/z/".session_id() ;
    $epVbvManagerObj->returnUrlDecline = $host.$url_root."/".Settings::getPaymentDeclineurl()."/z/".session_id() ;
    $epVbvManagerObj->postUrl = Settings::getPostUrl();
    $epVbvManagerObj->userId = $userId;
    $epVbvManagerObj->merid = Settings::getMerid();
    $epVbvManagerObj->acqBin = Settings::getAcqbin();

    $this->retObj = $epVbvManagerObj->setRequest();
    $this->setTemplate('vbvForm');
  }

  public function executeVbvFormRedirect(sfWebRequest $request) {

    $order_id = base64_decode($request->getParameter('id'));
    $this->request_details = Doctrine::getTable('VbvOrder')->findByOrderId($order_id);
    $epVbvManagerObj = new EpVbvManager();
    $this->retObj = $epVbvManagerObj->selectAllFromRequest($order_id);
    $this->setTemplate('vbvFormRedirect');
  }

  public function executeVbvRedirect(sfWebRequest $request) {
    $this->pageTitle = 'Visa Payment';
    return $this->renderText('Please wait...');
  }

  protected function saveVbvOrder($order_id, $tarnsaNo, $type) {
    $success = Doctrine::getTable('VbvOrder')->saveOrder($order_id, $tarnsaNo, $type);
  }


  public function executeVbvRechargeApprove(sfWebRequest $request) { //Approve

    $response = $this->saveResponse($request);

    $order_number = $response['orderid'];
    $orderObj = Doctrine::getTable('ipay4meRechargeOrder')->findByOrderNumber($order_number)->getFirst();
    $orderId = $orderObj->getId();


    //    $order_number = $orderObj->getOrderNumber();
    $requestId = $orderObj->getRechargeOrderId();
    $payManagerObj = new PaymentManager();
    $paymentResponseArray = array();
    $paymentResponseArray['response_code'] =$response['responsecode'] ;
    $paymentResponseArray['response_text'] = $response['responsedescription'];
    $paymentResponseArray['card_Num'] = $response['pan'];
    //if($response['responsecode']=='001')
    $paymentResponseArray['status'] = 1;
    // else
    // $paymentResponseArray['status'] = 0;

    $orderObj = $payManagerObj->updateRechargeOrderRequest($orderId,$paymentResponseArray);
    $rechargeObj = $payManagerObj->rechargeSuccess(base64_encode($requestId), $order_number);

    ////////////// Accounting for recharge //////////////

    $rechargeOrderObj = Doctrine::getTable('RechargeOrder')->find($requestId);
    $user_id = $rechargeOrderObj->getUserId();
    $gatewayid = $rechargeOrderObj->getGatewayId();
    $total_amount = $rechargeOrderObj->getAmount();

    ////////////////////////////////////////

    return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'paymentGateway',
            'action' => 'thankforRecharge','validate'=>''.base64_encode($requestId)))."'</script>");
  }

  public function executeVbvRechargeDecline(sfWebRequest $request) {
    $response = $this->saveResponse($request);
    $order_id = $response['orderid'];
    $orderObj = Doctrine::getTable('ipay4meRechargeOrder')->findByOrderNumber($order_id)->getFirst();
    $rechargeRequestId = $orderObj->getRechargeOrderId();
    $orderId = $orderObj->getId();


    $payManagerObj = new PaymentManager();
    $paymentResponseArray = array();
    $paymentResponseArray['response_code'] =$response['responsecode'] ;
    $paymentResponseArray['response_text'] = $response['responsedescription'];
    $paymentResponseArray['card_Num'] = $response['pan'];
    //if($response['responsecode']=='001')
    //$paymentResponseArray['status'] = 1;
    // else
    $paymentResponseArray['status'] = 0;

    $orderObj = $payManagerObj->updateRechargeOrderRequest($orderId,$paymentResponseArray);
    $errorMsgObj = new ErrorMsg();
    if(array_key_exists('pan',$response)){
      $errorMsg = $errorMsgObj->displayErrorMessage("E050", $rechargeRequestId);
      $this->getUser()->setFlash('notice', $errorMsg, true) ;
      //          $this->getUser()->setFlash('notice', 'Recharge Unsuccessful,Please try again', true) ;
    }
    else{
      $errorMsg = $errorMsgObj->displayErrorMessage("E051", $rechargeRequestId);
      $this->getUser()->setFlash('notice', $errorMsg, true) ;
      //                  $this->getUser()->setFlash('notice', 'Invalid payment.', true) ;
    }
    return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'recharge','rechargeStatus'=>'decline','rechargeRequestId'=>base64_encode($rechargeRequestId)))."'</script>");


  }
  public function executeVbvPaymentApprove(sfWebRequest $request) { //Approve

    $response = $this->saveResponse($request);

    $order_number = $response['orderid'];
    $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($order_number)->getFirst();
    $orderId = $orderObj->getId();
    $gatewayId = 4;// $orderObj->getGatewayId();


    //    $order_number = $orderObj->getOrderNumber();
    $requestId = $orderObj->getOrderRequestDetailId();
    $vbVManager = new VbVManager();
    $isValidPayment = $vbVManager->paymentVerify($order_number);
    if($isValidPayment)
    {
      $payManagerObj = new PaymentManager();
      $paymentResponseArray = array();
      $paymentResponseArray['response_code'] =$response['responsecode'] ;
      $paymentResponseArray['response_text'] = $response['responsedescription'];
      $paymentResponseArray['card_Num'] = $response['pan'];
      //if($response['responsecode']=='001')
      $paymentResponseArray['status'] = 1;
      // else
      // $paymentResponseArray['status'] = 0;

      $orderObj = $payManagerObj->updatePaymentRequest($orderId,$paymentResponseArray);
      $updateOrder = $payManagerObj->updateOrderRequest($orderId,$paymentResponseArray);


      if($updateOrder->getPaymentStatus() == 0)
      {
        $updateOrder = $payManagerObj->updateSplitAmount($requestId,$gatewayId);
        $result = $payManagerObj->paymentSuccess($order_number);
      }
      else
      {
        $result = $payManagerObj->paymentFailure($order_number);
      }

      ////////////////////////////////////////

      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'paymentGateway',
            'action' => 'thankYou','valid'=>$order_number,'response_code'=>$response['responsecode'],'response_text'=>$response['responsedescription']))."'</script>");
    }else{
      $errorMsgObj = new ErrorMsg();
      $errorMsg = $errorMsgObj->displayErrorMessage("E057", $requestId);
      $this->getUser()->setFlash('notice', $errorMsg, true) ;
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'payment','requestId'=>''.$requestId,"valid"=>"yes"))."'</script>");
    }
  }

  public function executeVbvPaymentDecline(sfWebRequest $request) {

    $response = $this->saveResponse($request);

    $order_id = $response['orderid'];
    $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($order_id)->getFirst();
    $orderId = $orderObj->getId();
    $requestId = $orderObj->getOrderRequestDetailId();

    $vbVManager = new VbVManager();
    $isValidPayment = $vbVManager->paymentVerify($order_id);

    $errorMsgObj = new ErrorMsg();
    if(array_key_exists('pan',$response)){
      $payManagerObj = new PaymentManager();
      $paymentResponseArray = array();
      $paymentResponseArray['response_code'] =$response['responsecode'] ;
      $paymentResponseArray['response_text'] = $response['responsedescription'];
      $paymentResponseArray['card_Num'] = $response['pan'];
      //if($response['responsecode']=='001')
      //$paymentResponseArray['status'] = 1;
      // else
      $paymentResponseArray['status'] = 0;

      $orderObj = $payManagerObj->updatePaymentRequest($orderId,$paymentResponseArray);

      if(array_key_exists('pan',$response)){
        $errorMsg = $errorMsgObj->displayErrorMessage("E052", $requestId);
        $this->getUser()->setFlash('notice', $errorMsg, true) ;
        //                $this->getUser()->setFlash('notice', 'Payment Unsuccessful, Please try again', true) ;
      }
      else{
        $errorMsg = $errorMsgObj->displayErrorMessage("E053", $requestId);
        $this->getUser()->setFlash('notice', $errorMsg, true) ;
        //                $this->getUser()->setFlash('notice', 'Invalid payment.', true) ;

      }
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'payment','requestId'=>''.$requestId,"valid"=>"yes"))."'</script>");
    }else{
      //             $this->getUser()->setFlash('notice', 'Invalid payment.', true) ;
      $errorMsg = $errorMsgObj->displayErrorMessage("E054", $requestId);
      $this->getUser()->setFlash('notice', $errorMsg, true) ;
      return $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'vbv_configuration',
            'action' => 'payment','requestId'=>''.$requestId,"valid"=>"yes"))."'</script>");
    }

  }

  protected function saveResponse($request) {
    $xmlmsg = strtolower($request->getParameter("xmlmsg"));
    $xdoc = new DOMDocument;
    $isloaded = $xdoc->loadXML($xmlmsg);
    $xdoc->saveXML();

    $setResponse = array();

    $p = xml_parser_create();
    xml_parser_set_option($p, XML_OPTION_CASE_FOLDING, 0);
    xml_parse_into_struct($p, $xmlmsg, $vals, $index);
    xml_parser_free($p);

    foreach($vals as $k=>$v) {
      $key = $v['tag'];

      if(array_key_exists('value',$v)) {
        $value = $v['value'];
      }
      else {
        $value="";
      }
      $setResponse[$key] = $value;
    }

    $setResponse['msgdate'] = $xdoc->getElementsByTagName('message')->item(0)->getAttribute('date');

    $type = 'response';
    $this->createLog($xmlmsg, $type, $setResponse['orderid']);

    $epVbvManager = new EpVbvManager();
    $retObj = $epVbvManager->setResponse($setResponse, $xmlmsg);

    return $setResponse;
  }

  protected function getOrderDetailId($order_id) {
    $orderArray = Doctrine::getTable('VbvOrder')->getOrderDetailId($order_id);
    return $orderArray;
  }
  protected function createLog($xmldata, $type, $orderId) {
    $nameFormate = $type.'_'.$orderId;
    $vbvLog = new Pay4meIntLog();
    $vbvLog->createLogData($xmldata,$nameFormate, 'vbvLog');
  }

  public function executeUpdatePayment(sfWebRequest $request)
  {
    $orderDetailId = base64_decode($request->getParameter('orderDetailId'));
    $userId = $this->getUser()->getGuardUser()->getId();
    $vbVManager = new VbVManager();
    $updatePayment = $vbVManager->updatePayment($orderDetailId, $userId);

    $this->forward('vbv_configuration', 'thankYou');
  }
  public function executeThankYou(sfWebRequest $request)
  {
    $this->result = 'success';
    $this->vumber = $request->getParameter('orderNumber');
  }
  public function createRechargeOrder($gateway_id,$rechargeRequestId) {
    $orderManagerObj = orderServiceFactory::getService(self::$version) ;
    $orderNumber = $orderManagerObj->getOrderNumber('recharge');

    $orderObj = new ipay4meRechargeOrder();
    $orderObj->setOrderNumber($orderNumber);
    $orderObj->setGatewayId($gateway_id);
    $orderObj->setRechargeOrderId($rechargeRequestId);
    $orderObj->save();
    return $orderObj;
  }

  function saveIp4mOrder($requestId)
  {
    $retArr = array();
    $activePaymentGateway = "vbv" ; // sfConfig::get('app_active_paymentgateway');
    //get gatway id
    $arrGateway = Doctrine::getTable('gateway')->findByName($activePaymentGateway);
    $intGatewayId = $arrGateway->getFirst()->getId();
    $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
    $amount = $request_details->getAmount();
    //order no
    $orderManagerObj = orderServiceFactory::getService(self::$version);
    $orderNumber = $orderManagerObj->getOrderNumber();
    $saveOrderDetail = new ipay4meOrder();
    $saveOrderDetail->setOrderNumber($orderNumber);
    $saveOrderDetail->setGatewayId($intGatewayId);
    $saveOrderDetail->setOrderRequestDetailId($requestId);
    $saveOrderDetail->save();
    $retArr['amount'] = $amount;
    $retArr['orderNumber'] = $orderNumber;
    return $retArr;
  }

  public function executeLastTransactionStatus(sfWebRequest $request)
  {
    $userId = $this->getUser()->getGuardUser()->getId();
    $lastOrder = $orderObj = Doctrine::getTable('ipay4meOrder')->findByUserid($userId);
    $orderReqDetailId = $lastOrder[0]['order_request_detail_id'];
    $order_number = $lastOrder[0]['order_number'];
    $amount = $lastOrder[0]['amount'];
    $vbVManager = new VbVManager();
    $status = $vbVManager->processPayment($order_number, $orderReqDetailId, $userId, $amount);
    if($status)
    {
        $this->redirect("paymentGateway/thankYou?valid=".Settings::encryptInput($order_number)."&response_code=001&response_text=APPROVED");
      $this->renderText("<script>window.parent.location = '".$this->generateUrl('default', array('module' => 'paymentGateway',
        'action' => 'thankYou','valid'=>Settings::encryptInput($order_number),'response_code'=>$response['responsecode'],'response_text'=>$response['responsedescription']))."'</script>");
    }else{
      $errorMsgObj = new ErrorMsg();
      $errorMsg = $errorMsgObj->displayErrorMessage("E057", $orderReqDetailId);
      $this->getUser()->setFlash('notice', $errorMsg, true) ;
      $this->redirect("vbv_configuration/payment?requestId=$orderReqDetailId&valid=yes");
    }
  }
}
?>
