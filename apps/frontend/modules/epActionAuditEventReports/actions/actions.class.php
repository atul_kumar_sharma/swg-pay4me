<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class epActionAuditEventReportsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->stDate = '';
        $this->enDate = '';
        $this->form = new EpActionAuditForm();
    }

    public function executeDisplayReport(sfWebRequest $request) {
        $this->form = new EpActionAuditForm();
        $this->form->bind($request->getParameter('audit'));

        if (($request->isMethod('post') && $this->form->isValid()) || ($request->isMethod('get'))) {

            if ($request->isMethod('post')) {
                $parameter = $request->getPostParameters();
                $parameter = $parameter['audit'];
            } else {
                $parameter = $request->getGetParameters();
            }

            $this->startDate = $parameter['startDate'];
            $this->endDate = $parameter['endDate'];

            if (isset($parameter['category'])) {
                $this->category = $parameter['category'];
            } else {
                $this->category = '';
            }
            if (isset($parameter['subCategory'])) {
                $this->subcategory = $parameter['subCategory'];
            } else {
                $this->subcategory = '';
            }

            //get the user credendtials
            $user = $this->getUser();
            $groupDetails = $user->getGroups()->toArray();

            $userDetails['id'] = $user->getGuardUser()->getId();
            $userDetails['username'] = $user->getGuardUser()->getUsername();
            $userDetails['groupId'] = $groupDetails[0]['id'];
            $userDetails['groupName'] = $groupDetails[0]['name'];

            if (!isset($this->endDate) || $this->endDate == '') {
                $this->endDate = $this->startDate;
            }

            //convert the date into the proper format
            $this->startDate = date('Y-m-d', strtotime("$this->startDate"));
            $this->startDate = $this->startDate . " 00:00:00";

            $this->endDate = date('Y-m-d', strtotime("$this->endDate"));
            $this->endDate = $this->endDate . " 23:59:59";

            $classObj = new auditTrailManager();
            $dataResult = $classObj->getDataForReport($this->startDate, $this->endDate, $userDetails, $this->category, $this->subcategory);

            //this below mentioned code is for executing the sql recd & get the data in paging order
            $this->page = 1;

            if ($request->hasParameter('page')) {
                $this->page = $request->getParameter('page');
            }

            $this->pager = new sfDoctrinePager('EpActionAuditEvent', sfConfig::get('app_records_per_page'));
            $this->pager->setQuery($dataResult);
            $this->pager->setPage($this->getRequestParameter('page', $this->page));
            $this->pager->init();
        } else {
            $this->setTemplate('index');
        }
    }

    public function executeGetSubCategory(sfWebRequest $request) {
        //get the category name
        $catName = $request->getParameter('category');
        $subCatList = EpAuditEvent::getAllSubcategory($catName);

        $str = '<option value="">-- All SubCategories --</option>';

        foreach ($subCatList as $value) {
            $str .= '<option value="' . $value . '">' . $value . '</option>';
        }
        return $this->renderText($str);
    }
}
?>