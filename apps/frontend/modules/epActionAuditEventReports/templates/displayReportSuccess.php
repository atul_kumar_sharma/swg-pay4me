<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Audit Trail'));?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<div id="nxtPage" class="overflow_auto">
<table width="100%">
  <tr>
    <td class="blbar" colspan="8" align="right">
    <div style="float:left">Audit Trail Details </div>
    <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
  </tr>
  <tr>
      <td width="50px"><span class="txtBold">S. No.</span></td>
      <td width="60px"><span class="txtBold">Username</span></td>
      <td width="50px"><span class="txtBold">Name</span></td>
      <td width="50px"><span class="txtBold">Ip address</span></td>
      <td width="50px"><span class="txtBold">Category</span></td>
      <td width="50px"><span class="txtBold">Subcategory</span></td>
      <td width="80px"><span class="txtBold">Description</span></td>
      <td width="50px"><span class="txtBold">Created at</span></td>
  </tr>
    <?php
      if(($pager->getNbResults())>0) {
          $limit = sfConfig::get('app_records_per_page');
          $page = $sf_context->getRequest()->getParameter('page',0);
          $i = max(($page-1),0)*$limit ;
    ?>



    <?php
      foreach ($pager->getResults() as $result)://echo "<pre>";print_r($result);die;
        $i++;
    ?>
    <tr>
      <td ><?php echo $i;?></td>      
      <td ><?php echo $result->getUsername();?></td>
      <td >
      <?php
     
      if($result->getuserId() && count($result->getsfGuardUser()->getUserDetail()) > 0){
       echo $result->getsfGuardUser()->getUserDetail()->getFirstName(); ?> &nbsp;<?php echo $result->getsfGuardUser()->getUserDetail()->getLastName();
      }else{
       echo "&nbsp;";
      }
       ?>

      </td>
      <td ><?php echo $result->getIpAddress();?></td>
      <td ><?php echo $result->getCategory();?></td>
      <td ><?php echo $result->getSubcategory();?></td>
      <td ><?php echo $result->getDescription();?></td>
      <td ><?php echo $result->getCreatedAt();?></td>

    </tr>
    <?php endforeach; ?>

   

<?php
          $url="";
          if($startDate != ""){ $url .= "startDate=".$startDate."&"; }
          if($endDate != ""){ $url .= "endDate=".$endDate."&"; }
          if($category != ""){ $url .= "category=".$category."&"; }
          if($subcategory != ""){ $url .= "subCategory=".$subcategory."&"; }
          
        ?>

    <tr>
        <td class="blbar" colspan="8" height="25px" align="right">
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?'.$url) ?></td>
    </tr>
    <?php
    }else { ?>
    <tr><td  colspan="8" align='center' ><div style="color:red"> No record found</div></td></tr>
    <?php } ?>
  </table>
  </div>
</div>
</div><div class="content_wrapper_bottom"></div>