<?php

/**
 * chargeback actions.
 *
 * @package    ama
 * @subpackage chargeback
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class chargebackActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        $this->setTemplate("index");
    }

    public function executeUploadchargeback() {
        $fileName = $this->getRequest()->getFileName('chargebackFile');

        $this->getRequest()->moveFile('chargebackFile', sfConfig::get('sf_upload_dir') . '/' . $fileName);

        echo "File" . $fileName . " Saved Successfully";
    }
}
?>