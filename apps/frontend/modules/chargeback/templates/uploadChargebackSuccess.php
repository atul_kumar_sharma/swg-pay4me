<html>
    <head>
        <title>
            Graypay data migration for Chargeback Report
        </title>
    </head>
    <body>
        <table border="1" width="100%" height="500" style="border:1px solid #000000">
            <tr height="90">
                <td colspan="2" style="border:1px solid #000000">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="20%" style="border:1px solid #000000" valign="top" align="left">
                    <?php include_once("_left.php");?>
                </td>
                <td valign="top" width="70%" align="center" style="font-family:arial ; border:1px solid #000000">
                    <br><br>

                     <?php echo form_tag('chargeback/uploadchargeback', 'multipart=true') ?>
                      
                      
                      <table style="border:1px solid #000000" width="80%">
                            <tr>
                                <td colspan="2" style="background:#cccccc;border:1px solid #000000">
                                    Upload Chargeback File
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Upload File:
                                </td>
                                <td>
                                   <?php echo input_file_tag('chargebackFile') ?>
			                    </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <?php echo submit_tag('Upload') ?>
                                </td>
                            </tr>
                        </table>

                       </form>
                </td>
            </tr>
        </table>
    </body>
</html>