<?php

/**
* payeasy actions.
*
* @package    ama
* @subpackage payeasy
* @author     Your name here
* @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
*/
class paymentGatewayActions extends sfActions {

    public static $version = 'V1';

    public function executeShow(sfWebRequest $request) {
        $this->ep_pay_easy_request = Doctrine::getTable('EpPayEasyRequest')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->ep_pay_easy_request);
    }

    public function executeOpenInfoBox() {
        //        $this->broadcast_message = $this->getBroadcastMessage();
        $this->setTemplate('openInfoBox');
        $this->setLayout(false);
    }

    public function executeOpenPaymentOptionBox(sfWebRequest $request) {

        $this->requestId = $request->getParameter('requestId');
        $this->setTemplate('openPaymentOptionBox');
        $this->setLayout(false);
    }

    public function executeNewViaEwallet(sfWebRequest $request) {
        $requestId = $request->getParameter('requestId');
        $this->pStatus = false;
        if ($requestId) {
            $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            //$request_details
            $orderRequestObj = OrderRequestDetailsTable::getInstance();
            if ($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
                $this->pStatus = true;
            }
            //$request_details
            $orderRequestObj = OrderRequestDetailsTable::getInstance();
            if ($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
                $this->pStatus = true;
            }
            $userId = $this->getUser()->getGuardUser()->getId();
            $amount = $request_details->getAmount();
            $this->pStatus = $this->isUserKYCapproved($amount, $userId);

            //////////////////////////////////////////////////////

            $this->request_details = $request_details;
            $this->requestId = $requestId;
            $this->getUser()->getAttributeHolder()->remove('requestId');
            //$this->form = new EpPayEasyRequestForm();
            //$this->form->setDefault('request_ip', $request->getRemoteAddress()) ;
        } else {
            throw New Exception('Payment page not found!');
        }
    }

    public function executeCreateViaEwallet(sfWebRequest $request) {
        $this->pStatus = false;
        $requestId = $request->getParameter('requestId');
        $this->request_ip = $request->getParameter('request_ip');

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $userId = $this->getUser()->getGuardUser()->getId();
        if (!$request_details->getUserId()) {
            $request_details->setUserId($userId);
            $request_details->save();
        }
        //$request_details
        $orderRequestObj = OrderRequestDetailsTable::getInstance();
        if ($orderRequestObj->getPaymentStatus($request_details->getOrderRequestId())) {
            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
            $this->pStatus = true;
        }

        $amount = $request_details->getAmount();
        $this->pStatus = $this->isUserKYCapproved($amount, $userId);
        $this->request_details = $request_details;
        $this->requestId = $requestId;

        $this->processViaeWallet($request);

        $this->setTemplate('new');
    }

    protected function processViaeWallet(sfWebRequest $request) {
        $params = array();
        $params = $request->getPostParameters();
        $payEasyObj = new PaymentManager();
        //$gatewayObj = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_active_paymentgateway'));
        $params['gateway_id'] = Settings::getEwalletGatewayId();
        $status = $payEasyObj->paymentRequestViaEwallet($params, $params['gateway_id']);
        $this->RedirectToStatusPage($status, $params['requestId'], 'newViaEwallet');
        $this->redirect($this->getModuleName() . '/thankYou?valid=' . $status['validation']);
    }

    public function RedirectToStatusPage($status, $requestId, $ActionName, $trans_type='payment') {
        switch ($status['status']) {
            case 'success':
                if ('recharge' == $trans_type) {
                    $this->redirect($this->getModuleName() . '/thankforRecharge?valid=' . $requestId);
                }
                $this->redirect($this->getModuleName() . '/thankYou?valid=' . $status['validation']);
                break;
            case 'failure':
                if ('recharge' == $trans_type)
                $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    $errorObj = new ErrorMsg();
                    $errorMessage = $errorObj->displayErrorMessage("E009", $requestId);
                    $this->getUser()->setFlash('error', $errorMessage, true);
                    //        $this->getUser()->setFlash('error', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
                }
                $this->redirect($this->getModuleName() . '/' . $ActionName . '?requestId=' . $requestId . '&paystatus=fail');
                break;
            case 'zip_failure':
                if ('recharge' == $trans_type)
                $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    $errorObj = new ErrorMsg();
                    $errorMessage = $errorObj->displayErrorMessage("E001", $requestId);
                    $this->getUser()->setFlash('error', $errorMessage, true);
                    //        $this->getUser()->setFlash('error', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
                }
                $this->redirect($this->getModuleName() . '/' . $ActionName . '?requestId=' . $requestId . '&paystatus=fail');
                break;
            case 'surname_failure':
                if ('recharge' == $trans_type)
                $this->getUser()->setFlash('error', 'Recharge not done , Please try again.', true);
                else {
                    sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
                    $errorObj = new ErrorMsg();
                    $url = url_for('page/printInstruction');
                    $errorMessage = $errorObj->displayErrorMessage("E056", $requestId, array('URL' => ' Kindly <a style="font-size: 15px; font-weight: bold;"href="javascript:;" onclick="instructionPopup(\'' . $url . '\');">click here</a> to read the instruction carefully.'));
                    $this->getUser()->setFlash('error', $errorMessage, true);
                    //        $this->getUser()->setFlash('error', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
                }
                $this->redirect($this->getModuleName() . '/' . $ActionName . '?requestId=' . $requestId . '&paystatus=fail');
                break;
            case 'Already Done':
                $this->redirect($this->getModuleName() . '/' . $ActionName . '?requestId=' . $requestId);
                break;
        } //swtich
    }

    public function executeRecharge(sfWebRequest $request) {
        $requestId = $request->getParameter('requestId');
        $this->pStatus = false;

        if ($requestId) {
            $getRequestId = base64_decode($requestId);
            $request_details = Doctrine::getTable('RechargeOrder')->find($getRequestId);

            if (0 == $request_details->getPaymentStatus()) {
                $this->getUser()->setFlash('notice', 'Recharge has been done');
                $this->pStatus = true;
            }
            $this->workType = "recharge";
            $this->request_details = $request_details;
            $this->requestId = $requestId;
            $this->getUser()->getAttributeHolder()->remove('requestId');


            // start - change by vineet for making ZIP mandatory
            $country = '';
            if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
                $country = $_REQUEST['ep_pay_easy_request']['country'];
            }
            // end - change by vineet for making ZIP mandatory
            $this->form = new EpPayEasyRequestForm('', array('task' => 'recharge', 'argument' => array('country' => $country)));
            $this->form->setDefault('request_ip', $request->getRemoteAddress());
        } else {
            throw New Exception('Payment page not found!');
        }
    }

    public function executeRechargeProcess(sfWebRequest $request) {
        $this->pStatus = false;
        $requestId = $request->getParameter('requestId');
        $this->request_ip = $request->getParameter('request_ip');

        $getRequestId = base64_decode($requestId);
        $request_details = Doctrine::getTable('RechargeOrder')->find($getRequestId);

        if (0 == $request_details->getPaymentStatus()) {
            $this->getUser()->setFlash('notice', 'Recharge has been done');
            $this->pStatus = true;
        }
        $this->workType = "recharge";
        $this->request_details = $request_details;
        $this->requestId = $requestId;

        // start - change by vineet for making ZIP mandatory
        $country = '';
        if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
            $country = $_REQUEST['ep_pay_easy_request']['country'];
        }
        // end - change by vineet for making ZIP mandatory
        $this->form = new EpPayEasyRequestForm('', array('task' => 'recharge', 'argument' => array('country' => $country)));

        $this->processRecharge($request, $this->form);

        $this->setTemplate('recharge');
    }

    protected function processRecharge(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $params = array();
            $params = $request->getPostParameters();
            $payEasyObj = new PaymentManager();
            $gatewayObj = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_active_paymentgateway'));
            $params['gateway_id'] = $gatewayObj->getFirst()->getId();
            $status = $payEasyObj->rechargeRequest($params, $params['gateway_id']);
            $this->RedirectToStatusPage($status, $params['requestId'], 'recharge', 'recharge');
        } else {
            //$this->forward($this->getModuleName(),'create');
        }
    }

    public function executeThankforRecharge(sfWebRequest $request) {
        $requestId = $request->getParameter('valid');
        $orderRequest = Doctrine::getTable('RechargeOrder')->find(base64_decode($requestId));

        $status['fname'] = $orderRequest->getSfGuardUser()->getUserDetail()->getFirstName();
        $status['lname'] = $orderRequest->getSfGuardUser()->getUserDetail()->getLastName();
        $status['amount'] = $orderRequest->getAmount();
        $status['item_fee'] = $orderRequest->getItemFee();
        $this->showPaymentDetails = $status;
    }

    public function updateFpsDetail($fpsId, $status) {
        FpsDetailTable::getInstance()->updateFpsDetailForGraypay($fpsId, $status);
    }

    public function executeThankYou(sfWebRequest $request) {

        /**
         * [WP: 085] => CR:124
         * Removed requestId from session...
         */
        $this->getUser()->getAttributeHolder()->remove('requestId');

        /**
         * [WP: 104] => CR:149
         * Removed session varibale for footer address for JNA...
         */        
        $this->getUser()->getAttributeHolder()->remove('sess_jna_processing');

        $postDataArray = $request->getParameterHolder()->getAll();
        $status['validation'] = Settings::decryptInput($postDataArray['valid']);
        if (isset($postDataArray['response_code']))
        $status['vbv_respcode'] = $postDataArray['response_code'];
        if (isset($postDataArray['response_text']))
        $status['vbv_resptext'] = $postDataArray['response_text'];

        $paymentDetails = Doctrine::getTable('ipay4meOrder')->paymentDetails($status['validation']);

        ## If payment not done then go to cart list...
        if (1 == $paymentDetails['paymentStatus']) {
            $this->paymentError = true;
        }else{
            $this->paymentError = false;
        }

        $status['fname'] = $paymentDetails['fname'];
        $status['lname'] = $paymentDetails['lname'];
        $status['merchant_name'] = $paymentDetails['mername'];
        $status['merchant_url'] = $paymentDetails['merurl'];
        $status['mabbr'] = $paymentDetails['mabbr'];
        $status['orderNumber'] = $postDataArray['valid'];

        $this->showPaymentDetails = $status;

        $this->vapFlag = false;
        $appDetailsObj = Doctrine::getTable('TransactionServiceCharges')->getAppDetailFromOrderNumber($status['orderNumber']);
        if(!empty($appDetailsObj)){
            foreach($appDetailsObj AS $appDetails){
                if(strtolower($appDetails->getAppType()) == 'vap'){
                    $this->vapFlag = true;
                }
            }
        }

        ###### START CLEAR CART ######
        $userObj = $this->getUser();
        //clear the cart
        $cartObj = new cartManager();
        $cartObj->emptyCart();
        $userObj->saveCartInDb();
        ###### END HERE ##############

        ## Fetching NIS url...
        $this->nisPortalUrl = sfConfig::get('app_nis_portal_url');


    }

    public function executeError(sfWebRequest $request) {
        $this->show = false;

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */        
        $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
        
        $this->isCardRegistrationAllowed = true;
        //if ($request->hasParameter('requestId')) {
        if ($this->requestId != '') {
            $this->show = true;
            $requestId = $this->requestId; //$request->getParameter('requestId');
            $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

            ## Getting application type...
            $this->application_type = $this->getApplicationType($this->request_details);

        }
        if ($request->hasParameter('ruleId') && ($request->getParameter('ruleId') == '5') || $request->getParameter('ruleId') == '7') {
            $this->isCardRegistrationAllowed = false;
        }
        $this->isregistered = ($request->hasParameter('registered')) ? $request->getParameter('registered') : 'no';
        $this->setTemplate('error');
    }

    public function executeCvvHelp(sfWebRequest $request) {

    }

    public function executeReason(sfWebRequest $request) {

    }

    public function isUserKYCapproved($amount, $userId) {

        ///////////////////////Check for kyc ///////////////////////////
        $user_details = Doctrine::getTable('UserDetail')->findByUserId($userId);

        $masterAccountId = $user_details->getFirst()->getMasterAccountId();
        $kycStatus = $user_details->getFirst()->getKycStatus();
        if (!isset($masterAccountId) || is_null($masterAccountId)) {
            $this->getUser()->setFlash('notice', 'Your account has not created .');
            $this->pStatus = true;
        }

        if (is_null($kycStatus) || 1 != $kycStatus) {

            $this->getUser()->setFlash('notice', 'Your KYC is not approved .');
            $approveBy = $user_details->getFirst()->getApprovedBy();
            if (1 == $approveBy && 3 == $kycStatus)
            $this->getUser()->setFlash('notice', 'Your KYC is disapproved by the admin .');
            $this->pStatus = true;
        }
        ////////////// Check balance  //////////////////////////
        ///////////// KYC aaprove val /////////////////////////
        /////////////// getBalance  //////////////////////////
        $balance = $user_details->getFirst()->getEpMasterAccount()->getClearBalance();
        $balance = $balance / 100;
        if ($balance < $amount) {
            $this->getUser()->setFlash('notice', 'You have not sufficient balance ($' . ($balance) . ')');
            $this->pStatus = true;
        }
        return $this->pStatus;
    }

    public function executeVbvPayment(sfWebRequest $request) {
        $requestId = $request->getParameter('requestId');
        $arg = false;
        if ($request->hasParameter('display_msg')) {
            $display_msg = true;
            $arg = "&display_msg=" . $display_msg;
        }
        $this->redirect('vbv_configuration/payment?requestId=' . $requestId . $arg);
    }

/*
* this function is used to display Approved Registered Cards for loged in users
* But display of card depends on some payment conditions that are given in points
* @point1: check if Cart is already processed
* @point2: check if application is already paid at NIS
* @point3: check if application is already processed
* @point4: check how many cards are registered for for logged in user
* @point5: show registered card accordingly
*/

    public function executeGotoApplicantVault(sfWebRequest $request) {

        ## Removing moflag varibale...
        ## This variable is being used to show Mo link if incase payment failure...
        $this->getUser()->getAttributeHolder()->remove('MoFlag');

        /**
         * [WP: 085] => CR:124         
         * Checking url temporing with requestId...
         */
        $this->requestId = $this->getUser()->getAttribute('requestId');
        Functions::checkRequestIdTempering($this->requestId);

        ## this variable provide time period which runs before page...
        ## if this is 0 that means no break will come before thanks...([WP: 079] => CR: 115)
        $sec = sfConfig::get('app_payment_success_time_out_limit');

        if($sec > 0){
            ## Check application is in process in job table...
            $this->verifyApplicationStatus();
        }

        /**
         * [WP:81] => FS#35260         * 
         * This function update cart items in case when application fees differ from nis database...
         */
        $this->updateCartItems();

        $cartValidationObj = new CartValidation($this->requestId);
        $cartValidationObj->gotoApplicantVaultValidations();

        ## Getting payment options...
        $paymentOptionsObj = new paymentOptions();
        $paymentOptionsObj->requestId = $this->requestId;

        $paymentModsArr = $paymentOptionsObj->getPaymentOptions();

        /**
         * [WP: 087] => CR:126
         * Fetching processing country and putting it into session...
         */
        $processingCountry = $paymentOptionsObj->getProcessingCountry();
        $this->getUser()->setAttribute('processingCountry', $processingCountry);

        ## insert app details with service charges
        $this->saveApplicationServiceCharge($this->requestId);

        ## This is for showing only registered card if exists...
        $this->isRegisteredCardCountry = $paymentModsArr['A3'];
        $this->isPrivilegeUser = $this->getUser()->isPrivilegeUser();
        ## $paymentModsArr = $this->getPaymentMode();
        $paymentModsArr = $paymentModsArr['A2'];
        $userId = $this->getUser()->getGuardUser()->getId();

        //check if vault is on or off
        $ifVaultActive = Doctrine::getTable('GlobalSettings')->findByVarKey('graypay-vault');

        if (count($paymentModsArr) > 0) {
            $paymentModeArray = array();
            $isVBV = false;
            $isMo = false;
            $isWt = false;
            foreach ($paymentModsArr as $key => $val) {
                for ($i = 0; $i < count($val['card_detail']); $i++) {
                    if ($val['card_detail'][$i]['card_type'] == 'nmi_mcs') {
                        $paymentModeArray[] = 'M';
                    } elseif ($val['card_detail'][$i]['card_type'] == 'nmi_vbv') {
                        $paymentModeArray[] = 'V';
                    } elseif ($val['card_detail'][$i]['card_type'] == 'nmi_amx') {
                        $paymentModeArray[] = 'A';
                    } elseif ($val['card_detail'][$i]['card_type'] == 'Vc') {
                        $paymentModeArray[] = 'V';
                    } elseif ($val['card_detail'][$i]['card_type'] == 'jna_nmi_vbv') {
                        $paymentModeArray[] = 'V';
                    } elseif ($val['card_detail'][$i]['card_type'] == 'jna_nmi_mcs') {
                        $paymentModeArray[] = 'M';
                    }else {
                        $paymentModeArray[] = $val['card_detail'][$i]['card_type'];
                    }
                    if ($val['card_detail'][$i]['card_type'] == 'Vc')
                    $isVBV = true;
                    if ($val['card_detail'][$i]['card_type'] == 'Mo')
                    $isMo = true;
                    if ($val['card_detail'][$i]['card_type'] == 'Wt')
                    $isWt = true;
                }//End of for($i = 0; $i < count($val['card_detail']); $i++) {...
            }//End of foreach ($paymentModsArr as $key => $val) {...
        }//End of if (count($paymentModsArr) > 0) {...
        $applicantVaultArray = array();
        if (!empty($paymentModeArray)) {
            $applicantVaultArray = ApplicantVaultTable::getInstance()->getFilterCardsFromVault($userId, $paymentModeArray);
        }

        ##### Start : Kuldeep #######
        if (count($applicantVaultArray) > 0 && $this->isPrivilegeUser) {
            $this->applicantVault = $applicantVaultArray;
        } else if (count($applicantVaultArray) > 0 && !$this->isPrivilegeUser) {
            $this->applicantVault = $applicantVaultArray;
        } else {
            if ($this->isRegisteredCardCountry && !$this->isPrivilegeUser) {
                $this->redirect('paymentGateway/payInfo');
            } else {
                $this->redirect('paymentGateway/ccChoice');
            }
        }
        ##### End : Kuldeep #######

        if ($isVBV && !$this->isPrivilegeUser) {
            if (count($applicantVaultArray) > 0) {
                $this->getUser()->setFlash('notice', 'Due to anti fraud reasons, currently approved cards are not available for use. We will be back with the service soon.');
            }
            $this->redirect('paymentGateway/ccChoice');
        }
    }

/*
* this function decide that the option which user choosed on registerd card page was use another card or registered card
* @point1: if option was registered card the user will see the payment gateway page
* @point2: if option was use another card the user will go to next payment options page
*/
    public function executeContinueChoice(sfWebRequest $request) {

        ## If cart is empty, redirect to cart list...
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'Applications has been already processed. ');
            $this->redirect('cart/list#msg');
        }

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */
        $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');

        $paymentMode = $request->getParameter('paymentMode');

        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);
        $paymentMode = base64_decode($paymentMode);
        if ($paymentMode == 'againcc') {
            ##$this->redirect('paymentGateway/ccChoice?requestId=' . $this->requestId);
            $this->redirect('paymentGateway/ccChoice');
            exit;
        } else {

            ## Removing payment card type by default...
            $this->getUser()->getAttributeHolder()->remove('paymentCardType');

            $applicantVaultValues = ApplicantVaultTable::getInstance()->find($paymentMode);
            $this->cardType = $applicantVaultValues->getCardType();

            ## $paymentModsArr = $this->getPaymentOptions($this->requestId);

            $paymentOptionsObj = new paymentOptions();
            $paymentOptionsObj->requestId = $this->requestId;
            $paymentModsArr = $paymentOptionsObj->getPaymentOptions();

            $paymentModsArr = $paymentModsArr['A2'];
            if (count($paymentModsArr) > 0) {
                $paymentModeArray = array();
                foreach ($paymentModsArr as $key => $val) {
                    for ($i = 0; $i < count($val['card_detail']); $i++) {
                        $paymentModeArray[] = $val['card_detail'][$i]['card_type'];
                    }
                }

                ## Setting up Mo flag if Mo exists...
                ## This variable will be used to show Mo link if incase payment failure...
                if(in_array('Mo', $paymentModeArray)){
                    $this->getUser()->setAttribute('MoFlag', true);
                }
            }

            ## Calling registered card page according to payment gateway...
            if (($this->cardType == 'V' && in_array('nmi_vbv', $paymentModeArray)) || ($this->cardType == 'M' && in_array('nmi_mcs', $paymentModeArray)) || ($this->cardType == 'A' && in_array('nmi_amx', $paymentModeArray))) {
                switch($this->cardType){
                    case 'V':
                        $paymentCardType = 'nmi_vbv';
                        break;
                    case 'M':
                        $paymentCardType = 'nmi_mcs';
                        break;
                    default:
                        $paymentCardType = 'nmi_amx';
                        break;
            }
            $this->getUser()->setAttribute('paymentCardType', $paymentCardType);
            $request->setParameter('applicantVaultValues', $applicantVaultValues);
            $request->setParameter('paymentMode', '');
            $this->forward('nmi', 'nmiVBVCardForm');    ///NMI => JNA NMI
        } elseif ( ($this->cardType == 'V' && in_array('jna_nmi_vbv', $paymentModeArray)) || ($this->cardType == 'M' && in_array('jna_nmi_mcs', $paymentModeArray) )) {

            ## START JNA service off and showing message...
            $jna_service_disabled = sfConfig::get('app_jna_service_disabled');
            if($jna_service_disabled){
                $disabled_daterange =  sfConfig::get('app_jna_service_disabled_daterange');
                if($disabled_daterange){
                    $startTime = sfConfig::get('app_jna_service_disabled_startdate');
                    $endTime = sfConfig::get('app_jna_service_disabled_enddate');
                }else{
                    $startTime = date('Y-m-d H:i:s');
                    $endTime = date('Y-m-d H:i:s');
                }
                $currdate = date('Y-m-d H:i:s');
                if($currdate >= $startTime && $currdate <= $endTime){
                    $notice = '<font color="RED">'.sfConfig::get('app_jna_service_message').'</font>';
                    $this->getUser()->setFlash('notice', $notice);
                    $this->redirect('paymentGateway/gotoApplicantVault');
                    exit;
                 }
            }
            ## END JNA service off and showing message...

            /**
             * [WP: 104] => CR: 149
             */
            $this->getUser()->setAttribute('sess_jna_processing', true);            

            /**
             * [WP: 111] => CR: 157
             * Adding condition of mastercard securecode for JNA service...
             */
             if($this->cardType == 'M'){
                $cardFullName = 'jna_nmi_mcs';
             }else{
                $cardFullName = 'jna_nmi_vbv';
             }

            $this->getUser()->setAttribute('paymentCardType', $cardFullName);
            $request->setParameter('applicantVaultValues', $applicantVaultValues);
            $request->setParameter('paymentMode', $cardFullName);
            
            $this->forward('nmi', 'nmiVBVCardForm');
            
        } else {
            $request->setParameter('applicantVaultValues', $applicantVaultValues);
            $this->forward('graypay', 'amexVaultCardForm');    ///GRAYPAY
        }
    }
}

/*
* this function is used to check how many groups of payment mode options are available and display those options to user
* @point1: check if there are less than two groups of payment mode option then redirect the request to received payment gateway
* @point2: check if there are more than two groups of payment mode option then dispaly the available payment options
*/
public function executeCcChoice(sfWebRequest $request) {

    /**
     * [WP: 104] => CR: 149
     * Removing session which is defined in nmi form(registered/non-registered)...
     */
    sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sess_paymentMode_for_footer');

    /**
     * [WP: 085] => CR:124
     * Fetching requestId from session...
     */
    $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');

    $paymentOptionsObj = new paymentOptions();
    $paymentOptionsObj->requestId = $this->requestId;

    //$this->processingCountry = $paymentOptionsObj->getProcessingCountry(); //arrProcessingCountry[count($paymentOptionsObj->arrProcessingCountry)-1];

    ## Fetching processing country from session...
    $this->processingCountry = $this->getUser()->getAttribute('processingCountry');

    $paymentModsArr = $paymentOptionsObj->getPaymentMode();

    //$paymentModsArr = $this->getPaymentMode($this->requestId);
    $this->cardTypeArray = $paymentModsArr['A1'];
    $paymentModsArr = $paymentModsArr['A2'];

    ## Removing session before setting it up...
    $this->getUser()->getAttributeHolder()->remove('SesPaymentModsArr');
    ## Setting payment mode array into session...
    $this->getUser()->setAttribute('SesPaymentModsArr', $paymentModsArr);
    if (empty($paymentModsArr)) {
        $this->getUser()->setFlash('notice', 'Transaction can not be processed at the moment. Please try again later. ');
        $this->redirect('cart/list#msg');
    }

    if (count($paymentModsArr) < 2) {
        //redirect directly to payment gateway without ccChoices
        $paymentMode = '';
        $isMoneyOrder = false;
        $isWireTransfer = false;
        foreach ($paymentModsArr as $key => $val) {
            for ($i = 0; $i < count($val['card_detail']); $i++) {
                if ($i == 0)
                $paymentMode .= $val['card_detail'][$i]['gateway_id'];
                $paymentMode .= ',' . $val['card_detail'][$i]['card_type'];
                if ($val['card_detail'][$i]['card_type'] == 'Mo')
                $isMoneyOrder = true;
                if ($val['card_detail'][$i]['card_type'] == 'Wt')
                $isWireTransfer = true;
            }
        }

        if ($isMoneyOrder) {
            $this->redirect('paymentGateway/moInfo');
        } else if ($isWireTransfer) {
            $request->setParameter('paymentMode', $paymentMode);
            $this->forward('wt_configuration', 'trackingProcess');
        } else {
            $request->setParameter('paymentMode', $paymentMode);
            $this->forward('paymentGateway', 'selectMode');
        }
    }

    $this->paymentModsArr = $paymentModsArr;

    //$this->isVBV = $request->getParameter('isVBV');
    $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
    $this->userdetails = $user_detail = Doctrine::getTable('UserDetail')->findByUserId($userId)->getFirst();
    $this->group = $groups = $this->getUser()->getGroupDescription();
    if (!$this->requestId) {
        throw New Exception('Payment page not found!');
    }
}

/*
* this function is used to display the Payment gateway page accoring to the selected payment mode option in last step
* @point1: check if which payment mode option was selected by user in last step
*/
public function executeSelectMode(sfWebRequest $request) {

    /**
     * [WP: 085] => CR:124
     * Fetching requestId from session...
     */
    $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
    Functions::checkRequestIdTempering($requestId);

    
    $orderRequest = Doctrine::getTable('OrderRequestDetails')->find($requestId);
    $orderRequestObj = OrderRequestDetailsTable::getInstance();
    // $orderRequestObj->getPaymentStatus($orderRequest->getOrderRequestId());
    if ($orderRequestObj->getPaymentStatus($orderRequest->getOrderRequestId())) {//print "in";exit;
        $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
        $this->redirect('cart/list');
    }

    $arrCartItems = $this->getUser()->getCartItems();
    if (count($arrCartItems) == 0) {
        $this->getUser()->setFlash('notice', 'Applications has been already processed. ');
        $this->redirect('cart/list');
    }
    //start - vineet for country zip validaton
    $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();

    $arrCountry = array();
    for ($i = 0; $i < count($countryCode); $i++) {
        $arrCountry[] = $countryCode[$i]['country_code'];
    }
    $this->arrCountry = $arrCountry;
    //end - vineet for country zip validaton

    ## Removing payment card type by default...
    $this->getUser()->getAttributeHolder()->remove('paymentCardType');

    $this->setLayout(null);
    if ($request->hasParameter('paymentMode')) {

        /**
         * [WP: 104] => CR: 149
         * Setting false for footer address...
         */        
        $this->getUser()->setAttribute('sess_jna_processing', false);

        $paymentMode = $request->getParameter('paymentMode');
        //      echo "PaymentMode" . $paymentMode ; die ;

        $_SESSION['card_type'] = $paymentMode;
        $explode = explode(',', $paymentMode);        
        $gatewayId = $explode[0];
        if ($gatewayId == 1) {   //grayPay
            $this->forward('graypay', 'new');
        } elseif ($gatewayId == 2) {   //payEasy
            $this->forward('graypay', 'new');
        } elseif ($gatewayId == 3) {   //ewallet
        } elseif ($gatewayId == 4) {   //vbv
            $this->redirect('paymentGateway/vbvPayment?requestId=' . $requestId);
        } elseif ($gatewayId == 5) {   //NMI
            $this->getUser()->setAttribute('paymentCardType', $explode[1]);
            $paymentMode = Settings::encryptInput($paymentMode);
            $this->redirect('nmi/payment?paymentMode=' . $paymentMode . '&mode=new');
        } elseif ($gatewayId == 6) {   //MO
            $this->redirect('paymentGateway/moInfo');
        } elseif ($gatewayId == 7) {   //WT
            $request->setParameter('paymentMode', $paymentMode);
            $this->forward('wt_configuration', 'trackingProcess');
        } elseif ($gatewayId == 10) {   //JNA
            /**
             * [WP: 104] => CR: 149
             */
            $this->getUser()->setAttribute('sess_jna_processing', true);
            
            $this->getUser()->setAttribute('paymentCardType', $explode[1]);
            $paymentMode = Settings::encryptInput($paymentMode);
            $this->redirect('nmi/payment?paymentMode=' . $paymentMode . '&mode=new');
        }

        //      echo "PaymentaMode" . $paymentMode ; die ;
        if ($paymentMode == 'visa') {

            $this->redirect('paymentGateway/vbvPayment?requestId=' . $requestId);
            return $this->renderText('');
        } elseif ($paymentMode == 'vbv' || $paymentMode == 'mcs') {
            $this->redirect('nmi/payment?requestId=' . $requestId . '&paymentMode=' . $paymentMode . '&mode=new');
            return $this->renderText('');
        } else {
            $this->setVar('paymentMode', $paymentMode);
            $this->forward('graypay', 'new');
        }
    } else {
        $this->requestId = $requestId;
        $this->setTemplate('ccChoice');
    }
}

public function ProcessBilling($params) {

    $userBillingprofileObj = new UserBilling();

    if (!empty($params['ep_pay_easy_request']['first_name'])) {
        $intBillingCount = Settings::getMaxBillingAddLimit();
        $userId = sfContext::getInstance()->getUser()->getAttribute('user_id', null, 'sfGuardSecurityUser');
        $concat_str = "~";
        $address = $params['ep_pay_easy_request']['address1'] . $concat_str . $params['ep_pay_easy_request']['address2'] . $concat_str . $params['ep_pay_easy_request']['town'] . $concat_str . $params['ep_pay_easy_request']['state'] . $concat_str . $params['ep_pay_easy_request']['zip'] . $concat_str . $params['ep_pay_easy_request']['country'];
        $arrCountBilling = Doctrine::getTable('UserBillingProfile')->getUserBillingProfileCount($userId, $params['gateway_id']);
        $strProfileName = $arrCountBilling + 1;
        $strProfileName = "P" . $strProfileName;

        if ($arrCountBilling == 0) {
            $userBillingprofileObj->insertUserBillingDetail($params, $strProfileName);
        } elseif ($arrCountBilling < $intBillingCount) {

            $arrCountDup = Doctrine::getTable('UserBillingProfile')->getUserBillingProfileCount($userId, $params['gateway_id'], $address, $params['ep_pay_easy_request']['first_name'], $params['ep_pay_easy_request']['last_name'], $params['ep_pay_easy_request']['email'], $params['ep_pay_easy_request']['phone']);
            if ($arrCountDup == 0) {
                $userBillingprofileObj->insertUserBillingDetail($params, $strProfileName);
            } else {
                $userBillingprofileObj->updatUserBillingDetail($params);
            }
        } else {
            $userBillingprofileObj->updatUserBillingDetail($params);
        }
    }
}

public function executeVaultProcess() {

    //find refund order number

    $arrOrderNumberVault = Doctrine::getTable('Vault')->getVaultOrder();

    $totalUnproccessAmount = 0;
    for ($i = 0; $i < count($arrOrderNumberVault); $i++) {
        $orderAmount = Doctrine::getTable('ipay4meOrder')->getOrderNumberPrice($arrOrderNumberVault[$i]['order_number']);
        if (isset($orderAmount[0]['amount'])) {
            $totalUnproccessAmount += $orderAmount[0]['amount'];
        }
    }
    $this->totalUnproccessAmount = $totalUnproccessAmount;
}

public function executeProcessVaultAmount(sfWebRequest $request) {

    $proccessAmount = $request->getPostParameter('process_amount');

    $arrOrderNumberVault = Doctrine::getTable('Vault')->getVaultOrder();
    $arrProcessingOrder = array();
    $isAttemptedOrder = false;

    $totalUnproccessAmount = 0;
    for ($i = 0; $i < count($arrOrderNumberVault); $i++) {
        $orderAmount = Doctrine::getTable('ipay4meOrder')->getOrderNumberPrice($arrOrderNumberVault[$i]['order_number']);
        $orderVaultRefundList = VaultRefundListTable::getInstance()->getOrderAmount($arrOrderNumberVault[$i]['order_number']);
        if (count($orderVaultRefundList) > 0) {
            $processAmount = $orderAmount[0]['amount'] - $orderVaultRefundList[0]['amount'];
        } else {
            $processAmount = $orderAmount[0]['amount'];
        }

        $totalUnproccessAmount += $processAmount;
        $arrProcessingOrder[$i]['vault_num'] = $arrOrderNumberVault[$i]['vault_num'];
        $arrProcessingOrder[$i]['order_number'] = $arrOrderNumberVault[$i]['order_number'];
        $arrProcessingOrder[$i]['amount'] = $processAmount;
        if ($totalUnproccessAmount >= $proccessAmount) {
            break;
        }
    }

    //update vault table
    for ($i = 0; $i < count($arrProcessingOrder); $i++) {
        $isAttemptedOrder = $this->processVaultAmount($arrProcessingOrder[$i]['vault_num'], $arrProcessingOrder[$i]['amount'], $arrProcessingOrder[$i]['order_number']);
    }
    if ($isAttemptedOrder)
    $this->getUser()->setFlash("notice", "Processing complete successfully.");
    else
    $this->getUser()->setFlash("notice", "Unprocessed Amount is ZERO.");
    $this->redirect("paymentGateway/vaultProcess");
}

private function processVaultAmount($vaultNumber, $orderAmount, $orderNumber) {
    // echo $vaultNumber,$orderAmount,$orderNumber;
    //update vault table
    $isAttemptedOrder = Doctrine::getTable('Vault')->updateVaultOrder($vaultNumber, $orderNumber, '1');
    //get gray pay response
    $epGrayPayManager = new EpGrayPayManager();

    $isSuccessPayment = $epGrayPayManager->processVault($vaultNumber, $orderAmount, $orderNumber);
    //$isSuccessPayment = true;
    if ($isSuccessPayment) {
        $isPaymentSuccess = Doctrine::getTable('Vault')->updateVaultOrder($vaultNumber, $orderNumber, '2');
        $type = 'response';
        $successMsg = "Vault successfully processed for Order Number:" . $orderNumber;
        $this->createLog($successMsg, $type, $orderNumber);
    } else {
        $isPaymentFailure = Doctrine::getTable('Vault')->updateVaultOrder($vaultNumber, $orderNumber, '3');
        $type = 'response';
        $failMsg = "Failure";
        $this->createLog($failMsg, $type, $orderNumber);
    }
    return true;
}

protected function createLog($xmldata, $type, $orderId) {
    $nameFormate = $type . '_' . $orderId;
    $vbvLog = new Pay4meIntLog();
    $vbvLog->createLogData($xmldata, $nameFormate, 'vaultprocessLog');
}

public function checkIfAlreadyPaid($appType, $appId) {

    $appIDArray = array();
    switch (strtolower($appType)) {
        case 'passport';
            $appKey = 'passport_id';
            break;
        case 'visa';
            $appKey = 'visa_id';
            break;
        case 'freezone';
            $appKey = 'freezone_id';
            break;
        case 'vap';
            $appKey = 'visa_arrival_program_id';
            break;
    }

    if (isset($appKey)) {
        $appIDArray[$appKey] = $appId;
        return $cartDetails = CartItemsTransactionsTable::getInstance()->getApplicationDetails($appIDArray);
    } else {
        return;
    }
}

private function checkCardStatus($cardNumber, $userId) {
    $cardHash = md5($cardNumber);
    $isRegistered = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);
    if (count($isRegistered) > 0) {
        return true;
    }
}

/**
* @param <type> $request
* if user try to tempor url then message will show...
*/
public function executePayInfo(sfWebRequest $request) {

    /**
     * [WP: 085] => CR:124
     * Fetching requestId from session...
     * If requestId does not exist, user redirect to cart page with tempering message...
     */

    $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
    Functions::checkRequestIdTempering($this->requestId);

    $paymentOptions = new PaymentOptions();
    $this->paymentCardTypeArray = $paymentOptions->getPaymentModeArray();

    if (count($this->paymentCardTypeArray) > 0) {
        $this->paymentCardTypeArray = array_unique($this->paymentCardTypeArray);
    }

    $orderRequest = Doctrine::getTable('OrderRequestDetails')->findById($this->requestId)->toArray();
    if (count($orderRequest) < 1) {
        $this->requestId = '';
    } else {
        $orderRequestCount = OrderRequestDetailsTable::getInstance()->getPaymentStatus($orderRequest[0]['order_request_id']);
        if ($orderRequestCount >= 1) {
            $this->requestId = '';
        }

        /**
         * [WP:084] => CR:122
         * If after threshold there are no cards remaining then
         * go to cart page with error message...
         */
        $finalPaymentMods = $paymentOptions->getCards();
        if(count($finalPaymentMods) > 0){
            if(in_array('Mo' , $finalPaymentMods)){
                if(count($finalPaymentMods) == 1){
                    $this->redirect('paymentGateway/moInfo');
                }else{
                    $this->MoFlag = true;
                }
            }else{
                $this->MoFlag = false;
            }
        }else{
            $this->getUser()->setFlash('notice', 'Transaction can not be processed at the moment. Please try again later.');
            $this->redirect('cart/list#msg');
        }        
    }
    //Start: vineet for flag registered or not
    $this->isRegistered = $this->getUser()->getAttribute('othercheck');
    //End: vineet for flag registered or not
}


public function executeRegisteredCardError(sfWebRequest $request) {    
    /**
     * [WP: 085] => CR:124
     * Getting requestId from session...
     */
    $this->requestId = $this->getUser()->getAttribute('requestId');
    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);
    if($this->request_details != ''){
        $this->show = true;
    }else{
        $this->show = false;
    }    
    $this->setTemplate('registeredCardError');
}

public function executePaymentAlready(sfWebRequest $request) {
    $this->getUser()->setFlash('notice', 'Payment has already been made for this Item');
    /**
     * [WP:085] => CR:124
     * Fetching requestId from session...
     */
    $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

    ## Getting application type...
    $this->application_type = $this->getApplicationType($this->request_details);
}

public function executePaymentConfirmation(sfWebRequest $request) {
    $this->valid = $request->getParameter('valid');
    $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($this->valid);
    if (count($ipay4meOrder) > 0) {
        $epJobData = Doctrine::getTable('epJob')->findById($ipay4meOrder[0]['jobId']);
        if (count($epJobData) > 0) {
            if (($epJobData[0]['state'] == 'suspended' || $epJobData[0]['state'] == 'finished') && ($epJobData[0]['last_execution_status'] == 'failed' || $epJobData[0]['last_execution_status'] == 'pass')) {
                $this->redirect('cart/list');
            }
        }
    } else {
        $this->getUser()->setFlash('notice', 'Tampering URL is not allowed!!!');
        $this->redirect('cart/list');
    }
}

//End of executePaymentConfirmation...

public function executeRefund(sfWebRequest $request) {

    $valid = $request->getParameter('valid');

    $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($valid);
    if (count($ipay4meOrder->toArray()) > 0) {
        $jobId = $ipay4meOrder[0]['jobId'];
        if ($jobId != '') {
            $epJob = Doctrine::getTable('EpJob')->findById($jobId);
            if ($epJob[0]['state'] == 'active' && ($epJob[0]['last_execution_status'] == 'failed' || $epJob[0]['last_execution_status'] == 'notexecuted')) {

                ## Changing status active to suspended...
                $epJobObj = Doctrine::getTable('EpJob')->find($jobId);
                $epJobObj->setState('suspended');
                $epJobObj->setLastExecutionStatus('failed');
                $epJobObj->save();

                ## Removing scheduled job...
                $queueData = Doctrine::getTable('EpJobQueue')->findByJobId($jobId);
                $queueData->delete();

                $gatewayId = ipay4meOrderTable::getInstance()->getGatewayId($valid);

                $price = ipay4meOrderTable::getInstance()->getOrderNumberPrice($valid);
                $totalAmount = $price[0]['amount'];

                $DontRefundGraypay = false;
                $DontRefundGraypay = VaultRefundListTable::getInstance()->isDontRefundGraypayVault($valid, $totalAmount);
                if ($DontRefundGraypay) {
                    $DontRefundGraypay = true;
                }

                $paymentManager = new PaymentManager();
                $midService = $paymentManager->getMidService($gatewayId);
                switch ($gatewayId) {
                    case 1: //ps1
                    case 8: //llc2
                    case 5 : //ps1
                    case 9 : //llc2
                    case 12 : //llc3
                    case 13 : //ps2
                    case 14 : //llc3
                    case 15 : //ps2
                    case 16 : //llc4 graypay
                    case 17 : //llc4 nmi
                                    if (!$DontRefundGraypay) {
                                        $transaction_number = '';
                                        ## if is in vault and processed then get the transaction_number from batch process response
                                        $vaultProcessedTxnNo = BatchProcessRequestTable::getInstance()->getVaultProcessedTxnNo($valid);
                                        $transaction_number = $vaultProcessedTxnNo;

                                        $epGrayPayManager = new EpGrayPayManager();
                                        $isRefunded = $epGrayPayManager->refund($valid, $totalAmount, $gatewayId, $transaction_number, $midService, $keepStatusNew = true);
                                    } else {
                                        $isRefunded = true;
                                    }
                                    break;
                                case 10 : //jna1
                                    $epNmiManager = new EpNmiManager();
                                    ## Setting gateway id and midservice to the class...
                                    $epNmiManager->Gateway_id = $gatewayId;
                                    $epNmiManager->midService = $midService;
                                    $isRefunded = $epNmiManager->refund($valid, $totalAmount, $gatewayId, $keepStatusNew = true);
                                    break;
                                default :
                                    $isRefunded = false;
                                    break;
                            }

                            if ($isRefunded) {
                                $this->getUser()->setFlash('notice', 'Transaction has been cancelled successfully.');
                            } else {
                                $this->getUser()->setFlash('notice', 'Transaction can not be cancelled at this moment. You may request for cancellation at refund@swgloballlc.com.');
                            }
                        } else if ($epJob[0]['state'] == 'finished' || $epJob[0]['last_execution_status'] == 'pass') {
                            $this->getUser()->setFlash('notice', 'Transaction can not be cancelled at this moment. You may request for cancellation at refund@swgloballlc.com.');
                        }
                        $this->redirect('cart/list');
                    }
                }
            }//End of executeRefund...

/**
 * This function check if application is in process or not... ([WP: 079] => CR: 115)
 */
            public function verifyApplicationStatus() {
                $arrCartItems = $this->getUser()->getCartItems();

                $orderNumberArray = array();
                $i = 0;
                foreach ($arrCartItems as $items) {
                    $appID = $items->getAppId();
                    $appType = strtolower($items->getType());
                    ## Fetching order request detail id...
                    $orderRequestDetails = Doctrine::getTable('TransactionServiceCharges')->getOrderRequestDetailId($appID, $appType);
                    if(count($orderRequestDetails) > 0){
                        foreach($orderRequestDetails AS $orderDetail){
                            $orderRequestDetailId = $orderDetail->getOrderRequestDetailId();
                            ## Fetching order number and epJobId...
                            $ipay4meOrderDetails = Doctrine::getTable('Ipay4meOrder')->findByOrderRequestDetailId($orderRequestDetailId);
                            if(count($ipay4meOrderDetails) > 0){
                                foreach($ipay4meOrderDetails AS $ipay4meOrder){
                                    $orderNumberArray[$i]['orderNumber'] = $ipay4meOrder->getOrderNumber();
                                    $orderNumberArray[$i]['jobId'] = $ipay4meOrder->getJobId();
                                    $i++;
                                }//End of foreach($ipay4meOrderDetails AS $ipay4meOrder){...
                            }//End of if(count($ipay4meOrderDetails) > 0){...
                        }//End of foreach($orderRequestDetails AS $orderDetail){...
                    }//End of if(count($orderRequestDetails) > 0){...
                }//End of foreach ($arrCartItems as $items) {...

                $completeOrderNumber = array();
                if (count($orderNumberArray) > 0) {
                    foreach ($orderNumberArray AS $order_number) {
                        if($ipay4meOrder['jobId'] > 0){
                            $epJobData = Doctrine::getTable('epJob')->findById($ipay4meOrder['jobId']);
                            if (count($epJobData) > 0) {
                                if ($epJobData[0]['state'] == 'active') {
                                    $completeOrderNumber[] = $order_number['order_number'];
                                }
                            }//End of if (count($epJobData) > 0) {...
                        }//End of if($ipay4meOrder['jobId'] > 0){...
                    }//End of foreach ($orderNumberArray AS $order_number) {...
                    if (count($completeOrderNumber) > 0) {
                        $this->getUser()->setFlash('notice', 'One or more applcation(s) from cart are already in process. Please try it after some time.');
                        $this->redirect('cart/list');
                    }
                }//End of if (count($orderNumberArray) > 0) {...

            }//End of public function verifyApplicationStatus(){...

/**
* @param <type> $request
*/
            public function executeApplicationUpdate(sfWebRequest $request){

                $this->setLayout('popupLayout');
                $this->valid = $request->getParameter('valid');
                $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($this->valid);

                if(count($ipay4meOrder) > 0){
                    if($ipay4meOrder[0]['payment_status'] == 0){
                        $this->redirect('paymentGateway/thankYou?valid='.Settings::encryptInput($this->valid));
                    }else{
                        if($ipay4meOrder[0]['jobId'] > 0){
                            $epJobData = Doctrine::getTable('epJob')->findById($ipay4meOrder[0]['jobId']);
                            if (count($epJobData) > 0) {
                                if ($epJobData[0]['state'] == 'suspended' && $epJobData[0]['last_execution_status'] == 'failed') {
                                    $this->getUser()->setFlash('notice', 'Current transaction has been already cancelled.');
                                    $this->redirect('cart/list');
                                }
                            }//End of if (count($epJobData) > 0) {...
                        }//End of if($ipay4meOrder[0]['jobId'] > 0){...
                    }
                }else{
                    $this->getUser()->setFlash('notice', 'Tampering URL is not allowed!!!');
                    $this->redirect('cart/list');
                }

            }

/**
* @param <type> $request
*/
            public function executeMoInfo(sfWebRequest $request) {

                ## If cart is empty, redirect to cart list...
                $arrCartItems = $this->getUser()->getCartItems();
                if (count($arrCartItems) == 0) {
                    $this->getUser()->setFlash('notice', 'Applications has been already processed.');
                    $this->redirect('cart/list#msg');
                }

                /**
                 * [WP: 085] => CR:124
                 * Fetching requestId from session variable...
                 * If requestId does not exist then user redirect to cart page with tempering message...
                 */
                $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
                Functions::checkRequestIdTempering($this->requestId);

                $this->mode = '';

                $paymentOptions = new paymentOptions();
                $this->processingCountry = $paymentOptions->arrProcessingCountry[count($paymentOptions->arrProcessingCountry)-1];

                if($this->processingCountry == 'GB'){
                    $this->currency = 'pound';
                    $this->moneyOrderTitle = 'United Kingdom Postal Services Money Order';
                }else{
                    $this->currency = 'dollar';
                    $this->moneyOrderTitle = 'United States Postal Services Money Order';
                }
                
                $orderRequest = Doctrine::getTable('OrderRequestDetails')->findById($this->requestId)->toArray();
                if(isset($orderRequest[0]['amount'])){

                    ## Fetching applications converted Amount for GB ...
                    if($this->processingCountry == 'GB'){
                        ## Get Application on NIS
                        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($orderRequest[0]['retry_id']);
                        $appIdArr = array();
                        $fromCurrency = 1;
                        $toCurrency = 5;
                        if(count($appDetails) > 0){
                            $itemIds = array();
                            $appAmount = 0;
                            foreach($appDetails AS $appDetail){
                                $amount = Functions::getApplicationFees($appDetail['id'], $appDetail['app_type']);
                                $appAmount = $appAmount + CurrencyManager::getConvertedAmount($amount,$fromCurrency,$toCurrency);
                            }
                            $this->totalAppsAmount = $appAmount;
                        }
                    }else{
                        $this->totalAppsAmount = $orderRequest[0]['amount'];
                    }

                }else{
                    $this->totalAppsAmount = 0;
                }
                if (count($orderRequest) < 1) {
                    $this->requestId = '';
                } else {
                    $orderRequestCount = OrderRequestDetailsTable::getInstance()->getPaymentStatus($orderRequest[0]['order_request_id']);
                    if ($orderRequestCount >= 1) {
                        $this->requestId = '';
                    }
                }
                $comeFrom = $request->getParameter('cf');
                if($comeFrom !=''){
                    $this->paymentModsCount = 1;
                    if('po' == $comeFrom){
                        $this->comeFrom = $comeFrom;
                    }else{
                        $this->requestId = '';
                    }
                }else{
                    $paymentOptionsObj = new paymentOptions();
                    $commonPaymentMods = $paymentOptionsObj->commonPaymentMods;
                    $this->paymentModsCount = count($commonPaymentMods);
                    $this->comeFrom = '';
                }

            }



            public function executePrintMoInfo(sfWebRequest $request){
                $this->requestId = '123'; // 123 is a test requestId...
                $this->setTemplate('moInfo');
                $this->setLayout('popupLayout');
                $this->mode = $request->getParameter('mode');
                $this->paymentModsCount = 1;
                $moneyOrderId = $request->getParameter('moi');
                $this->currency = 'dollar';
                if(!empty($moneyOrderId)){
                    $moneyOrderObj = Doctrine::getTable('MoneyOrder')->find($moneyOrderId);
                    if(!empty($moneyOrderObj)){
                        $this->currency = $moneyOrderObj->getCurrency();
                    }
                }
            }

/**
 *
 * @param <type> $requestId
 * Adding application information into transaction service charge table...
 */
            private function saveApplicationServiceCharge($requestId){

                ## get all cart items
                $cartItems = $this->getUser()->getCartItems();

                /**
                 * [WP: 087] => CR:126
                 * Fetching processing country...
                 */
                $processingCountry = $this->getUser()->getAttribute('processingCountry');
                $destiNationCurrencyId = CurrencyManager::getDestinationCurrency($processingCountry);

                foreach($cartItems as $value){

                    ## Getting ref number
                    $refNumber = $this->getAppRefNumber($value->getType(),$value->getAppId());

                    ## Checking duplicate record for the same request id...
                    $serviceObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($value->getAppId(), $value->getType(), $requestId);
                    if(count($serviceObj) < 1){
                        $serviceChargeObj = new TransactionServiceCharges();
                        $serviceChargeObj->setAppId($value->getAppId());
                        $serviceChargeObj->setRefNo($refNumber);
                        $serviceChargeObj->setAppType($value->getType());
                        if(strtolower($value->getType()) == 'vap'){
                           $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_arrival_additional_charges'));
                        }
                        
                        /**
                         * [WP: 102] => CR: 144
                         * If application is entry freezone means visacategoryid is 101 then update tables...
                         */
                        $additional_visa_flag = false;
                        if(sfConfig::get('app_visa_additional_charges_flag')){
                            
                            if(strtolower($value->getType()) == 'visa'){
                               $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_additional_charges'));
                               $additional_visa_flag = true;
                            }else if(strtolower($value->getType()) == 'freezone'){
                                   $isVisaEntryFreezone = Functions::isVisaEntryFreezone($value->getAppId());
                                   if($isVisaEntryFreezone){
                                        $serviceChargeObj->setServiceCharge(sfConfig::get('app_visa_additional_charges'));
                                        $additional_visa_flag = true;
                                   }
                            }
                        }
                        
                        $sourceCurrencyId = 1;
                        
                        $appAmount = $value->getPrice();
                        
                        $serviceChargeObj->setAppAmount($appAmount);
                        $serviceChargeObj->setAppCurrency($sourceCurrencyId);

                        if($sourceCurrencyId != $destiNationCurrencyId){
                            if(strtolower($value->getType()) != 'vap'){
                                $appConvertAmount = $value->getPrice();
                                $appAmount = Functions::getApplicationFees($value->getAppId(), $value->getType());                                
                                /**
                                 * [WP: 102] => CR: 144
                                 * Adding converted amount into database for visa and entry freezone...
                                 */
                                if($additional_visa_flag){
                                    $appConvertServiceCharge = CurrencyManager::getConvertedAmount(sfConfig::get('app_visa_additional_charges'), $sourceCurrencyId, $destiNationCurrencyId);
                                    $appFees = Functions::getVisaApplicationFeesWithoutAdditionalCharges($value->getAppId(), $value->getType());
                                    $appConvertedFees = CurrencyManager::getConvertedAmount($appFees, $sourceCurrencyId, $destiNationCurrencyId);
                                    $appConvertAmount = $appConvertedFees + $appConvertServiceCharge;
                                    $serviceChargeObj->setAppConvertServiceCharge($appConvertServiceCharge);                                    
                                }
                                
                                $serviceChargeObj->setAppConvertAmount($appConvertAmount);
                                $serviceChargeObj->setAppAmount($appAmount);
                                $serviceChargeObj->setAppCurrency($destiNationCurrencyId);
                            }//End of if(strtolower($value->getType()) != 'vap'){...
                        }//End of if($sourceCurrencyId != $destiNationCurrencyId){...

                        $serviceChargeObj->setOrderRequestDetailId($requestId);
                        $serviceChargeObj->save();
                    }
                }//End of foreach($cartItems as $value){...
            }//End of private function saveApplicationServiceCharge...


/**
 *
 * @param <type> $appType
 * @param <type> $appId
 * @return <type>
 * This function return application refrence number...
 */
            public function getAppRefNumber($appType,$appId) {
                switch (strtolower($appType)) {
                    case 'passport';
                        $table = 'PassportApplication';
                        break;
                    case 'visa';
                        case 'freezone';
                            $table = 'VisaApplication';
                            break;
                        case 'vap';
                            $table = 'VapApplication';
                            break;
                    }
                    $appDetails = Doctrine::getTable($table)->find($appId);
                    return $appDetails->getRefNo();

                }

                private function getApplicationType($request_details){
                    $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($request_details->getRetryId());
                    if (count($appDetails) == 1) {
                        $application_type = strtolower($appDetails[0]['app_type']);
                    }else{
                        $application_type = '';
                    }
                    return $application_type;
                }

                /**
                 * [WP:81] => FS#35260
                 * @return <type>
                 * This function update cart items in case when application fees differ from nis database...
                 */

                private function updateCartItems(){

                    ## get all cart items
                    $cartItems = $this->getUser()->getCartItems();

                    $updatedItems = array();
                    foreach($cartItems as $value){

                        ## Creating variable which contains table name...
                        $appType = strtolower($value->getType());
                        if ($appType == 'passport') {
                            $table = "PassportApplication";
                        } elseif ($appType == 'visa') {
                            $table = "VisaApplication";
                        } elseif ($appType == 'vap') {
                            $table = "VapApplication";
                        } else{
                            $table = "VisaApplication";
                        }
                        
                        ## Fetching application current amount...
                        $app = Doctrine::getTable($table)->find($value->getAppId());
                        $appCurrentPrice = $app->getDollarFee();

                        ## If cart amount is differ from application current amount then following condition will execute...
                        if($value->getPrice() != $appCurrentPrice){
                            $item = $app->getCartItem();
                            $updatedItems[$item->getAppId()] = $item;
                        }
                    }

                    ## If updateItems array does not empty that means there are some updates for cart...
                    if(!empty($updatedItems)){
                        $cartItems = $this->getUser()->updateCartItems($updatedItems);
                    }

                    return;
                }
            }
