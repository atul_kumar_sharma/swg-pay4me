<?php
use_javascript('dhtmlwindow.js');
use_stylesheet('dhtmlwindow.css');
use_javascript('modal.js');
use_stylesheet('modal.css');
?>
<script language="javascript">

    //printNISPaymentSlip(<?php //echo $showPaymentDetails['orderNumber']; ?>);

    function printNISPaymentSlip(order)
    {
        commentwindow=dhtmlmodal.open('printNISPaymentReceipt', 'iframe', '<?php echo url_for('supportTool/printCommand').'?orderId='?>'+order, 'Print NIS Application Payment Receipt', 'width=500px,height=200px,center=1,border=0, scrolling=yes')
        commentwindow.onclose=function(){
            return true ;
        }
    }

</script>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php include_partial('global/innerHeading',array('heading'=>'Order Status'));?>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <div>
            <p class="redBigMessage" >PLEASE READ CAREFULLY</p>
            <p> <h2><span class="approveInfo"></span>&nbsp;Thanks <?php echo ucfirst($showPaymentDetails['fname'])." ". ucfirst($showPaymentDetails['lname']);?>. You are done!</h2>
                <?php if(isset($showPaymentDetails['vbv_respcode']) && isset($showPaymentDetails['vbv_resptext']))
                {
                    ?>
                <span class="lspace"></span> Response Code is <b><?php echo $showPaymentDetails['vbv_respcode'];?></b> and Response Description is <b><?php  echo $showPaymentDetails['vbv_resptext'];?></b> with respect to  transaction number <b><?php  echo $showPaymentDetails['validation']; ?></b>
                <br>
                <?php
            }
            ?>
                <span class="lspace"></span>Your order has been sent to&nbsp;<?php echo $showPaymentDetails['mabbr'];?>. Please proceed as follows:
                <br><br>
                <div style="padding-left:15px;"><?php echo link_to('Click Here', url_for('report/getReceipt?receiptId='.Settings::encryptInput($showPaymentDetails['validation'])), 'class=bluelink');?> to print your <strong>Transaction Payment Receipt</strong>.</div>
                <br>
                <ol class="contentul">
                <strong>YOU MUST TAKE THE FOLLOWING TO THE EMBASSY/CONSULATE:</strong><br>
                <li>YOUR SIGNED APPLICATION FORM (IN CASE OF PASSPORT ONLY).</li>
                <li>TWO (2) PASSPORT SIZED PICTURES.</li>
                <li>ACKNOWLEDGEMENT SLIP AND PAYMENT RECEIPT.</li>
                <p> <a href="<?php echo $nisPortalUrl; ?>visa/OnlineQueryStatus" class="bluelink"> Click Here</a> to enter your Application ID and Reference Number to print the required <strong>Acknowledgement Slip</strong> and <strong>Payment Receipt</strong>.
                    <br><br>NOTE:  If you have made one payment for MULTIPLE Applications using our CART, you must enter the Application ID and Reference Number for each Application in order to pull up the Acknowledgement Slip and Payment Receipt for printing.
                    <br><br>(Please go to <a href="<?php echo $nisPortalUrl; ?>" class="bluelink"><?php echo $nisPortalUrl; ?></a> and "Query Your Application Status" to pull up the Acknowledgement Slip and Payment Receipt if you wish to do so at a later time).
                <br><br>PLEASE CHECK YOUR EMBASSY/CONSULATE WEB SITE FOR OTHER DOCUMENTS THAT MIGHT BE REQUIRED WHERE APPLICABLE.</p>
                <div class="clear">&nbsp;</div>
                <!--
<p><h2>How do I track my application?</h2>
<?php //echo link_to('Get up-to-date application progress', url_for('report/getReceipt?receiptId='.Settings::encryptInput($showPaymentDetails['validation'])), 'class=bluelink');?> on swgloballlc.com</p>
<?php //echo link_to('Return to '.$showPaymentDetails['mabbr'], url_for($showPaymentDetails['merchant_url']));?>
                -->
            </p>
            </ol>

            <br><br>
            <?php if($paymentError) { ?>
            <div class="highlight_new red">
                <strong>Note:</strong> Due to some technical reason, we will be able to update your application status wihin 24 Hrs.
            </div>
            <?php } ?>

        <?php if($vapFlag){
            $no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;
            list($y, $m, $d) = explode('-',date('Y-m-d'));
            $expiry_date = date('Y-m-d', mktime(0,0,0,$m+$no_of_months,$d-1,$y));
            $datetime = date_create($expiry_date);
            ?>
            <ol class="contentul redBigMessage"><strong>Note: Application(s) are valid till <?php echo $no_of_months; ?> <?php echo ($no_of_months > 1)?'months':'month';?>. it will expire automatically after <?php echo date_format($datetime, 'd/F/Y'); ?> .</strong></ol>
            <br /><br />
            <?php } ?>
        </div>
    </div>
</div>
<div class="content_wrapper_bottom"></div>