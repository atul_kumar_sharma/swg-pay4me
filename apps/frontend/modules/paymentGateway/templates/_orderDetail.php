<?php
$currencyId = $request_details->getCurrency();
$currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currencyId);
if($currencyId == 1){
    $amount = $request_details->getAmount();
}else{
    $amount = $request_details->getConvertAmount();
}
?>
<div class="clear"></div>
<div class="tmz-spacer"></div>
<table width="100%">
  <tr>
    <td class="blbar" colspan="3">Order Details - <?php //echo $request_details->getMerchant()->getAddress();?> </td>
  </tr>
  <tr>
    <td width="77%" class="txtBold">Item</td>
    <td width="23%" class="txtBold txtRight">Price</td>
  </tr>
  <tr>

    <td ><span class="txtBold"><?php  echo $request_details->getDescription();?> </span></td>

    <td class="txtRight"><?php echo format_amount($amount,1,0,$currencySymbol);?>
    <?php

        
        
        if(isset($application_type)){
            if($application_type == 'vap' ){
                echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: $'.sfConfig::get('app_visa_arrival_additional_charges').')</p></i>';
            }
            if(sfConfig::get('app_visa_additional_charges_flag')){
                if($application_type == 'visa' ){
                    ## Converted Additional charges for visa...
                    $additional_charges = CurrencyManager::getConvertedAmount(sfConfig::get('app_visa_additional_charges'),1,$currencyId);
                    //echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
                    echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                }else if($application_type == 'freezone' ){
                    $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($request_details->getRetryId());
                    if(count($appDetails) == 1){
                       $isVisaEntryFreezone = Functions::isVisaEntryFreezone($appDetails[0]['id']);
                       if($isVisaEntryFreezone){
                            ## Converted Additional charges for visa...
                            $additional_charges = CurrencyManager::getConvertedAmount(sfConfig::get('app_visa_additional_charges'),1,$currencyId);
                            //echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: '.html_entity_decode($currencySymbol).$additional_charges.')</p></i>';
                            echo Functions::getAdditionalText($additional_charges, $currencySymbol);
                       }
                    }//End of if(count($appDetails) == 1){...
                }
            }//End of if(sfConfig::get('app_visa_additional_charges_flag')){...
        }//End of if(isset($application_type)){...
      
      ?>
    </td>
  </tr>
  <tr class="blTxt">

    <td class="txtBold">&nbsp;</td>
    <td class="txtRight blTxt bigTxt">Total: <?php echo format_amount($amount,1,0,$currencySymbol);?></td>
  </tr>
</table>
