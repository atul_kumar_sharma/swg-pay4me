<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of actionsclass
 *
 * @author akumar1
 */
class backendPaymentActions extends sfActions {

    /**
     * Executes index actionvalidated params
     *
     * @param sfRequest $request A request object

     * */
    public function executePayment(sfWebRequest $request) {
        $this->setLayout(NULL);
        $isBackendPayment = 'backend';
        $version = $request->getParameter('payprocess');
        $pay4MeIntObj = Pay4MeIntServiceFactory::getService($version);
        $returnVal = $pay4MeIntObj->processOrder($request, $isBackendPayment);
        return $this->renderText($returnVal);
    }

    public function executeProceed(sfWebRequest $request) {
        $this->setLayout(NULL);
        if ($request->hasParameter('order')) {
            //decode the order number; which is combination of txn_ref:id of merchant_request table
            if ($request->getParameter('order') != "") {
                $order = base64_decode($request->getParameter('order'));
                if (strpos($order, ":") !== false) {
                    $order_details = explode(":", $order);
                    $this->getUser()->setAttribute('loginOnOrderRequest', true);

                    $requestId = $order_details['1']; //get id of merchant_request
                    $yml = sfConfig::get('sf_app_config_dir') . '/payment.yml';

                    $details = sfYaml::load($yml);
                    $details = $details['all'];
                    $paymentDetails = $details['payment_details'];

                    $userId = array_rand($paymentDetails);
                    $vaultNumber = $paymentDetails[$userId];
                    $cardDetails = $details['card_details'][$vaultNumber];
                    $cardDetailsArr = explode(',', $cardDetails);
                    //User sigin functionality
                    $userObj = Doctrine::getTable('sfGuardUser')->find($userId);
                    $this->getUser()->signin($userObj, false);

                    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                    if ($this->request_details) {
                        //check amount if amount is greater than $15 do not pay
                        if ($this->request_details->getAmount() > 15) {
                            print_r("Payment is not done due to Amount is more than $ 5 ");
                            die;
                        }
                        //payment part
                        $backendPaymentManager = new backendPaymentManager($this->request_details, $userId, $vaultNumber, $cardDetailsArr);
                        $doBackendPayment = $backendPaymentManager->doBackendPayment();

                        if ($doBackendPayment['status'] == 'success') {
                            $orderNumber = $doBackendPayment['order_number'];
                            print_r("Payment is done successfully with order number : $orderNumber.");
                            die;
                        } else {
                            print_r("Payment is not done.");
                            die;
                        }
                    } else {
                        print_r("Order Request Id has been tampered with!!");
                        die;
                    }
                } else {
                    print_r("Invalid Order");
                    die;
                }
            } else {
                print_r("Value of order cannot be empty!!");
                die;
            }
        } else {
            print_r("Mandatory Parameter Order not found!!");
            die;
        }
    }
}