<?php use_helper('Pagination');
include_partial('global/innerHeading',array('heading'=>'Unpaid Bills'));?>
<div class="global_content4 clearfix">
  <div id="nxtPage">
    <table width="100%">
      <tr>
        <td class="blbar" colspan="6" align="right">
        <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
      </tr>
      <tr>
        <td width="10%"><span class="txtBold">S. No.</span></td>
        <td width="20%"><span class="txtBold">Bill Number</span></td>
        <td width="15%"><span class="txtBold">Amount</span></td>
        <td width="15%"><span class="txtBold">Status</span></td>
        <td width="15%"><span class="txtBold">Created At</span></td>
        <td width="15%"><span class="txtBold">Action</span></td>
      </tr>
      <?php
      if(($pager->getNbResults())>0) {
        $limit = sfConfig::get('app_records_per_page');
        $page = $sf_context->getRequest()->getParameter('page',0);
        $i = max(($page-1),0)*$limit ;
        ?>
        <?php
        foreach ($pager->getResults() as $result)://echo "<pre>";print_r($result);die;
        $i++;

        $orderRequestDetails = Doctrine::getTable('OrderRequestDetails')->getLatestUnpaidOrderRequestDetails($result->getOrderRequestId());
        //echo "<pre>"; print_r($orderRequestDetails); die;

        ?>
      <tr>
        <td><?php echo $i;?></td>
        <td ><span><?php echo $result->getBillNumber();?></span></td>
        <td><span ><?php echo format_amount($result->getOrderRequest()->getAmount(),1);?></span></td>
        <td ><span ><?php echo ucfirst($result->getStatus());?></span></td>
        <td><span ><?php echo $result->getCreatedAt();?></span></td>
        <td>
          <div class="lblButton">
            <?php $vbv_active  = settings::isVbvActive();
            if($paymentRights){
              $url = url_for('paymentProcess/blockUserMsg');
            }else if($vbv_active){
              $url = 'paymentGateway/gotoApplicantVault?requestId='.$orderRequestDetails['id'];
            }else{
              $url = 'paymentGateway/new?requestId='.$orderRequestDetails['id'];
            }
            if($vbv_active) {
              echo button_to('Pay', $url, array('class' => 'button'));
            }
            else {
              echo button_to('Pay', $url, array('class' => 'button'));
            } ?>
            <?php //echo link_to('Pay', 'paymentGateway/new?requestId='.$result->getOrderRequestId(), 'post=true') ?>

          </div>
          <div class="lblButtonRight">
            <div class="btnRtCorner"></div>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
      <tr>
        <td class="blbar" colspan="6" height="25px" align="right">
        <?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName())) ?></td>
      </tr>
      <?php
    }else { ?>
      <tr><td  colspan="6" align='center' ><div style="color:red"> No record found</div></td></tr>
      <?php } ?>
    </table>
  </div>
</div>
