<?php

/**
 *  order actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class billActions extends sfActions {

    public function executeUnpaidBills(sfWebRequest $request) {

        $getGuardUser = sfContext::getInstance()->getUser()->getGuardUser();
        $user_id = $getGuardUser->getId();
        $this->paymentRights = $getGuardUser->getUserDetail()->getPaymentRights();
        $unpaidBills = Doctrine::getTable('Bill')->getUnpaidBillRecords($user_id);

        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('Bill', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($unpaidBills);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
}
