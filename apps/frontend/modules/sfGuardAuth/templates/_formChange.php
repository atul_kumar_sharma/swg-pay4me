<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>

<form onsubmit="return validate_frm(this);" action="<?php echo url_for('sfGuardAuth/savechange'); ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm" id="ama_user_change_password_form" name="ama_user_change_password_form">
<?php if (!$form->getObject()->isNew()): ?>
<input type="hidden" name="sf_method" value="put" />
<?php endif; ?>
  <table>
    <tbody>
      <tr>
      <td>
      <?php echo $form ?>
      </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="2">
             <dl>
              <dt></dt>
              <dd> <input type="submit" value="Change Password" /></dd>
             </dl>
        </td>
      </tr>
    </tfoot>
  </table>
</form>

