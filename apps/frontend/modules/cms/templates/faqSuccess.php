<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'FAQs'));?>
    <div class="inside_banner"><?php echo image_tag('/images/faq_banner.gif',array('alt'=>'')); ?></div>
    <div class="clear"></div>
  <div class="hlBox clear">
      <p class="hlarrow"><?php echo image_tag('/images/question.gif',array('alt'=>'')); ?></p>
      <p class="blhlTxt"> What does SW Global LLC do?
      <div class="clear"></div>
      <p class="answers">SW Global LLC provides complete, simple, and secure online registration and application processing services to consumers who wish to obtain a Visa or Passport to travel the world.</p>
    </p>
  </div>
  <div class="hlBox clear">
      <p class="hlarrow"><?php echo image_tag('/images/question.gif',array('alt'=>'')); ?></p>
      <p class="blhlTxt"> Why SW Global LLC? </p>
      <div class="clear"></div>
      <p class="answers">Unlike the usual way to obtain a visa or passport, SW Global LLC speeds up the application process and helps educate the consumer on how exactly to obtain your visa or passport the correct way. No need to mail in your application, SW Global LLC provides real time applications that will be sent electronically after application is completed.</p>
  </div>
  <div class="hlBox clear">
      <p class="hlarrow"><?php echo image_tag('/images/question.gif',array('alt'=>'')); ?></p>
      <p class="blhlTxt"> What is SW Global LLC refund policy? </p>
      <div class="clear"></div>
      <p class="answers"> Please go to <a href="https://www.swgloballlc.com/cms/refundPolicy" class="bluelink">https://www.swgloballlc.com/cms/refundPolicy</a> to view the SW Global LLC refund policy.</p>
  </div>
  </div>
  </div>
<div class="content_wrapper_bottom"></div>