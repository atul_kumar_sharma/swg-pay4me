<?php

/**
 * Cms actions.
 *
 * @package    symfony
 * @subpackage Cms
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class CmsActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $pageName = $request->getParameter('page');
        if (isset($pageName) && !empty($pageName) && $pageName != 'welcome') {
            $this->setTemplate($pageName);
        } else {
            $this->redirect404();
        }
    }
}