<?php include_partial('global/innerHeading',array('heading'=>'Order Status'));?>

<div class="">
  <?php if($show) include_partial('orderDetail', array('request_details'=>$request_details)); ?>
</div>

<div class="global_content4 clearfix">
  <div class="wrapForm2">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){ ?><br />
    <div id="flash_error" class="alertBox" >
      <?php
      echo nl2br($sf->getFlash('notice'));
      ?>
    </div><br />
    <?php }?>

    <?php
    ## If card type contains Mo then only this link will enable...
    $MoFlag = sfContext::getInstance()->getUser()->getAttribute('MoFlag');
    if($MoFlag){ ?>
    <!--<div class="highlight yellow" id="flash_notice_content" style="margin-right: 0px;margin-left: 0px;"><b><span style="font-size: 15px;"><a href="<?php //echo url_for("mo_configuration/trackingProcess?requestId=".$requestId);?>">CLICK HERE</a> if you would like to attempt this transaction via Money Order.</span></b></div>-->
    <?php } ?>

    <div class="thanksArea">
      <p class="redBigMessage" >
        <h2>
          <center style="font-size:15px;font-weight:bold"><a href="<?php echo url_for('cart/list')?>" style="font-size:15px;font-weight:bold">Click here</a> to see your cart.</center>
        </h2>
      </p>
    </div>
  </div>
</div>
