<?php include_partial('global/innerHeading',array('heading'=>'Order Status'));?>

<div class="global_content4 clearfix">
  <div class="thanksArea">
    <p class="redBigMessage" >PLEASE READ CAREFULLY</p>
    <p> <h2><span class="approveInfo"></span>&nbsp;Thanks <?php echo ucfirst($showPaymentDetails['fname'])." ". ucfirst($showPaymentDetails['lname']);?>, you're done!</h2>
    <?php if(isset($showPaymentDetails['vbv_respcode']) && isset($showPaymentDetails['vbv_resptext']))
      {
    ?>
     <span class="lspace"></span> Response Code is <b><?php echo $showPaymentDetails['vbv_respcode'];?></b> and Response Description is <b><?php  echo $showPaymentDetails['vbv_resptext'];?></b> with respect to  transaction number <b><?php  echo $showPaymentDetails['validation']; ?></b>
     <br>
    <?php
      }
    ?>
    <span class="lspace"></span>Your order has been sent to&nbsp;<?php echo $showPaymentDetails['mabbr'];?>. Please proceed as follows:
    <br><br>
    <ol class="contentul">
        <li>Click <a href="http://portal.immigration.gov.ng/visa/OnlineQueryStatus">HERE</a> to return to NIS (or go to <a href="http://portal.immigration.gov.ng/">http://portal.immigration.gov.ng/</a> and "Query Your Application Status")</li>
        <li>Using your APPLICATION ID and REFERENCE NUMBER (sent to you via email), please<br> pull up your NIS ACKNOWLEDGEMENT SLIP and PAYMENT RECEIPT.</li>
        <li>if you have made ONE payment for MULTIPLE APPLICATIONS using our CART, you must<br> enter each APPLICATION ID/REFERENCE NUMBER at <a href="http://portal.immigration.gov.ng/">http://portal.immigration.gov.ng/</a> and <br>"Query Your Application Status" to pull up the ACKNOWLEDGEMENT SLIP and PAYMENT RECEIPT<br> for each application.</li>
        <li>PRINT your ACKNOWLEDGEMENT SLIP and PAYMENT RECEIPT. You must present the <br>ACKNOWLEDGEMENT SLIP and PAYMENT RECEIPT to the Embassy/Consulate with other required documents.</li>
    </ol>
   
    <p><h3><span class="lspace"></span>How do I track my application?</h3>
    <span class="lspace"></span><?php echo link_to('Get up-to-date application progress', url_for('report/getReceipt?receiptId='.$showPaymentDetails['validation']));?> on swgloballlc.com</p>
   <?php //echo link_to('Return to '.$showPaymentDetails['mabbr'], url_for($showPaymentDetails['merchant_url']));?></p>
   <br><br>
  </div>
</div>