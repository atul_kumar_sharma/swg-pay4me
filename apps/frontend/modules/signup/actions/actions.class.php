<?php

class signupActions extends sfActions {

    public function executeCreate(sfWebRequest $request) {
        # Initilize Variables
        if (trim($request->getPostParameter('name')) != "")
            $this->name = $request->getPostParameter('name');
        if (trim($request->getPostParameter('lname')) != "")
            $this->lname = $request->getPostParameter('lname');
        if (trim($request->getPostParameter('username')) != "")
            $this->username = $request->getPostParameter('username');
        if (trim($request->getPostParameter('password')) != "")
            $this->password = $request->getPostParameter('password');
        if ($request->getPostParameter('dob[month]') != 0)
            $this->dob = $request->getPostParameter('dob[month]');
        if ($request->getPostParameter('dob[day]') != 0)
            $this->dob = $request->getPostParameter('dob[day]');
        if ($request->getPostParameter('dob[year]') != 0)
            $this->dob = $request->getPostParameter('dob[year]');
        if (trim($request->getPostParameter('email')) != "")
            $this->email = $request->getPostParameter('email');
        if (trim($request->getPostParameter('address')) != "")
            $this->address = $request->getPostParameter('address');
        if (trim($request->getPostParameter('mobileno')) != "")
            $this->mobileno = $request->getPostParameter('mobileno');
        if (trim($request->getPostParameter('workphone')) != "")
            $this->workphone = $request->getPostParameter('workphone');
        if (trim($request->getPostParameter('sms')) != "")
            $this->sms = $request->getPostParameter('sms');



        # Check Whether User Name is already exist or not
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($this->username);

        # Check Whether Email Id is already exist or not
        $emailObj = Doctrine::getTable('UserDetail')->chkEmailId($this->email, '');

        if ($username_count == 0) {

            if ($emailObj > 0) {
                $this->getUser()->setFlash('error', sprintf('E-mail Address already exists'));
                $this->forward('signup', 'index');
            }



            if (!$this->userDetailValid($request)) {
                $this->forward('signup', 'index');
            }

            //        if($this->password != ''){
            //          $password= base64_decode($this->password);
            //        }
            #Create New User
            $sfUser = new sfGuardUser();
            $userName = $this->username;
            $password = $this->password;
            $sfUser->setUsername($userName);
            $sfUser->setPassword($password);
            $sfUser->setIsActive(true);
            $sfUser->save();

            $sfUserId = $sfUser->getId();
            #create a User Group
            $userTypeToCreate = sfConfig::get('app_pay4me_role_user');

            $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName($userTypeToCreate);
            $sfGroupId = $sfGroupDetails->getFirst()->getId();
            $sfGuardUsrGrp = new sfGuardUserGroup();
            $sfGuardUsrGrp->setGroupId($sfGroupId);
            $sfGuardUsrGrp->setUserId($sfUserId);
            $sfGuardUsrGrp->save();

            //   associate user with permission
            //        $user_permission = sfConfig::get('app_pfm_permission_user');
            //
            //        $sfPermissionDetails = Doctrine::getTable('sfGuardPermission')->findByName($user_permission);
            //        $sfPermissionId = $sfPermissionDetails->getFirst()->getId();
            //        $sfPermissionUsrGrp = new sfGuardUserPermission();
            //        $sfPermissionUsrGrp->setPermissionId($sfPermissionId);
            //        $sfPermissionUsrGrp->setUserId($sfUserId);
            //        $sfPermissionUsrGrp->save();
            #Save User Detail
            $this->saveUserDetail($request, $sfUserId);
            $hashAlgo = sfConfig::get('app_user_token_hash_algo');
            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUserId));
            $email = $sfGuardUserDetail->getUserDetail()->getEmail();
            $fname = $sfGuardUserDetail->getUserDetail()->getFirstName();
            $sname = $sfGuardUserDetail->getUserDetail()->getLastName();
            //$userPwdSalt = $sfGuardUserDetail->getSalt();
            // $updatedAt = $sfGuardUserDetail->getUpdatedAt();
            //$activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo,$userPwdSalt,$updatedAt);

            $subject = "iPay4me Confirmation Mail";
            $partialName = 'mailTemplate';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            // $activation_url = url_for('signup/activate', true).'?i='.$sfUserId.'&t='.$activationToken;
            $taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation', $this->moduleName . "/sendMail", array('id' => $sfUserId, 'subject' => $subject, 'partialName' => $partialName));
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');

            #Generate Activation Token For Activation Link
            //        $hashAlgo = sfConfig::get('app_user_token_hash_algo');
            //        $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUserId));
            //        $userPwdSalt = $sfGuardUserDetail->getSalt();
            //        $updatedAt = $sfGuardUserDetail->getUpdatedAt();
            //
            //        $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo,$userPwdSalt,$updatedAt);
            //
            //        $subject = "User Confirmation Mail" ;
            //        $partialName = 'account_details' ;
            //        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            //        $activation_url = url_for('signup/activate', true).'?i='.$sfUserId.'&t='.$activationToken;
            //        $taskId = EpjobsContext::getInstance()->addJob('SendMailConfirmation',$this->moduleName."/sendEmail", array('userid'=>$sfUserId,'subject'=>$subject,'partialName'=>$partialName,'activation_url'=>$activation_url));
            //        $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            #End Code To Send Confirmation Mail To The eWallet Owner

            $this->forward('signup', 'thanks');
        } else {
            $this->getUser()->setFlash('error', sprintf('Username already exists'));
            $this->forward('signup', 'index');
        }
    }

    protected function saveUserDetail($request, $userId) {
        $userDetail = new UserDetail();
        $userDetail->setUserId($userId);
        if (trim($request->getPostParameter('name')) != "")
            $userDetail->setFirstName(trim($request->getPostParameter('name')));
        if (trim($request->getPostParameter('lname')) != "")
            $userDetail->setLastName(trim($request->getPostParameter('lname')));
        if ($request->getPostParameter('dob[month]') != 0 && $request->getPostParameter('dob[day]') != 0 && $request->getPostParameter('dob[year]') != 0)
            $date = date('Y-m-d', strtotime($request->getPostParameter('dob[day]') . '-' . $request->getPostParameter('dob[month]') . '-' . $request->getPostParameter('dob[year]')));
        $userDetail->setDob($date);
        if (trim($request->getPostParameter('address')) != "")
            $userDetail->setAddress(trim($request->getPostParameter('address')));
        if (trim($request->getPostParameter('email')) != "")
            $userDetail->setEmail(trim($request->getPostParameter('email')));
        if (trim($request->getPostParameter('mobileno')) != "")
            $userDetail->setMobilePhone(trim($request->getPostParameter('mobileno')));
        if (trim($request->getPostParameter('workphone')) != "")
            $userDetail->setWorkPhone(trim($request->getPostParameter('workphone')));
        if (trim($request->getPostParameter('sms')) != "")
        //$userDetail->setSendSms(trim($request->getPostParameter('sms')));
            $userDetail->save();
    }

    protected function userDetailValid($request) {

        $msg = array();
        if (trim($request->getPostParameter('name')) == "") {
            $msg[] = 'Please enter First Name';
        }
        if (trim($request->getPostParameter('lname')) == "") {
            $msg[] = 'Please enter Last Name';
        }
        if (trim($request->getPostParameter('username')) == "") {
            $msg[] = 'Please enter User Name ';
        }
        if (trim($request->getPostParameter('password')) == "") {
            $msg[] = 'Please enter Password';
        }
        if (trim($request->getPostParameter('cpassword')) == "") {
            $msg[] = 'Please enter Confirm Password';
        }
        if (trim($request->getPostParameter('password')) != trim($request->getPostParameter('cpassword'))) {
            $msg[] = 'Your confirmed password does not match the entered password';
        }
        if (trim($request->getPostParameter('dob[month]')) == 0) {
            $msg[] = 'Please select month in Date of Birth';
        }
        if (trim($request->getPostParameter('dob[day]')) == 0) {
            $msg[] = 'Please select day in Date of Birth';
        }
        if (trim($request->getPostParameter('dob[year]')) == 0) {
            $msg[] = 'Please select year in Date of Birth';
        }
        if (trim($request->getPostParameter('email')) == "") {
            $msg[] = 'Please enter Email<br />';
        }// else if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $request->getPostParameter('email'))) {
        // $msg[]= 'Please enter Valid Email';
        //}
        if (trim($request->getPostParameter('address')) == "") {
            $msg[] = 'Please enter Address';
        } else
        if (trim($request->getPostParameter('address')) != "") {
            $len = strlen(trim($request->getPostParameter('address')));
            if ($len > 255) {
                $msg[] = 'Please enter valid address';
            }
        }
        if (trim($request->getPostParameter('mobileno')) == "") {
            $msg[] = 'Please enter Mobile number';
        }
        if (trim($request->getPostParameter('sms')) == "gsmno" || trim($request->getPostParameter('sms')) == "both") {
            if (trim($request->getPostParameter('mobileno')) == "") {
                $msg[] = 'Please enter Mobile No.';
            }
            if (trim($request->getPostParameter('mobileno')) != "") {
                //        if (!eregi("^([+]{1})([0-9]{10,15})$", $request->getPostParameter('mobileno'))) {
                //          $msg[]= 'Please enter Valid Mobile No.';
                //        }
            }
        }
        if (trim($request->getPostParameter('sms')) == "workphone" || trim($request->getPostParameter('sms')) == "both") {
            if (trim($request->getPostParameter('workphone')) == "") {
                $msg[] = 'Please enter Work Phone.';
            }
        }

        if (trim($request->getPostParameter('workphone')) != "") {///  ^(\+)(\d){10,14}?$/
            //      if (!eregi("^([+]{1})([0-9]{10,15})$", $request->getPostParameter('workphone'))) {
            //        $msg[]= 'Please enter Valid Work Phone.'.$request->getPostParameter('workphone')."---".$request->getPostParameter('mobileno');
            //      }
        }

        if (trim($request->getPostParameter('captcha')) == "") {
            $msg[] = 'Please enter Verification Code';
        }
        if (trim($request->getPostParameter('captcha')) != "") {
            if (trim($request->getPostParameter('captcha')) != $this->getUser()->getAttribute('captcha')) {
                $msg[] = 'Verification Code Doesn\'t match with entered code';
            }
        }
        if (count($msg) > 0) {
            $msg = implode($msg, ', ');
            $this->getUser()->setFlash('error', sprintf($msg));
            return false;
        }
        return true;
    }

    public function executeThanks(sfWebRequest $request) {
        sfView::SUCCESS;
    }

    public function executeChkUserAvailability(sfWebRequest $request) {

        $username = $request->getPostParameter('username');
        $username_count = Doctrine::getTable('sfGuardUser')->chk_username($username);

        if ($username_count == 0) {
            return $this->renderText("<font color='green'>Username Available</font>");
        } else {
            return $this->renderText("<font color='red'>Username not Available</font>");
        }
    }

    public function executeChangePassword(sfWebRequest $request) {
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        $this->setTemplate('change');
    }

    public function executeCalculateStrength() {
        $strength = PasswordStrength::calculate(
                        $this->getRequestParameter('password'),
                        $this->getRequestParameter('username'),
                        $this->getRequestParameter('byPass')
        );
        return $this->renderText(PasswordStrength::getBar($strength));
    }

    public function executeSendMail(sfWebRequest $request) {

        $this->setLayout(null);
        $mailingClass = new Mailing();

        $partialName = $request->getParameter('partialName');


        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $userid = $request->getParameter('id');
        $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($userid));
        $email = $sfGuardUserDetail->getUserDetail()->getEmail();
        $name = $sfGuardUserDetail->getUserDetail()->getFirstName();
        //$partialVars = $name;
        $signature = 'iPay4Me Team';
        $partialVars = array('name' => $name, 'signature' => $signature);

        $mailFrom = sfConfig::get('app_email_local_settings_username');
        $mailingOptions['mailSubject'] = $request->getParameter('subject');
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);


        //
        //        $mailingClass = new Mailing();
        //        $partialName = 'mailTemplate' ;
        //        $partialVars = array();
        //        $mailingOptions = array();
        //        $mailingOptions['mailFrom'] = "shuchi.sarpal@gmail.com";
        //        $mailingOptions['mailTo'] = "varunchandwani@gmail.com";
        //        $mailingOptions['mailSubject'] = "Test Mail" ;
        //        $mailingClass->sendMail($partialName,$partialVars,$mailingOptions);
        //        return $this->renderText($mailInfo);
        //$this->getMailer()->composeAndSend('varun@gmail.com', 'varun.chandwani@tekmindz.com', '$subject', '$body');
        //$this->setTemplate(false);
        //$this->setLayout(false);
    }

    public function executeSavechange(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        if ($this->processForm($request, $this->form, false)) {
            $this->getUser()->setFlash('notice', "Password Changed Successfully", false);
            $this->forward($this->getModuleName(), 'changePassword');
        }

        $this->setTemplate('change');
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true) {
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {
            $postArr = $request->getPostParameters();
            $sf_guard_user = $this->getUser()->getGuardUser();
            if (EpPasswordPolicyManager::checkPasswordComplexity($postArr['sf_guard_user']['password'])) {
                $resVal = EpPasswordPolicyManager::checkPreviousPasswords($sf_guard_user->getId(), $postArr['sf_guard_user']['password'], $sf_guard_user->getSalt(), $sf_guard_user->getAlgorithm());
                if ($resVal) {
                    $oldPasswordLimit = EpPasswordPolicyManager::getOldPasswordLimit();
                    $this->getUser()->setFlash('error', 'New password can not be same as last ' . $oldPasswordLimit . ' password.', true);

                    $this->redirect('signup/changePassword');
                } else {
                    $form->save();
                    $this->logUserChangePasswordAuditDetails($sf_guard_user->getUsername(), $sf_guard_user->getId());
                    $sf_guard_user = $this->getUser()->getGuardUser();
                    EpPasswordPolicyManager::doChangePassword($sf_guard_user->getId(), $sf_guard_user->getPassword());
                }
            } else {
                $this->getUser()->setFlash('error', ' Password should have atleast 1 special character, 1 numeric character and 1 alphabetic character.', true);
                // die("hi");
                $this->redirect('signup/changePassword');
            }
            return true;
        }
        return false;
    }

    public function executeUserList() {
        $this->status_list = array('admin' => 'Admin User', 'payment' => 'Payment User');
    }

    public function executeUserListing(sfWebRequest $request) {
        $email = $request->getParameter('email');
        $userType = $request->getParameter('userType');

        $userObj = Doctrine::getTable('sfGuardUser')->getAllUsers($userType, $email);

        $this->maxToBlock = sfConfig::get('app_number_of_failed_attempts_for_blocked_user');
        $this->page = 1;
        if ($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }

        $this->pager = new sfDoctrinePager('sfGuardUser', sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($userObj);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }

    public function setStatus($request) {
        $userId = $request->getParameter('id');
        $status = $request->getParameter('status');
        $sfUid = $request->getParameter('sfUid');
        if ($userId != '') {
            $user = Doctrine::getTable('UserDetail')->find($userId);
            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUid));
            // $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_IPAY4MEINFO,$sfGuardUserDetail->getUsername(),$sfGuardUserDetail->getId()));

            if ($status == "activate") {
                $user->setFailedAttempt(0);
                $user->save();

                #End Code To Send Mail To The eWallet Owner
                //resetting password
                $pass_word = PasswordHelper::generatePassword();
                //$sfGuardUserDetail->setIsActive(true);
                $sfGuardUserDetail->setPassword($pass_word);
                $sfGuardUserDetail->setLastLogin(NULL);
                $sfGuardUserDetail->save();

                #Generate Activation Token For Activation Link
                $hashAlgo = sfConfig::get('app_user_token_hash_algo');
                $userPwdSalt = $sfGuardUserDetail->getSalt();
                $updatedAt = $sfGuardUserDetail->getUpdatedAt();

                $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);


                $subject = "iPay4me Unblock Account Mail";
                $partialName = 'ipay4me_status';
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $activation_url = url_for('signup/activate', true) . '?i=' . $sfUid . '&t=' . $activationToken;
                $taskId = EpjobsContext::getInstance()->addJob('SendEwalletUnblockMail', "signup/sendActivationMail", array('userid' => $sfUid, 'subject' => $subject, 'partialName' => $partialName, 'activation_url' => $activation_url, 'password' => $pass_word));
                $this->logMessage("sceduled mail job with id: $taskId", 'debug');

                $this->blockUnblockUsersAuditDetails($sfGuardUserDetail->getUsername(), $sfUid, EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_UNBLOCKED, EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_UNBLOCKED);
                #End Code To Send Mail To The eWallet Owner
            }
            if ($status == "deactivate") {
                $user->setFailedAttempt(sfConfig::get('app_number_of_failed_attempts_for_blocked_user'));
                $user->save();
                $status_msg = "Deactivated";
                $sfGuardUserDetail->setIsActive(false);
                $sfGuardUserDetail->save();
                $this->blockUnblockUsersAuditDetails($sfGuardUserDetail->getUsername(), $sfUid, EpAuditEvent::$SUBCATEGORY_TRANSACTION_USER_BLOCKED, EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED);
            }

            //$this->getUser()->setFlash('notice', sprintf('User '.$status_msg.' successfully'),TRUE);
            //$this->redirect('report/ewalletUserList');
        }
    }

    public function executeActivate(sfWebRequest $request) {
        // $account_number = $this->generateAccountNumber();
        # Initilize Variables
        $userid = $request->getParameter('i');
        $userToken = $request->getParameter('t');
        $user_details = Doctrine::getTable('sfGuardUser')->find(array($userid));
        $last_login = $user_details->getLastLogin();
        $sfguardUserId = $user_details->getId();
        $userPwdSalt = $user_details->getSalt();
        $updatedAt = $user_details->getUpdatedAt();

        #Generate Activation Token For Activation Link
        $hashAlgo = sfConfig::get('app_user_token_hash_algo');
        //$userPwdSalt = $user_details->getSalt();
        //$updatedAt = $user_details->getUpdatedAt();


        $activationToken = Doctrine::getTable('sfGuardUser')->getActivationToken($hashAlgo, $userPwdSalt, $updatedAt);

        // $detailObj = $user_details->getUserDetail()->getFirst();
        //check last login
        //    if($last_login == ""){
        //      //check is user having account id if yes it means user is blocked, and came for unblock, else assigin user an account id
        //      $accNo = $detailObj->getMasterAccountId();
        //      if(empty($accNo)){
        //        //create wallet for user with 0 balance
        //        $walletObj = new EpAccountingManager;
        //        $account_name = $detailObj->getName()." ".$detailObj->getLastName();
        //        $type = "ewallet";
        //        $account_number = $this->generateAccountNumber();
        //        $master_account_id =   $walletObj->createMasterAccount($account_name, $account_number, $type);
        //        if(isset($master_account_id) && ($master_account_id!="")){
        //          $detailObj->setMasterAccountId($master_account_id);
        //        }
        //      }
        //    }
        if ($userid == $sfguardUserId && $userToken == $activationToken) {
            $user_details->setIsActive(true);
            //if user having account id , it means user came for unblock, so we put last login in conditions.
            // by that when user go for login , it will be redirected to change password and security answers
            if (empty($accNo))
                $user_details->setLastLogin(date('Y-m-d h:i:s'));
            $user_details->save();
            $this->getUser()->setFlash('notice', sprintf('Your account activated successfully.'));
            $this->redirect('pages/index');
        }else {
            $this->redirect('pages/index');
        }
    }

    public function executeSendActivationMail(sfWebRequest $request) {
        $mailingClass = new Mailing();
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $activation_url = $request->getParameter('activation_url');
        if ($request->getParameter('password')) {
            $password = $request->getParameter('password');
        } else {
            $password = '';
        }

        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';

        $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($userid));
        $email = $sfGuardUserDetail->getUserDetail()->getEmail();
        $name = $sfGuardUserDetail->getUserDetail()->getFirstName();
        //$partialVars = $name;
        $signature = 'iPay4Me Team';
        $partialVars = array('name' => $name, 'signature' => $signature, 'url' => $activation_url, 'password' => $password);

        $mailFrom = sfConfig::get('app_email_local_settings_username');
        $mailingOptions['mailSubject'] = $subject;
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);
    }

    public function logUserChangePasswordAuditDetails($uname, $id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE, array('username' => $uname)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function blockUnblockUsersAuditDetails($uname, $id, $cat, $msg) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_SECURITY,
                        $cat,
                        EpAuditEvent::getFomattedMessage($msg, array('username' => $uname)),
                        null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
}