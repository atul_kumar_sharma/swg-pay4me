<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class paymentProcessActions extends sfActions {

    public function executeOrderDetails(sfWebRequest $request) {
        $this->err = "";
        try {
            if ($request->hasParameter('order')) {
                //decode the order number; which is combination of txn_ref:id of merchant_request table
                if ($request->getParameter('order') != "") {
                    $order = base64_decode($request->getParameter('order'));
                    if (strpos($order, ":") !== false) {
                        $order_details = explode(":", $order);
                        $this->getUser()->setAttribute('loginOnOrderRequest', true);

                        $requestId = $order_details['1']; //get id of merchant_request
                        //SETTING REQUEST ID SESSION
                        $this->getUser()->getAttributeHolder()->remove('requestId');
                        $this->getUser()->setAttribute('requestId', $requestId);
                        //fetch details
                        //   $OrderRequestTableObj = OrderRequestDetailsTable::getInstance();
                        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                        /**
                         * [WP: 091] => CR: 131 
                         */
                        $posPayment = $this->getUser()->getAttribute('posPayment');                        
                        if(1 == $posPayment && isset($posPayment)){

                            $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
                            if (count($appDetails) == 1) {
                                $this->application_type = strtolower($appDetails[0]['app_type']);
                                if ($this->application_type == 'vap') {
                                    $app = Doctrine::getTable('VapApplication')->find($appDetails[0]['id']);
                                    $appFee = $app->getDollarFee();
                                    $this->updateOrderRequestAmount($requestId, $appFee);
                                    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                                    
                                    ## Generating bill
                                    $billObj = new billManager();
                                    $billObj->addBill($this->request_details->getOrderRequestId());

                                    $this->redirect('configurationManager/sendPOSPaymentNotify?requestId='.$requestId);
                                    exit;
                                }
                            }
                        }

                        /**
                         * [WP: 099] => CR: 141
                         */
                        $oisPayment =  $request->getParameter('oisPayment');
                        $transactionNumber =  $request->getParameter('transactionNumber');
                        if(1 == $oisPayment && isset($oisPayment)){

                            $this->setLayout(false);
                            //$this->setTemplate(false);
                            

                            $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
                            if (count($appDetails) == 1) {
                                $this->application_type = strtolower($appDetails[0]['app_type']);
                                if ($this->application_type == 'visa') {
                                    $app = Doctrine::getTable('VisaApplication')->find($appDetails[0]['id']);
                                    $appFee = $app->getDollarFee();
                                    $this->updateOrderRequestAmount($requestId, $appFee);
                                    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

                                    ## Generating bill
                                    $billObj = new billManager();
                                    $billObj->addOISBill($this->request_details->getOrderRequestId());
                                    //$request->setParameter('requestId', $requestId);
                                    //request->setParameter('transactionNumber', $transactionNumber);
                                    
                                    //$this->forward('services', 'sendOISPaymentNotify');
                                    $this->redirect('services/sendOISPaymentNotify?requestId='.$requestId.'&transactionNumber='.$transactionNumber);
                                   
                                }
                            }
                            return sfView::NONE;
                            exit;
                        }
                        
                        ## Getting visa additional charges flag...
                        $visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');
                        
                        //$this->request_details = Doctrine::getTable('OrderRequest')->find($requestId);
                        if ($this->request_details) {

                            ## setting default application type...
                            $this->application_type = '';                         

                            $user = $this->getUser();
                            if ($user->isAuthenticated()) {
                                sfContext::getInstance()->getUser();

                                $getUrl = $request->getUri();
                                $isUserBlocked = $this->getUser()->getGuarduser()->getUserDetail()->getPaymentRights();
                                if ($isUserBlocked == 0) {
                                    $groupArray = sfContext::getInstance()->getUser()->getGroupNames();
                                    if (!in_array(sfConfig::get('app_ipfm_role_payment_user'), $groupArray)) {
                                        $sessId = session_id();
                                        Doctrine::getTable('AppSessions')->clearUserSessionBySessionId($sessId);
                                        $this->redirect($getUrl);
                                    }

                                    $arrCartItems = $this->getUser()->getCartItems();
                                    
                                    /**
                                     * [WP: 087] => CR:126
                                     * Fetching processing country...
                                     */
                                    $paymentOptions = new paymentOptions();
                                    $processingCountry = $paymentOptions->getProcessingCountry();
                                    

                                    //check if cart empty
                                    //add application if coming from NIS
                                    //get Application on NIS
                                    $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());                                    
                                    $errorMsgObj = new ErrorMsg();
                                    $arrItemId = array();
                                    $cartAppIds = array();
                                    if (count($appDetails) > 1) { //When redirected from swgloballlc

                                        /**
                                         * [WP: 087] => CR: 126
                                         * Updating order request and order request details table with updating amount...                                         
                                         */                                       
                                        
                                        // *  Commented by aswani as all tracking checks are moving to when user try to generate tracking number... *
                                        $amount = 0;
                                        foreach ($appDetails as $value) {
                                            if ($value['app_type'] == 'passport') {
                                                $application_type = 'p';
                                                $tmpAmount = currencyManager::getConvertedAmount(Functions::getApplicationFees($value['id'], $value['app_type']), 1, 4);
                                                $amount = $amount + $tmpAmount;
                                            } else if ($value['app_type'] == 'visa') {
                                                $application_type = 'v';                                                
                                                if($visa_additional_charges_flag){
                                                    $additionalCharges = sfConfig::get('app_visa_additional_charges');
                                                    $appAmountArray = Functions::getConvertedVisaAndAdditionalFees($value['id'], $value['app_type'], $additionalCharges);                                                    
                                                    if(count($appAmountArray) > 0){
                                                        $amount = $amount + $appAmountArray['appAmount'] + $appAmountArray['additional_charges'];
                                                    }
                                                }else{
                                                    $tmpAmount = currencyManager::getConvertedAmount(Functions::getApplicationFees($value['id'], $value['app_type']), 1, 4);
                                                    $amount = $amount + $tmpAmount;
                                                    
                                                }
                                            } else if ($value['app_type'] == 'freezone') { 
                                                $application_type = 'f';

                                                $isVisaEntryFreezone = Functions::isVisaEntryFreezone($value['id']);
                                                if($isVisaEntryFreezone){
                                                    if($visa_additional_charges_flag){
                                                        $additionalCharges = sfConfig::get('app_visa_additional_charges');
                                                        $appAmountArray = Functions::getConvertedVisaAndAdditionalFees($value['id'], $value['app_type'], $additionalCharges);
                                                        if(count($appAmountArray) > 0){
                                                            $amount = $amount + $appAmountArray['appAmount'] + $appAmountArray['additional_charges'];
                                                        }
                                                    }
                                                }
                                            } else if ($value['app_type'] == 'vap') {
                                                $application_type = 'vap';
                                            }
                                            $cartAppIds[] = $value['id'];
                                            $paymentRequestRecord = Doctrine::getTable('CartItemsTransactions')->getPaymentRequestMoneyOrder($value['id'], $application_type);

                                            foreach ($paymentRequestRecord as $key => $items) {
                                                $arrItemId[$key] = $items['id'];
                                            }
                                        }
                                        
                                        if($amount > 0){                                            
                                            $this->updateMultipleApplicationAmount($requestId, $processingCountry, $amount);
                                        }

                                        if (isset($arrItemId)) {

                                            $cartItemInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemAllIdInfo($arrItemId);

                                            foreach ($cartItemInfo as $key => $item) {
                                                $arrCartId[$key] = $item['cart_id'];
                                            }



                                            
                                            if (isset($arrCartId)) {
                                                $cartTrakingInfo = Doctrine::getTable('WireTrackingNumber')->getTrackingNumber($arrCartId);

                                                if (empty($cartTrakingInfo)) {
                                                    $cartTrakingInfo = Doctrine::getTable('CartTrackingNumber')->getTrackingNumber($arrCartId);
                                                }
                                            }
                                        }
                                        

                                        if (!empty($cartTrakingInfo) && count($cartTrakingInfo) > 0) {

                                              
                                            $cartId = $cartTrakingInfo[0]['cart_id']; 

                                            $fields = 'item_id AS item_id';
                                            $cartTrakingInfo = Doctrine::getTable('CartItemsTransactions')->getCartItemInfoByCartId($cartId, $fields);
                                            $cartTrakingInfoCount = count($cartTrakingInfo);

                                            for ($j = 0; $j < $cartTrakingInfoCount; $j++) {
                                                $item_id = $cartTrakingInfo[$j]['item_id'];
                                                if ($item_id != '') {
                                                    $appIdDetails = Doctrine::getTable('IPaymentRequest')->getAppId($item_id);
                                                    if (empty($appIdDetails))
                                                        continue;

                                                    $appDetails = array(); /* Bug id: 29837 */
                                                    if ($appIdDetails[0]['passport_id'] != '') {
                                                        $appDetails[] = $appIdDetails[0]['passport_id'];
                                                    } else if ($appIdDetails[0]['visa_id'] != '') {
                                                        $appDetails[] = $appIdDetails[0]['visa_id'];
                                                    } else if ($appIdDetails[0]['freezone_id'] != '') {
                                                        $appDetails[] = $appIdDetails[0]['freezone_id'];
                                                    } else if ($appIdDetails[0]['visa_arrival_program_id'] != '') {
                                                        $appDetails[] = $appIdDetails[0]['visa_arrival_program_id'];
                                                    }
                                                }//End of if($item_id != ''){...
                                            }//End of for($j=0;$j<$cartTrakingInfoCount;$j++){...


                                             

                                            $strMsg = '';
                                            foreach($appDetails AS $key => $value){
                                                 if(in_array($value, $cartAppIds)){                                                     
                                                     $strMsg .= $value . ' ,';
                                                 }
                                             }

                                            if($strMsg != ''){
                                                $this->getUser()->setFlash('notice', 'Application Id(s): ' . substr($strMsg, 0, -1) . 'has been already associated with money order. Please click remove link to remove this application from cart.');
                                                $this->redirect('cart/list');
                                            }else{
                                                $cartItems = $this->getUser()->clearCart();
                                                $userDetailObj = $this->getUser()->getGuardUser()->getUserDetail();
                                                $userDetailObj->setCartItems(NULL);
                                                $userDetailObj->save();
                                                $this->getUser()->setFlash('notice', 'Application(s) has been already associated with money order. ');
                                                $this->redirect('cart/list');
                                            }
                                        }                                        
                                         
                                    } else if (count($appDetails) == 1) { // When redirected from NIS or swgloballlc...

                                        $application_type = $appDetails[0]['app_type'];

                                        /**
                                         * [WP: 087] => CR:126
                                         * Getting application fees...
                                         */
                                        $appAmount = Functions::getApplicationFees($appDetails[0]['id'], $application_type);
                                        
                                        if ($application_type == 'passport') {
                                            $application_type = 'p';                                            
                                            ## Updating application amount...
                                            $this->updateApplicationAmount($requestId, $processingCountry, $appAmount, 1);
                                        } else if ($application_type == 'visa') {
                                            $application_type = 'v';                                            
                                            $serviceCharge = 0;
                                            if($visa_additional_charges_flag){
                                                ## Updating amount with addtional charges in order request deatils and order request table...
                                                $this->updateOrderRequestAmount($requestId, $appAmount);
                                                $appAmount = Functions::getVisaApplicationFeesWithoutAdditionalCharges($appDetails[0]['id'], $application_type);
                                                $serviceCharge = sfConfig::get('app_visa_additional_charges');
                                            }
                                            ## Updating application amount if currency differ from dollar...
                                            $this->updateApplicationAmount($requestId, $processingCountry, $appAmount, 1, $serviceCharge);
                                            $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);

                                        } else if ($application_type == 'freezone') {
                                            $application_type = 'v';

                                            /**
                                             * [WP: 102] => CR: 144
                                             * If application is entry freezone means visacategoryid is 101 then update tables...
                                             */
                                            if($visa_additional_charges_flag){
                                                $isVisaEntryFreezone = Functions::isVisaEntryFreezone($appDetails[0]['id']);
                                                if($isVisaEntryFreezone){
                                                    ## Updating amount with addtional charges in order request deatils and order request table...
                                                    $this->updateOrderRequestAmount($requestId, $appAmount);

                                                    $appAmount = Functions::getVisaApplicationFeesWithoutAdditionalCharges($appDetails[0]['id'], $application_type);
                                                    $serviceCharge = sfConfig::get('app_visa_additional_charges');

                                                    ## Updating application amount if currency differ from dollar...
                                                    $this->updateApplicationAmount($requestId, $processingCountry, $appAmount, 1, $serviceCharge);
                                                    $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                                                }
                                            }                                            

                                        } else if ($application_type == 'vap') {
                                            $application_type = 'vap';
                                            $this->application_type = $application_type;

                                            ## Updating amount with addtional charges in order request deatils and order request table...
                                            $app = Doctrine::getTable('VapApplication')->find($appDetails[0]['id']);
                                            $appFee = $app->getDollarFee();
                                            $this->updateOrderRequestAmount($requestId, $appFee);
                                            $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                                            
                                        }
                                        $application_id = $appDetails[0]['id'];


                                        ## If application is already processed for refund or chargeback
                                        $isRefunded = RollbackPaymentDetailsTable::getInstance()->isRefunded($application_id, $application_type);                                        
                                        if (count($isRefunded) > 0) {
                                            $errorMsg = $errorMsgObj->displayErrorMessage("E049", '001000', array('isRefunded' => $isRefunded[0]['action']));
                                            $this->getUser()->setFlash('notice', $errorMsg);
                                            $this->redirect('cart/list');
                                        }
                                        

                                        $cartObj = new cartManager();

                                        /**
                                         * [WP: 102] => CR: 144
                                         * Clear cart while coming from nis merchant...
                                         */
                                        //$this->getUser()->clearCart();
                                        
                                        $arrCartItems = $this->getUser()->getCartItems();
                                        
                                        if (count($arrCartItems) == 0) {
                                            $addToCart = $cartObj->addToCart($application_type, $application_id);
                                        } else {
                                            $flg = false;
                                            foreach ($arrCartItems as $items) {
                                                if ($items->getAppId() != $application_id) {
                                                    $flg = true;
                                                }
                                                if ($flg) {
                                                    $cartValidationObj = new CartValidation();                                                    
                                                    $addToCart = $cartObj->addToCart($application_type, $application_id);
                                                    if ($addToCart == 'common_opt_not_available') {
                                                        $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_add"), true);
                                                        $this->redirect('cart/list?#msg');
                                                    } else if ($addToCart == 'capacity_full') {
                                                        //start:Kuldeep for check cart capacity
                                                        $arrCartCapacity = array();
                                                        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                                                        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                                            $arrCartCapacity = $arrCartCapacityLimit;
                                                        }
                                                        //end:Kuldeep
                                                        $msg = $arrCartCapacity['cart_capacity'] . " application";
                                                        $errorMsg = $errorMsgObj->displayErrorMessage("E047", '001000', array("msg" => $msg));
                                                        $this->getUser()->setFlash('notice', $errorMsg);
                                                        $this->redirect('cart/list');
                                                    } else if ($addToCart == 'amount_capacity_full') {
                                                        //start:Kuldeep for check cart capacity
                                                        $arrCartCapacity = array();
                                                        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                                                        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                                                            $arrCartCapacity = $arrCartCapacityLimit;
                                                        }
                                                        //end:Kuldeep
                                                        $errorMsg = $errorMsgObj->displayErrorMessage("E048", '001000', array("msg" => $arrCartCapacity['cart_amount_capacity']));
                                                        $this->getUser()->setFlash("notice", $errorMsg);
                                                        $this->redirect('cart/list');
                                                    } else if ($addToCart == "application_edit") {
                                                        $errorMsg = $errorMsgObj->displayErrorMessage("E058", '001000', array("msg" => ''));
                                                        $this->getUser()->setFlash("notice", $errorMsg);
                                                    }else if ($addToCart == "application_rejected") {
                                                        $errorMsg = $errorMsgObj->displayErrorMessage("E060", '001000', array("msg" => ''));
                                                        $this->getUser()->setFlash("notice", $errorMsg);
                                                    } else if($addToCart == "only_vap_allowed"){

                                                        if(strtolower($appDetails[0]['app_type']) != 'vap'){
                                                            $msg = ucfirst($appDetails[0]['app_type']).' application can not be added into the cart with Visa on Arrival application.';
                                                        }else{
                                                            $msg = 'Visa on Arrival application can not be added into the cart with another type of application such as passport, visa, .. etc.';
                                                        }
                                                        $this->getUser()->setFlash('notice', sprintf($msg));
                                                        $this->redirect('cart/list');
                                                    }
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    


                                    //generating bill
                                    $billObj = new billManager();
                                    $billObj->addBill($this->request_details->getOrderRequestId());
                                    $this->redirect('paymentGateway/gotoApplicantVault');

                                } else {
                                    $this->redirect('paymentProcess/blockUserMsg');
                                }
                            }else{
                                
                                $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($this->request_details->getRetryId());
                                /**
                                 * [WP: 104] => CR: 149
                                 * Setting flag true for jna service to change address defined in bottom of the page...
                                 */
                                $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails[0]['id'], $appDetails[0]['app_type']);
                                $sess_jna_processing = Functions::setFooterAddressFlag($processingCountry);
                                $this->getUser()->setAttribute('sess_jna_processing', $sess_jna_processing);
                                if (count($appDetails) == 1) {
                                    $this->application_type = strtolower($appDetails[0]['app_type']);
                                    if ($this->application_type == 'vap') {
                                        $app = Doctrine::getTable('VapApplication')->find($appDetails[0]['id']);
                                        $appFee = $app->getDollarFee();
                                        $this->updateOrderRequestAmount($requestId, $appFee);
                                        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                                    }else{
                                        /**
                                         * [WP: 087] => CR:126
                                         * Updating application amount in case of passport or visa and application processing country is china...
                                         */                                        
                                        if ($this->application_type == 'visa' || $this->application_type == 'passport' || $this->application_type == 'freezone' ) {
                                            $appAmount = Functions::getApplicationFees($appDetails[0]['id'], $appDetails[0]['app_type']);
                                            $app_visa_additional_charges = 0;

                                            /**
                                             * [WP: 102] => CR: 144
                                             * Updating amount in order request detail table...
                                             */
                                            $visa_additional_charges_applied = false;
                                            $isVisaEntryFreezone = Functions::isVisaEntryFreezone($appDetails[0]['id']);
                                            if($isVisaEntryFreezone){
                                                $visa_additional_charges_applied = true;
                                            }else{
                                                if($this->application_type == 'visa'){
                                                    $visa_additional_charges_applied = true;
                                                }
                                            }                                             
                                            if($visa_additional_charges_flag && $visa_additional_charges_applied){
                                                $this->updateOrderRequestAmount($requestId, $appAmount);
                                                $appAmount = Functions::getVisaApplicationFeesWithoutAdditionalCharges($appDetails[0]['id'], $appDetails[0]['app_type']);
                                                $app_visa_additional_charges = sfConfig::get('app_visa_additional_charges');
                                            }

                                            $processingCountry = Functions::getProcessingCountryByAppIdAndType($appDetails[0]['id'], $appDetails[0]['app_type']);                                           
                                            $this->updateApplicationAmount($requestId, $processingCountry, $appAmount, 1, $app_visa_additional_charges);
                                        }                                        
                                    }
                                }
                        }
                        }else {
                            $this->logMessage("Order Request Id has been tampered with!!");
                            throw new Exception("Trying to search with a order request that does not exist!!");
                        }
                    } else {
                        throw New Exception("Invalid Order");
                    }
                } else {
                    throw New Exception("Value of order cannot be empty!!");
                }
            } else {
                throw New Exception("Mandatory Parameter Order not found!!");
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeInvalidAttempt(sfWebRequest $request) {
        $this->pageTitle = 'Invalid Payment';
    }

    public function executeOrderConfirmation(sfWebRequest $request) {
        $this->err = "";
        try {

            $this->form = new OrderConfirmationForm();
            if ($request->hasParameter('requestId') && ($request->getParameter('requestId') != "")) {
                $requestId = $request->getParameter('requestId');
                $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                $this->requestId = $requestId;


//REMOVING REQUEST ID SESSION
                $this->getUser()->getAttributeHolder()->remove('requestId');
                if ($this->request_details) {
                    $version = $this->request_details->getOrderRequest()->getVersion();
                } else {
                    $this->logMessage("Order Request Id has been tampered with!!");
                    throw new Exception("Trying to search unknown order !");
                }
            } else {
                throw New Exception("Invalid Order Request Id");
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
        }
    }

    public function executeTestAuthorize(sfWebRequest $request) {}

    public function executeAuthorizePayment(sfWebRequest $request) {

        $card_num = $request->getPostParameter("card_num");
        $expiry_date = $request->getPostParameter("expiry_date");
        $amount = $request->getPostParameter("amount");
        $cvv = $request->getPostParameter("cvv");
        $param = array();
        $param['x_invoice_num'] = $request->getPostParameter("transaction_num");
        $param['x_description'] = $request->getPostParameter("description");

        $gateway = "authorize";
        $paymentGatewayObj = PaymentGatewayServiceFactory::getService($gateway);
        $paymentResponse = $paymentGatewayObj->paymentRequest($card_num, $expiry_date, $amount, $cvv, $param);

//      print_r($paymentResponse) ;


        $this->paymentResponse = $paymentResponse;
    }

    public function executePaymentRequest(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod('post'));

        $this->err = "";
        try {
            if ($request->isMethod('post')) {  //echo "<pre>"; print_r($request->getPostParameters()); die;
                $this->form = new OrderConfirmationForm();
                $this->processForm($request, $this->form);

                $requestId = $request->getParameter('requestId');
                $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                $this->requestId = $requestId;
                $this->setTemplate('orderConfirmation');
            }
        } catch (Exception $e) {
            $this->err = $e->getMessage();
            $this->result = "error";
            $this->setTemplate('thankYou');
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter('cardDetails'));

        if ($form->isValid()) {
            $cardDetails = $request->getParameter('cardDetails');
            if (($cardDetails['card_num']) && ($cardDetails['month']) && ($cardDetails['year']) && ($request->hasParameter('card_type')) && ($cardDetails['cvv_number'])) {
                if (($cardDetails['card_num'] != "") && ($cardDetails['month'] != "") && ($cardDetails['year'] != "") && ($request->getParameter('card_type') != "") && ($cardDetails['cvv_number'] != "")) {
                    if (($request->hasParameter('requestId')) && ($request->getParameter('requestId') != "")) {
                        $requestId = $request->getParameter('requestId');
                        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                        if ($request_details) {
                            $version = $request_details->getOrderRequest()->getVersion();
                            $payment_status = $request_details->getPaymentStatus();
                            if ($payment_status != 0) {
                                $pay4MeIntObj = Pay4MeIntServiceFactory::getService($version);
                                $card_num = $cardDetails['card_num'];
                                $month = $cardDetails['month'];
                                $year = $cardDetails['year'];
                                $card_type = $request->getParameter('card_type');
                                $cvv_number = $cardDetails['cvv_number'];
                                try {
                                    $vumber = $pay4MeIntObj->saveOrderRequest($requestId, $card_num, $month, $year, $card_type, $cvv_number);
                                    $result = "success";

                                    //remove session if exists and set the thanks page data session
                                    $this->getUser()->getAttributeHolder()->remove('result');
                                    $this->getUser()->getAttributeHolder()->remove('vumber');

                                    $this->getUser()->setAttribute('result', $result);
                                    $this->getUser()->setAttribute('vumber', $vumber);

                                    $this->redirect('paymentProcess/thankYou');
                                } catch (Exception $e) {
                                    throw New Exception($e->getMessage());
                                }
                            } else {
                                $result = "invalid";
                                //remove session if exists and set the thanks page data session
                                $this->getUser()->getAttributeHolder()->remove('result');

                                $this->getUser()->setAttribute('result', $result);
                            }
                        } else {
                            $this->logMessage("Merchant Request Id has been tampered with!!");
                            throw new Exception("Trying to search with a merchant request that does not exist!!");
                        }
                    } else {
                        throw New Exception("Merchant Request Id required");
                    }
                } else {
                    throw New Exception("Please ensure that the values of Card Number, Month, Year, Card Type and CVV Number are not empty!!");
                }
            } else {
                throw New Exception("Mandatory Parameters missing!!");
            }
        }
    }

    public function executeThankYou(sfWebRequest $request) {
        $this->result = $this->getUser()->getAttribute('result', 0);
        $this->vumber = $this->getUser()->getAttribute('vumber', 0);
        //removing session
        $this->getUser()->getAttributeHolder()->remove('result');
        $this->getUser()->getAttributeHolder()->remove('vumber');
    }

    public function executeBlockUserMsg() {
        $this->msg = "Your transaction rights has been blocked by the admin. If you are attempting to make a payment from another ID and are not able to log-in then please first log-out from the service from which you are attempting to Log-in. Please contact us at ";
    }

    /**
     *
     * @param <type> $requestId
     * @param <type> $appFee
     * Updating amount in case of application is visa on arrival...
     * This function is made due to user coming from NIS end.
     * $currency = 1 means dollar...
     */
    private function updateOrderRequestAmount($requestId, $appFee, $currency = 1){
        $orderRequestDetailsObj = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        if(!empty($orderRequestDetailsObj)){
            if($currency == 1){
                $orderRequestDetailsObj->setAmount($appFee);
            }else{
                $orderRequestDetailsObj->setConvertAmount($appFee);
            }
            $orderRequestDetailsObj->setCurrency($currency);
            $orderRequestDetailsObj->save();

            $orderRequestObj = Doctrine::getTable('OrderRequest')->find($orderRequestDetailsObj->getOrderRequestId());
            if(!empty($orderRequestObj)){
                if($currency == 1){
                    $orderRequestObj->setAmount($appFee);
                }else{
                    $orderRequestObj->setConvertAmount($appFee);
                }
                $orderRequestObj->setCurrency($currency);
                $orderRequestObj->save();
            }//End of if(!empty($orderRequestObj)){...
        }//End of if(!empty($orderRequestDetailsObj)){...
    }//End of private function updateOrderRequestAmount($requestId, $appFee){...

    /**
     * [WP: 102] => CR: 144
     * @param <type> $requestId
     * @param <type> $processingCountry
     * @param <type> $appFee
     * @param <type> $totalApp
     * @param <type> $serviceCharge 
     */
    private function updateApplicationAmount($requestId, $processingCountry, $appFee, $totalApp, $serviceCharge = 0 ){

        $from_currency = 1; //dollar
        $to_currency = currencyManager::getDestinationCurrency($processingCountry);
        $appAmount = currencyManager::getConvertedAmount($appFee, $from_currency, $to_currency, $totalApp);
        
        if($serviceCharge > 0){
            $serviceCharge = currencyManager::getConvertedAmount($serviceCharge, $from_currency, $to_currency, $totalApp);
        }

        $appAmount = $appAmount + $serviceCharge;
        
        if($from_currency != $to_currency){
            $this->updateOrderRequestAmount($requestId, $appAmount, $to_currency);
        }
    }

    /**
     * [WP: 102] => CR: 144
     * @param <type> $requestId
     * @param <type> $processingCountry
     * @param <type> $appFee
     * Update order request detail table...
     */
    private function updateMultipleApplicationAmount($requestId, $processingCountry, $appFee ){
        $from_currency = 1; //dollar
        $to_currency = currencyManager::getDestinationCurrency($processingCountry);        
        if($from_currency != $to_currency){
            $this->updateOrderRequestAmount($requestId, $appFee, $to_currency);
        }
    }
    
}
?>
