<div class="clear">&nbsp;</div>
<div id="notice"><span class="red"><b>DO NOT REFRESH OR USE BROWSER BACK/PREVIOUS CONTROLS WHILE YOU ARE ON THIS PAGE.</b></span></div><br />
<table width="100%">
  <tr>
    <td class="blbar" colspan="3">Order Details</td>
  </tr>
  <tr>


    <td width="77%" class="txtBold">Item</td>
    <td width="23%" class="txtBold txtRight">Price</td>
  </tr>
  <tr>

    <td ><span class="txtBold"><?php  echo $request_details->getDescription();?> </span></td>

    <td class="txtRight">
        <?php echo format_amount($request_details->getAmount(),1);?>
        <?php if($application_type == 'vap'){ ?>
        <?php echo '<i><p style="font-size:10px;text-align:right">(Inclusive service charge: $'.sfConfig::get('app_visa_arrival_additional_charges').')</p></i>'; ?>
        <?php } ?>
    </td>
  </tr>
  <tr class="blTxt">

    <td class="txtBold">&nbsp;</td>
    <td class="txtRight blTxt"><span class="bigTxt">Total: <?php echo format_amount($request_details->getAmount(),1);?></span></td>
  </tr>
</table>
