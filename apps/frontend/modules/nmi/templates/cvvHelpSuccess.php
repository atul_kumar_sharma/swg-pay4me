<div class="brdBox">
<table width="600" border="0" cellpadding="0" cellspacing="0" class="maintitle">
  <tr>
    <td><b>CVV</b> is an anti-fraud security feature to help verify that you
      are in possession of your credit card. For Visa/Mastercard/Discover, the three-digit
      CVV number is printed on the signature panel on the back of the card immediately
      after the card's account number For American Express, the four-digit CVV
      number is printed on the front of the card above the
card account number. <br>

<br>
<div align="center">
<table class="white">
  <tbody>
    <tr>
      <td class="blbar"><b>Visa/Mastercard/Discover</b></td>
      <td class="blbar"><b>American Express</b></td>
    </tr>
    <tr>
      <td valign=top>
      <?php
            $visacvv = public_path('/images/',true).'visacvv.png';
            echo image_tag($visacvv, array('title'=>'Visa CVV')) ?>
      <br>

          <small>A 3-digit number in reverse italics<br>
          on the <b>back</b> of your credit card</small></td>
      <td valign=top>
      <?php
            $amexcvv = public_path('/images/',true).'amexcvv.png';
            echo image_tag($amexcvv, array('title'=>'Amex CVV')) ?>
      <br>
          <small>A 4-digit number on the <b>front</b>, just<br>
          above your credit card number</small></td>

    </tr>
  </tbody>
</table>
</div>
  </tr>
</table>
</div>
<div class="clear">&nbsp;</div>
<div align="center"><a href="javascript:window.close()" class="normalbutton">Close this window</a> </td></div>