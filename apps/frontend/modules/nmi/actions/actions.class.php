<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class nmiActions extends sfActions {

    public static $version = 'V1';

    /** function executePayment(sfWebRequest $request)
     * @purpose : This action will display payment form of NMI gateway
     * @param : (sfWebRequest $request)
     * @return :  Displays payment form
     * @author :
     * @date :
     */
    public function executePayment(sfWebRequest $request) {

        /**
         * WP:075] => CR:124
         * Fetching request id from session...         *
         */
        $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');

        ## decryp payment mode from query parameter...
       $paymentMode = Settings::decryptInput($request->getParameter('paymentMode'));

        /**
         * [WP: 104] => CR: 149
         * Setting Payment Mode for footer address if jna nmi vbv exist else remove session...
         */
        $paymentModeArray = array('10,jna_nmi_vbv','10,jna_nmi_mcs');
        if(in_array($paymentMode, $paymentModeArray)){
            $this->getUser()->setAttribute('sess_paymentMode_for_footer', $paymentMode);
        }else{
            $this->getUser()->getAttributeHolder()->remove('sess_paymentMode_for_footer');
        }

        $mode = $request->getParameter('mode');
        //get cart item for checking if application is already paid
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
            $this->redirect('cart/list');
        }
        $paymentModsArr = $this->getUser()->getAttribute('SesPaymentModsArr');
        $paymentModsArr = array_values($paymentModsArr);
        if (!isset($mode) && empty($mode)) {
            ## Following code will provide payment mode which contains jna_nmi
            $arrPaymentGateway = $this->getPaymentGatewayId();
            $activePaymentGatewayId = $arrPaymentGateway['gatewayId'];
        }



        $nmiCheckArray = array();
        $paymentModsArrCount = count($paymentModsArr);
        if ($paymentModsArrCount > 0) {
            for ($i = 0; $i < $paymentModsArrCount; $i++) {
                for ($j = 0; $j < count($paymentModsArr[$i]['card_detail']); $j++) {
                    if ($paymentModsArr[$i]['card_detail'][$j]['card_type'] == "nmi_mcs" || $paymentModsArr[$i]['card_detail'][$j]['card_type'] == "nmi_vbv" || $paymentModsArr[$i]['card_detail'][$j]['card_type'] == "nmi_amx" || $paymentModsArr[$i]['card_detail'][$j]['card_type'] == "jna_nmi_vbv" || $paymentModsArr[$i]['card_detail'][$j]['card_type'] == "jna_nmi_mcs") {
                        if (isset($activePaymentGatewayId) && !empty($activePaymentGatewayId)) {
                            $nmiCheckArray[] = $activePaymentGatewayId . ',' . $paymentModsArr[$i]['card_detail'][$j]['card_type'];
                        } else {
                            $nmiCheckArray[] = $paymentModsArr[$i]['card_detail'][$j]['gateway_id'] . ',' . $paymentModsArr[$i]['card_detail'][$j]['card_type'];
                        }
                    }
                }//END of for($j=0;$j<count($paymentModsArr[$i]['card_detail']);$j++){
            }//End of for($i=0;$i<$paymentOptionsArrCount;$i++){...
        }//End of if(count($paymentModsArr) > 0){
        else {
            $this->redirect('cart/list');
        }

        //check if card exist in array
        if (!in_array($paymentMode, $nmiCheckArray)) {
            $errorObj = new ErrorMsg();
            $errorMsg = $errorObj->displayErrorMessage("E059", '001000');
            $this->getUser()->setFlash('notice', $errorMsg);
            $this->redirect('cart/list');
        }

        ## This varibale is coming from nmi...
        ## Removing this variable...
        $this->getUser()->getAttributeHolder()->remove('ses_order_number');

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $this->payStatus = $request_details->getPaymentStatus();
        if (0 == $this->payStatus)
            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
        if ($request_details) {
            $this->request_details = $request_details;
            $this->requestId = $requestId;
            $this->paymentMode = $paymentMode;
        } else {
            throw New Exception('No Order details found for this request!');
        }

        ## Getting application type...
        $this->application_type = $this->getApplicationType($request_details);

        ## Getting processing country...
        $this->processingCountry = $this->getUser()->getAttribute('processingCountry');


    }

    /** function executeNmiForm(sfWebRequest $request)
     * @purpose : This action will display payment form of NMI gateway after executepayment() function
     * @param : (sfWebRequest $request)
     * @return : Displays payment form
     * @author :
     * @date :
     */
    public function executeNmiForm(sfWebRequest $request) {

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         */
        $requestId = $this->getUser()->getAttribute('requestId');

        ## get cart item for checking if application is already paid
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->setFlash('notice', 'There is no application exists in the cart.');
            return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'cart',
                            'action' => 'list')) . "'</script>");
        }
        $paymentMode = $request->getParameter('paymentMode');
        ## Encrypted paymentMode variabel...
        $encrptPaymentMode = Settings::encryptInput($paymentMode);

        $result = $this->checkPaymentAlreadyDone($requestId);
        switch ($result) {
            case 'serverdown':
                $this->getUser()->setFlash('notice', 'Oops!! Payment Server connection problem found!!!');
                return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'nmi',
                            'action' => 'payment', 'paymentMode' => $encrptPaymentMode)) . "'</script>");
            case 'paymentalready':
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
                return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'cart',
                            'action' => 'list')) . "'</script>");
                break;
            case 'success':
                return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'cart',
                            'action' => 'list')) . "'</script>");
                break;
            default:
                break;
        }//End of switch($result){...

        try {

            $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
            $this->payStatus = $request_details->getPaymentStatus();


            if ($this->payStatus == 0) {

                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
                return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'nmi',
                            'action' => 'payment', 'paymentMode' => $encrptPaymentMode)) . "'</script>");
            }

            $this->requestId = $requestId;
            $expload = explode(',', $paymentMode);

            if ($expload[1] == 'jna_nmi_vbv') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($expload[1]);
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->cardType = "V";
                $this->paymentCardType = 'jna_nmi_vbv';

            } else if ($expload[1] == 'jna_nmi_mcs') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($expload[1]);
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->cardType = "M";
                $this->paymentCardType = 'jna_nmi_mcs';

            } else if ($expload[1] == 'nmi_mcs') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($expload[1]);
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->cardType = "M";
                $this->paymentCardType = 'nmi_mcs';
            } else if ($expload[1] == 'nmi_vbv') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($expload[1]);
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->cardType = "V";
                $this->paymentCardType = 'nmi_vbv';
            } else {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($expload[1]);
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->cardType = "A";
                $this->paymentCardType = 'nmi_amx';
            }

            //start - vineet for country zip validaton
            $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();
            $arrCountry = array();
            for ($i = 0; $i < count($countryCode); $i++) {
                $arrCountry[] = $countryCode[$i]['country_code'];
            }
            $this->arrCountry = $arrCountry;

            $country = '';
            if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
                $country = $_REQUEST['ep_pay_easy_request']['country'];
            }
            //end - vineet for country zip validaton

            $this->form = new EpNmiRequestForm('', array('task' => 'payment', 'argument' => array('country' => $country, 'isRegistered' => false)));
            $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);

            $this->form->setDefault('request_ip', $ipReq);
            $this->setTemplate('nmiForm');

            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();

            $this->formActionURL = $host . $url_root . '/nmi/paymentProceed?paymentMode=' . $encrptPaymentMode;
        } catch (Exception $e) {
            $this->getUser()->setFlash('notice', sprintf($e->getMessage()));
            return $this->renderText("<script>window.location = '" . $this->generateUrl('default', array('module' => 'nmi',
                        'action' => 'payment', 'paymentMode' => $encrptPaymentMode)) . "'</script>");
        }
    }

    /** function executeForm(sfWebRequest $request)
     * @purpose : render for to display the form in NMIForm action
     * @param : (sfWebRequest $request)
     * @return : Render form
     * @author :
     * @date :
     */
    public function executeForm(sfWebRequest $request) {

        $paymentMode = $request->getParameter('paymentMode');
        if ('vbv' == $paymentMode) {
            $this->paymentMode = "Verified By Visa";
            $this->cardType = "V";
        } else {
            $this->paymentMode = "Master Card Secure";
            $this->cardType = "M";
        }

        $this->setLayout('iframe_layout');
        $this->postUrl = $request->getParameter('postUrl');

        //start - vineet for country zip validaton
        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }
        $this->arrCountry = $arrCountry;
        //end - vineet for country zip validaton

        $this->form = new EpNmiRequestForm('', array('task' => 'payment'));
        $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);

        $this->form->setDefault('request_ip', $ipReq);
    }

    //For CVV popup Menu
    public function executeCvvHelp(sfWebRequest $request) {
        $this->setLayout('iframe_layout');
    }

    /** function executeNmiResponse(sfWebRequest $request)
     * @purpose :get all active user from db
     * @param : (sfWebRequest $request)
     * @return : NMI response will hit this action, This action has been passed in the request. On faiure or success. NMI send its response to this action
     * @author :
     * @date :
     */
    public function executeNmiResponse(sfWebRequest $request) { //Approve
        $res = $request->getParameterHolder()->getAll();
        $orderNumber = $res['order'];


        ## If application get paid then this message will be shown on cart list page...
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $this->getUser()->getAttributeHolder()->remove('ses_order_number');
            $this->getUser()->setFlash('notice', 'Applications has been already processed.');
?>
            <script>
                window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'cart',
                'action' => 'list')); ?>";
            </script>
<?php
            exit;
        }

        ## If user coming twice for the same order id then following code will be executed...
        $ses_order_number = $this->getUser()->getAttribute('ses_order_number');


        if (isset($ses_order_number) && !empty($ses_order_number)) {
            $this->getUser()->getAttributeHolder()->remove('ses_order_number');
            $this->getUser()->setFlash('notice', 'Tampering URL is not allowed!!!');
?>
            <script>
                window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'cart',
                'action' => 'list')); ?>";
            </script>
<?php
            exit;
        } else {
            $this->getUser()->setAttribute('ses_order_number', $orderNumber);
        }

        $processingCountries = $this->getProcessingCountries();

        ## Getting payment gateway id && midService i.e, mid1 or mid2 or etc...
        $gateWayInfo = array();
        $gateWayInfo = $this->getPaymentGatewayId();
        $gateway_id = $gateWayInfo['gatewayId'];
        $midService = $gateWayInfo['midService'];

        ## Get Validation for country and MID basis
        $paymentValidationObj = new PaymentValidationFilter();
        $arrValidationRules = $paymentValidationObj->getCountryMidValidation(strtoupper($midService));
        ## Setting gateway id and midservice to the class...
        $epNmiManagerObj = new EpNmiManager();
        $epNmiManagerObj->Gateway_id = $gateway_id;
        $epNmiManagerObj->midService = $midService;

        ## Calling Finalise Transaction...
        $finaliseTransaction = $epNmiManagerObj->finaliseTransaction($res['token-id'], $processingCountries, $arrValidationRules, $orderNumber);

        ## Get object of error message class to display error messages
        $errMsgObj = new ErrorMsg();

        if ($finaliseTransaction) {
            $order_number = $finaliseTransaction['order_id'];
            $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($order_number)->getFirst();
            $orderId = $orderObj->getId();

            $requestId = $orderObj->getOrderRequestDetailId();
            $payManagerObj = new PaymentManager();
            $paymentResponseArray = array();
            $paymentResponseArray['response_code'] = $finaliseTransaction['result_code'];
            $paymentResponseArray['response_text'] = $finaliseTransaction['result_text'];
            $paymentResponseArray['card_Num'] = $finaliseTransaction['cc_number'];
            //if($response['responsecode']=='001')
            $paymentResponseArray['status'] = 0;
            if (substr($paymentResponseArray['card_Num'], 0, 1) == '4') {
                $paymentResponseArray['card_type'] = 'V';
            } elseif (substr($paymentResponseArray['card_Num'], 0, 1) == '5') {
                $paymentResponseArray['card_type'] = 'M';
            } elseif (substr($paymentResponseArray['card_Num'], 0, 1) == '3') {
                $paymentResponseArray['card_type'] = 'A';
            }

            ## this variable provide time period which runs before page...
            ## if this is 0 that means no break will come before thanks...
            $sec = sfConfig::get('app_payment_success_time_out_limit');

            $status = "failure";
            if ($finaliseTransaction['success'] == 'success') {
                $paymentResponseArray['status'] = 1;

                if ($sec == 0) {

                    $orderObj = $payManagerObj->updatePaymentRequestNmi($orderId, $paymentResponseArray);
                    $updateOrder = $payManagerObj->updateOrderRequest($orderId, $paymentResponseArray);
                    if ($updateOrder->getPaymentStatus() == 0) {

                        $updateOrder = $payManagerObj->updateSplitAmount($requestId, $gateway_id);
                        $result = $payManagerObj->paymentSuccess($order_number);
                        $status = "success";
                    } else {
                        $result = $payManagerObj->paymentFailure($order_number);
                        $status = "failure";
                    }
                } else {
                    $host = $request->getUriPrefix();
                    $url_root = $request->getPathInfoPrefix();
                    $sitePath = $host . $url_root;

                    $start_time = date('Y-m-d H:i:s', mktime(date("H"), date("i"), date("s") + $sec, date("m"), date("d"), date("Y")));
                    $url = "notification/updatePaymentStatus";
                    $paymentStatusJobId = EpjobsContext::getInstance()->addJob('ApplicationPaymentSuccess', $url, array('order_id' => $orderId, 'orderNumber' => $order_number, 'requestId' => $requestId, 'gateway_id' => $gateway_id, 'sitePath' => $sitePath, 'paymentStatus' => serialize($paymentResponseArray)), '', '', $start_time);
                    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $paymentStatusJobId");

                    ## Updationg jobid into ipay4me_order table...
                    Doctrine::getTable('ipay4meOrder')->updateJobId($order_number, $paymentStatusJobId);

                    $status = "success";
                }
            } elseif ($finaliseTransaction['error'] != '') {
                $paymentResponseArray['response_code'] = NULL;
                $paymentResponseArray['response_text'] = NULL;

                $orderObj = $payManagerObj->updatePaymentRequestNmi($orderId, $paymentResponseArray);

                if ($finaliseTransaction['error'] == 'zip') {
                    ## get proper error message
                    $errorMsg = $errMsgObj->displayErrorMessage("E001", $requestId);
                    $this->getUser()->setFlash('notice', $errorMsg, true);
                } else if ($finaliseTransaction['error'] == 'surname-match') {
                    ## get proper error message
                    sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
                    $url = url_for('page/printInstruction');
                    $errorMsg = $errMsgObj->displayErrorMessage("E056", $requestId, array('URL' => ' Kindly <a style="font-size: 15px; font-weight: bold;"href="javascript:;" onclick="instructionPopup(\'' . $url . '\');">click here</a> to read the instruction carefully.'));
                    $this->getUser()->setFlash('notice', $errorMsg, true);
                } else {
                    ## get proper error message
                    $errorMsg = $errMsgObj->displayErrorMessage("E002", $requestId);
                    $this->getUser()->setFlash('notice', $errorMsg, true);
                }
                $paymentMode = $gateway_id . ',' . $finaliseTransaction['card'];
                $encryptPaymentMode = Settings::encryptInput($paymentMode);
?>
                <script>
                    window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'nmi',
                    'action' => 'payment', 'paymentMode' => $encryptPaymentMode)); ?>";
                </script>
<?php
                exit;
            }
            FpsDetailTable::getInstance()->updateFpsDetailForNmi($order_number, $status, $finaliseTransaction['3d-verified'], $arrValidationRules);
            if ($status == "success") {

                $actionName = ($sec == 0) ? 'thankYou' : 'paymentConfirmation';
?>
                <script>
                    window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'paymentGateway',
                                                'action' => $actionName, 'valid' => Settings::encryptInput($order_number), 'response_code' => $paymentResponseArray['response_code'], 'response_text' => $paymentResponseArray['response_text'])); ?>";
                </script>
<?php
                exit;
            } else {
                ##$this->getUser()->setFlash('notice', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
                $errorMsg = $errMsgObj->displayErrorMessage("E003", $requestId);
                $this->getUser()->setFlash('notice', $errorMsg, true);
?>
                <script>
                    window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'paymentGateway',
                    'action' => 'registeredCardError')); ?>";
                </script>
<?php
                exit;
            }
        } else {
            FpsDetailTable::getInstance()->updateFpsDetailForNmi($orderNumber, 'failure', 'no', $arrValidationRules);
            $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber)->getFirst();
            $requestId = $orderObj->getOrderRequestDetailId();
            //get proper error message
            $errorMsg = $errMsgObj->displayErrorMessage("E003", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg, true);
            //$this->getUser()->setFlash('notice', 'Your bank has declined the transaction, please contact your bank for further details. Please try another card.', true);
            //$this->redirect("paymentGateway/registeredCardError?requestId=$requestId");
?>
            <script>
                window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'paymentGateway', 'action' => 'registeredCardError')); ?>";
            </script>
<?php
            exit;
        }
    }

    /** function saveIp4mOrder($params)
     * @purpose : get all active user from db
     * @param : ($params)
     * @return : Save order in order request table
     * @author :
     * @date :
     */
    function saveIp4mOrder($params) {

        /**
         * [WP: 103] => CR: 148
         * Fetching processing country...
         */
        $paymentOptions = new paymentOptions();
        $processingCountry = $paymentOptions->getProcessingCountry();

        $retArr = array();
        ## Getting payment gateway id...
        $gateWayInfo = array();
        $gateWayInfo = $this->getPaymentGatewayId();
        $intGatewayId = $gateWayInfo['gatewayId'];

        //$activePaymentGateway = "NMI" ; // sfConfig::get('app_active_paymentgateway');
        //get gatway id
        //$arrGateway = Doctrine::getTable('gateway')->findByName($activePaymentGateway);
        //$intGatewayId = $arrGateway->getFirst()->getId();
        $request_details = Doctrine::getTable('OrderRequestDetails')->find($params['requestId']);

        $amount = $request_details->getAmount();
        //order no
        $orderManagerObj = orderServiceFactory::getService(self::$version);
        $orderNumber = $orderManagerObj->getOrderNumber();
        $saveOrderDetail = new ipay4meOrder();
        $saveOrderDetail->setOrderNumber($orderNumber);
        $saveOrderDetail->setGatewayId($intGatewayId);
        $saveOrderDetail->setOrderRequestDetailId($params['requestId']);
        $saveOrderDetail->setPayorName($params['payorName']);
        $saveOrderDetail->setCardHolder($params['card_holder']);
        $saveOrderDetail->setAddress($params['address']);
        $saveOrderDetail->setPhone($params['phone']);
        $saveOrderDetail->setCardFirst($params['card_first']);
        $saveOrderDetail->setCardLast($params['card_last']);
        $saveOrderDetail->setCardLen($params['card_len']);
        $saveOrderDetail->setCardType($params['cardType']);
        $saveOrderDetail->setCardHash(md5($params['cardNumber']));
        $saveOrderDetail->setProcessingCountry($processingCountry);
        $saveOrderDetail->save();
        $retArr['amount'] = $amount;
        $retArr['orderNumber'] = $orderNumber;
        $retArr['gatewayId'] = $intGatewayId;
        return $retArr;
    }

    public function executeVisaMasterInfo() {
        $this->setLayout(false);
    }

    /** function executeValidateCard(sfWebRequest $request)
     * @purpose : validate form and cards details for non - registered card
     * @param : (sfWebRequest $request)
     * @return : true/ or error message
     * @author :
     * @date :
     */
    public function executeValidateCard(sfWebRequest $request) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');

        /**
         * [WP: 103] => CR: 148
         * Checking empty cart validations...
         */
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            $redirectUrl = url_for('cart/list');
            $this->getUser()->setFlash('notice', 'There is no application exist in the cart.');
            return $this->renderText($redirectUrl.'#type=emptycart');
        }

        ## If reqestId not found then sending user to cart page with tempering message...
        $requestId = $this->getUser()->getAttribute('requestId');

        if(isset($requestId) && $requestId == ''){
            $redirectUrl = url_for('cart/list');
            $redirectUrl .= "#type=error";
            $errorMsg = Functions::temperingURLMessage();
            $this->getUser()->setFlash('notice', $errorMsg);
            return $this->renderText($redirectUrl);
        }

        ## Creating Order...
        $params = $this->createOrder($request);

        ## card payment mode...
        $paymentCardType = $request->getParameter('paymentCardType');

        ## Getting Userid...
        $userId = $this->getUser()->getGuardUser()->getId();
        $is3dVerified = 0;

        ## Getting payment gateway id && midService i.e, mid1 or mid2 or etc...
        $gateWayInfo = array();
        $gateWayInfo = $this->getPaymentGatewayId();
        $intGatewayId = $gateWayInfo['gatewayId'];
        $midService = $gateWayInfo['midService'];


        ## Checing fraud prevention...
        //Get Validation for country and MID basis
        $ruleId = '';
        //if(!$this->getUser()->isPrivilegeUser()){
        $paymentValidationObj = new PaymentValidationFilter();
        $arrValidationRules = $paymentValidationObj->getCountryMidValidation(strtoupper($midService));

        if (is_array($arrValidationRules) && in_array('NO', $arrValidationRules)) {
            $ruleId = '';
        } else if (is_array($arrValidationRules) && count($arrValidationRules) == 1 && in_array('SN', $arrValidationRules)) {
            $ruleId = '';
        } else {
            //Start:code for putting fps rule in diff lib, by:kuldeep
            //setup param array
            $arrParams['ip_address'] = $params['ip_address'];
            $arrParams['card_number'] = $params['cardNumber'];
            $arrParams['gateway_id'] = $params['gateway_id'];
            $arrParams['requestId'] = $params['requestId'];
            $arrParams['request_ip'] = $params['ip_address'];
            $arrParams['email'] = $params['billing-email'];
            $arrParams['orderNumber'] = $params['orderNumber'];

            $objValidator = new FpsRuleValidation();
            $fpsRuleDetail = $objValidator->chkFraudPrevention($arrParams, $arrValidationRules);
            $fpsId = $fpsRuleDetail['fpsId'];
            $ruleId = $fpsRuleDetail['ruleId'];
        }
        //}

        $redirectUrl = "";
        $isRegistered = $this->checkCardStatus($params['cardNumber'], $userId);
        if ($isRegistered) {
            $requestId = $params['requestId'];
            $redirectUrl = url_for('nmi/stopByCardValidate');
            $redirectUrl .="#type=error";
            return $this->renderText($redirectUrl);
        }//End of if($isRegistered){...
        if ($ruleId) {
            $requestId = $params['requestId'];
            $redirectUrl = url_for('nmi/stopByRule');
            $redirectUrl .="?ruleId=$ruleId" . "#type=error";
        } else { //End if($ruleId){...

           if(strtoupper($midService) == "JNA1"){
               $card = 'jna_nmi_vbv';
           }else{
               if(substr($params['cardNumber'], 0, 1) == '4'){
                  $card = 'nmi_vbv' ;
               }elseif(substr($params['cardNumber'], 0, 1) == '5'){
                  $card = 'nmi_mcs' ;
               }elseif(substr($params['cardNumber'], 0, 1) == '3'){
                  $card = 'nmi_amx' ;
               }
           }

           $paymentMode = $intGatewayId.','.$card;

            ## surname will be matched only for JNA1 in first step
            ## and for NMI surname will be matched in third step i.e.,
            if (is_array($arrValidationRules) && in_array('SN', $arrValidationRules)) {
                //setting up param array
                $arrSurNameParam['first_name'] = $params['fname'];
                $arrSurNameParam['last_name'] = $params['lname'];
                //setup object of surname validation class
                $objSurName = new SurNameValidation();
                $arrReturnSurName = $objSurName->checkForSurNameValidation($arrSurNameParam, $card);
                $isSurNameMatch = $arrReturnSurName['isSurNameMatch'];
                $isFullNameMatch = $arrReturnSurName['isFullNameMatch'];
            } else {
                $isSurNameMatch = true;
                $isFullNameMatch = true;
            }
            if (!$isSurNameMatch || !$isFullNameMatch) {
                $requestId = $params['requestId'];
                $errorObj = new ErrorMsg();
                $url = url_for('page/printInstruction');
                $errorMessage = $errorObj->displayErrorMessage("E056", $requestId, array('URL' => ' Kindly <a style="font-size: 15px; font-weight: bold;"href="javascript:;" onclick="instructionPopup(\'' . $url . '\');">click here</a> to read the instruction carefully.'));
                $this->getUser()->setFlash('notice', $errorMessage, true);

                /**
                 * [WP: 086] => CR:125
                 * Added ruleId 6 while surname failure...
                 */
                if (count($arrValidationRules) > 1) {
                   $fpsObj = Doctrine::getTable('FpsDetail')->find($fpsId);
                   if(!empty($fpsObj)){
                       $fpsObj->setFpsRuleId(6);
                       $fpsObj->save();
                   }
                }

                /**
                 * [WP: 085] => CR:124
                 * Making paymentMode variable encrypted...
                 */
                $encryptPaymentMode = Settings::encryptInput($paymentMode);
                $redirectUrl = url_for('nmi/payment?paymentMode='.$encryptPaymentMode);
                $redirectUrl .= "#type=error";
                return $this->renderText($redirectUrl);
            }//End of if(!$isSurNameMatch || !$isFullNameMatch ){...

            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();

            $epNmiManagerObj = new EpNmiManager();
            ## Setting gatewayid and midService to the class...
            $epNmiManagerObj->Gateway_id = $intGatewayId;
            $epNmiManagerObj->midService = $midService;

            $url = $host . $url_root;

            $postUrl = $epNmiManagerObj->getFormUrl($params['amount'], $params['orderNumber'], $url, $is3dVerified);

            if ($postUrl) {
                $redirectUrl .=$postUrl . "#type=success";
            }
        }
        return $this->renderText($redirectUrl);
    }

    /** function setErrorMsg($ruleId, $requestId)
     * @purpose : validate form and cards details
     * @param : ($ruleId, $requestId)
     * @return : error message
     * @author :
     * @date :
     */
    protected function setErrorMsg($ruleId, $requestId) {
        $errorMsgObj = new ErrorMsg();
        if (1 == $ruleId) {
            $time = Settings::maxip_time();
            //      $this->getUser()->setFlash('notice', ' You have reached the maximum allowed number of transactions form this computer at the current time. Please try again after '.$time);
            $errorMsg = $errorMsgObj->displayErrorMessage("E004", $requestId, array("time" => $time));
            $this->getUser()->setFlash('notice', $errorMsg);
        }
        if (2 == $ruleId) {
            //start:Kuldeep for check cart capacity
            $arrSameCardLimit = array();
            $cartValidationObj = new CartValidation();
            $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
            if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
                $arrSameCardLimit = $arrCartCapacityLimit;
            }
            //end:Kuldeep
            $time = Settings::maxcard_time($arrSameCardLimit['transaction_period']);
            $errorMsg = $errorMsgObj->displayErrorMessage("E005", $requestId, array("time" => $time));
            //      $this->getUser()->setFlash('notice', ' You have reached the maximum allowed number of transactions from this card at this time. Please try again after '.$time);
            $this->getUser()->setFlash('notice', $errorMsg);
        }
        if (3 == $ruleId) {
            $time = Settings::maxemail_time();
            $errorMsg = $errorMsgObj->displayErrorMessage("E006", $requestId, array("time" => $time));
            $this->getUser()->setFlash('notice', $errorMsg);
            //      $this->getUser()->setFlash('notice', ' Due to anti fraud reasons this transaction can not be processed currently. Please try again after '.$time);
        }
        if (4 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E007", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
            //      $this->getUser()->setFlash('notice', ' Your card has been declined by the Processor, please use another card.');
        }
        if (5 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E008", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
            //          $this->getUser()->setFlash('notice', ' This card is black listed by Admin, please use another card.');
        }
        /**
         * [WP: 086] => CR:125
         * Setting error message for RuleId 7...
         * Credit card country does not match...
         */
        if (7 == $ruleId) {
            $errorMsg = $errorMsgObj->displayErrorMessage("E061", $requestId);
            $this->getUser()->setFlash('notice', $errorMsg);
        }
    }

    /** function executeStopByRule(sfWebRequest $request)
     * @purpose : display error screen when user is stop by rule Id
     * @param : (sfWebRequest $request)
     * @return : error message
     * @author :
     * @date :
     */
    public function executeStopByRule(sfWebRequest $request) {
        $ruleId = $request->getParameter('ruleId');
        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */
        $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
        $isregistered = ($request->hasParameter('registered')) ? $request->getParameter('registered') : 'no';
        $this->setErrorMsg($ruleId, $requestId);
        $this->redirect("paymentGateway/error?ruleId=$ruleId&registered=$isregistered");
    }

    /** function createOrder($request)
     * @purpose : put entry in ipay4me_order table
     * @param : ($request)
     * @return : Array
     * @author :
     * @date :
     */
    public function createOrder($request) {
        $params = array();
        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */
        $params['requestId'] = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');             // order request detail id
        $params['cardNumber'] = $request->getParameter('billing-cc-number');
        $params['cardType'] = $request->getParameter('cardType');
        $params['billing-email'] = $request->getParameter('billing-email');
        $params['ip_address'] = $request->getParameter('request_ip');
        $params['fname'] = $request->getParameter('billing-first-name');
        $params['lname'] = $request->getParameter('billing-last-name');
        $params['card_holder'] = $request->getParameter('card_holder');
        $params['cardType'] = $request->getParameter('cardType');
        $params['phone'] = $request->getParameter('billing-phone');
        $concat_str = "~";
        $params['address'] = $request->getParameter('billing-address1') . $concat_str . $request->getParameter('billing-address2') . $concat_str . $request->getParameter('billing-city') . $concat_str . $request->getParameter('billing-state')
                . $concat_str . $request->getParameter('billing-postal') . $concat_str . $request->getParameter('billing-country');
        $params['payorName'] = $params['fname'] . " " . $params['lname'];
        $params['card_len'] = strlen($params['cardNumber']);
        $params['card_first'] = substr($params['cardNumber'], 0, 4);
        $params['card_last'] = substr($params['cardNumber'], -4, 4);

        $retArr = $this->saveIp4mOrder($params);

        $params['gateway_id'] = $retArr['gatewayId'];
        $params['amount'] = $retArr['amount'];
        $params['orderNumber'] = $retArr['orderNumber'];

        return $params;
    }

    /** function : executeValidateInCardVault(sfWebRequest $request)
     * @purpose : validate form
     * @param : (sfWebRequest $request)
     * @return : true/ or error message
     * @author :
     * @date :
     */
    public function executeValidateInCardVault(sfWebRequest $request) {

        /**
         * [WP: 103] => CR: 148
         * Checking empty cart validations...
         */
        $arrCartItems = $this->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            return $this->renderText('#type=emptycart');
        }
        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         */
        $requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');

        ## If reqestId not found then sending user to cart page with tempering message...
        sfContext::getInstance()->getConfiguration()->loadHelpers('Url');
        if(isset($requestId) && $requestId == ''){
            $redirectUrl = url_for('cart/list');
            $redirectUrl .= "#type=error";
            $errorMsg = Functions::temperingURLMessage();
            $this->getUser()->setFlash('notice', $errorMsg);
            return $this->renderText($redirectUrl);
        }

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
        $params = $this->createOrder($request);
        $is3dVerified = 0;
        //strat:Kuldeep
        ## Getting payment gateway id && midService i.e, mid1 or mid2 or etc...
        $gateWayInfo = array();
        $gateWayInfo = $this->getPaymentGatewayId();
        $intGatewayId = $gateWayInfo['gatewayId'];
        $midService = $gateWayInfo['midService'];

        //Get Validation for country and MID basis
        $ruleId = '';
        if (!$this->getUser()->isPrivilegeUser()) {
            $paymentValidationObj = new PaymentValidationFilter();
            $arrValidationRules = $paymentValidationObj->getCountryMidValidation(strtoupper($midService));

            /**
               * Commented by ashwani kumar...
               */
            ##if (!is_array($arrValidationRules) && strtoupper($arrValidationRules) == "NO") {
            if (is_array($arrValidationRules) && in_array('NO', $arrValidationRules)) {
                $ruleId = '';
            } else if (is_array($arrValidationRules) && count($arrValidationRules) == 1 && in_array('SN', $arrValidationRules)) {
                $ruleId = '';
            } else {
                $arrParams['ip_address'] = $params['ip_address'];
                $arrParams['card_number'] = $params['cardNumber'];
                $arrParams['gateway_id'] = $params['gateway_id'];
                $arrParams['requestId'] = $params['requestId'];
                $arrParams['request_ip'] = $params['ip_address'];
                $arrParams['email'] = $params['billing-email'];
                $arrParams['orderNumber'] = $params['orderNumber'];
                $objValidator = new FpsRuleValidation();
                $fpsRuleDetail = $objValidator->chkFraudPrevention($arrParams, $arrValidationRules);
                $ruleId = $fpsRuleDetail['ruleId'];
            }
        }
        //end:kuldeep

        $redirectUrl = "";
        if ($request_details->getPaymentStatus() == 0) {
            $redirectUrl = url_for('paymentGateway/paymentAlready');
            $redirectUrl .= "#type=error";
        }
        if ($ruleId) {
            //$requestId = $params['requestId'];
            $redirectUrl = url_for('nmi/stopByRule');
            $redirectUrl .="?ruleId=$ruleId&registered=yes" . "#type=error";
        } else {
            $isCardExistInApplicantVault = ApplicantVaultTable::getInstance()->validateCardInApplicantVault($params['cardNumber']);
            if (!$isCardExistInApplicantVault) {
                $redirectUrl = "cardnotfound";
            } else {
                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();

                $epNmiManagerObj = new EpNmiManager();
                ## Setting gatewayid and midService to the class...
                $epNmiManagerObj->Gateway_id = $intGatewayId;
                $epNmiManagerObj->midService = $midService;

                $url = $host . $url_root;

                $postUrl = $epNmiManagerObj->getFormUrl($params['amount'], $params['orderNumber'], $url, $is3dVerified);

                if ($postUrl) {
                    $redirectUrl .=$postUrl . "#type=success";
                }
            }
        }
        return $this->renderText($redirectUrl);
    }

    /** function : executeError(sfWebRequest $request)
     * @purpose : display error template
     * @param : (sfWebRequest $request)
     * @return : true/ or error message
     * @author :
     * @date :
     */
    public function executeError(sfWebRequest $request) {
        $this->setTemplate('error');
    }

    /** function : getProcessingCountries
     * @purpose : display error template
     * @param : Returns the array of processing country, on the basis of application in the cart
     * @return : Array
     * @author :
     * @date :
     */
    public function getProcessingCountries() {
        $arrCartItems = $this->getUser()->getCartItems();
        $arrProcessingCountry = array();
        foreach ($arrCartItems as $items) {
            if ($items->getType() == 'Passport') {
                //get Application details
                $arrProcessingCountry[] = Doctrine::getTable('PassportApplication')->getProcessingCountry($items->getAppId());
            } elseif ($items->getType() == 'Freezone') {
                $arrProcessingCountryId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($items->getAppId());
                $arrProcessingCountry[] = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($arrProcessingCountryId);
            } elseif ($items->getType() == 'Visa') {
                $arrProcessingCountry[] = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($items->getAppId());
            }
        }

        $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);
        $ipReq = "72.32.3.101";
        $country_code = Doctrine::getTable('CountryIp')->getCountryCode($ipReq);
        $arrIntersec = in_array($country_code, $arrProcessingCountry);
        if (!$arrIntersec) {
            $arrProcessingCountry[count($arrProcessingCountry)] = $country_code;
        }
        return $arrProcessingCountry;
    }

    /** function : checkCardStatus($cardNumber, $userId)
     * @purpose : return true if card is registered with User/ and false
     * @param : ($cardNumber, $userId)
     * @return : true/false
     * @author :
     * @date :
     */
    private function checkCardStatus($cardNumber, $userId) {
        $cardHash = md5($cardNumber);
        $isRegistered = ApplicantVaultTable::getInstance()->getApprovedCardByHash($cardHash, $userId);
        if (count($isRegistered) > 0) {
            return true;
        }
    }

    /** function : executeStopByCardValidate(sfWebRequest $request)
     * @purpose : displays error
     * @param : (sfWebRequest $request)
     * @return : true/false
     * @author :
     * @date :
     */
    public function executeStopByCardValidate(sfWebRequest $request) {
        //$ruleId = $request->getParameter('ruleId');
        //$requestId = $request->getParameter('requestId');
        $this->getUser()->setFlash('notice', 'This card is already registered on this portal. Please choose this card from registered card list for payment.');
        $this->redirect("paymentGateway/gotoApplicantVault");
    }

    /** function : executeVbvPay(sfWebRequest $request)
     * @purpose : displays error
     * @param : (sfWebRequest $request)
     * @return : true/false
     * @author :
     * @date :
     */
    public function executeVbvPay(sfWebRequest $request) {
        $this->setTemplate("paymentCus");
    }

    /** function : getPaymentGatewayId
     * @purpose : return gatewayId
     * @param :
     * @return : Array
     * @author :
     * @date :
     */
    function getPaymentGatewayId() {

        ## Checking weather user  is privilege or not...
        if (!$this->getUser()->isPrivilegeUser()) {
            $payModeObj = new PaymentModeManager();

            ## Getting first porcessing country...
            $pcountry = $payModeObj->getFirstProcessingCountry();

            ## Getting MID service according to country...
            $midService = $payModeObj->getActiveMidService($pcountry);
        } else {
            $paymentModeObj = new PaymentModeManager();
            $midService = $paymentModeObj->getPrivilegeUserGatewayId();
        }
        $midService = strtolower($midService);

       /**
         * Fetching gateway id from mid details table...
         */
        $midDetailsObj = Doctrine::getTable('MidDetails')->findByServiceAndServiceType('nmi', $midService);

        if(count($midDetailsObj)){
            $intGatewayId = $midDetailsObj->getFirst()->getGatewayId();
        }else{
            $intGatewayId = 5;
            $midService = 'ps1';
        }

//        switch ($midService) {
//            case 'ps1':
//                $intGatewayId = 5;
//                break;
//            case 'llc2':
//                $intGatewayId = 9;
//                break;
//            case 'jna1':
//                $intGatewayId = 10;
//                break;
//            default:
//                $intGatewayId = 5;
//                $midService = 'ps1';
//                break;
//        }//End of switch ($midService){...


        return array('gatewayId' => $intGatewayId, 'midService' => $midService);
    }

//End of function getPaymentGatewayId(){...

    /** function : executePaymentHiddenForm(sfWebRequest $request)
     * @purpose : return gatewayId
     * @param : (sfWebRequest $request)
     * @return :
     * @author :
     * @date :
     */
    public function executePaymentHiddenForm(sfWebRequest $request) {

        $this->formData = json_decode(urldecode($request->getParameter('formData')));
        $requestId = $this->formData->requestId;
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->getOrderNumbersLast($requestId);
        $this->orderNumber = $ipay4meOrder[0]['order_number'];

        if (empty($this->formData->billing_first_name) || empty($this->orderNumber)) {
            ##get object of error message class to display error messages
            $errMsgObj = new ErrorMsg();
            $errorMsg = $errMsgObj->displayErrorMessage("E059", '001000');
            $this->getUser()->setFlash('notice', $errorMsg, true);
?>
            <script>
                window.parent.location = "<?php echo $this->generateUrl('default', array('module' => 'cart', 'action' => 'list')); ?>";
            </script>
<?php
        }
        $host = $request->getUriPrefix();
        $url_root = $request->getPathInfoPrefix();
        $url = $host . $url_root;
        $this->err_url = $url . '/cart/list';
        $this->setLayout('popupLayout');
    }

    /** function : executePaymentProceed(sfWebRequest $request)
     * @purpose : payment form process
     * @param : (sfWebRequest $request)
     * @return :
     * @author :
     * @date :
     */
    public function executePaymentProceed(sfWebRequest $request) {

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session variable...
         * If requestId does not exist then sending user to cart page with  tempering message...
         */
        $requestId = $this->getUser()->getAttribute('requestId');
        Functions::checkRequestIdTempering($requestId);

        $formData = array();
        $billing_cc_exp = $request->getParameter('billing-cc-exp');

        if (empty($billing_cc_exp)) {
            ##get object of error message class to display error messages
            $errorMsg = Functions::temperingURLMessage();
            $this->getUser()->setFlash('notice', $errorMsg, true);
            $this->redirect('cart/list');
        }//End of if(empty($billing_cc_exp)){...


        if ($billing_cc_exp[0] == 0) {
            $exp_month = $billing_cc_exp[1];
        } else {
            $exp_month = $billing_cc_exp[0] . $billing_cc_exp[1];
        }


        $exp_year = (int) '20' . $billing_cc_exp[2] . $billing_cc_exp[3];

        $formData['billing_first_name'] = $request->getParameter('billing-first-name');
        $formData['billing_last_name'] = $request->getParameter('billing-last-name');
        $formData['billing_address1'] = $request->getParameter('billing-address1');
        $formData['billing_address2'] = $request->getParameter('billing-address2');
        $formData['billing_city'] = $request->getParameter('billing-city');
        $formData['billing_state'] = $request->getParameter('billing-state');
        $formData['billing_postal'] = $request->getParameter('billing-postal');
        $formData['billing_country'] = $request->getParameter('billing-country');
        $formData['billing_email'] = $request->getParameter('billing-email');
        $formData['billing_phone'] = $request->getParameter('billing-phone');
        $formData['cardType'] = $request->getParameter('cardType');
        $formData['billing_cc_number'] = $request->getParameter('billing-cc-number');
        $formData['card_holder'] = $request->getParameter('card_holder');
        $formData['billing_cc_exp_month'] = $exp_month;
        $formData['billing_cc_exp_year'] = $exp_year;
        $formData['billing_cvv'] = $request->getParameter('billing-cvv');
        $formData['request_ip'] = $request->getParameter('request_ip');
        $formData['requestId'] = $requestId; //$request->getParameter('requestId');
        $formData['billing_cc_exp'] = $request->getParameter('billing-cc-exp');
        $formData['thirdPartyFormActionURL'] = $request->getParameter('thirdPartyFormActionURL');

        $this->formData = $formData;
        $this->setLayout(false);
        $host = $request->getUriPrefix();
        $url_root = $request->getPathInfoPrefix();

        $this->formURL = $host . $url_root . '/nmi/paymentHiddenForm?formData=' . urlencode(json_encode($formData));
    }

    /** function : executeGetCartInfo(sfWebRequest $request)
     * @purpose : cart Info
     * @param : (sfWebRequest $request)
     * @return :
     * @author :
     * @date :
     */
    function executeGetCartInfo(sfWebRequest $request) {

        ## If application get paid then this message will be shown on cart list page...
        $arrCartItems = $this->getUser()->getCartItems();
        $result = 0;
        if (count($arrCartItems) > 0) {
            $result = 1;
        }
        return $this->renderText($result);
    }

    /** function : executeNmiVBVCardForm(sfWebRequest $request)
     * @purpose : displaying NMI Visa form
     * @param : (sfWebRequest $request)
     * @return :
     * @author :
     * @date :
     * This function show registered card form for NMI/JNA...
     */
    public function executeNmiVBVCardForm(sfWebRequest $request) {

        /**
         * [WP: 085] => CR:124
         * Fetching requestId from session...
         */
        $this->requestId = $this->getUser()->getAttribute('requestId'); //$request->getParameter('requestId');
        ## Checking requestId exists...
        Functions::checkRequestIdTempering($this->requestId);

        $result = $this->checkPaymentAlreadyDone($this->requestId);
        switch ($result) {
            case 'serverdown':
                $this->getUser()->setFlash('notice', 'Oops!! Payment Server connection problem found!!!');
                $this->redirect('paymentGateway/gotoApplicantVault');
            case 'paymentalready':
                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
                $this->redirect('cart/list');
                break;
            case 'success':
                $this->redirect('cart/list');
                break;
            default:
                break;
        }//End of switch($result){...


        $applicantVaultValues = $request->getParameter('applicantVaultValues');
        $this->request_details = Doctrine::getTable('OrderRequestDetails')->find($this->requestId);

        ## Getting application type...
        $this->application_type = $this->getApplicationType($this->request_details);

        $this->cardType = $applicantVaultValues->getCardType();

        $paymentMode = $request->getParameter('paymentMode');

        /**
         * [WP: 111] => CR: 157
         * Added condition of MasterCard securecode for JNA service...
         */
        if ($paymentMode == 'jna_nmi_vbv' || $paymentMode == 'jna_nmi_mcs') {
            $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType($paymentMode);
            $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
            $this->paymentCardType = $paymentMode;
            sfContext::getInstance()->getUser()->setAttribute('sess_paymentMode_for_footer', '10,'.$paymentMode);            
        } else {
            sfContext::getInstance()->getUser()->getAttributeHolder('sess_paymentMode_for_footer');
            if ($this->cardType == 'V') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType('nmi_vbv');
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->paymentCardType = 'nmi_vbv';
            } else if ($this->cardType == 'M') {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType('nmi_mcs');
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->paymentCardType = 'nmi_mcs';
            } else {
                $paymentModeData = Doctrine::getTable('PaymentMode')->findByCardType('nmi_amx');
                $this->paymentMode = $paymentModeData->getFirst()->getDisplayName();
                $this->paymentCardType = 'nmi_amx';
            }
        }

        // start - change by vineet for making ZIP mandatory
        $country = '';
        if (isset($_REQUEST['ep_pay_easy_request']['country'])) {
            $country = $_REQUEST['ep_pay_easy_request']['country'];
        }

        //$_SESSION['cardType'] = $this->cardType;
        $request->setAttribute('mode', $this->cardType);

        $this->form = new EpNmiRequestForm('', array('task' => 'payment', 'argument' => array('country' => $country, 'isRegistered' => true)));

        $ipReq = (empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? (empty($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_CLIENT_IP']) : $_SERVER['HTTP_X_FORWARDED_FOR']);
        //start - vineet for country zip validaton
        $countryCode = Doctrine::getTable('countryBasedZip')->getAllCountryCode();
        $arrCountry = array();
        for ($i = 0; $i < count($countryCode); $i++) {
            $arrCountry[] = $countryCode[$i]['country_code'];
        }

        $this->arrCountry = $arrCountry;

        //end - vineet for country zip validaton
        $this->appCountry = $applicantVaultValues->getCountry();
        $this->expYear = $applicantVaultValues->getExpiryYear();
        $this->expMonth = $applicantVaultValues->getExpiryMonth();

        $this->form->setDefault('billing-first-name', $applicantVaultValues->getFirstName());
        $this->form->setDefault('billing-last-name', $applicantVaultValues->getLastName());
        $this->form->setDefault('billing-address1', $applicantVaultValues->getAddress1());
        $this->form->setDefault('billing-address2', $applicantVaultValues->getAddress2());
        $this->form->setDefault('billing-city', $applicantVaultValues->getTown());
        $this->form->setDefault('billing-state', $applicantVaultValues->getState());
        $this->form->setDefault('billing-postal', $applicantVaultValues->getZip());
        $this->form->setDefault('billing-country', $this->appCountry);
        $this->form->setDefault('billing-cc-exp', array("year" => $this->expYear, "month" => $this->expMonth));
        $this->form->setDefault('billing-email', $applicantVaultValues->getEmail());
        $this->form->setDefault('billing-phone', $applicantVaultValues->getPhone());
        $this->form->setDefault('card_holder', $applicantVaultValues->getCardHolder());
        $this->cardFirst = $applicantVaultValues->getCardFirst();
        $this->cardLast = $applicantVaultValues->getCardLast();
        $this->midlen = $applicantVaultValues->getCardLen() - (strlen($this->cardFirst) + strlen($this->cardLast));
        $this->form->setDefault('request_ip', $ipReq);

        $host = $request->getUriPrefix();
        $url_root = $request->getPathInfoPrefix();

        $this->formActionURL = $host . $url_root . '/nmi/paymentProceed';
    }

    /** function : getStatus($order_id, $epNmiManager, $midService)
     * @purpose : Get Status of a transaction made at JNA
     * @param : ($order_id, $epNmiManager, $midService)
     * @return : array
     * @author :
     * @date :
     */
    private function getStatus($order_id, $epNmiManager, $midService) {

        $requestString = "order_id=" . $order_id;
        $epNmiManager->createLogData($requestString, 'StatusRequest-' . $order_id, 'nmiRollbackLog');

        $url = sfConfig::get('app_nmi_' . $midService . '_query_url');

        $browser = $epNmiManager->getBrowser();
        $browser->post($url, $requestString);
        $code = $browser->getResponseCode();

        if ($code != 200) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            return false;
        }

        $respObj = simplexml_load_string($browser->getResponseText());

        if ($respObj) {
            $res = $epNmiManager->getArrayfrmXML($respObj, array());
            return
            /** function : getStatus($order_id, $epNmiManager, $midService)
             * @purpose : Get Status of a transaction made at JNA
             * @param : ($order_id, $epNmiManager, $midService)
             * @return : array
             * @author :
             * @date :
             */ $res;
        }
        return false;
    }

    /** function : checkPaymentAlreadyDone()
     * @purpose : check payment already done
     * @param :
     * @return : array
     * @author :
     * @date :
     */
    public function checkPaymentAlreadyDone($requestId='') {
        ## Getting payment gateway id && midService i.e, mid1 or mid2 or etc...
        $gateWayInfo = array();
        $gateWayInfo = $this->getPaymentGatewayId();
        $intGatewayId = $gateWayInfo['gatewayId'];
        $midService = $gateWayInfo['midService'];


        if (strtolower($midService) == 'jna1') {

            $url = sfConfig::get('app_nmi_' . strtolower($midService) . '_query_url');
            $headers = array();
            $headers = @get_headers($url);

            $userId = $this->getUser()->getGuardUser()->getId();

            if ($headers == '') {
                  $data  = "FormQueryURL:   $url\n";
                  $data .= "UserId:         $userId\n";
                  $data .= "RequestId:      $requestId\n";
                  $data .= "Date & Time:    ".date('Y-m-d H:i:s')."\n";
                  $data .= "Step:           Continue \n";
                  $logfile = 'FormRequestURL';
                  $epNmiManager = new EpNmiManager();
                  $epNmiManager->createLogData($data, $logfile, 'NmiServerDownLog');

                return 'serverdown';
            } else {
                /* start payment status from backend */

                $arrCartItems = $this->getUser()->getCartItems();

                $orderDetailArray = array();
                $orderNumberArray = array();
                $cartAppIdArray = array();
                $i = 0;
                foreach ($arrCartItems as $items) {
                    $appID = $items->getAppId();
                    $appType = strtolower($items->getType());
                    $refNo = '';
                    $cartAppIdArray[] = $appID;

                    ## Fetching order request detail id... ([WP: 079] => CR: 115)
                    $orderRequestDetails = Doctrine::getTable('TransactionServiceCharges')->getOrderRequestDetailId($appID, $appType);
                    if(count($orderRequestDetails) > 0){
                        foreach($orderRequestDetails AS $orderDetail){
                            ## Fetching order number and epJobId...
                            $ipay4meOrderDetails = Doctrine::getTable('Ipay4meOrder')->findByOrderRequestDetailId($orderDetail->getOrderRequestDetailId());
                            if(count($ipay4meOrderDetails) > 0){
                                foreach($ipay4meOrderDetails AS $ipay4meOrder){
                                    if ($ipay4meOrder->getGatewayId() == 10) {
                                        $orderNumberArray[$i]['order_id'] = $ipay4meOrder->getId();
                                        $orderNumberArray[$i]['order_number'] = $ipay4meOrder->getOrderNumber();
                                        $orderNumberArray[$i]['payment_status'] = $ipay4meOrder->getPaymentStatus();
                                        $orderNumberArray[$i]['gateway_id'] = $ipay4meOrder->getGatewayId();
                                        $orderNumberArray[$i]['order_request_id'] = $ipay4meOrder->getOrderRequestDetailId();
                                        $orderNumberArray[$i]['card_type'] = $ipay4meOrder->getCardType();
                                        $i++;
                                    }
                                }//End of foreach($ipay4meOrderDetails AS $ipay4meOrder){...
                            }//End of if(count($ipay4meOrderDetails) > 0){...
                        }//End of foreach($orderRequestDetails AS $orderDetail){...
                    }//End of if(count($orderRequestDetails) > 0){...
                }//End of foreach($arrCartItems as $items){...

                if (count($orderNumberArray) > 0) {

                    $epNmiManager = new EpNmiManager();
                    ## Setting gateway id and midservice to the class...
                    $epNmiManager->Gateway_id = $intGatewayId;
                    $epNmiManager->midService = $midService;

                    foreach ($orderNumberArray AS $order_number) {


                        $orderStatusArr = $this->getStatus($order_number['order_number'], $epNmiManager, $midService);

                        if (empty($orderStatusArr)) {
                            continue;
                        }

                        $orderStatus = $epNmiManager->getPaymentStatus($orderStatusArr);

                        switch ($orderStatus) {
                            case 'complete' :
                                if ($order_number['payment_status'] == 0) {
                                    return 'paymentalready';
                                } else if ($order_number['payment_status'] == 1) {

                                    $payManagerObj = new PaymentManager();
                                    $paymentResponseArray = array();
                                    $paymentResponseArray['response_code'] = '100';
                                    $paymentResponseArray['response_text'] = 'SUCCESS';
                                    $paymentResponseArray['card_type'] = $order_number['card_type'];

                                    $paymentResponseArray['status'] = 1;
                                    $orderObj = $payManagerObj->updatePaymentRequestNmi($order_number['order_id'], $paymentResponseArray);
                                    $updateOrder = $payManagerObj->updateOrderRequest($order_number['order_id'], $paymentResponseArray);

                                    if ($updateOrder->getPaymentStatus() == 0) {
                                        $updateOrder = $payManagerObj->updateSplitAmount($order_number['order_request_id'], $order_number['gateway_id']);
                                        $result = $payManagerObj->paymentSuccess($order_number['order_number']);

                                        $orderRequesDetailsArray = Doctrine_Core::getTable('OrderRequestDetails')->findById($order_number['order_request_id']);
                                        $appID = '';
                                        if (!empty($orderRequesDetailsArray)) {
                                            $retry_id = $orderRequesDetailsArray[0]['retry_id'];
                                            $cartDetailsArray = Doctrine_Core::getTable('CartItemsTransactions')->findByTransactionNumber($retry_id);
                                            if (!empty($cartDetailsArray)) {
                                                $cartDetailsArrayCount = count($cartDetailsArray);
                                                $appIDArray = array();
                                                for ($i = 0; $i < $cartDetailsArrayCount; $i++) {
                                                    $item_id = $cartDetailsArray[$i]['item_id'];
                                                    $applicationDetailsArray = Doctrine_Core::getTable('IPaymentRequest')->findById($item_id);
                                                    if (!empty($applicationDetailsArray)) {
                                                        if ($applicationDetailsArray[0]['passport_id'] != NULL) {
                                                            $appIDArray[] = $applicationDetailsArray[0]['passport_id'];
                                                        } elseif ($applicationDetailsArray[0]['visa_id'] != NULL) {
                                                            $appIDArray[] = $applicationDetailsArray[0]['visa_id'];
                                                        } elseif ($applicationDetailsArray[0]['freezone_id'] != NULL) {
                                                            $appIDArray[] = $applicationDetailsArray[0]['freezone_id'];
                                                        } else {
                                                            $appIDArray[] = $applicationDetailsArray[0]['visa_arrival_program_id'];
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        $clearCart = false;
                                        if (count($appIDArray) > 0) {

                                            $newAppIdArray = array_diff($cartAppIdArray, $appIDArray);

                                            if (count($newAppIdArray) > 0) {
                                                $this->getUser()->setFlash('notice', 'All Applications except <b>' . implode(',', $newAppIdArray) . '</b> has been already processed. Please click remove link to remove paid application from cart.');
                                            } else {
                                                $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
                                                $clearCart = true;
                                            }
                                        } else {
                                            $this->getUser()->setFlash('notice', 'Payment has already been made for this Item.');
                                            $clearCart = true;
                                        }

                                        if ($clearCart) {
                                            $userObj = $this->getUser();
                                            ###### START CLEAR CART ######
                                            $cartObj = new cartManager();
                                            $cartObj->emptyCart();
                                            $userObj->saveCartInDb();
                                            ###### END HERE ##############
                                        }

                                        return 'success';
                                    }
                                }
                                break;
                            default :
                                break;
                        }//End of switch ($orderStatus){...
                    }//End of foreach($orderDetailArray...
                }//End of if(count($orderDetailArray) > 0){...

                /* end payment status from backend */
            }
        }//End of if(strtolower($midService) == 'jna1'){...
    }//End of public function checkPaymentAlreadyDone(){...

    private function getApplicationType($request_details){
        $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($request_details->getRetryId());
        if (count($appDetails) == 1) {
            $application_type = strtolower($appDetails[0]['app_type']);
        }else{
            $application_type = '';
        }
        return $application_type;
    }
}
?>
