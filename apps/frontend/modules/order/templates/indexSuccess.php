<?php echo ePortal_pagehead(" "); ?>
<?php use_helper('Form') ?>

<?php echo form_tag('paymentSystem/advancedDetail',array('name'=>'pfm_nis_details_form','class'=>'dlForm multiForm', 'id'=>'pfm_nis_details_form', 'onsubmit' => 'return validate_nis_index_form(this)')) ?>
 <div class="wrapForm2">
        <?php
        echo ePortal_legend('SW Global LLC Bank Payment');
        echo formRowFormatRaw('Transaction Id:',input_tag('txnId'));
        echo formRowFormatRaw(' ',input_tag('','OR',array('style' => 'font-weight:bold;color:#056CB6;width:20px;', 'readonly' => true)));
        echo formRowFormatRaw('Application Id:',input_tag('appId'));
        echo formRowFormatRaw('Reference Number:',input_tag('refNo'));
        echo formRowFormatRaw('Application Type:',select_tag('type', options_for_select(array('NIS PASSPORT'=>'NIS Passport', 'NIS VISA'=>'NIS VISA'))));
        echo formRowFormatRaw(' ', submit_tag('Submit', array('id' => 'submit', 'name' => 'submit')));
        ?>    
  </div>
</form>