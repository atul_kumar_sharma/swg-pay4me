
<?php include_partial('global/innerHeading',array('heading'=>'Order Details'));
?>

<div class="global_content4 clearfix">
<table width="100%">

 <tr>
   <td class="blbar" colspan="2">Order date: <?php echo  $getDetails['pdate'];?>
   &nbsp;&nbsp; Order Number:<?php echo $receiptId; ?></td>
 </tr>
 <tr>
   <td width="62%" class="txtBold">Item</td>
   <td width="38%" class="txtBold txtRight">Price ($)</td>
 </tr>
 <tr>
   <td ><span class="txtBold"><?php echo $getDetails['mname'] ?>- </span><?php echo $getDetails['msname'] ?></td>
   <td class="txtRight"><?php echo $getDetails['amount'] ?></td>
 </tr>
 <tr class="blTxt">
   <td class="txtRight blTxt" colspan="2"><span class="bigTxt">Total: <?php echo $getDetails['amount']; ?> </span></td>
 </tr>

</table>

</div>

<?php
?>
