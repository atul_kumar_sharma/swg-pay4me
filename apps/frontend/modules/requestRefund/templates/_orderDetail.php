<?php $currencySymbol = CurrencyManager::currencySymbolByCurrencyId($currency) ?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
<div class="content_container">
<?php
use_helper('Form');
use_helper('Pagination');
use_javascript('jquery.form.js');
include_partial('global/innerHeading',array('heading'=>'Customer Support Request'));?>
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="alertBox" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>

<div class="clear"></div>

<div class="tmz-spacer"></div>
<table width="99%" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td class="blbar" colspan="3">Order Details -
      <?php //echo $request_details->getMerchant()->getAddress();?>
    </td>
  </tr>
  <tr>
    <td width="50%" class="txtBold">Order Number</td>
    <td width="50%" class="txtBold txtLeft">Amount</td>
  </tr>
  <tr>
    <td ><span class="">
      <?php  echo $orderNumber;?>
      </span></td>
    <td class="txtLeft"><?php echo format_amount($amount,1,0,$currencySymbol);?></td>
  </tr>
</table>
