


<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
  <div class="content_container">
<?php include_partial('global/innerHeading',array('heading'=>'Support Request Details'));?>
  <div class="clear"></div>
  <div class="tmz-spacer"></div>
    <div id="nxtPage">
      <form action = "" method ="post" name = "custmr_respns_form"  enctype="multipart/form-data" onsubmit="return validateForm();">
        <table border ="99%" width="99%">
          <tr>
            <td width="25%" class="txtBold">Order Number</td>
            <td width="50%" class="txtLeft"><?php echo $requestDetails->getFirst()->getOrderNumber(); ?></td>
          </tr>
          <tr>

            <td ><span class="txtBold"> Support Category</span></td>

            <td class="txtLeft"><?php echo $requestDetails->getFirst()->getSupportCategory();?></td>
          </tr>
          <tr>
            <td width="25%" class="txtBold"> Sub Support Category</td>
            <?php
           $subcategory = $requestDetails->getFirst()->getSubSupportCategory();
           if(isset ($subcategory) && $subcategory != '') { ?>

           <td width="50%" class="txtLeft"><?php echo $subcategory;?></td>
           <?php } else { ?>
           <td width="50%" class="txtLeft"><?php echo 'N/A';?></td>
           <?php } ?>
          </tr>
          <tr>
            <td width="25%" class="txtBold"> Status</td>
            <?php if($requestDetails->getFirst()->getStatus() == 'Pending' || $requestDetails->getFirst()->getStatus() == 'Assigned') { ?>
            <td width="50%" class="txtLeft"><?php echo 'Pending';?></td>
            <?php } else if($requestDetails->getFirst()->getStatus() == 'Responded') { ?>
            <td width="50%" class="txtLeft"><?php echo 'Replied';?></td>            
            <?php  } else  { ?>              
            <td width="50%" class="txtLeft"><?php echo 'Closed' ;?></td>
           <?php }?>
          </tr>
          </table>
          <?php
          //echo $form;die;
          $countComment = count($requestCommentsDetails);
          if($countComment >0){
            foreach($requestCommentsDetails as $details): ?>
            <div class="Sap"></div>
            <table class="CommentNew" width="100%">
      <tr>
        <td  valign="top" width="25%" class="txtBold">
          Customer Comment On <?php echo date_format(date_create($details['created_at']),'Y-m-d');?> :
        </td>
        <td valign="top" width="50%"><?php if($details['customer_comments']){ echo nl2br($details['customer_comments']) ;}else{ echo "N/A";}?></td>
      </tr>
      <tr>
        <td valign="top" class="txtBold" width="25%">SWGlobal Comment On <?php echo date_format(date_create($details['updated_at']),'Y-m-d');?> :</td>
        <td  valign="top" width="50%"><?php if($details['support_user_comments']){ $support_user_comments = $details['support_user_comments'];
                $support_user_comments = (strlen($support_user_comments) > 100)?substr($support_user_comments, 0, 100):$support_user_comments;
                echo nl2br($support_user_comments) ;}else{ echo "N/A";}?></td>

      </tr>
      </table><div class="Sap"></div>
          
          <?php endforeach; } else { ?>
          <tr><td colspan = "2" align="center"><b>No Any previous Comment from Anyside </b></td></tr> <?php } ?>

         <table width="99%">
          <?php if($requestDetails->getFirst()->getStatus() == "Responded") {?>
           

          <tr>
            <td><?php echo $form['customer_comments']->renderLabel(); ?><span class="red"></span></td>
            <td colspan="2"><?php echo $form['customer_comments']->render(); ?>
              <br>
              <div class="red" id="customerComment">
                <?php echo $form['customer_comments']->renderError();?>
              </div>
            </td>


          </tr>
          <div>
            <tr id ="documentUploadRow1" >
              <td ><?php echo $form['upload_document2']->renderLabel(); ?><span class="red"></span></td>
              <td colspan="2"><?php echo $form['upload_document2']->render(); ?>
                <div class="red" id="document1">
                  <?php echo $form['upload_document2']->renderError();?>
                </div>
              </td>
            </tr>
            <tr id ="documentUploadRow2">
              <td ><?php echo $form['upload_document3']->renderLabel(); ?><span class="red"></span></td>
              <td colspan="2"><?php echo $form['upload_document3']->render(); ?>
                <br>
                <div class="red" id="document2">
                  <?php echo $form['upload_document3']->renderError();?>
                </div>

              </td>
            </tr>
            <tr id ="documentUploadRow3">
              <td ><?php echo $form['upload_document4']->renderLabel(); ?><span class="red"></span></td>
              <td colspan="2"><?php echo $form['upload_document4']->render(); ?>
                <br>
                <div class="red" id="document3">
                  <?php echo $form['upload_document4']->renderError();?>
                </div>
              </td>
            </tr>
            <?php } elseif($requestDetails->getFirst()->getStatus() == "Pending" || $requestDetails->getFirst()->getStatus() == "Assigned" ){ ?>
            <tr>
              <td colspan="2" valign="middle" align="center"><b>Your request is in Pending Status.</b>
              </td>
            </tr>

              <?php } ?>       

              
          </div>
        </table>

        <?php echo $form->renderHiddenFields();?>
        <br/>
        <div align="center">       
        <?php if($requestDetails->getFirst()->getStatus() == "Responded"){?>
        <input type = "submit" class ="normalbutton" value ="Post Comments">&nbsp;&nbsp;
        <?php } ?>
        <input type = "button" class ="loginbutton" value ="Back" onclick = "javascript:history.go(-1)">
            </div>
      </form>
    </div>
<script type="text/javascript">
  function validateForm(){
    var textAreaValue = jQuery.trim($('#support_request_comments_customer_comments').val());
    if(textAreaValue == ''){
      alert('Please Entery any valid Comment');
      $('#support_request_comments_customer_comments').focus();
      return false;
    }
  }
  //$(document).ready(function(){
  //    $("#addmoredocument").click(function() {
  //       var newUploadRow = $(document.createElement('tr'));
  ////       newUploadRow.setAttribute('id', 1);
  //       newUploadRow.html('<tr><td><b>Upload Document:</b></td><td><input type = "file" name ="document1" class ="" id ="documentUpload1" ><input type ="button" value ="Upload More Document" class = "loginbutton" name = "addmoredocument" id = "addmoredocument"></td></tr>');
  //       newUploadRow.appendTo('#documentUploadRow');
  //
  //
  //    });
  //});

</script>

  </div>
</div>
<div class="content_wrapper_bottom"></div>

