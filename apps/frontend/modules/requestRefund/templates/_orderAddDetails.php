    <?php
    use_helper('Pagination');
    if($isFound) {
      $i = 1;
      ?>

    <div class="clearfix">
      <div id="nxtPage">
        <table width="99%" border="0" cellpadding="0" cellspacing="0" class="dataTable" ><br/>
          <tr class="blbar" colspan="2">
            <td class="blbar" align="left">Application Details </td>
            <td class="blbar" colspan="2" align="right">
            <span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total  <b><?php echo $pager->getNbResults(); ?></b>  results</span></td>
          </tr>

          <tr>
            <td width="9%"><span class="txtBold">Application type</span></td>
            <td width="9%"><span class="txtBold">App Id</span></td>
          </tr>
          <tbody>
            <?php
            if($pager->getNbResults()>0)
            {


              foreach ($pager->getResults() as $result):
//                          echo "<pre>";print_r($result);die;
              $app['passport_id'] = $result['passport_id'];
              $app['visa_id'] = $result['visa_id'];
              $app['freezone_id'] = $result['freezone_id'];
              $appDetails = $appObj->getApplicationDetails($app);
//                          echo "<pre>";print_R($appDetails);die;
              $finalStatus = $nisHelper->isAllAppReadyRefunded($sf_params->get('orderNumber'),$appDetails['id']);
              $finalStatus= $finalStatus->getRawValue();
              $fStatus = null;
              if(isset ($finalStatus) && is_array($finalStatus) && count($finalStatus)>0){
                $fStatus = $finalStatus['action'];
              }
            if(!$fStatus) { ?>
            <tr>
              <input type="hidden" name="chk_dfee[]" id="<?= "chk_dfee".$i; ?>" value="<?= $appDetails['id']."_".$appDetails['app_type'];?>" >
              <td width="9%"><span><?php echo strtoupper($appDetails['app_type']);?></span></td>
              <td width="10%"><span><?php echo $appDetails['id'];?></span></td>
            </tr>
            <?php }
            $i++;
            endforeach; ?>
            <tr>
              <td class="blbar" colspan="2" height="25px" align="right">
                <div class="paging pagingFoot">
                  <?php
                  echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?orderNumber='.$sf_params->get('orderNumber'));
                  ?>
                </div>
              </td>
            </tr>
            <?php } else { ?>
            <tr><td  align='center' class='error' colspan="11">No Results found</td></tr>
            <?php } ?>
          </tbody>
        </table>
        <br>

      </div>


    <?php

  } ?>
    </div>
