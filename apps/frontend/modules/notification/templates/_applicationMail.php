<?php use_helper('EPortal');?>
<b>Hello <?php echo ucwords($name); ?>,</b>
<br /><br />Thanks for successful submission of your application for NIS <?php echo $type;?> using  <b>SW Global LLC!<br /><br /></b>



<!--<br /><br />You have successfully done payment on SW Global LLC , transfers and other services (published terms of use apply).<br /><br/>
-->

<table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
  <tr>
    <td>
    <table style="background-color: rgb(229, 236, 249);" width="100%" border="0" cellpadding="2" cellspacing="0"><tbody>
<tr><td>Your application details are mentioned below. Please keep them safe. You may need them later.</td></tr>
       <!-- <tr>
            <td style="font-size: 83%;">&nbsp;Order date: <b><?php // echo $rDetails['paid_date'];?></b><br>&nbsp;SW Global LLC order number: <b><?php // echo $orderNumber;;?></b></td>
            <td style="padding-right: 15px;" nowrap="nowrap" align="right"></td></tr>-->
        </tbody></table>
    </td>
  </tr>
 <!-- <tr style="color:#00345A;background-color:#E7F0F7;">
    <td  colspan="4">Order Date: <b><?php  //echo $rDetails['paid_date'];?> </b></td>
  </tr>
   <tr style="color:#00345A;background-color:#E7F0F7;">
    <td  colspan="4">SW Global LLC order number:  <b><?php  //echo $orderNumber;;?></b> </td>
  </tr>
  -->
  <tr>
    <td>
        <table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(229, 236, 249);" width="100%" border="0" cellpadding="4" cellspacing="0">
            <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Name:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            <?php echo $app_details->getApplicantName();?></td>
                    </tr>



 <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Date of Birth:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            <?php echo date("Y-m-d",strtotime($app_details->getDateOfBirth()));?></td>
                    </tr>


                     <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Application Id:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            <?php echo $app_details->getId();?></td>
                    </tr>

                         <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Reference Number:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            <?php echo $app_details->getRefNo();?></td>
                    </tr>
    <?php if($rejected=='yes') { ?>
                    <tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Status:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
             <?php echo 'Rejected'; ?></td>
                    </tr>
    <?php } ?>

                         <!--<tr style="margin-right: 15px; margin-left: 5px;" valign="top">
                <td colspan="10" style="font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); font-weight: bold; color: black; width: 15%; text-align: left; padding-right: 1em;" nowrap="nowrap">&nbsp;<b>Processing Country:</b>&nbsp;</td>
                    <td colspan="50" style="font-family: Arial,Sans-Serif; font-size: 83%; border-bottom: 1px solid rgb(229, 236, 249); width: 15%;" align="left">
            <?php //echo $app_details->getProcessingCountryId();?></td>
                    </tr>-->



        </table>
    </td>
  </tr>
 </table>
<br><br>
<?php if($rejected=='no') {
  /* To find if an application is gratis or not? */
 if(!$isGratis){  ?>
Please note that your application is NOT yet paid. This application will be eligible for further processing at NIS (Nigeria Immigration Service) Portal only after payment.
<?php } else{  ?>
Please note that your application is Gratis which doesn't require payment. This application will be eligible for further processing at NIS (Nigeria Immigration Service) Portal.
<?php } }?>
<!-- <br>Please look out for our newsletter in your registered email with more information about ways to utilize your account.<br /><br />"SW Global LLC.....KEEPING IT SIMPLE" -->
<br /><br />



<?php if($app_type=='vap') { ?>
<br><br>
<strong>Note:</strong> Any change in arrival schedule should be updated on the SWGlobalLLC portal in advance to avoid unnecessary delay in the service on airport at the time of arrival. Kindly update the <strong>"Flight details & date of Arrival"</strong> from below navigation.<br />
<?php  echo link_to1("Application >> Update VOAP Arrival Details", $voapArrivalUrl); ?>
<br /><br />
<?php 
/** 
 * [WP: 080] => CR: 116
 */
if(!$isGratis){
$no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30; ?>
<strong>Your application will be valid till <?php echo $no_of_months; ?> <?php echo ($no_of_months > 1)?'months':'month';?> from the date of payment after that it will expire automatically.</strong>
<br /><br />
<?php } }?>

<?php echo $signature;?>


