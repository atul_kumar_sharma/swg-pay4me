<?php

/**
 *  paymentNotification actions.
 *
 * @package    mysfp
 * @subpackage  order
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class notificationActions extends sfActions {

    /**
     * Executes index actionvalidated params
     *
     * @param sfRequest $request A request object
     */
    public function executePaymentNotification(sfWebRequest $request) {
        $this->setLayout(NULL);
        sfView::NONE;
        $request_order_number = $request->getParameter('request_order_number');

        //$payForMeObj = Pay4MeIntServiceFactory::getService("V1");
        $TDetails = Doctrine::getTable('Response')->getOrderDetails($request_order_number);
        if ($TDetails) {
            $gateway = "authorize";
            $params['x_invoice_num'] = $request_order_number;
            $params['x_trans_id'] = $TDetails->getFirst()->getResponseTransactionCode();
            $paymentGatewayObj = PaymentGatewayServiceFactory::getService($gateway);
            $paymentResponse = $paymentGatewayObj->paymentRequest('PRIOR_AUTH_CAPTURE', $params);

            Doctrine::getTable('PaymentResponse')->savePaymentResponse($request_order_number, $paymentResponse);

            if (($paymentResponse['response_code'] == 1) && ($paymentResponse['response_reason'] == 1)) {
                $this->processOrder($request_order_number);
                return $this->renderText("Notification sent Successfully");
            }
        }
    }

    public function processOrder($request_order_number) {

        $version = "v1";
        $pay4MeIntObj = Pay4MeIntServiceFactory::getService($version);

        $url = $pay4MeIntObj->getNotificationUrl($request_order_number);

        $browser = $this->getBrowser();
        $header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";

        $xmlData = $pay4MeIntObj->generatePaymentNotificationXMLV1($request_order_number);



        $this->createLog($xmlData, 'PaymentNotification');

        $browser->post($url, $xmlData, $header);
        $code = $browser->getResponseCode();

        if ($code != 200) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            throw new Exception("Error in posting notification:" . PHP_EOL . $code . ' ' . $respMsg . PHP_EOL . $respText);
        } else {
            $responseObj = Doctrine::getTable('Request')->findOneByRequestOrderNumber($request_order_number);

            $requestObj = $responseObj->getOrderRequestDetails();
            $user_id = $responseObj->getUpdatedBy();
            $paid_date = date('Y-m-d h:i:s');
            $requestObj->setPaymentStatus('0');
            $requestObj->setPaidDate($paid_date);
            $requestObj->setUserId($user_id);
            $con = Doctrine_Manager::connection();
            try {
                $con->beginTransaction();
                $responseObj->save();
                $this->auditPaymentTransaction($request_order_number, $responseObj->getOrderRequestDetails()->getUserId(), $responseObj->getOrderRequestDetails()->getSfGuardUser()->getUsername());
                $taskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $this->moduleName . "/paymentSuccessMail", array('request_order_number' => $request_order_number));
                $this->logMessage("sceduled payment successful mail job with id: $taskId", 'debug');
                $con->commit();
            } catch (Exception $e) {
                $this->logMessage("Exception in processOrder " . $e);
                $con->rollback();
            }

            $this->logMessage('notification send');
        }
    }

    public function getBrowser() {
        if (!$this->browserInstance) {
            $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                            array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
    }

    public function createLog($xmldata, $nameFormate) {
        Pay4MeIntUtility::setLog($xmldata, $nameFormate);
    }

    public function executePaymentSuccessMail(sfWebRequest $request) { 
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'mailTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderDetailId = $request->getParameter('order_detail_id');
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);        
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        $reTryId = $requestDetails->getRetryId();
        $ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($reTryId);
        
        $orderNumber = $ipay4meOrder['order_number'];
        $url = $request->getParameter('url');
        $url1 = $request->getParameter('url1');

        $cardName = $requestDetails['name'];
        $card_last = $ipay4meOrder['card_last'];

        $card_len = $ipay4meOrder['card_len'];
        $card_type = $ipay4meOrder['card_type']; //'A';
        if ('' != $card_type) {
            if ('V' == strtoupper($card_type))
                $card_type = "VISA";
            if ('M' == strtoupper($card_type))
                $card_type = "MASTER";
            if ('A' == strtoupper($card_type))
                $card_type = "AMEX";
        }
        
        $paid_date = date('d-m-Y', strtotime($requestDetails->getPaidDate()));
        $merchantName = $requestDetails->getMerchant()->getName();
        $merchantAbbr = $requestDetails->getMerchant()->getAbbr();
        $merchantDescription = $requestDetails->getMerchant()->getDescription();
        $currencyId = $requestDetails->getCurrency();
        if($currencyId == '1') { // 1 means dollar...
            $amount = $requestDetails->getAmount();
        }else{
            $amount = $requestDetails->getConvertAmount();
        }

//        $ipayNisDetailsQuery = array('0' => array('id' => '116067','passport_id' => '4163999','visa_id' => '', 'freezone_id' =>'','created_at' => '2011-10-25 14:02:27','updated_at' => '2011-10-25 14:02:27','pid' => '4163999'));
        $sendArr = array('paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'cardLast' => $card_last, 'cardType' => $card_type, 'cardLen' => $card_len, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'cardName' => $cardName, 'url' => $url, 'url1' => $url1,'ipayNisDetailsQuery'=>$ipayNisAppDetails, 'currencyId' => $currencyId);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $requestDetails->getPaymentStatus();
        if ($payment_status == 0) {
            $user_id = $requestDetails->getUserId();            
            if ($user_id) {
                $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();                
                $user_email_address = $userDetailObj->getEmail();
                $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

                if (sfConfig::get('app_host') == "local") {
                    $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                    $signature = sfConfig::get('app_email_local_settings_signature');
                } else if (sfConfig::get('app_host') == "production") {
                    $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                    $signature = sfConfig::get('app_email_productionl_settings_signature');
                }
                $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
                $mailingOptions['mailSubject'] = 'Payment is done successfully for swgloballlc Order No '.$orderNumber;
                $mailingOptions['mailTo'] = $user_email_address;
                $mailingOptions['mailFrom'] = $mailFrom;

                $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);

                $gateway_id = $ipay4meOrder['gateway_id'];
                $cartIdArray = array();
                if($gateway_id != 6 && $gateway_id != 7){ // 6 for Mo and 7 for Wt
                    
                    $ipayNisAppDetailsCount = count($ipayNisAppDetails);                    
                    
                    for ($i = 0; $i < $ipayNisAppDetailsCount; $i++) {
                      $app_type = $ipayNisAppDetails[$i]['app_type'];
                      $appId = $ipayNisAppDetails[$i]['id'];
                      
                      ## Getting application details...
                      switch($app_type){
                          case 'passport':                              
                              $appIdArray = Doctrine::getTable('IPaymentRequest')->findByPassportId($appId);
                              break;
                          case 'visa':                              
                              $appIdArray = Doctrine::getTable('IPaymentRequest')->findByVisaId($appId);
                              break;
                          case 'vap':
                              $appIdArray = Doctrine::getTable('IPaymentRequest')->findByVisaArrivalProgramId($appId);
                              break;
                          default;                              
                              $appIdArray = Doctrine::getTable('IPaymentRequest')->findByFreezoneId($appId);
                              break;
                      }//End of switch($app_type){...

                      $appIdArrayCount = count($appIdArray);
                      if($appIdArrayCount > 0){
                          
                          $itemIdArray = array();

                          ## Getting ItemIds...
                          for($j=0;$j<$appIdArrayCount;$j++){
                            $itemIdArray[$j] = $appIdArray[$j]['id'];
                          }

                          ## Getting cartinfo from itemids...
                          $cartDetails = Doctrine::getTable('CartItemsTransactions')->getCartItemAllIdInfo($itemIdArray);
                          $cartDetailsCount = count($cartDetails);

                          ## Making array of cartIds...
                          for($j=0;$j<$cartDetailsCount;$j++){
                              $cartIdArray[] = $cartDetails[$j]['cart_id'];
                          }
                          
                      }//End of if($appIdArrayCount > 0){...
                      
                    }//End of for ($i = 0; $i...

                    $cartIdArray = array_unique($cartIdArray);                    
                    
                    ## Finding cartid exists in cartTrackingNumber table and if exists then set status 1 for delete field...
                    $cartIdArrayCount = count($cartIdArray);
                    for($i=0;$i<$cartIdArrayCount;$i++){
                        $cartObj = Doctrine::getTable('CartTrackingNumber')->findByCartId($cartIdArray[$i]);                        
                        if(count($cartObj) > 0){
                            ## Deleting tracking number...
                            Doctrine::getTable('CartTrackingNumber')->setTrackingNumberDeleteStatus($cartObj[0]['cart_id'], 1);

                            $cartTrackId = $cartObj[0]['id'];
                            $trackMoneyOrderObj = Doctrine::getTable('TrackingMoneyOrder')->findByCartTrackId($cartObj[0]['id']);
                            if(count($trackMoneyOrderObj) > 0){
                                ## Deleting money order number...
                                Doctrine::getTable('MoneyOrder')->setMoneyOrderDeleteStatus($trackMoneyOrderObj[0]['moneyorder_id'], 1);
                            }
                        }                        
                    }

                }//End of if($gateway_id != 6 && $gateway_id != 7){...

                return $this->renderText($mailInfo);
            }//End of if ($user_id) {...
        }//End of if ($payment_status == 0) {...
    }

    public function auditPaymentTransaction($order_number, $userId, $userName) {
        //Log audit Details
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $order_number, $order_number));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $order_number)),
                        $applicationArr, $userId, $userName);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function executePaymentNotificationV1(sfWebRequest $request) {
        $this->setLayout(NULL);
        sfView::NONE;
        $orderDetailId = $request->getParameter('order_detail_id');
        $this->sendNotification($orderDetailId);
        return $this->renderText("Notification sent Successfully");
    }

    function sendNotification($orderDetailId) {
        $version = "v1";
        $pay4MeIntObj = Pay4MeIntServiceFactory::getService($version);
        $detailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $url = $detailsObj->getMerchant()->getNotificationUrl();

        $browser = $this->getBrowser();
        $header['Content-Type'] = "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";

        $xmlData = $pay4MeIntObj->generatePaymentNotificationXMLV1($orderDetailId);

        $this->createLog($xmlData, 'PaymentNotification');

        $browser->post($url, $xmlData, $header);
        $code = $browser->getResponseCode();

        $this->logMessage('notification code: '.$code);

        if ($code != 200) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            throw new Exception("Error in posting notification:" . PHP_EOL . $code . ' ' . $respMsg . PHP_EOL . $respText);
        } else {
            /**
             * [WP: 087] => CR: 126
             * Updating passport and visa table...
             */            
            $transObj = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($detailsObj->getId());
            if(count($transObj) > 0){
                foreach($transObj AS $appDetails){
                    $appObj = Doctrine::getTable('TransactionServiceCharges')->getApplicationDetails($appDetails->getAppId(), $appDetails->getAppType(), $detailsObj->getId());
                    $appCurrency = $appObj->getFirst()->getAppCurrency();                    
                    $appType = strtolower($appDetails->getAppType());                    
                    if($appCurrency != 1){
                        switch($appType){
                            case 'passport':
                                $passportObj = Doctrine::getTable('PassportApplication')->find($appDetails->getAppId());                                
                                if(!empty($passportObj)){
                                    /**
                                     * [WP: 102] => CR: 145
                                     * Updated amount excluded service charges...
                                     */
                                    $appAmount = $appObj->getFirst()->getAppConvertAmount() - $appObj->getFirst()->getAppConvertServiceCharge();
                                    $passportObj->setAmount($appAmount);
                                    $passportObj->setCurrencyId($appCurrency);
                                    $passportObj->save();
                                }
                                break;
                            case 'visa':
                                $visaObj = Doctrine::getTable('VisaApplication')->find($appDetails->getAppId());
                                if(!empty($visaObj)){
                                    /**
                                     * [WP: 102] => CR: 145
                                     * Updated amount excluded service charges...
                                     */
                                    $appAmount = $appObj->getFirst()->getAppConvertAmount() - $appObj->getFirst()->getAppConvertServiceCharge();
                                    $visaObj->setAmount($appAmount);
                                    $visaObj->setCurrencyId($appCurrency);
                                    $visaObj->save();
                                }
                                break;
                            default:
                                break;
                        }
                    }//End of if($appCurrency != 1){...
                }//End of foreach($transObj AS $appDetails){...
            }//End of if(count($transObj) > 0){...

            $this->logMessage('notification send');
        }
    }

    public function executeMailOnPaymentCrash(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'mailOnPaymentCrash';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderNumber = $request->getParameter('order_number');
        $responseXML = $request->getParameter('responseXML');
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);

        $cardName = $ipay4meOrder->getFirst()->getPayorName();
        $card_last = $ipay4meOrder->getFirst()->getCardLast();
        $card_type = $ipay4meOrder->getFirst()->getCardType();
        $response_txt = $ipay4meOrder->getFirst()->getResponseText();
        if ('V' == strtoupper($card_type))
            $card_type = "VISA";
        if ('M' == strtoupper($card_type))
            $card_type = "MASTER";

        $sendArr = array('cardLast' => $card_last, 'cardType' => $card_type, 'cardName' => $cardName, 'response_txt' => $response_txt);
        $rDetails = base64_encode(serialize($sendArr));

        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_productionl_settings_signature');
        }
        $partialVars = array('orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails, 'responseXML' => base64_encode(serialize($responseXML)));
        $mailingOptions['mailSubject'] = 'Undesirable behaviour from payment gateway';
        $mailingOptions['mailTo'] = sfConfig::get('app_mail_payment_failure');
        if (sfConfig::get('app_mail_payment_failure_cc') != '')
            $mailingOptions['mailCc'] = sfConfig::get('app_mail_payment_failure_cc');
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);
    }

    public function executeAppSubmissionMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'applicationMail';
        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        $app_id = $request->getParameter('app_id');
        $app_type = $request->getParameter('app_type');
        $user_id = $request->getParameter('user_id');
        $sitePath = $request->getParameter('sitePath');
        
        //get user's email address
        $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
        $userDetailObj = $userObj->getUserDetail();
        $userEmail = $userDetailObj->getEmail();
        $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

        if ($app_type == 'p') {
            $table = "PassportApplication";
            $type = "Passport";
        } elseif ($app_type == 'v') {
            $table = "VisaApplication";
            $type = "Visa";
        }elseif ($app_type == 'vap') {
            $table = "VapApplication";
            $type = "Visa Arrival";
        }

        $app_details = Doctrine::getTable($table)->find($app_id);
        /**
         * [WP:083] => CR:121
         * To find out if application is gratis or not?
         */
        if($app_type == 'vap' || $app_type == 'v'){
            $isGratis = $app_details->isAppGratis();
        }else{
            $isGratis = false;
        }
        $rejected = "no";
        if($app_type == 'vap'){
            if($app_details['contagious_disease'] == 'Yes' || $app_details['police_case'] == 'Yes' || $app_details['narcotic_involvement'] == 'Yes'){
               $rejected = "yes";
            }
        }

        if ($user_id) {
            $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
            $userDetailObj = $userObj->getUserDetail();
            $userEmail = $userDetailObj->getEmail();
            $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();




            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }

            //sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            //echo $url = url_for('visaArrival/searchFlightDetail', true);

            $url = $sitePath.'/visaArrival/searchFlightDetail';
            
            $partialVars = array('name' => $applicant_name, 'signature' => $signature, 'app_details' => $app_details, 'type' => $type,'rejected'=>$rejected, 'app_type' => $app_type, 'voapArrivalUrl' => $url,'isGratis'=>$isGratis);
            $mailingOptions['mailSubject'] = 'NIS Application Notification';
            $mailingOptions['mailTo'] = $userEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            return $this->renderText($mailInfo);
        }
    }

    public function executePaymentRefundMailUser(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'refundMailTemplate';
        $mailingOptions = array();
        $orderDetailId = $request->getParameter('orderRequestDetailId');
        $totalRefund = $request->getParameter('totalRefund');
        $orderNumber = $request->getParameter('orderNumber');
        //$orderDetailId =;
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        //    $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        //
        //    $orderNumber = $ipay4meOrder['order_number'];

        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            /**
             * [WP:087] => CR: 126
             * China Currency related changes...
             */
            $currencyId = $requestDetails->getCurrency();
            if($currencyId == 1){
                $amount = $requestDetails->getAmount();
            }else{
                $amount = $requestDetails->getConvertAmount();
            }
            $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('orderNumber' => $orderNumber, 'signature' => $signature, 'name' => $name, 'merchantName' => $merchantName, 'amount' => $totalRefund, 'currencyId' => $currencyId);
            $mailingOptions['mailSubject'] = 'Payment refund notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executePaymentRefundMailAdmin(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'refundAdminMailTemplate';
        $mailingOptions = array();
        $orderDetailId = $request->getParameter('orderRequestDetailId');
        $totalRefund = $request->getParameter('totalRefund');
        $orderNumber = $request->getParameter('orderNumber');
        //$orderDetailId =;
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);

        $user_id = $requestDetails->getUpdatedBy();
        if ($user_id) {
            $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id)->getFirst();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            /**
             * [WP: 087] => CR: 126
             * China currency related changes...
             */
            $currencyId = $requestDetails->getCurrency();
            if($currencyId == 1){
                $amount = $requestDetails->getAmount();
            }else{
                $amount = $requestDetails->getConvertAmount();
            }
            $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('orderNumber' => $orderNumber, 'signature' => $signature, 'name' => $name, 'merchantName' => $merchantName, 'amount' => $totalRefund, 'currencyId' => $currencyId);
            $mailingOptions['mailSubject'] = 'Payment refund notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executePaymentRefundRequestMailUser(sfWebRequest $request) {


        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'userRefundMailTemplate';
        $mailingOptions = array();
        $orderNumber = $request->getParameter('orderNumber');

        $concatinatedMsg = $request->getParameter('concatinatedMsg');
        $mailInfo = 'Mail sent successfully';

        $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);

        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);

        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            $amount = $requestDetails->getAmount();
            $merchantName = $requestDetails->getMerchant()->getName();
            //$amount = $requestDetails->getAmount();
            //$merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('concatinatedMsg' => $concatinatedMsg, 'signature' => $signature, 'name' => $name);
            $mailingOptions['mailSubject'] = 'Support notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executePaymentRefundRequestMailAdmin(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'adminRefundMailTemplate';
        $mailingOptions = array();
        $orderNumber = $request->getParameter('orderNumber');
        $concatinatedMsg = $request->getParameter('concatinatedMsg');
        $mailInfo = 'Mail sent successfully';
        $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);

        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);


        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();

            //$user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            $amount = $requestDetails->getAmount();
            $merchantName = $requestDetails->getMerchant()->getName();
            //$amount = $requestDetails->getAmount();
            //$merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }

            if (sfConfig::get('app_host') == "local") {
                $mailTo = sfConfig::get('app_support_email');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailTo = sfConfig::get('app_support_email');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('concatinatedMsg' => $concatinatedMsg, 'signature' => $signature, 'name' => $name);
            $mailingOptions['mailSubject'] = 'Support notification';
            $mailingOptions['mailTo'] = $mailTo;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executeRefundMailVault(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'refundMailVaultTemplate';
        $mailingOptions = array();
        $orderDetailId = $request->getParameter('orderRequestDetailId');
        $totalRefund = $request->getParameter('totalRefund');
        $orderNumber = $request->getParameter('orderNumber');
        //$orderDetailId =;
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        //    $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        //
        //    $orderNumber = $ipay4meOrder['order_number'];

        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            $amount = $requestDetails->getAmount();
            $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('orderNumber' => $orderNumber, 'signature' => $signature, 'name' => $name, 'merchantName' => $merchantName, 'amount' => $totalRefund);
            $mailingOptions['mailSubject'] = 'Payment refund notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executeRefundAdminVaultMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'refundAdminVaultMailTemplate';
        $mailingOptions = array();
        $orderDetailId = $request->getParameter('orderRequestDetailId');
        $totalRefund = $request->getParameter('totalRefund');
        $orderNumber = $request->getParameter('orderNumber');
        //$orderDetailId =;
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        //    $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        //
        //    $orderNumber = $ipay4meOrder['order_number'];

        $user_id = $requestDetails->getUpdatedBy();
        if ($user_id) {
            $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id)->getFirst();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
            $amount = $requestDetails->getAmount();
            $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('orderNumber' => $orderNumber, 'signature' => $signature, 'name' => $name, 'merchantName' => $merchantName, 'amount' => $totalRefund);
            $mailingOptions['mailSubject'] = 'Payment refund notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executeEmailToNisApplicant(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'emailToNisApplicantTemplate';
        $mailingOptions = array();

        $appId = $request->getParameter('appId');
        $appType = $request->getParameter('appType');
        $name = "";
        $email = "";
        $appliedDate = "";
        $refrenceNumber = "";
        $dob = "";
        $interview = "";
        $refundType = "";
        $refundDate = "";
        switch ($appType) {
            case "passport" :
                $passportAppObj = PassportApplicationTable::getInstance();
                $appObj = $passportAppObj->find($appId);
                $name = $appObj->getFirstName() . " " . $appObj->getMidName() . " " . $appObj->getLastName();
                $email = $appObj->getEmail();
                $appliedDate = $appObj->getCreatedAt();
                $refrenceNumber = $appObj->getRefNo();
                $dob = $appObj->getDateOfBirth();
                $interview = $appObj->getInterviewDate();
                break;

            case "visa":
            case "freezone":
                $visaAppObj = VisaApplicationTable::getInstance();
                $appObj = $visaAppObj->find($appId);
                $name = $appObj->getOtherName() . " " . $appObj->getMiddleName() . " " . $appObj->getSurname();
                $email = $appObj->getEmail();
                $appliedDate = $appObj->getCreatedAt();
                $refrenceNumber = $appObj->getRefNo();
                $dob = $appObj->getDateOfBirth();
                $interview = $appObj->getInterviewDate();
                break;
            case "vap":
                $visaAppObj = VapApplicationTable::getInstance();
                $appObj = $visaAppObj->find($appId);
                $name = $appObj->getFirstName() . " " . $appObj->getMiddleName() . " " . $appObj->getSurname();
                $email = $appObj->getEmail();
                $appliedDate = $appObj->getCreatedAt();
                $refrenceNumber = $appObj->getRefNo();
                $dob = $appObj->getDateOfBirth();
                //$interview = $appObj->getInterviewDate();
                break;
        }

        $refundDetails = RollbackPaymentDetailsTable::getInstance()->getRefundType($appId, $appType);
        $refundType = $refundDetails[0]['action'];
        $refundDate = $refundDetails[0]['created_at'];

        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_productionl_settings_signature');
        }
        $partialVars = array('name' => $name, 'appId' => $appId, 'appType' => $appType, 'appliedDate' => $appliedDate, 'refrenceNumber' => $refrenceNumber, 'dob' => $dob,
            'interview' => $interview, 'refundType' => $refundType, 'refundDate' => $refundDate, 'signature' => $signature);
        $mailingOptions['mailSubject'] = 'Payment refund notification';
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    public function executeCreditCardRequestMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'creditCardAddRequest';
        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        $request_id = $request->getParameter('request_id');
        $user_id = $request->getParameter("user_id");
        //get user's email address

        $card_details = Doctrine::getTable("ApplicantVault")->find($request_id);


        if ($user_id) {
            $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
            $userDetailObj = $userObj->getUserDetail();
            $userEmail = $userDetailObj->getEmail();
            $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();




            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('name' => $applicant_name, 'signature' => $signature, 'app_details' => $card_details, "action" => "add");
            $mailingOptions['mailSubject'] = 'SW Global LLC: Application submission confirmation';
            $mailingOptions['mailTo'] = $userEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            return $this->renderText($mailInfo);
        }
    }

    public function executeCreditCardProcessMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'creditCardAddRequest';
        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        $request_id = $request->getParameter('request_id');
        $user_id = $request->getParameter("user_id");
        $action = $request->getParameter("paction");
        $comments = $request->getParameter("comments");
        //get user's email address

        $card_details = Doctrine::getTable("ApplicantVault")->find($request_id);


        if ($user_id) {
            $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
            $userDetailObj = $userObj->getUserDetail();
            $userEmail = $userDetailObj->getEmail();
            $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

            $user_limit = Doctrine::getTable('UserTransactionLimit')->findByUserId($user_id);
            $cart_capacity = "";
            $cart_amount = "";
            $transaction = "";
            if ($user_limit->getFirst()) {
                $cart_capacity = $user_limit->getFirst()->getCartCapacity();
                $cart_amount = $user_limit->getFirst()->getCartAmountCapacity();
                $transaction = $user_limit->getFirst()->getNumberOfTransaction();
            }


            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('name' => $applicant_name, 'signature' => $signature, 'app_details' => $card_details, "action" => $action, 'cart_capacity' => $cart_capacity, 'cart_amount' => $cart_amount, 'transaction' => $transaction);
            if ($action == "Approved")
                $mailingOptions['mailSubject'] = 'SW Global LLC: Application to register your Credit Card: Approved';
            if ($action == "Rejected")
                $mailingOptions['mailSubject'] = 'SW Global LLC: Application to register your Credit Card: Rejected';
            $mailingOptions['mailTo'] = $userEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            return $this->renderText($mailInfo);
        }
    }

    public function executeTrackingMoneyOrderMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'emailMoneyOrderTemplate';
        $mailingOptions = array();

        $moneyOrderId = $request->getParameter('money_order_id');
        $user_id = $request->getParameter('user_id');
        $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
        $userDetailObj = $userObj->getUserDetail();
        $email = $userDetailObj->getEmail();
        $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

        $moneyOrderDetails = MoneyorderTable::getInstance()->find($moneyOrderId)->toArray();
        $trackingDetails = TrackingMoneyorderTable::getInstance()->getMoneyOrderDetails($moneyOrderId);
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];                    
                    if($orderDetail['currency'] == '1'){
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    }else{
                        $applicationDetails[$i]['cart_amount'] = $orderDetail['convert_amount'];
                    }
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                }
                $i++;
            }
            //echo '<pre>';print_r($applicationDetails);die;
        }


        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_productionl_settings_signature');
        }
        $partialVars = array('name' => $applicant_name, 'moneyOrderDetails' => $moneyOrderDetails, 'trackingDetails' => $applicationDetails, 'signature' => $signature);
        $mailingOptions['mailSubject'] = 'Money Order notification';
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    public function executeMoneyOrderPaymentSuccessMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'mailMoneyOrderTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderDetailId = $request->getParameter('order_detail_id');
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);
        $reTryId = $requestDetails->getRetryId();
        $ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($reTryId);
        $orderNumber = $ipay4meOrder['order_number'];
        $url = $request->getParameter('url');
        $url1 = $request->getParameter('url1');


        $paid_date = date('d-m-Y', strtotime($requestDetails->getPaidDate()));
        $merchantName = $requestDetails->getMerchant()->getName();
        $merchantAbbr = $requestDetails->getMerchant()->getAbbr();
        $merchantDescription = $requestDetails->getMerchant()->getDescription();
        $amount = $requestDetails->getAmount();
        $convert_amount = $requestDetails->getConvertAmount();
        $currency = $requestDetails->getCurrency();


        $sendArr = array('paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'convert_amount' => $convert_amount, 'currency' => $currency, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url, 'url1' => $url1, 'ipayNisAppDetails'=>$ipayNisAppDetails);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $requestDetails->getPaymentStatus();
        if ($payment_status == 0) {
            $user_id = $requestDetails->getUserId();
            if ($user_id) {
                $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
                $user_email_address = $userDetailObj->getEmail();
                $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

                if (sfConfig::get('app_host') == "local") {
                    $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                    $signature = sfConfig::get('app_email_local_settings_signature');
                } else if (sfConfig::get('app_host') == "production") {
                    $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                    $signature = sfConfig::get('app_email_productionl_settings_signature');
                }
                $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
                $mailingOptions['mailSubject'] = 'Money Order Payment is done successfully for swgloballlc Order No '.$orderNumber;
                $mailingOptions['mailTo'] = $user_email_address;
                $mailingOptions['mailFrom'] = $mailFrom;

                $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
                return $this->renderText($mailInfo);
            }
        }
    }

    public function executeTrackingWireTransferMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailInfo = 'Mail sent successfully';
        $mailingClass = new Mailing();
        $partialName = 'emailWireTransferTemplate';
        $mailingOptions = array();
        $wireTransferId = $request->getParameter('wire_transfer_id');
        $user_id = $request->getParameter('user_id');
        $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
        $userDetailObj = $userObj->getUserDetail();
        $email = $userDetailObj->getEmail();
        $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
        $wireTransferDetails = WiretransferTable::getInstance()->find($wireTransferId)->toArray();
        $trackingDetails = WireTrackingNumberTable::getInstance()->getWireTransferDetails($wireTransferId);
        if (isset($trackingDetails) && is_array($trackingDetails) && count($trackingDetails) > 0) {
            $i = 0;
            foreach ($trackingDetails as $orderDetail) {
                //get retry id
                $retryId = OrderRequestDetailsTable::getInstance()->find($orderDetail['order_request_detail_id'])->toArray();

                $this->ipayNisAppDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($retryId['retry_id']);
                foreach ($this->ipayNisAppDetails as $key => $app) {

                    $applicationDetails[$i]['tracking_number'] = $orderDetail['tracking_number'];
                    $applicationDetails[$i]['cart_amount'] = $orderDetail['cart_amount'];
                    $applicationDetails[$i]['order_number'] = $orderDetail['order_number'];
                    $applicationDetails[$i]['app_detail'][$key]['app_id'] = $app['id'];
                    $applicationDetails[$i]['app_detail'][$key]['ref_no'] = $app['ref_no'];
                    $applicationDetails[$i]['app_detail'][$key]['app_type'] = $app['app_type'];
                }
                $i++;
            }
        }


        if (sfConfig::get('app_host') == "local") {
            $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
            $signature = sfConfig::get('app_email_local_settings_signature');
            $account = sfConfig::get('app_email_local_settings_account');
            $accountName = sfConfig::get('app_email_local_settings_account_name');
        } else if (sfConfig::get('app_host') == "production") {
            $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
            $signature = sfConfig::get('app_email_production_settings_signature');
            $account = sfConfig::get('app_email_production_settings_account');
            $accountName = sfConfig::get('app_email_local_settings_account_name');
        }
        $partialVars = array('name' => $applicant_name, 'wireTransferDetails' => $wireTransferDetails, 'trackingDetails' => $applicationDetails, 'signature' => $signature, 'account' => $account, 'accountName' => $accountName);
        $mailingOptions['mailSubject'] = 'Wire Transfer notification';
        $mailingOptions['mailTo'] = $email;
        $mailingOptions['mailFrom'] = $mailFrom;

        $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    public function executeWireTransferPaymentSuccessMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'mailWireTransferTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
        $orderDetailId = $request->getParameter('order_detail_id');
        $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findSuccessfulPaymentDetail($orderDetailId);

        $orderNumber = $ipay4meOrder['order_number'];
        $url = $request->getParameter('url');
        $url1 = $request->getParameter('url1');


        $paid_date = date('d-m-Y', strtotime($requestDetails->getPaidDate()));
        $merchantName = $requestDetails->getMerchant()->getName();
        $merchantAbbr = $requestDetails->getMerchant()->getAbbr();
        $merchantDescription = $requestDetails->getMerchant()->getDescription();
        $amount = $requestDetails->getAmount();


        $sendArr = array('paid_date' => $paid_date, 'merchant_name' => $merchantName, 'amount' => $amount, 'mabbr' => $merchantAbbr, 'mdesc' => $merchantDescription, 'url' => $url, 'url1' => $url1);
        $rDetails = base64_encode(serialize($sendArr));

        $payment_status = $requestDetails->getPaymentStatus();
        if ($payment_status == 0) {
            $user_id = $requestDetails->getUserId();
            if ($user_id) {
                $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
                $user_email_address = $userDetailObj->getEmail();
                $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

                if (sfConfig::get('app_host') == "local") {
                    $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                    $signature = sfConfig::get('app_email_local_settings_signature');
                } else if (sfConfig::get('app_host') == "production") {
                    $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                    $signature = sfConfig::get('app_email_productionl_settings_signature');
                }
                $partialVars = array('name' => $name, 'orderNumber' => $orderNumber, 'signature' => $signature, 'request_details' => $rDetails);
                $mailingOptions['mailSubject'] = 'Wire Transfer Payment is done successfully';
                $mailingOptions['mailTo'] = $user_email_address;
                $mailingOptions['mailFrom'] = $mailFrom;

                $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
                return $this->renderText($mailInfo);
            }
        }
    }

    public function executeVbvPaymentByCron(sfWebRequest $request) {
        $numRecord = $request->getParameter('limit');
        $vbVManager = new VbVManager();
        $vbVManager->paymentByCron($numRecord);
        return $this->renderText('Cron is executed');
    }

    public function executeAddVbvCron(sfWebRequest $request) {
        $limit = $request->getParameter('limit');
        EpjobsContext::getInstance()->addRepetableJob('VbvCron', 'notification/vbvPaymentByCron', "2020-3-12 12:12:12", array('limit' => $limit), null, null, null, '*/5');
        return $this->renderText('Job added successfully');
    }

    /**
     * Function to send mail on update of User Transaction Limit
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeUpdateUserTransactionLimitMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'updateUserTransactionLimitMail';
        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        $user_id = $request->getParameter("user_id");
        $cart_capacity = $request->getParameter('cart_capacity');
        $cart_amount_capacity = $request->getParameter('cart_amount_capacity');
        $number_of_transaction = $request->getParameter('number_of_transaction');
        $transaction_period = $request->getParameter('transaction_period');
        ;

        //get user's email address

        if ($user_id) {
            $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
            $userDetailObj = $userObj->getUserDetail();
            $userEmail = $userDetailObj->getEmail();
            $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('name' => $applicant_name, 'signature' => $signature, 'cart_capacity' => $cart_capacity, 'cart_amount_capacity' => $cart_amount_capacity, 'number_of_transaction' => $number_of_transaction, 'transaction_period' => $transaction_period);
            $mailingOptions['mailSubject'] = 'SW Global LLC: User Transaction Limit Updated';
            $mailingOptions['mailTo'] = $userEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            return $this->renderText($mailInfo);
        }
    }

    /**
     * Function to send email on addition of user transaction limit
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeAddUserTransactionLimitMail(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'addUserTransactionLimitMail';
        $mailingOptions = array();
        $mailInfo = 'Mail sent successfully';
        $user_id = $request->getParameter("user_id");
        //get user's email address

        if ($user_id) {
            $userObj = Doctrine::getTable('sfGuardUser')->find($user_id);
            $userDetailObj = $userObj->getUserDetail();
            $userEmail = $userDetailObj->getEmail();
            $applicant_name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();

            $user_limit = Doctrine::getTable('UserTransactionLimit')->findByUserId($user_id);
            $cart_capacity = "";
            $cart_amount_capacity = "";
            $number_of_transaction = "";
            $transaction_period = "";
            if ($user_limit->getFirst()) {
                $cart_capacity = $user_limit->getFirst()->getCartCapacity();
                $cart_amount_capacity = $user_limit->getFirst()->getCartAmountCapacity();
                $number_of_transaction = $user_limit->getFirst()->getNumberOfTransaction();
                $transaction_period = $user_limit->getFirst()->getTransactionPeriod();
            }


            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('name' => $applicant_name, 'signature' => $signature, 'cart_capacity' => $cart_capacity, 'cart_amount_capacity' => $cart_amount_capacity, 'number_of_transaction' => $number_of_transaction, 'transaction_period' => $transaction_period);
            $mailingOptions['mailSubject'] = 'SW Global LLC: User Transaction Limit Added';
            $mailingOptions['mailTo'] = $userEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            return $this->renderText($mailInfo);
        }
    }

    public function executeDeleteApplicantProfileFiles(sfWebRequest $request) {
        $dir = sfConfig::get('sf_upload_dir') . '/applicant_profile/';
        $daysToDelete = sfConfig::get('app_delete_old_file_days');
        $daysToDelete = -$daysToDelete . ' days';
        $message = "";
        $deleteCount = 0;

        /*         * *************** Delete N day old files *************** */

        if ($dh = opendir($dir)) {
            while ($file = readdir($dh)) {
                if (!is_dir($dir . $file)) {
                    if (filemtime($dir . $file) < strtotime($daysToDelete)) {       //if N days old, delete
                        $message .= "Deleting $dir.$file (old)<br />";
                        unlink($dir . $file);
                        $deleteCount += 1;
                    }
                }
            }
        } else {
            $message = "ERROR. Could not open directory: $dir\n";
        }

        closedir($dh);
        $message = "Total deleted file(s) count is $deleteCount <br />" . $message;
        return $this->renderText($message);
    }

    public function executeAddJobToDeleteApplicantProfileFiles(sfWebRequest $request) {
        EpjobsContext::getInstance()->addJobForEveryDay('deleteApplicantProfileFiles', 'notification/deleteApplicantProfileFiles', "2020-3-12 12:12:12", null, null, null, null, "30", "07");
        return $this->renderText('Job To Delete Applicant\'s Profile Files added successfully');
    }

    public function executeProceedPaymentByCron(sfWebRequest $request) {
        ## Layout will not be called by action...
        $this->setLayout(null);
        ## Getting cature id...
        $captureDataId = $request->getParameter('captureDataId');
        ## Findind capture data form capture id...
        $captureObj = Doctrine::getTable('EpGrayPayCaptureData')->find($captureDataId);
        ## If capture data found then...
        if (!empty($captureObj)) {
            $amount = $captureObj->getAmount();
            $transactionId = $captureObj->getTransactionid();
            $order_number = $captureObj->getOrderid();
            $midService = $captureObj->getMidService();
            $captureResponse = $this->grayPayCapturePayment($amount, $transactionId, $midService);

            parse_str($captureResponse['text'], $output);
            $this->createLogData($captureResponse['text'], 'Re-Capture' . $order_number);
            $respText = $output['responsetext'];

            if ($output['response_code'] == '100') {
                $status = 'success';
            } else {
                $status = 'fail';
                $sendMailUrl = "notification/mailOnPaymentCrash";
                $taskId = EpjobsContext::getInstance()->addJob('PaymentErrorMail', $sendMailUrl, array('order_number' => $order_number, 'responseXML' => $respText));
                sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
            }

            ## Updated table with status...
            Doctrine::getTable('EpGrayPayCaptureData')->update($captureDataId, $status);

            $msg = "Re-Capture: " . $respText;
        } else {
            $msg = 'Re-Capture id did not find.';
        }

        return $this->renderText($msg);
    }

    private function grayPayCapturePayment($amount, $transactionId, $midService) {
        if ($midService) {
            $username = sfConfig::get('app_marine_' . $midService . '_username');
            $password = sfConfig::get('app_marine_' . $midService . '_password');
            $url = sfConfig::get('app_marine_' . $midService . '_url');
        } else {
            $username = sfConfig::get('app_marine_mid1_username');
            $password = sfConfig::get('app_marine_mid1_password');
            $url = sfConfig::get('app_marine_mid1_url');
        }

        $manadatoryLogString = "username=" . $username . "&password=" . $password . "&type=capture&transactionid=" . $transactionId . "&amount=" . $amount;

        $browser = $this->getBrowser();
        $browser->post($url, $manadatoryLogString);
        $code = $browser->getResponseCode();
        $respText = $browser->getResponseText();
        $responaseArr = array();
        $responaseArr['code'] = $code;
        $responaseArr['text'] = $respText;
        return $responaseArr;
    }

    public function createLogData($xmlData, $nameFormate, $parentLogFolder='', $appendTime=true, $append = false) {
        $path = $this->getLogPath($parentLogFolder);
        if ($appendTime) {
            $file_name = $path . "/" . ($nameFormate . '-' . date('Y_m_d_H:i:s')) . ".txt";
        } else {
            $file_name = $path . "/" . $nameFormate . ".txt";
        }
        if ($append) {
            @file_put_contents($file_name, $xmlData, FILE_APPEND);
        } else {
            $i = 1;
            while (file_exists($file_name)) {
                $file_name = $path . "/" . ($nameFormate . '-' . date('Y_m_d_H:i:s-')) . $i . ".txt";
                $i++;
            }
            @file_put_contents($file_name, $xmlData);
        }
    }

    public function getLogPath($parentLogFolder) {
        $logPath = $parentLogFolder == '' ? '/grayPaylog' : '/' . $parentLogFolder;
        $logPath = sfConfig::get('sf_log_dir') . $logPath . '/' . date('Y-m-d');

        if (is_dir($logPath) == '') {
            $dir_path = $logPath . "/";
            mkdir($dir_path, 0777, true);
            chmod($dir_path, 0777);
        }
        return $logPath;
    }

    /**
     *
     * @param <type> $request
     * @return <type>
     * This function check payment on JNA and update on our database after some given interval...
     */
    public function executeJNAPaymentUpdate(sfWebRequest $request) {


        $url = sfConfig::get('app_nmi_jna1_query_url');
        $headers = array();
        $headers = @get_headers($url);
        $message = '';

        if ($headers == '') {
            $message .= 'JNA Server not working!!!';
        } else {

            $secs = sfConfig::get('app_nmi_jna1_payment_interval_in_sec');
            $startDate = date("Y-m-d H:i:s", mktime(date("H"), date("i"), date("s") - $secs, date("m"), date("d"), date("Y")));
            $endDate = date("Y-m-d H:i:s" , mktime(date("H")-1, date("i"), date("s"), date("m"), date("d"), date("Y")));

            $orderDetails = Doctrine::getTable('ipay4meOrder')->getOrderNumbersForPaymentUpdateCron($startDate, $endDate);

            $orderDetailsCount = count($orderDetails);
            $message = 'No Application found for payment.';
            if ($orderDetailsCount > 0) {

                $gatewayId = 10;
                $payManagerObj = new PaymentManager();
                ## Getting midservice...
                $midService = $payManagerObj->getMidService($gatewayId);

                ## Setting gateway id and midservice to the class...
                $epNmiManager = new EpNmiManager();
                $epNmiManager->Gateway_id = $gatewayId;
                $epNmiManager->midService = $midService;


                for ($i = 0; $i < $orderDetailsCount; $i++) {
                    $userId = $orderDetails[$i]['created_by'];
                    $orderStatusArr = $this->getStatus($orderDetails[$i]['order_number'], $epNmiManager);
                    if (empty($orderStatusArr)) {
                        $message .= $orderDetails[$i]['order_number'] . ': Status Not Found<br />';
                        continue;
                    }
                    $orderStatus = $epNmiManager->getPaymentStatus($orderStatusArr);
                    switch ($orderStatus) {
                        case 'complete' :
                            $paymentResponseArray = array();
                            $paymentResponseArray['response_code'] = '100';
                            $paymentResponseArray['response_text'] = 'SUCCESS';
                            $paymentResponseArray['card_type'] = $orderDetails[$i]['card_type'];

                            $paymentResponseArray['status'] = 1;
                            $orderObj = $payManagerObj->updatePaymentRequestNmi($orderDetails[$i]['id'], $paymentResponseArray);
                            $orderObj->setUpdatedBy($userId);
                            $orderObj->save();

                            $updateOrder = $payManagerObj->updateOrderRequest($orderDetails[$i]['id'], $paymentResponseArray);

                            if ($updateOrder->getPaymentStatus() == 0) {
                                $updateOrder = $this->updateSplitAmount($orderDetails[$i]['order_request_detail_id'], $orderDetails[$i]['gateway_id'], $userId);
                                $result = $this->paymentSuccess($orderDetails[$i]['order_number'], $userId);
                                $message .= $orderDetails[$i]['order_number'] . ' : Payment Updated by cron<br />';
                            }
                            break;
                        default :
                            $message .= $orderDetails[$i]['order_number'] . ': Payment not require by cron<br />';
                            break;
                    }//End of switch ($orderStatus){...
                }//End of for($i=0;$i<$orderDetailsCount;$i++){...
            }//End of if($orderDetailsCount > 0){...
        }

        return $this->renderText($message);
    }

    // Get Status of a transaction made at JNA
    private function getStatus($order_id, $epNmiManager) {

        $requestString = "order_id=" . $order_id;
        $epNmiManager->createLogData($requestString, 'StatusRequest-' . $order_id, 'nmiRollbackLog');

        $url = sfConfig::get('app_nmi_jna1_query_url');

        $browser = $this->getBrowser();
        $browser->post($url, $requestString);
        $code = $browser->getResponseCode();

        if ($code != 200) {
            $respMsg = $browser->getResponseMessage();
            $respText = $browser->getResponseText();
            return false;
        }

        $respObj = simplexml_load_string($browser->getResponseText());

        if ($respObj) {
            $res = $epNmiManager->getArrayfrmXML($respObj, array());
            return $res;
        }
        return false;
    }

    public function updateSplitAmount($orderRequestDetail_id, $gateway_id, $user_id) {

        $OrderSplitAllObj = Doctrine::getTable('OrderRequestSplit')->findByOrderDetailId($orderRequestDetail_id);

        if ($gateway_id != sfConfig::get('app_ewallet_gateway_id')) { //hardcoded to be changed to eWallet
            $transctionChargeObj = Doctrine::getTable('TransactionCharges')->findByGatewayId($gateway_id);



            if (is_object($transctionChargeObj)) {
                if ("percentage" == $transctionChargeObj->getfirst()->getSplitType()) {
                    foreach ($OrderSplitAllObj as $orderObj) {
                        $transCharge = round(($orderObj->getAmount() * $transctionChargeObj->getfirst()->getTransactionCharges()) / 100);
                        $itemFee = $orderObj->getAmount() - $transCharge;
                        $orderObj->setItemFee($itemFee);
                        $orderObj->setTransactionCharges($transCharge);
                        $orderObj->save();
                    }
                }
            }
        } else {
            $accountObj = new UserEwalletConsolidation();
            $detailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetail_id);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
            $payable_amount = convertToCents($detailsObj->getAmount());
            $splitArray = $accountObj->getSplitConfiguration($payable_amount, $user_id);
            //                    print "<pre>";
            //                    print_R($splitArray);
            $transactionCharge = 0;
            $charge = 0;
            foreach ($splitArray as $key => $value) {
                $charge = $charge + $value['service_charge'] * $value['amount'];
            }
            $transactionCharge = $charge / $payable_amount;

            foreach ($OrderSplitAllObj as $orderObj) {
                $transCharge = round(($orderObj->getAmount() * $transactionCharge) / 100);
                $itemFee = $orderObj->getAmount() - $transCharge;
                $orderObj->setItemFee($itemFee);
                $orderObj->setTransactionCharges($transCharge);
                $orderObj->save();
            }
            $accountObj->updateEwalletConsolidationAcct($splitArray, $user_id);
            //      exit;
        }
        return $orderObj;
    }

    public function paymentSuccess($order_number, $userId, $sitePath = '') {

        $result = array();

        $orderDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number);

        $updatePayment = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderDetailId, $userId);

        /**
         * [WP: 112] => CR: 158
         * Updating payment status set to 0...
         */
        Functions::updateTransactionServiceChargesPaymentStatus($orderDetailId);       

        $notificationUrl = "notification/PaymentNotificationV1";
        $senMailUrl = "notification/paymentSuccessMail";
        $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication', $notificationUrl, array('order_detail_id' => $orderDetailId));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        $this->paymentAuditPaymentTransaction($orderDetailId, $order_number);
        ##add mail in job
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        if($sitePath != ''){
            $url = $sitePath."/report/getReceipt/receiptId/".$order_number;
            $url1 =$sitePath."/report/paymentHistory";
        }else{
            $url = url_for("report/getReceipt", true) . '?receiptId=' . $order_number;
            $url1 = url_for("report/paymentHistory", true);
        }
        

        $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail', $senMailUrl, array('order_detail_id' => $orderDetailId, 'url' => $url, 'url1' => $url1));
        sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");
        
        $result['status'] = 'success';
        $result['validation'] = $order_number;
        return $result;
    }

    public function paymentAuditPaymentTransaction($orderDetailId, $cusId) {
        //Log audit Details
        $orderDetailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $amount = $orderDetailsObj->getAmount();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $amount_formatted = format_amount($amount, 1);
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $orderDetailId, $orderDetailId));
        $eventHolder = new pay4meAuditEventHolder(
                        EpAuditEvent::$CATEGORY_TRANSACTION,
                        EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
                        EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $cusId, 'amount' => $amount_formatted)),
                        $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    /**
     *
     * @param <type> $request
     * @return <type>
     * This function run by cron before going to thanx page...
     *
     */
    public function executeUpdatePaymentStatus(sfWebRequest $request) {

        $this->setLayout(null);
        $requestId = $request->getParameter('requestId');
        $gateway_id = $request->getParameter('gateway_id');
        $order_id = $request->getParameter('order_id');
        $order_number = $request->getParameter('orderNumber');
        $paymentStatus = unserialize($request->getParameter('paymentStatus'));
        $sitePath = $request->getParameter('sitePath');

        $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findById($order_id);

        if (isset($ipay4meOrder[0]['id'])) {

            $user_id = $ipay4meOrder[0]['created_by'];

            $paymentManager = new PaymentManager();

            $updateOrder = $paymentManager->updateOrderRequest($order_id, $paymentStatus);

            $updateOrder = $this->updateSplitAmount($requestId, $gateway_id, $user_id);

            $result = array();
            $result = $this->paymentSuccess($order_number, $user_id, $sitePath);

            //$result['status'] = 'success';
            return $this->renderText('Payment Process Before Thanks: ' . $result['status']);
        } else {
            return 'Payment Process Before Thanks: User Id not found';
        }
    }

//End of public function executeUpdatePaymentStatus(sfWebRequest $request){   ...

    public function executeSupportResponseMail(sfWebRequest $request) {


        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'supportResponseMailTemplate';
        $mailingOptions = array();
        $mailInfo = 'Mail is sent successfully';
//    $orderNumber = $request->getParameter('orderNumber');
        $requestId = $request->getParameter('request_id');
        $requestDetails = Doctrine::getTable('SupportRequestList')->find($requestId);
        $orderNumber = $requestDetails->getOrderNumber();
        $supportCategory = $requestDetails->getSupportCategory();
        if (!$supportCategory) {
            $supportCategory = 'N/A';
        }
        $supportRequestApplicationDetails = Doctrine::getTable('SupportRequestApplicationDetails')->getRequestId($requestId);
        $request_for = '';
        if ($supportRequestApplicationDetails['0']['request_for']) {
            $request_for = $supportRequestApplicationDetails['0']['request_for'];
        } else {
            $request_for = "N/A";
        }
        $supportList = Doctrine::getTable('SupportRequestComments')->getRequestDetails($requestId);
//    $test = $supportList['0']['support_user_comments'];
        $url = $request->getParameter('url');
        $url = $url . "?request_id=" . $requestId;
        $sendArr = array('url' => $url);
        $rDetails = base64_encode(serialize($sendArr));
//    $concatinatedMsg = $request->getParameter('concatinatedMsg');
        $mailInfo = 'Mail sent successfully';

//    $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
//    $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);

        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
//      $amount = $requestDetails->getAmount();
//      $merchantName = $requestDetails->getMerchant()->getName();
//      $amount = $requestDetails->getAmount();
//      $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('signature' => $signature, 'name' => $name, 'request_for' => $request_for, 'rDetails' => $rDetails, 'supportList' => $supportList, 'supportCategory' => $supportCategory, 'orderNumber'=>$orderNumber);
            $mailingOptions['mailSubject'] = 'Support Response Notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executeSupportCloseMail(sfWebRequest $request) {

        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'supportCloseMailTemplate';
        $mailingOptions = array();
//    $orderNumber = $request->getParameter('orderNumber');
        $requestId = $request->getParameter('request_id');
        $requestDetails = Doctrine::getTable('SupportRequestList')->find($requestId);
        $orderNumber = $requestDetails->getOrderNumber();
        $supportRequestApplicationDetails = Doctrine::getTable('SupportRequestApplicationDetails')->getRequestId($requestId);
        $request_for = '';
        if ($supportRequestApplicationDetails['0']['request_for']) {
            $request_for = $supportRequestApplicationDetails['0']['request_for'];
        } else {
            $request_for = "N/A";
        }

        $supportList = Doctrine::getTable('SupportRequestComments')->getRequestDetails($requestId);
        $test = $supportList['0']['support_user_comments'];

//    $concatinatedMsg = $request->getParameter('concatinatedMsg');
        $mailInfo = 'Mail sent successfully';

//    $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetailId($orderNumber);
//    $requestDetails = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);

        $user_id = $requestDetails->getUserId();
        if ($user_id) {
            $userDetailObj = $requestDetails->getSfGuardUser()->getUserDetail();
            $user_email_address = $userDetailObj->getEmail();
            $name = $userDetailObj->getFirstName() . " " . $userDetailObj->getLastName();
//      $amount = $requestDetails->getAmount();
//      $merchantName = $requestDetails->getMerchant()->getName();
//      $amount = $requestDetails->getAmount();
//      $merchantName = $requestDetails->getMerchant()->getName();
            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('signature' => $signature, 'name' => $name, 'request_for' => $request_for, 'supportList' => $supportList,'orderNumber'=>$orderNumber);
            $mailingOptions['mailSubject'] = 'Support Close Notification';
            $mailingOptions['mailTo'] = $user_email_address;
            $mailingOptions['mailFrom'] = $mailFrom;
            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
        }
        return $this->renderText($mailInfo);
    }

    public function executeCustCommentMailToAdmin(sfWebRequest $request) {
        $this->setLayout(null);
        $mailingClass = new Mailing();
        $partialName = 'custCommentMailToAdmin';
        $mailingOptions = array();

        $userId = $this->getUser()->getGuardUser()->getId();
        $custComment = $request->getParameter('cust_comment');
        $requestId = $request->getParameter('request_id');
        $requestData = Doctrine::getTable('SupportRequestList')->find($requestId)->toArray();
        $requstCategory = $requestData['support_category'];
        $requstSubCategory = $requestData['sub_support_category'];

        $commentArray = Doctrine::getTable('SupportRequestComments')->getUpdatedComments($requestId);
        $SupportUserId = Doctrine::getTable('SupportRequestAssignment')->getAssignedSupUser($requestId);
        $SupportUserEmailArr = Doctrine::getTable('UserDetail')->findByUserId($SupportUserId)->toArray();

        if ($SupportUserEmailArr != 0 && !empty($SupportUserEmailArr)) {
            $name = $SupportUserEmailArr[0]['first_name'] . ' ' . $SupportUserEmailArr[0]['last_name'];
            $SupportUserEmail = $SupportUserEmailArr[0]['email'];

            if (sfConfig::get('app_host') == "local") {
                $mailFrom = sfConfig::get('app_email_local_settings_mail_from');
                $signature = sfConfig::get('app_email_local_settings_signature');
            } else if (sfConfig::get('app_host') == "production") {
                $mailFrom = sfConfig::get('app_email_production_settings_mail_from');
                $signature = sfConfig::get('app_email_productionl_settings_signature');
            }
            $partialVars = array('cust_comment' => $custComment, 'signature' => $signature, 'name' => $name, 'requstCategory' => $requstCategory, 'requstSubCategory' => $requstSubCategory, 'commentArray' => $commentArray);
            $mailingOptions['mailSubject'] = 'Support Notification';
            $mailingOptions['mailTo'] = $SupportUserEmail;
            $mailingOptions['mailFrom'] = $mailFrom;

            $mailingClass->sendMail($partialName, $partialVars, $mailingOptions);
            $this->forward('requestRefund', 'sendCustomerResponse');
        } else {
            $this->forward404('Page Not Found');
        }
    }
    /**
     * 
     * @param <type> $request
     * @return <type>
     * This function will be executed only once for setting cron. Once cron set up, this function will have to delete...
     */
    public function executeHourlyJNACron(sfWebRequest $request){        
        $url = 'notification/jNAPaymentUpdate';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")+26));
        $jobId = EpjobsContext::getInstance()->addJobForEveryHour('JNAPaymentUpdate', $url, $end_time, array(), '', '', $start_time, 59 );
        return $this->renderText($jobId);
    }


    /**
     * [WP: 080] => CR: 116
     * @param <type> $request
     * @return <type>
     * This function will be executed only once for setting cron. Once cron set up, this function will have to delete...
     * This cron job will call "executeExpireVOAPApp" function...
     */
    public function executeDailyCronForExpiredVOAPApp(sfWebRequest $request){
        $url = 'notification/expireVOAPApp';
        $start_time = date('Y-m-d H:i:s');
        $end_time = date('Y-m-d H:i:s', mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")+25));
        $jobId = EpjobsContext::getInstance()->addJobForEveryDay('VOAPApplicationExpired', $url, $end_time, array(), '', '', $start_time, 10, 1 );
        return $this->renderText($jobId);
    }
    

    /**
     * [WP: 080] => CR: 116
     * @param <type> $request
     * This function is being used by cronjob...
     * This function will fetch last 5 days records to mark status expired of if application status is paid...
     * 5 days is defined in app.yml... 
     */
    public function executeExpireVOAPApp(sfWebRequest $request){
        
        ## Getting records which need to be expired by system...
        $voapObj = Doctrine::getTable('VapApplication')->getVisaArrivalApplication();
        $totalRecords = count($voapObj);
        $execution = false;
        if($totalRecords > 0){
            $appArray = array();
            foreach($voapObj As $voap){
                if($voap->getStatus() == 'Paid'){
                    $appArray[] = $voap->getId();
                    $voap->setStatus('Expired');
                    $voap->save();
                    $execution = true;
                }
            }
        }
        if($execution){
            $msg = $totalRecords.' application(s) found and expired successfully.<br />'.serialize($appArray);
        }else{
            $msg = 'Application(s) not found to get expired.';
        }
        return $this->renderText($msg);
    }

    public function getOrderDetails($orderNumber){

        $orderNumberArray = array();
        $ipay4meOrderDetails = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderNumber);
        if(count($ipay4meOrderDetails) > 0){
            foreach($ipay4meOrderDetails AS $ipay4meOrder){
                if($ipay4meOrder->getPaymentStatus() == 1){
                    $orderNumberArray['order_id'] = $ipay4meOrder->getId();
                    $orderNumberArray['order_number'] = $ipay4meOrder->getOrderNumber();
                    $orderNumberArray['payment_status'] = $ipay4meOrder->getPaymentStatus();
                    $orderNumberArray['gateway_id'] = $ipay4meOrder->getGatewayId();
                    $orderNumberArray['order_request_id'] = $ipay4meOrder->getOrderRequestDetailId();
                    $orderNumberArray['card_type'] = $ipay4meOrder->getCardType();
                    $orderNumberArray['created_by'] = $ipay4meOrder->getCreatedBy();
                }
            }            
        }

        return $orderNumberArray;
    }


    public function executeUpdateJnaTransactionByOrderNo(sfWebRequest $request){

        ##$orderNumberArray = array('134144247315788','134144180652056','134144163092547');

        ## production array
//        $orderNumberArray = array( '134144247315788', '134144180652056', '134144163092547', '134144158899841', '134144153343636', '134144135577905', '134144002348372', '134143952145189', '134143907096205', '134143846250677', '134143800884300', '134143772659420', '134143756382780', '134143755534848', '134143657322410', '134143640376079', '134143583986397', '134143552524237', '134143549522117', '134143545337465', '134143515797744', '134143450936299', '134143447684398', '134143448226932', '134143415997926', '134143415145495', '134143419179293', '134143389377732', '134143346655674', '134143235693161', '134143231014627', '134143183733049', '134143160599711', '134143157939708', '134143151780890', '134143088921294', '134143063186001', '134143017467403', '134142987850499', '134142995525755', '134142987221368', '134142931367512', '134142909030514', '13414287 4015742', '134142856290992', '134142765989448', '134142754410268', '134142643240342', '134142621686090', '134142546568437', '134142496854265', '134142419671537', '134142310297900', '134142306513397', '134142283976756', '134142283245086', '134142275010223', '134142195774964', '134142191647824', '134142187523347', '134142161114553', '134142177398049', '134142169786765', '134142165753874', '134142121566779', '134142120193970', '134142057592987', '134142051177796', '134142048913331', '134142030453516', '134142030940126', '134142008438100', '134141949232807', '134141942947138', '134141942661952', '134141937982737', '134141916165777', '134141914617835', '134141883919993', '134141897082118', '134141 896169247', '134141827133765', '134141807247502', '134141563656798', '134141789268135', '134141778065592', '134141755550160', '134141738095245', '134141734945457', '134141693464985', '134141670732439', '134141665039668', '134141607610512', '134141614982526', '134141610760023', '134141579687202', '134141526148841', '134141517991297', '134141496641160', '134141467750384', '134141432975421', '134141430279756', '134141425581708', '134141387611290', '134141380791315', '134141376592781', '134141289360818', '1341 41270092468', '134141170671053', '134141247643596', '134141242380053', '134141235457211', '134141228897350', '134141154532561', '134141147267775', '134141115072909', '134141121646333', '134141127114213', '134141083647806', '134141099076181', '134141073497450', '134141075050241', '134141066517851', '134141035639680', '134141018718014', '134141016589486', '134140977135257', '134140974743649', '134140936883076', '134140927160070', '134140918679916', '134140879951522', '134140911094463', '134140878788470', '134140865758977', '134140832820220', '134140847788920', '134140842840750', '13 4140825029620', '134140802968710', '134140796191389', '134140785827955', '134140714381208', '134140721514318', '134140708885611', '134140695922478', '134140669491402', '134140654197670', '134140648387657', '134140645415039', '134140625051737', '134140622143514', '134140599030172', '134140579754826', '134140546636954', '134140565126675', '134140533433735', '134140499142535', '134140497084000', '134140476346109', '134140491613980', '134140493625550', '134140428921216', '134140481267238', '134140475346866', '134140474765783', '134140469590884', '134140452493295', '134140444034754', '134140422238079', '134140343794031', '134140321530947', '134140301596277', '134140282099645', '134140256469757', '134140236863937', '134140232423243', '134140226282735', '134140206762947', '134140187070704', '134140158792049', '134140145532020', '134140135313767', '134140099225060', '134140089527972', '134140078038918', '134140070083450', '134140053142095', '134140048111666', '134140038223240', '134140018890432', '134140020498463', '134139986768748', '13413996783368 4', '134139917619855', '134139964075180', '134139951872428', '134139944443496', '134139899683184', '134139885718335', '134139858689495', '134139864646839', '134139829951788', '134139793522704', '134139794022881', '134139778122279', '134139768961298', '134139755028664', '134139740925173', '134139715360663', '134139712161229', '134139699055230', '134139661665739', '134139652996452', '134139637245798', '134139627934036', '134139545849959', '134139558166991', '134139530935067', '134139516736141', '134139511520473', '134139510853944', '134139446821636', '134139459412427', '134139455844 508', '134139456754968', '134139443815594', '134139347867143', '134139327376109', '134139325265701', '134139304630930', '134139242486658', '134139220828919', '134139201154441', '134139191512346', '134139162757562', '134139137349595', '134139113944816', '134139049777597', '134138997498915', '134138990243414', '134138980030023', '134138945093449', '134138935549710', '134138877111635', '134138882282104', '134138827914999', '134138797616275', '134138785490510', '134138744450674');
//        $orderNumberArray = array('134138587226908', '134138587692787', '134138420854501', '134138376531236', '134138348387119', '134138308162035', '134138109595273', '134138087316365', '134138098060742', '134138076734583', '134138077173128', '134138016127969', '134138014195870', '134137978424721', '134137891757925', '134137860449252', '134137659820613', '134137658154911', '134137617559660', '134137334443741', '134136696857359', '134136683184898', '134136661835564', '134136646577158', '134152878652310', '134152768275746', '134152756461997', '134152699933371', '134152636947444', '134152528462902', '134152510586893', '134152468471775', '134152347116423', '134152267144858', '134152198965193', '134152166439111', '134152147819449', '134152143330547', '134152114162631', '134152026073407', '134152030538695', '134151949311534', '134151943729534', '134151910652355', '134151896588370', '134151902844735', '134151889159014', '134151874899636', '134151805383483', '134151735096403', '134151731976847', '134151683531160', '134151672142660', '134151666530719', '134151651017729', '13415150714 0926', '134151500472790', '134151368818905', '134151355116774', '134151332570350', '134151321315285', '134151312776874', '134151288649366');
//        $orderNumberArray = array('134151245416441', '134151234698486', '134151212757993', '134151176279815', '134151158387853', '134151156425182', '134151137333462', '134151101979187', '134151036988388', '134151005321839', '134150994862795', '134150936874609', '134150914766363', '134150804657150', '134150802982104', '134150801283294', '134150755499758', '134150742486634', '134150733738990', '134150717246316', '134150715323133', '134150686668994', '134150599030300', '134150592495721', '134150595229390', '134150559158981', '134150545449119', '134150529742217', '134150532635295', '134150453531307', '134150443738036', '134150377654529', '134150368148285', '134150367853011', '134150303035456', '134150292191370', '134150285055861', '134150280919466', '134150237524643', '134150208173629', '134150105118778', '134150085583063', '134150064197575', '134150059135884', '134150058679994', '134149939695581', '134149929222092', '134149885740939', '134149865283954', '134149808047293', '134149787470413', '134149764116885', '134149740057269', '134149731186598', '134149730547987', '134149704920194', '134149670417214', '134149652186120', '134149643527785', '134149590787585', '134149589463962', '134149589838453', '134149560663796');
//        $orderNumberArray = array('134149569031258', '134149571822773', '134149509332145', '134149479078798', '134149474312686', '134149463732038', '134149428281452', '134149344581271', '134149394253005', '134149279464046', '134149358497374', '134149346728144', '134149298499581', '134149265033169', '134149266437757', '134149255875557', '134149248230457', '134149234959855', '134149202564817', '134149171351642', '134149153131890', '134149127338780', '134149103618185', '134149098099053', '134149086819915', '134149072433861', '134149058690639', '134149017322154', '134148989421633', '134148983983811', '134148957772053', '134148953596892', '134148919510225', '134148897666131', '134148881445738', '13414886619 9713', '134148783046829', '134148734896727', '134148740832558', '134148718970802', '134148683145494', '134148646749777', '134148623759276', '134148617976000', '134148603217659', '134148599116882', '134148582629599', '134148578995321', '134148495271356', '134148489826331', '134148483264880', '134148465220313', '134148439831120', '134148439752514', '134148383451157', '134148431612082', '134148415832803', '134148407282087', '134148389771691', '134148376574879', '134148365411758', '134148357053453', '134148291670896');
//        $orderNumberArray = array('134148283355252', '134148088754457', '134148056892654', '134148054161261', '134148045274505', '134148045837887', '134148021491043', '134147979836630', '134148002814333', '134147978350831', '134147938442440', '134147935071576', '134147929032536', '134147840659866', '134147818940781', '134147775028864', '134147739435599', '134147673321885', '134147667696382', '134147657725405', '134147570881989', '134147630945333', '134147577314031', '134147527166579', '134147379339177', '134147356051318', '134147334545438', '134147313015446', '134147309921462', '134147293541147', '134147286830376', '134147255893889', '134147221314591', '134147202140076', '134146994672751', '134146972590494', '134146948340819', '134146821164389', '134146446530283', '134146155353306', '13414603353 5411', '134145923751442', '134145820497893', '134145798643907', '134145262552223', '134144695392557', '134144602816940', '134159439147746', '134159412155616', '134159399497025', '134159365880514', '134159359398135', '134159339555105', '134159252620077', '134159244846766', '134159024675767', '134159013930785', '134158996673378', '134158977180717', '134158878822288', '134158841970888', '134158813459353', '134158722424744');
//        $orderNumberArray = array('134146033535411', '134158653275138', '134158641127828', '134158625293168', '134158607288555', '134158572074728', '134158471642787', '134158452950287', '134158454388436', '134158442011143', '134158437434845', '134158370718561', '134158326769479', '134158290575175', '134158247469321', '134158232331313', '134158155635555', '134158141867437', '134158147579680', '134158050960336', '134158029855883', '134158012294658', '134157953190640', '134157926069750', '134157896426739', '134157807287419', '134157800892335', '134157781135638', '134157774271782', '134157765924690', '134157661645087', '134157712536595');
//        $orderNumberArray = array('134157690997467', '134157677315905', '134157680781149', '134157572491452', '134157552478344', '134157446515017', '134157518667319', '134157501869565', '134157454090496', '134157437478205', '134157397710189', '134157405070039', '134157252727707', '134157209784539', '134157202197714', '134157182631839', '134157146132609', '13415715239 9490', '134157100541361', '134157065439665', '134157077943531', '134157052044802', '134157055276210', '134157014590149', '134156859859325', '134156805657532', '134156765112139', '134156746630155', '134156653831717', '134156642038681', '134156559968559', '134156567911093', '134156533395837', '134156509727492', '134156496574714', '134156446590147', '134156435215325', '134156359687781', '134156229166778', '134156210041109', '134156165646337', '134156103754436', '134155878416493', '134155800989793', '134155672633840', '134155571635650', '134155379586267', '134155361364648', '134155276719115', '134155128089971', '134154836664861', '134154692191862', '134154530489419', '134154443317663', '134154264521987', '134153987735778', '134153948326400', '134153619010711', '134153494660654', '134153441846108', '134153300413390', '134153102798694', '134153027014299');
//        $orderNumberArray = array('134157152399490');


        $outputArray = array();
        if (count($orderNumberArray) > 0) {
            
            foreach ($orderNumberArray AS $orderNumber) {

                $order_number = $this->getOrderDetails($orderNumber);

                if(count($order_number) < 1) continue;

                $outputArray[] = $orderNumber;

                $payManagerObj = new PaymentManager();
                $paymentResponseArray = array();
                $paymentResponseArray['response_code'] = '100';
                $paymentResponseArray['response_text'] = 'SUCCESS';
                $paymentResponseArray['card_type'] = $order_number['card_type'];

                $paymentResponseArray['status'] = 1;
                $orderObj = $payManagerObj->updatePaymentRequestNmi($order_number['order_id'], $paymentResponseArray);
                $updateOrder = $payManagerObj->updateOrderRequest($order_number['order_id'], $paymentResponseArray);

                if ($updateOrder->getPaymentStatus() == 0) {
                    $user_id = $order_number['created_by'];
                    $updateOrder = $this->updateSplitAmount($order_number['order_request_id'], $order_number['gateway_id'], $user_id);
                    $result = $this->paymentSuccess($order_number['order_number'], $user_id);
                }
                
            }//End of foreach($orderDetailArray...

            
        }//End of if(count($orderDetailArray) > 0){...

        echo "<pre>";
        print_r($outputArray);
        
        die("Success");
    }

 ##$orderArr = array('134159003598931','134159012850809','134181631230314', '134182349456660');

 public function executeFindNonDuplicateOrderNumbers(sfWebRequest $request){
     
     //$orderArr = array('134159003598931','134159012850809','134181631230314', '134182349456660','134182661266113','134182698461261','134182703613312');

     ## Final array
     ##$orderArr = array('134144247315788','134144180652056','134144163092547','134144158899841','134144153343636','134144135577905','134144073396884','134144032046828','134144002348372','134143952145189','134143916322531','134143912011399','134143907096205','134143846250677','134143846465705','134143800884300','134143772659420','134143756382780','134143755534848','134143657322410','134143640376079','134143584772936','134143583986397','134143542923169','134143552524237','134143549522117','134143545337465','134143532476900','134143515797744','134143450936299','134143447684398','134143448226932','134143415997926','134143415145495','134143419179293','134143404466584','134143389377732','134143346655674','134143303115988','134143235693161','134143231014627','134143211116904','134143191363681','134143183733049','134143160599711','134143161848012','134143157939708','134143151780890','134143113671002','134143088921294','134143063186001','134143057675978','134143017467403','134142987850499','134142995525755','134142987221368','134142960186112','134142931367512','134142926570822','134142909030514','134142896554767','13414287 4015742','134142856290992','134142765989448','134142754410268','134142703615297','134142708151455','134142674253288','134142643240342','134142621686090','134142611199335','134142546568437','134142496854265','134142475245336','134142423999143','134142419671537','134142350335483','134142310297900','134142306513397','134142296489337','134142283976756','134142283245086','134142275010223','134142271476912','134142195774964','134142191647824','134142187523347','134142161114553','134142177398049','134142169786765','134142165753874','134142143012037','134142135646348','134142121566779','134142121064320','134142120193970','134142120297578','134142114837363','134142097213494','134142064213754','134142057592987','134142056766978','134142051177796','134142048913331','134142030453516','134142030940126','134142008438100','134142011814466','134141998768748','134141986443251','134141967280046','134141967292705','134141949232807','134141942947138','134141942661952','134141937982737','134141922956590','134141916165777','134141914617835','134141912835433','134141883919993','134141908049431','134141897082118','134141 896169247','134141880498910','134141877871019','134141868544229','134141852575767','134141849657638','134141826920981','134141827133765','134141784478099','134141807247502','134141563656798','134141789268135','134141787971932','134141778065592','134141579252554','134141727932961','134141755550160','134141749196039','134141746764716','134141738095245','134141734945457','134141725582937','134141708725788','134141710962221','134141703939488','134141693464985','134141663432645','134141670732439','134141665039668','134141652195391','134141644370890','134141607610512','134141614982526','134141610760023','134141602094342','134141579687202','134141555497913','134141552626361','134141544035801','134141529241499','134141526148841','134141517991297','134141496641160','134141498442197','134141467750384','134141457530034','134141454372274','134141432975421','134141430279756','134141410331226','134141425581708','134141406998510','134141401021303','134141387611290','134141380791315','134141376592781','134141348018682','134141331236653','134141329210780','134141309364407','134141302125488','134141289360818','1341 41270092468','134141278750706','134141270771992','134141262470474','134141170671053','134141247643596','134141242380053','134141235457211','134141228897350','134141209053166','134141170114456','134141160749214','134141154532561','134141147267775','134141146481495','134141124345233','134141136813170','134141115072909','134141121646333','134141127114213','134141121081354','134141113053352','134141104430005','134141083647806','134141099076181','134141085816048','134141073497450','134141081472845','134141075050241','134141066517851','134141065242690','134141031043761','134141051586460','134141042869237','134141035639680','134141025372938','134141023856351','134141018718014','134141016589486','134140998013224','134140977135257','134140974743649','134140951343913','134140945457282','134140936883076','134140934915782','134140927160070','134140918679916','134140879951522','134140909014180','134140911094463','134140914539542','134140897682200','134140878788470','134140879111187','134140872352087','134140865758977','134140832820220','134140847788920','134140843655769','134140842840750','134140834594819','13 4140825029620','134140806156380','134140802391301','134140802968710','134140796191389','134140785827955','134140784782447','134140773421312','134140773116726','134140765146492','134140767865161','134140751212646','134140732645984','134140667464432','134140714381208','134140717722767','134140721514318','134140708885611','134140699630905','134140695922478','134140691860700','134140685721599','134140673988246','134140669491402','134140605733686','134140654197670','134140649157693','134140648387657','134140645415039','134140624773276','134140625051737','134140622143514','134140599030172','134140577255987','134140579754826','134140566414325','134140566381939','134140546636954','134140565126675','134140532414893','134140533433735','134140499142535','134140523582502','134140482638306','134140497084000','134140476346109','134140500226624','134140491613980','134140493625550','134140428921216','134140477871393','134140481267238','134140479310779','134140475346866','134140474988721','134140474765783','134140469590884','134140465258132','134140459963262','134140460352704','134140452493295','134140444034754',' 134140439960932','134140439083144','134140422238079','134140416493436','134140407744419','134140403797614','134140398531997','134140398177141','134140384827441','134140376614960','134140364743104','134140353285958','134140350592544','134140343794031','134140301362544','134140321530947','134140319026494','134140304095588','134140301596277','134140287259404','134140282099645','134140256469757','134140253168308','134140237849678','134140236863937','134140232423243','134140226282735','134140206762947','134140205653399','134140187070704','134140187238238','134140186047322','134140179248724','134140166141147','134140160653016','134140158792049','134140153467660','134140154884810','134140145532020','134140135313767','134140138017068','134140089643188','134140099225060','134140096560977','134140093783861','134140089527972','134140078038918','134140070083450','134140068995951','134140053142095','134140049276110','134140048111666','134140019421118','134140038223240','134140008417331','134140020351534','134140018890432','134140020498463','134139990982412','134139986768748','134139977535299','13413996783368 4','134139917619855','134139964075180','134139962841197','134139951872428','134139952261754','134139951480572','134139944443496','134139927683520','134139920448051','134139919749634','134139899683184','134139885718335','134139881161973','134139858689495','134139868218508','134139864646839','134139865991183','134139844819881','134139824636439','134139833327283','134139829951788','134139819661661','134139767888868','134139799669255','134139793522704','134139794022881','134139783789342','134139778122279','134139769713725','134139768961298','134139755028664','134139749527240','134139740925173','134139715360663','134139712161229','134139699055230','134139695119662','134139690598988','134139653651585','134139674825527','134139661665739','134139652996452','134139637245798','134139627934036','134139623065777','134139607574753','134139604060569','134139545849959','134139578567687','134139567546856','134139558166991','134139530935067','134139528283193','134139516736141','134139511520473','134139510853944','134139496753227','134139490523934','134139465269405','134139446821636','134139459412427','134139455844 508','134139456754968','134139447696980','134139443815594','134139438549478','134139436957850','134139348893953','134139347867143','134139342379216','134139327376109','134139325265701','134139304630930','134139301540234','134139271518707','134139242486658','134139220828919','134139218317250','134139201154441','134139194540328','134139191512346','134139162757562','134139149279738','134139137349595','134139113944816','134139089786060','134139091373534','134139057931038','134139063442637','134139051788349','134139049777597','134139037021951','134139035272817','134139017881812','134139014176373','134138997498915','134138990154579','134138990243414','134138986572183','134138980030023','134138966756537','134138946544460','134138945093449','134138935549710','134138926094467','134138914316273','134138877111635','134138882282104','134138853213812','134138853689910','134138846699164','134138827914999','134138826151537','134138816497766','134138797616275','134138785490510','134138751725740','134138744450674','134138735562509','134138703938583','134138649148176','134138611370079','134138609162348','1341385872 26908','134138587692787','134138577464985','134138538238011','134138525113367','134138503551909','134138457187796','134138420854501','134138376531236','134138369534192','134138348387119','134138306598399','134138308162035','134138284297634','134138257782309','134138257485521','134138240518822','134138145250924','134138141394927','134138137512256','134138116239833','134138109595273','134138087316365','134138098060742','134138072475221','134138093375530','134138089875920','134138087224238','134138076734583','134138077173128','134138055124214','134138016127969','134138014195870','134137978424721','134137969070893','134137956031485','134137918333439','134137910223252','134137888568663','134137891757925','134137869191159','134137860449252','134137843430672','134137823652864','134137798568268','134137728541605','134137713476876','134137706740544','134137659820613','134137658154911','134137634519497','134137617559660','134137589219379','134137571387420','134137520652315','134137509447063','134137334443741','134136740634591','134136696857359','134136683184898','134136665993837','134136661835564','13413664 6577158', '134152878652310','134152854530017','134152768275746','134152758986803','134152756461997','134152738336996','134152713757649','134152699933371','134152680838174','134152690684392','134152636947444','134152600416090','134152571515548','134152528462902','134152510586893','134152483774986','134152468471775','134152347116423','134152267144858','134152198965193','134152175557620','134152166439111','134152147819449','134152143330547','134152114162631','134152111734260','134152026073407','134152030538695','134151968981387','134151949311534','134151943729534','134151923067818','134151910652355','134151896588370','134151902844735','134151889159014','134151879680017','134151874899636','134151826980255','134151830135004','134151808488621','134151805383483','134151735096403','134151731976847','134151679549007','134151683531160','134151672142660','134151666530719','134151651017729','134151630731329','134151602036037','134151578813680','134151556522207','134151537817289','13415150714 0926','134151500472790','134151480599610','134151426763644','134151367476281','134151368818905','134151355116774','134151334728810','134151332570350','134151321315285','134151312776874','134151288649366','134151267422071','134151245416441','134151234698486','134151223157572','134151212757993','134151213898760','134151205730803','134151206016902','134151176279815','134151158387853','134151156425182','134151146171604','134151138252378','134151137333462','134151101979187','134151072052212','134151036988388','134151037612593','134151008259624','134151004881248','134151005321839','134150994862795','134150960119161','134150936874609','134150914766363','134150817035474','134150804657150','134150802982104','134150801283294','134150767987350','134150755499758','134150749671683','134150742486634','134150733738990','134150717246316','134150715323133','134150715643662','134150707321524','134150686668994','134150672426970','134150599030300','134150592495721','134150595229390','13415055915 8981','134150539833172','134150545449119','134150529742217','134150533154732','134150532635295','134150517454377','134150513171568','134150495943059','134150482587755','134150469387513','134150465649303','134150455515756','134150453531307','134150450491193','134150443738036','134150435169109','134150362688935','134150377654529','134150368148285','134150367853011','134150346199692','134150293696854','134150303035456','134150292191370','134150285055861','134150280919466','134150283340080','134150265877043','134150233311136','134150237524643','134150207896079','134150216491365','134150170572254','134150208173629','134150148755662','134150134392026','134150120331197','134150105118778','134150085583063','134150064197575','134150059135884','134150058679994','134150053734963','134150027771960','134150000191238','134149973142586','134149939695581','134149938449298','134149929222092','134149906365241','134149914384614','134149913947675','134149905639295','134149892753616','13414988574 0939','134149874997577','134149874748008','134149873249701','134149871220921','134149865283954','134149859772423','134149808047293','134149795168359','134149787470413','134149787884441','134149764116885','134149740057269','134149731186598','134149730547987','134149713810487','134149704920194','134149705633447','134149670945443','134149688389966','134149639582258','134149671839458','134149670417214','134149668410855','134149668462125','134149664393856','134149652186120','134149643527785','134149641047024','134149638275320','134149614750518','134149590787585','134149589463962','134149596046661','134149592776892','134149589838453','134149560663796','134149569031258','134149567595127','134149571822773','134149548656061','134149536970415','134149510574953','134149509332145','134149490393896','134149475495236','134149479078798','134149474312686','134149463732038','134149456635984','134149431586331','134149434881586','134149428281452','134149420587968','134149344581271','13414939425 3005','134149393389232','134149279464046','134149364595091','134149358497374','134149175350532','134149346728144','134149343915196','134149337961802','134149310258906','134149309642504','134149298499581','134149294867369','134149292624032','134149289474250','134149285231560','134149265033169','134149266437757','134149255875557','134149248230457','134149237582060','134149234959855','134149232960463','134149202564817','134149191095881','134149171737560','134149171351642','134149149516218','134149153131890','134149127338780','134149101659048','134149103618185','134149098099053','134149086819915','134149088183366','134149084147113','134149072433861','134149058690639','134149040670863','134149017322154','134148989421633','134148987627487','134148983983811','134148984517055','134148957772053','134148968288620','134148953596892','134148941112217','134148915211982','134148919510225','134148910927043','134148897666131','134148881445738','134148886732993','134148873575178','13414886619 9713','134148850046030','134148830610726','134148832072442','134148784264793','134148785043342','134148783046829','134148772775031','134148743340152','134148757159013','134148734896727','134148740832558','134148718970802','134148694933259','134148685341695','134148683145494','134148665253780','134148668877304','134148661852260','134148661731231','134148646749777','134148644367030','134148638479953','134148623759276','134148621122872','134148617976000','134148616411779','134148603217659','134148600328694','134148599116882','134148585156613','134148588412077','134148582629599','134148586676716','134148583498525','134148578995321','134148569520841','134148549798027','134148558873496','134148559962957','134148558267521','134148557568849','134148538499116','134148533753720','134148528577298','134148525134921','134148510960746','134148512139213','134148495271356','134148497039796','134148489826331','134148483264880','134148469229150','134148465220313','134148439831120','13414843975 2514','134148430970117','134148383451157','134148431612082','134148423194975','134148415832803','134148377024262','134148405268696','134148407282087','134148393775087','134148389771691','134148388688333','134148383090181','134148370040719','134148376574879','134148365411758','134148365792787','134148357053453','134148349265667','134148344833324','134148342336602','134148338067656','134148325727813','134148313982946','134148313469708','134148309358723','134148283897010','134148293347791','134148291670896','134148290865704','134148289024323','134148283355252','134148260126402','134148274448062','134148264270683','134148267779576','134148259294236','134148259042963','134148251495941','134148228116781','134148215586160','134148216998649','134148210634293','134148194972170','134148197581219','134148194421948','134148173631745','134148153325386','134148132442560','134148144318761','134148119426655','134148117444999','134148118846964','134148111826830','134148111119032','13414808875 4457','134148091512031','134148080869524','134148056892654','134148054161261','134148051170675','134148045274505','134148045837887','134148037418258','134148039440516','134148030759108','134148035114088','134148028876644','134148024421674','134148021491043','134148022616464','134148017921111','134148019560116','134147979836630','134148002814333','134147999516110','134147991786400','134147978650294','134147978350831','134147979091901','134147976010459','134147959770816','134147944375169','134147938442440','134147935227682','134147935071576','134147929032536','134147924120435','134147913023283','134147901075519','134147903346214','134147896319171','134147895111357','134147882485113','134147882827836','134147874342461','134147849978485','134147840659866','134147839673580','134147832880725','134147818940781','134147804864808','134147803940869','134147796569901','134147792887712','134147782132650','134147775028864','134147775264743','134147765093184','134147754365722','13414773943 5599','134147738484403','134147730069906','134147715433628','134147722829275','134147712861877','134147673321885','134147691210442','134147681145182','134147667696382','134147661783411','134147654092522','134147657725405','134147655291721','134147654029130','134147652987031','134147570881989','134147630945333','134147627581360','134147606580635','134147577314031','134147570876990','134147527166579','134147534857271','134147518269378','134147515237244','134147514677602','134147496412237','134147496237270','134147487089718','134147473114991','134147458013213','134147424599353','134147414315586','134147414214767','134147382613112','134147382511659','134147379339177','134147352947414','134147356051318','134147325445689','134147334545438','134147314960046','134147313015446','134147309595368','134147309921462','134147301048358','134147298662727','134147293541147','134147286830376','134147286928668','134147276351328','134147268455819','134147262816352','134147261669274','13414725589 3889','134147243839635','134147221314591','134147202140076','134147203871625','134147181689329','134147175076626','134147174633983','134147175779146','134147174051811','134147166623977','134147161151540','134147151986191','134147155421305','134147153743345','134147129765910','134147119840902','134147107436817','134147095734810','134147096036784','134147084933578','134147061038587','134147046146776','134147033150139','134147026930444','134147022773586','134147012634679','134147008863274','134146994672751','134146957093094','134146972590494','134146948340819','134146928954816','134146911569200','134146905940331','134146897813081','134146855697285','134146821164389','134146763139335','134146714638212','134146656024027','134146647032413','134146641253663','134146611984344','134146548633689','134146544549593','134146542780197','134146524852634','134146513296546','134146446530283','134146376646018','134146300787532','134146263967141','134146230242153','134146155353306','13414603353 5411','134145968084114','134145946239319','134145923751442','134145820497893','134145802641000','134145798643907','134145742341177','134145602782926','134145262552223','134145131828901','134145108016514','134144796769300','134144793543106','134144694738839','134144695392557','134144690262710','134144665842491','134144602681866','134144602816940','134144543099718','134144532793483','134144521436703','134144489310528','134144469182695','134144473256034','134144477348446','134144410538427','134144431826238','134144331986619','134144291980344','134159439147746','134159412155616','134159386189384','134159399497025','134159378433795','134159373980306','134159365880514','134159359398135','134159339555105','134159274988106','134159252620077','134159244846766','134159215253439','134159070873344','134159024675767','134159013930785','134158996673378','134158914393553','134158977180717','134158944532302','134158878822288','134158841970888','134158827588737','134158813459353','134158787419282','134158722424744','134158653275138','134158641127828','134158625293168','134158607288555','134158572074728','134158553745922','134158527283838','134158510422946','134158471642787','134158452950287','134158460268505','134158454388436','134158442011143','134158437434845','134158370718561','134158353981058','134158326769479','134158290575175','134158281713154','134158247469321','134158247736727','134158232331313','134158155635555','134158141867437','134158147579680','134158151978949','134158134363368','134158087919176','13415805096 0336','134158029855883','134157936170815','134158016652265','134158012294658','134157953190640','134157936763056','134157926069750','134157896426739','134157894312500','134157807287419','134157800892335','134157781135638','134157780517279','134157774271782','134157765924690','134157765315112','134157661645087','134157712536595','134157690997467','134157678555613','134157677315905','134157680781149','134157672743976','134157572491452','134157556042833','134157553296225','134157552478344','134157446515017','134157518667319','134157522016500','134157501869565','134157497987082','134157454090496','134157474146475','134157441799746','134157461644482','134157444731293','134157437478205','134157426444332','134157397710189','134157408693357','134157405070039','134157409490148','134157278547931','134157252727707','134157261136139','134157253766300','134157238831856','134157209784539','134157202197714','134157182631839','134157146132609','134157152383694','134157146718750','13415715239 9490','134157123845041','134157108819002','134157105763973','134157100541361','134157065439665','134157087084911','134157077943531','134157073214091','134157071543838','134157063523317','134157052044802','134157055276210','134157058589557','134157040934345','134157026460721','134157014590149','134157013421831','134156987957586','134156979024208','134156973489535','134156967718696','134156945084328','134156937722633','134156930895196','134156929267743','134156908378520','134156903374086','134156880249521','134156859859325','134156850475256','134156853598204','134156848515178','134156847812647','134156831160752','134156805657532','134156765112139','134156738178431','134156756836545','134156745025924','134156749022694','134156746630155','134156715859091','134156724280129','134156709532823','134156668167363','134156653831717','134156642038681','134156628591096','134156595799103','134156593060189','134156580339625','134156559968559','134156567911093','134156563868653','13415653339 5837','134156539622522','134156542239732','134156509727492','134156525567328','134156514457156','134156517661533','134156505344996','134156496574714','134156485348607','134156478220780','134156467958062','134156459310191','134156446590147','134156435215325','134156425358763','134156405571325','134156394868896','134156375517188','134156359687781','134156323852974','134156304657925','134156295375052','134156274533977','134156268410055','134156258654399','134156249270807','134156252075342','134156251672084','134156245233527','134156229166778','134156210041109','134156181931762','134156160298507','134156165646337','134156126665342','134156105644227','134156103754436','134156041886647','134156043287096','134156030777108','134155987571890','134155963077953','134155921352614','134155875078073','134155878416493','134155837521004','134155800989793','134155789947804','134155771289422','134155672633840','134155651613877','134155571635650','134155472840194','134155379586267','13415536136 4648','134155320475832','134155312146546','134155304458476','134155276719115','134155174496621','134155157927642','134155134222150','134155128089971','134155095253846','134154906583794','134154867159695','134154836664861','134154834015576','134154692191862','134154644332240','134154530489419','134154471196344','134154449762469','134154443317663','134154427777805','134154264521987','134154229767983','134153991782959','134153987735778','134153952196855','134153948326400','134153917739955','134153667768348','134153656988637','134153637465266','134153619010711','134153494660654','134153448856097','134153441846108','134153300413390','134153248862781','134153102798694','134153033520281','134153024394409','134153027014299','134152979972637','134152963844408');

     ## Duplicate array
     ##$duplicateArray = array('134144073396884', '134144032046828', '134143916322531',  '134143912011399', '134143846465705', '134143584772936', '134143542923169', '134143532476900', '134143404466584', '134143303115988', '134143211116904', '134143191363681', '134143161848012', '134143113671002', '134143057675978', '134142960186112', '134142926570822', '134142896554767', '134142703615297', '134142708151455', '134142674253288', '134142611199335', '134142475245336', '134142423999143', '134142350335483', '134142296489337', '134142271476912', '134142143012037', '134142135646348', '134142121064320', '134142120297578', '134142114837363', '134142097213494', '134142064213754', '134142056766978', '134142011814466', '134141998768748', '134141986443251', '134141967280046', '134141967292705', '134141922956590', '134141912835433', '134141908049431', '134141880498910', '134141877871019', '134141868544229', '134141852575767', '134141849657638', '134141826920981', '134141784478099', '134141787971932', '134141579252554', '134141727932961', '134141749196039', '134141746764716', '134141725582937', '134141708725788', '134141710962221', '134141703939488', '134141663432645', '134141652195391', '134141644370890', '134141602094342', '134141555497913', '134141552626361', '134141544035801', '134141529241499', '134141498442197', '134141457530034', '134141454372274', '134141410331226', '134141406998510', '134141401021303', '134141348018682', '134141331236653', '134141329210780', '134141309364407', '134141302125488', '134141278750706', '134141270771992', '134141262470474', '134141209053166', '134141170114456', '134141160749214', '134141146481495', '134141124345233', '134141136813170', '134141121081354', '134141113053352', '134141104430005', '134141085816048', '134141081472845', '134141065242690', '134141031043761', '134141051586460', '134141042869237', '134141025372938', '134141023856351', '134140998013224', '134140951343913', '134140945457282', '134140934915782', '134140909014180', '134140914539542', '134140897682200', '134140879111187', '134140872352087', '134140843655769', '134140834594819', '134140806156380', '134140802391301', '134140784782447', '134140773421312', '134140773116726', '134140765146492', '134140767865161', '134140751212646', '134140732645984', '134140667464432', '134140717722767', '134140699630905', '134140691860700', '134140685721599', '134140673988246', '134140605733686', '134140649157693', '134140624773276', '134140577255987', '134140566414325', '134140566381939', '134140532414893', '134140523582502', '134140482638306', '134140500226624', '134140477871393', '134140479310779', '134140474988721', '134140465258132', '134140459963262', '134140460352704', ' 134140439960932', '134140439083144', '134140416493436', '134140407744419', '134140403797614', '134140398531997', '134140398177141', '134140384827441', '134140376614960', '134140364743104', '134140353285958', '134140350592544', '134140301362544', '134140319026494', '134140304095588', '134140287259404', '134140253168308', '134140237849678', '134140205653399', '134140187238238', '134140186047322', '134140179248724', '134140166141147', '134140160653016', '134140153467660', '134140154884810', '134140138017068', '134140089643188', '134140096560977', '134140093783861', '134140068995951', '134140049276110', '134140019421118', '134140008417331', '134140020351534', '134139990982412', '134139977535299', '134139962841197', '134139952261754', '134139951480572', '134139927683520', '134139920448051', '134139919749634', '134139881161973', '134139868218508', '134139865991183', '134139844819881', '134139824636439', '134139833327283', '134139819661661', '134139767888868', '134139799669255', '134139783789342', '134139769713725', '134139749527240', '134139695119662', '134139690598988', '134139653651585', '134139674825527', '134139623065777', '134139607574753', '134139604060569', '134139578567687', '134139567546856', '134139528283193', '134139496753227', '134139490523934', '134139465269405', '134139447696980', '134139438549478', '134139436957850', '134139348893953', '134139342379216', '134139301540234', '134139271518707', '134139218317250', '134139194540328', '134139149279738', '134139089786060', '134139091373534', '134139057931038', '134139063442637', '134139051788349', '134139037021951', '134139035272817', '134139017881812', '134139014176373', '134138990154579', '134138986572183', '134138966756537', '134138946544460', '134138926094467', '134138914316273', '134138853213812', '134138853689910', '134138846699164', '134138826151537', '134138816497766', '134138751725740', '134138735562509', '134138703938583', '134138649148176', '134138611370079', '134138609162348', '134138577464985', '134138538238011', '134138525113367', '134138503551909', '134138457187796', '134138369534192', '134138306598399', '134138284297634', '134138257782309', '134138257485521', '134138240518822', '134138145250924', '134138141394927', '134138137512256', '134138116239833', '134138072475221', '134138093375530', '134138089875920', '134138087224238', '134138055124214', '134137969070893', '134137956031485', '134137918333439', '134137910223252', '134137888568663', '134137869191159', '134137843430672', '134137823652864', '134137798568268', '134137728541605', '134137713476876', '134137706740544', '134137634519497', '134137589219379', '134137571387420', '134137520652315', '134137509447063', '134136740634591', '134136665993837', '134152854530017', '134152758986803', '134152738336996', '134152713757649', '134152680838174', '134152690684392', '134152600416090', '134152571515548', '134152483774986', '134152175557620', '134152111734260', '134151968981387', '134151923067818', '134151879680017', '134151826980255', '134151830135004', '134151808488621', '134151679549007', '134151630731329', '134151602036037', '134151578813680', '134151556522207', '134151537817289', '134151480599610', '134151426763644', '134151367476281', '134151334728810', '134151267422071', '134151223157572', '134151213898760', '134151205730803', '134151206016902', '134151146171604', '134151138252378', '134151072052212', '134151037612593', '134151008259624', '134151004881248', '134150960119161', '134150817035474', '134150767987350', '134150749671683', '134150715643662', '134150707321524', '134150672426970', '134150539833172', '134150533154732', '134150517454377', '134150513171568', '134150495943059', '134150482587755', '134150469387513', '134150465649303', '134150455515756', '134150450491193', '134150435169109', '134150362688935', '134150346199692', '134150293696854', '134150283340080', '134150265877043', '134150233311136', '134150207896079', '134150216491365', '134150170572254', '134150148755662', '134150134392026', '134150120331197', '134150053734963', '134150027771960', '134150000191238', '134149973142586', '134149938449298', '134149906365241', '134149914384614', '134149913947675', '134149905639295', '134149892753616', '134149874997577', '134149874748008', '134149873249701', '134149871220921', '134149859772423', '134149795168359', '134149787884441', '134149713810487', '134149705633447', '134149670945443', '134149688389966', '134149639582258', '134149671839458', '134149668410855', '134149668462125', '134149664393856', '134149641047024', '134149638275320', '134149614750518', '134149596046661', '134149592776892', '134149567595127', '134149548656061', '134149536970415', '134149510574953', '134149490393896', '134149475495236', '134149456635984', '134149431586331', '134149434881586', '134149420587968', '134149393389232', '134149364595091', '134149175350532', '134149343915196', '134149337961802', '134149310258906', '134149309642504', '134149294867369', '134149292624032', '134149289474250', '134149285231560', '134149237582060', '134149232960463', '134149191095881', '134149171737560', '134149149516218', '134149101659048', '134149088183366', '134149084147113', '134149040670863', '134148987627487', '134148984517055', '134148968288620', '134148941112217', '134148915211982', '134148910927043', '134148886732993', '134148873575178', '134148850046030', '134148830610726', '134148832072442', '134148784264793', '134148785043342', '134148772775031', '134148743340152', '134148757159013', '134148694933259', '134148685341695', '134148665253780', '134148668877304', '134148661852260', '134148661731231', '134148644367030', '134148638479953', '134148621122872', '134148616411779', '134148600328694', '134148585156613', '134148588412077', '134148586676716', '134148583498525', '134148569520841', '134148549798027', '134148558873496', '134148559962957', '134148558267521', '134148557568849', '134148538499116', '134148533753720', '134148528577298', '134148525134921', '134148510960746', '134148512139213', '134148497039796', '134148469229150', '134148430970117', '134148423194975', '134148377024262', '134148405268696', '134148393775087', '134148388688333', '134148383090181', '134148370040719', '134148365792787', '134148349265667', '134148344833324', '134148342336602', '134148338067656', '134148325727813', '134148313982946', '134148313469708', '134148309358723', '134148283897010', '134148293347791', '134148290865704', '134148289024323', '134148260126402', '134148274448062', '134148264270683', '134148267779576', '134148259294236', '134148259042963', '134148251495941', '134148228116781', '134148215586160', '134148216998649', '134148210634293', '134148194972170', '134148197581219', '134148194421948', '134148173631745', '134148153325386', '134148132442560', '134148144318761', '134148119426655', '134148117444999', '134148118846964', '134148111826830', '134148111119032', '134148091512031', '134148080869524', '134148051170675', '134148037418258', '134148039440516', '134148030759108', '134148035114088', '134148028876644', '134148024421674', '134148022616464', '134148017921111', '134148019560116', '134147999516110', '134147991786400', '134147978650294', '134147979091901', '134147976010459', '134147959770816', '134147944375169', '134147935227682', '134147924120435', '134147913023283', '134147901075519', '134147903346214', '134147896319171', '134147895111357', '134147882485113', '134147882827836', '134147874342461', '134147849978485', '134147839673580', '134147832880725', '134147804864808', '134147803940869', '134147796569901', '134147792887712', '134147782132650', '134147775264743', '134147765093184', '134147754365722', '134147738484403', '134147730069906', '134147715433628', '134147722829275', '134147712861877', '134147691210442', '134147681145182', '134147661783411', '134147654092522', '134147655291721', '134147654029130', '134147652987031', '134147627581360', '134147606580635', '134147570876990', '134147534857271', '134147518269378', '134147515237244', '134147514677602', '134147496412237', '134147496237270', '134147487089718', '134147473114991', '134147458013213', '134147424599353', '134147414315586', '134147414214767', '134147382613112', '134147382511659', '134147352947414', '134147325445689', '134147314960046', '134147309595368', '134147301048358', '134147298662727', '134147286928668', '134147276351328', '134147268455819', '134147262816352', '134147261669274', '134147243839635', '134147203871625', '134147181689329', '134147175076626', '134147174633983', '134147175779146', '134147174051811', '134147166623977', '134147161151540', '134147151986191', '134147155421305', '134147153743345', '134147129765910', '134147119840902', '134147107436817', '134147095734810', '134147096036784', '134147084933578', '134147061038587', '134147046146776', '134147033150139', '134147026930444', '134147022773586', '134147012634679', '134147008863274', '134146957093094', '134146928954816', '134146911569200', '134146905940331', '134146897813081', '134146855697285', '134146763139335', '134146714638212', '134146656024027', '134146647032413', '134146641253663', '134146611984344', '134146548633689', '134146544549593', '134146542780197', '134146524852634', '134146513296546', '134146376646018', '134146300787532', '134146263967141', '134146230242153', '134145968084114', '134145946239319', '134145802641000', '134145742341177', '134145602782926', '134145131828901', '134145108016514', '134144796769300', '134144793543106', '134144694738839', '134144690262710', '134144665842491', '134144602681866', '134144543099718', '134144532793483', '134144521436703', '134144489310528', '134144469182695', '134144473256034', '134144477348446', '134144410538427', '134144431826238', '134144331986619', '134144291980344', '134159386189384', '134159378433795', '134159373980306', '134159274988106', '134159215253439', '134159070873344', '134158914393553', '134158944532302', '134158827588737', '134158787419282', '134158553745922', '134158527283838', '134158510422946', '134158460268505', '134158353981058', '134158281713154', '134158247736727', '134158151978949', '134158134363368', '134158087919176', '134157936170815', '134158016652265', '134157936763056', '134157894312500', '134157780517279', '134157765315112', '134157678555613', '134157672743976', '134157556042833', '134157553296225', '134157522016500', '134157497987082', '134157474146475', '134157441799746', '134157461644482', '134157444731293', '134157426444332', '134157408693357', '134157409490148', '134157278547931', '134157261136139', '134157253766300', '134157238831856', '134157152383694', '134157146718750', '134157123845041', '134157108819002', '134157105763973', '134157087084911', '134157073214091', '134157071543838', '134157063523317', '134157058589557', '134157040934345', '134157026460721', '134157013421831', '134156987957586', '134156979024208', '134156973489535', '134156967718696', '134156945084328', '134156937722633', '134156930895196', '134156929267743', '134156908378520', '134156903374086', '134156880249521', '134156850475256', '134156853598204', '134156848515178', '134156847812647', '134156831160752', '134156738178431', '134156756836545', '134156745025924', '134156749022694', '134156715859091', '134156724280129', '134156709532823', '134156668167363', '134156628591096', '134156595799103', '134156593060189', '134156580339625', '134156563868653', '134156539622522', '134156542239732', '134156525567328', '134156514457156', '134156517661533', '134156505344996', '134156485348607', '134156478220780', '134156467958062', '134156459310191', '134156425358763', '134156405571325', '134156394868896', '134156375517188', '134156323852974', '134156304657925', '134156295375052', '134156274533977', '134156268410055', '134156258654399', '134156249270807', '134156252075342', '134156251672084', '134156245233527', '134156181931762', '134156160298507', '134156126665342', '134156105644227', '134156041886647', '134156043287096', '134156030777108', '134155987571890', '134155963077953', '134155921352614', '134155875078073', '134155837521004', '134155789947804', '134155771289422', '134155651613877', '134155472840194', '134155320475832', '134155312146546', '134155304458476', '134155174496621', '134155157927642', '134155134222150', '134155095253846', '134154906583794', '134154867159695', '134154834015576', '134154644332240', '134154471196344', '134154449762469', '134154427777805', '134154229767983', '134153991782959', '134153952196855', '134153917739955', '134153667768348', '134153656988637', '134153637465266', '134153448856097', '134153248862781', '134153033520281', '134153024394409', '134152979972637', '134152963844408');


     $orderArrCount = count($orderArr);
     
     $duplicateOrders = array();
     $authenticAppIdArr = array();

     for($i=0;$i<$orderArrCount;$i++){
         
         $ipay4meOrder = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderArr[$i])->toArray();
         
         if(count($ipay4meOrder) > 0) {
            $origRequestId = $ipay4meOrder[0]['order_request_detail_id'];
            $transService = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($origRequestId)->toArray();
            

            if(count($transService) > 0){
                if(!in_array($transService[0]['app_id'], $authenticAppIdArr)){
                    $authenticAppIdArr[] = $transService[0]['app_id'];
                }else{
                    $duplicateRequestId[] = $origRequestId;
                }
            }
        }
    }

    $duplicateRequestIdCount = count($duplicateRequestId);
    if($duplicateRequestIdCount > 0){
        for($j=0;$j<$duplicateRequestIdCount;$j++){
            $ipay4meOrderArr = Doctrine::getTable('Ipay4meOrder')->findByOrderRequestDetailId($duplicateRequestId[$j])->toArray();
            if(isset($ipay4meOrderArr[0]['order_number']) && $ipay4meOrderArr[0]['created_at'] > '2012-07-04 00:00:00'){
                $duplicateOrders[] =  $ipay4meOrderArr[0]['order_number'];
            }
        }
    }

    echo "<pre>";
    print_r($duplicateOrders);
    exit;
}

public function executeFindNonDuplicateOrderNumber(sfWebRequest $request){

     ##$orderArr = array('134159003598931','134159012850809','134181631230314', '134182349456660','134182661266113','134182698461261','134182703613312');

     ##$orderArr = array('134159012850809','134181631230314', '134182349456660','134182661266113','134182698461261');

//     $orderArr = array( '134144247315788', '134144180652056', '134144163092547', '134144158899841', '134144153343636', '134144135577905', '134144002348372', '134143952145189', '134143907096205', '134143846250677', '134143800884300', '134143772659420', '134143756382780', '134143755534848', '134143657322410', '134143640376079', '134143583986397', '134143552524237', '134143549522117', '134143545337465', '134143515797744', '134143450936299', '134143447684398', '134143448226932', '134143415997926', '134143415145495', '134143419179293', '134143389377732', '134143346655674', '134143235693161', '134143231014627', '134143183733049', '134143160599711', '134143157939708', '134143151780890', '134143088921294', '134143063186001', '134143017467403', '134142987850499', '134142995525755', '134142987221368', '134142931367512', '134142909030514', '13414287 4015742', '134142856290992', '134142765989448', '134142754410268', '134142643240342', '134142621686090', '134142546568437', '134142496854265', '134142419671537', '134142310297900', '134142306513397', '134142283976756', '134142283245086', '134142275010223', '134142195774964', '134142191647824', '134142187523347', '134142161114553', '134142177398049', '134142169786765');
//     $orderArr = array( '134142165753874', '134142121566779', '134142120193970', '134142057592987', '134142051177796', '134142048913331', '134142030453516', '134142030940126', '134142008438100', '134141949232807', '134141942947138', '134141942661952', '134141937982737', '134141916165777', '134141914617835', '134141883919993', '134141897082118', '134141896169247', '134141827133765', '134141807247502', '134141563656798', '134141789268135', '134141778065592', '134141755550160', '134141738095245', '134141734945457', '134141693464985', '134141670732439', '134141665039668', '134141607610512', '134141614982526', '134141610760023', '134141579687202', '134141526148841', '134141517991297', '134141496641160', '134141467750384', '134141432975421', '134141430279756', '134141425581708', '134141387611290', '134141380791315', '134141376592781', '134141289360818', '134141270092468', '134141170671053', '134141247643596', '134141242380053', '134141235457211', '134141228897350', '134141154532561', '134141147267775', '134141115072909', '134141121646333', '134141127114213', '134141083647806', '134141099076181', '134141073497450', '134141075050241', '134141066517851', '134141035639680', '134141018718014', '134141016589486');
//     $orderArr = array( '134140977135257', '134140974743649', '134140936883076', '134140927160070', '134140918679916', '134140879951522', '134140911094463', '134140878788470', '134140865758977', '134140832820220', '134140847788920', '134140842840750', '13 4140825029620', '134140802968710', '134140796191389', '134140785827955', '134140714381208', '134140721514318', '134140708885611', '134140695922478', '134140669491402', '134140654197670', '134140648387657', '134140645415039', '134140625051737', '134140622143514', '134140599030172', '134140579754826', '134140546636954', '134140565126675', '134140533433735', '134140499142535', '134140497084000', '134140476346109', '134140491613980', '134140493625550', '134140428921216', '134140481267238', '134140475346866', '134140474765783', '134140469590884', '134140452493295', '134140444034754', '134140422238079', '134140343794031', '134140321530947', '134140301596277', '134140282099645', '134140256469757', '134140236863937', '134140232423243', '134140226282735', '134140206762947', '134140187070704', '134140158792049', '134140145532020', '134140135313767', '134140099225060', '134140089527972', '134140078038918', '134140070083450', '134140053142095', '134140048111666');
//     $orderArr = array( '134140038223240', '134140018890432', '134140020498463', '134139986768748', '13413996783368 4', '134139917619855', '134139964075180', '134139951872428', '134139944443496', '134139899683184', '134139885718335', '134139858689495', '134139864646839', '134139829951788', '134139793522704', '134139794022881', '134139778122279', '134139768961298', '134139755028664', '134139740925173', '134139715360663', '134139712161229', '134139699055230', '134139661665739', '134139652996452', '134139637245798', '134139627934036', '134139545849959', '134139558166991', '134139530935067', '134139516736141', '134139511520473', '134139510853944', '134139446821636', '134139459412427', '134139455844508', '134139456754968', '134139443815594', '134139347867143', '134139327376109', '134139325265701', '134139304630930', '134139242486658', '134139220828919', '134139201154441', '134139191512346', '134139162757562', '134139137349595', '134139113944816', '134139049777597', '134138997498915', '134138990243414', '134138980030023', '134138945093449', '134138935549710', '134138877111635', '134138882282104', '134138827914999', '134138797616275', '134138785490510', '134138744450674', '134138587226908', '134138587692787');
//     $orderArr = array( '134138420854501', '134138376531236', '134138348387119', '134138308162035', '134138109595273', '134138087316365', '134138098060742', '134138076734583', '134138077173128', '134138016127969', '134138014195870', '134137978424721', '134137891757925', '134137860449252', '134137659820613', '134137658154911', '134137617559660', '134137334443741', '134136696857359', '134136683184898', '134136661835564', '13413664 6577158', '134152878652310', '134152768275746', '134152756461997', '134152699933371', '134152636947444', '134152528462902', '134152510586893', '134152468471775', '134152347116423', '134152267144858', '134152198965193', '134152166439111', '134152147819449', '134152143330547', '134152114162631', '134152026073407', '134152030538695', '134151949311534', '134151943729534', '134151910652355', '134151896588370', '134151902844735', '134151889159014', '134151874899636', '134151805383483', '134151735096403', '134151731976847', '134151683531160', '134151672142660', '134151666530719', '134151651017729', '13415150714 0926', '134151500472790', '134151368818905', '134151355116774', '134151332570350', '134151321315285', '134151312776874', '134151288649366', '134151245416441', '134151234698486');
//     $orderArr = array( '134151212757993', '134151176279815', '134151158387853', '134151156425182', '134151137333462', '134151101979187', '134151036988388', '134151005321839', '134150994862795', '134150936874609', '134150914766363', '134150804657150', '134150802982104', '134150801283294', '134150755499758', '134150742486634', '134150733738990', '134150717246316', '134150715323133', '134150686668994', '134150599030300', '134150592495721', '134150595229390', '13415055915 8981', '134150545449119', '134150529742217', '134150532635295', '134150453531307', '134150443738036', '134150377654529', '134150368148285', '134150367853011', '134150303035456', '134150292191370', '134150285055861', '134150280919466', '134150237524643', '134150208173629', '134150105118778', '134150085583063', '134150064197575', '134150059135884', '134150058679994', '134149939695581', '134149929222092', '134149885740939', '134149865283954', '134149808047293', '134149787470413', '134149764116885', '134149740057269', '134149731186598', '134149730547987', '134149704920194', '134149670417214', '134149652186120', '134149643527785', '134149590787585', '134149589463962', '134149589838453', '134149560663796', '134149569031258', '134149571822773');
//     $orderArr = array( '134149509332145', '134149479078798', '134149474312686', '134149463732038', '134149428281452', '134149344581271', '134149394253005', '134149279464046', '134149358497374', '134149346728144', '134149298499581', '134149265033169', '134149266437757', '134149255875557', '134149248230457', '134149234959855', '134149202564817', '134149171351642', '134149153131890', '134149127338780', '134149103618185', '134149098099053', '134149086819915', '134149072433861', '134149058690639', '134149017322154', '134148989421633', '134148983983811', '134148957772053', '134148953596892', '134148919510225', '134148897666131', '134148881445738', '134148866199713', '134148783046829', '134148734896727', '134148740832558', '134148718970802', '134148683145494', '134148646749777', '134148623759276', '134148617976000', '134148603217659', '134148599116882', '134148582629599', '134148578995321', '134148495271356', '134148489826331', '134148483264880', '134148465220313', '134148439831120', '13414843975 2514', '134148383451157', '134148431612082', '134148415832803', '134148407282087', '134148389771691', '134148376574879', '134148365411758', '134148357053453', '134148291670896', '134148283355252', '134148088754457');
//     $orderArr = array( '134148056892654', '134148054161261', '134148045274505', '134148045837887', '134148021491043', '134147979836630', '134148002814333', '134147978350831', '134147938442440', '134147935071576', '134147929032536', '134147840659866', '134147818940781', '134147775028864', '13414773943 5599', '134147673321885', '134147667696382', '134147657725405', '134147570881989', '134147630945333', '134147577314031', '134147527166579', '134147379339177', '134147356051318', '134147334545438', '134147313015446', '134147309921462', '134147293541147', '134147286830376', '134147255893889', '134147221314591', '134147202140076', '134146994672751', '134146972590494', '134146948340819', '134146821164389', '134146446530283', '134146155353306', '134146033535411', '134145923751442', '134145820497893', '134145798643907', '134145262552223', '134144695392557', '134144602816940', '134159439147746', '134159412155616', '134159399497025', '134159365880514', '134159359398135', '134159339555105', '134159252620077', '134159244846766', '134159024675767', '134159013930785', '134158996673378', '134158977180717', '134158878822288', '134158841970888', '134158813459353', '134158722424744', '134158653275138', '134158641127828');
//     $orderArr = array( '134158625293168', '134158607288555', '134158572074728', '134158471642787', '134158452950287', '134158454388436', '134158442011143', '134158437434845', '134158370718561', '134158326769479', '134158290575175', '134158247469321', '134158232331313', '134158155635555', '134158141867437', '134158147579680', '13415805096 0336', '134158029855883', '134158012294658', '134157953190640', '134157926069750', '134157896426739', '134157807287419', '134157800892335', '134157781135638', '134157774271782', '134157765924690', '134157661645087', '134157712536595', '134157690997467', '134157677315905', '134157680781149', '134157572491452', '134157552478344', '134157446515017', '134157518667319', '134157501869565', '134157454090496', '134157437478205', '134157397710189', '134157405070039', '134157252727707', '134157209784539', '134157202197714', '134157182631839', '134157146132609', '134157152399490', '134157100541361', '134157065439665', '134157077943531', '134157052044802', '134157055276210', '134157014590149', '134156859859325', '134156805657532', '134156765112139', '134156746630155', '134156653831717', '134156642038681', '134156559968559', '134156567911093', '134156533395837', '134156509727492');
//     $orderArr = array( '134156496574714', '134156446590147', '134156435215325', '134156359687781', '134156229166778', '134156210041109', '134156165646337', '134156103754436', '134155878416493', '134155800989793', '134155672633840', '134155571635650', '134155379586267', '134155361364648', '134155276719115', '134155128089971', '134154836664861', '134154692191862', '134154530489419', '134154443317663', '134154264521987', '134153987735778', '134153948326400', '134153619010711', '134153494660654', '134153441846108', '134153300413390', '134153102798694', '134153027014299');

 ##$valuCard = array('134159439147746', '134159412155616', '134159399497025', '134159365880514', '134159359398135', '134159339555105', '134159252620077', '134159244846766', '134159024675767', '134159013930785', '134158996673378', '134158977180717', '134158878822288', '134158841970888', '134158813459353', '134158722424744', '134158653275138', '134158641127828', '134158625293168', '134158607288555', '134158572074728', '134158471642787', '134158452950287', '134158454388436', '134158442011143', '134158437434845', '134158370718561', '134158326769479', '134158290575175', '134158247469321', '134158232331313', '134158155635555', '134158141867437', '134158147579680', '134158050960336', '134158029855883', '134158012294658', '134157953190640', '134157926069750', '134157896426739', '134157807287419', '134157800892335', '134157781135638', '134157774271782', '134157765924690', '134157661645087', '134157712536595', '134157690997467', '134157677315905', '134157680781149', '134157572491452', '134157552478344', '134157446515017', '134157518667319', '134157501869565', '134157454090496', '134157437478205', '134157397710189', '134157405070039', '134157252727707', '134157209784539', '134157202197714', '134157182631839', '134157146132609', '134157152399490', '134157100541361', '134157065439665', '134157077943531', '134157052044802', '134157055276210', '134157014590149', '134156859859325', '134156805657532', '134156765112139', '134156746630155', '134156653831717', '134156642038681', '134156559968559', '134156567911093', '134156533395837', '134156509727492', '134156496574714', '134156446590147', '134156435215325', '134156359687781', '134156229166778', '134156210041109', '134156165646337', '134156103754436', '134155878416493', '134155800989793', '134155672633840', '134155571635650', '134155379586267', '134155361364648', '134155276719115', '134155128089971', '134154836664861', '134154692191862', '134154530489419', '134154443317663', '134154264521987', '134153987735778', '134153948326400', '134153619010711', '134153494660654', '134153441846108', '134153300413390', '134153102798694', '134153027014299', '134152878652310', '134152768275746', '134152756461997', '134152699933371', '134152636947444', '134152528462902', '134152510586893', '134152468471775', '134152347116423', '134152267144858', '134152198965193', '134152166439111', '134152147819449', '134152143330547', '134152114162631', '134152026073407', '134152030538695', '134151949311534', '134151943729534', '134151910652355', '134151896588370', '134151902844735', '134151889159014', '134151874899636', '134151805383483', '134151735096403', '134151731976847', '134151683531160', '134151672142660', '134151666530719', '134151651017729', '134151507140926', '134151500472790', '134151368818905', '134151355116774', '134151332570350', '134151321315285', '134151312776874', '134151288649366', '134151245416441', '134151234698486', '134151212757993', '134151176279815', '134151158387853', '134151156425182', '134151137333462', '134151101979187', '134151036988388', '134151005321839', '134150994862795', '134150936874609', '134150914766363', '134150804657150', '134150802982104', '134150801283294', '134150755499758', '134150742486634', '134150733738990', '134150717246316', '134150715323133', '134150686668994', '134150599030300', '134150592495721', '134150595229390', '134150559158981', '134150545449119', '134150529742217', '134150532635295', '134150453531307', '134150443738036', '134150377654529', '134150368148285', '134150367853011', '134150303035456', '134150292191370', '134150285055861', '134150280919466', '134150237524643', '134150208173629', '134150105118778', '134150085583063', '134150064197575', '134150059135884', '134150058679994', '134149939695581', '134149929222092', '134149885740939', '134149865283954', '134149808047293', '134149787470413', '134149764116885', '134149740057269', '134149731186598', '134149730547987', '134149704920194', '134149670417214', '134149652186120', '134149643527785', '134149590787585', '134149589463962', '134149589838453', '134149560663796', '134149569031258', '134149571822773', '134149509332145', '134149479078798', '134149474312686', '134149463732038', '134149428281452', '134149344581271', '134149394253005', '134149279464046', '134149358497374', '134149346728144', '134149298499581', '134149265033169', '134149266437757', '134149255875557', '134149248230457', '134149234959855', '134149202564817', '134149171351642', '134149153131890', '134149127338780', '134149103618185', '134149098099053', '134149086819915', '134149072433861', '134149058690639', '134149017322154', '134148989421633', '134148983983811', '134148957772053', '134148953596892', '134148919510225', '134148897666131', '134148881445738', '134148866199713', '134148783046829', '134148734896727', '134148740832558', '134148718970802', '134148683145494', '134148646749777', '134148623759276', '134148617976000', '134148603217659', '134148599116882', '134148582629599', '134148578995321', '134148495271356', '134148489826331', '134148483264880', '134148465220313', '134148439831120', '134148439752514', '134148383451157', '134148431612082', '134148415832803', '134148407282087', '134148389771691', '134148376574879', '134148365411758', '134148357053453', '134148291670896', '134148283355252', '134148088754457', '134148056892654', '134148054161261', '134148045274505', '134148045837887', '134148021491043', '134147979836630', '134148002814333', '134147978350831', '134147938442440', '134147935071576', '134147929032536', '134147840659866', '134147818940781', '134147775028864', '134147739435599', '134147673321885', '134147667696382', '134147657725405', '134147570881989', '134147630945333', '134147577314031', '134147527166579', '134147379339177', '134147356051318', '134147334545438', '134147313015446', '134147309921462', '134147293541147', '134147286830376', '134147255893889', '134147221314591', '134147202140076', '134146994672751', '134146972590494', '134146948340819', '134146821164389', '134146446530283', '134146155353306', '134146033535411', '134145923751442', '134145820497893', '134145798643907', '134145262552223', '134144695392557', '134144602816940', '134144247315788', '134144180652056', '134144163092547', '134144158899841', '134144153343636', '134144135577905', '134144002348372', '134143952145189', '134143907096205', '134143846250677', '134143800884300', '134143772659420', '134143756382780', '134143755534848', '134143657322410', '134143640376079', '134143583986397', '134143552524237', '134143549522117', '134143545337465', '134143515797744', '134143450936299', '134143447684398', '134143448226932', '134143415997926', '134143415145495', '134143419179293', '134143389377732', '134143346655674', '134143235693161', '134143231014627', '134143183733049', '134143160599711', '134143157939708', '134143151780890', '134143088921294', '134143063186001', '134143017467403', '134142987850499', '134142995525755', '134142987221368', '134142931367512', '134142909030514', '134142874015742', '134142856290992', '134142765989448', '134142754410268', '134142643240342', '134142621686090', '134142546568437', '134142496854265', '134142419671537', '134142310297900', '134142306513397', '134142283976756', '134142283245086', '134142275010223', '134142195774964', '134142191647824', '134142187523347', '134142161114553', '134142177398049', '134142169786765', '134142165753874', '134142121566779', '134142120193970', '134142057592987', '134142051177796', '134142048913331', '134142030453516', '134142030940126', '134142008438100', '134141949232807', '134141942947138', '134141942661952', '134141937982737', '134141916165777', '134141914617835', '134141883919993', '134141897082118', '134141896169247', '134141827133765', '134141807247502', '134141563656798', '134141789268135', '134141778065592', '134141755550160', '134141738095245', '134141734945457', '134141693464985', '134141670732439', '134141665039668', '134141607610512', '134141614982526', '134141610760023', '134141579687202', '134141526148841', '134141517991297', '134141496641160', '134141467750384', '134141432975421', '134141430279756', '134141425581708', '134141387611290', '134141380791315', '134141376592781', '134141289360818', '134141270092468', '134141170671053', '134141247643596', '134141242380053', '134141235457211', '134141228897350', '134141154532561', '134141147267775', '134141115072909', '134141121646333', '134141127114213', '134141083647806', '134141099076181', '134141073497450', '134141075050241', '134141066517851', '134141035639680', '134141018718014', '134141016589486', '134140977135257', '134140974743649', '134140936883076', '134140927160070', '134140918679916', '134140879951522', '134140911094463', '134140878788470', '134140865758977', '134140832820220', '134140847788920', '134140842840750', '134140825029620', '134140802968710', '134140796191389', '134140785827955', '134140714381208', '134140721514318', '134140708885611', '134140695922478', '134140669491402', '134140654197670', '134140648387657', '134140645415039', '134140625051737', '134140622143514', '134140599030172', '134140579754826', '134140546636954', '134140565126675', '134140533433735', '134140499142535', '134140497084000', '134140476346109', '134140491613980', '134140493625550', '134140428921216', '134140481267238', '134140475346866', '134140474765783', '134140469590884', '134140452493295', '134140444034754', '134140439960932', '134140422238079', '134140343794031', '134140321530947', '134140301596277', '134140282099645', '134140256469757', '134140236863937', '134140232423243', '134140226282735', '134140206762947', '134140187070704', '134140158792049', '134140145532020', '134140135313767', '134140099225060', '134140089527972', '134140078038918', '134140070083450', '134140053142095', '134140048111666', '134140038223240', '134140018890432', '134140020498463', '134139986768748', '134139967833684', '134139917619855', '134139964075180', '134139951872428', '134139944443496', '134139899683184', '134139885718335', '134139858689495', '134139864646839', '134139829951788', '134139793522704', '134139794022881', '134139778122279', '134139768961298', '134139755028664', '134139740925173', '134139715360663', '134139712161229', '134139699055230', '134139661665739', '134139652996452', '134139637245798', '134139627934036', '134139545849959', '134139558166991', '134139530935067', '134139516736141', '134139511520473', '134139510853944', '134139446821636', '134139459412427', '134139455844508', '134139456754968', '134139443815594', '134139347867143', '134139327376109', '134139325265701', '134139304630930', '134139242486658', '134139220828919', '134139201154441', '134139191512346', '134139162757562', '134139137349595', '134139113944816', '134139049777597', '134138997498915', '134138990243414', '134138980030023', '134138945093449', '134138935549710', '134138877111635', '134138882282104', '134138827914999', '134138797616275', '134138785490510', '134138744450674', '134138587226908', '134138587692787', '134138420854501', '134138376531236', '134138348387119', '134138308162035', '134138109595273', '134138087316365', '134138098060742', '134138076734583', '134138077173128', '134138016127969', '134138014195870', '134137978424721', '134137891757925', '134137860449252', '134137659820613', '134137658154911', '134137617559660', '134137334443741', '134136696857359', '134136683184898', '134136661835564', '134136646577158');
 ##$orderNumberArray=array('134144247315788','134144180652056','134144163092547','134144158899841','134144153343636','134144135577905','134144002348372','134143952145189','134143907096205','134143846250677','134143800884300','134143772659420','134143756382780','134143755534848','134143657322410','134143640376079','134143583986397','134143552524237','134143549522117','134143545337465','134143515797744','134143450936299','134143447684398','134143448226932','134143415997926','134143415145495','134143419179293','134143389377732','134143346655674','134143235693161','134143231014627','134143183733049','134143160599711','134143157939708','134143151780890','134143088921294','134143063186001','134143017467403','134142987850499','134142995525755','134142987221368','134142931367512','134142909030514','134142874015742','134142856290992','134142765989448','134142754410268','134142643240342','134142621686090','134142546568437','134142496854265','134142419671537','134142310297900','134142306513397','134142283976756','134142283245086','134142275010223','134142195774964','134142191647824','134142187523347','134142161114553','134142177398049','134142169786765','134142165753874','134142121566779','134142120193970','134142057592987','134142051177796','134142048913331','134142030453516','134142030940126','134142008438100','134141949232807','134141942947138','134141942661952','134141937982737','134141916165777','134141914617835','134141883919993','134141897082118','134141896169247','134141827133765','134141807247502','134141563656798','134141789268135','134141778065592','134141755550160','134141738095245','134141734945457','134141693464985','134141670732439','134141665039668','134141607610512','134141614982526','134141610760023','134141579687202','134141526148841','134141517991297','134141496641160','134141467750384','134141432975421','134141430279756','134141425581708','134141387611290','134141380791315','134141376592781','134141289360818','134141270092468','134141170671053','134141247643596','134141242380053','134141235457211','134141228897350','134141154532561','134141147267775','134141115072909','134141121646333','134141127114213','134141083647806','134141099076181','134141073497450','134141075050241','134141066517851','134141035639680','134141018718014','134141016589486','134140977135257','134140974743649','134140936883076','134140927160070','134140918679916','134140879951522','134140911094463','134140878788470','134140865758977','134140832820220','134140847788920','134140842840750','134140825029620','134140802968710','134140796191389','134140785827955','134140714381208','134140721514318','134140708885611','134140695922478','134140669491402','134140654197670','134140648387657','134140645415039','134140625051737','134140622143514','134140599030172','134140579754826','134140546636954','134140565126675','134140533433735','134140499142535','134140497084000','134140476346109','134140491613980','134140493625550','134140428921216','134140481267238','134140475346866','134140474765783','134140469590884','134140452493295','134140444034754','134140422238079','134140343794031','134140321530947','134140301596277','134140282099645','134140256469757','134140236863937','134140232423243','134140226282735','134140206762947','134140187070704','134140158792049','134140145532020','134140135313767','134140099225060','134140089527972','134140078038918','134140070083450','134140053142095','134140048111666','134140038223240','134140018890432','134140020498463','134139986768748','134139967833684','134139917619855','134139964075180','134139951872428','134139944443496','134139899683184','134139885718335','134139858689495','134139864646839','134139829951788','134139793522704','134139794022881','134139778122279','134139768961298','134139755028664','134139740925173','134139715360663','134139712161229','134139699055230','134139661665739','134139652996452','134139637245798','134139627934036','134139545849959','134139558166991','134139530935067','134139516736141','134139511520473','134139510853944','134139446821636','134139459412427','134139455844508','134139456754968','134139443815594','134139347867143','134139327376109','134139325265701','134139304630930','134139242486658','134139220828919','134139201154441','134139191512346','134139162757562','134139137349595','134139113944816','134139049777597','134138997498915','134138990243414','134138980030023','134138945093449','134138935549710','134138877111635','134138882282104','134138827914999','134138797616275','134138785490510','134138744450674','134138587226908','134138587692787','134138420854501','134138376531236','134138348387119','134138308162035','134138109595273','134138087316365','134138098060742','134138076734583','134138077173128','134138016127969','134138014195870','134137978424721','134137891757925','134137860449252','134137659820613','134137658154911','134137617559660','134137334443741','134136696857359','134136683184898','134136661835564','134136646577158','134152878652310','134152768275746','134152756461997','134152699933371','134152636947444','134152528462902','134152510586893','134152468471775','134152347116423','134152267144858','134152198965193','134152166439111','134152147819449','134152143330547','134152114162631','134152026073407','134152030538695','134151949311534','134151943729534','134151910652355','134151896588370','134151902844735','134151889159014','134151874899636','134151805383483','134151735096403','134151731976847','134151683531160','134151672142660','134151666530719','134151651017729','134151507140926','134151500472790','134151368818905','134151355116774','134151332570350','134151321315285','134151312776874','134151288649366','134151245416441','134151234698486','134151212757993','134151176279815','134151158387853','134151156425182','134151137333462','134151101979187','134151036988388','134151005321839','134150994862795','134150936874609','134150914766363','134150804657150','134150802982104','134150801283294','134150755499758','134150742486634','134150733738990','134150717246316','134150715323133','134150686668994','134150599030300','134150592495721','134150595229390','134150559158981','134150545449119','134150529742217','134150532635295','134150453531307','134150443738036','134150377654529','134150368148285','134150367853011','134150303035456','134150292191370','134150285055861','134150280919466','134150237524643','134150208173629','134150105118778','134150085583063','134150064197575','134150059135884','134150058679994','134149939695581','134149929222092','134149885740939','134149865283954','134149808047293','134149787470413','134149764116885','134149740057269','134149731186598','134149730547987','134149704920194','134149670417214','134149652186120','134149643527785','134149590787585','134149589463962','134149589838453','134149560663796','134149569031258','134149571822773','134149509332145','134149479078798','134149474312686','134149463732038','134149428281452','134149344581271','134149394253005','134149279464046','134149358497374','134149346728144','134149298499581','134149265033169','134149266437757','134149255875557','134149248230457','134149234959855','134149202564817','134149171351642','134149153131890','134149127338780','134149103618185','134149098099053','134149086819915','134149072433861','134149058690639','134149017322154','134148989421633','134148983983811','134148957772053','134148953596892','134148919510225','134148897666131','134148881445738','134148866199713','134148783046829','134148734896727','134148740832558','134148718970802','134148683145494','134148646749777','134148623759276','134148617976000','134148603217659','134148599116882','134148582629599','134148578995321','134148495271356','134148489826331','134148483264880','134148465220313','134148439831120','134148439752514','134148383451157','134148431612082','134148415832803','134148407282087','134148389771691','134148376574879','134148365411758','134148357053453','134148291670896','134148283355252','134148088754457','134148056892654','134148054161261','134148045274505','134148045837887','134148021491043','134147979836630','134148002814333','134147978350831','134147938442440','134147935071576','134147929032536','134147840659866','134147818940781','134147775028864','134147739435599','134147673321885','134147667696382','134147657725405','134147570881989','134147630945333','134147577314031','134147527166579','134147379339177','134147356051318','134147334545438','134147313015446','134147309921462','134147293541147','134147286830376','134147255893889','134147221314591','134147202140076','134146994672751','134146972590494','134146948340819','134146821164389','134146446530283','134146155353306','134146033535411','134145923751442','134145820497893','134145798643907','134145262552223','134144695392557','134144602816940','134159439147746','134159412155616','134159399497025','134159365880514','134159359398135','134159339555105','134159252620077','134159244846766','134159024675767','134159013930785','134158996673378','134158977180717','134158878822288','134158841970888','134158813459353','134158722424744','134158653275138','134158641127828','134158625293168','134158607288555','134158572074728','134158471642787','134158452950287','134158454388436','134158442011143','134158437434845','134158370718561','134158326769479','134158290575175','134158247469321','134158232331313','134158155635555','134158141867437','134158147579680','134158050960336','134158029855883','134158012294658','134157953190640','134157926069750','134157896426739','134157807287419','134157800892335','134157781135638','134157774271782','134157765924690','134157661645087','134157712536595','134157690997467','134157677315905','134157680781149','134157572491452','134157552478344','134157446515017','134157518667319','134157501869565','134157454090496','134157437478205','134157397710189','134157405070039','134157252727707','134157209784539','134157202197714','134157182631839','134157146132609','134157152399490','134157100541361','134157065439665','134157077943531','134157052044802','134157055276210','134157014590149','134156859859325','134156805657532','134156765112139','134156746630155','134156653831717','134156642038681','134156559968559','134156567911093','134156533395837','134156509727492','134156496574714','134156446590147','134156435215325','134156359687781','134156229166778','134156210041109','134156165646337','134156103754436','134155878416493','134155800989793','134155672633840','134155571635650','134155379586267','134155361364648','134155276719115','134155128089971','134154836664861','134154692191862','134154530489419','134154443317663','134154264521987','134153987735778','134153948326400','134153619010711','134153494660654','134153441846108','134153300413390','134153102798694','134153027014299');

//echo count($valuCard);
//echo '<br>';
//echo count($orderNumberArray);

//    $arr = array_diff($valuCard, $orderNumberArray);
//    echo "<pre>";
//    print_r($arr);
//    exit;
//die;


//$valueCardRefundedOrders  = array('134159386189384', '134159378433795', '134159373980306', '134159274988106', '134159215253439', '134159070873344', '134158914393553', '134158944532302', '134158827588737', '134158787419282', '134158553745922', '134158527283838', '134158510422946', '134158460268505', '134158353981058', '134158281713154', '134158247736727', '134158151978949', '134158134363368', '134158087919176', '134157936170815', '134158016652265', '134157936763056', '134157894312500', '134157780517279', '134157765315112', '134157678555613', '134157672743976', '134157556042833', '134157553296225', '134157522016500', '134157497987082', '134157474146475', '134157441799746', '134157461644482', '134157444731293', '134157426444332', '134157408693357', '134157409490148', '134157278547931', '134157261136139', '134157253766300', '134157238831856', '134157152383694', '134157146718750', '134157123845041', '134157108819002', '134157105763973', '134157087084911', '134157073214091', '134157071543838', '134157063523317', '134157058589557', '134157040934345', '134157026460721', '134157013421831', '134156987957586', '134156979024208', '134156973489535', '134156967718696', '134156945084328', '134156937722633', '134156930895196', '134156929267743', '134156908378520', '134156903374086', '134156880249521', '134156850475256', '134156853598204', '134156848515178', '134156847812647', '134156831160752', '134156738178431', '134156756836545', '134156745025924', '134156749022694', '134156715859091', '134156724280129', '134156709532823', '134156668167363', '134156628591096', '134156595799103', '134156593060189', '134156580339625', '134156563868653', '134156539622522', '134156542239732', '134156525567328', '134156514457156', '134156517661533', '134156505344996', '134156485348607', '134156478220780', '134156467958062', '134156459310191', '134156425358763', '134156405571325', '134156394868896', '134156375517188', '134156323852974', '134156304657925', '134156295375052', '134156274533977', '134156268410055', '134156258654399', '134156249270807', '134156252075342', '134156251672084', '134156245233527', '134156181931762', '134156160298507', '134156126665342', '134156105644227', '134156041886647', '134156043287096', '134156030777108', '134155987571890', '134155963077953', '134155921352614', '134155875078073', '134155837521004', '134155789947804', '134155771289422', '134155651613877', '134155472840194', '134155320475832', '134155312146546', '134155304458476', '134155174496621', '134155157927642', '134155134222150', '134155095253846', '134154906583794', '134154867159695', '134154834015576', '134154644332240', '134154471196344', '134154449762469', '134154427777805', '134154229767983', '134153991782959', '134153952196855', '134153917739955', '134153667768348', '134153656988637', '134153637465266', '134153448856097', '134153248862781', '134153033520281', '134153024394409', '134152979972637', '134152963844408', '134152854530017', '134152758986803', '134152738336996', '134152713757649', '134152680838174', '134152690684392', '134152600416090', '134152571515548', '134152483774986', '134152175557620', '134152111734260', '134151968981387', '134151923067818', '134151879680017', '134151826980255', '134151830135004', '134151808488621', '134151679549007', '134151630731329', '134151602036037', '134151578813680', '134151556522207', '134151537817289', '134151480599610', '134151426763644', '134151367476281', '134151334728810', '134151267422071', '134151223157572', '134151213898760', '134151205730803', '134151206016902', '134151146171604', '134151138252378', '134151072052212', '134151037612593', '134151008259624', '134151004881248', '134150960119161', '134150817035474', '134150767987350', '134150749671683', '134150715643662', '134150707321524', '134150672426970', '134150539833172', '134150533154732', '134150517454377', '134150513171568', '134150495943059', '134150482587755', '134150469387513', '134150465649303', '134150455515756', '134150450491193', '134150435169109', '134150362688935', '134150346199692', '134150293696854', '134150283340080', '134150265877043', '134150233311136', '134150207896079', '134150216491365', '134150170572254', '134150148755662', '134150134392026', '134150120331197', '134150053734963', '134150027771960', '134150000191238', '134149973142586', '134149938449298', '134149906365241', '134149914384614', '134149913947675', '134149905639295', '134149892753616', '134149874997577', '134149874748008', '134149873249701', '134149871220921', '134149859772423', '134149795168359', '134149787884441', '134149713810487', '134149705633447', '134149670945443', '134149688389966', '134149639582258', '134149671839458', '134149668410855', '134149668462125', '134149664393856', '134149641047024', '134149638275320', '134149614750518', '134149596046661', '134149592776892', '134149567595127', '134149548656061', '134149536970415', '134149510574953', '134149490393896', '134149475495236', '134149456635984', '134149431586331', '134149434881586', '134149420587968', '134149393389232', '134149364595091', '134149175350532', '134149343915196', '134149337961802', '134149310258906', '134149309642504', '134149294867369', '134149292624032', '134149289474250', '134149285231560', '134149237582060', '134149232960463', '134149191095881', '134149171737560', '134149149516218', '134149101659048', '134149088183366', '134149084147113', '134149040670863', '134148987627487', '134148984517055', '134148968288620', '134148941112217', '134148915211982', '134148910927043', '134148886732993', '134148873575178', '134148850046030', '134148830610726', '134148832072442', '134148784264793', '134148785043342', '134148772775031', '134148743340152', '134148757159013', '134148694933259', '134148685341695', '134148665253780', '134148668877304', '134148661852260', '134148661731231', '134148644367030', '134148638479953', '134148621122872', '134148616411779', '134148600328694', '134148585156613', '134148588412077', '134148586676716', '134148583498525', '134148569520841', '134148549798027', '134148558873496', '134148559962957', '134148558267521', '134148557568849', '134148538499116', '134148533753720', '134148528577298', '134148525134921', '134148510960746', '134148512139213', '134148497039796', '134148469229150', '134148430970117', '134148423194975', '134148377024262', '134148405268696', '134148393775087', '134148388688333', '134148383090181', '134148370040719', '134148365792787', '134148349265667', '134148344833324', '134148342336602', '134148338067656', '134148325727813', '134148313982946', '134148313469708', '134148309358723', '134148283897010', '134148293347791', '134148290865704', '134148289024323', '134148260126402', '134148274448062', '134148264270683', '134148267779576', '134148259294236', '134148259042963', '134148251495941', '134148228116781', '134148215586160', '134148216998649', '134148210634293', '134148194972170', '134148197581219', '134148194421948', '134148173631745', '134148153325386', '134148132442560', '134148144318761', '134148119426655', '134148117444999', '134148118846964', '134148111826830', '134148111119032', '134148091512031', '134148080869524', '134148051170675', '134148037418258', '134148039440516', '134148030759108', '134148035114088', '134148028876644', '134148024421674', '134148022616464', '134148017921111', '134148019560116', '134147999516110', '134147991786400', '134147978650294', '134147979091901', '134147976010459', '134147959770816', '134147944375169', '134147935227682', '134147924120435', '134147913023283', '134147901075519', '134147903346214', '134147896319171', '134147895111357', '134147882485113', '134147882827836', '134147874342461', '134147849978485', '134147839673580', '134147832880725', '134147804864808', '134147803940869', '134147796569901', '134147792887712', '134147782132650', '134147775264743', '134147765093184', '134147754365722', '134147738484403', '134147730069906', '134147715433628', '134147722829275', '134147712861877', '134147691210442', '134147681145182', '134147661783411', '134147654092522', '134147655291721', '134147654029130', '134147652987031', '134147627581360', '134147606580635', '134147570876990', '134147534857271', '134147518269378', '134147515237244', '134147514677602', '134147496412237', '134147496237270', '134147487089718', '134147473114991', '134147458013213', '134147424599353', '134147414315586', '134147414214767', '134147382613112', '134147382511659', '134147352947414', '134147325445689', '134147314960046', '134147309595368', '134147301048358', '134147298662727', '134147286928668', '134147276351328', '134147268455819', '134147262816352', '134147261669274', '134147243839635', '134147203871625', '134147181689329', '134147175076626', '134147174633983', '134147175779146', '134147174051811', '134147166623977', '134147161151540', '134147151986191', '134147155421305', '134147153743345', '134147129765910', '134147119840902', '134147107436817', '134147095734810', '134147096036784', '134147084933578', '134147061038587', '134147046146776', '134147033150139', '134147026930444', '134147022773586', '134147012634679', '134147008863274', '134146957093094', '134146928954816', '134146911569200', '134146905940331', '134146897813081', '134146855697285', '134146763139335', '134146714638212', '134146656024027', '134146647032413', '134146641253663', '134146611984344', '134146548633689', '134146544549593', '134146542780197', '134146524852634', '134146513296546', '134146376646018', '134146300787532', '134146263967141', '134146230242153', '134145968084114', '134145946239319', '134145802641000', '134145742341177', '134145602782926', '134145131828901', '134145108016514', '134144796769300', '134144793543106', '134144694738839', '134144690262710', '134144665842491', '134144602681866', '134144543099718', '134144532793483', '134144521436703', '134144489310528', '134144469182695', '134144473256034', '134144477348446', '134144410538427', '134144431826238', '134144331986619', '134144291980344', '134144073396884', '134144032046828', '134143916322531', '134143912011399', '134143846465705', '134143584772936', '134143542923169', '134143532476900', '134143404466584', '134143303115988', '134143211116904', '134143191363681', '134143161848012', '134143113671002', '134143057675978', '134142960186112', '134142926570822', '134142896554767', '134142703615297', '134142708151455', '134142674253288', '134142611199335', '134142475245336', '134142423999143', '134142350335483', '134142296489337', '134142271476912', '134142143012037', '134142135646348', '134142121064320', '134142120297578', '134142114837363', '134142097213494', '134142064213754', '134142056766978', '134142011814466', '134141998768748', '134141986443251', '134141967280046', '134141967292705', '134141922956590', '134141912835433', '134141908049431', '134141880498910', '134141877871019', '134141868544229', '134141852575767', '134141849657638', '134141826920981', '134141784478099', '134141787971932', '134141579252554', '134141727932961', '134141749196039', '134141746764716', '134141725582937', '134141708725788', '134141710962221', '134141703939488', '134141663432645', '134141652195391', '134141644370890', '134141602094342', '134141555497913', '134141552626361', '134141544035801', '134141529241499', '134141498442197', '134141457530034', '134141454372274', '134141410331226', '134141406998510', '134141401021303', '134141348018682', '134141331236653', '134141329210780', '134141309364407', '134141302125488', '134141278750706', '134141270771992', '134141262470474', '134141209053166', '134141170114456', '134141160749214', '134141146481495', '134141124345233', '134141136813170', '134141121081354', '134141113053352', '134141104430005', '134141085816048', '134141081472845', '134141065242690', '134141031043761', '134141051586460', '134141042869237', '134141025372938', '134141023856351', '134140998013224', '134140951343913', '134140945457282', '134140934915782', '134140909014180', '134140914539542', '134140897682200', '134140879111187', '134140872352087', '134140843655769', '134140834594819', '134140806156380', '134140802391301', '134140784782447', '134140773421312', '134140773116726', '134140765146492', '134140767865161', '134140751212646', '134140732645984', '134140667464432', '134140717722767', '134140699630905', '134140691860700', '134140685721599', '134140673988246', '134140605733686', '134140649157693', '134140624773276', '134140577255987', '134140566414325', '134140566381939', '134140532414893', '134140523582502', '134140482638306', '134140500226624', '134140477871393', '134140479310779', '134140474988721', '134140465258132', '134140459963262', '134140460352704', '134140439083144', '134140416493436', '134140407744419', '134140403797614', '134140398531997', '134140398177141', '134140384827441', '134140376614960', '134140364743104', '134140353285958', '134140350592544', '134140301362544', '134140319026494', '134140304095588', '134140287259404', '134140253168308', '134140237849678', '134140205653399', '134140187238238', '134140186047322', '134140179248724', '134140166141147', '134140160653016', '134140153467660', '134140154884810', '134140138017068', '134140089643188', '134140096560977', '134140093783861', '134140068995951', '134140049276110', '134140019421118', '134140008417331', '134140020351534', '134139990982412', '134139977535299', '134139962841197', '134139952261754', '134139951480572', '134139927683520', '134139920448051', '134139919749634', '134139881161973', '134139868218508', '134139865991183', '134139844819881', '134139824636439', '134139833327283', '134139819661661', '134139767888868', '134139799669255', '134139783789342', '134139769713725', '134139749527240', '134139695119662', '134139690598988', '134139653651585', '134139674825527', '134139623065777', '134139607574753', '134139604060569', '134139578567687', '134139567546856', '134139528283193', '134139496753227', '134139490523934', '134139465269405', '134139447696980', '134139438549478', '134139436957850', '134139348893953', '134139342379216', '134139301540234', '134139271518707', '134139218317250', '134139194540328', '134139149279738', '134139089786060', '134139091373534', '134139057931038', '134139063442637', '134139051788349', '134139037021951', '134139035272817', '134139017881812', '134139014176373', '134138990154579', '134138986572183', '134138966756537', '134138946544460', '134138926094467', '134138914316273', '134138853213812', '134138853689910', '134138846699164', '134138826151537', '134138816497766', '134138751725740', '134138735562509', '134138703938583', '134138649148176', '134138611370079', '134138609162348', '134138577464985', '134138538238011', '134138525113367', '134138503551909', '134138457187796', '134138369534192', '134138306598399', '134138284297634', '134138257782309', '134138257485521', '134138240518822', '134138145250924', '134138141394927', '134138137512256', '134138116239833', '134138072475221', '134138093375530', '134138089875920', '134138087224238', '134138055124214', '134137969070893', '134137956031485', '134137918333439', '134137910223252', '134137888568663', '134137869191159', '134137843430672', '134137823652864', '134137798568268', '134137728541605', '134137713476876', '134137706740544', '134137634519497', '134137589219379', '134137571387420', '134137520652315', '134137509447063', '134136740634591', '134136665993837');
//$duplicateArray = array('134144073396884', '134144032046828', '134143916322531',  '134143912011399', '134143846465705', '134143584772936', '134143542923169', '134143532476900', '134143404466584', '134143303115988', '134143211116904', '134143191363681', '134143161848012', '134143113671002', '134143057675978', '134142960186112', '134142926570822', '134142896554767', '134142703615297', '134142708151455', '134142674253288', '134142611199335', '134142475245336', '134142423999143', '134142350335483', '134142296489337', '134142271476912', '134142143012037', '134142135646348', '134142121064320', '134142120297578', '134142114837363', '134142097213494', '134142064213754', '134142056766978', '134142011814466', '134141998768748', '134141986443251', '134141967280046', '134141967292705', '134141922956590', '134141912835433', '134141908049431', '134141880498910', '134141877871019', '134141868544229', '134141852575767', '134141849657638', '134141826920981', '134141784478099', '134141787971932', '134141579252554', '134141727932961', '134141749196039', '134141746764716', '134141725582937', '134141708725788', '134141710962221', '134141703939488', '134141663432645', '134141652195391', '134141644370890', '134141602094342', '134141555497913', '134141552626361', '134141544035801', '134141529241499', '134141498442197', '134141457530034', '134141454372274', '134141410331226', '134141406998510', '134141401021303', '134141348018682', '134141331236653', '134141329210780', '134141309364407', '134141302125488', '134141278750706', '134141270771992', '134141262470474', '134141209053166', '134141170114456', '134141160749214', '134141146481495', '134141124345233', '134141136813170', '134141121081354', '134141113053352', '134141104430005', '134141085816048', '134141081472845', '134141065242690', '134141031043761', '134141051586460', '134141042869237', '134141025372938', '134141023856351', '134140998013224', '134140951343913', '134140945457282', '134140934915782', '134140909014180', '134140914539542', '134140897682200', '134140879111187', '134140872352087', '134140843655769', '134140834594819', '134140806156380', '134140802391301', '134140784782447', '134140773421312', '134140773116726', '134140765146492', '134140767865161', '134140751212646', '134140732645984', '134140667464432', '134140717722767', '134140699630905', '134140691860700', '134140685721599', '134140673988246', '134140605733686', '134140649157693', '134140624773276', '134140577255987', '134140566414325', '134140566381939', '134140532414893', '134140523582502', '134140482638306', '134140500226624', '134140477871393', '134140479310779', '134140474988721', '134140465258132', '134140459963262', '134140460352704', ' 134140439960932', '134140439083144', '134140416493436', '134140407744419', '134140403797614', '134140398531997', '134140398177141', '134140384827441', '134140376614960', '134140364743104', '134140353285958', '134140350592544', '134140301362544', '134140319026494', '134140304095588', '134140287259404', '134140253168308', '134140237849678', '134140205653399', '134140187238238', '134140186047322', '134140179248724', '134140166141147', '134140160653016', '134140153467660', '134140154884810', '134140138017068', '134140089643188', '134140096560977', '134140093783861', '134140068995951', '134140049276110', '134140019421118', '134140008417331', '134140020351534', '134139990982412', '134139977535299', '134139962841197', '134139952261754', '134139951480572', '134139927683520', '134139920448051', '134139919749634', '134139881161973', '134139868218508', '134139865991183', '134139844819881', '134139824636439', '134139833327283', '134139819661661', '134139767888868', '134139799669255', '134139783789342', '134139769713725', '134139749527240', '134139695119662', '134139690598988', '134139653651585', '134139674825527', '134139623065777', '134139607574753', '134139604060569', '134139578567687', '134139567546856', '134139528283193', '134139496753227', '134139490523934', '134139465269405', '134139447696980', '134139438549478', '134139436957850', '134139348893953', '134139342379216', '134139301540234', '134139271518707', '134139218317250', '134139194540328', '134139149279738', '134139089786060', '134139091373534', '134139057931038', '134139063442637', '134139051788349', '134139037021951', '134139035272817', '134139017881812', '134139014176373', '134138990154579', '134138986572183', '134138966756537', '134138946544460', '134138926094467', '134138914316273', '134138853213812', '134138853689910', '134138846699164', '134138826151537', '134138816497766', '134138751725740', '134138735562509', '134138703938583', '134138649148176', '134138611370079', '134138609162348', '134138577464985', '134138538238011', '134138525113367', '134138503551909', '134138457187796', '134138369534192', '134138306598399', '134138284297634', '134138257782309', '134138257485521', '134138240518822', '134138145250924', '134138141394927', '134138137512256', '134138116239833', '134138072475221', '134138093375530', '134138089875920', '134138087224238', '134138055124214', '134137969070893', '134137956031485', '134137918333439', '134137910223252', '134137888568663', '134137869191159', '134137843430672', '134137823652864', '134137798568268', '134137728541605', '134137713476876', '134137706740544', '134137634519497', '134137589219379', '134137571387420', '134137520652315', '134137509447063', '134136740634591', '134136665993837', '134152854530017', '134152758986803', '134152738336996', '134152713757649', '134152680838174', '134152690684392', '134152600416090', '134152571515548', '134152483774986', '134152175557620', '134152111734260', '134151968981387', '134151923067818', '134151879680017', '134151826980255', '134151830135004', '134151808488621', '134151679549007', '134151630731329', '134151602036037', '134151578813680', '134151556522207', '134151537817289', '134151480599610', '134151426763644', '134151367476281', '134151334728810', '134151267422071', '134151223157572', '134151213898760', '134151205730803', '134151206016902', '134151146171604', '134151138252378', '134151072052212', '134151037612593', '134151008259624', '134151004881248', '134150960119161', '134150817035474', '134150767987350', '134150749671683', '134150715643662', '134150707321524', '134150672426970', '134150539833172', '134150533154732', '134150517454377', '134150513171568', '134150495943059', '134150482587755', '134150469387513', '134150465649303', '134150455515756', '134150450491193', '134150435169109', '134150362688935', '134150346199692', '134150293696854', '134150283340080', '134150265877043', '134150233311136', '134150207896079', '134150216491365', '134150170572254', '134150148755662', '134150134392026', '134150120331197', '134150053734963', '134150027771960', '134150000191238', '134149973142586', '134149938449298', '134149906365241', '134149914384614', '134149913947675', '134149905639295', '134149892753616', '134149874997577', '134149874748008', '134149873249701', '134149871220921', '134149859772423', '134149795168359', '134149787884441', '134149713810487', '134149705633447', '134149670945443', '134149688389966', '134149639582258', '134149671839458', '134149668410855', '134149668462125', '134149664393856', '134149641047024', '134149638275320', '134149614750518', '134149596046661', '134149592776892', '134149567595127', '134149548656061', '134149536970415', '134149510574953', '134149490393896', '134149475495236', '134149456635984', '134149431586331', '134149434881586', '134149420587968', '134149393389232', '134149364595091', '134149175350532', '134149343915196', '134149337961802', '134149310258906', '134149309642504', '134149294867369', '134149292624032', '134149289474250', '134149285231560', '134149237582060', '134149232960463', '134149191095881', '134149171737560', '134149149516218', '134149101659048', '134149088183366', '134149084147113', '134149040670863', '134148987627487', '134148984517055', '134148968288620', '134148941112217', '134148915211982', '134148910927043', '134148886732993', '134148873575178', '134148850046030', '134148830610726', '134148832072442', '134148784264793', '134148785043342', '134148772775031', '134148743340152', '134148757159013', '134148694933259', '134148685341695', '134148665253780', '134148668877304', '134148661852260', '134148661731231', '134148644367030', '134148638479953', '134148621122872', '134148616411779', '134148600328694', '134148585156613', '134148588412077', '134148586676716', '134148583498525', '134148569520841', '134148549798027', '134148558873496', '134148559962957', '134148558267521', '134148557568849', '134148538499116', '134148533753720', '134148528577298', '134148525134921', '134148510960746', '134148512139213', '134148497039796', '134148469229150', '134148430970117', '134148423194975', '134148377024262', '134148405268696', '134148393775087', '134148388688333', '134148383090181', '134148370040719', '134148365792787', '134148349265667', '134148344833324', '134148342336602', '134148338067656', '134148325727813', '134148313982946', '134148313469708', '134148309358723', '134148283897010', '134148293347791', '134148290865704', '134148289024323', '134148260126402', '134148274448062', '134148264270683', '134148267779576', '134148259294236', '134148259042963', '134148251495941', '134148228116781', '134148215586160', '134148216998649', '134148210634293', '134148194972170', '134148197581219', '134148194421948', '134148173631745', '134148153325386', '134148132442560', '134148144318761', '134148119426655', '134148117444999', '134148118846964', '134148111826830', '134148111119032', '134148091512031', '134148080869524', '134148051170675', '134148037418258', '134148039440516', '134148030759108', '134148035114088', '134148028876644', '134148024421674', '134148022616464', '134148017921111', '134148019560116', '134147999516110', '134147991786400', '134147978650294', '134147979091901', '134147976010459', '134147959770816', '134147944375169', '134147935227682', '134147924120435', '134147913023283', '134147901075519', '134147903346214', '134147896319171', '134147895111357', '134147882485113', '134147882827836', '134147874342461', '134147849978485', '134147839673580', '134147832880725', '134147804864808', '134147803940869', '134147796569901', '134147792887712', '134147782132650', '134147775264743', '134147765093184', '134147754365722', '134147738484403', '134147730069906', '134147715433628', '134147722829275', '134147712861877', '134147691210442', '134147681145182', '134147661783411', '134147654092522', '134147655291721', '134147654029130', '134147652987031', '134147627581360', '134147606580635', '134147570876990', '134147534857271', '134147518269378', '134147515237244', '134147514677602', '134147496412237', '134147496237270', '134147487089718', '134147473114991', '134147458013213', '134147424599353', '134147414315586', '134147414214767', '134147382613112', '134147382511659', '134147352947414', '134147325445689', '134147314960046', '134147309595368', '134147301048358', '134147298662727', '134147286928668', '134147276351328', '134147268455819', '134147262816352', '134147261669274', '134147243839635', '134147203871625', '134147181689329', '134147175076626', '134147174633983', '134147175779146', '134147174051811', '134147166623977', '134147161151540', '134147151986191', '134147155421305', '134147153743345', '134147129765910', '134147119840902', '134147107436817', '134147095734810', '134147096036784', '134147084933578', '134147061038587', '134147046146776', '134147033150139', '134147026930444', '134147022773586', '134147012634679', '134147008863274', '134146957093094', '134146928954816', '134146911569200', '134146905940331', '134146897813081', '134146855697285', '134146763139335', '134146714638212', '134146656024027', '134146647032413', '134146641253663', '134146611984344', '134146548633689', '134146544549593', '134146542780197', '134146524852634', '134146513296546', '134146376646018', '134146300787532', '134146263967141', '134146230242153', '134145968084114', '134145946239319', '134145802641000', '134145742341177', '134145602782926', '134145131828901', '134145108016514', '134144796769300', '134144793543106', '134144694738839', '134144690262710', '134144665842491', '134144602681866', '134144543099718', '134144532793483', '134144521436703', '134144489310528', '134144469182695', '134144473256034', '134144477348446', '134144410538427', '134144431826238', '134144331986619', '134144291980344', '134159386189384', '134159378433795', '134159373980306', '134159274988106', '134159215253439', '134159070873344', '134158914393553', '134158944532302', '134158827588737', '134158787419282', '134158553745922', '134158527283838', '134158510422946', '134158460268505', '134158353981058', '134158281713154', '134158247736727', '134158151978949', '134158134363368', '134158087919176', '134157936170815', '134158016652265', '134157936763056', '134157894312500', '134157780517279', '134157765315112', '134157678555613', '134157672743976', '134157556042833', '134157553296225', '134157522016500', '134157497987082', '134157474146475', '134157441799746', '134157461644482', '134157444731293', '134157426444332', '134157408693357', '134157409490148', '134157278547931', '134157261136139', '134157253766300', '134157238831856', '134157152383694', '134157146718750', '134157123845041', '134157108819002', '134157105763973', '134157087084911', '134157073214091', '134157071543838', '134157063523317', '134157058589557', '134157040934345', '134157026460721', '134157013421831', '134156987957586', '134156979024208', '134156973489535', '134156967718696', '134156945084328', '134156937722633', '134156930895196', '134156929267743', '134156908378520', '134156903374086', '134156880249521', '134156850475256', '134156853598204', '134156848515178', '134156847812647', '134156831160752', '134156738178431', '134156756836545', '134156745025924', '134156749022694', '134156715859091', '134156724280129', '134156709532823', '134156668167363', '134156628591096', '134156595799103', '134156593060189', '134156580339625', '134156563868653', '134156539622522', '134156542239732', '134156525567328', '134156514457156', '134156517661533', '134156505344996', '134156485348607', '134156478220780', '134156467958062', '134156459310191', '134156425358763', '134156405571325', '134156394868896', '134156375517188', '134156323852974', '134156304657925', '134156295375052', '134156274533977', '134156268410055', '134156258654399', '134156249270807', '134156252075342', '134156251672084', '134156245233527', '134156181931762', '134156160298507', '134156126665342', '134156105644227', '134156041886647', '134156043287096', '134156030777108', '134155987571890', '134155963077953', '134155921352614', '134155875078073', '134155837521004', '134155789947804', '134155771289422', '134155651613877', '134155472840194', '134155320475832', '134155312146546', '134155304458476', '134155174496621', '134155157927642', '134155134222150', '134155095253846', '134154906583794', '134154867159695', '134154834015576', '134154644332240', '134154471196344', '134154449762469', '134154427777805', '134154229767983', '134153991782959', '134153952196855', '134153917739955', '134153667768348', '134153656988637', '134153637465266', '134153448856097', '134153248862781', '134153033520281', '134153024394409', '134152979972637', '134152963844408');
//
//echo count($valueCardRefundedOrders);
//echo "<br />";
//echo count($duplicateArray);
//
//$arr = array_diff($duplicateArray, $valueCardRefundedOrders);
//echo "<pre>";
//print_r($arr);
//exit;


//
//
// $duplicateArray = array('134143912011399',  '134144331986619', '134144473256034', '134151077751151', '134151119712368', '134140255394916', '134140474988721', '134140566414325', '134140685721599', '134144431826238', '134144032046828', '134148783112971', '134144073396884', '134144602816940', '134148342336602', '134143542923169', '134143584772936', '134073659833781', '134073685994960', '134073759424317', '134073779538675', '134143916322531', '134144034765338', '134131313332230', '134143846465705', '134147978650294', '134152854530017', '134156485348607', '134143660458621', '134144489310528', '134148191957344', '134142611199335', '134142674253288', '134151556522207', '134142121064320', '134142143012037', '134142393565471', '134142417475532', '134156258654399', '134156295375052', '134156425358763', '134176991038365', '134143728555618', '134141104430005', '134141160749214', '134141348018682', '134141381259935', '134143817337673', '134141552626361', '134142056766978', '134142350335483', '134143057675978', '134143532476900', '134146611984344', '134146905940331', '134147008863274', '134147806399732', '134147839673580', '134147691210442', '134147765093184', '134143603384755', '134143624464880', '134143678172539', '134079756236892', '134139017881812', '134141406998510', '134141419963969', '134141498442197', '134147796569901', '134149600421075', '134150016884375', '134150063282151', '134150077526551', '134150107528529', '134150237167383', '134152962694004', '134153086028314', '134157568735419', '134158624060111', '134160949955491', '134140532414893', '134140649157693', '134140802391301', '134140934915782', '134141023856351', '134141121081354', '134141146481495', '134141329210780', '134142708151455', '134147792887712', '134148017921111', '134148210634293', '134152718733399', '134156516439635', '134164431180141', '134143161848012', '134143191363681', '134143364834768', '134143404466584', '134148117351733', '134148832072442', '134148984517055', '134149023618944', '134151004881248', '134134672198904', '134134732694997', '134139272371432', '134140428071028', '134140477871393', '134143504881894', '134143545534060', '134143303115988', '134151800472261', '134151869272029', '134151932942212', '134152090017954', '134147976010459', '134143211116904', '134143801553944', '134141170114456', '134141188429652', '134177718534243', '134143022827183', '134143573962821', '134144625679408', '134144697312898', '134143102958724', '134143131165324', '134148080869524', '134143113671002', '134143194021923', '134135774465747', '134135803050215', '134135823828626', '134140667464432', '134140773421312', '134140806156380', '134141544035801', '134142926570822', '134142896554767', '134175315136049', '134176400718666', '134146544549593', '134152111734260', '134156745025924', '134157040934345', '134167337353117', '134167362615257', '134144410538427', '134144543099718', '134141531738931', '134141666816568', '134141708725788', '134141817671522', '134141999544202', '134142822137207', '134142944356349', '134142965796023', '134144291980344', '134148329346536', '134140413983795', '134140605733686', '134140691860700', '134140717722767', '134140765146492', '134140909014180', '134142971921342', '134146928954816', '134147151986191', '134156555568747', '134156749022694', '134158641127828', '134182513521121', '134118616672810', '134118827037165', '134118933898881', '134135442422499', '134135467336654', '134135504444351', '134142960186112', '134151050380801', '134160896224649', '134142296489337', '134142910489127', '134028598765771', '134028711243429', '134130315046442', '134142703615297', '134142874015742', '134134573552535', '134134608599363', '134142831517195', '134149445122527', '134149454071743', '134149466013327', '134149491952556', '134176043520313', '134140767865161', '134140825029620', '134140865165072', '134140879111187', '134141042869237', '134140945457282', '134140973034580', '134140998013224', '134141032173715', '134141331236653', '134141912835433', '134142450455608', '134144469182695', '134144521436703', '134158827588737', '134142475626503', '134142511869647', '134142608644350', '134142423999143', '134142475245336', '134142548118573', '134142594470969', '134151943049227', '134142271476912', '134176491730222', '134143104137325', '134146646889812', '134147046146776', '134147133839688', '134150134392026', '134150148755662', '134141998768748', '134142135646348', '134141967280046', '134142064213754', '134142097213494', '134142120297578', '134142085839355', '134147570876990', '134141784478099', '134141868544229','134141967292705',  '134141986443251', '134142011814466', '134142114837363', '134154867159695', '134154906583794', '134141602094342', '134156533395837', '134158493899863', '134142099141681', '134142131392662', '134151497747008', '134155615574145', '134147414214767', '134148638479953', '134130513736122', '134138984433424', '134139006695191', '134139149279738', '134139247573165', '134147661783411', '134126019647388', '134126047799797', '134133275983057', '134141663432645', '134141703939488', '134141749196039', '134141974574734', '134140185734782', '134140205653399', '134140244430798', '134141233640492', '134141253127480', '134141284565126', '134141787971932', '134148095166218', '134148476186290', '134141966399502', '134141849657638', '134141189433801', '134141922956590', '134142021477935', '134147804864808', '134150397220295', '134141692867135', '134141725582937', '134141746764716', '134141826920981', '134141908049431', '134157195182135', '134141652195391', '134145602782926', '134148423194975', '134148497039796', '134141852575767', '134141880498910', '134141935185425', '134142001031259', '134142064667385', '134144517795977', '134144552425335', '134145107776404', '134145210552331', '134145368628612', '134148692467454', '134151624291706', '134151826921637', '134159048637767', '134141390679588', '134141442332657', '134141473063078', '134141503679931', '134156756836545', '134138735562509', '134138853689910', '134138948051763', '134138966756537', '134139089786060', '134141644370890', '134141752238274', '134141877871019', '134141929897063', '134141959182343', '134147242320640', '134147261671879', '134150440520607', '134150465649303', '134147375684801', '134147390937008', '134148060642546', '134148077180009', '134148113477376', '134148131332593', '134138751725740', '134138826151537', '134139301540234', '134139990982412', '134147349961974', '134156057374297', '134141727932961', '134141848754484', '134148907427853', '134149101659048', '134149171737560', '134149310258906', '134149787884441', '134141828694614', '134141835144198', '134141844299972', '134141857864390', '134149871220921', '134149892753616', '134150545449119', '134156069272224', '134130931049180', '134139271518707', '134140315749005', '134140482638306', '134147896319171', '134149431586331', '134150707321524', '134158684010443', '134182829036577', '134132385346657', '134139739919500', '134139752036938', '134139824636439', '134140376288314', '134140026761818', '134140301362544', '134141579252554', '134139831744666', '134139984436204', '134140019421118', '134140872352087', '134147654092522', '134148677717876', '134149149516218', '134149209268903', '134133288218770', '134141780933235', '134141710962221', '134141081472845', '134141302125488', '134141371144772', '134150450491193', '134150559158981', '134150749671683', '134152610366231', '134153172012315', '134155348653771', '134159064665527', '134174909578012', '134174926026687', '134178663024322', '134131953138369', '134131988081027', '134141939835340', '134147929032536', '134141454372274', '134141814045314', '134139856140820', '134139871070889', '134139884012489', '134139904979304', '134139927234938', '134139956399918', '134140096560977', '134140153467660', '134140353285958', '134140566381939', '134141513054540', '134143889773607', '134147627652736', '134148359337011', '134152931668972', '134141529241499', '134141555497913', '134147882485113', '134141410331226', '134143473162516', '134157409490148', '134139348893953', '134139365183431', '134139460286960', '134141186367951', '134147924120435', '134182827666573', '134137600121794', '134137636776228', '134138117621793', '134139878727828', '134141421771306', '134141457530034', '134122078555475', '134122101946106', '134140067926296', '134140115879394', '134140128936340', '134141837114168', '134141849789472', '134141203073449', '134141278750706', '134141115882566', '134141288290814', '134141309364407', '134148939562964', '134149003095398', '134141029619436', '134141085816048', '134141254037386', '134141401021303', '134146957093094', '134147012634679', '134147022773586', '134147033150139', '134147084933578', '134147096036784', '134147107436817', '134147129765910', '134147144992000', '134147155421305', '134147166623977', '134147179410245', '134147195380437', '134147217986731', '134147229463195', '134147261669274', '134147286928668', '134147298662727', '134147314960046', '134147325445689', '134147382511659', '134147730069906', '134147913023283', '134148216998649', '134150064197575', '134141175716073', '134141494689954', '134147906853453', '134148028876644', '134148166647531', '134141262470474', '134147161151540', '134147399015750', '134140557688147', '134138986572183', '134139014176373', '134139881161973', '134140843655769', '134141270771992', '134147026930444', '134147061038587', '134147175076626', '134141410045653', '134141285119528', '134141481793277', '134141266478917', '134137957128610', '134137970038270', '134137991563079', '134138042093302', '134138072475221', '134138116239833', '134139868218508', '134141209053166', '134149447536362', '134149462860626', '134149596533174', '134149668462125', '134173844155612', '134130851289333', '134131839583658', '134141124345233', '134135562840990', '134139465269405', '134139508195351', '134139623065777', '134139670426551', '134139769210090', '134139745329267', '134140219895306', '134141113053352', '134141136813170', '134141263227705', '134141031043761', '134141540468125', '134141065242690', '134147496237270', '134141154392683', '134141127828032', '134141153170463', '134141176244468', '134141201879093', '134157304099925', '134157334748097', '134157364482515', '134140384298583', '134158507513241', '134159098444717', '134182502360141', '134139967833684', '134147832880725', '134158612174782', '134141025372938', '134141051586460', '134141096417730', '134141116262521', '134141162968210', '134139051788349', '134139218317250', '134139490523934', '134139504527292', '134140319026494', '134147784292022', '134148487087971', '134156209596702', '134156406121470', '134158711295865', '134158878548350', '134159103528803', '134159316071656', '134182123860799', '134139194540328', '134157211674061', '134157371034207', '134157980461272', '134158507491871', '134182171778380', '134140855746297', '134182688024046', '134140897682200', '134140914539542', '134148313982946', '134148344833324', '134148384814605', '134148757159013', '134148785043342', '134156525567328',
// '134140951343913',  '134141000911028', '134141054418083', '134141151017245', '134141235093594', '134141266899369', '134141399652384', '134141479119844', '134141508564193', '134141817286734', '134146296422370', '134146312811918', '134146356641057', '134146520840829', '134149601673719', '134149985098436', '134140088699256', '134140341493064', '134140409091462', '134140958131067', '134182460215431', '134181417149024', '134140936734737', '134140980684247', '134141006398818', '134142210341074', '134142256441972', '134144316256951', '134144385278296', '134144396441180', '134144420915014', '134148647396464', '134148764712064', '134149200415013', '134149234858895', '134149259263785', '134150085043925', '134150929685023', '134151099444706', '134139799669255', '134139864173649', '134139904967963', '134140059885639', '134140680970524', '134138473857465', '134138503551909', '134138538238011', '134138667212870', '134140834594819', '134139438549478', '134139690598988', '134140364743104', '134140480661633', '134140492776436', '134140500687061', '134112595768778', '134139465282898', '134139567546856', '134139844819881', '134139920448051', '134140089643188', '134140499142535', '134140699630905', '134140501377075', '134140653232410', '134140751212646', '134140784782447', '134140732645984', '134141026747892', '134140838867425', '134141110321454', '134147087493852', '134140773116726', '134140815865218', '134141364448436', '134140673988246', '134140624773276', '134144477348446', '134140728110235', '134139769713725', '134148024421674', '134148153325386', '134149289474250', '134164888170604', '134139783789342', '134139819661661', '134139850337597', '134139920266340', '134140351856734', '134140635916567', '134141011088049', '134141430099259', '134173598761384', '134140707323311', '134147268455819', '134147301048358', '134147606580635', '134148439752514', '134148616411779', '134148772775031', '134156228240284', '134156908275237', '134139792079059', '134139962841197', '134140049276110', '134140068995951', '134140253168308', '134140398177141', '134146230242153', '134146542780197', '134146855697285', '134148525347189', '134139343320115', '134139532216855', '134139668890117', '134140577255987', '134147458013213', '134149285231560', '134140460352704', '134140479310779', '134140500226624', '134140523582502', '134146300787532', '134146548633689', '134146641253663', '134146897813081', '134146911569200', '134147153743345', '134147518269378', '134148596736110', '134148730739376', '134172384754553', '134139436957850', '134139674825527', '134140138017068', '134138694353580', '134138817160591', '134140632733506', '134141033235088', '134140832820220', '134140416493436', '134147352947414', '134124056175330', '134124083245366', '134140426463696', '134140459963262', '134140514292235', '134140407744419', '134140439960932', '134140465258132', '134141591741721', '134148953162599', '134157056374401', '134158544471072', '134158598043674', '134183611554734', '134140398531997', '134140439083144', '134140460054452', '134140710679551', '134148661731231', '134148685341695', '134140376614960', '134140516433864', '134140570473538', '134142376787485', '134142406261146', '134142431981087', '134147696461773', '134147735488708', '134147758114789', '134147792181651', '134147814852974', '134147850749085', '134140350592544', '134140610374229', '134149446122278', '134149475495236', '134149567595127', '134149638275320', '134151932961049', '134151987742916', '134140403797614', '134146656024027', '134140384827441', '134135940135864', '134135960225874', '134140287259404', '134140304095588', '134140357352302', '134140476822349', '134140490770858', '134147534857271', '134147565426291', '134140077752204', '134140093783861', '134140138663872', '134140160653016', '134140237849678', '134147496412237', '134156089796131', '134140324995523', '134140377692310', '134140404285815', '134149873249701', '134139057931038', '134139695119662', '134147175779146', '133545261434904', '134131074236508', '134140186047322', '134140631596012', '134150065141638', '134140150741795', '134140199113673', '134144246163394', '134144359198810', '134144522346993', '134144594824614', '134145277845587', '134145348899095', '134145686191522', '134150233311136', '134150293696854', '134156539622522', '134156715859091', '134156837154897', '134158099465590', '134158122218126', '134158164995053', '134158237362612', '134164897381563', '134140166141147', '134140187238238', '134140965776148', '134140988083930', '134141475079524', '134141528540437', '134144665842491', '134144690262710', '134147452588235', '134149679994566', '134151669130042', '134151768298263', '134156894613483', '134166183285582', '134140154884810', '134140179248724', '134132319229543', '134140190854677', '134139540766030', '134139569369952', '134139841848752', '134139866873593', '134139891096881', '134139833071379', '134139937188290', '134139919749634', '134139952261754', '134147652987031', '134148111119032', '134148144318761', '134148383090181', '134148530293603', '134148552597443', '134148765066137', '134148981421315', '134159338386544', '134139961879636', '134139647481634', '134139865991183', '134140008417331', '134139951480572', '134140036157330', '134139986768748', '134140020351534', '134140068272377', '134140106068464',
// '134139911358594',  '134139917021667', '134144749870486', '134144793543106', '134152758986803', '134139977535299', '134150672426970', '134150715643662', '134155954523786', '134181658785362', '134140020351534', '134140048111666', '134140068272377', '134140106068464', '134139767888868', '134140855954725', '134139927683520', '134147174633983', '134138768288693', '134139369419434', '134139833327283', '134139827356443', '134139963474727', '134140023231330', '134140193034424', '134140207320323', '134140292047229', '134140313936292', '134140408523412', '134139948541741', '134139903245491', '134139934384947', '134139972274568', '134143756518035', '134143795888399', '134157030786334', '134138906377683', '134139027314639', '134139122610830', '134139165419342', '134139330477592', '134139364730520', '134139385598114', '134139444040393', '134139457519862', '134139475222059', '134139552273470', '134139604941219', '134139647776598', '134139718790919', '134139755028664', '134139899060215', '134144089759062', '134144132451699', '134144235293850', '134144336153922', '134144694738839', '134146366781737', '134146537268996', '134155654314862', '134139749527240', '134139901182890', '134139958666734', '134140024287957', '134140584371860', '134139822374439', '134139870055168', '134138093375530', '134138141394927', '134138369534192', '134139299879032', '134141958726766', '134142021065645', '134142707082124', '134142978980014', '134143021819391', '134145278983131', '134087288865584', '134095926476908', '134118082051909', '134118165069927', '134139864646839', '134138233842602', '134138466323164', '134138789665912', '134139777048050', '134139889689642', '134143817615195', '134143911743838', '134135030580654', '134139653651585', '134139726031404', '134144766121422', '134144796769300', '134139717040102', '134139762259409', '134140519819119', '134151347295160', '134165751075090', '134139604060569', '134139508317748', '134139607574753', '134149211519990', '134149232960463', '134139676623218', '134140150741795', '134140199113673', '134144246163394', '134144359198810', '134144522346993', '134144594824614', '134145277845587', '134145348899095', '134156837154897', '134158099465590', '134158122218126', '134158164995053', '134158237362612', '134165030653824', '134139496753227', '134139549084048', '134139565855067', '134139575017127', '134139474045779', '134140089485188', '134140554265508', '134140632974430', '134140916797446', '134140742632487', '134147808974708', '134148008371655', '134148054161261', '134148093443696', '134148270840733', '134148370953188', '134148910927043', '134149201053798', '134149450080454', '134150485495173', '134156144729981', '134156268113850', '134156443272103', '134156798825392', '134139447696980', '134148510960746', '134177160814077', '134139342379216', '134137634519497', '134137910223252', '134138101878800', '134139023273031', '134139137526590', '134139163646339', '134139376541163', '134139420216143', '134139777532340', '134139835732568', '134139946089159', '134144286584822', '134144501594967', '134144630967928', '134144855699224', '134145014760725', '134145724525154', '134145965782238', '134146033535411', '134146277263789', '134146962361307', '134147495861198', '134147912097710', '134148002766511', '134153053185646', '134153214885067', '134153383037420', '134154738287613', '134155468324092', '134156559261242', '134156880112798', '134158013258418', '134179289318762', '134139497692779', '134139561832817', '134135434067093', '134135466053918', '134135604983019', '134135663524173', '134135729027083', '134135761030148', '134135809516741', '134135837318612', '134135868857168', '134136459947944', '134138457187796', '134138525113367', '134138552253906', '134138587692787', '134139191512346', '134139352235985', '134140234751649', '134140651199310', '134142155659448', '134142180963907', '134142238359537', '134143419816316', '134144236668524', '134144310543674', '134144856724945', '134146708636999', '134147887827870', '134148279235380', '134149894221088', '134149927062307', '134152700371031', '134155005685573', '134139035272817', '134139063442637', '134139091373534', '134172050534370', '134149218868742', '134149294090299', '134149347366302', '134149390676745', '134149480389238', '134140870792352', '134152470112182', '134152716422201', '134138816497766', '134138846699164', '134138990154579', '134139037021951', '134139481672044', '134139529827876', '134140257257906', '134148469859805', '134139459398077', '134138926094467', '134145773647702', '134145802641000', '134145915440177', '134145960581333', '134146376646018', '134148557568849', '134156570313824', '134126413925309', '134126600293871', '134126632728239', '134128563424861', '134128699366673', '134129000028878', '134132133389101', '134132152520530', '134132253070076', '134132771495033', '134132814358593', '134132916447594', '134137616150581', '134137737237118', '134138081772261', '134138326141656', '134143058811880', '134148489256934', '134139455844508', '134139229125314', '134132463428319', '134133870710962', '134133875573695', '134133880732759', '134140469936263', '134181899261228', '134106577193777', '134106605749114', '134106484897130', '134118159099693', '134118205653914', '134135732679456', '134135764146896', '134135840564190', '134136004540496', '134136278183838', '134136310874788', '134136365490503', '134136579345701', '134137865696059', '134139234372203', '134139301320353', '134138772734980', '134140778164371', '134147478657258', '134149362254195', '134156174617836', '134138914316273', '134138946544460', '134139124762453', '134139191388386', '134139237546659', '134139282210570', '134141625144313', '134141698284729', '134143906654567', '134150310652560', '134150358740309', '134159088193237', '134159142553097', '134159539126804', '134138965715912', '134144513614216', '134144532793483', '134153341760280', '134153364390996', '134153454838174', '134153468948462', '134153509815332', '134154219995147', '134154229029766', '134138577464985', '134138609162348', '134138703938583', '134138816650181', '134138853213812', '134138954755242', '134139302838487', '134141438645083', '134141483055310', '134141592266254', '134141549111217', '134141576161383', '134141723639621', '134141745172296', '134143806749623', '134143852995031', '134144304420400', '134144344748806', '134144660625672', '134144825336160', '134144890524264', '134145301480857', '134146159720280', '134146357876604', '134146889140024', '134148056063480', '134148215972307', '134149067550833', '134149428629001', '134149596976559', '134153052213257', '134153108679557', '134153187489779', '134153352993838', '134154183928292', '134154546946494', '134155448535468', '134156394012113', '134163311730085', '134131348178762', '134138807323662', '134147813036466', '134138611370079', '134138751710935', '134138794628480', '134139111772923', '134140569749455', '134138649148176', '134139455381993', '134143807027253', '134147011983378', '134158163760324', '134138770954852',
// '134136150119538',  '134136164785133', '134136184120387', '134136194175283', '134136218726124', '134136250184720', '134136282074224', '134136524928403', '134136564863782', '134136582128280', '134136611724333', '134136646577158', '134136665993837', '134136740634591', '134137509447063', '134137571387420', '134137589219379', '134137713476876', '134137798568268', '134137823652864', '134137843430672', '134137869191159', '134138055124214', '134138087224238', '134138114755864', '134138137512256', '134138257782309', '134138284297634', '134138306598399', '134138335481584', '134138407587697', '134138455060952', '134138724155393', '134138331228293', '134137706740544', '134137728541605', '134137969070893', '134138089875920', '134138145250924', '134138240518822', '134138257485521', '134140691284881', '134142541484612', '134145924272101', '134147116168601', '134147132634628', '134148044182610', '134148075178386', '134148644715404', '134134780740804', '134137942980185', '134137976430174', '134146787048368', '134131751987735', '134138077173128', '134140148415393', '134146513296546', '134149592776892', '134156577160232', '134138098060742', '134138103685639', '134138868448129', '134138063580160', '134141544376451', '134141562434692', '134145946239319', '134146888491321', '134147022094512', '134147175948285', '134146481341944', '134146524852634', '134146122388478', '134151932972056', '134137819362689', '134137844235278', '134137863297125', '134137888568663', '134137918333439', '134137956031485', '134138198258432', '134138221252924', '134138237836965', '134146056237120', '134146137662612', '134146153454126', '134149353785381', '134149403417409', '134149419015471', '134149464366795', '134149487181786', '134149514536183', '134149548656061', '134150121681439', '134153370322356', '134154915859866', '134155138992118', '134155887360394', '134175934738608', '134137906858935', '134140458560795', '134140508680998', '134140545363013', '134140565348033', '134144249356721', '134144279328220', '134137520652315', '134187793996282', '134187819983363', '134188791336811', '134136060616274', '134137855255942', '134137866383397', '134137913037621', '134137962425467', '134147095734810', '134147119840902', '134147181689329', '134147262816352', '134147313015446', '134148614761686', '134148740967308', '134149959070276', '134151709235979', '134154814253599', '134155793039709', '134161143050927', '134085154483345', '134087008274645', '134120101381804', '134128290111980', '134136229693742', '134136246732421', '134136271654568', '134136283186096', '134136898836528', '134137060459648', '134138221749958', '134145682138648', '134147673678998', '134148128014211', '134156470828081', '134164769659417', '134164836417214', '134179393352490', '134179513856201', '134179536844192', '134136435915210', '134136481938752', '134136623266093', '134136642148163', '134147424599353', '134144370177789', '134152690684392', '134152713757649', '134152917410601', '134153023874390', '134157073214091', '134158353981058', '134159730530957', '134152107131515', '134152205253848', '134152350954617', '134152612680122', '134152663015894', '134156668167363', '134156930895196', '134152604362011', '134152645364779', '134152680838174', '134152738336996', '134152946844153', '134148621122872', '134148644367030', '134148665253780', '134150767987350', '134150817035474', '134156850475256', '134183264936013', '134152571515548', '134152600416090', '134151507140926', '134151537817289', '134151597179226', '134151636844344', '134151655492603', '134152008244153', '134152129824380', '134152483774986', '134169348855753', '134169666477926', '134156945084328', '134152491678869', '134178196727540', '134157611254770', '134157644532941', '134157670249485', '134152175557620', '134152425984843', '134154443317663', '134150116627545', '134151830135004', '134151879680017', '134149935833332', '134149719577393', '134149906365241', '134150027771960', '134151923067818', '134151426763644', '134151480599610', '134151681844120', '134149874748008', '134151968981387', '134159373980306', '134176679542215', '134151808488621', '133798226574624', '134124490836344', '134149639582258', '134151826980255', '134166900723271', '134143950518039', '134147243839635', '134149614750518', '134150151336242', '134151205730803', '134148132442560', '134149972564095', '134150057890068', '134150192243947', '134150314316913', '134150360111510', '134150554184872', '134150744951845', '134150960119161', '134151037612593', '134151072052212', '134151206016902', '134151370465545', '134151406748060', '134151875934836', '134140730581517', '134151832724369', '134151865693644', '134152057715925', '134151896018346', '134182641138830', '134151873539907', '134151939190211', '134151267422071', '134151679549007', '134151791517462', '134151807926127', '134151828483089', '134151871127048', '134153831891430', '134153917739955', '134154229767983', '134126554840936', '134126621123146', '134126714745570', '134129298751185', '134148549798027', '134151602036037', '134169175983188', '133971677097431', '133971706079979', '134151578813680', '134151630731329', '134151705419535', '134151773492135', '134062342576514', '134062618519521', '134062857680426', '134062889580544', '134071192092437', '134128491411841', '134128552651631', '134132016417732', '134132034342265', '134133097521887', '134133767770528', '134135738829383', '134135793998615', '134138336170536', '134140600188210', '134143932248570', '134146466313227', '134149337961802', '134151570149254', '134064728889155', '134065293666498', '134065369227083', '134065400384644', '134151223157572', '134151367476281', '134151395024985', '134151629120878', '134152134540190', '134157079794157', '134157339510467', '134158862210699', '134159379136981', '134151403249462', '134151334728810', '134140213828227', '134140243530024', '134140272467289', '134140388466224', '134140417345317', '134140524315993', '134142400932899', '134142497864071', '134142904442228', '134146153932021', '134146384118799', '134151304859223', '134151678184699', '134150346199692', '134150469387513', '134151138252378', '134153854129284', '134153991782959', '134150207896079', '134150265877043', '134150362688935', '134150455515756', '134159582168944', '134151269947882', '134149859772423', '134149885740939', '134149913947675', '134151213898760', '134151735392997', '134151754581486', '134151859021990', '134151911465185', '134155687691547',
// '134150487590966',  '134150517454377', '134150533154732', '134151146171604', '134158090694222', '134159259072240', '134160042155988', '134151116984824', '134151160731934', '134151219795301', '134167672828027', '134150182686822', '134150217459311', '134150400654090', '134151008259624', '134151328528164', '134151373399549', '134155774182777', '134155822296439', '134151081810599', '134149175350532', '134149393389232', '134148538499116', '134148559962957', '134148600328694', '134156584651361', '134149795168359', '134150053734963', '134156724280129', '134156853598204', '134156967718696', '134151560140738', '134151672942689', '134151769311579', '134159340178423', '134159575145945', '134149565792438', '134149670945443', '134151055171687', '134151574815333', '134150866116968', '134148019560116', '134157474146475', '134157665757386', '134157780398274', '134157719214798', '134158926660882', '134158979618813', '134145079874121', '134145108016514', '134145131828901', '134145170136813', '134148117819559', '134148177445250', '134148194421948', '134148469229150', '134149292624032', '134170143327954', '134150519256881', '134148987627487', '134148309358723', '134147979091901', '134147991786400', '134148035114088', '134150736086107', '134150806941125', '134150883789539', '134150495943059', '134146211837114', '134146235925625', '134146273778728', '134146341694233', '134146573044079', '134146683731178', '134146721168587', '134150539833172', '134150674018104', '134157657921954', '134157678555613', '134150553194082', '134141563656798', '134141828694614', '134141835144198', '134141844299972', '134141857864390', '134149871220921', '134149892753616', '134156069272224', '134148259042963', '134148525134921', '134149394253005', '134149420587968', '134149434881586', '134147487089718', '134147515237244', '134150513171568', '134150435169109', '134150482587755', '134150587393442', '134148037418258', '134148088754457', '134149596046661', '134157522016500', '134089281989244', '134095920735782', '134096415957677', '134122917992564', '134123987279175', '134124000163794', '134124166387813', '134124172735487', '134124183664721', '134130579698597', '134130955837014', '134131395747143', '134138949051209', '134138964294374', '134138971841719', '134138991965802', '134139569046882', '134139971992179', '134140685646593', '134148474471319', '134148830610726', '134150108615535', '134150120331197', '134156563868653', '134158281713154', '134182878879541', '134108407911592', '134150448780980', '134150544557487', '134150900145048', '134150216491365', '134150283340080', '134155837521004', '134155921352614', '134155963077953', '134169175983188', '134150351798779', '134150393031816', '134150675359474', '134150735882664', '134151020323004', '134151067550313', '134152677222264', '134149874997577', '134149905639295', '134149954329795', '134149973142586', '134150000191238', '134150063758966', '134150110749843', '134150361764047', '134150268264285', '134150307049324', '134148362123392', '134150170572254', '134149365758721', '134149429159751', '134149536970415', '134148405268696', '134182072352991', '134149747815263', '134149769923844', '134152979972637', '134153033520281', '134159108483453', '134141029619436', '134141085816048', '134141254037386', '134141401021303', '134141425581708', '134146957093094', '134147012634679', '134147022773586', '134147033150139', '134147084933578', '134147096036784', '134147107436817', '134147129765910', '134147144992000', '134147155421305', '134147166623977', '134147179410245', '134147195380437', '134147217986731', '134147229463195', '134147261669274', '134147286928668', '134147298662727', '134147314960046', '134147325445689', '134147382511659', '134147730069906', '134147913023283', '134148216998649', '134143573962821', '134144625679408', '134144646966614', '134144697312898', '134147754365722', '134147782132650', '134147942750028', '134148033078596', '134151190372444', '134149250684747', '134149264265953', '134149292091704', '134149433346529', '134149914384614', '134149938449298', '134151200977922', '134156323774720', '134156848515178', '134182375042412', '134133870710962', '134133875573695', '134133880732759', '134140469936263', '134147030841678', '134147737492737', '134148999830866', '134149017432497', '134149034244957', '134149164299223', '134182207841358', '134149859772423', '134149913947675', '134151245416441', '134147640556428', '134147822194334', '134147861927121', '134147935227682', '134147959747247', '134147983612772', '134149893166238', '134149641047024', '134149664393856', '134149688389966', '134149713810487', '134149871257624', '134181151998907', '134181994441804', '134149675941446', '134149671839458', '134156304657925', '134158460268505', '134182335635303', '134149661858106', '134149705633447', '134150149016099', '134156613614442', '134182558764677', '134148489826331', '134149668410855', '134139365619395', '134140577453036', '134149125573408', '134149324799814', '134149485463971', '134155320475832', '134157152383694', '134182445492032', '134149343915196', '134149510574953', '134156144457259', '134156249270807', '134156580339625', '134097965963458', '134148091512031', '134071851928734', '134071875519871', '134071897638234', '134071921341216', '134072055932618', '134149456635984', '134149490393896', '134148886732993', '134149040670863', '134155602769509', '134123902416514', '134123954125836', '134149566775531', '134095239251925', '134095256640952', '134095262844488', '134095460717024', '134095474092924', '134095837170475', '134095838828188', '134095863121398', '134095889160828', '134096619358435', '134096630882000', '134104278925138', '134104286787995', '134104293797280', '134107251896703', '134107278715476', '134107294930817', '134121166163819', '134121174035305', '134121182348860', '134121944359849', '134121959588666', '134121969815410', '134125497324236', '134125505427430', '134125534133499', '134129876919772', '134129893018052', '134130210751049', '134130227574832', '134130242066012', '134138462480982', '134138485283570', '134138516327458', '134140620199352', '134140630672627', '134149246926734', '134149547479308', '134147655291721', '134156323852974', '134165651689931',
// '134147712861877',  '134147739435599', '134147775264743', '134148022616464', '134148313469708', '134149294867369', '134149339455814', '134156649260779', '134156738178431', '134156880249521', '134156987957586', '134157164444963', '134147654029130', '134147681145182', '134147803940869', '134148274448062', '134148388688333', '134148528577298', '134148558267521', '134148866199713', '134149088183366', '134149364595091', '134151261614663', '134156143389436', '134148259042963', '134148525134921', '134149420587968', '134149434881586', '134150545449119', '134156069272224', '134147849978485', '134147882827836', '134148039440516', '134148377024262', '134148665662812', '134149690110372', '134150034972550', '134150292211951', '134151232494821', '134155875078073', '134156252075342', '134149263596191', '134149279791384', '134181911159488', '134149309642504', '134149750458473', '134148591413677', '134157444731293', '134158016652265', '134149237582060', '134149286336843', '134149345485548', '134149367178110', '134149396863428', '134148193176896', '134148215586160', '134148260126402', '134148296119270', '134148345772103', '134149542237787', '134148784264793', '134149191095881', '134149320511872', '134148259294236', '134148283897010', '134148402730499', '134148955953737', '134155987571890', '134156100516301', '134156105644227', '134165016166463', '134165239931123', '134149178470173', '134149114011563', '134149148467253', '134149619622930', '134150744286507', '134150754011149', '134156343357255', '134156423838064', '134175511131969', '134097678589294', '134132379391908', '134139713491194', '134140213420769', '134140897083696', '134141063844748', '134147837149270', '134148971267981', '134149035651135', '134148051170675', '134148118846964', '134148264270683', '134148293347791', '134148558873496', '134148586676716', '134148941112217', '134148968288620', '134149058690639', '134149084147113', '134158247736727', '134069712236521', '134069771311543', '134083599244988', '134092287922607', '134149131183034', '134147309595368', '134172881256060', '134149103618185', '134087342185104', '134087386991751', '134087399075051', '134122458553519', '134123377779369', '134125817912193', '134125831029523', '134125845345556', '134130085023766', '134130109219442', '134130124242650', '134130142172945', '134130158344974', '134147715433628', '134147874342461', '134147895111357', '134147999516110', '134148850046030', '134148915211982', '134157461644482', '134157553296225', '134144526423898', '134144555822090', '134144572497842', '134148800495469', '134149371177272', '134147727790000', '134148827142306', '134157408693357', '134148698920941', '134148743340152', '134148984175044', '134148873575178', '134148859595526', '134167664683637', '134167705458764', '134149463732038', '134148668877304', '134159950413141', '134146647032413', '134146714638212', '134146763139335', '134146833429779', '134148588412077', '134148694933259', '134148267779576', '134148290865704', '134148325727813', '134156908378520', '134156937722633', '134148117444999', '134148661852260', '134150575236521', '134150710630860', '134156160298507', '134156268410055', '134148435722925', '134183082089379', '134130402450825', '134130413444399', '134130422435458', '134130444687993', '134130461495661', '134131563438810', '134131589535206', '134132768941210', '134132778864660', '134144652241458', '134144670118446', '134148585156613', '134160231078738', '134148583498525', '134148569520841', '134148489466511', '134150358920820', '134148512139213', '134148533753720', '134148686481685', '134145915914868', '134145944373758', '134148338067656', '134148365792787', '134149731186598', '134143086475338', '134143135622091', '134143169395881', '134148119426655', '134148173631745', '134148194972170', '134148430970117', '134148370040719', '134156958880857', '134174064440266', '134149847092598', '134148197581219', '134148349265667', '134148503618686', '134148919356648', '134157235349489', '134157261136139', '134157313376078', '134148393775087', '134148654777665', '134149089788161', '134149097843316', '134148406579560', '134148896527141', '134148929990950', '134156184698151', '134156603357584', '134148111826830', '134148228116781', '134148289024323', '134148427964151', '134147441285077', '134147514677602', '134147463156828', '134147632368657', '134147722829275', '134153083727541', '134157925235951', '134157969755409', '134178755910426', '134148251495941', '134148302680898', '134148037418258', '134149596046661', '134150453531307', '134157522016500', '134147908741606',  '134147976293195', '134148030759108', '134148121882357', '134148180442212', '134140554265508', '134140632974430', '134140916797446', '134140742632487', '134147808974708', '134148008371655', '134148093443696', '134148270840733', '134148370953188', '134149201053798', '134149450080454', '134157302660142', '134157719459980', '134157845165030', '134158032710794', '134158161780922', '134158332861309', '134139474045779', '134139516736141', '134140089485188', '134148910927043', '134150485495173', '134156144729981', '134156268113850', '134156443272103', '134156798825392', '134147728263945', '134148029174300', '134156251672084', '134159002846890', '134147382613112', '134147414315586', '134147835853660', '134147860253661', '134147959770816', '134148028281831', '134149523611148', '134155982151806', '134156515738771', '134157952754731', '134147175076626', '134147473114991', '134149092257886', '134155734767132', '134147901075519', '134147944375169', '134148147827557', '134144100539676', '134144360784959', '134144449977917', '134144483911250', '134144507887418', '134148062439435', '134148562117265', '134148699930940', '134148773630408', '134148462820302', '134147903346214', '134141607610512', '134147256693187', '134147704644185', '134147738484403', '134122692350217', '134147515078848', '134147532186310', '134147616946042', '134147627581360', '134149468468492', '134147408588368', '134147503661582', '134147701571893', '134147013457168', '134147223615936', '134148032023664', '134114672679932', '134147178891214', '134147314331100', '134080790222292', '134086772190255', '134086796785389', '134086906624472', '134098113084434', '134122634496053', '134122646321635', '134123573433860', '134131797752612', '134131810936708', '134131986233248', '134147174051811', '134147203871625', '134078135594486', '134147095734810', '134147119840902', '134147181689329', '134147262816352', '134148614761686', '134148740967308', '134149959070276', '134151709235979', '134154814253599', '134155793039709', '134161143050927', '134137334443741', '134137855255942', '134137866383397', '134137913037621', '134137962425467', '134147276351328', '134147363315635', '134155789947804', '134146934241285', '134146994672751', '134155304458476', '134147399015750', '134141306558959', '134141386562511', '134147255893889', '134155134222150', '134155174496621', '134155472840194', '134146263967141', '134148069913130', '134148132625518', '134149267898642', '134155312146546', '134145968084114', '134146059880405', '134146081587832', '134180376674556', '134137634519497', '134137910223252', '134138101878800', '134139023273031', '134139137526590', '134139163646339', '134139376541163', '134139420216143', '134139446821636', '134139777532340', '134139835732568', '134139946089159', '134144286584822', '134144501594967', '134144630967928', '134144855699224', '134145014760725', '134145724525154', '134145965782238', '134146277263789', '134146962361307', '134147495861198', '134147912097710', '134148002766511', '134153053185646', '134153214885067', '134153383037420', '134154738287613', '134155468324092', '134156559261242', '134156880112798', '134158013258418', '134179289318762', '134145738865071', '134145776347317', '134145998190689', '134146143263300', '134155095253846', '134145742341177', '134145829598875', '134154028449938', '134084834546216', '134085094647257', '134085527975169', '134086198821875', '134144602681866', '134152963844408', '134153024394409', '134161717447898', '134144073396884', '134144135577905', '134148342336602', '134159146945239', '134159386189384', '134145983141050', '134146015733157', '134153947051665', '134159378433795', '134179434148389', '134156929267743', '134156973489535', '134157058589557', '134157123845041', '134157238831856', '134159274988106', '134159070873344', '134159215253439', '134158944532302', '134182602076491', '134158914393553', '134169082669050', '134156375517188', '134156405571325', '134156514457156', '134168421784332', '134159661142059', '134158974512207', '134159048542825', '134156394868896', '134156517661533', '134156709532823', '134156831160752', '134157071543838', '134157672743976', '134157765315112', '134158134363368', '134158308768039', '134159131369372', '134159455953691', '134160228255347', '134158899941008', '134158964731438', '134158787419282', '134159883629740', '134158527283838', '134158553745922', '134140413983795', '134140605733686', '134140691860700', '134140717722767', '134140765146492', '134140909014180', '134142971921342', '134142995525755', '134146928954816', '134147151986191', '134156555568747', '134156749022694', '134182513521121',
// '134158594864313',  '134165659398726',  '134061503777463', '134061547291417', '134061563021856', '134070113050975', '134157929884933', '134157936763056', '134166357216095', '134158510422946', '134151566243356', '134151587393436', '134151607769932', '134151709659101', '134158413732871', '134160151225096', '134158412010383', '134158491719050', '134159859587630', '134135443329446', '134135500293376', '134155034835192', '134155157927642', '134156903374086', '134158151978949', '134183351765085', '134158400150126', '134158462981774', '134158375565205', '134156041886647', '134156181931762', '134157146718750', '134157780517279', '134156628591096', '134164269963303', '134158236421712', '134182371678678', '134157936170815', '134158087919176', '134183738319117', '134156598454286', '134157253766300', '134158066546309', '134158092593766', '134113903572041', '134157894312500', '134157962641260', '134157990331908', '134159341865569', '134159370643568', '134159546029545', '134122014314152', '134122242461935', '134147871665104', '134153637465266', '134153656988637', '134159130348265', '134159964878190', '134062396271180', '134062466924421', '134062761346654', '134157871150194', '134157501043455', '134182807887682', '134157885719879', '134157556042833', '134055162443928', '134158753418532', '134159160127663', '134160135714981', '134157834075989', '134158996762783', '134157328779621', '134074858379813', '134075031364914', '134157441799746', '134157497987082', '134149145433502', '134149182439843', '134149218026933', '134149249983037', '134149288170710', '134149321850514', '134149370062446', '134149397747353', '134149452717907', '134149502799832', '134149582815254', '134149649634502', '134149957416464', '134150054265874', '134150106554493', '134150143236042', '134155459654696', '134155531390216', '134156077646231', '134157105763973', '134157426444332', '134158233398119', '134159590870025', '134157415995683', '134157444377165', '134183242487494', '134039557245850', '134168829871894', '134147390937008', '134148060642546', '134148077180009', '134148113477376', '134148131332593', '134148166859646', '134148182463382', '134148496096834', '134148516763043', '134148751993398', '134148773349297', '134150009244405', '134150022748915', '134150222684019', '134150273532685', '134150299791723', '134150675628222', '134150697121194', '134155912569512', '134155934855477', '134156162626256', '134156240078416', '134156351922079', '134157191922826', '134157253698704', '134157286414472', '134157359370236', '134157398286986', '134157459126799', '134157523989450', '134157995368183', '134158125341800', '134158147114633', '134158174063439', '134158314595055', '134158462657916', '134158483080323', '134158621059618', '134158945052569', '134159093017097', '134159163380181', '134157278547931', '134157496479034', '134157087084911', '134157108819002', '134157288143213', '134183162795955', '134156274533977', '134156505344996', '134164967576390', '134157152399490', '134157267457094', '134157392968951', '134157444535946', '134157560367332', '134157202197714', '134157026460721', '134157063523317', '134157046143479', '134157124525227', '134158964630817', '134156847812647', '134159795161065', '134156826713758', '134148717295994', '134154785221767', '134156030777108', '134156126665342', '134156245233527', '134156350675746', '134156358662423', '134156459310191', '134156595799103', '134180369143135', '134126434160175', '134126566573550', '134156593060189', '134156631131946', '134181685979844', '134156773244121', '134156788464605', '134156950977096', '134157522013898', '134148606244625', '134156538891184', '134182370563863', '134156542239732', '134141602094342', '134142121566779', '134158493899863', '133941302873894', '133969190512252', '134156657063332', '134156691662722', '134158214049619', '134160274987328',
// '134156467958062',  '134156478220780', '134181692824701', '134142393565471', '134143857018651', '134156258654399', '134156511854186', '134156070369293', '134156176423389', '134152040881674', '134130686559894', '134156161757284', '134156163167846', '134156188949779', '134156218235154', '134156512166376', '134166665869232', '134156043287096', '134146962361307', '134147912097710', '134148002766511', '134153214885067', '134153667768348', '134154834015576', '134155468324092', '134155651613877', '134156559261242', '134156880112798', '134158013258418', '134180227791674', '134179389959494', '134155771289422', '134159598916856', '134155361364648', '134155404585726', '134155379586267', '134093760936616', '134093772583437', '134093789561283', '134094301933188', '134094330664528', '134094534688984', '134095468280633', '134095484074633', '134095499352189', '134095592761768', '134104072541339', '134104119975887', '134121184076317', '134121202535439', '134139815510207', '134145174782648', '134145197473997', '134145339830360', '134145557262797', '134154028449938', '134154264521987', '134154290732382', '134154306939503', '134154449762469', '134154644332240', '134153952196855', '134154427777805', '134154471196344', '134154530489419', '134163348986862', '134154692191862', '134152425984843', '134152175557620', '134152198965193', '134155128089971', '134095581115981', '134095697535167', '134103567853772', '134127189010090', '134153912591337', '134154267770208', '134156975394913', '134122049472559', '134122080234038', '134122093328735', '134153479926361', '134153643259063', '134153448856097', '134170234564869', '133975911442617', '133975940830063', '133999288916047', '134011190490016', '134011335961873', '134030840915442', '134077804141217', '134082167233272', '134137335966084', '134137367070449', '134137393688597', '134139107564938', '134139448123358', '134141224717451', '134141242349987', '134143132054683', '134143837777844', '134143857681605', '134145015892348', '134145077976443', '134145096844484', '134145184245706', '134145219537562', '134147073371377', '134151764148640', '134151924135853', '134151982549937', '134152972770353', '134153005683912', '134153248862781', '134161190021842');
//
//
//
// $totalPaidArray = array('134144247315788', '134144180652056', '134144163092547', '134144158899841', '134144153343636', '134144135577905', '134144073396884', '134144032046828', '134144002348372', '134143952145189', '134143916322531', '134143912011399', '134143907096205', '134143846250677', '134143846465705', '134143800884300', '134143772659420', '134143756382780', '134143755534848', '134143657322410', '134143640376079', '134143584772936', '134143583986397', '134143542923169', '134143552524237', '134143549522117', '134143545337465', '134143532476900', '134143515797744', '134143450936299', '134143447684398', '134143448226932', '134143415997926', '134143415145495', '134143419179293', '134143404466584', '134143389377732', '134143346655674', '134143303115988', '134143235693161', '134143231014627', '134143211116904', '134143191363681', '134143183733049', '134143160599711', '134143161848012', '134143157939708', '134143151780890', '134143113671002', '134143088921294', '134143063186001', '134143057675978', '134143017467403', '134142987850499', '134142995525755', '134142987221368', '134142960186112', '134142931367512', '134142926570822', '134142909030514', '134142896554767', '13414287 4015742', '134142856290992', '134142765989448', '134142754410268', '134142703615297', '134142708151455', '134142674253288', '134142643240342', '134142621686090', '134142611199335', '134142546568437', '134142496854265', '134142475245336', '134142423999143', '134142419671537', '134142350335483', '134142310297900', '134142306513397', '134142296489337', '134142283976756', '134142283245086', '134142275010223', '134142271476912', '134142195774964', '134142191647824', '134142187523347', '134142161114553', '134142177398049', '134142169786765', '134142165753874', '134142143012037', '134142135646348', '134142121566779', '134142121064320', '134142120193970', '134142120297578', '134142114837363', '134142097213494', '134142064213754', '134142057592987', '134142056766978', '134142051177796', '134142048913331', '134142030453516', '134142030940126', '134142008438100', '134142011814466', '134141998768748', '134141986443251', '134141967280046', '134141967292705', '134141949232807', '134141942947138', '134141942661952', '134141937982737', '134141922956590', '134141916165777', '134141914617835', '134141912835433', '134141883919993', '134141908049431', '134141897082118', '134141 896169247', '134141880498910', '134141877871019', '134141868544229', '134141852575767', '134141849657638', '134141826920981', '134141827133765', '134141784478099', '134141807247502', '134141563656798', '134141789268135', '134141787971932', '134141778065592', '134141579252554', '134141727932961', '134141755550160', '134141749196039', '134141746764716', '134141738095245', '134141734945457', '134141725582937', '134141708725788', '134141710962221', '134141703939488', '134141693464985', '134141663432645', '134141670732439', '134141665039668', '134141652195391', '134141644370890', '134141607610512', '134141614982526', '134141610760023', '134141602094342', '134141579687202', '134141555497913', '134141552626361', '134141544035801', '134141529241499', '134141526148841', '134141517991297', '134141496641160', '134141498442197', '134141467750384', '134141457530034', '134141454372274', '134141432975421', '134141430279756', '134141410331226', '134141425581708', '134141406998510', '134141401021303', '134141387611290', '134141380791315', '134141376592781', '134141348018682', '134141331236653', '134141329210780', '134141309364407', '134141302125488', '134141289360818', '1341 41270092468', '134141278750706', '134141270771992', '134141262470474', '134141170671053', '134141247643596', '134141242380053', '134141235457211', '134141228897350', '134141209053166', '134141170114456', '134141160749214', '134141154532561', '134141147267775', '134141146481495', '134141124345233', '134141136813170', '134141115072909', '134141121646333', '134141127114213', '134141121081354', '134141113053352', '134141104430005', '134141083647806', '134141099076181', '134141085816048', '134141073497450', '134141081472845', '134141075050241', '134141066517851', '134141065242690', '134141031043761', '134141051586460', '134141042869237', '134141035639680', '134141025372938', '134141023856351', '134141018718014', '134141016589486', '134140998013224', '134140977135257', '134140974743649', '134140951343913', '134140945457282', '134140936883076', '134140934915782', '134140927160070', '134140918679916', '134140879951522', '134140909014180', '134140911094463', '134140914539542', '134140897682200', '134140878788470', '134140879111187', '134140872352087', '134140865758977', '134140832820220', '134140847788920', '134140843655769', '134140842840750', '134140834594819', '13 4140825029620', '134140806156380', '134140802391301', '134140802968710', '134140796191389', '134140785827955', '134140784782447', '134140773421312', '134140773116726', '134140765146492', '134140767865161', '134140751212646', '134140732645984', '134140667464432', '134140714381208', '134140717722767', '134140721514318', '134140708885611', '134140699630905', '134140695922478', '134140691860700', '134140685721599', '134140673988246', '134140669491402', '134140605733686', '134140654197670', '134140649157693', '134140648387657', '134140645415039', '134140624773276', '134140625051737', '134140622143514', '134140599030172', '134140577255987', '134140579754826', '134140566414325', '134140566381939', '134140546636954', '134140565126675', '134140532414893', '134140533433735', '134140499142535', '134140523582502', '134140482638306', '134140497084000', '134140476346109', '134140500226624', '134140491613980', '134140493625550', '134140428921216', '134140477871393', '134140481267238', '134140479310779', '134140475346866', '134140474988721', '134140474765783', '134140469590884', '134140465258132', '134140459963262', '134140460352704', '134140452493295', '134140444034754', ' 134140439960932', '134140439083144', '134140422238079', '134140416493436', '134140407744419', '134140403797614', '134140398531997', '134140398177141', '134140384827441', '134140376614960', '134140364743104', '134140353285958', '134140350592544', '134140343794031', '134140301362544', '134140321530947', '134140319026494', '134140304095588', '134140301596277', '134140287259404', '134140282099645', '134140256469757', '134140253168308', '134140237849678', '134140236863937', '134140232423243', '134140226282735', '134140206762947', '134140205653399', '134140187070704', '134140187238238', '134140186047322', '134140179248724', '134140166141147', '134140160653016', '134140158792049', '134140153467660', '134140154884810', '134140145532020', '134140135313767', '134140138017068', '134140089643188', '134140099225060', '134140096560977', '134140093783861', '134140089527972', '134140078038918', '134140070083450', '134140068995951', '134140053142095', '134140049276110', '134140048111666', '134140019421118', '134140038223240', '134140008417331', '134140020351534', '134140018890432', '134140020498463', '134139990982412', '134139986768748', '134139977535299', '13413996783368 4', '134139917619855', '134139964075180', '134139962841197', '134139951872428', '134139952261754', '134139951480572', '134139944443496', '134139927683520', '134139920448051', '134139919749634', '134139899683184', '134139885718335', '134139881161973', '134139858689495', '134139868218508', '134139864646839', '134139865991183', '134139844819881', '134139824636439', '134139833327283', '134139829951788', '134139819661661', '134139767888868', '134139799669255', '134139793522704', '134139794022881', '134139783789342', '134139778122279', '134139769713725', '134139768961298', '134139755028664', '134139749527240', '134139740925173', '134139715360663', '134139712161229', '134139699055230', '134139695119662', '134139690598988', '134139653651585', '134139674825527', '134139661665739', '134139652996452', '134139637245798', '134139627934036', '134139623065777', '134139607574753', '134139604060569', '134139545849959', '134139578567687', '134139567546856', '134139558166991', '134139530935067', '134139528283193', '134139516736141', '134139511520473', '134139510853944', '134139496753227', '134139490523934', '134139465269405', '134139446821636', '134139459412427', '134139455844 508', '134139456754968', '134139447696980', '134139443815594', '134139438549478', '134139436957850', '134139348893953', '134139347867143', '134139342379216', '134139327376109', '134139325265701', '134139304630930', '134139301540234', '134139271518707', '134139242486658', '134139220828919', '134139218317250', '134139201154441', '134139194540328', '134139191512346', '134139162757562', '134139149279738', '134139137349595', '134139113944816', '134139089786060', '134139091373534', '134139057931038', '134139063442637', '134139051788349', '134139049777597', '134139037021951', '134139035272817', '134139017881812', '134139014176373', '134138997498915', '134138990154579', '134138990243414', '134138986572183', '134138980030023', '134138966756537', '134138946544460', '134138945093449', '134138935549710', '134138926094467', '134138914316273', '134138877111635', '134138882282104', '134138853213812', '134138853689910', '134138846699164', '134138827914999', '134138826151537', '134138816497766', '134138797616275', '134138785490510', '134138751725740', '134138744450674', '134138735562509', '134138703938583', '134138649148176', '134138611370079', '134138609162348', '1341385872 26908', '134138587692787', '134138577464985', '134138538238011', '134138525113367', '134138503551909', '134138457187796', '134138420854501', '134138376531236', '134138369534192', '134138348387119', '134138306598399', '134138308162035', '134138284297634', '134138257782309', '134138257485521', '134138240518822', '134138145250924', '134138141394927', '134138137512256', '134138116239833', '134138109595273', '134138087316365', '134138098060742', '134138072475221', '134138093375530', '134138089875920', '134138087224238', '134138076734583', '134138077173128', '134138055124214', '134138016127969', '134138014195870', '134137978424721', '134137969070893', '134137956031485', '134137918333439', '134137910223252', '134137888568663', '134137891757925', '134137869191159', '134137860449252', '134137843430672', '134137823652864', '134137798568268', '134137728541605', '134137713476876', '134137706740544', '134137659820613', '134137658154911', '134137634519497', '134137617559660', '134137589219379', '134137571387420', '134137520652315', '134137509447063', '134137334443741', '134136740634591', '134136696857359', '134136683184898', '134136665993837', '134136661835564', '13413664 6577158', '134152878652310', '134152854530017', '134152768275746', '134152758986803', '134152756461997', '134152738336996', '134152713757649', '134152699933371', '134152680838174', '134152690684392', '134152636947444', '134152600416090', '134152571515548', '134152528462902', '134152510586893', '134152483774986', '134152468471775', '134152347116423', '134152267144858', '134152198965193', '134152175557620', '134152166439111', '134152147819449', '134152143330547', '134152114162631', '134152111734260', '134152026073407', '134152030538695', '134151968981387', '134151949311534', '134151943729534', '134151923067818', '134151910652355', '134151896588370', '134151902844735', '134151889159014', '134151879680017', '134151874899636', '134151826980255', '134151830135004', '134151808488621', '134151805383483', '134151735096403', '134151731976847', '134151679549007', '134151683531160', '134151672142660', '134151666530719', '134151651017729', '134151630731329', '134151602036037', '134151578813680', '134151556522207', '134151537817289', '13415150714 0926', '134151500472790', '134151480599610', '134151426763644', '134151367476281', '134151368818905', '134151355116774', '134151334728810', '134151332570350', '134151321315285', '134151312776874', '134151288649366', '134151267422071', '134151245416441', '134151234698486', '134151223157572', '134151212757993', '134151213898760', '134151205730803', '134151206016902', '134151176279815', '134151158387853', '134151156425182', '134151146171604', '134151138252378', '134151137333462', '134151101979187', '134151072052212', '134151036988388', '134151037612593', '134151008259624', '134151004881248', '134151005321839', '134150994862795', '134150960119161', '134150936874609', '134150914766363', '134150817035474', '134150804657150', '134150802982104', '134150801283294', '134150767987350', '134150755499758', '134150749671683', '134150742486634', '134150733738990', '134150717246316', '134150715323133', '134150715643662', '134150707321524', '134150686668994', '134150672426970', '134150599030300', '134150592495721', '134150595229390', '13415055915 8981', '134150539833172', '134150545449119', '134150529742217', '134150533154732', '134150532635295', '134150517454377', '134150513171568', '134150495943059', '134150482587755', '134150469387513', '134150465649303', '134150455515756', '134150453531307', '134150450491193', '134150443738036', '134150435169109', '134150362688935', '134150377654529', '134150368148285', '134150367853011', '134150346199692', '134150293696854', '134150303035456', '134150292191370', '134150285055861', '134150280919466', '134150283340080', '134150265877043', '134150233311136', '134150237524643', '134150207896079', '134150216491365', '134150170572254', '134150208173629', '134150148755662', '134150134392026', '134150120331197', '134150105118778', '134150085583063', '134150064197575', '134150059135884', '134150058679994', '134150053734963', '134150027771960', '134150000191238', '134149973142586', '134149939695581', '134149938449298', '134149929222092', '134149906365241', '134149914384614', '134149913947675', '134149905639295', '134149892753616', '13414988574 0939', '134149874997577', '134149874748008', '134149873249701', '134149871220921', '134149865283954', '134149859772423', '134149808047293', '134149795168359', '134149787470413', '134149787884441', '134149764116885', '134149740057269', '134149731186598', '134149730547987', '134149713810487', '134149704920194', '134149705633447', '134149670945443', '134149688389966', '134149639582258', '134149671839458', '134149670417214', '134149668410855', '134149668462125', '134149664393856', '134149652186120', '134149643527785', '134149641047024', '134149638275320', '134149614750518', '134149590787585', '134149589463962', '134149596046661', '134149592776892', '134149589838453', '134149560663796', '134149569031258', '134149567595127', '134149571822773', '134149548656061', '134149536970415', '134149510574953', '134149509332145', '134149490393896', '134149475495236', '134149479078798', '134149474312686', '134149463732038', '134149456635984', '134149431586331', '134149434881586', '134149428281452', '134149420587968', '134149344581271', '13414939425 3005', '134149393389232', '134149279464046', '134149364595091', '134149358497374', '134149175350532', '134149346728144', '134149343915196', '134149337961802', '134149310258906', '134149309642504', '134149298499581', '134149294867369', '134149292624032', '134149289474250', '134149285231560', '134149265033169', '134149266437757', '134149255875557', '134149248230457', '134149237582060', '134149234959855', '134149232960463', '134149202564817', '134149191095881', '134149171737560', '134149171351642', '134149149516218', '134149153131890', '134149127338780', '134149101659048', '134149103618185', '134149098099053', '134149086819915', '134149088183366', '134149084147113', '134149072433861', '134149058690639', '134149040670863', '134149017322154', '134148989421633', '134148987627487', '134148983983811', '134148984517055', '134148957772053', '134148968288620', '134148953596892', '134148941112217', '134148915211982', '134148919510225', '134148910927043', '134148897666131', '134148881445738', '134148886732993', '134148873575178', '13414886619 9713', '134148850046030', '134148830610726', '134148832072442', '134148784264793', '134148785043342', '134148783046829', '134148772775031', '134148743340152', '134148757159013', '134148734896727', '134148740832558', '134148718970802', '134148694933259', '134148685341695', '134148683145494', '134148665253780', '134148668877304', '134148661852260', '134148661731231', '134148646749777', '134148644367030', '134148638479953', '134148623759276', '134148621122872', '134148617976000', '134148616411779', '134148603217659', '134148600328694', '134148599116882', '134148585156613', '134148588412077', '134148582629599', '134148586676716', '134148583498525', '134148578995321', '134148569520841', '134148549798027', '134148558873496', '134148559962957', '134148558267521', '134148557568849', '134148538499116', '134148533753720', '134148528577298', '134148525134921', '134148510960746', '134148512139213', '134148495271356', '134148497039796', '134148489826331', '134148483264880', '134148469229150', '134148465220313', '134148439831120', '13414843975 2514', '134148430970117', '134148383451157', '134148431612082', '134148423194975', '134148415832803', '134148377024262', '134148405268696', '134148407282087', '134148393775087', '134148389771691', '134148388688333', '134148383090181', '134148370040719', '134148376574879', '134148365411758', '134148365792787', '134148357053453', '134148349265667', '134148344833324', '134148342336602', '134148338067656', '134148325727813', '134148313982946', '134148313469708', '134148309358723', '134148283897010', '134148293347791', '134148291670896', '134148290865704', '134148289024323', '134148283355252', '134148260126402', '134148274448062', '134148264270683', '134148267779576', '134148259294236', '134148259042963', '134148251495941', '134148228116781', '134148215586160', '134148216998649', '134148210634293', '134148194972170', '134148197581219', '134148194421948', '134148173631745', '134148153325386', '134148132442560', '134148144318761', '134148119426655', '134148117444999', '134148118846964', '134148111826830', '134148111119032', '13414808875 4457', '134148091512031', '134148080869524', '134148056892654', '134148054161261', '134148051170675', '134148045274505', '134148045837887', '134148037418258', '134148039440516', '134148030759108', '134148035114088', '134148028876644', '134148024421674', '134148021491043', '134148022616464', '134148017921111', '134148019560116', '134147979836630', '134148002814333', '134147999516110', '134147991786400', '134147978650294', '134147978350831', '134147979091901', '134147976010459', '134147959770816', '134147944375169', '134147938442440', '134147935227682', '134147935071576', '134147929032536', '134147924120435', '134147913023283', '134147901075519', '134147903346214', '134147896319171', '134147895111357', '134147882485113', '134147882827836', '134147874342461', '134147849978485', '134147840659866', '134147839673580', '134147832880725', '134147818940781', '134147804864808', '134147803940869', '134147796569901', '134147792887712', '134147782132650', '134147775028864', '134147775264743', '134147765093184', '134147754365722', '13414773943 5599', '134147738484403', '134147730069906', '134147715433628', '134147722829275', '134147712861877', '134147673321885', '134147691210442', '134147681145182', '134147667696382', '134147661783411', '134147654092522', '134147657725405', '134147655291721', '134147654029130', '134147652987031', '134147570881989', '134147630945333', '134147627581360', '134147606580635', '134147577314031', '134147570876990', '134147527166579', '134147534857271', '134147518269378', '134147515237244', '134147514677602', '134147496412237', '134147496237270', '134147487089718', '134147473114991', '134147458013213', '134147424599353', '134147414315586', '134147414214767', '134147382613112', '134147382511659', '134147379339177', '134147352947414', '134147356051318', '134147325445689', '134147334545438', '134147314960046', '134147313015446', '134147309595368', '134147309921462', '134147301048358', '134147298662727', '134147293541147', '134147286830376', '134147286928668', '134147276351328', '134147268455819', '134147262816352', '134147261669274', '13414725589 3889', '134147243839635', '134147221314591', '134147202140076', '134147203871625', '134147181689329', '134147175076626', '134147174633983', '134147175779146', '134147174051811', '134147166623977', '134147161151540', '134147151986191', '134147155421305', '134147153743345', '134147129765910', '134147119840902', '134147107436817', '134147095734810', '134147096036784', '134147084933578', '134147061038587', '134147046146776', '134147033150139', '134147026930444', '134147022773586', '134147012634679', '134147008863274', '134146994672751', '134146957093094', '134146972590494', '134146948340819', '134146928954816', '134146911569200', '134146905940331', '134146897813081', '134146855697285', '134146821164389', '134146763139335', '134146714638212', '134146656024027', '134146647032413', '134146641253663', '134146611984344', '134146548633689', '134146544549593', '134146542780197', '134146524852634', '134146513296546', '134146446530283', '134146376646018', '134146300787532', '134146263967141', '134146230242153', '134146155353306', '13414603353 5411', '134145968084114', '134145946239319', '134145923751442', '134145820497893', '134145802641000', '134145798643907', '134145742341177', '134145602782926', '134145262552223', '134145131828901', '134145108016514', '134144796769300', '134144793543106', '134144694738839', '134144695392557', '134144690262710', '134144665842491', '134144602681866', '134144602816940', '134144543099718', '134144532793483', '134144521436703', '134144489310528', '134144469182695', '134144473256034', '134144477348446', '134144410538427', '134144431826238', '134144331986619', '134144291980344', '134159439147746', '134159412155616', '134159386189384', '134159399497025', '134159378433795', '134159373980306', '134159365880514', '134159359398135', '134159339555105', '134159274988106', '134159252620077', '134159244846766', '134159215253439', '134159070873344', '134159024675767', '134159013930785', '134158996673378', '134158914393553', '134158977180717', '134158944532302', '134158878822288', '134158841970888', '134158827588737', '134158813459353', '134158787419282', '134158722424744', '134158653275138', '134158641127828', '134158625293168', '134158607288555', '134158572074728', '134158553745922', '134158527283838', '134158510422946', '134158471642787', '134158452950287', '134158460268505', '134158454388436', '134158442011143', '134158437434845', '134158370718561', '134158353981058', '134158326769479', '134158290575175', '134158281713154', '134158247469321', '134158247736727', '134158232331313', '134158155635555', '134158141867437', '134158147579680', '134158151978949', '134158134363368', '134158087919176', '13415805096 0336', '134158029855883', '134157936170815', '134158016652265', '134158012294658', '134157953190640', '134157936763056', '134157926069750', '134157896426739', '134157894312500', '134157807287419', '134157800892335', '134157781135638', '134157780517279', '134157774271782', '134157765924690', '134157765315112', '134157661645087', '134157712536595', '134157690997467', '134157678555613', '134157677315905', '134157680781149', '134157672743976', '134157572491452', '134157556042833', '134157553296225', '134157552478344', '134157446515017', '134157518667319', '134157522016500', '134157501869565', '134157497987082', '134157454090496', '134157474146475', '134157441799746', '134157461644482', '134157444731293', '134157437478205', '134157426444332', '134157397710189', '134157408693357', '134157405070039', '134157409490148', '134157278547931', '134157252727707', '134157261136139', '134157253766300', '134157238831856', '134157209784539', '134157202197714', '134157182631839', '134157146132609', '134157152383694', '134157146718750', '13415715239 9490', '134157123845041', '134157108819002', '134157105763973', '134157100541361', '134157065439665', '134157087084911', '134157077943531', '134157073214091', '134157071543838', '134157063523317', '134157052044802', '134157055276210', '134157058589557', '134157040934345', '134157026460721', '134157014590149', '134157013421831', '134156987957586', '134156979024208', '134156973489535', '134156967718696', '134156945084328', '134156937722633', '134156930895196', '134156929267743', '134156908378520', '134156903374086', '134156880249521', '134156859859325', '134156850475256', '134156853598204', '134156848515178', '134156847812647', '134156831160752', '134156805657532', '134156765112139', '134156738178431', '134156756836545', '134156745025924', '134156749022694', '134156746630155', '134156715859091', '134156724280129', '134156709532823', '134156668167363', '134156653831717', '134156642038681', '134156628591096', '134156595799103', '134156593060189', '134156580339625', '134156559968559', '134156567911093', '134156563868653', '13415653339 5837', '134156539622522', '134156542239732', '134156509727492', '134156525567328', '134156514457156', '134156517661533', '134156505344996', '134156496574714', '134156485348607', '134156478220780', '134156467958062', '134156459310191', '134156446590147', '134156435215325', '134156425358763', '134156405571325', '134156394868896', '134156375517188', '134156359687781', '134156323852974', '134156304657925', '134156295375052', '134156274533977', '134156268410055', '134156258654399', '134156249270807', '134156252075342', '134156251672084', '134156245233527', '134156229166778', '134156210041109', '134156181931762', '134156160298507', '134156165646337', '134156126665342', '134156105644227', '134156103754436', '134156041886647', '134156043287096', '134156030777108', '134155987571890', '134155963077953', '134155921352614', '134155875078073', '134155878416493', '134155837521004', '134155800989793', '134155789947804', '134155771289422', '134155672633840', '134155651613877', '134155571635650', '134155472840194', '134155379586267', '13415536136 4648', '134155320475832', '134155312146546', '134155304458476', '134155276719115', '134155174496621', '134155157927642', '134155134222150', '134155128089971', '134155095253846', '134154906583794', '134154867159695', '134154836664861', '134154834015576', '134154692191862', '134154644332240', '134154530489419', '134154471196344', '134154449762469', '134154443317663', '134154427777805', '134154264521987', '134154229767983', '134153991782959', '134153987735778', '134153952196855', '134153948326400', '134153917739955', '134153667768348', '134153656988637', '134153637465266', '134153619010711', '134153494660654', '134153448856097', '134153441846108', '134153300413390', '134153248862781', '134153102798694', '134153033520281', '134153024394409', '134153027014299', '134152979972637', '134152963844408');
//
// $temp = array();
// foreach($totalPaidArray AS $paidOrder){
//     if(in_array($paidOrder,$duplicateArray)){
//        $temp[] = $paidOrder;
//     }
// }

// echo "<pre>";
// print_r($temp);
//
//
//
// $duplicateArrayYes = array('134144073396884', '134144032046828', '134143916322531',  '134143912011399', '134143846465705', '134143584772936', '134143542923169', '134143532476900', '134143404466584', '134143303115988', '134143211116904', '134143191363681', '134143161848012', '134143113671002', '134143057675978', '134142960186112', '134142926570822', '134142896554767', '134142703615297', '134142708151455', '134142674253288', '134142611199335', '134142475245336', '134142423999143', '134142350335483', '134142296489337', '134142271476912', '134142143012037', '134142135646348', '134142121064320', '134142120297578', '134142114837363', '134142097213494', '134142064213754', '134142056766978', '134142011814466', '134141998768748', '134141986443251', '134141967280046', '134141967292705', '134141922956590', '134141912835433', '134141908049431', '134141880498910', '134141877871019', '134141868544229', '134141852575767', '134141849657638', '134141826920981', '134141784478099', '134141787971932', '134141579252554', '134141727932961', '134141749196039', '134141746764716', '134141725582937', '134141708725788', '134141710962221', '134141703939488', '134141663432645', '134141652195391', '134141644370890', '134141602094342', '134141555497913', '134141552626361', '134141544035801', '134141529241499', '134141498442197', '134141457530034', '134141454372274', '134141410331226', '134141406998510', '134141401021303', '134141348018682', '134141331236653', '134141329210780', '134141309364407', '134141302125488', '134141278750706', '134141270771992', '134141262470474', '134141209053166', '134141170114456', '134141160749214', '134141146481495', '134141124345233', '134141136813170', '134141121081354', '134141113053352', '134141104430005', '134141085816048', '134141081472845', '134141065242690', '134141031043761', '134141051586460', '134141042869237', '134141025372938', '134141023856351', '134140998013224', '134140951343913', '134140945457282', '134140934915782', '134140909014180', '134140914539542', '134140897682200', '134140879111187', '134140872352087', '134140843655769', '134140834594819', '134140806156380', '134140802391301', '134140784782447', '134140773421312', '134140773116726', '134140765146492', '134140767865161', '134140751212646', '134140732645984', '134140667464432', '134140717722767', '134140699630905', '134140691860700', '134140685721599', '134140673988246', '134140605733686', '134140649157693', '134140624773276', '134140577255987', '134140566414325', '134140566381939', '134140532414893', '134140523582502', '134140482638306', '134140500226624', '134140477871393', '134140479310779', '134140474988721', '134140465258132', '134140459963262', '134140460352704', ' 134140439960932', '134140439083144', '134140416493436', '134140407744419', '134140403797614', '134140398531997', '134140398177141', '134140384827441', '134140376614960', '134140364743104', '134140353285958', '134140350592544', '134140301362544', '134140319026494', '134140304095588', '134140287259404', '134140253168308', '134140237849678', '134140205653399', '134140187238238', '134140186047322', '134140179248724', '134140166141147', '134140160653016', '134140153467660', '134140154884810', '134140138017068', '134140089643188', '134140096560977', '134140093783861', '134140068995951', '134140049276110', '134140019421118', '134140008417331', '134140020351534', '134139990982412', '134139977535299', '134139962841197', '134139952261754', '134139951480572', '134139927683520', '134139920448051', '134139919749634', '134139881161973', '134139868218508', '134139865991183', '134139844819881', '134139824636439', '134139833327283', '134139819661661', '134139767888868', '134139799669255', '134139783789342', '134139769713725', '134139749527240', '134139695119662', '134139690598988', '134139653651585', '134139674825527', '134139623065777', '134139607574753', '134139604060569', '134139578567687', '134139567546856', '134139528283193', '134139496753227', '134139490523934', '134139465269405', '134139447696980', '134139438549478', '134139436957850', '134139348893953', '134139342379216', '134139301540234', '134139271518707', '134139218317250', '134139194540328', '134139149279738', '134139089786060', '134139091373534', '134139057931038', '134139063442637', '134139051788349', '134139037021951', '134139035272817', '134139017881812', '134139014176373', '134138990154579', '134138986572183', '134138966756537', '134138946544460', '134138926094467', '134138914316273', '134138853213812', '134138853689910', '134138846699164', '134138826151537', '134138816497766', '134138751725740', '134138735562509', '134138703938583', '134138649148176', '134138611370079', '134138609162348', '134138577464985', '134138538238011', '134138525113367', '134138503551909', '134138457187796', '134138369534192', '134138306598399', '134138284297634', '134138257782309', '134138257485521', '134138240518822', '134138145250924', '134138141394927', '134138137512256', '134138116239833', '134138072475221', '134138093375530', '134138089875920', '134138087224238', '134138055124214', '134137969070893', '134137956031485', '134137918333439', '134137910223252', '134137888568663', '134137869191159', '134137843430672', '134137823652864', '134137798568268', '134137728541605', '134137713476876', '134137706740544', '134137634519497', '134137589219379', '134137571387420', '134137520652315', '134137509447063', '134136740634591', '134136665993837', '134152854530017', '134152758986803', '134152738336996', '134152713757649', '134152680838174', '134152690684392', '134152600416090', '134152571515548', '134152483774986', '134152175557620', '134152111734260', '134151968981387', '134151923067818', '134151879680017', '134151826980255', '134151830135004', '134151808488621', '134151679549007', '134151630731329', '134151602036037', '134151578813680', '134151556522207', '134151537817289', '134151480599610', '134151426763644', '134151367476281', '134151334728810', '134151267422071', '134151223157572', '134151213898760', '134151205730803', '134151206016902', '134151146171604', '134151138252378', '134151072052212', '134151037612593', '134151008259624', '134151004881248', '134150960119161', '134150817035474', '134150767987350', '134150749671683', '134150715643662', '134150707321524', '134150672426970', '134150539833172', '134150533154732', '134150517454377', '134150513171568', '134150495943059', '134150482587755', '134150469387513', '134150465649303', '134150455515756', '134150450491193', '134150435169109', '134150362688935', '134150346199692', '134150293696854', '134150283340080', '134150265877043', '134150233311136', '134150207896079', '134150216491365', '134150170572254', '134150148755662', '134150134392026', '134150120331197', '134150053734963', '134150027771960', '134150000191238', '134149973142586', '134149938449298', '134149906365241', '134149914384614', '134149913947675', '134149905639295', '134149892753616', '134149874997577', '134149874748008', '134149873249701', '134149871220921', '134149859772423', '134149795168359', '134149787884441', '134149713810487', '134149705633447', '134149670945443', '134149688389966', '134149639582258', '134149671839458', '134149668410855', '134149668462125', '134149664393856', '134149641047024', '134149638275320', '134149614750518', '134149596046661', '134149592776892', '134149567595127', '134149548656061', '134149536970415', '134149510574953', '134149490393896', '134149475495236', '134149456635984', '134149431586331', '134149434881586', '134149420587968', '134149393389232', '134149364595091', '134149175350532', '134149343915196', '134149337961802', '134149310258906', '134149309642504', '134149294867369', '134149292624032', '134149289474250', '134149285231560', '134149237582060', '134149232960463', '134149191095881', '134149171737560', '134149149516218', '134149101659048', '134149088183366', '134149084147113', '134149040670863', '134148987627487', '134148984517055', '134148968288620', '134148941112217', '134148915211982', '134148910927043', '134148886732993', '134148873575178', '134148850046030', '134148830610726', '134148832072442', '134148784264793', '134148785043342', '134148772775031', '134148743340152', '134148757159013', '134148694933259', '134148685341695', '134148665253780', '134148668877304', '134148661852260', '134148661731231', '134148644367030', '134148638479953', '134148621122872', '134148616411779', '134148600328694', '134148585156613', '134148588412077', '134148586676716', '134148583498525', '134148569520841', '134148549798027', '134148558873496', '134148559962957', '134148558267521', '134148557568849', '134148538499116', '134148533753720', '134148528577298', '134148525134921', '134148510960746', '134148512139213', '134148497039796', '134148469229150', '134148430970117', '134148423194975', '134148377024262', '134148405268696', '134148393775087', '134148388688333', '134148383090181', '134148370040719', '134148365792787', '134148349265667', '134148344833324', '134148342336602', '134148338067656', '134148325727813', '134148313982946', '134148313469708', '134148309358723', '134148283897010', '134148293347791', '134148290865704', '134148289024323', '134148260126402', '134148274448062', '134148264270683', '134148267779576', '134148259294236', '134148259042963', '134148251495941', '134148228116781', '134148215586160', '134148216998649', '134148210634293', '134148194972170', '134148197581219', '134148194421948', '134148173631745', '134148153325386', '134148132442560', '134148144318761', '134148119426655', '134148117444999', '134148118846964', '134148111826830', '134148111119032', '134148091512031', '134148080869524', '134148051170675', '134148037418258', '134148039440516', '134148030759108', '134148035114088', '134148028876644', '134148024421674', '134148022616464', '134148017921111', '134148019560116', '134147999516110', '134147991786400', '134147978650294', '134147979091901', '134147976010459', '134147959770816', '134147944375169', '134147935227682', '134147924120435', '134147913023283', '134147901075519', '134147903346214', '134147896319171', '134147895111357', '134147882485113', '134147882827836', '134147874342461', '134147849978485', '134147839673580', '134147832880725', '134147804864808', '134147803940869', '134147796569901', '134147792887712', '134147782132650', '134147775264743', '134147765093184', '134147754365722', '134147738484403', '134147730069906', '134147715433628', '134147722829275', '134147712861877', '134147691210442', '134147681145182', '134147661783411', '134147654092522', '134147655291721', '134147654029130', '134147652987031', '134147627581360', '134147606580635', '134147570876990', '134147534857271', '134147518269378', '134147515237244', '134147514677602', '134147496412237', '134147496237270', '134147487089718', '134147473114991', '134147458013213', '134147424599353', '134147414315586', '134147414214767', '134147382613112', '134147382511659', '134147352947414', '134147325445689', '134147314960046', '134147309595368', '134147301048358', '134147298662727', '134147286928668', '134147276351328', '134147268455819', '134147262816352', '134147261669274', '134147243839635', '134147203871625', '134147181689329', '134147175076626', '134147174633983', '134147175779146', '134147174051811', '134147166623977', '134147161151540', '134147151986191', '134147155421305', '134147153743345', '134147129765910', '134147119840902', '134147107436817', '134147095734810', '134147096036784', '134147084933578', '134147061038587', '134147046146776', '134147033150139', '134147026930444', '134147022773586', '134147012634679', '134147008863274', '134146957093094', '134146928954816', '134146911569200', '134146905940331', '134146897813081', '134146855697285', '134146763139335', '134146714638212', '134146656024027', '134146647032413', '134146641253663', '134146611984344', '134146548633689', '134146544549593', '134146542780197', '134146524852634', '134146513296546', '134146376646018', '134146300787532', '134146263967141', '134146230242153', '134145968084114', '134145946239319', '134145802641000', '134145742341177', '134145602782926', '134145131828901', '134145108016514', '134144796769300', '134144793543106', '134144694738839', '134144690262710', '134144665842491', '134144602681866', '134144543099718', '134144532793483', '134144521436703', '134144489310528', '134144469182695', '134144473256034', '134144477348446', '134144410538427', '134144431826238', '134144331986619', '134144291980344', '134159386189384', '134159378433795', '134159373980306', '134159274988106', '134159215253439', '134159070873344', '134158914393553', '134158944532302', '134158827588737', '134158787419282', '134158553745922', '134158527283838', '134158510422946', '134158460268505', '134158353981058', '134158281713154', '134158247736727', '134158151978949', '134158134363368', '134158087919176', '134157936170815', '134158016652265', '134157936763056', '134157894312500', '134157780517279', '134157765315112', '134157678555613', '134157672743976', '134157556042833', '134157553296225', '134157522016500', '134157497987082', '134157474146475', '134157441799746', '134157461644482', '134157444731293', '134157426444332', '134157408693357', '134157409490148', '134157278547931', '134157261136139', '134157253766300', '134157238831856', '134157152383694', '134157146718750', '134157123845041', '134157108819002', '134157105763973', '134157087084911', '134157073214091', '134157071543838', '134157063523317', '134157058589557', '134157040934345', '134157026460721', '134157013421831', '134156987957586', '134156979024208', '134156973489535', '134156967718696', '134156945084328', '134156937722633', '134156930895196', '134156929267743', '134156908378520', '134156903374086', '134156880249521', '134156850475256', '134156853598204', '134156848515178', '134156847812647', '134156831160752', '134156738178431', '134156756836545', '134156745025924', '134156749022694', '134156715859091', '134156724280129', '134156709532823', '134156668167363', '134156628591096', '134156595799103', '134156593060189', '134156580339625', '134156563868653', '134156539622522', '134156542239732', '134156525567328', '134156514457156', '134156517661533', '134156505344996', '134156485348607', '134156478220780', '134156467958062', '134156459310191', '134156425358763', '134156405571325', '134156394868896', '134156375517188', '134156323852974', '134156304657925', '134156295375052', '134156274533977', '134156268410055', '134156258654399', '134156249270807', '134156252075342', '134156251672084', '134156245233527', '134156181931762', '134156160298507', '134156126665342', '134156105644227', '134156041886647', '134156043287096', '134156030777108', '134155987571890', '134155963077953', '134155921352614', '134155875078073', '134155837521004', '134155789947804', '134155771289422', '134155651613877', '134155472840194', '134155320475832', '134155312146546', '134155304458476', '134155174496621', '134155157927642', '134155134222150', '134155095253846', '134154906583794', '134154867159695', '134154834015576', '134154644332240', '134154471196344', '134154449762469', '134154427777805', '134154229767983', '134153991782959', '134153952196855', '134153917739955', '134153667768348', '134153656988637', '134153637465266', '134153448856097', '134153248862781', '134153033520281', '134153024394409', '134152979972637', '134152963844408');
// echo count($duplicateArrayYes);
//
//
// $a = array_diff($temp, $duplicateArrayYes);
// sort($a);
//
// echo "<pre>";
// print_r($a);
// exit;
// 

// $duplicateArray = array_unique($duplicateArray);
// echo "<pre>";
// print_r($duplicateArray);
// exit;






     $orderArrCount = count($orderArr);
     $newAppIdArray = array();

     $duplicateOrders = array();

     
     $paidArray = array();
     $unPaidArray = array();

     for($i=0;$i<$orderArrCount;$i++){

         $ipay4meOrder = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderArr[$i])->toArray();         

         if(count($ipay4meOrder) > 0) {
             
            $origRequestId = $ipay4meOrder[0]['order_request_detail_id'];
            $transService = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($origRequestId)->toArray();
            $transServiceCount = count($transService);
            
            $tempArray = array();
            for($j=0; $j<$transServiceCount; $j++){
                $tempArray[]  = $transService[$j]['app_id'];
                $transServiceObj = '';
                $transServiceObj = Doctrine::getTable('TransactionServiceCharges')->findByAppId($transService[$j]['app_id']);
                if(count($transServiceObj) > 1){                    
                    foreach($transServiceObj AS $transObj){                        
                        $ipay4meOrderObj = Doctrine::getTable('Ipay4meOrder')->findByOrderRequestDetailId($transObj->getOrderRequestDetailId());                        
                        if(count($ipay4meOrderObj) > 0){
                            foreach($ipay4meOrderObj AS $ipay4meObj){
                                if($orderArr[$i] != $ipay4meObj->getOrderNumber()){
                                    $duplicateOrders[] = $ipay4meObj->getOrderNumber();
                                }
                            }
                        }                        
                    }
                }
            }
         }     
     }

     $duplicateOrders = array_unique($duplicateOrders);

     echo "<pre>";print_r($duplicateOrders);die;
}

public function executeExcelReader(sfWebRequest $request){

    $filePath = sfConfig::get('sf_upload_dir').'/excel.xls';
    $data = new sfExcelReader($filePath);
    echo $data->dump(true, true);
    die("A");

}
    



    
}
