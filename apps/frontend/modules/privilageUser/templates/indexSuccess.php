<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
    <div class="content_container">
        <?php use_helper('Pagination');
        use_helper('Form');
        use_javascript('dhtmlwindow.js');
        use_stylesheet('dhtmlwindow.css');
        use_javascript('modal.js');
        use_stylesheet('modal.css');
        include_partial('global/innerHeading',array('heading'=>'Privilege User'));
        $sessionMsg = sfContext::getInstance()->getUser()->getAttribute('sessMsg');
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('sessMsg');
        ?>
        <div id="flash_notice" class="alertBox" style="display:<?php echo ($sessionMsg!='')?'':'none'?>"><?php echo $sessionMsg;?></div>
        <div class="clear"></div>
        <div class="tmz-spacer"></div>
        <div id="nxtPage"> <?php echo form_tag('privilageUser/index',array('name'=>'order_search_form','class'=>'', 'method'=>'post','id'=>'order_search_form', 'onsubmit' => 'return frmValidate()')) ?>
            <?php
            $sf = sfContext::getInstance()->getUser();
            if($sf->hasFlash('notice')){ ?>
            <div id="flash_notice" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('notice'));
                ?>
            </div>
            <br/>
            <?php }?>
        <?php
        $sf = sfContext::getInstance()->getUser();
        if($sf->hasFlash('error')){ ?>
            <div id="flash_error" class="alertBox" >
                <?php
                echo nl2br($sf->getFlash('error'));
                ?>
            </div>
            <br/>
            <?php }?>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <?php
                $fname = (isset($_REQUEST['fname'])) ? $_REQUEST['fname'] : sfContext::getInstance()->getUser()->getAttribute('fname');
                $lname = (isset($_REQUEST['lname'])) ? $_REQUEST['lname'] : sfContext::getInstance()->getUser()->getAttribute('lname');
                $email = (isset($_REQUEST['email'])) ? $_REQUEST['email'] : sfContext::getInstance()->getUser()->getAttribute('email');
                $status = (isset($_REQUEST['status'])) ? $_REQUEST['status'] : sfContext::getInstance()->getUser()->getAttribute('status');
                $userList = (isset($_REQUEST['userList'])) ? $_REQUEST['userList'] : sfContext::getInstance()->getUser()->getAttribute('userList');

                $statusArray = array('all'=>'All','active'=>'Active','Inactive'=>'InActive');

                echo formRowComplete('Privilege Status',select_tag('status',options_for_select($statusArray,$status,array('class'=>'txt-input'))));
                echo "</tr>";
                echo formRowComplete('First Name',input_tag('fname',$fname , array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                echo "</tr>";
                echo formRowComplete('Last Name',input_tag('lname', $lname, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                echo "</tr>";
                echo formRowComplete('Email',input_tag('email', $email, array('size' => 27, 'maxlength' => 50,'class'=>'txt-input')));
                echo "</tr>";
                //echo formRowComplete('Email',radiobutton_tag('email', $email, '', array('size' => 27, 'maxlength' => 50)));
                ?>
                <tr>
                    <td class="ltgray">User List</td>
                    <td>
                        <input type="radio" class="txt-input" name="userList" id="userList" value="pu" checked="checked">Privilege User&nbsp;&nbsp;&nbsp;
                        <input type="radio" class="txt-input" name="userList" id="userList" value="npu" <?php if($userList == 'npu'){ echo "checked"; } else { echo ""; } ?> >Non-Privilege User
                    </td>
                </tr>
                <?php

                echo "</tr>";

                ?>
                <tr valign="top" >
                    <td height="30" valign="top" style="border-right-style:none; vertical-align:top;">&nbsp;</td>
                    <td height="30" valign="top" style="border-left-style:none; vertical-align:top;"><div class="lblButton"> <?php echo submit_tag('Search',array('class' => 'normalbutton')); ?> </div>
                        <div class="lblButtonRight">
                            <div class="btnRtCorner"></div>
                    </div></td>
                </tr>
            </table>
            </form>
            <div class="clear"></div>
            <div class="tmz-spacer"></div><div class="scroll_div">
                <table width="100%">
                    <tr>
                        <td class="blbar" colspan="11" align="right"><span>Showing <b><?php echo $pager->getFirstIndice() ?></b> - <b><?php echo $pager->getLastIndice() ?></b> of total <b><?php echo $pager->getNbResults(); ?></b> results</span></td>
                    </tr>
                    <tr>
                        <td width="10%"><span class="txtBold">S. No.</span></td>
                        <td width="20%"><span class="txtBold">First Name</span></td>
                        <td width="15%"><span class="txtBold">Last Name</span></td>
                        <td width="15%"><span class="txtBold">Email</span></td>
                        <td width="15%"><span class="txtBold">Mobile Number</span></td>
                        <td width="10%"><span class="txtBold">Mid Service</span></td>
                        <td width="10%"><span class="txtBold">Card Type</span></td>
                        <td width="15%"><span class="txtBold">Status</span></td>
                        <td width="15%"><span class="txtBold">Last Login</span></td>
                        <td width="15%"><span class="txtBold">Login Type</span></td>
                        <td width="15%"><span class="txtBold">Action</span></td>
                    </tr>
                    <?php

                    //if(count($arrActiveUserId)){
                    if(($pager->getNbResults())>0) {
                        $limit = sfConfig::get('app_records_per_page');
                        $page = $sf_context->getRequest()->getParameter('page',0);
                        $i = max(($page-1),0)*$limit ;
                        ?>
                        <?php
                        foreach ($pager->getResults() as $result):

                        //echo "<pre>";print_r($result->toArray());echo "</pre>"; die;



                        $i++;
                        ## Fetching login type...
                        $userType = Settings::getLoginType($result->sfGuardUser->getUsername());
                        ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td ><span>
                                <?php if($result->sfGuardUser->UserDetail->getFirstName() !='') {echo $result->sfGuardUser->UserDetail->getFirstName();}else{echo "--";}?>
                        </span></td>
                        <td><span >
                                <?php if($result->sfGuardUser->UserDetail->getLastName() !=''){echo $result->sfGuardUser->UserDetail->getLastName();}else{echo "--";}?>
                        </span></td>
                        <td ><span >
                                <?php if($result->sfGuardUser->UserDetail->getEmail() !=''){echo $result->sfGuardUser->UserDetail->getEmail();}else{echo "--";}?>
                        </span></td>
                        <td><span >
                                <?php if($result->sfGuardUser->UserDetail->getMobilePhone() !=''){echo $result->sfGuardUser->UserDetail->getMobilePhone();}else{echo "--";}?>
                        </span></td>
                        <td><span ><?php
                                $card_type  = '--';
                                $userStatus = '--';
                                if ($result->sfGuardUser->UserDetail->getUserId() != '') {

                                    $midService = Doctrine_Core::getTable('PaymentPrivilageUser')->findByUserId($result->sfGuardUser->UserDetail->getUserId());
                                    if(count($midService)){
                                        if ($midService[0]['mid_service']) {
                                            echo $midService[0]['mid_service'];
                                        } else {
                                            echo "--";
                                        }


                                        if ($midService[0]['card_type']) {
                                            $card_type = $midService[0]['card_type'];
                                        }

                                        if (isset($midService[0]['status']) && $midService[0]['status'] != '') {
                                            $userStatus = $midService[0]['status'];
                                        }
                                    }else{
                                        echo "--";
                                    }


                                } ?>
                        </span></td>
                        <td><span ><?php
                                echo $card_type
                                ?>
                        </span></td>
                        <td>
                            <span>
                                <?php

                                echo ucfirst($userStatus);
                                ?>
                            </span>
                        </td>
                        <td><span >
                                <?php if($result->sfGuardUser->getLastLogin() !=''){echo $result->sfGuardUser->getLastLogin();}else{echo "--";}?>
                        </span></td>
                        <td><span >
                                <?php $priviligeUser = Doctrine_Core::getTable('PaymentPrivilageUser')->findByUserId($result->sfGuardUser->UserDetail->getUserId());
                                ?>
                                <?php if($result->sfGuardUser->getUsername() !=''){echo $userType;}else{echo "--";}?>
                        </span></td>
                        <td id="edit_<?php echo $result['user_id']; ?>">
                            <?php if($priviligeUser[0]['id']!='') { ?><span><a  href="javascript:void(0);" onclick="addPriviligeUserValue('edit','<?php echo $priviligeUser[0]['id']; ?>','<?php echo $page;?>');">Edit</a></span>
                                <?php } else { ?><span><a  href="javascript:void(0);" onclick="addPriviligeUserValue('add','<?php echo $result['user_id']; ?>','<?php echo $page;?>');">Assign</a></span>
                                <?php } ?>
                        </td>

                    </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td class="blbar" colspan="11" height="25px" align="right"><?php echo pager_navigation($pager, url_for($sf_context->getModuleName().'/'.$sf_context->getActionName()).'?fname='.$fname.'&lname='.$lname.'&email='.$email.'&status='.$status.'&usePager=true'.'&back=1') ?></td>
                    </tr>
                    <?php
                }else { ?>
                    <tr>
                        <td  colspan=11" align='center' ><div style="color:red"> No record found</div></td>
                    </tr>
                    <?php } ?>
            </table></div>
        </div>
</div></div><div class="content_wrapper_bottom"></div>
<script>
    function addPriviligeUserValue(mode,id,page){
        var url = "<?php echo url_for('privilageUser/addPriviligeUserValue?totalRecords='.$pager->getNbResults()) ?>";
        if(id !=''){
            url = url + "/id/"+ id + "/mode/" + mode + "/page/" + page;
        }else {
            url = url + "/mode/" + mode + "/page/" + page;
        }
        emailwindow = dhtmlmodal.open('EmailBox', 'iframe', url ,'Privilige User Value', ' width=700,height=235,center=1,border=0, scrolling=no')

    }
    function activateDeactivate(id,status,page){

        var  showStatus;
        if(status == 'active'){
            showStatus = 'active';
        }
        if(status == 'Inactive'){
            showStatus = 'inactive';

        }
        if(confirm("Do you want to "+showStatus+" User ?")){
            var url = "<?php echo url_for('privilageUser/setStatus')?>";
            window.location = url+'?id='+id+'&status='+status+'&page='+page;

        }else{
            return false;
        }
        //
    }

    function frmValidate(){

        var radioVal = $("input[type='radio'][name='userList']:checked").val();
        if(radioVal == 'npu'){

            var fname = $.trim($('#fname').val());
            var lname = $.trim($('#lname').val());
            var email = $.trim($('#email').val());

            if(fname == '' && lname == '' && email == ''){
                alert("Please input data to search in non-privilege user list.")
                return false;
            }
        }
    }

</script>