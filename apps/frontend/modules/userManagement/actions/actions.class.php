<?php

/**
 * userManagement actions.
 *
 * @package    ama
 * @subpackage userManagement
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class userManagementActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
    public function executeIndex(sfWebRequest $request)
    {
        $this->userList = array();
        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_report_user')); //get Group Id; of bank admin
        $groupid = $sfGroupDetails->getFirst()->getId();



        $this->userName = $request->getParameter('user_search');
        $this->user_list = Doctrine::getTable('sfGuardUser')->getReportUsers($groupid);
        //        $res = $this->user_list->execute();
        //        echo "<pre>";
        //        print_r($res->toArray());die;
        $this->page = 1;
        if($request->hasParameter('page')) {
            $this->page = $request->getParameter('page');
        }
        $this->pager = new sfDoctrinePager('sfGuardUser',sfConfig::get('app_records_per_page'));
        $this->pager->setQuery($this->user_list);
        $this->pager->setPage($this->page);
        $this->pager->init();
    }
    public function executeSetStatus(sfWebRequest $request)
    {
        $userId = $request->getParameter('id');
        $status = $request->getParameter('status');
        $sfUid  = $request->getParameter('sfUid');
        if($sfUid!='') {


            $user = Doctrine::getTable('UserDetail')->find(array($userId));;//print_r($user);
            $sfGuardUserDetail = Doctrine::getTable('sfGuardUser')->find(array($sfUid));
            if($status == "activate") {

                $user->setFailedAttempt(0);
                $user->save();
                $sfGuardUserDetail->setIsActive(true);
                $sfGuardUserDetail->setLastLogin (NULL);
                $sfGuardUserDetail->save();
                $this->getUser()->setFlash('notice', 'User unblocked successfully.');
            }
            if($status == "deactivate") {
                $user->setFailedAttempt(sfConfig::get('app_number_of_failed_attempts_for_blocked_user'));
                $user->save();
                $sfGuardUserDetail->setIsActive(false);
                $sfGuardUserDetail->save();
                $this->getUser()->setFlash('notice', 'User blocked successfully.');
            }
        }
        $this->forward('userManagement','index');

    }
    public function executeNew(sfWebRequest $request)
    {
        $this->form = new reportUserForm();
        $postdataArray = array();
        $this->setTemplate('new');
        if($request->isMethod('post'))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            if ($this->form->isValid())
            {
                $postdataArray['username'] = $this->form->getValue('username');
                $postdataArray['name'] = $this->form->getValue('name');
                $postdataArray['dob'] = $this->form->getValue('dob');
                $postdataArray['address'] = $this->form->getValue('address');
                $postdataArray['email'] = $this->form->getValue('email');
                $postdataArray['phone'] = $this->form->getValue('phone');
                $this->saveReportUser($postdataArray);
            }
        }
    }
    public function executeEdit(sfWebRequest $request) {
        $this->setTemplate('edit');
        $this->setLayout(false);
        $this->supportUserFlag = $request->getParameter('supportUserFlag');
        $this->page = $request->getParameter('page');
        $this->method="edit";
        $this->form = new reportUserForm('',array('action'=>'edit','supportUserFlag'=>$this->supportUserFlag));
        $this->forward404Unless($sf_guard_user = Doctrine::getTable('sfGuardUser')->find($request->getParameter('user_id')), sprintf('Object report user does not exist (%s).', $request->getParameter('user_id')));
        if($sf_guard_user->getUserDetail()=='') {
            $this->first_name = '';
            $this->last_name = '';
            $this->dob = '';
            $this->address = '';
            $this->email = '';
            $this->phone = '';
            $this->userId = '';
        }
        else {
            $this->first_name = $sf_guard_user->getUserDetail()->getFirstName();
            $this->last_name = $sf_guard_user->getUserDetail()->getLastName();
            $this->dob = $sf_guard_user->getUserDetail()->getDob();
            $this->address = $sf_guard_user->getUserDetail()->getAddress();
            $this->phone = $sf_guard_user->getUserDetail()->getMobilePhone();
            $this->email = $sf_guard_user->getUserDetail()->getEmail();
            $this->userId = $sf_guard_user->getUserDetail()->getUserId();
        }
//        $this->form->setDefault('username',  $this->username);
        $this->form->setDefault('first_name',  $this->first_name);
        $this->form->setDefault('last_name',  $this->last_name);
        $this->form->setDefault('dob', $this->dob);
        $this->form->setDefault('address',  $this->address);
        $this->form->setDefault('email',  $this->email);
        $this->form->setDefault('phone',  $this->phone);
        $this->form->setDefault('user_id',  $this->userId);
    }
    public function executeModify(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post'));
        $this->supportUserFlag = $request->getParameter('supportUserFlag');
        $this->form = new reportUserForm('',array('action'=>'edit','supportUserFlag'=>$this->supportUserFlag));
        $this->processForm($request, $this->form);
        $this->setTemplate('edit');
    }
    protected function processForm(sfWebRequest $request, $form)
    {
        $param = $request->getParameter('report_user');
        $page = $request->getParameter('page');
        $form->bind($param);
        if ($form->isValid())
        {
            $user_id =$param['user_id'];
            $first_name = $param['first_name'];
            $last_name = $param['last_name'];
            $address =$param['address'];
            $phone = $param['phone'];
            if((!empty($phone) && $phone != '')){
            if($phone[0] != '+'){
                $phone = '+'.$phone;
            }
            }
            $dob = $param['dob'];
            $email =$param['email'];            

            $userDetailObj = Doctrine::getTable('UserDetail')->findByUserId($user_id);
            $userDetailId = "";
            if($userDetailObj->count()) {
               $userDetailId = $userDetailObj->getFirst()->getId();
            }
            if($userDetailId != "") {
                $user_detail = Doctrine::getTable('UserDetail')->find($userDetailId);
            }
            else {
                $user_detail = new UserDetail();
            }

            $user_detail->setFirstName(trim($first_name));
            $user_detail->setLastName(trim($last_name));
            if(($dob) && ($dob!="")) {
                $user_detail->setDob(date('Y-m-d', strtotime(trim($dob))));
            }
            $user_detail->setAddress(trim($address));
            $user_detail->setMobilePhone(trim($phone));
            $user_detail->setEmail(trim($email));
            $user_detail->save();
            ?>
                <script>
                    page = '<?php echo $page; ?>';
                    alertMsg = 'User Profile Updated Successfully';
                    parent.checkValidations(page,alertMsg);
                    parent.emailwindow.hide();
                </script>
            <?php
        }else{
            $this->setLayout(false);
        }
    }

    protected function saveReportUser($postdataArray)
    {
        $sfUser = new sfGuardUser();
        $sfUser->setUsername($postdataArray['username']);
        $pass_word = PasswordHelper::generatePassword();
        $sfUser->setPassword($pass_word);
        $sfUser->setIsActive(true);
        $sfUser->save();
        //insert userid and password in EpPasswordPolicy table
        EpPasswordPolicyManager::savePasswordForNewRegis($sfUser->getId(),$sfUser->getPassword());
        $sfUserId = $sfUser->getId();

        $sfGroupDetails = Doctrine::getTable('sfGuardGroup')->findByName(sfConfig::get('app_pfm_role_report_user')); //hard coded as of now; is the bank admin
        $sfGroupId = $sfGroupDetails->getFirst()->getId();


        $sfGuardUsrGrp = new sfGuardUserGroup();
        $sfGuardUsrGrp->setGroupId($sfGroupId);
        $sfGuardUsrGrp->setUserId($sfUserId);
        $sfGuardUsrGrp->save();

        $userDetail = new UserDetail();
        $userDetail->setUserId($sfUserId);
        $userDetail->setFirstName(trim($postdataArray['name']));
        $userDetail->setDob($postdataArray['dob']);
        $userDetail->setAddress(trim($postdataArray['address']));
        $userDetail->setEmail(trim($postdataArray['email']));
        $userDetail->setMobilePhone(trim($postdataArray['phone']));
        $userDetail->save();
        $username = $postdataArray['username'];

        $subject = "Welcome to Pay4Me" ;
        $partialName = 'welcome_email' ;
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
       // $login_url = url_for('@bank_login?bankName='.$bankname->getFirst()->getAcronym(), true);
        $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification',$this->moduleName."/sendEmail", array('userid'=>$sfUserId,'password'=>$pass_word,'subject'=>$subject,'partialName'=>$partialName));
        $this->logMessage("sceduled mail job with id: $taskId", 'debug');

        $this->getUser()->setFlash('notice',  "'$username'".' created successfully as  Report User <br/>The password is - '.$pass_word);


        $this->redirect('userManagement/index');

    }

     public function executeSendEmail(sfWebRequest $request) {
        $this->setLayout(null);
        $userid = $request->getParameter('userid');
        $password = $request->getParameter('password');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partialName');
        $login_url = $request->getParameter('login_url');
        $sendMailObj = new Mailing() ;
        $mailInfo = $sendMailObj->sendUserEmail($userid,$password,$subject,$partialName,$login_url);
        return $this->renderText($mailInfo);
    }

    public function executeView(sfWebRequest $request){  
        $this->email = '';
        $this->phone = '';
        $this->userId = '';
    }

    public function executeShowUserDetail(sfWebRequest $request){
        $email = $request->getParameter('email');
        $phone = $request->getParameter('phone');
        $userId = $request->getParameter('userId');
//        if(isset($phone) && $phone != ''){
//           $phone = '+'.$phone;
//        }
        $userDetailQuery = UserDetailTable::getInstance()->getUserDetailList($email, $phone, $userId);

        $this->getUser()->setAttribute('email', $email);
        $this->getUser()->setAttribute('fdate', $phone);
        $this->getUser()->setAttribute('tdate', $userId);

        $this->page = 1;
        if ($request->hasParameter('page')) {
        $this->page = $request->getParameter('page');
       }
       

       $this->pager = new sfDoctrinePager('UserDetail', sfConfig::get('app_records_per_page'));
       $this->pager->setQuery($userDetailQuery);
       $this->pager->setPage($this->getRequestParameter('page', $this->page));
       $this->pager->init();
       $this->setTemplate('showUserDetail');


        
    }
}
