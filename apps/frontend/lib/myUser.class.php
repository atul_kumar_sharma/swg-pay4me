<?php

class myUser extends sfGuardSecurityUser {
    protected $cart_var = "cartItems";
    private $groups = null;
    public function initialize(sfEventDispatcher $dispatcher, sfStorage $storage, $options = array()) {
        //echo sfcontext::getInstance()->getRequest()->getForwardedFor();
        $userId = "";
        $authenticated = $storage->read(self::AUTH_NAMESPACE);
        if($authenticated) {
            $timeout = $options['timeout'];
            $lastRequest =  $storage->read(self::LAST_REQUEST_NAMESPACE);
            if (false !== $timeout && !is_null($lastRequest) && time() - $lastRequest >= $timeout) {
                $attributeArray =  $storage->read(self::ATTRIBUTE_NAMESPACE);
                $userId =  $attributeArray['sfGuardSecurityUser']['user_id'];
                $sessId = session_id();
            }
        }
        parent::initialize($dispatcher,$storage,$options);
        //  $cookie = $this->setCookies();
        //    print "<pre>";
        //  print_r($this->getAttributeHolder()->getAll());exit;

        if($userId) {//print "fdd";exit;
            $this->doSessionCleanUp($sessId);
            //check request type: if is ajax type, then return symply 'logout'
            if($this->checkRequestType()) {
                echo "logout";
                die;
            }
            $this->logTimeoutAuditDetails($userId);
            $groupArray = Doctrine::getTable('sfGuardUserGroup')->getUserGroupDetails($userId);
            $groupName =  array();
            $groupName[] = $groupArray['sfGuardGroup']['name'];
            $this->redirectToUserHome($groupName,$userId);
            //redirect to group last login
        }
        //else {}
    }

    public function redirectToUserHome($groupName,$userId) {


        if(in_array(sfConfig::get('app_ipfm_role_portal_admin'),$groupName) ||  in_array(sfConfig::get('app_ipfm_role_admin'),$groupName) ) {
            return sfContext::getInstance()->getController()->redirect('@adminhome');

        }
        //echo $_COOKIE['login_direct'];die;
        //return sfContext::getInstance()->getController()->redirect('@paymentUserhome?openid_identifier=pay4me');

        $this->setFlash('session_expired_message', sfConfig::get('app_session_expired_message'));
        return sfContext::getInstance()->getController()->redirect('@homepage');
    }

    ##########################
    #### if request is ajax type, then return false(not supported)
    ##########################
    private function checkRequestType() {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));

    }
    public function logTimeoutAuditDetails($id='') {
        //Log audit Details
        $username = '';
        if($id) {
            $userGuardObj = Doctrine::getTable('sfGuardUser')->find($id);
            $username = $userGuardObj->getUsername();
        }
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_TIMEOUT,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_TIMEOUT, array('username'=>$username)),
            null,$id,$username
        );
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }
    public function getGroupDescription() {
        $this->loadGroupsDescriptions();
        return array_keys($this->groups);
    }
    public function loadGroupsDescriptions() {
        if (!$this->groups) {
            $groups = $this->getGroups();
            foreach ($groups as $group) {
                $this->groups[$group->getDescription()] = $group;
            }
        }
    }
    public function signIn($user, $remember = false, $con = null) {
        // signin


        $this->setAttribute('user_id', $user->getId(), 'sfGuardSecurityUser');
        $this->setAuthenticated(true);
        $this->clearCredentials();
        $this->addCredentials($user->getAllPermissionNames());

        // Check if user is first time login
        if($this->isPortalAdmin($user)) {
            if($this->isFirstLogin($user)) {
                //clear all credentials
                //      echo "clearing all permissions";
                $this->clearCredentials();
                //create a log; that user has logged in for the first time
                $this->log_first_login();
                //Audit trail log first login
                $this->logFirstLoginSuccessAuditDetails($user->getUsername(),$user->getId());
                $this->setFlash('notice', 'Please change your password to continue.');
                $current_action = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();
                $current_action->redirect('@change_first_password');
            }
            else {
                // save last login
                $this->logLoginSuccessAuditDetails($user->getUsername(),$user->getId());
                $user->setLastLogin(date('Y-m-d h:i:s'));

            }

            $userDetailObj = $user->getUserDetail();



            //    echo "<pre>";print_r($userDetailObj);die;
            if($userDetailObj->getFailedAttempt()>0)
            $userDetailObj->setFailedAttempt(0);
            $user->save($con);
            //
            $request = sfContext::getInstance()->getRequest();
            $ipAddress =  $request->getRemoteAddress();
            $this->saveSessionData();
            //evaluate password plicy
            $this->evalPasswordPolicy($user->getId());
        }
        $userDetailObj = $user->getUserDetail();

        $cartItems = $userDetailObj->getCartItems();
        if($cartItems!="") {
            //$items = $this->getAttribute($this->cart_var,array());
            // array_unshift($items, $item);
            $this->setAttribute($this->cart_var,unserialize($cartItems));

            $items = $this->getAttribute($this->cart_var);


        }

        // remember?
        if ($remember) {
            $expiration_age = sfConfig::get('app_sf_guard_plugin_remember_key_expiration_age', 15 * 24 * 3600);
            // remove old keys
            Doctrine_Query::create()
            ->delete()
            ->from('sfGuardRememberKey k')
            ->where('created_at < ?', date('Y-m-d H:i:s', time() - $expiration_age))
            ->execute();

            // remove other keys from this user
            Doctrine_Query::create()
            ->delete()
            ->from('sfGuardRememberKey k')
            ->where('k.user_id = ?', $user->getId())
            ->execute();

            // generate new keys
            $key = $this->generateRandomKey();

            // save key
            $rk = new sfGuardRememberKey();
            $rk->setRememberKey($key);
            $rk->setsfGuardUser($user);
            $rk->setIpAddress($ipAddress);
            $rk->save($con);

            // make key as a cookie
            $remember_cookie = sfConfig::get('app_sf_guard_plugin_remember_cookie_name', 'sfRemember');
            sfContext::getInstance()->getResponse()->setCookie($remember_cookie, $key, time() + $expiration_age);
        }

        //    $this->saveSessionData();
    }
    private function saveSessionData() {
        $saveSession = sfConfig::get('app_save_userdetails_with_sessiondb');
        if($saveSession) {
            $EpDBSessionDetailObj =  new EpDBSessionDetail();
            $EpDBSessionDetailObj->setUserSessionData();
        }
    }
    private function doSessionCleanUp($sessId='') {
        $EpDBSessionDetailObj =  new EpDBSessionDetail();
        $EpDBSessionDetailObj->doCleanUpSessionByUser($sessId);
    }

    public function isFirstLogin($user = null) {
        // die('First Login');

        if(isset($user) && !$user) {
            $user = $this->user;
        }
        if(is_array($user->getGroupNames())) {
            $group_name = $user->getGroupNames();
        }
        else
        if(is_object($user->getGroupNames())) {
            $group_name = sfContext::getInstance()->getUser()->getGroupNames();
        }
        //check for super admin if yes, then don't check last login
        if(in_array(sfConfig::get('app_ipfm_role_admin'),$group_name)) {
            $lastLogin =  $user['last_login'];
            return (empty($lastLogin))?true:false;
        }
        else {
            return false;
        }
    }

    public function isPortalAdmin($user = null) {
        // die('First Login');

        if(isset($user) && !$user) {
            $user = $this->user;
        }
        if(is_array($user->getGroupNames())) {
            $group_name = $user->getGroupNames();
        }
        else
        if(is_object($user->getGroupNames())) {
            $group_name = sfContext::getInstance()->getUser()->getGroupNames();
        }
        //check for super admin if yes, then don't check last login
        if(in_array(sfConfig::get('app_ipfm_role_portal_admin'),$group_name)) {
            return true;
        }elseif(in_array(sfConfig::get('app_ipfm_role_admin'),$group_name)) {
            return true;
        }
        else {
            return false;
        }
    }

    private function evalPasswordPolicy($userId) {
        $val = EpPasswordPolicyManager::evaluateSiginPolicy($userId);
        if($val) {
            if($val <= 7)
            $this->setFlash('error', 'Your password will be expired after '.$val.' days.');
        }else {
            $this->clearCredentials();
            $this->setFlash('error', 'Your password has been expired, please update your password.');
            $current_action = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();
            $current_action->redirect('@change_first_password');
            //            return sfContext::getInstance()->getController()->redirect('@change_first_password');
        }
    }


    public function log_first_login() {
        return 1;
    }

    public function log_successful_login() {
        return 1;
    }

    public function log_logout() {
        return 1;
    }
    public function logFirstLoginSuccessAuditDetails($uname,$id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN, array('username'=>$uname)),
            null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function logLoginSuccessAuditDetails($uname,$id) {
        //Log audit Details
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_SECURITY,
            EpAuditEvent::$SUBCATEGORY_SECURITY_LOGIN,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_LOGIN, array('username'=>$uname)),
            null);
        $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public function addCartItem(INisCartItem $item) {
        // add this item to cart
        $item_appId = $item->getAppId();
        $item_type = $item->getType();
        $item_fee = $item->getPrice() ;
        if($item_fee == 0)
            return "gratis";
            
        if($item_type == 'Vap'){
          $vapStatus = $this->checkIfAppRejected($item_appId);
          if($vapStatus == 'Rejected'){
            return "application_rejected";
          }
        }
        $cartValidationObj = new CartValidation();
        $hasCommonOption = $cartValidationObj->getCommonOption($item->getApplication(),$item_type,$item_appId);
        $isPrivilegeUser = $this->isPrivilegeUser();
        // check is MO country
        //    $isMoCountry = $this->isMoApplication($item->getApplication(),$item_type,$item_appId);
        //check valid for current cart
          if(!$hasCommonOption){
              if(count($this->getCartItems())>0){
                  return "common_opt_not_available";
              }
          }
        
        $isDuplicate = $this->chk_item_dulicacy($item_appId, $item_type);
        if($isDuplicate) {
            switch($isDuplicate){
                case "1":
                    return "duplicate";
                    break;
                case "2":
                    return "application_edit";
                    break;
                case "3":
                    return "only_vap_allowed";
                    break;
                default:
                    break;
            }
        }
        // check items in cart
        $cartValue = $this->getCartValue() ;
        if($isPrivilegeUser == false){
            if(!(count($this->getCartItems()) == 0)){
                //Start Code:Kuldeep -> get catr amount capacity and application capacity //
                //$isUserFlg = false;
                $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
                if(!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)){
                    $arrCartCapacity = $arrCartCapacityLimit;
                }


                if((count($this->getCartItems()) >= $arrCartCapacity['cart_capacity']))
                {
                    return "capacity_full";
                }
                if(($arrCartCapacity['cart_amount_capacity'] && ( ($cartValue + $item_fee) >  $arrCartCapacity['cart_amount_capacity'])))
                {

                    return "amount_capacity_full" ;
                }
            }
        }

        
        $items = $this->getAttribute($this->cart_var,array());
        array_unshift($items, $item);
        $this->setAttribute($this->cart_var,$items);
        $this->saveCartInDb();
    }
    
/**function chk_item_dulicacy()
   *@purpose :get cart capacity by user
   *@param : N/A
   *@return :  array
   *@author : KSingh
   *@date : 30-05-2011
   */
    public function chk_item_dulicacy($appId, $appType) { 
        if ( !$this->hasAttribute($this->cart_var)) {
            return 0; // doesn't exists
        }
        $items = $this->getAttribute($this->cart_var);
        foreach($items as $item) {
            if ($item->getAppId() == $appId && $item->getType() == $appType) {
                return 1;
            }else if($item->getAppId() == $appId  && ($item->getType() == 'Visa' || $item->getType() == 'Freezone')){
                //$this->removeCartItemById($item->getAppId(),$item->getType());
                return 2;
            }else if(($item->getType() == 'Vap') && ($appType != 'Vap') || ($item->getType() != 'Vap') && ($appType == 'Vap')){
                 return 3;
            }
        }
        return 0;
    }
    public function getCartItems() {
        return $this->getAttribute($this->cart_var,array());
    }

    public function removeCartItem(INisCartItem $item) {
        $this->removeCartItemById($item->getAppId(), $item->getType());

    }

    public function getCartValue() {
        if ( !$this->hasAttribute($this->cart_var)) {
            return; // doesn't exists
        }
        $items = $this->getAttribute($this->cart_var);
        $cartValue=0;
        foreach ($items as $item) {
            $cartValue += $item->getPrice() ;
        }
        return $cartValue;
    }

    public function removeCartItemById($appId,$appType) {
        if ( !$this->hasAttribute($this->cart_var)) {
            return; // doesn't exists
        }
        $items = $this->getAttribute($this->cart_var);
        $new_items=array();
        foreach ($items as $item) {
            if ($item->getAppId() == $appId && $item->getType() == $appType) {
                // this needs to be removed;
            } else {
                $new_items[]=$item;
            }
        }
        $this->setAttribute($this->cart_var,$new_items);
        $this->saveCartInDb();
    }

    public function clearCart() {
        $this->setAttribute($this->cart_var,null);
    }

    

    public function saveCartInDb() {
        $items = $this->getAttribute($this->cart_var);

        $userDetailObj = $this->getGuardUser()->getUserDetail();
        if(count($items)) {
            $userDetailObj->setCartItems(serialize($items));
        }
        else {
            $userDetailObj->setCartItems(NULL);
        }
        $userDetailObj->save();
    }

    public function isMoApplication($nisApplication,$appType,$aapId){
        $appArr = $nisApplication->toArray();
        switch ($appType){
            case "Passport":
                $processingCountry = $appArr['processing_country_id'];
                $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($processingCountry);
                if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                {
                    if($paymentMode[0]['card_type'] == "Mo")
                    return true;
                    else
                    return false;
                }else{
                    return false;
                }
                break;
            case "Visa":
                $arrProcessingCountry = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($aapId);
                $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($arrProcessingCountry);
                if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                {
                    if($paymentMode[0]['card_type'] == "Mo")
                    return true;
                    else
                    return false;
                }else{
                    return false;
                }
                break;
            case "Freezone":
                $arrProcessingCountryId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($aapId);
                $arrProcessingCountry = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($arrProcessingCountryId);
                $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($arrProcessingCountry);
                if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                {
                    if($paymentMode[0]['card_type'] == "Mo")
                    return true;
                    else
                    return false;
                }else{
                    return false;
                }
                break;
        }
    }
    public function isMoCart(){
        $this->cartItems = $this->getCartItems();
        //        echo "<pre>";print_r(get_class_methods($this->cartItems[0]));
        //      echo "<pre>";print_r($this->cartItems[0]->getApplication
        //()->toArray());die;
        //die(var_dump());
        //    echo "<pre>";print_r($this->cartItems);die;
        if(count($this->cartItems) > 0){
            foreach ($this->cartItems as $cartObj){
                switch ($cartObj->getType()){
                    case 'Passport':
                        $applicationArr = $cartObj->getApplication()->toArray();
                        $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($applicationArr['processing_country_id']);
                        if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                        {
                            if($paymentMode[0]['card_type'] != "Mo")
                            return false;
                        }else{
                            return false;
                        }
                        break;
                    case "Visa":
                        $arrProcessingCountry = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($cartObj->getAppId());
                        $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($arrProcessingCountry);
                        if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                        {
                            if($paymentMode[0]['card_type'] != "Mo")
                            return false;
                        }else{
                            return false;
                        }
                        break;
                    case "Freezone":
                        $arrProcessingCountryId = Doctrine::getTable('ReEntryVisaApplication')->getProcessingCountry($cartObj->getAppId());
                        $arrProcessingCountry = Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($arrProcessingCountryId);
                        $paymentMode = Doctrine::getTable("CountryPaymentMode")->getCardType($arrProcessingCountry);
                        if(isset ($paymentMode[0]['card_type']) && $paymentMode[0]['card_type']!='')
                        {
                            if($paymentMode[0]['card_type'] != "Mo")
                            return false;
                        }else{
                            return false;
                        }
                        break;
                }
            }
            return true;
        }else{
            return '';
        }
    }

    
/**function isPrivilegeUser()
   *@purpose :check if privilege user or not
   *@param : N/A
   *@return :  boolean
   *@author : KSingh
   *@date : 04-05-2011
   */
    public function isPrivilegeUser(){
        $arrActiveUser = Doctrine::getTable('PaymentPrivilageUser')->getAllActivePaymentUser();
        $arrActiveUserId = array();
        $isPrivilegeUser = false;
        if(!empty($arrActiveUser)){
            foreach($arrActiveUser as $val){
                $arrActiveUserId[] = $val['user_id'];
            }

            if(in_array($this->getGuardUser()->getId(),$arrActiveUserId)){
                $isPrivilegeUser = sfConfig::get('app_is_privilege_user');
            }
        }

        return $isPrivilegeUser;
    }

/**function checkIfAppRejected()
   *@purpose :check if app rejected or not
   *@param   : $appId
   *@return  :  boolean
   *@author  : KSingh
   *@date    : 04-05-2011
   */
  public function checkIfAppRejected($appId) {
        if (isset($appId)) {
           $visa_application = Doctrine::getTable('VapApplication')->getVisaArrivalInfo($appId);
           return $visa_application[0]['status'];
        } else {
            return;
        }
    }

    /**
     *
     * @param <type> $appId
     * @param <type> $appType
     * @param <type> $amount
     * @return <type> 
     * @author <name> Ashwani Kumar
     * @date   <date> 31 May 2012
     * This function update cart items and save into database as well...
     */
    public function updateCartItems($updatedItems) {
        

        if ( !$this->hasAttribute($this->cart_var)) {
            return; // doesn't exists
        }
        $items = $this->getAttribute($this->cart_var);
        $new_items=array();
        
        foreach ($items as $item) {
            
            if(isset($updatedItems[$item->getAppId()])){
                if($item->getPrice() != $updatedItems[$item->getAppId()]->getPrice()){
                    $new_items[] = $updatedItems[$item->getAppId()];
                }else{
                    $new_items[] = $item;
                }
            }else{
                $new_items[] = $item;
            }
        }
        
        $this->setAttribute($this->cart_var,$new_items);
        $this->saveCartInDb();
    }

}

