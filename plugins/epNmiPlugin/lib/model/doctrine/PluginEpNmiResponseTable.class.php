<?php

/**
 * PluginEpNmiResponseTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpNmiResponseTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginEpNmiResponseTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginEpNmiResponse');
    }
}