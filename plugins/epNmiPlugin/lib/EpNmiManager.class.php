<?php

class EpNmiManager
{
  public $browserInstance ;
  protected $Request_XML;
  protected $Response_XML;
  protected $Request_id;
  private $Gateway_id = 10; // 10 for mid1...
  private $midService = 'jna1';
  
  /**
  *
  * @param <type> $var contains variable name...
  * @param <type> $val contains variable value...
  */
  public function __set($var, $val) {
      $this->$var = $val;
  }
  
  public function getFormUrl($amount, $orderNumber, $url, $is3dVerified=1){    

    $transDate = date('d-M-Y H:i:s') ;

    try{

      $requestXml = $this->generateFormUrlXml($amount, $url,$orderNumber, $is3dVerified);      

      /**
       * [WP: 087] => CR:126
       * Creating Form URL Request Log...
       */      
      $this->createLogData($requestXml, $orderNumber.'_FormURLRequestXml'); 

      if($requestXml){
        // Log Form URl Request
        //Send Form URL
        $formUrlResponse = $this->sendFormUrlRequest($requestXml, $orderNumber.'_FormURLResponseXml', $orderNumber) ;

        if(count($formUrlResponse)){             
          $this->saveNmiRequest($formUrlResponse['transaction-id'], $orderNumber, $amount);          
          // save nmi_transaction_request data
          return $formUrlResponse['form-url'] ;
        }else{
          sfContext::getInstance()->getLogger()->debug("This service (JNA NMI) is currently not available. Please try again later.");
          return '';
        }
      }else{
        return '';
      }
    }
    catch (Exception $e)
    {
      sfContext::getInstance()->getLogger()->debug("NMI Exception on getting post url : ".$e->getMessage());
      return '';
    }

  }

  /**
   *
   * @return <type> nmi api key...
   */
  function getApiKey(){
      return sfConfig::get('app_nmi_'.$this->midService.'_api_key');
  }

  /**
   *
   * @return <type> nmi redirect url...
   */
  function getRedirectUrl(){
      return sfConfig::get('app_nmi_'.$this->midService.'_redirect_url');
  }

  /**
   *
   * @return <type> nmi form request url...
   */
  function getFormRequestUrl(){
      return sfConfig::get('app_nmi_'.$this->midService.'_form_request_url');
  }

  /**
   *
   * @return <type> 
   */
  function getQueryUrl(){
      return sfConfig::get('app_nmi_'.$this->midService.'_query_url');
  }



  private function generateFormUrlXml($amount , $url,$orderNumber, $is3dVerified){


    /**
     * [WP: 087] => CR:126
     */
    ## Getting processing country...
    $processingCountry = sfContext::getInstance()->getUser()->getAttribute('processingCountry');

    $destCurrencyId = CurrencyManager::getDestinationCurrency($processingCountry);

    $currencyCode = '840'; // dollar
    
    ## 4 means yuan...
    if($destCurrencyId == 4){
        $orderReqDetailId = Doctrine::getTable('ipay4meOrder')->getLatestOrderRequestDetailId($orderNumber);
        $transactionObj = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($orderReqDetailId);
        $appCount = count($transactionObj);
        $chinaFlag = true;
        foreach($transactionObj AS $transaction){
            if(strtolower($transaction['app_type']) == 'vap'){
                $chinaFlag = false;
                break;
            }//End of if(strtolower($transaction['app_type']) == 'vap'){...
        }//End of foreach($transactionObj AS $transaction){...
        if($chinaFlag){
            $currencyCode = '156'; // yuan
            /**
             * [WP: 102] => CR: 144
             * Getting convert amount from table...
             */
            $orderReqObj = Doctrine::getTable('OrderRequestDetails')->find($orderReqDetailId);
            if(!empty($orderReqObj)){
                //CurrencyManager::getConvertedAmount($amount, 1, $destCurrencyId, $appCount); // 1 is for dollar...
                $amount = $orderReqObj->getConvertAmount();
            }//End of if(!empty($orderReqObj)){...
        }//End of if($chinaFlag){...
    }//End of if($destCurrencyId == 4){...


    $api_key = $this->getApiKey();
    $redirect_url = $this->getRedirectUrl();
    $redirect_url = $url.'/'.$redirect_url."?order=$orderNumber";
    if($is3dVerified){
        $element = 'sale';
    }else{
        $element = 'auth';
    }
    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement($element);
    $root = $doc->appendChild($root);

    $structElm = $doc->createElement("api-key");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($api_key));

    $structElm = $doc->createElement("redirect-url");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($redirect_url));

    $structElm = $doc->createElement("amount");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($amount));

    $structElm = $doc->createElement("order-id");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($orderNumber));

    /**
     * [WP: 087] => CR:126
     * Adding currency code value...
     */
    $paymentOptions = new paymentOptions();
    $arrProcessingCountry = $paymentOptions->arrProcessingCountry;
    $paymentCards = $paymentOptions->getCards();

    /**
     * [WP: 111] => CR: 157
     * Added condition of MasterCard SecureCode for JNA service...
     */      
    if(in_array('jna_nmi_vbv', $paymentCards) || in_array('jna_nmi_mcs', $paymentCards)){
        $structElm = $doc->createElement("currency-code");
        $root->appendChild($structElm);
        $structElm->appendChild($doc->createTextNode($currencyCode));
    }

    $xmldata = $doc->saveXML();
    return $xmldata;

  }

  private function sendFormUrlRequest($requestXml, $logFile = 'FormURLResponseXml', $orderNumber = ''){

    try{
      ////////Send Curl Request  Start ///////////
      $header[] = "Content-Type: text/xml;charset=UTF-8";

      $url =  $this->getFormRequestUrl() ;

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $requestXml);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

      $responseXml = curl_exec($curl);

      $this->Response_XML = $responseXml;
      ////////Send Curl Request  End ///////////

      ///////////////////// LOGGING //////////////////////////////
      $this->createLogData($responseXml, $logFile);

      /**
       * This log will generate either URL is not working  or response xml does not come...
       */
      if(trim($responseXml) == ''){

          ## Fetching order details from ipay4meorder table using given order...
          $orderDetails = Doctrine::getTable('Ipay4meOrder')->findByOrderNumber($orderNumber);
          
          if(count($orderDetails)> 0){ 
              $orderNumber = $orderDetails[0]['order_number'];
              $gatewayId = $orderDetails[0]['gateway_id'];
              $requestId = $orderDetails[0]['order_request_detail_id'];
              $card_first = $orderDetails[0]['card_first'];
              $card_last = $orderDetails[0]['card_last'];
              $card_type = $orderDetails[0]['card_type'];
              $card_hash = $orderDetails[0]['card_hash'];
              $userId = $orderDetails[0]['created_by'];
          }else{
              $orderNumber = '';
              $requestId = '';
              $gatewayId = '';
              $card_first = '';
              $card_last = '';
              $card_type = '';
              $card_hash = '';
              $userId = '';              
          }

          $data  = "FormRequestURL: $url\n";
          $data .= "UserId:         $userId\n";
          $data .= "RequestId:      $requestId\n";
          $data .= "Gateway Id:     $gatewayId\n";
          $data .= "Order Number:   $orderNumber\n";
          $data .= "Card Type:      $card_type\n";
          $data .= "Card First:     $card_first\n";
          $data .= "Card Last:      $card_last\n";
          $data .= "Card Hash:      $card_hash\n";
          $data .= "Date & Time:    ".date('Y-m-d H:i:s')."\n";          
          $data .= "Step:           Step 1\n";
          
          $logfile = 'FormRequestURL';
          $this->createLogData($data, $logfile, 'NmiServerDownLog');

      }//End of if(trim($responseXml) == ''){...
      
      ///////////////////////////////////////////////////////////
      $respObj = simplexml_load_string($responseXml);
      
      $respArr = array();
      if(!isset($responseXml) || ''==$responseXml || !$respObj || ''===$respObj)
      {
        $crash = 1;
      }
      else
      {
        $respArr = $this->getArrayfrmXML($respObj,array());       
      }
      return $respArr;

    }catch(Exception $e){
      $sendMailUrl = "notification/mailOnPaymentCrash" ;
      $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

    }
  }
  public function finaliseTransaction($tokenId, $processingCountries,$arrValidationRules, $orderNumber){

    try{

      $transactionRequestXml = $this->generateTransactionXml($tokenId);

      ## Creating Transaction Request Log...
      $this->createLogData($transactionRequestXml, $orderNumber.'_TransactionRequestXml');

      if($transactionRequestXml){
        // Log Form URl Request
        //Send Form URL
        $transactionResponse = $this->sendTransactionRequest($transactionRequestXml, $orderNumber);

        if($transactionResponse['result-code'] != '100'){
            return false;
        }
        $saeResponse = Doctrine::getTable('EpNmiRequest')->getNmiRequestId($transactionResponse['transaction-id']);
        $saveResponse = Doctrine::getTable('EpNmiResponse')->setResponse($transactionResponse,$saeResponse[0]['id'],$saeResponse[0]['order_id'],$tokenId);
        $isPrivilegeUser = sfContext::getInstance()->getUser()->isPrivilegeUser();
        $is3DVerified = 'no';
        if(array_key_exists('cardholder-auth', $transactionResponse))
        {
          $authValue = $transactionResponse['cardholder-auth'];
          if(strtolower($authValue) == 'verified')
          $is3DVerified = 'yes';
        }
        $returnArr = $saveResponse->toArray();
        $returnArr['3d-verified'] = $is3DVerified;

        switch ($transactionResponse['action-type'])
        {
          case 'sale':
            $returnArr['success'] = 'success';
            break;
          case 'auth':
            $card = '' ;
            $capture = false ;
            $matchSurname = true ;
            $goFor3dCheck = true ;
            $error = '';

            if(substr($transactionResponse['billing'][0]['cc-number'], 0, 1) == '4'){
              $card = 'nmi_vbv' ;
            }elseif(substr($transactionResponse['billing'][0]['cc-number'], 0, 1) == '5'){
              $card = 'nmi_mcs' ;
            }elseif(substr($transactionResponse['billing'][0]['cc-number'], 0, 1) == '3'){
              $card = 'nmi_amx' ;
            }

            
                if($this->isCardRegistered($saeResponse[0]['order_id']))
                    {
                      $capture = true ;
                      $matchSurname = false ;
                      $goFor3dCheck = false ;
                    }

                    if($is3DVerified == 'yes')
                    {
                      $capture = true ;
                      $matchSurname = false ;
                      $goFor3dCheck = false ;
                    }

                    $country_code = $transactionResponse['billing'][0]['country'];

                    if($goFor3dCheck)
                    {
                      $arrIntersec = in_array($country_code, $processingCountries);
                      if(!$arrIntersec)
                      {
                        $processingCountries[count($processingCountries)] = $country_code;
                      }
                      $isCountryHas3dCheck = CountryBasedPaymentTable::getInstance()->isCountryHas3dCheck($card, $processingCountries);
                      if($isCountryHas3dCheck)
                      {
                        if($is3DVerified == 'yes')
                        {
                          $capture = true ;
                          $matchSurname = false ;
                        }else{
                          $capture = false ;
                          $matchSurname = false ;
                          $error = 'service';
                        }
                      }
                    }

                    $isCountryHasZipCheck = CountryBasedZipTable::getInstance()->isCountryHasZipCheck($country_code);

                    if($capture && $isCountryHasZipCheck && $is3DVerified == 'no')
                    {
                      if(array_key_exists('avs-result', $transactionResponse))
                      {
                        switch ($transactionResponse['avs-result'])
                        {
                          case 'X':
                            $capture = true ;
                            break;
                          case 'Y':
                            $capture = true ;
                            break;
                          case 'D':
                            $capture = true ;
                            break;
                          case 'M':
                            $capture = true ;
                            break;
                          case 'W':
                            $capture = true ;
                            break;
                          case 'Z':
                            $capture = true ;
                            break;
                          case 'P':
                            $capture = true ;
                            break;
                          case 'L':
                            $capture = true ;
                            break;
                          default:
                            $capture = false ;
                            $error = 'zip';
                            break;
                        }
                      }else{
                        $capture = false ;
                        $matchSurname = false ;
                        $error = 'zip';
                      }
                    }

                    
                    if($matchSurname)
                    {
//                      if(!empty($arrValidationRules) && is_array($arrValidationRules) && in_array('SN',$arrValidationRules))
//                      {
//                          ## Start -- Kuldeep -- Getting surname check variable...
//                          //setting up param array
//                          $arrSurNameParam['first_name'] = $returnArr['first_name'];
//                          $arrSurNameParam['last_name'] = $returnArr['last_name'];
//                          //setup object of surname validation class
//                          $objSurName = new SurNameValidation();
//                          $arrReturnSurName = $objSurName->checkForSurNameValidation($arrSurNameParam,$card);
//                          $isSurNameMatch = $arrReturnSurName['isSurNameMatch'];
//                          $isFullNameMatch = $arrReturnSurName['isFullNameMatch'];
//                          //$isMatch = $this->matchSurname($surnameArray, $returnArr['last_name']);
//                          //if($isMatch)
//                          //END:
//                          if($isSurNameMatch && $isFullNameMatch)
//                          {
//                            $capture = true;
//                          }else{
//                            $capture = false;
//                            $error = 'surname-match';
//
//                            /**
//                             * [WP: 086] => CR:125
//                             * Added ruleId 6 while surname failure...
//                             */
//
//                            if (is_array($arrValidationRules) && in_array('SN', $arrValidationRules) && count($arrValidationRules) > 1) {
//                               $fpsId = sfContext::getInstance()->getUser()->getAttribute('fpsId');
//                               $fpsObj = Doctrine::getTable('FpsDetail')->find($fpsId);
//                               if(!empty($fpsObj)){
//                                   $fpsObj->setFpsRuleId(6);
//                                   $fpsObj->save();
//                               }
//                            }
//
//
//                          }
//                        }else{
//                            $capture = true;
//                        }
                          $capture = true;
                    }

            if($capture)
            {
              $captureXl = $this->capturePayment($transactionResponse['amount'] ,$transactionResponse['transaction-id']);              
              $formUrlResponse = $this->sendFormUrlRequest($captureXl, 'CaptureResponseXml');              
              if($formUrlResponse['result-code'] == '100'){
                $returnArr['success'] = 'success';
              }else{
                $returnArr['success'] = 'filure';
              }
              $capture = 'capture';
            }else{
              $voidXl = $this->voidPayment($transactionResponse['transaction-id']);
              $formUrlResponse = $this->sendFormUrlRequest($voidXl, 'VoidResponseXml');
              $returnArr['success'] = 'filure';
              $capture = 'void';
            }
            $saveCapture = Doctrine::getTable('EpNmiResponse')->setCapture($transactionResponse['order-id'], $capture);
            $returnArr['error'] = $error;
            $returnArr['card'] = $card;
            break;
          default:
            return false;
            break;
        }

        return $returnArr;
        // return $formUrlResponse['form-url'] ;
      }
    }
    catch (Exception $e)
    {
      throw new Exception ($e->getMessage());
    }
  }

  private function isCardRegistered($orderNumber)
  {
    $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);
    $cardHash = $orderObj->getFirst()->getCardHash();
    $userId = $orderObj->getFirst()->getCreatedBy();

    $applicantVaultValues = Doctrine::getTable('ApplicantVault')->getDeplicateCardDetails($cardHash, $userId);

    //if card is registered return true
    if($applicantVaultValues)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  private function generateTransactionXml($tokenId){

    $api_key = $this->getApiKey();
    $redirect_url = $this->getRedirectUrl();

    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement('complete-action');
    $root = $doc->appendChild($root);

    $structElm = $doc->createElement("api-key");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($api_key));

    $structElm = $doc->createElement("token-id");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($tokenId));

    $xmldata = $doc->saveXML();
    return $xmldata;

  }
  private function sendTransactionRequest($requestXml, $orderNumber){

    try{
      ////////Send Curl Request  Start ///////////
      $header[] = "Content-Type: text/xml;charset=UTF-8";
      $url =  $this->getFormRequestUrl() ;

      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $requestXml);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

      $responseXml = curl_exec($curl);
      $this->Response_XML = $responseXml;
      
      ////////Send Curl Request  End ///////////
      ///////////////////// LOGGING //////////////////////////////
      $this->createLogData($responseXml, $orderNumber.'_TransactionResponseXml');
      ///////////////////////////////////////////////////////////
      $respObj = simplexml_load_string($responseXml);
      
      $respArr = array();
      if(!isset($responseXml) || ''==$responseXml || !$respObj || $respObj === '')
      {
        $crash = 1;
      }
      else
      { 
        $respArr = $this->getArrayfrmXML($respObj,array());
      }
      return $respArr;

    }catch(Exception $e){
      $sendMailUrl = "notification/mailOnPaymentCrash" ;
      $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

    }
  }

  public function doPayment($userDetail){

    $transDate = date('d-M-Y H:i:s') ;
    $userDetail['transDate'] = $transDate;
    $userDetail['ip_address'] = $userDetail['request_ip'];//sfConfig::get('app_pay_easy_ip_address');
    $userDetail['card_len'] = strlen($userDetail['card_Num']);
    $userDetail['card_first'] = substr($userDetail['card_Num'], 0, 4);
    $userDetail['card_last'] = substr($userDetail['card_Num'], -4, 4);

    try{

      $requestXml = $this->getRequestXML($userDetail);

      if($requestXml){

        $xml = simplexml_load_string($requestXml);
        $xml->STRUCTURE->CARD_INFO->Card_No = $userDetail['card_first'].$this->getNoOfX($userDetail['card_len'] - 8).$userDetail['card_last']  ;
        $xml->STRUCTURE->CARD_INFO->CVV = $this->getNoOfX(strlen($userDetail['cvv']));

        $storingXml = $xml->asXML();
        //           $storingXml = $this->getRequestXML($userDetail);
        $this->createLogData($storingXml,'RequestXml'.$userDetail['cus_id']);
        $Request_id = $this->storeDetail($userDetail,'Request');

        $resp = $this->getResponse($requestXml,$userDetail, $Request_id);
        if($resp)
        {
          $resp['trans_time'] = date('Y-m-d H:i:s',strtotime($transDate)) ;
          return $resp;
        }
      }
    }
    catch (Exception $e)
    {
      throw new Exception ($e->getMessage());
    }

  }

  public function getRequestXML($userDetail){
    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement('PURCHASE_REQUEST');
    //        $root->setAttribute ( "xmlns:xsi", 'http://www.w3.org/2001/XMLSchema-instance' );
    //        $root->setAttribute ( "xmlns", 'http://www.ipay4me.com/schema/pay4meorder/v1' );
    //        $root->setAttribute ( "xsi:schemaLocation", 'http://www.ipay4me.com/schema/ipay4meorder/v1 payEasy.xsd' );


    $root = $doc->appendChild($root);

    $headerElm=$this->getHeaderElm($userDetail,$doc);
    $root->appendChild($headerElm);


    $structElm = $doc->createElement("STRUCTURE");

    $transInfoElm=$this->getTransInfo($userDetail,$doc);
    $structElm->appendChild($transInfoElm);

    $cardInfoElm=$this->getCardInfo($userDetail,$doc);
    $structElm->appendChild($cardInfoElm);

    $billInfoElm=$this->getBillInfo($userDetail,$doc);
    $structElm->appendChild($billInfoElm);

    $contactInfoElm=$this->getContactInfo($userDetail,$doc);
    $structElm->appendChild($contactInfoElm );

    $otherInfoElm=$this->getOtherInfo($userDetail,$doc);
    $structElm->appendChild($otherInfoElm);

    $rebillingInfoElm=$this->getRebillingInfo($userDetail,$doc);
    $structElm->appendChild($rebillingInfoElm);

    $root->appendChild($structElm);

    $xmldata = $doc->saveXML();
    $xmldata = str_replace('<?xml version="1.0"?>','',$xmldata);

    //        vallidate the XML
    //        $xdoc = new DomDocument;
    //
    //        $isLoaded = $xdoc->LoadXML($xmldata);

    //        if($isLoaded)
    //        {
    //            $validRes=$this->validateXMLcontent($xdoc);
    //            if($validRes)
    //            {
    $this->Request_XML = $xmldata;
    return $xmldata;
    //            }
    //        }
    //        else
    //        {   return false;     }
  }



  public function validateXMLcontent($xdoc){
    $plugin_dir = sfConfig::get('sf_plugins_dir') ;

    $xmlschema = $plugin_dir. '/epPayEasyPlugin/config/payEasy.xsd';

    $handle = @fopen($xmlschema, "r");

    try {
      if ($xdoc->schemaValidate($xmlschema))
      return true;
      else
      {return false ;}

    }catch(Exception $ex)  {
      echo $ex->getMessage();
    }

  }
  public function storeDetail($getArr,$type='',$Request_id=''){
    if('Request' == $type){

      $payEasyRequest = new EpPayEasyRequest ();
      $payEasyRequest->setTranDate(date('Y-m-d H:i:s',strtotime($getArr['transDate'])));
      $payEasyRequest->setIssuer(sfConfig::get('app_pay_easy_issuer'));
      $payEasyRequest->setRequestIp(sfConfig::get('app_pay_easy_request_IP'));
      $payEasyRequest->setTranType(sfConfig::get('app_pay_easy_tran_type'));
      $payEasyRequest->setCurrency('USD');
      $payEasyRequest->setAmount($getArr['amount']);
      $payEasyRequest->setCardFirst($getArr['card_first']);
      $payEasyRequest->setCardLast($getArr['card_last']);
      $payEasyRequest->setCardLen($getArr['card_len']);
      $payEasyRequest->setCardType($getArr['card_type']);
      $payEasyRequest->setCardHolder($getArr['card_holder']);
      $payEasyRequest->setExpiryMonth($getArr['expiry_date']['month']);
      $payEasyRequest->setExpiryYear($getArr['expiry_date']['year']);
      //            $payEasyRequest->setCvv($getArr['cvv']);
      $payEasyRequest->setCustomersupport(sfConfig::get('app_pay_easy_customersupport'));
      $payEasyRequest->setAddress1($getArr['address1']);
      $payEasyRequest->setAddress2($getArr['address2']);
      $payEasyRequest->setTown($getArr['town']);
      $payEasyRequest->setState($getArr['state']);
      $payEasyRequest->setZip($getArr['zip']);
      $payEasyRequest->setCountry($getArr['country']);
      $payEasyRequest->setFirstName($getArr['first_name']);
      $payEasyRequest->setLastName($getArr['last_name']);
      $payEasyRequest->setEmail($getArr['email']);
      $payEasyRequest->setPhone($getArr['phone']);
      $payEasyRequest->setCusId($getArr['cus_id']);
      $payEasyRequest->setIpAddress($getArr['ip_address']);
      $payEasyRequest->setRequestXml($this->Request_XML);

      $payEasyRequest->save();
      return $this->Request_id = $payEasyRequest->getId();
    }
    if('Response' == $type){
      $payEasyResponse = new EpPayEasyResponse ();
      $payEasyResponse->setPayeasyReqId($Request_id);
      $payEasyResponse->setTimeTaken($getArr['HEADER'][0]['Time_Taken']);
      $payEasyResponse->setIssuer($getArr['HEADER'][0]['Issuer']);
      $payEasyResponse->setResponseIp($getArr['HEADER'][0]['ResponseIP']);
      $payEasyResponse->setTranType($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Tran_Type']);
      $payEasyResponse->setCurrency($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Currency']);
      $payEasyResponse->setAmount($getArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Amount']);
      $payEasyResponse->setTranId($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Tranid']);
      $payEasyResponse->setResponseCode($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code']);
      $payEasyResponse->setResponseText($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Text']);
      $payEasyResponse->setAuthCode($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Auth_Code']);
      $payEasyResponse->setStatus($getArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Status']);
      if(isset($getArr['STRUCTURE'][1]['FRAUD_INFO']))
      {
        $payEasyResponse->setFraudScore($getArr['STRUCTURE']['FRAUD_INFO']['Score']);
        $payEasyResponse->setFraudMessage($getArr['STRUCTURE']['FRAUD_INFO']['Message']);
      }
      $payEasyResponse->setResponseXml($this->Response_XML);

      $payEasyResponse->save();
    }



  }

  public function getResponse($transXml,$userDetail,$Request_id =''){
    try{
      //        if(!empty($this->Request_XML)){
      ////////Send Curl Request  Start ///////////
      $header[] = "Content-Type: application/x-www-form-urlencoded";
      $url =  sfConfig::get('app_pay_easy_web_url') ;
      //. sfConfig::get('app_pay_easy_terminal_id').
      $xmlRequest = 't='.sfConfig::get('app_pay_easy_t').'&load=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22%3F%3E'.$transXml;

      $curl = curl_init();



      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
      //curl_setopt($curl, CURLOPT_HEADER, 1);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
      curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

      $responseXml = curl_exec($curl);

      $this->Response_XML = $responseXml;
      ////////Send Curl Request  End ///////////
      ///////////////////// LOGGING //////////////////////////////
      $this->createLogData($responseXml,'ResponseXml'.$userDetail['cus_id']);
      ///////////////////////////////////////////////////////////
      //            $xdoc = new DomDocument;
      //            if(isset($responseXml) &&  $responseXml!='')
      //              $isLoaded = $xdoc->LoadXML($responseXml);
      //            if(isset($isLoaded) && $isLoaded)
      //            {
      ////                $validRes=$this->validateXMLcontent($xdoc);
      ////                if($validRes)
      ////                {
      //                    //return $responseXml;//$xmldata;
      ////                }
      //            }
      //            else
      //            {  // return false;     }

      //////////////////////////////////////////////////

      $crash = 0;
      $respObj = simplexml_load_string($responseXml);
      if(!isset($responseXml) || ''==$responseXml || !$respObj || ''==$respObj)
      {
        $crash = 1;
      }
      else
      {
        $respArr = $this->getArrayfrmXML($respObj,array());
      }

      if(0 == $crash && isset($respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid']) &&  $userDetail['cus_id']==$respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid'])
      {
        $this->storeDetail($respArr,'Response',$Request_id);
        $abResp = $this->extractResp($respArr,$userDetail);
      }
      else
      {
        if( isset($respArr) && ($userDetail['cus_id'] != $respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid']))
        $responseXml ='CusId is not same as sent'.$responseXml;
        $abResp = array();
        $abResp['cusid'] = $userDetail['cus_id'];
        $abResp['status'] = 0;
        $abResp['response_code'] = '';
        $abResp['response_text'] = '';

        $sendMailUrl = "notification/mailOnPaymentCrash" ;
        $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        return $abResp;

      }



      return $abResp;
    }catch(Exception $e){
      $sendMailUrl = "notification/mailOnPaymentCrash" ;
      $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$sendMailUrl, array('order_number' => $userDetail['cus_id'],'responseXML' =>$responseXml));
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

    }
    //        }


    //        ////////Send Curl Request  ///////////
    //        $url = sfConfig::get('app_pay_easy_url') ;
    //        $browser = $this->getBrowser() ;
    //        $header['Content-Type'] = "application/x-www-form-urlencode(htmlspecialcharsd";
    //        $obj = new pay4MeXMLV1();
    //        $browser->post($url, $transXml, $header);
    //        $code = $browser->getResponseCode();
    //        if ($code != 200 ) {
    //          $respMsg = $browser->getResponseMessage();
    //          $respText = $browser->getResponseText();
    //          throw new Exception("Error in posting easypay request:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
    //        }
    //        $this->logMessage('payment XML send') ;
    //        $this->logMessage($respText) ;
    //        print_r($respText) ;
    //
    //        ////////Get Curl Response ///////////
    ////        $this->storeDetail($respText,'response');
    //        if(200 == $respCode ){
    //            $absResp = $this->extractResp($resp);
    //
    //        }
    //        return false;
  }
  public function extractResp($respArr, $userDetail){
    $abResp = array();
    if( !isset($respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code']))
    {
      $abResp['cusid'] = $userDetail['cus_id'];
      $abResp['status']= 0;
    }
    else{

      $abResp['cusid']= $respArr['STRUCTURE'][1]['TRANSACTION_INFO'][0]['Cusid'];
      if(8 == $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'] || 9 == $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'])
      $abResp['status']= 1;
      else
      $abResp['status']= 0;
    }
    $abResp['response_code'] = $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Code'];
    $abResp['response_text'] = $respArr['STRUCTURE'][1]['PROCESSED_INFO'][1]['Response_Text'];

    return $abResp;

  }
  public function getArrayfrmXML($xml,$arr){
    $iter = 0;    
    foreach($xml->children() as $b){
      $a = $b->getName();
      if(!$b->children()){
        $arr[$a] = trim($b[0]);
      }
      else{
        $arr[$a][$iter] = array();
        $arr[$a][$iter] = $this->getArrayfrmXML($b,$arr[$a][$iter]);
        $iter++;
      }

    }    
    return $arr;
  }


  public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
  {
    $path = $this->getLogPath($parentLogFolder);
    if($appendTime){
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    }else{
      $file_name = $path."/".$nameFormate.".txt";
    }
    if($append)
    {
      @file_put_contents($file_name, $xmlData, FILE_APPEND);
    }else{
      $i=1;
      while(file_exists($file_name)) {
        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
        $i++;
      }
      @file_put_contents($file_name, $xmlData);
    }
  }



  public function getLogPath($parentLogFolder)
  {

    $logPath =$parentLogFolder==''?'/Nmilog/'.strtoupper($this->midService):'/'.$parentLogFolder.'/'.strtoupper($this->midService);
    $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }

  public function getNoOfX($length)
  {
    $text = "";
    for($i=0;$i<$length;$i++){
      $text.="x";
    }
    return $text;
  }
  function saveNmiRequest($transactionId, $orderNumber, $amount)
  {
    $epNmiRequest = new EpNmiRequest();
    $epNmiRequest->setGatewayId($this->Gateway_id);
    $epNmiRequest->setOrderId($orderNumber);
    $epNmiRequest->setTransactionId($transactionId);
    $epNmiRequest->setAmount($amount);
    $epNmiRequest->save();    
  }
  // Refund start

  public function createRefundXML($transaction_id, $amount){

    $api_key = $this->getApiKey();    

    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement('refund');
    $root = $doc->appendChild($root);

    $structElm = $doc->createElement("api-key");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($api_key));

    $structElm = $doc->createElement("transaction-id");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($transaction_id));

    $structElm = $doc->createElement("amount");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($amount));

    /**
     * [WP: 087] => CR:126
     * Adding currency code value...
     */
     $jnaResponseObj = Doctrine::getTable('EpNmiResponse')->findByTransactionId($transaction_id);
     if(count($jnaResponseObj) > 0){
         $order_id = $jnaResponseObj->getFirst()->getOrderId();
         $gatewayId = Doctrine::getTable('ipay4meOrder')->getGatewayId($order_id);
         if($gatewayId == 10){
            $currency_code = '840';
            $responseObj = Doctrine::getTable('EpNmiResponse')->findByOrderId($order_id);
            if(count($responseObj) > 0){
                $currency = $responseObj->getFirst()->getCurrency();
                if($currency == '156'){
                    $currency_code = $currency;
                }
            }
            $structElm = $doc->createElement("currency-code");
            $root->appendChild($structElm);
            $structElm->appendChild($doc->createTextNode($currency_code));
         }
     }
    

    $xmldata = $doc->saveXML();
    return $xmldata;
  }

  // Get Status of a transaction made at GrayPay
  private function getStatus($order_id){

    $requestString = "order_id=".$order_id ;
    $this->createLogData($requestString,'StatusRequest-'.$order_id, 'nmiRollbackLog');
    
    $url = $this->getQueryUrl();

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();
      $respText = $browser->getResponseText();
      return false ;
    }
    
    $respObj = simplexml_load_string($browser->getResponseText());

    if($respObj){
      $res = $this->getArrayfrmXML($respObj,array()) ;
      return $res ;
    }
    return false ;

  }
  // Return payment status
  public function getPaymentStatus($res){
      
    if(is_array($res)){
        return $res['transaction'][0]['condition'] ;
    }else{
        return '';
    }
    
  }

  //Return total amount paid
  public function getTotalPaidAmount($res){

    $paidAmount = 0 ;
    if(is_array($res)){
        foreach($res['transaction'] as $statusArr){
          foreach($statusArr['action'] as $action){
            if('Purchase' == $action['action_type'] && 1 == $action['success']){
              $paidAmount += $action['amount'] ;
            }
          }
        }
    }    
    return $paidAmount ;
  }

  //Return total amount refunded
  public function getTotalRefundAmount($res){
    $refundAmount = 0 ;
    if(is_array($res)){
        foreach($res['transaction'] as $statusArr){
          foreach($statusArr['action'] as $action){
            if('refund' == $action['action_type'] && 1 == $action['success']){
              $refundAmount += $action['amount'] ;
            }
          }
        }
    }
    return $refundAmount ;
  }

  //Return current balance amount (paid - refund)
  public function getBalanceAmount($res)
  {
    $paidAmount = $this->getTotalPaidAmount($res);

    $refundAmount = $this->getTotalRefundAmount($res);

    $balanceAmount = $paidAmount + $refundAmount ;
    return $balanceAmount ;
  }

  public function storeRollbackRequest($getArr, $type=''){

    if('RefundRequest' == $type){
      $nmiRequest = new EpNmiRequest ();
      $nmiRequest->setGatewayId($this->Gateway_id);
      $nmiRequest->setTransactionId($getArr['transactionid']);
      $nmiRequest->setType($getArr['type']);
      $nmiRequest->setAmount($getArr['amount']);
      $nmiRequest->save();
      return $this->Request_id = $nmiRequest->getId();
    }elseif('RefundResponse' == $type){
      $nmiReponse = new EpNmiResponse ();
      $nmiReponse->setNmiReqId($getArr['nmi_req_id']);
      $nmiReponse->setResult($getArr['result']);
      $nmiReponse->setResultText($getArr['result-text']);
      $nmiReponse->setResultCode($getArr['result-code']);
      $nmiReponse->setActionType('refund');
      $nmiReponse->setAmount($getArr['amount']);
      if(isset($getArr['transaction-id']) && $getArr['transaction-id'])
      $nmiReponse->setTransactionId($getArr['transaction-id']);
      if(isset($getArr['order-id']) && $getArr['order-id'])
      $nmiReponse->setOrderId($getArr['order-id']);
      $nmiReponse->save();
      return $this->Request_id = $nmiReponse->getId();
    }
  }

  private function setRefund($transactionid, $amount, $keepStatusNew){

    $xmlRequest = $this->createRefundXML($transactionid, $amount) ;

    if($keepStatusNew){
        $refundFileName = 'CancelPaymentRefundRequest-'.$transactionid;
        $responseFileName = 'CancelPaymentRefundResponse-'.$transactionid;
    }else{
        $refundFileName = 'RefundRequest-'.$transactionid;
        $responseFileName = 'RefundResponse-'.$transactionid;
    }

    $this->createLogData($xmlRequest, $refundFileName, 'nmiRollbackLog');
    
    $url = $this->getFormRequestUrl();

    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $getArr = array() ;
        $getArr['transactionid'] = $transactionid ;
        $getArr['type'] = 'refund' ;
        $getArr['amount'] = $amount ;
        $requestId = $this->storeRollbackRequest($getArr, 'RefundRequest') ;
    }//End of if(isset($keepStatusNew) && !$keepStatusNew){

    $header[] = "Content-Type: text/xml;charset=UTF-8";

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

    $responseXml = curl_exec($curl);

    $respObj = simplexml_load_string($responseXml);    
    $responseArr = array();
    if(!empty($respObj))
        $responseArr = $this->getArrayfrmXML($respObj, $responseArr) ;

    $this->createLogData($responseXml, $responseFileName, 'nmiRollbackLog');
    
    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $responseArr['nmi_req_id'] = $requestId ;
        $responseArr['amount'] = $amount ;
        $resp = $this->storeRollbackRequest($responseArr, 'RefundResponse') ;
    }//End of if(isset($keepStatusNew) && !$keepStatusNew){...

    if('100' == $responseArr['result-code']){
      return true ;
    }
    return false ;
  }

  public function refund($order_id,$amount, $gatewayId, $keepStatusNew = false){
    // Get current Status
    $orderStatusArr = $this->getStatus($order_id) ;    
    if(empty ($orderStatusArr))
    return false;
    $orderStatus = $this->getPaymentStatus($orderStatusArr) ;
    $amountActual = $this->getBalanceAmount($orderStatusArr) ;
    if($orderStatus){
      if($gatewayId == 10){
        $transaction_number = Doctrine::getTable('EpNmiResponse')->getTransactionId($order_id) ;
      }else{
        return false ;
      }
      if($amountActual >= $amount){

        if($transaction_number){
          switch ($orderStatus){
            case 'complete' :
              if($amount != ''){ 
                  if($this->setRefund($transaction_number,$amount, $keepStatusNew)){
                    return true ;
                  }else{ return false ; }
              }else{  return false ; }
              break ;
            default :
              return false ;
              break ;
          }
        }
      }else{
        return false ;
      }
    }else{
      return false ;
    }
  }

  public function getBrowser() {
    if(!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }

  private function capturePayment($amount ,$tranx_id)
  {
    $api_key = $this->getApiKey();

    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement('capture');
    $root = $doc->appendChild($root);

    $structElm = $doc->createElement("api-key");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($api_key));

    $structElm = $doc->createElement("transaction-id");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($tranx_id));

    $structElm = $doc->createElement("amount");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($amount));

    $xmldata = $doc->saveXML();
    return $xmldata;

  }

  private function voidPayment($tranx_id)
  {
    $api_key = $this->getApiKey();

    $doc = new DomDocument('1.0');
    $doc->formatOutput = true;

    $root = $doc->createElement('void');
    $root = $doc->appendChild($root);

    $structElm = $doc->createElement("api-key");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($api_key));

    $structElm = $doc->createElement("transaction-id");
    $root->appendChild($structElm);
    $structElm->appendChild($doc->createTextNode($tranx_id));

    $xmldata = $doc->saveXML();
    return $xmldata;

  }
  
 public function getStatusFromNmi($orderNo,$amountRequired)
 {
     
     $orderStatusArr = $this->getStatus($orderNo) ;
     if(empty($orderStatusArr))
        return 'Not Found at Back-End';

    $orderStatus = $this->getPaymentStatus($orderStatusArr) ;
    $amountActual = $this->getBalanceAmount($orderStatusArr);
    if(!empty($amountActual))
    {
    if($amountActual == $amountRequired)
    {
         return $orderStatus;
    }
    else{
        return $orderStatus ='Refunded';
      }
    }

  }

}
?>
