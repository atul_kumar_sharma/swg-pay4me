<?php

class EpAuditEventAttributeHolder {
  private $name;
  private $svalue='';
  private $ivalue=0;

  public function EpAuditEventAttributeHolder($name, $svalue, $ivalue) {
    $this->name= $name;
    $this->svalue = $svalue;
    $this->ivalue = $ivalue;
  }

  public function getName() {
    return $this->name;
  }
  public function getSvalue() {
    return $this->svalue;
  }
  public function getIvalue() {
    return $this->ivalue;
  }
}