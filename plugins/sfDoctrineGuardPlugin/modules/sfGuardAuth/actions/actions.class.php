<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once(dirname(__FILE__).'/../lib/BasesfGuardAuthActions.class.php');

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: actions.class.php 23319 2009-10-25 12:22:23Z Kris.Wallsmith $
 */
class sfGuardAuthActions extends BasesfGuardAuthActions
{
  public function executeSignout($request)
  {
    $this->getUser()->signOut();
    $signout_url = '';
//    $this->redirect('' != $signout_url ? $signout_url : '@super_admin_signin');
    $this->redirect('' != $signout_url ? $signout_url : '@sf_guard_signin');
  }

public function executeSignin($request)
  {
    $user = $this->getUser();
    if ($user->isAuthenticated())
    {
      return $this->redirect('@homepage');
    }

    $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
    $this->form = new $class(array(),array(),false);
    if ($request->isXmlHttpRequest())
      {
        $this->getResponse()->setHeaderOnly(true);
        $this->getResponse()->setStatusCode(401);

        return sfView::NONE;
      }
    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter('signin'));
      if ($this->form->isValid())
      {
        $values   = $this->form->getValues();
       	$remember = isset($values['remember']) ? $values['remember'] : false;

       	$this->getUser()->signin($values['user'], $remember);
        $role = AclHelper::userGroupUrl();
        if ($role == "")
        {
          $this->getUser()->setFlash('error','No roles assigned to this user');
          $this->getUser()->signOut();
          $signout_url = sfConfig::get('app_sf_guard_plugin_success_signout_url', $request->getReferer());
          $this->redirect('' != $signout_url ? $signout_url : '@homepage');
        }

        return $this->redirect(AclHelper::userGroupUrl()) ;
      }
    }
    else
    {
      if ($request->isXmlHttpRequest())
      {
        $this->getResponse()->setHeaderOnly(true);
        $this->getResponse()->setStatusCode(401);
        return sfView::NONE;
      }
      $user->setReferer($request->getReferer());
      $module = sfConfig::get('sf_login_module');
      if ($this->getModuleName() != $module){
        return $this->redirect($module.'/'.sfConfig::get('sf_login_action'));
      }
      $this->getResponse()->setStatusCode(401);
    }
  }

 
}
