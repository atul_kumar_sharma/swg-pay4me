<?php

class EpGrayPayManager {
  public $browserInstance ;
  protected $Request_XML;
  protected $Response_String;
  protected $Request_id;
  protected $username;
  protected $password;
  protected $url;
  protected $gateway_id;
  private $midService = 'ps1';

  public function createRequest($paymentDetail, $gateway_id, $isMatch=false, $midService='') {

    $isPrivilegeUser = sfContext::getInstance()->getUser()->isPrivilegeUser();
    $reqType = ($isMatch)? 'auth' : 'sale';
    $month = $paymentDetail['expiry_date']['month'];
    $year = $paymentDetail['expiry_date']['year'];
    $card_expiry = date('my', mktime(0, 0, 0, $month, "1", $year));

    $card_first = substr($paymentDetail['card_Num'], 0, 4);
    $card_last = substr($paymentDetail['card_Num'], -4, 4);

    $card_details = "&ccnumber=".$paymentDetail['card_Num']."&cvv=".$paymentDetail['cvv'];

    $this->gateway_id = $gateway_id;
    $this->midService = $midService;
    if($midService)
    {
      $this->username = sfConfig::get('app_marine_'.$midService.'_username');
      $this->password = sfConfig::get('app_marine_'.$midService.'_password');
      $this->url = sfConfig::get('app_marine_'.$midService.'_url');
    }else{
      $this->username = sfConfig::get('app_marine_ps1_username');
      $this->password = sfConfig::get('app_marine_ps1_password');
      $this->url = sfConfig::get('app_marine_ps1_url');
    }

    $manadatoryString="username=".$this->username."&password=".$this->password."&type=$reqType&orderid=".$paymentDetail['order_number'].$card_details."&ccexp=".$card_expiry."&amount=".$paymentDetail['amount']."&ipaddress=".$paymentDetail['request_ip'] ;
    $user_details = "&firstname=".$paymentDetail['first_name']."&lastname=".$paymentDetail['last_name'];
    $address_details = "&address1=".htmlspecialchars(addslashes($paymentDetail['address1']))."&city=".$paymentDetail['town']."&state=".$paymentDetail['state']."&zip=".$paymentDetail['zip']."&country=".$paymentDetail['country']."&phone=".$paymentDetail['phone']."&email=".$paymentDetail['email'];
    
    $log_card_details = "&ccnumber=".$card_first.$this->getNoOfX(strlen($paymentDetail['card_Num']) - 8).$card_last."&cvv=".$this->getNoOfX(strlen($paymentDetail['cvv']));
    $manadatoryLogString="username=".$this->username."&password=".$this->password."&type=$reqType&orderid=".$paymentDetail['order_number'].$log_card_details."&ccexp=".$card_expiry."&amount=".$paymentDetail['amount']."&ipaddress=".$paymentDetail['request_ip'] ;

    $visaConfig = Doctrine::getTable('GlobalSettings')->getByKey('graypay-vault');
    $autoVaultOn = sfConfig::get('app_auto_vault_on');
    $isVault = false;
    $allow4Auth = true;
    if(1==$autoVaultOn)
    {
      $amount = $paymentDetail['amount'];
      $vaultObj =  new VaultHelper();
      $isVault = $vaultObj->is_processed_with_vault($paymentDetail['card_type'], $amount);

    }

    //vault active for AMEX Cards too
    if(($isVault ) || (('M' == $paymentDetail['card_type'] || 'V' == $paymentDetail['card_type'] || 'A' == $paymentDetail['card_type'])   && (strcmp('active', $visaConfig) == 0)))
    {
      $allow4Auth = false;

      $newVault =  new vault();
      $newVault->setOrderNumber($paymentDetail['order_number']);
      $newVault->setGatewayId($this->gateway_id);
      $newVault->save();


      $vaultObj = Doctrine::getTable('Vault')->findByOrderNumber($paymentDetail['order_number']);

      $firstVaultObj = $vaultObj->getFirst();
      $newvaultNo = $firstVaultObj->getId() + 25 ;
      $firstVaultObj->setVaultNum($newvaultNo);
      $firstVaultObj->save();

      $manadatoryString="username=".$this->username."&password=".$this->password."&orderid=".$paymentDetail['order_number'].$card_details."&ccexp=".$card_expiry."&amount=".$paymentDetail['amount']."&ipaddress=".$paymentDetail['request_ip'] ;
      $manadatoryLogString="username=".$this->username."&password=".$this->password."&orderid=".$paymentDetail['order_number'].$log_card_details."&ccexp=".$card_expiry."&amount=".$paymentDetail['amount']."&ipaddress=".$paymentDetail['request_ip'] ;
      $address_details .= '&customer_vault=add_customer&customer_vault_id='.$newvaultNo;
    }

    $requestString = $manadatoryString.$user_details.$address_details;
    $log_request_string = $manadatoryLogString.$user_details.$address_details;

    $this->createLogData($log_request_string,'RequestString'.$paymentDetail['order_number']);
    $request_id = $this->storeDetail($paymentDetail,'Request');

    $browser = $this->getBrowser() ;
    $browser->post($this->url, $requestString);
    $code = $browser->getResponseCode();

    $formattedResponse = array();
    
    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();
      $respText = $browser->getResponseText();      
      $formattedResponse['response_text'] = $respText;
      $formattedResponse['response'] = $code;
      $formattedResponse['response_code'] = $code;
      $formattedResponse['orderid'] = $paymentDetail['order_number'];
      $formattedResponse['status'] = 0;
      //add job for mail
      $sendMailUrl = "notification/mailOnPaymentCrash" ;
      $taskId = EpjobsContext::getInstance()->addJob('PaymentEmptyMail',$sendMailUrl, array('order_number' => $paymentDetail['order_number'],'responseXML' =>$respText));
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

      return $formattedResponse ;
      // throw new Exception("Error in sending payment request:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
    }

    $respText = $browser->getResponseText();
    $formattedResponse = $this->formatResponse($respText, $request_id,$paymentDetail['order_number']) ;

    
    if(array_key_exists("response",$formattedResponse)) {
      $this->storeDetail($formattedResponse,'Response',$request_id);
      // Send Mail if we receive responsetext as "Error Processing Transaction. Please contact customer service."

      if(strtolower("Error Processing Transaction. Please contact customer service.") == strtolower($formattedResponse['responsetext'])){
        $sendMailUrl = "notification/mailOnPaymentCrash" ;
        //$formattedResponse = array();
        $formattedResponse['response_text'] = $formattedResponse['responsetext'];
        $formattedResponse['response'] = $formattedResponse['response'];
        $formattedResponse['response_code'] = $formattedResponse['response_code'];
        $formattedResponse['orderid'] = $paymentDetail['order_number'];
        $formattedResponse['status'] = 0;
        $taskId = EpjobsContext::getInstance()->addJob('PaymentErrorMail',$sendMailUrl, array('order_number' => $paymentDetail['order_number'],'responseXML' =>$respText));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        return $formattedResponse ;
      }

        if($reqType == 'auth' && $formattedResponse['response_code'] != '100')
        {
              $formattedResponse['response_text'] = 'Payment Auth Fail';
              $formattedResponse['response'] = 0;
              $formattedResponse['response_code'] = $code;
              $formattedResponse['orderid'] = $paymentDetail['order_number'];
              $formattedResponse['status'] = 0;
            return $formattedResponse;
        }
      $formattedResponse['request_id']=$request_id;
      $transactionId = $formattedResponse['transactionid'];
      // Start code: KSingh ----code for removing zip validations
      if($reqType == 'auth' && $isPrivilegeUser && $this->isCardRegistered($formattedResponse['orderid']))
      {
        $capture = true;

        if($capture)
        {
          $captureResponse = $this->capturePayment($paymentDetail, $transactionId);
          parse_str($captureResponse['text'], $output);

          if($captureResponse['code'] == 200 && $formattedResponse['response_code'] == '100'){
            $formattedResponse['response_code'] = $output['response_code'];
            $formattedResponse['response'] = $output['response'];
            if($formattedResponse['response_code'] == '100'){
                $formattedResponse['status'] = 1;
                $formattedResponse['response'] = 1;
            } elseif ($formattedResponse['response_code'] == '410'){ //410
                $formattedResponse['status'] = 1;
                $output['orderid'] = $paymentDetail['order_number'];
                $output['midService'] = $midService;
                $this->proceedPaymentByCron($output, $this->gateway_id, $paymentDetail['amount']);
                $formattedResponse['response'] = 1;
            }else {
                $formattedResponse['status'] = 0;
                $formattedResponse['response'] = 0;
            }            
          }else{
              $formattedResponse['status'] = 0;
          }

          $this->createLogData($captureResponse['text'],'Capture'.$paymentDetail['order_number']);
          $formattedResponse['response_text'] = $output['responsetext'];
        }
      }
      else if($reqType == 'auth' && $allow4Auth)
      {

        $capture = true;


        $isCountryHasZipCheck = CountryBasedZipTable::getInstance()->isCountryHasZipCheck($paymentDetail['country']);
        if($isCountryHasZipCheck) //zip code should match for this country
        {
          $isZipVerified = $this->zipVerification($formattedResponse);
          if($isZipVerified) //zip has been verified
          {
            $capture = true;
          }else{
            $capture = false;
          }
        }

        if($capture)
        {
          $captureResponse = $this->capturePayment($paymentDetail, $transactionId);
          
          parse_str($captureResponse['text'], $output);          
          
          if ($captureResponse['code'] == 200 && $formattedResponse['response_code'] == '100'){            

            $formattedResponse['response_code'] = $output['response_code'];
            $formattedResponse['response'] = $output['response'];

            if($formattedResponse['response_code'] == '100'){
                $formattedResponse['status'] = 1;
                $formattedResponse['response'] = 1;
            }elseif ($formattedResponse['response_code'] == '410'){ //410
                $formattedResponse['status'] = 1;
                $output['orderid'] = $paymentDetail['order_number'];
                $output['midService'] = $midService;
                $this->proceedPaymentByCron($output, $this->gateway_id, $paymentDetail['amount']);                
                $formattedResponse['response'] = 1;
            }else {
                $formattedResponse['status'] = 0;
                $formattedResponse['response'] = 0;
            }
            
          } else {
            $formattedResponse['status'] = 0;
          }
          
          $this->createLogData($captureResponse['text'],'Capture'.$paymentDetail['order_number']);
          $formattedResponse['response_text'] = $output['responsetext'];
        }
        else
        {
          $voidResponse = $this->voidPayment($transactionId);
          $formattedResponse['status'] = 0;
          $this->createLogData($voidResponse['text'],'Void'.$paymentDetail['order_number']);
          $formattedResponse['response_text'] = 'zip not matched';
          $formattedResponse['response'] = 0;
        }
        //        }
      }else{

        if ($formattedResponse['response'] == 1 && $formattedResponse['response_code'] == 100) { //payment done successfully
          $formattedResponse['status'] = 1;
        }
        else {
          $formattedResponse['status'] = 0;
          $formattedResponse['response'] = 0;
        }
        $formattedResponse['response_text'] = $formattedResponse['responsetext'];
        $formattedResponse['request_id']=$request_id;
      }
      return $formattedResponse ;

    }
    else {
      $sendMailUrl = "notification/mailOnPaymentCrash" ;
      $formattedResponse = array();
      $formattedResponse['response_text'] = $respText;
      $formattedResponse['response'] = 0;
      $formattedResponse['response_code'] = "";
      $formattedResponse['orderid'] = $paymentDetail['order_number'];
      $formattedResponse['status'] = 0;
      $taskId = EpjobsContext::getInstance()->addJob('PaymentCrashMail',$sendMailUrl, array('order_number' => $paymentDetail['order_number'],'responseXML' =>$respText));
      sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

      return $formattedResponse ;
    }



  }

  private function zipVerification($formattedResponse)
  {
    $capture = false;
    if(array_key_exists('avsresponse', $formattedResponse))
    {
      switch ($formattedResponse['avsresponse'])
      {
        case 'X':
          $capture = true ;
          break;
        case 'Y':
          $capture = true ;
          break;
        case 'D':
          $capture = true ;
          break;
        case 'M':
          $capture = true ;
          break;
        case 'W':
          $capture = true ;
          break;
        case 'Z':
          $capture = true ;
          break;
        case 'P':
          $capture = true ;
          break;
        case 'L':
          $capture = true ;
          break;
        default:
          $capture = false ;
          break;
      }
    }
    return $capture;
  }
  private function formatResponse($response, $request_id,$order_number) {

    $this->Response_String = $response;
    parse_str($response, $output);
    $this->createLogData($response,'ResponseString'.$order_number);
    return $output ;
  }

  public function getBrowser() {
    if(!$this->browserInstance) {
      $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
    }
    return $this->browserInstance;
  }



  public function storeDetail($getArr,$type='',$request_id='') {
    if('Request' == $type) {


      $card_len = strlen($getArr['card_Num']);
      $card_first = substr($getArr['card_Num'], 0, 4);
      $card_last = substr($getArr['card_Num'], -4, 4);

      $payEasyReponse = new EpGrayPayRequest ();
      $payEasyReponse->setGatewayId($this->gateway_id);
      $payEasyReponse->setTranDate(date('Y-m-d H:i:s'));
      $payEasyReponse->setRequestIp(sfConfig::get('app_pay_easy_request_IP'));
      $payEasyReponse->setAmount($getArr['amount']);
      $payEasyReponse->setCardFirst($card_first);
      $payEasyReponse->setCardLast($card_last);
      $payEasyReponse->setCardLen($card_len);
      $payEasyReponse->setCardType($getArr['card_type']);
      $payEasyReponse->setCardHolder($getArr['card_holder']);
      $payEasyReponse->setExpiryMonth($getArr['expiry_date']['month']);
      $payEasyReponse->setExpiryYear($getArr['expiry_date']['year']);
      $payEasyReponse->setAddress1(htmlspecialchars(addslashes($getArr['address1'])));
      $payEasyReponse->setAddress2($getArr['address2']);
      $payEasyReponse->setCity($getArr['town']);
      $payEasyReponse->setState($getArr['state']);
      $payEasyReponse->setZip($getArr['zip']);
      $payEasyReponse->setCountry($getArr['country']);
      $payEasyReponse->setFirstName($getArr['first_name']);
      $payEasyReponse->setLastName($getArr['last_name']);
      $payEasyReponse->setEmail($getArr['email']);
      $payEasyReponse->setPhone($getArr['phone']);
      $payEasyReponse->setOrderNumber($getArr['order_number']);
      $payEasyReponse->setIpAddress($getArr['request_ip']);
      //  $payEasyReponse->setRequestXml($this->Request_XML);

      $payEasyReponse->save();
      return $this->Request_id = $payEasyReponse->getId();
    }
    if('Response' == $type) {
      $grayPayReponse = new EpGrayPayResponse();
      $grayPayReponse->setGraypayReqId($request_id);
      $grayPayReponse->setResponse($getArr['response']);
      $grayPayReponse->setResponseText($getArr['responsetext']);
      $grayPayReponse->setResponseCode($getArr['response_code']);
      $grayPayReponse->setTransactionid($getArr['transactionid']);
      $grayPayReponse->setAuthcode($getArr['authcode']);
      $grayPayReponse->setType($getArr['type']);
      $grayPayReponse->setOrderid($getArr['orderid']);
      $grayPayReponse->setAvsresponse($getArr['avsresponse']);
      $grayPayReponse->setCvvresponse($getArr['cvvresponse']);
      //$grayPayReponse->setResponseStr($this->Response_String);
      $grayPayReponse->save();
    }
  }



  public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false) {
    $path = $this->getLogPath($parentLogFolder);
    if($appendTime) {
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    }else {
      $file_name = $path."/".$nameFormate.".txt";
    }
    if($append) {
      @file_put_contents($file_name, $xmlData, FILE_APPEND);
    }else {
      $i=1;
      while(file_exists($file_name)) {
        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
        $i++;
      }
      @file_put_contents($file_name, $xmlData);
    }
  }



  public function getLogPath($parentLogFolder) {

    $logPath =$parentLogFolder==''?'/grayPaylog/'.strtoupper($this->midService):'/'.$parentLogFolder.'/'.strtoupper($this->midService);
    $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='') {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }

  public function getNoOfX($length)
  {
    $text = "";
    for($i=0;$i<$length;$i++){
      $text.="x";
    }
    return $text;
  }

  function getArrayfrmXML($xml,$arr){
    $iter = 0;
    foreach($xml->children() as $b){
      $a = $b->getName();
      if(!$b->children()){
        $arr[$a] = trim($b[0]);
      }
      else{
        $arr[$a][$iter] = array();
        $arr[$a][$iter] = $this->getArrayfrmXML($b,$arr[$a][$iter]);
      }
      $iter++;
    }
    return $arr;
  }


  function extractResponse($res){
    parse_str($res, $output);
    return $output ;
  }


  // Get Status of a transaction made at GrayPay
  private function getStatus($order_id, $midService=''){
    if($this->midService){
      $manadatoryString = "username=".sfConfig::get('app_marine_'.$this->midService.'_username')."&password=".sfConfig::get('app_marine_'.$this->midService.'_password') ;
      $url = sfConfig::get('app_marine_'.$this->midService.'_query_url');
    }else{
      $manadatoryString = "username=".sfConfig::get('app_marine_ps1_username')."&password=".sfConfig::get('app_marine_ps1_password') ;
      $url = sfConfig::get('app_marine_ps1_query_url');
    }

    $orderDetail = "&order_id=".$order_id ;
    $requestString = $manadatoryString.$orderDetail;

    $this->createLogData($requestString,'StatusRequest-'.$order_id, 'graypayRollbackLog');

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();
      $respText = $browser->getResponseText();
      return false ;
    }

    $respObj = simplexml_load_string($browser->getResponseText());

    if($respObj){
      $res = $this->getArrayfrmXML($respObj,array()) ;

      return $res ;
      //            return $res['transaction'][0]['condition'] ;
    }
    return false ;

  }

  // Return payment status
  public function getPaymentStatus($res){
    if(isset($res['transaction'][0]['condition'])){
        return $res['transaction'][0]['condition'] ;
    }else{
        return '';
    }    
  }

  //Return total amount paid
  public function getTotalPaidAmount($res){

    $paidAmount = 0 ;
    if(isset($res['transaction'])){
        foreach($res['transaction'] as $statusArr){
          foreach($statusArr['action'] as $action){
            // if('sale' == $action['action_type'] && 1 == $action['success']){
            if(('sale' == $action['action_type'] || 'settle' == $action['action_type'] || 'capture' == $action['action_type']) && 1 == $action['success']){
              $paidAmount += $action['amount'] ;
            }
          }
        }
    }    
    return $paidAmount ;
  }

  //Return total amount refunded
  public function getTotalRefundAmount($res){
    $refundAmount = 0 ;
    if(isset($res['transaction'])){
        foreach($res['transaction'] as $statusArr){
          foreach($statusArr['action'] as $action){
            if('refund' == $action['action_type'] && 1 == $action['success']){
              $refundAmount += $action['amount'] ;
            }
          }
        }
    }    
    return $refundAmount ;
  }

  //Return current balance amount (paid - refund)
  public function getBalanceAmount($res)
  {
    $paidAmount = $this->getTotalPaidAmount($res);

    $refundAmount = $this->getTotalRefundAmount($res);

    $balanceAmount = $paidAmount + $refundAmount ;
    return $balanceAmount ;
  }

    /*
     * author : Anurag
     *
     */

  public function storeRollbackRequest($getArr, $type=''){

    if('RefundRequest' == $type){
      $grayPayReponse = new EpGrayPayRequest ();
      $grayPayReponse->setTranDate(date('Y-m-d H:i:s'));
      $grayPayReponse->setTransactionId($getArr['transactionid']);
      $grayPayReponse->setType($getArr['type']);
      $grayPayReponse->setAmount($getArr['amount']);
      $grayPayReponse->save();
      return $this->Request_id = $grayPayReponse->getId();
    }elseif('RefundResponse' == $type){
      $grayPayReponse = new EpGrayPayResponse ();
      $grayPayReponse->setGraypayReqId($getArr['graypay_req_id']);
      $grayPayReponse->setResponse($getArr['response']);
      $grayPayReponse->setResponseText($getArr['responsetext']);
      $grayPayReponse->setResponseCode($getArr['response_code']);
      $grayPayReponse->setTransactionid($getArr['transactionid']);
      $grayPayReponse->setAuthcode($getArr['authcode']);
      $grayPayReponse->setType($getArr['type']);
      $grayPayReponse->setOrderid($getArr['orderid']);
      $grayPayReponse->setAvsresponse($getArr['avsresponse']);
      $grayPayReponse->setCvvresponse($getArr['cvvresponse']);
      $grayPayReponse->save();
    }elseif('VoidRequest' == $type){
      $grayPayReponse = new EpGrayPayRequest ();
      $grayPayReponse->setTranDate(date('Y-m-d H:i:s'));
      $grayPayReponse->setTransactionId($getArr['transactionid']);
      $grayPayReponse->setType($getArr['type']);
      $grayPayReponse->save();
      return $this->Request_id = $grayPayReponse->getId();
    }elseif('VoidResponse' == $type){
      $grayPayReponse = new EpGrayPayResponse ();
      $grayPayReponse->setGraypayReqId($getArr['graypay_req_id']);
      $grayPayReponse->setResponse($getArr['response']);
      $grayPayReponse->setResponseText($getArr['responsetext']);
      $grayPayReponse->setResponseCode($getArr['response_code']);
      $grayPayReponse->setTransactionid($getArr['transactionid']);
      $grayPayReponse->setAuthcode($getArr['authcode']);
      $grayPayReponse->setType($getArr['type']);
      $grayPayReponse->setOrderid($getArr['orderid']);
      $grayPayReponse->setAvsresponse($getArr['avsresponse']);
      $grayPayReponse->setCvvresponse($getArr['cvvresponse']);
      $grayPayReponse->save();
    }
  }

  private function setRefund($transactionid, $amount, $midService='', $keepStatusNew){
    if($midService){
      $manadatoryString = "username=".sfConfig::get('app_marine_'.$midService.'_username')."&password=".sfConfig::get('app_marine_'.$midService.'_password') ;
      $url = sfConfig::get('app_marine_'.$midService.'_url');
    }else{
      $manadatoryString = "username=".sfConfig::get('app_marine_ps1_username')."&password=".sfConfig::get('app_marine_ps1_password') ;
      $url = sfConfig::get('app_marine_ps1_url');
    }

    $refundString = "&type=refund&transactionid=".$transactionid."&amount=".$amount ;
    $requestString = $manadatoryString.$refundString;

    if($keepStatusNew){
        $refundFileName = 'CancelPaymentRefundRequest-'.$transactionid;
        $responseFileName = 'CancelPaymentRefundResponse-'.$transactionid;
    }else{
        $refundFileName = 'RefundRequest-'.$transactionid;
        $responseFileName = 'RefundResponse-'.$transactionid;
    }

    $this->createLogData($requestString, $refundFileName, 'graypayRollbackLog');

    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $getArr = array() ;
        $getArr['transactionid'] = $transactionid ;
        $getArr['type'] = 'refund' ;
        $getArr['amount'] = $amount ;
        $requestId = $this->storeRollbackRequest($getArr, 'RefundRequest') ;
    }

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();

      return false ;
    }
    $respText = $browser->getResponseText();
    $responseArr = $this->extractResponse($respText) ;

    $this->createLogData($respText, $responseFileName, 'graypayRollbackLog');
    //        echo "<pre>" ;
    //        print_r($responseArr) ;
    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $responseArr['graypay_req_id'] = $requestId ;
        $responseArr = $this->storeRollbackRequest($responseArr, 'RefundResponse') ;
    }

    if('100' == $responseArr['response_code']){
      return true ;
    }
    return false ;
  }

  private function setVoid($transactionid,$midService, $keepStatusNew){

    if($midService){
      $manadatoryString = "username=".sfConfig::get('app_marine_'.$midService.'_username')."&password=".sfConfig::get('app_marine_'.$midService.'_password') ;
      $url = sfConfig::get('app_marine_'.$midService.'_url');
    }else{
      $manadatoryString = "username=".sfConfig::get('app_marine_ps1_username')."&password=".sfConfig::get('app_marine_ps1_password') ;
      $url = sfConfig::get('app_marine_ps1_url');
    }


    //$manadatoryString = "username=".sfConfig::get('app_graypay_username')."&password=".sfConfig::get('app_graypay_password') ;
    $refundString = "&type=void&transactionid=".$transactionid ;
    $requestString = $manadatoryString.$refundString;

    if($keepStatusNew){
        $refundFileName = 'CancelPaymentVoidRequest-'.$transactionid;
        $responseFileName = 'CancelPaymentVoidResponse-'.$transactionid;
    }else{
        $refundFileName = 'VoidRequest-'.$transactionid;
        $responseFileName = 'VoidResponse-'.$transactionid;
    }

    $this->createLogData($requestString, $refundFileName, 'graypayRollbackLog');

    //$url = sfConfig::get('app_graypay_url');
    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $getArr = array() ;
        $getArr['transactionid'] = $transactionid ;
        $getArr['type'] = 'void' ;
        $requestId = $this->storeRollbackRequest($getArr, 'VoidRequest') ;
    }

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();

      return false ;
    }
    $respText = $browser->getResponseText();
    $responseArr = $this->extractResponse($respText) ;
    $this->createLogData($respText, $responseFileName, 'graypayRollbackLog');
    ## This will not work when user cancel transaction from frontend...
    if(isset($keepStatusNew) && !$keepStatusNew){
        $responseArr['graypay_req_id'] = $requestId ;
        $responseArr = $this->storeRollbackRequest($responseArr, 'VoidResponse') ;
    }

    if('100' == $responseArr['response_code']){
      return true ;
    }
    return false ;
  }

  public function refund($order_id,$amount, $gatewayId, $transaction_number='', $midService='', $keepStatusNew = false){

    ## Setting mid service...
    $this->midService = $midService;
    
    // Get current Status
    $orderStatusArr = $this->getStatus($order_id) ;
    if(empty ($orderStatusArr))
    return false;
    $orderStatus = $this->getPaymentStatus($orderStatusArr) ;
    $amountActual = $this->getBalanceAmount($orderStatusArr) ;
    //echo $order_id;
    if($orderStatus){

      $grapayMids = Doctrine::getTable('MidDetails')->findAll() ;
      
      $graypayMidsArray = array();
      $nmiMidsArray = array();
      if(count($grapayMids)){
          foreach($grapayMids AS $grapayMid){
              if($grapayMid['service'] == 'graypay'){
                  $graypayMidsArray[] = $grapayMid['gateway_id'];
              }else{
                  $nmiMidsArray[] = $grapayMid['gateway_id'];
              }
          }
      }     

      
      if(in_array($gatewayId, $graypayMidsArray)){
        if($transaction_number==''){
            $transaction_number = Doctrine::getTable('EpGrayPayResponse')->getTransactionId($order_id) ;
        }
      }else{
          if(in_array($gatewayId, $nmiMidsArray)){
            if($transaction_number==''){
                $transaction_number = Doctrine::getTable('EpNmiResponse')->getTransactionId($order_id);
            }
          }
      }

      
      //echo $amountActual .'>='. $amount;die;
      if($amountActual >= $amount){

        if($transaction_number){

          switch ($orderStatus){
            case 'complete' :
              $this->setRefund($transaction_number,$amount,$midService, $keepStatusNew) ;
              return true ;
              break ;
            case 'pending':
              $this->setVoid($transaction_number, $keepStatusNew) ;
              return true ;
              break ;
            case 'pendingsettlement' :
              $this->setVoid($transaction_number,$midService, $keepStatusNew) ;
              return true ;
              break ;
            default :
              return false ;
              break ;
          }
        }
      }else{
        return false ;
      }
    }else{
      return false ;
    }
  }

  public function processVault($vault_id, $amount,$orderNumber){
    //echo $vault_id, $amount,$orderNumber;die;
    $manadatoryString = "username=".sfConfig::get('app_graypay_username')."&password=".sfConfig::get('app_graypay_password') ;
    $vaultString = "&amount=".$amount."&customer_vault_id=".$vault_id."&orderid=".$orderNumber ;
    $requestString = $manadatoryString.$vaultString;

    $this->createLogData($requestString,'VaultProcessRequest'.$vault_id, 'processVaultLog');

    $url = sfConfig::get('app_graypay_url');

    $requestId = $this->storeBatchProcessingRequest($vault_id, $amount,$orderNumber) ;

    $browser = $this->getBrowser() ;
    $browser->post($url, $requestString);
    $code = $browser->getResponseCode();

    if ($code != 200 ) {
      $respMsg = $browser->getResponseMessage();

      return false ;
    }
    $respText = $browser->getResponseText();
    $responseArr = $this->extractResponse($respText) ;
    $this->createLogData($respText,'VaultResponse-'.$vault_id, 'VaultProcessLog');

    $responseArr['graypay_req_id'] = $requestId ;
    $responseArr['amount'] = $amount ;

    $responseAr = $this->storeBatchProcessingResponse($responseArr) ;

    if(strcmp('100', $responseArr['response_code']) == 0){
      return true ;
    }
    return false ;
  }


  public function storeBatchProcessingRequest($vaultId,$amount,$orderNumber){
    //echo $vaultId,$amount,$orderNumber;die;
    $batchProcesingObj = new BatchProcessRequest();
    $batchProcesingObj->setVaultNum($vaultId);
    $batchProcesingObj->setAmount($amount);
    $batchProcesingObj->setOrderNumber($orderNumber);
    $batchProcesingObj->save();
    return $this->Request_id = $batchProcesingObj->getId();
  }

  public function storeBatchProcessingResponse($responseArr){

    $batchProcesingObj = new BatchProcessResponse();
    $batchProcesingObj->setRequestId($responseArr['graypay_req_id']);
    $batchProcesingObj->setResponse($responseArr['response']);
    $batchProcesingObj->setResponseText($responseArr['responsetext']);
    $batchProcesingObj->setAuthcode($responseArr['authcode']);
    $batchProcesingObj->setTransactionid($responseArr['transactionid']);
    $batchProcesingObj->setType($responseArr['type']);
    $batchProcesingObj->setOrderid($responseArr['orderid']);
    $batchProcesingObj->setAmount($responseArr['amount']);
    $batchProcesingObj->setAvsresponse($responseArr['avsresponse']);
    $batchProcesingObj->setCvvresponse($responseArr['cvvresponse']);
    $batchProcesingObj->setResponseCode($responseArr['response_code']);
    $batchProcesingObj->save();

  }

  public function getStatusFromGrayPay($orderNo,$amountRequired, $midService='')
  {
    $orderStatusArr = $this->getStatus($orderNo,$midService) ;
    if(empty($orderStatusArr))
    {
      return 'Not Found at Back-End';
    }
    else
    {
      $orderStatus = $this->getPaymentStatus($orderStatusArr) ;
      $amountActual = $this->getBalanceAmount($orderStatusArr);
      if(!empty($amountActual))
      {
        if($amountActual == $amountRequired)
        {
          return $orderStatus;
        }
        else{
          return $orderStatus ='Refunded' ;
        }
      }
    }

  }

  private function capturePayment($paymentDetail, $transactionId)
  {
    $manadatoryLogString="username=".$this->username."&password=".$this->password."&type=capture&transactionid=".$transactionId."&amount=".$paymentDetail['amount'];

    $browser = $this->getBrowser() ;
    $browser->post($this->url, $manadatoryLogString);
    $code = $browser->getResponseCode();
    $respText = $browser->getResponseText();
    $responaseArr = array();
    $responaseArr['code'] = $code;
    $responaseArr['text'] = $respText;
    return $responaseArr;
  }

  private function voidPayment($transactionId)
  {
    $manadatoryLogString="username=".$this->username."&password=".$this->password."&type=void&transactionid=".$transactionId ;

    $browser = $this->getBrowser() ;
    $browser->post($this->url, $manadatoryLogString);
    $code = $browser->getResponseCode();
    $respText = $browser->getResponseText();
    $responaseArr = array();
    $responaseArr['code'] = $code;
    $responaseArr['text'] = $respText;
    return $responaseArr;
  }

  private function isCardRegistered($orderNumber)
  {
    $orderObj = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($orderNumber);
    $cardHash = $orderObj->getFirst()->getCardHash();
    $userId = $orderObj->getFirst()->getCreatedBy();

    $applicantVaultValues = Doctrine::getTable('ApplicantVault')->getDeplicateCardDetails($cardHash, $userId);

    //if card is registered return true
    if($applicantVaultValues)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  protected function proceedPaymentByCron($output, $gateway_id, $amount)
  {
    $epGrayPayCaptureData  = new EpGrayPayCaptureData();
    $epGrayPayCaptureData->setGatewayId($gateway_id);
    $epGrayPayCaptureData->setTransactionid($output['transactionid']);
    $epGrayPayCaptureData->setOrderid($output['orderid']);
    $epGrayPayCaptureData->setAmount($amount);
    $epGrayPayCaptureData->setResponseText($output['responsetext']);
    $epGrayPayCaptureData->setResponseCode($output['response_code']);
    $epGrayPayCaptureData->setMidService($output['midService']);
    $epGrayPayCaptureData->save();
    $captureDataId = $epGrayPayCaptureData->getId();

    $proceedUrl = "notification/proceedPaymentByCron" ;
    $starttime = date('Y-m-d H:i:s', strtotime('+5 minutes'));
    $taskId = EpjobsContext::getInstance()->addJob('ProceedPaymentToCaptureByCron',$proceedUrl, array('captureDataId' => $captureDataId), $num_of_retry=-1, $app_name=null, $starttime);
    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
  }
}
?>