<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EpOpenIDExt
{
  public static function getOpenidProvider ($identifierType,$screenName=null) {
    switch (strtoupper($identifierType))
    {
      case sfConfig::get('app_openid_provider_google'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getAuthenticationUrl();
        break;
      case sfConfig::get('app_openid_provider_yahoo'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$yahooConfig)->getAuthenticationUrl();
        break;
      case sfConfig::get('app_openid_provider_aol'):
        if($screenName != null){
          $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$aolConfig)->getAuthenticationUrl($screenName);
        }else{
          throw new Exception("User Name required !");
        }
        break;
      case sfConfig::get('app_openid_provider_pay4me'):
        $openIdUrl = EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$pay4meConfig)->getAuthenticationUrl();
        break;
      default:
        throw new Exception("invalid Identifier type !");
        break;
    }
    try{
      header('Location: ' . $openIdUrl);
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public static function getOpenidProviderDetails ($data,$identfType) {
    switch (strtoupper($identfType))
    {
      case sfConfig::get('app_openid_provider_google'):
        return EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$googleConfig)->getDbDetails($data);
        break;
      case sfConfig::get('app_openid_provider_yahoo'):
        return EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$yahooConfig)->getDbDetails($data);
        break;
      case sfConfig::get('app_openid_provider_aol'):
        return EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$aolConfig)->getDbDetails($data);
        break;
      case sfConfig::get('app_openid_provider_pay4me'):
        return EpOpenIDServiceProviderFactory::getService(EpOpenIDServiceProviderFactory::$pay4meConfig)->getDbDetails($data);
        break;
      default:
        throw new Exception("invalid Identifier type !");
        break;
    }
  }
}