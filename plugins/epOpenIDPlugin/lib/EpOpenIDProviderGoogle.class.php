<?php

class EpOpenIDProviderGoogle extends EpOpenIDProviderIntf {
  public function getAuthenticationUrl($screenName=null) {
    $openIdUrl = sfConfig::get('app_openid_url_google_url');
    return $this->preAuthenticate($openIdUrl);
  }

  public function getDbDetails($data) {
    $map = new DbtoOpendidAttrMap();
    $map[DbtoOpendidAttrMap::$USER_KEY] = $data['openid_identity'];
    $map[DbtoOpendidAttrMap::$FULL_NAME_KEY] = $data['openid_ext1_value_namePerson_first']." ".$data['openid_ext1_value_namePerson_last'];
    $map[DbtoOpendidAttrMap::$EMAIL_KEY]  = $data['openid_ext1_value_contact_email'];
    return $map;
  }
}