<?php

abstract class EpOpenIDProviderIntf {
 abstract public function getAuthenticationUrl($screenName=null);
 abstract public function getDbDetails($data);

  protected function preAuthenticate($openIdUrl) {
    try {
      if(!isset($_GET['openid_mode'])) {
        $openid = new EpOpenIDPlugin;
        $openid->identity = $openIdUrl;
        $openid->required = array('namePerson/first','namePerson','namePerson/friendly', 'contact/email','namePerson/last');
        $openid->optional = array('birthDate', 'person/gender','contact/country/home');
        return $openid->authUrl(true);
      }
    }catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
}