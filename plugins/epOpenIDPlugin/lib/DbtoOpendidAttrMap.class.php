<?php

class DbtoOpendidAttrMap implements ArrayAccess {
  public static $USER_KEY = "username",
    $FULL_NAME_KEY = "fullname",
    $EMAIL_KEY = "email";
  protected $attr = array();

  public function setUserName( $user ) {
    $this->attr[self::$USER_KEY] = $user;
  }

  public function getUserName() {
    $this->attr[self::$USER_KEY] = $user;
  }

  public function setFullName( $user ) {
    $this->attr[self::$FULL_NAME_KEY] = $user;
  }

  public function getFullName() {
    $this->attr[self::$FULL_NAME_KEY] = $user;
  }

  public function setEmail( $user ) {
    $this->attr[self::$EMAIL_KEY] = $user;
  }

  public function getEmail() {
    $this->attr[self::$EMAIL_KEY] = $user;
  }



	/**
	 * @param offset
	 */
	public function offsetExists ($offset) {
    return in_array($offset, $this->attr);
  }

	/**
	 * @param offset
	 */
	public function offsetGet ($offset) {
    return $this->attr[$offset];
  }

	/**
	 * @param offset
	 * @param value
	 */
	public function offsetSet ($offset, $value) {
    $this->attr[$offset] = $value;
  }

	/**
	 * @param offset
	 */
	public function offsetUnset ($offset) {
    unset($this->attr[$offset]);
  }


}