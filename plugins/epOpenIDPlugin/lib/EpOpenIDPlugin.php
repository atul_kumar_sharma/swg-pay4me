<?php
/* The library depends on curl, and requires PHP 5.
 * @author
 * @copyright Copyright (c) 2010, Mewp
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 */
class EpOpenIDPlugin extends LightOpenID
{

    protected function papeParams() {
      $params['openid.ns.pape'] = 'http://specs.openid.net/extensions/pape/1.0';
      $params['openid.pape.max_auth_age'] = 0;
      //$params['openid_auth_age'] = 0;
      //$params['openid.preferred_auth_policies'] = 'http://schemas.openid.net/pape/policies/2007/06/phishing-resistant';
      return $params;
    }

    protected function authUrl_v2($identifier_select)
    {
        $params = array(
            'openid.ns'          => 'http://specs.openid.net/auth/2.0',
            'openid.mode'        => 'checkid_setup',            
            'openid.return_to'   => $this->returnUrl,
            'openid.realm'       => $this->trustRoot,            
            'openid.ui.mode'     => 'popup',            
        );

        ## This will add for google and yahoo...
        ## Also due to query parameter exceeds in IE, need to ignore below line...
        if(sfConfig::get('app_openid_url_pay4me_url') != $this->identity){
            $params['openid.ns.ui'] = 'http://specs.openid.net/extensions/ui/1.0';
            $params['openid.ui.icon'] = 'true';
        }//End of if(sfConfig::get('app_openid_provider_pay4me') != 'PAY4ME'){...            
        
        if($this->ax) {

            $params += $this->axParams();
        } if($this->sreg) {
            $params += $this->sregParams();
        } else {
            # If OP doesn't advertise either SREG, nor AX, let's send them both
            # in worst case we don't get anything in return.
            $params += $this->axParams() + $this->sregParams();
        }
        //$params += $this->papeParams();
        if($identifier_select) {
            $params['openid.identity'] = $params['openid.claimed_id']
                 = 'http://specs.openid.net/auth/2.0/identifier_select';
        } else {
            $params['openid.identity'] = $params['openid.claimed_id'] = $this->identity;
        }
        $retUrl = $this->build_url(parse_url($this->server)
                               , array('query' => http_build_query($params)));
                             return $retUrl;
    }

}
