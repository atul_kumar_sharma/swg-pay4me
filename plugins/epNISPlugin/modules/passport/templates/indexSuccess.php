<h1>Passport List</h1>

<table width='1200' border='1'>
  <thead>
    <tr>
      <th>Id</th>
      <th>Category</th>
      <th>Type</th>
      <th>Ref no</th>
      <th>Title</th>
      <th>First name</th>
      <th>Last name</th>
      <th>Mid name</th>
      <th>Email</th>
      <th>Occupation</th>
      <th>Gender</th>
      <th>Place of birth</th>
      <th>Date of birth</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($passport_application_list as $passport_application): ?>
    <tr>
      <td><a href="<?php echo url_for('passport/show?id='.$passport_application['id']) ?>"><?php echo $passport_application->getid() ?></a></td>
      <td><?php echo $passport_application->getpassportcategory_id() ?></td>
      <td><?php echo $passport_application->getpassporttype_id() ?></td>
      <td><?php echo $passport_application->getref_no() ?></td>
      <td><?php echo $passport_application->gettitle_id() ?></td>
      <td><?php echo $passport_application->getfirst_name() ?></td>
      <td><?php echo $passport_application->getlast_name() ?></td>
      <td><?php echo $passport_application->getmid_name() ?></td>
      <td><?php echo $passport_application->getemail() ?></td>
      <td><?php echo $passport_application->getoccupation() ?></td>
      <td><?php echo $passport_application->getgender_id() ?></td>
      <td><?php echo $passport_application->getplace_of_birth() ?></td>
      <td><?php echo $passport_application->getdate_of_birth() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('pages/index/welcome') ?>">New</a>
