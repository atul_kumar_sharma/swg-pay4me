<div id="multiForm" class="dlForm">
      <h1>Passport Guarantor&#39;s Form</h1>
      <div style="background:#fff;color:#333;padding:10px;">
      <fieldset class='multiForm' style="margin:0px auto;">
      <div class="section">
        <h3 style="margin:5px 0px;text-align:center;">Applicant's Profile Details</h3>
        <table><tr><td style="width:150px;">
            <div style="border:1px solid #333;width:120px;height:140px;vertical-align:middle;text-align:center;"> Guarantor's Passport Photograph

              Duly Signed at the Reverse Side </div>

          </td>
          <td align="left">
          <div>
            <table>

              <tbody>
                <tr>
                  <td width="30%">Full Name:</td>
                  <td width="70%" ><input type="text" class="medium" id="txtFulName" readonly="readonly" value="<?php echo $passport_application[0]['first_name']; ?>" name="txtFulName"/></td>
                </tr>
                <tr>
                  <td >Gender:</td>
                  <td ><input type="text" class="medium" id="txtgender" readonly="readonly" value="<?php echo $passport_application[0]['gender_id']; ?>" name="txtgender"/></td>
                </tr>
                <tr>
                  <td >Date  Of Birth:</td>
                  <td ><input type="text" class="medium" id="txtBirthDate" readonly="readonly" value="<?php echo $passport_application[0]['date_of_birth']; ?>" name="txtBirthDate"/></td>
                </tr>
                <tr>
                  <td >Country Of Origin:</td>
                  <td ><input type="text" class="medium" id="txtCountry" readonly="readonly" value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['Country']['cName']; ?>" name="txtCountry"/></td>
                </tr>
                <tr>
                  <td >State Of Origin:</td>
                  <td ><input type="text" class="medium" id="txtState" readonly="readonly" value="<?php echo $passport_application[0]['sName']; ?>" name="txtState"/></td>
                </tr>
                <tr>
                  <td >Occupation:</td>
                  <td ><input type="text" class="medium" id="txtProfession" readonly="readonly" value="Not Available" name="txtProfession"/></td>
                </tr>
              </tbody>
            </table>
          </div>

        </td></tr>
        </table>
        <div class="pixbr"></div>
      </div>
      <div class="section">
        <div class="highlight"  style="margin:5px 0px;padding:3px 5px;">Guarantor&#39;s are warned that any false declaration on this form will attract serve consequences which may include prosecution</div>
        <div>
          <center>
            (To be furnished by applicants for Nigerian passports)
          </center>
        </div>
      </div>
      <div class="section">
        <div class="highlight" style="margin:5px 0px;padding:3px 5px;">
          <b><center>PART I</center></b>
          Particulars of Guarantor&#39;s  (To be completed in block letters) </div>
          <div>
          <table>
          	<tr>
            	<td>
          Names(s)</td><td width="80%"><div>...........................................................................................................................</div></td>
          </tr>
          <tr><td>
Profession        </td><td width="80%"><div>...........................................................................................................................</div></td>
</tr>
<tr><td>
Business Address</td><td width="80%"><div>...........................................................................................................................</div></td>
</tr>
<tr><td>
Residential Address</td><td width="80%"><div>...........................................................................................................................</div></td>
</tr>
</table>
<center>
(if not related state any other connection)</center>
          </div>
      </div>
      <div class="section">
      <div class="highlight"  style="margin:5px 0px;padding:3px 5px;">Particulars of Asset</div>
      <div>
      	<table>
        	<tr>
          	<td width="35%">(a) Cash Available:</td>
        <td width="65%"><div>...................................................................................................</div></td>
        </tr>
        <tr>
        <td>(b)  Names and Address of Banker(s):</td>
        <td><div>...................................................................................................</div></td>
</tr>
<tr><td colspan="2"><div>.........................................................................................................................................................</div></td>
</tr>
<tr><td>
(c) Houses:</td><td><div>...................................................................................................</div></td></tr>
<tr><td colspan="2"><div>.........................................................................................................................................................</div></td></tr>
<tr><td>
(d) Other Assets:</td><td><div>...................................................................................................</div></td>
</tr>
</table>
      </div>
      </div>
      <div class="section">
        <div class="highlight"  style="margin:5px 0px;padding:3px 5px;">
          <b><center>PART II</center></b>
(To be completed in case of all applicants proceeding overseas except those sponsored by the Federal or State Government or Corporations)</div>
          <div style="line-height:18px;">
         <table width="100%" cellspacing="0" cellpadding="1" border="0" id="Table27">
																<tbody>
                                <tr>
																	<td >I </td>
                                  <td><div>........................................................................................</div></td>

                                  <td><div>......................................................</div></td>
																</tr>
																<tr>
                                	<td></td>
																	<td align="center">(Full Name)</td>                                  
                                  <td align="center">(Nationality)</td>
																</tr>
																<tr>
																	<td >I </td>
                                  <td><div>........................................................................................</div> </td>
                                  
                                  <td><div>......................................................</div> </td>
																</tr>
                                <tr>
																	<td > </td>
                                  <td align="center"> (profession)   </td>
                                 
                                  <td align="center">(full Address)</td>
																</tr>

																<tr>
																	<td colspan="3"  valign="bottom">do hereby solemnly and sincerely
																		declare and say as follows:</td>
																</tr>
																<tr>
																	<td  colspan="3"  valign="bottom">(i) That (name of Applicant)
																		........................................................................is
																		proposing with my full knowledge and consent to
																		proceed to (name of Country)............................... for the purpose of
																		.......................................................................................................</td>
																</tr>
																<tr>
																	<td  colspan="3"  valign="bottom">(ii) That the particulars of asset
																		given in Part I hereof are correct.</td>
																</tr>
																<tr>
																	<td  colspan="3"  valign="bottom">(iii) That i will be responsible for
																		the cost of living, accomodation and repartriation if necessary.</td>
																</tr>
																<tr>
																	<td  colspan="3"  valign="bottom">
																		And I make this solemn declaration conscientiously believing the same to be
																		true  by vitue of the statutory Declaration Act., 1963.
																	</td>
																</tr>
															</tbody></table>
          </div>
      </div>
      </fieldset>
      <div class="pixbr"><center><b>Page 1 of 2</b></center></div>
      <div class="pixbr Y20">
        <center class="noPrint">
          <button onclick="window.location = '<?php echo url_for('passport/PassportGuarantor') ?>' ">Close</button>

          <button onclick="window.print();">Print</button>
          <button onclick="window.location = '<?php echo url_for('passport/ShowGuarantorFormNext?id='.$passport_application[0]['id']) ?>' ">Next</button>
        </center>
      </div>
      </div>
       <div class="pixbr Y20"></div>
    </div>