<script>
  function validateForm()
   {
     if(document.getElementById('passport_app_id').value=='')
       {
         alert('Please insert Passport Application Id.');
         document.getElementById('passport_app_id').focus();
         return false;
       }
     if(document.getElementById('passport_app_refId').value=='')
       {
         alert('Please insert Passport Application Reference No.');
         document.getElementById('passport_app_refId').focus();
         return false;
       }
   }
  </script>

 <?php include_partial('global/innerHeading',array('heading'=>"Edit Passport Application"));?>
<div class="global_content4 clearfix">

 <form name='passportEditForm' action='<?php echo url_for('passport/CheckPassportAppRef');?>' method='post' class="dlForm multiForm">
  <?php if(isset($errMsg)) echo '<div class="error_list">'.$errMsg.'</div>';?>
  <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Application"); ?>      
      <dl>
        <dt><label >Passport Application Id<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='passport_app_id' id='passport_app_id' value='<?php if(isset($_POST['passport_app_id'])) echo $_POST['passport_app_id'];?>' autocomplete='off'>
        </dd>
      </dl>

      <dl>
        <dt><label >Passport Reference No<sup>*</sup>:</label ></dt>
        <dd>
          <input type="text" name='passport_app_refId' id='passport_app_refId'  value='<?php if(isset($_POST['passport_app_refId'])) echo $_POST['passport_app_refId'];?>' autocomplete='off'>
        </dd>
      </dl>
  </fieldset>
  <div class="pixbr XY20"><center>
  <input type='submit' value='Submit' onclick='return validateForm();'>&nbsp;<!--<input type='reset' value='Cancel'>-->
  </center></div>
  <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>
  </form>


</div>