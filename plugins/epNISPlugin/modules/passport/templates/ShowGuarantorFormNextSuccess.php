<pre><?php //print_r($passport_application[0]);?></pre>
<div id="multiForm" class="dlForm">
  <h1>Passport Guarantor&#39;s Form</h1>
  <div style="background:#fff;color:#333;padding:10px;">
    <div class='multiForm' style="margin:0px auto;">

      <div class="section">
        <h3 style="margin:5px 0px;text-align:center;">Applicant's Details</h3>
        <table cellspacing="0" cellpadding="2" border="0" align="center" id="Table171">

          <tbody><tr>
              <td width="35%" >Full Name:</td>
              <td width="65%"><input type="text" class="medium" id="txtFullNameSecond" readonly="readonly" value="<?php echo $passport_application[0]['first_name']; ?> " name="txtFullNameSecond"/></td>
            </tr>
            <tr>
              <td>Gender:</td>
              <td height="19"><input type="text" class="medium" id="txtSexSecond" readonly="readonly" value="<?php echo $passport_application[0]['gender_id']; ?>" name="txtSexSecond"/></td>
            </tr>
            <tr>
              <td>Date  Of Birth:</td>
              <td height="19"><input type="text" class="medium" id="txtDOBSecond" readonly="readonly" value="<?php echo $passport_application[0]['date_of_birth']; ?>" name="txtDOBSecond"/></td>
            </tr>
            <tr>
              <td>Country Of Origin:</td>
              <td height="19"><input type="text" class="medium" id="txtCountryOriginSecond" readonly="readonly" value="<?php echo $passport_application[0]['PassportApplicantContactinfo']['Country']['cName']; ?>" name="txtCountryOriginSecond"/></td>
            </tr>
            <tr>
              <td>State Of Origin:</td>
              <td height="19"><input type="text" class="medium" id="txtStateOriginSecond" readonly="readonly" value="<?php echo $passport_application[0]['sName']; ?>" name="txtStateOriginSecond"/></td>
            </tr>
            <tr>
              <td>Occupation:</td>
              <td height="19"><input type="text" class="medium" id="txtOccupationSecond" readonly="readonly" value="Not Available" name="txtOccupationSecond"/></td>
            </tr>
        </tbody></table>
      </div>

      <div class="section">
        <div class="highlight" style="margin:5px 0px;padding:3px 5px;"><center>
        <b>PART III</b><br />(To be completed in all cases)</center></div>
        <div>
          <table cellspacing="5" cellpadding="2" border="0" id="Table30">
            <tbody><tr>
                <td align="right" style="width: 12px;">WHEREAS</td>
                <td ><input type="text" class="medium" id="txtApplicant" readonly="readonly" value="<?php echo ePortal_displayName($passport_application[0]['first_name'],$passport_application[0]['mid_name'],$passport_application[0]['last_name']); ?>" name="txtApplicant"/></td>
              </tr>
              <tr>
                <td align="right" style="width: 12px;">of</td>
                <td ><input type="text" class="medium" id="txtAddress" readonly="readonly" value="<?php 
                if($street = $passport_application[0]['PassportApplicationDetails']['correspondence_address']) echo $street.", ";
                if($city= $passport_application[0]['PassportApplicationDetails']['city'])echo $city.", ";
                if($state = $passport_application[0]['sName']) echo $state.", ";
                if($country = $passport_application[0]['cName']) echo $country;
                ?>" name="txtAddress"/></td>
              </tr>
              <tr>
              <td colspan="2" style="line-height:15px;">
                (hereinafter
                referred to as "the Applicant") wishes to travel outside Nigeria for the
                purpose of ............................................................................................................................................... ...................................................................................................................................................
                <br />
                AND  WHEREAS the said Applicant has appplied to the Government of the Federal
                Republic of Nigeria(hereinafter referred to as "the Federal Government") for a
                PASSPORT to facilitate this journey. 
              </td>
          </tbody></table>
          <p>
            NOW THEREFORE IN CONSIDERATION of the said PASSPORT by the Federal Government to the said Applicant.
          </p>
          <p  style="line-height:15px;">
            I/WE ............................................................................................................................................
            <br /> of .............................................................................................................................(hereinafter
            referred to as "the Guarantor(s)")
          </p>
        </div>
        <div class="section">
          <p>HEREBY AGREE as follows:
            <ol type="1">
              <li>I/WE hold myself/ourselves responsible for the cost of repatriating the said Applicant to Nigeria; and</li>
              <li><span class="smtextDotted4">I/WE will indemnify the Federal Government against all or any expenses incured
                  by the Federal Government for the eventual repatriation of the said Applicant
              to Nigeria.</span></li>
            </ol>
          </p>
        </div>

        <div class="section X50 Y20">
          <table>
            <tr>
              <td width="40%">&nbsp;</td>
              <td width="20%"></td>
              <td width="40%">........................................................................</td>
            </tr>
            <tr>
              <td>RIGHT THUMB PRINT</td>
              <td></td>
              <td align="center">Signature of Guarantor&#39;s/Declarant</td>
            </tr>
          </table>
        </div>

        <div class="section bdr">
          <div class="highlight"><h2><center>NOTICE OF REQUIREMENTS FOR GUARANTOR&#39;S</center></h2></div>
          <ol type="1">
            <li>One Passport Photogragh of the Guarantor&#39;s.</li>
            <li>Photocopy of the datapage of the Gurantor's Current international Passport.</li>
            <li>Photocopies of the following Guarantor's documents:
              <ol type="a">
                <li>National I.D. Card.</li>
                <li>(b) national Driver's Licence</li>
              </ol>
            </li>

          </ol>
        </div>
      </div>

      <div class="section bdr" >
      <div class="highlight"><h2><center>Commissioner's oath:</center></h2></div>
        <table style="line-height:15px;">
          <tbody>
            <tr>
              
              <td valign="bottom">Sworn to at
              .........................................................................................................</td>
            </tr>
            <tr>
              
              <td valign="bottom">Registry this
                ....................................................day of
              ........................20...............</td>
            </tr>
            <tr>
              
              <td valign="bottom">Before me.</td>
            </tr>
            <tr>              
              <td valign="bottom"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;..................................................................</td>
            </tr>
            <tr>              
              <td valign="bottom"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong>Commisioner for oaths</strong></td>
            </tr>
        </tbody></table>
      
        Note:
        <ol>
          <li>This section must be sworn before either a magistrate, Justice
            of the Peace, aCommisioner for oaths or a Notary Public and guarantor&#39;s should
          note that falsity of this declaration may amount to a criminal offence.</li>
          <li>This Form of undersatanding and indemnity must be presented to
            the commisioner for stamp Duties for stamping within forty days from the Date
          it is sworn to.</li>
        </ol>

      </div>
    </div>
    <div class="pixbr"><center><b>Page 2 of 2</b></center></div>
    <div class="pixbr Y20">
      <center class="noPrint">
      <button onclick="window.location = '<?php echo url_for('passport/PassportGuarantor') ?>' ">Close</button>
        <button onclick="window.print();">Print</button>
        <button onclick="history.back();" >Back </button>
      </center>
    </div>
  </div>
  <div class="pixbr Y20"></div>
</div>