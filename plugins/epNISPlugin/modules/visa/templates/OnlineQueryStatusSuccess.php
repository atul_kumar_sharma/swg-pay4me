<?php use_helper('Form') ?>
<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }

    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }

    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
    <?php /*
    if($("input[name='GatewayType']:checked").val() == 2)
    {
        if(document.getElementById('validation_number').value == '')
        {
          alert('Please insert Validation Number.');
          document.getElementById('validation_number').focus();
          return false;
        }
        if(document.getElementById('validation_number').value != "")
        {
          if(isNaN(document.getElementById('validation_number').value))
          {
            alert('Please insert only numeric value.');
            document.getElementById('validation_number').value = "";
            document.getElementById('validation_number').focus();
            return false;
          }
        }
    }
  */?>
  }
  function ApplyGratis(){ 
    if($('#AppType').val()==1 || $('#AppType').val()==3){
        $('#div_gratis').show();
    }else{
        $('#div_gratis').hide();
        $('#app_gratis').val(1);
        $('#div_gateway').show();
    }
  }
  function ApplyGratisVal(){
    if(parseInt($('#app_gratis').val())==1){
        $('#div_gateway').show();
         if($("input[name='GatewayType']:checked").val() == 2)
          {
           $('#div_validation_number').show();
          }else{
            $('#div_validation_number').hide();
          }

    }else if(parseInt($('#app_gratis').val())==2){
        $('#div_gateway').hide();
        $('#div_validation_number').hide();
    }
  }
  function ApplyValidation(){
    if($("input[name='GatewayType']:checked").val()==2)
    {
        $("#div_validation_number").show();
    }else{
        $("#div_validation_number").hide();
    }
  }
</script>

<h1>Online Query Status</h1>
<div class="multiForm dlForm">
  <form name='visaEditForm' action='<?php echo url_for('visa/OnlineQueryStatusReport');?>' method='post' class="dlForm">
    <div align="center"><font color='red'><?php if(isset($errMsg)) echo $errMsg;?></font></div>
    <fieldset class="bdr">
      <?php echo ePortal_legend("Search for Application"); ?>
      <dl>
        <dt><label>Application <sup>*</sup>:</label></dt>
        <dd><?php

          $Visa_type = (isset($_POST['AppType']))?$_POST['AppType']:"";
//          if($Visa_type == 1)
//          {
//            $vselected = 1;
//          }
//
//          if($Visa_type == 2)
//          {
//            $pselected = 2;
//          }
//
//          if($Visa_type == 3)
//          {
//            $eselected = 3;
//          }
//          if($Visa_type == 4)
//          {
//            $fzselected = 4;
//          }
//          if($Visa_type == 5)
//          {
//            $ecselected = 5;
//          }
//          echo radiobutton_tag ('AppType', '1', $checked = $vselected, $options = array()).'Visa Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '2', $checked = $pselected, $options = array()).'Passport Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '4', $checked = $pselected, $options = array()).'Free Zone Type&nbsp;&nbsp;<br><br>';
//          if(sfConfig::get('app_enable_ecowas'))
//          echo radiobutton_tag ('AppType', '3', $checked = $eselected, $options = array()).'ECOWAS TC Type&nbsp;&nbsp;';
//          echo radiobutton_tag ('AppType', '5', $checked = $ecselected, $options = array()).'ECOWAS RC Type&nbsp;&nbsp;';
          $option =array(0=>'Please Select',1=>'Visa',2=>'Passport',3=>'Free Zone',4=>'ECOWAS Travel Certificate',5=>'ECOWAS Residence Card');
          echo select_tag('AppType', options_for_select($option,$Visa_type), 'onChange="ApplyGratis()"');?>
        </dd>
      </dl>
      <div id="div_gratis">
      <dl>
        <dt><label>Application Type<sup>*</sup>:</label></dt>
        <dd><?php //for visa only
          $app_gratis = (isset($_POST['app_gratis']))?$_POST['app_gratis']:"";
          $option_gratis =array(1=>'Simple',2=>'Gratis');
          echo select_tag('app_gratis', options_for_select($option_gratis,$app_gratis),'onChange="ApplyGratisVal()"'); ?>
        </dd>
      </dl>
      </div>
      <dl>
        <dt><label>Application Id<sup>*</sup>:</label></dt>
        <dd><?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
          echo input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
        </dd>
      </dl>
      <dl>
        <dt><label>Reference No<sup>*</sup>:</label></dt>
        <dd><?php
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
          echo input_tag('visa_app_refId', $reff_id, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
        </dd>
      </dl>
      <?php if(sfConfig::get('app_enable_pay4me_validation')==1){ ?>
      <div id="div_gateway">
      <dl>
        <dt><label>Payment Gateway Used<sup>*</sup>:</label></dt>
        <dd><?php
          $GatewayType = (isset($_POST['GatewayType']))?$_POST['GatewayType']:"";
          $GatewayType1= (($GatewayType==1)?1:0);
          $GatewayType2= (($GatewayType==2)?1:0);
          $GatewayType3= (($GatewayType==3)?1:0);
          $GatewayType4= (($GatewayType==4)?1:0);
          if($GatewayType=='')
          {
              $GatewayType1=1;
          }
          echo radiobutton_tag ('GatewayType', '1', $GatewayType1, $options = array('onClick'=>'$("#div_validation_number").hide()')).'Google&nbsp;&nbsp;';
          echo radiobutton_tag ('GatewayType', '2', $GatewayType2, $options = array('onClick'=>'$("#div_validation_number").show()')).'Pay4Me&nbsp;&nbsp;';
          if(sfConfig::get('app_enable_amazon')){
          echo radiobutton_tag ('GatewayType', '3', $GatewayType3, $options = array('onClick'=>'$("#div_validation_number").hide()')).'Amazon&nbsp;&nbsp;';
          }
          echo radiobutton_tag ('GatewayType', '4', $GatewayType4, $options = array('onClick'=>'$("#div_validation_number").hide()')).'ipay4me&nbsp;&nbsp;';
         ?>
        </dd>
      </dl>
      </div>
      <div id="div_validation_number">
      <dl>
        <dt><label>Validation Number<sup><font color="green"><b>**</b></font></sup>:</label></dt>
        <dd><?php
          $validation_number = (isset($_POST['validation_number']))?$_POST['validation_number']:"";
          echo input_tag('validation_number', $validation_number, array('size' => 20, 'maxlength' => 20,'autocomplete'=>'off')); ?>
        </dd>
      </dl>
      </div>
      <?php } ?>
    </fieldset>
    <div class="pixbr XY20">
      <center id="multiFormNav"><input type='submit' id="multiFormSubmit" value='Search Record' onclick='return validateForm();'>
        &nbsp;<!--<input type='reset' value='Cancel'>-->
      </center>
    </div>
  </form>
</div>
<script>

    ApplyGratis();
    ApplyGratisVal();
    ApplyValidation();
</script>
<?php echo ePortal_highlight('<b>**</b> VALIDATION NUMBER IS REQUIRED IF YOU HAVE DONE PAYMENT IN NAIRA.','NOTE',array('class'=>'green'));?>
