<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
 <div class="content_container">

  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>


 <?php include_partial('global/innerHeading', array('heading' => 'Update VOAP Arrival Details')); ?>

   <div class="clear"></div>
    <div class="tmz-spacer"></div>
 <form action="<?php echo url_for('visaArrival/editFlightDetail') ?>" name="searchApp" id="searchApp" method="post" onsubmit="return validateForm()">
 <table  width="100%">
<tr>
  <td width="20%">Application Id: <span class="redLbl">*</span></td>
  <td>
    <input type="text" name="app_id" id="app_id" class="txt-input">
    <div id="app_id_error" class="red"></div>
  </td>
</tr>
<tr>
    <td width="20%">Reference Number: <span class="redLbl">*</span></td>
    <td><input type="text" name="ref_no" id="ref_no" class="txt-input">
    <div id="ref_no_error" class="red"></div>
    </td>
</tr>
<tr>
    
    <td colspan="2" align="center"><input type="submit" name="Search" id="search" value="Search" class="normalbutton"></td>
</tr>
 </table>
</form>
</div>
 </div>
<div class="content_wrapper_bottom"></div>
<script>

    function validateForm(){

        var appId = jQuery.trim($('#app_id').val());
        var refNo = jQuery.trim($('#ref_no').val());

        var err = 0;
        
        if(appId == ''){
            $('#app_id_error').html('Please enter Application Id.');
            err = err + 1;
        }else{
            if(isNaN(appId)){
                $('#app_id_error').html('Please enter valid Application Id.');
                err = err + 1;
            }else{
                $('#app_id_error').html('');
            }            
        }
        
        if(refNo == ''){
            $('#ref_no_error').html('Please enter Reference Number.');            
            err = err + 1;
        }else{
            if(isNaN(refNo)){
                $('#ref_no_error').html('Please enter valid Reference Number.');
                err = err + 1;
            }else{
                $('#ref_no_error').html('');
            }
        }

        if(err > 0){
            return false;
        }else{
            return true;
        }
        
    }


</script>