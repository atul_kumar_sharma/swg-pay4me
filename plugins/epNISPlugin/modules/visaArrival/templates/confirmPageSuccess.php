<?php 
$countryId = base64_decode($type);
if($countryId != 'KE'){
    $nonKenya = true;
}else{
    $nonKenya = false;;
}
?>
<div class="content_wrapper_top"></div>
<div class="content_wrapper_bg">
 <div class="content_container">

  <?php
  $sf = sfContext::getInstance()->getUser();
  if($sf->hasFlash('error')){ ?>
  <div id="flash_error" class="alertBox" >
    <?php
    echo nl2br($sf->getFlash('error'));
    ?>
  </div>
  <?php }?>
 
   
 <?php include_partial('global/innerHeading', array('heading' => 'VISA ON ARRIVAL PROGRAM')); ?>
  
   <div class="clear"></div>
    <div class="tmz-spacer"></div>
<?php echo form_tag('visaArrival/newVisaApplication?type='.$type,array('name'=>'confirm_page','class'=>'','onsubmit'=>'return validateMultipleCheckboxes("confirm_page")', 'method'=>'post','id'=>'confirm_page')) ?>
<h1>Which travelers may use the Visa On Arrival Program to enter Nigeria?</h1><br/>
      <table width="100%">
          
          <tr>
	 <td> <input type ="checkbox" name = "firstCheckbox" id ="firstCheckbox"></td>
	 <td>They have received an authorization to travel under the VOAP through the Electronic System for Travel Authorization (ESTA).</td>
          </tr>
	<tr>
	<td> <input type ="checkbox" name = "secondCheckbox" id ="secondCheckbox"></td>
	<td>They present the appropriate type of passport valid for six months past their expected stay in Nigeria (unless country-specific agreements provide exemptions). This is a requirement in addition to other passport requirements for all categories of passports -- standard and official - when the traveler is seeking to enter Nigeria for business purpose for a maximum of 90 days.   </td>
	</tr>
	<tr>
        <td> <input type ="checkbox" name = "thirdCheckbox" id ="thirdCheckbox"></td>
	<td>The purpose of their stay in Nigeria is 90 days or less for business (Visitor (B) visa) travel. (If in doubt, travelers should check with the nearest Nigerian Embassy or Consulate to verify that what they plan to do is considered <?php echo ($nonKenya)?'business':'transit and tourist/visitor'?>.) Transit through Nigeria is generally permitted.</td>
	</tr>
	<tr id ="errMsg" style="display:none">
        <td colspan = "2"><span class="red"> Please read and select all checkboxes to confirm if you fulfill all above requirement.</span></td>
    </tr>
         <tr id="result_div1" style="display: none">
             <td colspan="2" height="30" valign="top" style="font-size: 12px;">
                    <div id="result_div" style="display: none;width:500px" align="center">Please Wait...</div></td>
          </tr>
          <tr>
            <td colspan = "2" align ="center" height="30">
                <input type="submit" name="btnSearch" value="Next" class="normalbutton" />
  
              </td>
          </tr>
 </table>
</div>
</div>
<div class="content_wrapper_bottom"></div>



<script>

  function validateMultipleCheckboxes(formId){
    if(formId == undefined){
        var totalCheckbox = $('input:checkbox').length;
    }else{
        var totalCheckbox = $('#'+formId+' input:checkbox').length;
    }
    var checkedChecbox = 0;
    if(totalCheckbox > 0){
        $(":input:checkbox:checked").each(function(){
            checkedChecbox = checkedChecbox + 1;
        })
    }

    if(totalCheckbox != checkedChecbox ){
        $('#errMsg').show();
        return false;
    }else{
        $('#errMsg').hide();
        return true;
    }
}

</script>


