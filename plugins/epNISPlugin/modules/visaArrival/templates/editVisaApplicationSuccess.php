<script>
  function validateForm()
  {
    if(document.getElementById('visa_app_id').value == '')
    {
      alert('Please insert Visa Application Id.');
      document.getElementById('visa_app_id').focus();
      return false;
    }
    if(document.getElementById('visa_app_id').value != "")
    {
      if(isNaN(document.getElementById('visa_app_id').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_id').value = "";
        document.getElementById('visa_app_id').focus();
        return false;
      }

    }
    if(document.getElementById('visa_app_refId').value == '')
    {
      alert('Please insert Visa Application Ref No.');
      document.getElementById('visa_app_refId').focus();
      return false;
    }
    if(document.getElementById('visa_app_refId').value != "")
    {
      if(isNaN(document.getElementById('visa_app_refId').value))
      {
        alert('Please insert only numeric value.');
        document.getElementById('visa_app_refId').value = "";
        document.getElementById('visa_app_refId').focus();
        return false;
      }

    }
  }
</script>

<?php
$sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('error')){ ?>
        <div id="flash_error" class="alertBox" >
            <?php
            echo nl2br($sf->getFlash('error'));
            ?>
        </div>
        <?php }?>
      <div class="content_wrapper_top"></div>
      <div class="content_wrapper_bg">
      <div class="content_container">
      <?php use_helper('Form');

      include_partial('global/innerHeading',array('heading'=>'Edit Visa on Arrival Application'));
      ?>

<div class="clear"></div>
    <div class="tmz-spacer"></div>
  <div class="clearfix"/>
  <form name='visaEditForm' action='<?php echo url_for('visaArrival/CheckVisaAppRef');?>' method='post'>
  <table width="100%">
   <?php
          $app_id = (isset($_POST['visa_app_id']))?$_POST['visa_app_id']:"";
          $reff_id = (isset($_POST['visa_app_refId']))?$_POST['visa_app_refId']:"";
          echo formRowComplete('Application Id<font color="red">*</font>',input_tag('visa_app_id', $app_id, array('size' => 20, 'maxlength' => 20,'class'=>'txt-input')),'','visa_app_id','detail_error','visa_app_id_row');
          echo "</tr>";
          echo formRowComplete('Reference Number<font color="red">*</font>',input_tag('visa_app_refId', $app_id, array('size' => 20, 'maxlength' => 20,'class'=>'txt-input')),'','visa_app_refId','detail_error','visa_app_refId_row');
          echo "</tr>";
         ?>

           <tr valign="top" >
            <td height="30" valign="top">&nbsp;</td>
            <td height="30" valign="top">
                <input type='submit' id="multiFormSubmit" value='Search' class="normalbutton" onclick='return validateForm();'>
              </td>
          </tr>
  </table>
  </form>
 <?php echo ePortal_highlight('PLEASE ENSURE THAT ALL INFORMATION PROVIDED ON YOUR APPLICATION IS CORRECT BEFORE YOU PROCEED TO PAYMENT.  YOUR APPLICATION CANNOT BE EDITED ONCE PAYMENT IS INITIATED.','WARNING',array('class'=>'yellow'));?>

</div>
</div>
