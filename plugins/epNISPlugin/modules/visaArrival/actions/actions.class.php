<?php

/**
 * passport actions.
 * @package    nisng
 * @subpackage passport
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class visaArrivalActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->redirect('@homepage');
    }

    public function executeConfirmPage(sfWebRequest $request) {
        //$request->setParameter('type',$request->getParameter('type'));
        $this->type = $request->getParameter('type');
        

        /**
         * [WP: 104] => CR: 149
         * Setting flag true for jna service to change address defined in bottom of the page...
         */
        $this->countryId = base64_decode($this->type);
        $sess_jna_processing = Functions::setFooterAddressFlag($this->countryId);
        $this->getUser()->setAttribute('sess_jna_processing', $sess_jna_processing);

    }

    public function executeNewVisaApplication(sfWebRequest $request) {

        $appId = $request->getPostParameter('appId');
        $countryId = $request->getPostParameter('countryId');

        $businessAddress = $request->getPostParameter('vap_application[business_address]');
        $this->businessAddress = $businessAddress;
        $applicantType = $request->getPostParameter('vap_application[applicant_type]');
        $this->applicantType = $applicantType;

        if($appId != '' && $countryId != ''){
            $request->setParameter('id', $appId);
            $this->forward('visaArrival','edit');
        }else{
            $request->setParameter('countryId',base64_decode($request->getParameter('type')));
            $this->countryId = base64_decode($request->getParameter('type'));


            $this->form = new VisaArrivalForm(null,array('applying_country_id'=>$this->countryId));

            if($request->hasParameter("type")){
                $this->countryId = base64_decode($request->getParameter('type'));
            }else{
                $this->forward404Unless($request->hasParameter("type"));
            }

            ## Prefield email and phone number fields
            $loggedUserDetailsObj = $this->getUser()->getGuardUser()->getUserDetail();
            $email = $loggedUserDetailsObj->getEmail();
            $phoneNo = $loggedUserDetailsObj->getMobilePhone();
            $this->form->setDefault('perm_phone_no', $phoneNo);
            $this->form->setDefault('email', $email);
        }
    }
    /**
     *
     * @param <type> $request
     * @return <type>
     * This function return html showing in popup window...
     */
    public function executeNewVisaApplicationAjax(sfWebRequest $request)
    {
        $first_name = trim($request->getPostParameter('vap_application[first_name]'));
        $last_name = trim($request->getPostParameter('vap_application[surname]'));
        $gender_id = trim($request->getPostParameter('vap_application[gender]'));

        $day = trim($request->getPostParameter('vap_application[date_of_birth][day]'));
        $month = trim($request->getPostParameter('vap_application[date_of_birth][month]'));
        $year = trim($request->getPostParameter('vap_application[date_of_birth][year]'));

        $date_of_birth = date("Y-m-d H:i:s", mktime(0,0,0,date($month), date($day), date($year)));
        $place_of_birth = trim($request->getPostParameter('vap_application[place_of_birth]'));
        $email = trim($request->getPostParameter('vap_application[email]'));

        if($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != ''  && $place_of_birth != '' && $email != ''){
            $data = Doctrine::getTable('VapApplication')->checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email);
            if(is_array($data)){
                if(count($data) > 0){

                    $this->appDetails = $data[0];
                    
                    $this->isProcessMO = "no";
                    $this->status = strtolower($data[0]['status']);
                    $this->appId = $data[0]['appId'];
                    $this->act = 'found';
                    $this->ref_no = $data[0]['ref_no'];
                    $this->remarks = $data[0]['remarks'];

                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $this->printUrl = url_for("visaArrival/printVisaApp?id=".SecureQueryString::ENCODE(SecureQueryString::ENCRYPT_DECRYPT($data[0]['appId'])));
                    
                    ## Below code checks weather application is associated with money order or not...
                    if($data[0]['status'] == 'New'){
                        $cartValidation = new CartValidation();
                        $processMO = $cartValidation->isAppProcessed($data[0]['appId'], 'vap'); // vap means visa on arrival program...
                        if($processMO){
                            $this->isProcessMO = "yes";
                        }
                        $paymentHelper = new paymentHelper();
                        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($data[0]['appId'], 'NIS VISA ARRIVAL');
                        if(!$paymentRequestStatus){
                            $this->isAppProcess = "yes";
                        }else{
                            $this->isAppProcess = "no";
                        }
                    }                    
                }else{                    
                    return $this->renderText('notfound');
                }
            }else{                
                return $this->renderText('error');
            }
        }else{
            return $this->renderText('error');
        }
    }

    /**
   * @menu.description:: Apply for Visa
   * @menu.text::Apply for Visa
   */
    public function executeNewvisa(sfWebRequest $request)
    {
        $request->setParameter('countryId',base64_decode($request->getParameter('type')));
        $formType = $request->getParameter('formType');
        $this->countryId = base64_decode($request->getParameter('type'));

        $this->form = new VisaArrivalForm(null,array('applying_country_id'=>$this->countryId));

        if($request->hasParameter("type")){
            $this->countryId = base64_decode($request->getParameter('type'));
        }else{
            $this->forward404Unless($request->hasParameter("type"));
        }
        $business_address = trim($request->getPostParameter('vap_application[business_address]'));
        $applicant_type = trim($request->getPostParameter('vap_application[applicant_type]'));        
        $this->form->setDefault('applicant_type', $applicant_type);
        $this->form->setDefault('business_address', $business_address);        
        $this->applicantType = $applicant_type;
        $this->sponsore_type = '';
        
        if($formType == '') {

            ## prefield email and phone number fields
            $loggedUserDetailsObj = $this->getUser()->getGuardUser()->getUserDetail();
            $email = $loggedUserDetailsObj->getEmail();
            $phoneNo = $loggedUserDetailsObj->getMobilePhone();
            $this->form->setDefault('perm_phone_no', $phoneNo);
            $this->form->setDefault('email', $email);

            /*####  Start setting default values into the form coming last page...   ####*/
            $first_name = trim($request->getPostParameter('vap_application[first_name]'));
            $this->form->setDefault('first_name', $first_name);
            $mid_name = trim($request->getPostParameter('vap_application[middle_name]'));
            $this->form->setDefault('middle_name', $mid_name);
            $last_name = trim($request->getPostParameter('vap_application[surname]'));
            $this->form->setDefault('surname', $last_name);
            $gender_id = trim($request->getPostParameter('vap_application[gender]'));
            $this->form->setDefault('gender', array('default',$gender_id) );

            $day = trim($request->getPostParameter('vap_application[date_of_birth][day]'));
            $month = trim($request->getPostParameter('vap_application[date_of_birth][month]'));
            $year = trim($request->getPostParameter('vap_application[date_of_birth][year]'));
            $date_of_birth = date("Y-m-d H:i:s", @mktime(0,0,0,date($month), date($day), date($year)));
            $this->form->setDefault('date_of_birth', array('day' => $day, 'month' => $month, 'year' => $year));

            $place_of_birth = trim($request->getPostParameter('vap_application[place_of_birth]'));
            $this->form->setDefault('place_of_birth', $place_of_birth);
            $email = trim($request->getPostParameter('vap_application[email]'));
            $this->form->setDefault('email', $email);

            $vap_company_id = trim($request->getPostParameter('vap_application[vap_company_id]'));
            $this->form->setDefault('vap_company_id', $vap_company_id);
            $customer_service_number = trim($request->getPostParameter('vap_application[customer_service_number]'));
            $this->form->setDefault('customer_service_number', $customer_service_number);
            //$this->form->setDefault('present_nationality_id', $this->countryId);
            $this->form->setDefault('applying_country_id', $this->countryId);
            /*####  End setting default values into the form coming last page...   ####*/
            
        }//End of if($formType == '') {...
    }
    /**
     *
     * @param <type> $request
     * This functions is being used for adding visa arrival...
     */
    public function executeCreate(sfWebRequest $request)
    {
        if($request->hasParameter('countryId')) {
            $applying_country_id = $request->getParameter('countryId');
        }else{
            $applying_country_id = '';
        }        
        $this->lga_id = $request->getPostParameter('vap_application[VapLocalComanyAddressForm][lga_id]');
        $this->countryId = $applying_country_id;
        $this->forward404Unless($request->isMethod('post'));
        $this->form = new VisaArrivalForm(null,array('applying_country_id'=>$applying_country_id));
        $this->processForm($request, $this->form);
        
        $this->sponsore_type = trim($request->getPostParameter('vap_application[sponsore_type]'));
        $this->applicantType = trim($request->getPostParameter('vap_application[applicant_type]'));
        
        $this->setTemplate('newvisa');
    }

    /**
     *
     * @param <type> $request
     * This functions is being used for editing visa arrival...
     */
    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($visa_application = Doctrine::getTable('VapApplication')->find(array($request->getParameter('id'))), sprintf('Object visa_application does not exist (%s).', array($request->getParameter('id'))));

        $applying_country_id =  $visa_application->getApplyingCountryId();
        $this->countryId = $applying_country_id;
        $localCompanyAdressId =  $visa_application->getLocalCompanyAddressId();
        $this->lga_id = '';
        if(isset($localCompanyAdressId)){
            $addressObj = Doctrine::getTable('AddressMaster')->find($localCompanyAdressId);
            $this->lga_id = $addressObj->getLgaId();
        }
        
        $encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($request->getParameter('id'));
        $encriptedAppId = SecureQueryString::ENCODE($encriptedAppId);
        $this->setVar('encriptedAppId',$encriptedAppId);

        $mid_name = trim($request->getPostParameter('vap_application[middle_name]'));
        
        
        $this->applicantType = $visa_application['applicant_type'];
        $this->sponsore_type = $visa_application['sponsore_type'];
        
        $application_created_at =  date('Y-m-d H:i:s'); //$visa_application->getCreatedAt();
        $this->form = new VisaArrivalForm($visa_application,array('applying_country_id'=>$applying_country_id, 'application_created_at' => $application_created_at));

        if(!empty ($mid_name)){
            $this->form->setDefault('middle_name', $mid_name);
        }
    }

   /**
     *
     * @param <type> $request
     * This functions is being used for updating visa arrival...
     */

    public function executeUpdate(sfWebRequest $request)
    {
        if($request->hasParameter('countryId')) {
            $applying_country_id = $request->getParameter('countryId');
        }else{
            $applying_country_id = '';
        }

        //$business_address = trim($request->getPostParameter('vap_application[business_address]'));
        $this->applicantType = trim($request->getPostParameter('vap_application[applicant_type]'));
        $this->sponsore_type = trim($request->getPostParameter('vap_application[sponsore_type]'));

        $this->countryId = $applying_country_id;
        $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->forward404Unless($visa_application = Doctrine::getTable('VapApplication')->find(array($getAppId)), sprintf('Object visa_application does not exist (%s).', array($getAppId)));

        $localCompanyAdressId =  $visa_application->getLocalCompanyAddressId();
        $this->lga_id = '';
        if(isset($localCompanyAdressId)){
            $addressObj = Doctrine::getTable('AddressMaster')->find($localCompanyAdressId);
            $this->lga_id = $addressObj->getLgaId();
        }

        $param = $request->getParameterHolder()->getAll();
        $application_created_at =  date('Y-m-d H:i:s'); //$visa_application->getCreatedAt();
        $arrival_date_in_db = $visa_application->getArrivalDate();
        $day = trim($request->getPostParameter('vap_application[arrival_date][day]'));
        $month = trim($request->getPostParameter('vap_application[arrival_date][month]'));
        $year = trim($request->getPostParameter('vap_application[arrival_date][year]'));
        $arrival_date_in_form = date("Y-m-d H:i:s", @mktime(0,0,0,date($month), date($day), date($year)));

        ## Fetiching currend date...
        $curr_date = strtotime(date('Y-m-d'));
        ## Creating arrival date validation flag...
        if($curr_date == strtotime($arrival_date_in_db)){
            if(strtotime($arrival_date_in_db) == strtotime($arrival_date_in_form)){
                $arrival_date_validation = false;
            }else{
                $arrival_date_validation = true;
            }
        }else{
            $arrival_date_validation = true;
        }
                
        $this->form = new VisaArrivalForm($visa_application,array('applying_country_id'=>$applying_country_id, 'application_created_at' => $application_created_at, 'arrival_date_validation' => $arrival_date_validation));
                
        $this->processForm($request, $this->form);
        
        $this->setVar('encriptedAppId',$request->getParameter('id'));
        
        $this->setTemplate('edit');
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {

        //if user has not payment rights then  he can not summit NIS application
        if($this->getUser()->getGuardUser()->getUserDetail()->getPaymentRights()){
            $this->redirect("paymentProcess/blockUserMsg");
        }
        $requestParam = $request->getParameter($form->getName());
        if($form->getObject()->getIsEmailValid()){
            if(isset($requestParam['id']) && $requestParam['id']!=''){
                $requestParam['email'] = $form->getObject()->getEmail();
            }
        }

        ## binding form fields and file tag as well...
        $form->bind($requestParam,$request->getFiles($form->getName()));

        if ($form->isValid())
        {
            $applying_country_id = $requestParam['applying_country_id'];

            /**
             * [WP: 089] => CR: 128
             * Setting flag for non kenya or kenya country...
             */
            if($applying_country_id != 'KE'){
                $nonKenya = true;
            }else{
                $nonKenya = false;
            }

            $isNew = $form->getObject()->isNew();
            $arrForVerification = array();
            $arrForVerification['fname'] = $requestParam['first_name'];
            $arrForVerification['mname'] = $requestParam['middle_name'];
            $arrForVerification['lname'] = $requestParam['surname'];
            $arrForVerification['dob'] = $requestParam['date_of_birth'];
            if(TblBlockApplicantTable::getInstance()->isApplicantBlocked($arrForVerification))
            {
                $this->getUser()->setFlash('error',"You are blocked for applying a Visa on Arrival Program on this portal.");
                return ;
            }
            $type = 'vap';
            if($form->getObject()->isNew()){
                $visa_application = $form->save();

//                $visa_application['type_of_visa'] = $requestParam['type_of_visa'];
//                $visa_application = $form->save();
                        
                //call new visa application listener
                $id = "";
                $id = (int)$visa_application['id'];

                if($nonKenya){
                    $path = '/vap_document';
                    $final_path = sfConfig::get('sf_upload_dir') . $path;

                    if(is_dir($final_path)=='')
                    {
                      $dir_path=$final_path."/";
                      mkdir($dir_path,0777,true);
                      chmod($dir_path, 0777);
                    }

                    //upload files in folder
                    $files = $request->getFiles($form->getName());
                    for($i=1;$i<=2;$i++){
                        $document_name = '';
                        if($files['document_'.$i]['tmp_name'] !=''){
                            $document_name = "document_".$i;

                            //chmod($final_path, 0777);
                            $tmpfileName = $files['document_'.$i]['tmp_name'];
                            $file_name = time() .$i. '_' . str_replace(' ','',$files['document_'.$i]['name']);
                            $path_to_upload = $final_path . '/'.$file_name;
                            $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                            chmod($path_to_upload, 0755);
                            //update file names in DB
                            $visa_application->set($document_name, $file_name);
                            $visa_application->save();
                        }
                    }
                }

                ## Application will be rejected if atleast one 'Yes' exists...
                if($requestParam['contagious_disease'] == 'Yes' || $requestParam['police_case'] == 'Yes' || $requestParam['narcotic_involvement'] == 'Yes'){
                    $visa_application->setStatus('Rejected');
                    $visa_application->save();
                }

                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $sitePath = $host . $url_root;

                $mailUrl = "notification/appSubmissionMail" ;
                $mailTaskId = EpjobsContext::getInstance()->addJob('NisApplicationMail',$mailUrl, array('app_id'=>$id, 'app_type'=>$type, 'user_id'=>$this->getUser()->getGuardUser()->getId(), 'sitePath' => $sitePath));

            }
            else
            {
                $form_data = $request->getParameter($form->getName());
                //$app_id = $form_data['id'];
                $current_application_status = Doctrine::getTable('VapApplication')->find($form_data['id']);

                $is_paid = $current_application_status->getStatus();
                if($is_paid == 'New')
                {     //find if any payment request create
                    $paymentHelper = new paymentHelper();
                    $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($form_data['id'], 'NIS VISA ARRIVAL');
                    $paymentRequestStatus = true; //Added by ashwani ...
                    if(!$paymentRequestStatus)
                    {
                        $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
                        $this->redirect('visaArrival/editVisaApplication');
                    }
                    else
                    {
                        $visa_application = $form->save();
//                        $visa_application['type_of_visa'] = $requestParam['type_of_visa'];
//                        $visa_application = $form->save();
                        
                        $id = "";
                        $id = (int)$visa_application['id'];

                        if($nonKenya){
                            $path = '/vap_document';
                            $final_path = sfConfig::get('sf_upload_dir') . $path;
                            $dir_path=$final_path."/";
                            if(is_dir($final_path)=='')
                            {
                              mkdir($dir_path,0777,true);
                              chmod($dir_path, 0777);
                            }

                            //upload files in folder
                            $files = $request->getFiles($form->getName());
                            for($i=1;$i<=2;$i++){
                                $document_name = '';
                                if($files['document_'.$i]['tmp_name'] !=''){
                                    $document_name = "document_".$i;

                                    $tmpfileName = $files['document_'.$i]['tmp_name'];
                                    $file_name = time() .$i. '_' . str_replace(' ','',$files['document_'.$i]['name']);
                                    $path_to_upload = $final_path .'/'. $file_name;
                                    $upload = move_uploaded_file($tmpfileName, $path_to_upload);
                                    chmod($path_to_upload, 0755);

                                    //update file names in DB
                                    $visa_application->set($document_name, $file_name);
                                    $visa_application->save();

                                    //if(file_exists($dir_path.$current_application_status[$document_name])){
                                        //echo $dir_path.$current_application_status[$document_name];

                                        //unlink($dir_path.$current_application_status[$document_name]);
                                    //}

                                }
                            }
                        }//End of if($applying_country_id != 'KE'){...

                        ## Application will be rejected if atleast one 'Yes' exists...
                        if($form_data['contagious_disease'] == 'Yes' || $form_data['police_case'] == 'Yes' || $form_data['narcotic_involvement'] == 'Yes'){
                            $visa_application->setStatus('Rejected');
                            $visa_application->save();
                        }

                    }
                }else
                {
                    if($is_paid != 'Rejected'){
                        $this->getUser()->setFlash('error',"Your application is already in process. This application cann't be change.",true);
                    }
                    $id = "";
                    $id = $form_data['id'];
                }
            }
            // incrept application id for security//
            $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($id);
            $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
            $show_popup = 'i';
            if($isNew)
            $show_popup = 'n';
            $this->redirect('visaArrival/printVisaApp?chk=1&p='.$show_popup.'&id='.$this->encriptedAppId);
        }
    }

    public function executePrintVisaApp(sfWebRequest $request){
        $this->chk = $request->getParameter('chk');
        $this->mode = $request->getParameter('mode');
        if($this->mode){
            $this->setLayout('popupLayout');
        }
        $this->p = $request->getParameter('p');
        $this->idParam = $request->getParameter('id');
        $idParam = $request->getParameter('id');
        $id_arr=explode('||',$request->getParameter('id'));
        $getAppId = SecureQueryString::DECODE($id_arr[0]);
        // $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);

        $arrivalObj = Doctrine::getTable('VapApplication')->find($getAppId);;
        if(!empty($arrivalObj)){
              $this->visa_application = Doctrine::getTable('VapApplication')->getVisaFreshRecordSummery($getAppId);
        }else{
              $this->getUser()->setFlash('notice','Tempering URL is not allowed!');
              $this->redirect('cart/list#msg');
        }
        
        if($this->visa_application[0]['status'] == 'New'){
            $errorMsgObj = new ErrorMsg();
            $cartObj = new cartManager();
            $status = $cartObj->addToCart('vap', $getAppId);
            $cartValidationObj = new CartValidation();

            //start:Kuldeep for check cart capacity
            $arrCartCapacity = array();
            $arrCartCapacityLimit =$cartValidationObj->checkUserCartCapacity();
            if(!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)){
                $arrCartCapacity = $arrCartCapacityLimit;
            }
            //end:Kuldeep
            if($status == "capacity_full"){
                if($arrCartCapacity['cart_capacity']>1){
                    $msg = $arrCartCapacity['cart_capacity']." applications";
                }else{
                    $msg = $arrCartCapacity['cart_capacity']." application";
                }
                $this->getUser()->setFlash("notice", "Cart Capacity has been full, you can add maximum upto ".$msg." in cart",true);
            }else if($status == "amount_capacity_full"){
                $confVal = $arrCartCapacity['cart_amount_capacity'] ;
                $msg = ($confVal > $cartObj->getCartCurrValue())?$confVal:$cartObj->getCartCurrValue() ;
                $this->getUser()->setFlash("notice", sprintf("Cart Capacity has been full, total value of cart can not be above $".$msg.""));
            }else if($status == "common_opt_not_available"){
                $this->getUser()->setFlash("notice", sfConfig::get("app_cart_err_message_save"),true);
            }
            else if($status == "application_edit"){
                $this->getUser()->setFlash("notice", 'Your visa application has been changed .Please remove it from the cart and add it again',true);
            }else if ($status == "application_rejected") {
                $errorMsg = $errorMsgObj->displayErrorMessage("E060", '001000', array("msg" => ''));
                $this->getUser()->setFlash("notice", $errorMsg);
            }else if($status == "only_vap_allowed"){                
                $msg = 'Visa on Arrival application can not be added into the cart with another type of application such as passport, visa, .. etc.';
                $this->getUser()->setFlash('notice', sprintf($msg));
            }
        }

        //get info about travel history
        //$this->applicantTravelHistory = Doctrine::getTable('VisaApplicantTravelHistory')->getTravelHistory($getAppId);
        //get info about State the period of previous visits to Nigeria
        //$this->applicantPreviousNigeriaHistory = Doctrine::getTable('VisaApplicantPreviousHistory')->getPreviousHistory($getAppId);
        // echo '<pre>';print_r($this->visa_application);echo '<pre>';print_r($this->applicantTravelHistory);die;
    }

    public function executeShow(sfWebRequest $request)
    {
        $idParam = $request->getParameter('id');
        $id_arr=explode('||',$request->getParameter('id'));
        if(isset($id_arr[1]) && $id_arr[1]!=NULL){
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($id_arr[1]));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($id_arr[2]));
        }else{
            $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('ErrorPage')));
            $GatewayType = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('GatewayType')));
        }

        $this->show_payment=0;
        $this->show_details=1;
        if(isset($GatewayType) && $errorPage == 1 && (sfConfig::get('app_enable_pay4me_validation')))
        {  //viewing payment status
            $this->show_payment=1;
            $this->show_details=0;
        }
        if(sfConfig::get('app_enable_pay4me_validation')==0)
        {
            $this->show_payment=1;
            $this->show_details=1;
        }

        $this->chk = $request->getParameter('chk');
        $this->statusType = $request->getParameter('reportType');
        $errorPage = SecureQueryString::ENCRYPT_DECRYPT(SecureQueryString::DECODE($request->getParameter('id')));
        $GatewayType = $request->getParameter('GatewayType');

        $getAppId = SecureQueryString::DECODE($id_arr[0]);

        $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
        $visa_application = $this->getVisaFreshRecord($getAppId);

        $applyingCountryId = $visa_application[0]['applying_country_id'];
        //   check processing country payment gateway
        $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions($applyingCountryId);
        if($paymentOptions === 0){
            $paymentOptions = Doctrine::getTable('CountryPaymentMode')->getPaymentOptions('ALL');
        }
        $msgFlag = false;
        if($paymentOptions){
            $arrPaymentOption = explode(',',$paymentOptions);
            $arrPaymentOptionCount = count($arrPaymentOption);
            if($arrPaymentOptionCount <= 2){
                for($i=0;$i<$arrPaymentOptionCount;$i++){
                    if($arrPaymentOption[$i] == 'Wt' || $arrPaymentOption[$i] == 'Mo'){
                        $msgFlag = true;
                    }else{
                        $msgFlag = false;
                        break;
                    }
                }//End of for($i=0;$i<$arrPaymentOptionCount;$i++){...
            }//End of if($arrPaymentOptionCount <=2){...
        }//End of if($paymentOptions){...

        $this->msgFlag = $msgFlag;
        //end code
        $this->visa_application = $visa_application;
        $this->forward404Unless($this->visa_application);

        $country_id = $visa_application[0]['present_nationality_id'];
        $visa_count_id = $visa_application[0]['applying_country_id'];
        $visa_type_id = $visa_application[0]['type_of_visa'];

        $payment_details = Doctrine::getTable('VisaFee')->getVisaArrivalFee($country_id, $visa_type_id);

        $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);


        if($visa_application[0]['ispaid'] != 1){            
            // This condition is for gratis application 
            if ( /* If the naira amount is not set - means it doesn't exists */
                ((!isset($payment_details['naira_amount'])) ||
        /* OR if it exists but is zero */
                    (!$payment_details['naira_amount'])) && /* AND the dollar condition and naira conditions*/
        /* If the dollar amount is not set - means it doesn't exists*/
                ((!isset($payment_details['dollar_amount'])) ||
        /* OR if dollar amount is zero */
                    (!$payment_details['dollar_amount']))) {


                ## Remove the application from cart; this is a gratis one
                $type = "Vap";
                $cartObj = new cartManager();
                $cartObj->removeCart($getAppId, $type);

                // update payment status and amount of gratis application
                $this->notifyNoAmountPayment($visa_application[0]['id']);
                $appId = $visa_application[0]['id'];
                $refNo = $visa_application[0]['ref_no'];

                $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($appId);
                $VisaAppID = SecureQueryString::ENCODE($VisaAppID);
                $VisaRefNo = SecureQueryString::ENCRYPT_DECRYPT($refNo);
                $VisaRefNo = SecureQueryString::ENCODE($VisaRefNo);

                $this->redirect(sfConfig::get("app_nis_portal_url")."VisaArrivalProgram/visaArrivalStatusReport?visa_arrival_app_id=".$VisaAppID."&visa_arrival_app_refId=".$VisaRefNo);
//                $this->visa_application = $this->getVisaFreshRecord($getAppId);

            }
        }else if($visa_application[0]['ispaid'] == 1){ 
            ## Remove the application from cart; this is a gratis one
            $type = "Vap";
            $cartObj = new cartManager();
            $cartObj->removeCart($getAppId, $type);
        }

        ## Incrept application id for security
        $this->encriptedAppId = $request->getParameter('id');

        ## To Get Naria and Dollar Amount
        $this->paidAmount = 0;

        ## Check payment
        if($visa_application[0]['ispaid'] == 1 && isset($visa_application[0]['payment_gateway_id']))
        {
            $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($visa_application[0]['payment_gateway_id']);
            if($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' ||$PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)')
            {
                $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
                $this->paymentCurrency = "Naira";
                $this->currencySign = "NGN";
            }else
            {
                if($this->data['pay4me']){
                    if($this->data['currency'] == 'naira'){
                        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
                        $this->paymentCurrency = "Naira";
                        $this->currencySign = "NGN";
                    }else{
                        $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
                        $this->paymentCurrency = "Dollar";
                        $this->currencySign = "USD";
                    }
                }else{
                    $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
                    $this->paymentCurrency = "Dollar";
                    $this->currencySign = "USD";
                }
            }
            $this->PaymentGatewayType = $PaymentGatewayType;
        }else
        {
            $this->PaymentGatewayType = "";
            $this->paymentCurrency = "";
            $this->currencySign = "";
        }
        // incrept application id for security//
        $this->encriptedAppId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['id']);
        $this->encriptedAppId = SecureQueryString::ENCODE($this->encriptedAppId);
        $this->encriptedRefId = SecureQueryString::ENCRYPT_DECRYPT($this->visa_application[0]['ref_no']);
        $this->encriptedRefId = SecureQueryString::ENCODE($this->encriptedRefId);

        // SET SESSION VARIABLES FOR PAYMENT SYSTEM
        $applicant_name = $visa_application[0]['surname'].', '.$visa_application[0]['first_name'];
        $user = $this->getUser();
        $user->setAttribute("is_re_entry", false);
        $user->setAttribute("ref_no", $visa_application[0]['ref_no']);
        $user->setAttribute('applicant_name', $applicant_name);
        $user->setAttribute('app_id', $getAppId);

        $visaCatId = Doctrine::getTable('VisaApplication')->getVisaCategoryId($getAppId);
        $ReEntryIdFE = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $ReEntryIdFFE = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        if($visaCatId == $ReEntryIdFE){
            $payAppType = "visa";
            $user->setAttribute('app_type','Visa');
        }if($visaCatId == $ReEntryIdFFE){
            $payAppType = "freezone";
            $user->setAttribute('app_type','Freezone');
            $request->setAttribute('zone_type','Free Zone');
        }
        //        require_once(sfConfig::get('sf_lib_dir').'/paygateways/google/model/paymentHistoryClass.php');
        //        $hisObj = new paymentHistoryClass($getAppId, $payAppType);
        //        $lastAttemptedTime = $hisObj->getDollarPaymentTime();
        //        $this->IsProceed = true;
        //        if($lastAttemptedTime){
        //            $this->IsProceed = false;
        //        }
        //    $user->setAttribute('app_type','Visa');
        $user->setAttribute('email_id','');
        $user->setAttribute('mobile_no','');
        $this->payment_details = $payment_details;

        if(isset($payment_details['naira_amount'])) {
            $user->setAttribute('naira_amount',$payment_details['naira_amount']);
        } else {
            $user->setAttribute('naira_amount',0);
        }

        if(isset($payment_details['dollar_amount'])) {
            $user->setAttribute('dollar_amount',$payment_details['dollar_amount']);
        } else {
            $user->setAttribute('dollar_amount',0);
        }
        //application details sent as increpted to payment system
        if($visaCatId == $ReEntryIdFFE)
        $getAppIdToPaymentProcess = $getAppId.'~'.'freezone';
        else
        $getAppIdToPaymentProcess = $getAppId.'~'.'visa';
        $getAppIdToPaymentProcess = SecureQueryString::ENCRYPT_DECRYPT($getAppIdToPaymentProcess);
        $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($getAppIdToPaymentProcess);
    }

    protected function getVisaFreshRecord($id)
    {
        $visa_application = Doctrine::getTable('VapApplication')->getVisaFreshRecord($id);
        return $visa_application;
    }

    public function executeGetState(sfWebRequest $request)
    {
        $nigeriaContryId = Doctrine::getTable('Country')->getNigeriaId();
        if($request->getParameter('country_id') == $nigeriaContryId)
        {
            sfContext::getInstance()->getLogger()->info("+++".$request->getParameter('country_id'));

            $q = Doctrine::getTable('State')->createQuery('st')->
            addWhere('country_id = ?', $request->getParameter('country_id'))
            ->orderBy('state_name ASC')
            ->execute()
            ->toArray();

            $str = '<option>-- Please Select --</option>';
            if(count($q) > 0 )
            {
                foreach($q as $key => $value){
                    $str .= '<option value="'.$value['id'].'" >'.$value['state_name'].'</option>';
                }
            }
            return $this->renderText($str);
        }
        else if($request->getParameter('add_option')!="")
        {
            $str = '<option value="">'.$request->getParameter('add_option').'</option>';
        }
        else
        {
            $str = '<option value="">Not Applicable</option>';
        }
        return $this->renderText($str);
    }


    public function executeGetLga(sfWebRequest $request)
    {
        $lga_id = $request->getParameter('lga_id');
        $q = Doctrine::getTable('LGA')->createQuery('st')->
        addWhere('branch_state = ?', $request->getParameter('state_id'))->execute()->toArray();
        $str = '<option value="">-- Please Select --</option>';
        $sel = '';
        if(count($q) > 0 ){
            foreach($q as $key => $value){
                if($lga_id != ''){
                $sel = ($value['id'] == $lga_id)?'selected="selected"':'';
                }
                $str .= '<option value="'.$value['id'].'" '.$sel.' >'.$value['lga'].'</option>';
            }
        }
        return $this->renderText($str);
    }
    public function executeCheckVisaAppRef(sfWebRequest $request)
    {
        //1. find out if this is fresh entry or re-entry
        //   Find out if this appid , refid exists or not
        $VisaRef['visa_app_id'] = $request->getParameter("visa_app_id");
        $VisaRef['visa_app_refId'] = $request->getParameter("visa_app_refId");
        //die('ddddddddddd');
        //        echo '<pre>';
        //        print_r($VisaRef);
        //        echo '</pre>';
        //        exit;

        $visaFound = Doctrine::getTable('VapApplication')->getVisaAppIdRefId($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
        if (!$visaFound) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error','Application Not Found!! Please try again.',false);
            $this->setTemplate('editVisaApplication');
            return;
        }
        //find if any payment request create
        $paymentHelper = new paymentHelper();
        $checkAppType = "NIS VISA ARRIVAL";
        $paymentRequestStatus = $paymentHelper->getPaymentRequestStatus($VisaRef['visa_app_id'], $checkAppType);
        if(!$paymentRequestStatus)
        {
            $this->getUser()->setFlash('error','Your application under payment awaited state!! you can not edit this application.',true);
            if($checkAppType == "NIS VISA ARRIVAL")
            $this->redirect('visaArrival/editVisaApplication');
            return;
        }
        //Find out Payment is done
        $isPayment = Doctrine::getTable('VapApplication')->isPayment($VisaRef['visa_app_id'],$VisaRef['visa_app_refId']);
        if ($isPayment == 1) {
            // TODO redirect to not found error page
            $this->getUser()->setFlash('error','Your application is already in process!! you cannot edit this application.',false);
            $this->setTemplate('editVisaApplication');

            return;
        }
        //   create DQL to find out if this is fresh or re-entry

        $request->setParameter('id', $visaFound);
        $this->forward('visaArrival', 'edit');
        $this->setTemplate('editVisaApplication');
    }
    public function executeVisaAcknowledgmentSlip(sfWebRequest $request)
  {
    $getAppId = SecureQueryString::DECODE($request->getParameter('id'));
    $getAppId = SecureQueryString::ENCRYPT_DECRYPT($getAppId);
    $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
    $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
    $this->visa_application = $this->getVisaFreshRecord($getAppId);
    $cat_id = $this->visa_application[0]['visacategory_id'];
    $country_id = $this->visa_application[0]['present_nationality_id'];
    $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
    $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
    $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];
    $visa_count_id = $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'];
    $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$cat_id,$visa_id,$entry_id,$no_of_entry,'', true);
    $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);

    $entriesNo ="";
    if(isset($this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']) && $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] != "" && $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'] != 0)
    {$entriesNo = "-&nbsp;[&nbsp;".$this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type']."&nbsp;Multiple Entries ]";
    }
    if($cat_id==$freshEntry){
      $request_type = "Entry Visa".$entriesNo;
    }else if($cat_id==$freezoneFreshEntry){
      $request_type = "Free Zone Entry Visa".$entriesNo;
    }
    //Dollar Amount
    $dollarAmt = 0;
    if($this->visa_application[0]['ispaid'] == 1)
    {
      $dollarAmt =  $this->visa_application[0]['paid_dollar_amount'];
    }else
    {
      if(isset($this->payment_details['dollar_amount'])){ $dollarAmt = $this->payment_details['dollar_amount'];}
    }
    ##############//Get payment gateway type and Paid amount ###########################
    $this->paidAmount = 0;
    $PaymentGatewayType = "";
    if($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id']))
    {
      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
      if($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch'||$PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)')
      {
        $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
        if($this->paidAmount != 0.00)
        {
          $this->paidAmount = "NGN&nbsp;".$this->paidAmount;
        }else{
          $this->paidAmount = "Not Applicable";
        }
      }else
      {
        $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($getAppId,'VISA');
        if($this->data['pay4me']){
          if($this->data['currency'] == 'naira'){
            $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
            if($this->paidAmount != 0.00)
            {
              $this->paidAmount = "NGN&nbsp;".$this->paidAmount;
            }else{
              $this->paidAmount = "Not Applicable";
            }
          }else{
            $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
            if($this->paidAmount != 0.00)
            {
              $this->paidAmount = "USD&nbsp;".$this->paidAmount;
            }else
            {
              $this->paidAmount = "Not Applicable";
            }
          }
        }else{
          $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
          if($this->paidAmount != 0.00)
          {
            $this->paidAmount = "USD&nbsp;".$this->paidAmount;
          }else
          {
            $this->paidAmount = "Not Applicable";
          }
        }
      }
    }
    ################### End of Code ############################################################
    //Get Payment status
    $interviewDate = "";
    $nairaAmt = 0;
    $isGratis = false;  //set flag for application is gratis or not
    if($this->visa_application[0]['ispaid'] == 1)
    {
      $interviewDate = $this->visa_application[0]['interview_date'];
      //      $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
      if(($this->visa_application[0]['paid_naira_amount'] == 0.00) && ($this->visa_application[0]['paid_dollar_amount'] == 0.00))
      {
        $isGratis = true;
        $payment_status = "This Application is Gratis (Requires No Payment)";
        $PaymentGatewayType = "Not Applicable";
        $interviewDate = "";
        $dollarAmt = 0;
        $nairaAmt = 0;
      }else
      {
        $payment_status = "Payment Done";
      }
    }else{
      $payment_status = "";
      $interviewDate = "Available after Payment";
      //      $PaymentGatewayType = "";
    }

    //Get Embassy Name
    if($this->applying_country !== "Nigeria")
    {
      $embassyName = $this->visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];

    }else{
      $embassyName = "Not Applicable";
    }
    if($this->visa_application[0]['VisaCategory']['var_value']=='Fresh Freezone'){
      $app_cat='Fresh Free Zone';
    }else{
      $app_cat=$this->visa_application[0]['VisaCategory']['var_value'];
    }
    $data = array (
            "profile"  => array(
                        "title"=>$this->visa_application[0]['title'],
                        "first_name" => $this->visa_application[0]['other_name'],
                        "middle_name" => $this->visa_application[0]['middle_name'],
                        "last_name" => $this->visa_application[0]['surname'],
                         "maiden_name" => "",
                        "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                        "sex" => $this->visa_application[0]['gender'],
                        "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                        "state_origin" => "",
                        "occupation" => $this->visa_application[0]['profession']),
            "contact_info" =>array(
        // "permanent_address"=>$this->visa_application[0]['permanent_address'],
                        "phone" => $this->visa_application[0]['perm_phone_no'],
                        "email" => "",
                        "mobile" => "",
                        "home_town" => "",
                        "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                        "state_origin" => "",
      ),
              "personal_info" => array(
                        "marital_status"=>$this->visa_application[0]['marital_status'],
                        "eye_color" => $this->visa_application[0]['eyes_color'],
                        "hair_color" =>$this->visa_application[0]['hair_color'],
                        "height" => $this->visa_application[0]['height'],
                        "complexion" => "",
                        "mark" => $this->visa_application[0]['id_marks'],
      ),
            "app_info" => array(
                      "application_category"=>$app_cat."&nbsp;Visa/Permit&nbsp;",
                      "application_type"=>$this->visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']."&nbsp;Visa",
                      "request_type"=>$request_type,
                      "application_date"=>$this->visa_application[0]['created_at'],
                      "application_id"=>$this->visa_application[0]['id'],
                      "reference_no"=>$this->visa_application[0]['ref_no'],
                      "form_type"=>"visa",
                      "isGratis"=>$isGratis
      ),
            "process_info"   => array(
                        "country" => $this->applying_country,
                        "state" => "",
                        "embassy" =>$embassyName,
                        "office" => "",
                        "interview_date" =>$interviewDate,
      ),
            "payment_info"   => array(
                        "naira_amount" => $nairaAmt,
                        "dollor_amount" => $dollarAmt,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $this->paidAmount
      ),
    );
    $this->forward404Unless($this->visa_application);
    $this->setVar('data',$data);
    $this->setLayout('layout_print');
  }
  //Print Receipt After Payment
    public function executePrintRecipt(sfWebRequest $request)
    {

      $VisaAppID = SecureQueryString::DECODE($request->getParameter('visa_app_id'));
      $VisaAppID = SecureQueryString::ENCRYPT_DECRYPT($VisaAppID);
      $VisaRef = SecureQueryString::DECODE($request->getParameter('visa_app_refId'));
      $VisaRef = SecureQueryString::ENCRYPT_DECRYPT($VisaRef);
      $VisaReportType = $request->getParameter('printType');

      $visaFound = Doctrine::getTable('VisaApplication')->getVisaAppIdRefId( $VisaAppID,$VisaRef);
      if (!$visaFound) {
        // TODO redirect to not found error page
        $this->setVar('errMsg','Application Not Found!! Please try again.');
        $this->setTemplate('VisaStatus');
        return;
      }
      $isFreshEntry = Doctrine::getTable('VisaApplication')->isFreshEntry($VisaAppID);
      $isFreezoneFreshEntry = Doctrine::getTable('VisaApplication')->isFreezoneFreshEntry($VisaAppID);

      $freshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryId();
      $reEntry = Doctrine::getTable('VisaCategory')->getReEntryId();
      $freezoneFreshEntry = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
      $freezoneReEntry = Doctrine::getTable('VisaCategory')->getReEntryFreezoneId();

      if($isFreshEntry!='' || $isFreezoneFreshEntry!=''){
        //For show the Information
        $this->visa_application = $this->getVisaFreshRecord($VisaAppID);
        $cat_id = $this->visa_application[0]['visacategory_id'];
        $country_id = $this->visa_application[0]['present_nationality_id'];
        $visa_id = $this->visa_application[0]['VisaApplicantInfo']['visatype_id'];
        $entry_id = $this->visa_application[0]['VisaApplicantInfo']['entry_type_id'];
        $zone_id = $this->visa_application[0]['zone_type_id'];
        $no_of_entry = $this->visa_application[0]['VisaApplicantInfo']['no_of_re_entry_type'];

        $visa_count_id = $this->visa_application[0]['VisaApplicantInfo']['applying_country_id'];
        //Get Processing Country Name
        $this->applying_country = Doctrine::getTable('Country')->getPassportPCountry($visa_count_id);

      }else
      {
        $this->visa_application = $this->getVisaReEntryRecord($VisaAppID);
        $cat_id = $this->visa_application[0]['visacategory_id'];
        $country_id = $this->visa_application[0]['present_nationality_id'];
        $visa_id = $this->visa_application[0]['ReEntryVisaApplication']['visa_type_id'];
        $entry_id = $this->visa_application[0]['ReEntryVisaApplication']['re_entry_type'];
        $no_of_entry = $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type'];
        $zone_id = $this->visa_application[0]['zone_type_id'];
        $this->forward404Unless($this->visa_application);
      }
      if($cat_id==$freshEntry){
        $this->appType='Visa';
      }else if($cat_id==$reEntry){
        $this->appType='Visa';
      }else if($cat_id==$freezoneFreshEntry){
        $this->appType='Free Zone';
      }else if($cat_id==$freezoneReEntry){
        $this->appType='Free Zone';
      }
      $this->payment_details = Doctrine::getTable('VisaFee')->getVisaFee($country_id,$cat_id,$visa_id,$entry_id,$no_of_entry,$zone_id, true);
      //Get Payment status
      $isGratis = false; // if application is gratic
      if($this->visa_application[0]['ispaid'] == 1)
      {
        if(($this->visa_application[0]['paid_naira_amount'] == 0) && ($this->visa_application[0]['paid_dollar_amount'] == 0))
        {
          $payment_status = "This Application is Gratis (Requires No Payment)";
          $isGratis = true;
        }else
        {
          $payment_status = "Payment Done";
        }
      }


      ##############//Get payment gateway type and Paid amount ###########################
      $this->paidAmount = 0;
      if($this->visa_application[0]['ispaid'] == 1 && isset($this->visa_application[0]['payment_gateway_id']))
      {
        $PaymentGatewayType = Doctrine::getTable('PaymentGatewayType')->getGatewayName($this->visa_application[0]['payment_gateway_id']);
        if($PaymentGatewayType == 'eTranzact' || $PaymentGatewayType == 'Interswitch' ||$PaymentGatewayType == 'Pay4me (Interswitch)' || $PaymentGatewayType == 'Pay4me (Bank)' || $PaymentGatewayType == 'Pay4me (eWallet)')
        {
          $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
          if($this->paidAmount != 0.00)
          {
            $this->paidAmount = "NGN&nbsp;".$this->paidAmount;
          }else
          {
            $this->paidAmount = "Not Applicable";
          }
        }else
        {
          $this->data = Doctrine::getTable('PaymentRequest')->getValidationNumberStatus($VisaAppID,'VISA');
          if($this->data['pay4me']){
            if($this->data['currency'] == 'naira'){
              $this->paidAmount = $this->visa_application[0]['paid_naira_amount'] ;
              if($this->paidAmount != 0.00)
              {
                $this->paidAmount = "NGN&nbsp;".$this->paidAmount;
              }else
              {
                $this->paidAmount = "Not Applicable";
              }
            }else{
              $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
              if($this->paidAmount != 0.00)
              {
                $this->paidAmount = "USD&nbsp;".$this->paidAmount;
              }else
              {
                $this->paidAmount = "Not Applicable";
              }
            }
          }else{
            $this->paidAmount = $this->visa_application[0]['paid_dollar_amount'] ;
            if($this->paidAmount != 0.00)
            {
              $this->paidAmount = "USD&nbsp;".$this->paidAmount;
            }else
            {
              $this->paidAmount = "Not Applicable";
            }
          }
        }
      }
      ################### End of Code ############################################################

      if($this->visa_application[0]['VisaCategory']['var_value'] != "Fresh" && $this->visa_application[0]['VisaCategory']['var_value'] !="Fresh Freezone")
      {
        //Naira Amount
        $nairaAmt = "0";
        if($this->visa_application[0]['VisaZoneType']['var_value']!='Free Zone' ){
          if($this->visa_application[0]['ispaid'] == 1  )
          {
            $nairaAmt =  $this->visa_application[0]['paid_naira_amount'];
          }else
          {
            if(isset($this->payment_details['naira_amount'])){$nairaAmt = $this->payment_details['naira_amount'];}
          }
        }
        //Dollar Amount
        $dollarAmt = "0";
        if($this->visa_application[0]['VisaZoneType']['var_value']=='Free Zone' ){
          if($this->visa_application[0]['ispaid'] == 1)
          {
            $dollarAmt = $this->visa_application[0]['paid_dollar_amount'];
          }else
          {
            if(isset($this->payment_details['dollar_amount'])){ $dollarAmt =  $this->payment_details['dollar_amount'];}
          }
          $VisaTypeId=Doctrine::getTable('ReEntryVisaApplication')->getVisaTypeId($this->visa_application[0]['id']);
          $FreezoneSingleVisaId=Doctrine::getTable('FreezoneSingleVisaType')->getFreeZoneVisaTypeId();
          $FreezoneMultipleVisaId=Doctrine::getTable('FreezoneMultipleVisaType')->getFreeZoneVisaTypeId();
          if($VisaTypeId==$FreezoneSingleVisaId){
            $app_type='Single Re-entry';
          }else if($VisaTypeId==$FreezoneMultipleVisaId){
            $app_type='Multiple Re-entry';
          }
          $app_category=$this->visa_application[0]['VisaCategory']['var_value']."&nbsp;Visa/Permit&nbsp";
          $processing_centre=Doctrine::getTable('VisaProcessingCentre')->getProcessingCentre($this->visa_application[0]['ReEntryVisaApplication']['processing_centre_id']);
        }else{
          $app_type=$this->visa_application[0]['ReEntryVisaApplication']['VisaType']['var_value'];
          $app_category=$this->visa_application[0]['VisaCategory']['var_value']."&nbsp;Visa/Permit&nbsp;(".$this->visa_application[0]['VisaZoneType']['var_value'].")";
          $processing_centre='';
        }

        if(isset($this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']) && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']!="" && $this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']!=0 )
        {$no_of_entry ="-&nbsp;[&nbsp;".$this->visa_application[0]['ReEntryVisaApplication']['no_of_re_entry_type']."&nbsp;Multiple Re Entries ]"; }
        else
        $no_of_entry = "";

        $request_type =  $this->visa_application[0]['VisaCategory']['var_value']."&nbsp;Visa".$no_of_entry;

        if(isset($this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'])){
          $state = $this->visa_application[0]['ReEntryVisaApplication']['VisaProceesingState']['state_name'];
        }else{
          $state='';
        }
        if(isset($this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'])){
          $visaOffice = $this->visa_application[0]['ReEntryVisaApplication']['VisaOffice']['office_name'];
        }else{
          $visaOffice='';
        }

        $data = array (
            "profile"  => array(
                        "title"=>$this->visa_application[0]['title'],
                        "first_name" => $this->visa_application[0]['other_name'],
                        "middle_name" => $this->visa_application[0]['middle_name'],
                        "last_name" => $this->visa_application[0]['surname'],
                        "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                        "sex" => $this->visa_application[0]['gender'],
                        "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                        "state_origin" => "",
                        "occupation" => $this->visa_application[0]['ReEntryVisaApplication']['profession']),
            "app_info" => array(
                      "application_category"=>$app_category,
                      "application_type"=>$app_type."&nbsp;Visa",
                      "request_type"=>$request_type,
                      "application_date"=>$this->visa_application[0]['created_at'],
                      "application_id"=>$this->visa_application[0]['id'],
                      "reference_no"=>$this->visa_application[0]['ref_no'],
                      "isGratis" => $isGratis,
          ),
            "process_info"   => array(
                        "country" => "Nigeria",
                        "processing_centre" => $processing_centre,
                        "state" => $state,
                        "embassy" => "",
                        "office" => $visaOffice,
                        "interview_date" =>$this->visa_application[0]['interview_date'],
          ),
            "payment_info"   => array(
                        "naira_amount" => $nairaAmt,
                        "dollor_amount" => $dollarAmt,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $this->paidAmount
          ),
        );
      }else{
        if(isset($no_of_entry) && $no_of_entry!="" && $no_of_entry!=0)
        {
          $no_of_entry = "-&nbsp;[&nbsp;".$no_of_entry."&nbsp;Multiple Entries ]";
        }
        else
        $no_of_entry = "";
        $request_type = "Entry Visa".$no_of_entry;

        //Dollar Amount
        $dollarAmt = 0;
        $nairaAmt = 0;
        if($this->visa_application[0]['ispaid'] == 1)
        {
          $dollarAmt =  $this->visa_application[0]['paid_dollar_amount'];
        }else
        {
          if(isset($this->payment_details['dollar_amount'])){ $dollarAmt = $this->payment_details['dollar_amount'];}
        }

        //Get Embassy Name
        if($this->applying_country != "Nigeria")
        {
          $embassyName = $this->visa_application[0]['VisaApplicantInfo']['EmbassyMaster']['embassy_name'];
        }else
        {
          $embassyName = "Not Applicable";
        }
        if($cat_id==$freshEntry){
          $category_type='Entry Visa/Permit';
        }else if($cat_id==$freezoneFreshEntry){
          $category_type='Fresh Free Zone Visa/Permit';
        }

        $data = array (
            "profile"  => array(
                        "title"=>$this->visa_application[0]['title'],
                        "first_name" => $this->visa_application[0]['other_name'],
                        "middle_name" => $this->visa_application[0]['middle_name'],
                        "last_name" => $this->visa_application[0]['surname'],
                        "date_of_birth" => $this->visa_application[0]['date_of_birth'],
                        "sex" => $this->visa_application[0]['gender'],
                        "country_origin" => $this->visa_application[0]['CurrentCountry']['country_name'],
                        "state_origin" => "",
                        "occupation" => $this->visa_application[0]['profession']),
            "app_info" => array(
                      "application_category"=>$category_type,
                      "application_type"=>$this->visa_application[0]['VisaApplicantInfo']['VisaTypeId']['var_value']."&nbsp;Visa",
                      "request_type"=>$request_type,
                      "application_date"=>$this->visa_application[0]['created_at'],
                      "application_id"=>$this->visa_application[0]['id'],
                      "reference_no"=>$this->visa_application[0]['ref_no'],
                      "isGratis" => $isGratis,
          ),
                "process_info"   => array(
                        "country" => $this->applying_country,
                        "state" => "",
                        "embassy" => $embassyName,
                        "office" => "",
                        "interview_date" =>$this->visa_application[0]['interview_date'] ),
            "payment_info"   => array(
                        "naira_amount" => $nairaAmt,
                        "dollor_amount" => $dollarAmt,
                        "payment_status" =>$payment_status,
                        "payment_gateway" => $PaymentGatewayType,
                        "paid_amount" => $this->paidAmount
          ),
        );
      }

      $this->setLayout('layout_print');
      $this->setVar('appId', $VisaAppID); // set application id
      $this->setVar('data',$data);
    }

    public function executeEditVisaApplication(sfWebRequest $request)
    {
    }

    /**
     *
     * @param <type> $request
     * provide search area where user can search application with applicatio id and reference number... ([WP: 078] => CR: 113)
     */
    public function executeSearchFlightDetail(sfWebRequest $request){
        
    }

    /**
     *
     * @param <type> $request
     * This function will work after user input application details into seach fields and press enter... ([WP: 078] => CR: 113)
     * Again adding ([WP: 080] => CR: 116)
     */
    public function executeEditFlightDetail(sfWebRequest $request){
        $this->appId = trim($request->getParameter('app_id'));
        $this->refNo = trim($request->getParameter('ref_no'));
        $msg = '';        
        $this->expiry_arrival_days =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;
        if((isset($this->appId) && $this->appId != '') && (isset($this->refNo) && $this->refNo != '')){
            $detailObj = Doctrine::getTable('VapApplication')->find($this->appId);
            if(!empty($detailObj)){
                if($detailObj->getRefNo() == $this->refNo){
                    $status = $detailObj->getStatus();                    
                    $statusArray = array('New', 'Paid');
                    if(in_array($status, $statusArray)){                        
                        if($detailObj->getStatus() == 'New'){
                           $this->createdAt = $detailObj->getCreatedAt();
                           $this->dateLabel = 'Application Creation Date';                           
                           $expiryDate = strtotime('+ '.$this->expiry_arrival_days.' months', strtotime(date('Y-m-d H:i:s')));
                        }else{
                           $this->createdAt =$detailObj->getPaidDate().' 00:00:00';
                           $this->dateLabel = 'Application Paid Date';                           
                           $expiryDate = strtotime('+ '.$this->expiry_arrival_days.' months', strtotime($this->createdAt));
                        }


                        $this->expiry_date = date('Y-m-d',strtotime('-1 day', $expiryDate));
                        if(strtotime('now') < $expiryDate ){
                            $this->form = new VisaArrivalFlightDetailForm($detailObj);
                            $this->getUser()->setAttribute('id', $this->appId);
                            $this->getUser()->setAttribute('ref_no', $this->refNo);
                        }else{
                            $msg = 'Application has been expired.';
                        }
                    }else{
                        $msg = 'You can not edit arrival details due to its '.$status. ' status.';
                    }
                }else{
                    $msg = 'Application is not found.';
                }
            }else{
                $msg = 'Application is not found.';
            }
        }else{
            $msg = 'Please enter below required details.';
        }

        if(!empty($msg)){
            $this->getUser()->setFlash('error',$msg);
            $this->redirect('visaArrival/searchFlightDetail');
        }
    }

    /**
     *
     * @param <type> $request
     * This function update flight details... ([WP: 078] => CR: 113)
     * Again adding ([WP: 080] => CR: 116)
     */
    
    public function executeUpdateFlightDetail(sfWebRequest $request){
        
        $appId = $this->getUser()->getAttribute('id');
        $refNo = $this->getUser()->getAttribute('ref_no');        
        $this->expiry_arrival_days =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;        
         if((isset($appId) && $appId != '') && (isset($refNo) && $refNo != '')){
             $detailObj = Doctrine::getTable('VapApplication')->find($appId);
             if(!empty($detailObj)){
               if($detailObj->getRefNo() == $refNo){                   
                   if($detailObj->getStatus() == 'New'){
                           $this->createdAt = $detailObj->getCreatedAt();
                           $this->dateLabel = 'Application Creation Date';
                           $createdAt = date('Y-m-d H:i:s');
                           $dateLabel = 'Today';
                           $expiryDate = strtotime('+ '.$this->expiry_arrival_days.' months', strtotime(date('Y-m-d H:i:s')));
                        }else{
                           $this->createdAt =$detailObj->getPaidDate().' 00:00:00';
                           $this->dateLabel = 'Application Paid Date';
                           $dateLabel = 'Application Paid Date';
                           $createdAt =$detailObj->getPaidDate().' 00:00:00';
                           $expiryDate = strtotime('+ '.$this->expiry_arrival_days.' months', strtotime($this->createdAt));
                        }

                        $this->expiry_date = date('Y-m-d',strtotime('-1 day', $expiryDate));
                        
                    $arrival_date_in_db = $detailObj->getArrivalDate();
                    $day = trim($request->getPostParameter('vap_application[arrival_date][day]'));
                    $month = trim($request->getPostParameter('vap_application[arrival_date][month]'));
                    $year = trim($request->getPostParameter('vap_application[arrival_date][year]'));
                    $arrival_date_in_form = date("Y-m-d H:i:s", @mktime(0,0,0,date($month), date($day), date($year)));

                    ## Fetiching currend date...
                    $curr_date = strtotime(date('Y-m-d'));
                    ## Creating arrival date validation flag...
                    if($curr_date == strtotime($arrival_date_in_db)){
                        if(strtotime($arrival_date_in_db) == strtotime($arrival_date_in_form)){
                            $arrival_date_validation = false;
                        }else{
                            $arrival_date_validation = true;
                        }
                    }else{
                        $arrival_date_validation = true;
                    }

                   $this->form = new VisaArrivalFlightDetailForm($detailObj,array('application_created_at'=>$createdAt,'arrival_date_validation'=>$arrival_date_validation,'date_label'=>$dateLabel));
                   
                   $reqParm = $request->getPostParameter($this->form->getName());
                   $this->form->bind($reqParm,$request->getFiles($this->form->getName()));
                   if($this->form->isValid()){
                       $this->form->save();
                       $this->getUser()->setFlash('error','Flight Information have been updated successfully.');
                       $this->redirect('visaArrival/editFlightDetail?app_id='.$appId.'&ref_no='.$refNo);
                   }else{
                        $this->setTemplate('editFlightDetail');
                   }

               }else{
                   $this->getUser()->setFlash('error','Application not found.');
                   $this->redirect('visaArrival/searchFlightDetail');
               }
         }
      }

      $this->setTemplate('editFlightDetail');
    }

     protected function notifyNoAmountPayment($appid)
    {
      $status_id = true;
      $transid = 1;
      $dollarAmount = 0;
      $nairaAmount = 0;
      $gtypeTble = Doctrine::getTable('PaymentGatewayType');
      $gatewayTypeId = $gtypeTble->getGatewayId(PaymentGatewayTypeTable::$TYPE_NONE);
      $todayDate = date('Y-m-d');

      //Update tbl_vap table in case of gratis application

         $vapObj = Doctrine::getTable('VapApplication')->find($appid);
         $vapObj->setPaidDollarAmount($dollarAmount);
         $vapObj->setPaidNairaAmount($nairaAmount);
         $vapObj->setIspaid(1);
         $vapObj->setPaymentGatewayId($gatewayTypeId);
         $vapObj->setStatus('Paid');
         $vapObj->setPaymentTransId($transid);
         $vapObj->setPaidDate($todayDate);
         $vapObj->save();

    }

    
    

}
