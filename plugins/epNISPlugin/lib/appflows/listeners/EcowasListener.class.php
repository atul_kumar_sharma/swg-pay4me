<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class EcowasListener {
 

  public static function getExecutionIdFromAppId($appid) {
    $q = Doctrine::getTable('Workpool')
    ->createQuery('qr')->select('execution_id')
    ->where('application_id = ?', $appid)
    ->andWhere('flow_name = ?', EcowasWorkflow::$ECOWAS_FLOW_NAME)
    ->execute()->toArray();

    if(!isset($q[0]['execution_id']))
        throw new Exception('Could not load execution state');
      else
        return $q[0]['execution_id'];
  }

  public static function getDBInstance() {
    $db = null;
    $db = WorkflowHelper::getDbInstance();
    ezcDbInstance::set( $db );
    return $db = ezcDbInstance::get();
  }
  public static function getDBExecuter($appid = null) {
    // TODO: get these values from symfony config file
    $workflowID  = null;
    $db = EcowasListener::getDBInstance();
    $executer = null;
    if ($appid == null) {
      $executer = new ezcWorkflowDatabaseExecution( $db );
    } else {
      $workflowID  = EcowasListener::getExecutionIdFromAppId($appid);
      $executer = new ezcWorkflowDatabaseExecution( $db,intval($workflowID));
    }
    return $executer;
  }

  public static function newApplication (sfEvent $event) {
    // * fetch the application id from event
    $evtData = $event->getSubject();
    $appid = $evtData [ EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR ];

    // * instantiate the new workflow object
    
    $excuter = EcowasListener::getDBExecuter();

    // Set up workflow definition storage (database).
    $db = EcowasListener::getDBInstance();
    $definition = new ezcWorkflowDatabaseDefinitionStorage($db);

    // Load latest version of workflow named "Registration".
    $workflow = $definition->loadByName(EcowasWorkflow::$ECOWAS_FLOW_NAME);

    $excuter->workflow = $workflow;
    $workflowId = $excuter->start();


    // TODO: Find out name uniqueness funda
    $workflowName = EcowasWorkflow::$ECOWAS_FLOW_NAME;

    // *  store the app-id, worflow id, workflow name in workpool table
    $workpool = new Workpool();
    $workpool->setFlowName($workflowName);
    $workpool->setExecutionId($workflowId);
    $workpool->setApplicationId($appid);
    $workpool->save();
  }

  public static function newPayment (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR ];
    $trasid = $evtData [ EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR ];
    $tranStatus = $evtData [ EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR ];
    $nairaAmount = $evtData [ EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR];
    $gatewayType = $evtData [ EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR];
    if(empty($dollarAmount)){$dollarAmount=0;}
    if(empty($nairaAmount)){$nairaAmount=0;}
    // load the workflow
    try
    {
      $excuter = EcowasListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        echo "Error: ('{$e->getMessage()}')\n. Please contact to Portal Administrator.";
        die;
    }
    $excuter->resume(array(
        EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR=>$tranStatus,
        EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR=>$trasid,
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR=>$appid,
        EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR=>$nairaAmount,
        EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR=>$gatewayType
      ));          
  }

  public static function newVetter (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_VETTER ];
   
    // load the workflow
    try
    {
      $excuter = EcowasListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        echo "Error: ('{$e->getMessage()}')\n. Pease contact to Portal Administrator.";
        die;
    }
    // * resume workflow for next input node
    $excuter->resume(array(
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_VETTER=>$appid
      ));    
  }
  public static function newApprover (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER ];
    
    // load the workflow
    try
    {
     $excuter = EcowasListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        echo "Error: ('{$e->getMessage()}')\n. Pease contact to Portal Administrator.";
        die;
    }
    //$excuter->resume(array('approverInput'=>'NextStep'));
    
    // * resume workflow for next input node
    $excuter->resume(array(
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER=>$appid
      ));
   
  }

  public static function newIssue (sfEvent $event) {
    // fetch the variables of interests - these are the one
    // which needs to be passed to workflow

    $evtData = $event->getSubject();

    $appid = $evtData [ EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_ISSUE ];

    // load the workflow
    try
    {
     $excuter = EcowasListener::getDBExecuter($appid);
    }
    catch(Exception $e)
    {
        echo "Error: ('{$e->getMessage()}')\n. Pease contact to Portal Administrator.";
        die;
    }
    //$excuter->resume(array('approverInput'=>'NextStep'));

    // * resume workflow for next input node
    $excuter->resume(array(
        EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_ISSUE=>$appid
      ));

  }
}