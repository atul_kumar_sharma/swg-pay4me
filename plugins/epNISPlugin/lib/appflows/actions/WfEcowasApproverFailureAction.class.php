<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasApproverFailureAction implements ezcWorkflowServiceObject {
 

  public function __construct(  )
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_APPROVER);
    
    
    $q = Doctrine::getTable('EcowasApplication')
    ->createQuery('qr')->select('qr.ref_no')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

     Doctrine_Query::create()
      ->update('EcowasApplication pa')
      ->set('pa.status',"'Rejected by Approver'")
      ->where('pa.id = ?', $appid)
      ->execute();

    $deleted = Doctrine_Query::create()
    ->delete()
    ->from('EcowasApprovalQueue u')
    ->where('u.application_id = ?', $appid)
    ->andWhere('u.ref_id = ?', $q[0]['ref_no'])
    ->execute();

   return true;
  }
  

  public function __toString() {
    return "Ecowas Approver Failure Actions";
  }
}
