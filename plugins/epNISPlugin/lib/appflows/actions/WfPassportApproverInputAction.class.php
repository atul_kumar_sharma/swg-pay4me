<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfPassportApproverInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {


    //$approverStatus = $execution->getVariable(PassportWorkflow::$APPROVER_SUCCESS_VAR);
    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR_FROM_APPROVER);
    // $comments = $execution->getVariable(PassportWorkflow::$APPROVER_COMMENT_VAR);

    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('PassportApprovalRecommendation')->getGrantId();

    $q = Doctrine::getTable('PassportApprovalInfo')
    ->createQuery('qr')->select('recomendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();


    if ( $q[0]['recomendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "Passport Application Id:".$appid.", Approver status:Approved");
      $execution->setVariable(PassportWorkflow::$PASSPORT_SUCCESS_VAR_FROM_APPROVER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Passport Application Id:".$appid.", Approver status:Rejected");
      $execution->setVariable(PassportWorkflow::$PASSPORT_SUCCESS_VAR_FROM_APPROVER,false);
      return true;
    }

  }

  public function __toString() {
    return " Passport Approver Successful Actions";
  }
}
