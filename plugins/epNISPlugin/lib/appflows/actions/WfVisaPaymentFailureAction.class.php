<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfVisaPaymentFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(VisaWorkflow::$VISA_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(VisaWorkflow::$VISA_APPLICATION_ID_VAR);
    $transstatus = $execution->getVariable(VisaWorkflow::$VISA_TRANS_SUCCESS_VAR);
    $dollarAmount = $execution->getVariable(VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR);
    $nairaAmount = $execution->getVariable(VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(VisaWorkflow::$VISA_GATEWAY_TYPE_VAR);

    
    sfContext::getInstance()->getLogger()->err(
      "{Visa PaymentFailureAction} Passed in var APPID:".$appid.
      ", Xns id:". $transid. ", XnsSts: ".$transstatus);

    if($execution->hasVariable(VisaWorkflow::$VISA_TRANSACTION_ID_VAR))
    {

      // *  store the transaction-status payment-transaction-id in Visa _application table
      /*
    Doctrine_Query::create()
      ->update('VisaApplication pa')
      ->set('pa.ispaid',0)
      ->set('pa.paid_at',"'".date('Y-m-d')."'")
      ->set('pa.payment_trans_id',$transid)          
      ->set('pa.payment_gateway_id',"'".$gatewayType."'")
      ->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
      ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
      ->where('pa.id = ?', $appid)
      ->execute();
    */


      $execution->unsetVariable(VisaWorkflow::$VISA_TRANSACTION_ID_VAR);
      $execution->unsetVariable(VisaWorkflow::$VISA_APPLICATION_ID_VAR);
      $execution->unsetVariable(VisaWorkflow::$VISA_TRANS_SUCCESS_VAR);
      $execution->unsetVariable(VisaWorkflow::$VISA_DOLLAR_AMOUNT_VAR);
      $execution->unsetVariable(VisaWorkflow::$VISA_NAIRA_AMOUNT_VAR);
      $execution->unsetVariable(VisaWorkflow::$VISA_GATEWAY_TYPE_VAR);
    }     

    return true;
  }

  public function __toString() {
    return "Visa Payment failure Actions";
  }
}
