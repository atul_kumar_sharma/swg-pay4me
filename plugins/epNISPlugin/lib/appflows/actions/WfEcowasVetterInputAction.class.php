<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfEcowasVetterInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

  
    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR_FROM_VETTER);   


    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('EcowasVettingRecommendation')->getGrantId();

    $q = Doctrine::getTable('EcowasVettingInfo')
    ->createQuery('qr')->select('recomendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();
    
       
    if ( $q[0]['recomendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid.", Vetter status:Approved");
      $execution->setVariable(EcowasWorkflow::$ECOWAS_SUCCESS_VAR_FROM_VETTER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Ecowas Application Id:".$appid.", Vetter status:Rejected");
      $execution->setVariable(EcowasWorkflow::$ECOWAS_SUCCESS_VAR_FROM_VETTER,false);
      return true;
    }    
 }
  public function __toString() {
    return "Ecowas Vetter Successful Actions";
  }
}
