<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasPaymentFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR);
    $transstatus = $execution->getVariable(EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR);    
    $nairaAmount = $execution->getVariable(EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR);

    
    sfContext::getInstance()->getLogger()->info(
      "{Ecowas PaymentFailureAction} Passed in var APPID:".$appid.
      ", Xns id:". $transid. ", XnsSts: ".$transstatus);

    if($execution->hasVariable(EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR))
    {
      // *  store the transaction-status payment-transaction-id in passport _application table
      /*
      Doctrine_Query::create()
        ->update('EcowasApplication pa')
        ->set('pa.ispaid',0)
        ->set('pa.paid_at',"'".date('Y-m-d')."'")
        ->set('pa.payment_trans_id',$transid)       
        ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
         ->set('pa.payment_gateway_id',"'".$gatewayType."'")
        ->where('pa.id = ?', $appid)
        ->execute();
      */
      $execution->unsetVariable(EcowasWorkflow::$ECOWAS_TRANSACTION_ID_VAR);
      $execution->unsetVariable(EcowasWorkflow::$ECOWAS_APPLICATION_ID_VAR);
      $execution->unsetVariable(EcowasWorkflow::$ECOWAS_TRANS_SUCCESS_VAR);      
      $execution->unsetVariable(EcowasWorkflow::$ECOWAS_NAIRA_AMOUNT_VAR);
      $execution->unsetVariable(EcowasWorkflow::$ECOWAS_GATEWAY_TYPE_VAR);
    }  
   


    return true;
  }

  public function __toString() {
    return "Ecowas Payment Failure Actions";
  }
}
