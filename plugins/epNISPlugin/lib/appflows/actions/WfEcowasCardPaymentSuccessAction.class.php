<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasCardPaymentSuccessAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR);
    $transstatus = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANS_SUCCESS_VAR);
    $nairaAmount = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_GATEWAY_TYPE_VAR);

    //$gTypeName = Doctrine::getTable('PaymentGatewayType')->find(array($gatewayType));
    //print($gTypeName); die;
    //$gTypeName = $gTypeName->__toString();



    // TODO:
    // 1. update the ecowas application table with paid status set to true
    // 2.            and insert the transaction id in the same table
    // 3. If all done successfully return true
    // 4. If all not done successfully return false - log everything


    $q = Doctrine::getTable('EcowasCardApplication')
    ->createQuery('qr')->select('qr.ref_no,qr.processing_country_id,qr.processing_office_id')
    ->where('qr.id = ?', $appid)
    ->execute()->toArray();

    //get interview date
    $officeType = "";
    $processingOfficeId = "";


    $processingOfficeId =$q[0]['processing_office_id'];
    $officeType = 'ecowas';

    $interviewDate = InterviewSchedulerHelper::getInstance()->getInterviewDateForEcowasCard($q[0]['processing_country_id'],$appid,$processingOfficeId,$officeType);

/*
    //set default server time zone
    date_default_timezone_set(InterviewSchedulerHelper::getInstance()->get_server_time_zone());
 */

    // *  store the transaction-status payment-transaction-id in ecowas _application table
    $query = Doctrine_Query::create()
      ->update('EcowasCardApplication pa')
      ->set('pa.ispaid',$transstatus)
      ->set('pa.payment_trans_id',"'".$transid."'")
      ->set('pa.status',"'Paid'")
      ->set('pa.status_updated_date',"'".date('Y-m-d')."'")
      ->set('pa.paid_at',"'".date('Y-m-d')."'")
      ->set('pa.interview_date',"'".$interviewDate."'")
      ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
      ->set('pa.payment_gateway_id',"'".$gatewayType."'")
            ->where('pa.id = ?', $appid)
            ->execute();

    // *  store the app-id, refernce-id in ecowas_vetting_queue table
    $ecowasVettingQueue = new EcowasCardVettingQueue();
    $ecowasVettingQueue->setRefId($q[0]['ref_no']);
    $ecowasVettingQueue->setApplicationId($appid);
    $ecowasVettingQueue->setAttribute(Doctrine::ATTR_VALIDATE, Doctrine::VALIDATE_NONE);
    $ecowasVettingQueue->save();


    sfContext::getInstance()->getLogger()->err(
    "{Ecowas PaymentSuccessAction} Passed in var APPID:".$appid.
    ", Xns id:". $transid. ", XnsSts: ".$transstatus);

    return true;
  }

  public function __toString() {
    return "Ecowas Payment Successful Actions";
  }
}