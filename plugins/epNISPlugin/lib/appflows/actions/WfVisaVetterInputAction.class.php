<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

  class WfVisaVetterInputSuccessAction implements ezcWorkflowServiceObject {



  public function __construct()
  {

  }
  public function execute( ezcWorkflowExecution $execution ) {

 
    $appid = $execution->getVariable(VisaWorkflow::$VISA_APPLICATION_ID_VAR_FROM_VETTER);   

    //GET vettring grant status id from global table
    $gid = Doctrine::getTable('VisaVettingRecommendation')->getGrantId();

    $q = Doctrine::getTable('VisaVettingInfo')
    ->createQuery('qr')->select('recomendation_id')
    ->where('application_id = ?', $appid)
    ->execute()->toArray();
    
       
    if ( $q[0]['recomendation_id'] == $gid) {
      sfContext::getInstance()->getLogger()->info(
      "Visa Application Id:".$appid.", Vetter status:Approved");
      $execution->setVariable(VisaWorkflow::$VISA_SUCCESS_VAR_FROM_VETTER,true);
      return true;
    }
    else
    {
      sfContext::getInstance()->getLogger()->info(
      "Visa Application Id:".$appid.", Vetter status:Rejected");
      $execution->setVariable(VisaWorkflow::$VISA_SUCCESS_VAR_FROM_VETTER,false);
      return true;
    }
   
 }
  public function __toString() {
    return "Visa Vetter Successful Actions";
  }
}
