<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfEcowasCardPaymentFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {echo"fff";die;
    // get the transaction id
    $transid = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR);
    $transstatus = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANS_SUCCESS_VAR);
    $nairaAmount = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(EcowasCardWorkflow::$ECOWAS_CARD_GATEWAY_TYPE_VAR);


    sfContext::getInstance()->getLogger()->info(
      "{Ecowas PaymentFailureAction} Passed in var APPID:".$appid.
      ", Xns id:". $transid. ", XnsSts: ".$transstatus);

    if($execution->hasVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANSACTION_ID_VAR))
    {
      // *  store the transaction-status payment-transaction-id in passport _application table
      /*
      Doctrine_Query::create()
        ->update('EcowasApplication pa')
        ->set('pa.ispaid',0)
        ->set('pa.paid_at',"'".date('Y-m-d')."'")
        ->set('pa.payment_trans_id',$transid)
        ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
         ->set('pa.payment_gateway_id',"'".$gatewayType."'")
        ->where('pa.id = ?', $appid)
        ->execute();
      */
      $execution->unsetVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANSACTION_ID_VAR);
      $execution->unsetVariable(EcowasCardWorkflow::$ECOWAS_CARD_APPLICATION_ID_VAR);
      $execution->unsetVariable(EcowasCardWorkflow::$ECOWAS_CARD_TRANS_SUCCESS_VAR);
      $execution->unsetVariable(EcowasCardWorkflow::$ECOWAS_CARD_NAIRA_AMOUNT_VAR);
      $execution->unsetVariable(EcowasCardWorkflow::$ECOWAS_CARD_GATEWAY_TYPE_VAR);
    }



    return true;
  }

  public function __toString() {
    return "Ecowas Payment Failure Actions";
  }
}
