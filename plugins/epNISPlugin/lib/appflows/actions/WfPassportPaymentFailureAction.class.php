<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class WfPassportPaymentFailureAction implements ezcWorkflowServiceObject {
  public function execute( ezcWorkflowExecution $execution ) {
    // get the transaction id
    $transid = $execution->getVariable(PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR);
    $appid = $execution->getVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR);
    //$reffid = $execution->getVariable(PassportWorkflow::$PASSPORT_REFERENCE_ID);
    $transstatus = $execution->getVariable(PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR);

    $dollarAmount = $execution->getVariable(PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR);
    $nairaAmount = $execution->getVariable(PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR);
    $gatewayType = $execution->getVariable(PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR);

    
    sfContext::getInstance()->getLogger()->err(
      "{Passport PaymentFailureAction} Passed in var APPID:".$appid.
      ", Xns id:". $transid. ", XnsSts: ".$transstatus);

    if($execution->hasVariable(PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR))
    {
      // *  store the transaction-status payment-transaction-id in passport _application table
      /*
      Doctrine_Query::create()
        ->update('PassportApplication pa')
        ->set('pa.ispaid',0)
        ->set('pa.paid_at',"'".date('Y-m-d')."'")
        ->set('pa.payment_trans_id',$transid)
        ->set('pa.paid_dollar_amount',"'".$dollarAmount."'")
        ->set('pa.paid_naira_amount',"'".$nairaAmount."'")
         ->set('pa.payment_gateway_id',"'".$gatewayType."'")
        ->where('pa.id = ?', $appid)
        ->execute();
      */
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_TRANSACTION_ID_VAR);
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_APPLICATION_ID_VAR);
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_TRANS_SUCCESS_VAR);
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_DOLLAR_AMOUNT_VAR);
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_NAIRA_AMOUNT_VAR);
      $execution->unsetVariable(PassportWorkflow::$PASSPORT_GATEWAY_TYPE_VAR);
    }  
   


    return true;
  }

  public function __toString() {
    return "Passport Payment Failure Actions";
  }
}
