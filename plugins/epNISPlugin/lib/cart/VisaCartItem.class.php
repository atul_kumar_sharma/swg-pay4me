<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VisaCartItem
 *
 * @author apundir
 */
class VisaCartItem extends NisCartItem {

  /**
   * Construct a visa cart item.
   * @param <integer> $appid application id
   * @param <type> $name full name of the applicant
   */
  public function VisaCartItem($appid, $name, $fee, $type=INisCartItem::TYPE_VISA) {
    $this->setAppId($appid);
    $this->setName($name);
    $this->setPrice($fee);
    $this->setType($type);
  }

  public function getApplication() {
    return Doctrine::getTable("VisaApplication")->find($this->getAppId());
  }
}
?>
