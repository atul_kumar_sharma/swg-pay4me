<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PassportCartItem
 *
 * @author apundir
 */
class PassportCartItem extends NisCartItem {

  /**
   * Construct a passport application cart item.
   * @param <integer> $appid application id
   * @param <type> $name full name of the applicant
   */
  public function PassportCartItem($appid, $name, $fee) {
    $this->setAppId($appid);
    $this->setName($name);
    $this->setPrice($fee);
    $this->setType(INisCartItem::TYPE_PASSPORT);
  }

  public function getApplication() {
    return Doctrine::getTable("PassportApplication")->find($this->getAppId());
  }
}
?>
