<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NisCartItem
 *
 * @author apundir
 */
abstract class NisCartItem implements INisCartItem {
  private $app_type = null;
  private $app_id = 0;
  private $app_name = null;
  private $app_fee = 0;

  public function getType() {
    return $this->app_type;
  }

  protected function setType($app_type) {
    if ($app_type != INisCartItem::TYPE_PASSPORT &&
        $app_type != INisCartItem::TYPE_VISA &&
        $app_type != INisCartItem::TYPE_FREEZONE &&
            $app_type != INisCartItem::TYPE_VAP) {
      throw new Exception ("Unsupported app type");
    }
    $this->app_type = $app_type;
  }

  public function isPassport() {
    if ($this->app_type == INisCartItem::TYPE_PASSPORT) {
      return true;
    }
    return false;
  }

  public function isVisa() {
    if ($this->app_type == INisCartItem::TYPE_VISA) {
      return true;
    }
    return false;
  }

  public function getAppId() {
    return $this->app_id;
  }

  protected function setAppId($appid) {
    $this->app_id=$appid;
  }

  public function getName() {
    return $this->app_name;
  }

  protected function setName($name) {
    $this->app_name = $name;
  }

  public function setPrice($app_fee) {
    $this->app_fee = $app_fee;
  }

  public function getPrice() {
    return $this->app_fee;
  }

  public function getAppData() {
    $data =  $this->app_id.'~'.strtolower($this->app_type);
    $data = SecureQueryString::ENCRYPT_DECRYPT($data);
    $data =  $this->getAppIdToPaymentProcess = SecureQueryString::ENCODE($data);
    return $data;
  }

}
?>
