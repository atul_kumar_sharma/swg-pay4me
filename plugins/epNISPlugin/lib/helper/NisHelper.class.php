<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class NisHelper
{
  public static function getRefNo(){
    return (int) "1".rand(10000,99999).rand(1000,9999);
  }
  public static function getTransRefNo(){
    return (int) $randNum = rand(100000000000, 9999999999999) ;
  }

  public function isValidRefund($orderNumber){

         $detailsOnIpay4me = ipay4meOrderTable::getInstance()->getOrderNumberStatus($orderNumber);
         $ipayStatus = false;
         $nisStatus = false;
         if(isset ($detailsOnIpay4me) && is_array($detailsOnIpay4me) && count($detailsOnIpay4me)>0){
           if($detailsOnIpay4me[0]['status'] == 0)
            {
              $ipayStatus = true;
              $nisOrderDetails = CartItemsTransactionsTable::getInstance()->getOrderDetailsOnNis($orderNumber);
              if(isset ($nisOrderDetails) && is_array($nisOrderDetails) && count($nisOrderDetails)>0)
              {
                $nisStatus = true;
                foreach ($nisOrderDetails as $details){
                  if($details['status']=='Vetted' || $details['status']=='Approved'){
                    $nisStatus = false;
                    break;
                  }
                }
              }
            }
         }
         return array("nisStatus"=>$nisStatus,"ipay4meStatus"=>$ipayStatus);
//    return array("nisStatus"=>true,"ipay4meStatus"=>true);
  }

  public function revertNisApplicationStatusOLD($orderNumber){
    $nisOrderDetails = CartItemsTransactionsTable::getInstance()->getOrderDetailsOnNis($orderNumber);
    if(isset ($nisOrderDetails) && is_array($nisOrderDetails) && count($nisOrderDetails)>0)
    {
      foreach ($nisOrderDetails as $details){
        switch ($details['app_type']){
          case "passport" :

            $revertPayment = Doctrine_Query::create()
            ->update("PassportApplication")
            ->set("ispaid",'NULL')
            ->set("payment_trans_id",'Null')
            ->set("status","?","New")
            ->set("interview_date",'Null')
            ->set("payment_gateway_id",'Null')
            ->set("paid_dollar_amount",'Null')
            ->set("paid_local_currency_amount",'Null')
            ->set("paid_at",'Null')
            ->where("id=?",$details['id'])
            ->execute();
            $reverVettingQueue = PassportVettingQueueTable::getInstance()->findByApplicationId($details['id'])->delete();
            //delete from work flow tables
            $executionStatus = $this->deleteExecutionDetails("passport", $details['id']);
            break;

          case "visa":
            case "freezone":

              $revertPayment = Doctrine_Query::create()
              ->update("VisaApplication")
              ->set("ispaid",'NULL')
              ->set("payment_trans_id",'Null')
              ->set("status","?","New")
              ->set("interview_date",'Null')
              ->set("payment_gateway_id",'Null')
              ->set("paid_dollar_amount",'Null')
              ->set("paid_naira_amount",'Null')
              ->set("paid_at",'Null')
              ->where("id=?",$details['id'])
              ->execute();
              $reverVettingQueue = VisaVettingQueueTable::getInstance()->findByApplicationId($details['id'])->delete();
              //delete from work flow tables
              $executionStatus = $this->deleteExecutionDetails("visa", $details['id']);
              break;

          }
        }
        return true;
      }
    }

    protected function deleteExecutionDetails($appType, $appId){
      $execution_id = null;
      $workpoolArr = Doctrine_Query::create()
      ->select("t.execution_id")
      ->from("Workpool t")
      ->where("t.application_id=$appId")
      ->andWhere("t.flow_name='".ucwords($appType)."Workflow'")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(isset ($workpoolArr) && is_array($workpoolArr) && count($workpoolArr)>0){
        $execution_id = $workpoolArr[0]['execution_id'];
      }
      if(isset ($execution_id) && $execution_id!=null){
        try{
          $query1 = Doctrine_Query::create()
          ->delete("Workpool t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();

          $query2 = Doctrine_Query::create()
          ->delete("Execution t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();

          $query3 = Doctrine_Query::create()
          ->delete("ExecutionState t")
          ->where("t.execution_id=?",$execution_id)
          ->execute();
          return true;
        }catch(Exception $e){
          echo $e->getMessage();
          return false;
        }
      }
    }

    public function isAllReadyRefunded($orderNumber){
      //echo $appId;die;
      $finalArr = array();
      if(isset ($orderNumber) && $orderNumber!=0){
        $refundArr = RollbackPaymentTable::getInstance()->getOrderNumber($orderNumber);
        //echo "<pre>";print_r($refundArr);
        if(isset ($refundArr) && is_array($refundArr) && count($refundArr)>0){
          $finalArr['order_no'] = $refundArr[0]['order_number'];
          $finalArr['action'] = $refundArr[0]['RollbackPaymentDetails']['action'];
        }
      }
      return $finalArr;
    }

    public function revertNisApplicationStatus($orderNumber = null){

      $nisOrderDetails = CartItemsTransactionsTable::getInstance()->getOrderDetailsOnNis($orderNumber);      
      if(isset ($nisOrderDetails) && is_array($nisOrderDetails) && count($nisOrderDetails)>0)
      {
        foreach ($nisOrderDetails as $details){
          $blockApplicant = TblBlockApplicantTable::getInstance()->blockApplicant($details);
          $status = $this->revertStatus($details['id'],$details['app_type']);
        }
      }

      return true;
    }

    public function revertStatus($id,$app_type){

      switch (strtolower($app_type)){
        case "passport" :

          $revertPayment = Doctrine_Query::create()
          ->update("PassportApplication")
          ->set("ispaid",'NULL')
          ->set("payment_trans_id",'Null')
          ->set("status","?","New")
          ->set("interview_date",'Null')
          ->set("payment_gateway_id",'Null')
          ->set("paid_dollar_amount",'Null')
          ->set("paid_local_currency_amount",'Null')
          ->set("paid_at",'Null')
          ->set("amount",'Null')
          ->set("currency_id",'Null')
          ->where("id=?",$id)
          ->execute();
          $reverVettingQueue = PassportVettingQueueTable::getInstance()->findByApplicationId($id)->delete();
          //delete from work flow tables
          $executionStatus = $this->deleteExecutionDetails("passport", $id);
          break;
        case "visa":
          case "freezone":

            $revertPayment = Doctrine_Query::create()
            ->update("VisaApplication")
            ->set("ispaid",'NULL')
            ->set("payment_trans_id",'Null')
            ->set("status","?","New")
            ->set("interview_date",'Null')
            ->set("payment_gateway_id",'Null')
            ->set("paid_dollar_amount",'Null')
            ->set("paid_naira_amount",'Null')
            ->set("paid_at",'Null')
            ->set("amount",'Null')
            ->set("currency_id",'Null')
            ->where("id=?",$id)
            ->execute();
            $reverVettingQueue = VisaVettingQueueTable::getInstance()->findByApplicationId($id)->delete();
            //delete from work flow tables
            $executionStatus = $this->deleteExecutionDetails("visa", $id);
            break;

          case "vap" :
              
          $revertPayment = Doctrine_Query::create()
          ->update("VapApplication")
          ->set("ispaid",'NULL')
          ->set("payment_trans_id",'Null')
          ->set("status","?","New")
          //->set("interview_date",'Null')
          ->set("payment_gateway_id",'Null')
          ->set("paid_dollar_amount",'Null')
          ->set("paid_naira_amount",'Null')
          ->set("paid_date",'Null')
          ->where("id=?",$id)
          ->execute();
          break;

        }
        $this->sendEmailToApplicant($id,$app_type);
        return true;
      }

      public function isAllAppReadyRefunded($orderNumber,$appId){
        //echo $appId;die;
        $finalArr = array();
        if(isset ($orderNumber) && $orderNumber!=0){
          $refundArr = RollbackPaymentTable::getInstance()->getOrderAppNumber($orderNumber,$appId);
          //echo "<pre>";print_r($refundArr);
          if(isset ($refundArr) && is_array($refundArr) && count($refundArr)>0){
            $finalArr['order_no'] = $refundArr[0]['order_number'];
            $finalArr['action'] = $refundArr[0]['RollbackPaymentDetails']['action'];
          }
        }
        return $finalArr;
      }
      public function sendEmailToApplicant($id,$app_type)
      {
        $notificationUrl = "notification/emailToNisApplicant" ;
        $taskId = EpjobsContext::getInstance()->addJob('emailToNisApplicant',$notificationUrl, array('appId' => $id,'appType'=>$app_type));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");

      }
    }