<?php

/**
 * PluginColorHairStatusTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginColorHairStatusTable extends GlobalMasterTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginColorHairStatusTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginColorHairStatus');
    }
}