<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentVerifyclass
 *
 * @author akumar1
 */
class PaymentVerify {
  //put your code here

  public function paymentVerifyOnBackend($orderId, $merchantId = '', $amount = '')
  {
    if($merchantId == '' && $amount == '')
    {
      $data = $this->getMerchantIdAndAmount($orderId);
      if(count($data) == 0)
      return 0;
      $merchantId = $data['1'];
      $amount = $data['2'];
    }
    //turn off WSDP caching if not in a production environment
    $ini = ini_set("soap.wsdl_cache_enabled","1");
    $insertedId = $this->verifyRequest($orderId);
    //instantiate the SOAP client
    $url = sfConfig::get('app_web_url');
    $client = new SoapClient($url);
    $params = array();
    $params["merchantID"] = $merchantId;
    $params["orderID"] = $orderId;
    $soapResponse = $client->TransactionStatus($params)->TransactionStatusResult;

    $this->updateResponse($insertedId, $soapResponse);

    $isvalidPayment = $this->validate($soapResponse, $amount);

    return $isvalidPayment;
  }

  private function getMerchantIdAndAmount($orderId)
  {
    $data = array();
    $q = Doctrine_Query::create()
    ->select('id, mer_id, purchase_amount')
    ->from('EpVbvRequest')
    ->where('order_id = ?',$orderId)
    ->fetchArray();
    if(count($q)>0)
    {
      $data['1'] = $q[0]['mer_id'];
      $data['2'] = $q[0]['purchase_amount'];
    }

    return $data;
  }

  private function validate($soapResponse, $amount)
  {
    $resArray =  explode('~', $soapResponse);
    if($resArray['0'] == '001' && $resArray['2'] == $amount)
    {
      return 1;
    }else{
      return 0;
    }
  }

  protected function verifyRequest($orderId)
  {
    $obj = new EpVbvVerify();
    $obj->setOrderId ($orderId);
    $obj->save();
    return $obj->getId();
  }

  protected function updateResponse($id, $result)
  {
    $FpsDetailObj = Doctrine::getTable('EpVbvVerify')->find($id);

    $FpsDetailObj->setTransactionstatusresult($result);
    $FpsDetailObj->save();

  }

}
?>
