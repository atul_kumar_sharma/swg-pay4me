<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class epSmsFactory{
    public static function getSmsGatewayObject() {
        $gateway=sfConfig::get('app_active_gateway');
        switch($gateway)
        {
            case 'infobib':
                $smsObj=new epInfobibManager();
                break;
            case 'clickatell':
                $smsObj=new epClickatellManager();
                break;
        }
        return $smsObj;

    }
}

?>
