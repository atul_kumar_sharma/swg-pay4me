<?php

class epInfobibManager {


    public function sendSms($mobileNumber,$message, $smsMethod, $senderName='', $options= array()){
        if($senderName=='')
        {
            $senderName=sfConfig::get('app_infobib_sender');
        }
        if(strtolower($smsMethod)=='post')
        {
            $xmlData= $this->generateSmsXml($mobileNumber, $message, $senderName, $options);
            $getStatus= $this->postXML($xmlData);
        }

        else if(strtolower($smsMethod)=='get')
        {
            $getStatus= $this->sendSmsGet($mobileNumber,$message, $senderName, $options);

        }

        return $getStatus;
    }


    private function generateSmsXml($mobileNumber,$message, $senderName, $options= array())
    {

        $xml_data = new gc_XmlBuilderSms();
        $xml_data->Push('SMS');
        $xml_data->Push('authentification');
        $xml_data->Element('username', sfConfig::get('app_infobib_username'));
        $xml_data->Element('password', sfConfig::get('app_infobib_password'));
        $xml_data->Pop('authentification');
        $xml_data->Push('message');
        $xml_data->Element('sender', $senderName);
        $xml_data->Element('text', $message);
        foreach($options as $key=>$val){
            $xml_data->Element($key, $val);
        }

        $xml_data->Pop('message');
        $xml_data->Push('recipients');
        $xml_data->Element('gsm', $mobileNumber);
        $xml_data->Pop('recipients');
        $xml_data->Pop('SMS');
        return $xml_data->GetXML();

    }
    private function postXML($xmlData)
    {
        $response= array();
        $browser = $this->getBrowser();
        $header['Content-Type'] =  "application/xml;charset=UTF-8";
        $header['Accept'] = "application/xml;charset=UTF-8";
        $uri = sfConfig::get('app_infobib_posturl');
        $browser->post($uri, $xmlData, $header);
        $xdoc = new DomDocument;
        $responseXML= $browser->getResponseText();
        if($responseXML!='' && $xdoc->LoadXML($responseXML))
        {

            switch($xdoc->getElementsByTagName('status')->item(0)->nodeValue)
            {
                case '-1':
                    $msg_sms= "AUTH_FAILED";
                    break;
                case '-2':
                    $msg_sms= "XML_ERROR";
                    break;
                case '-3':
                    $msg_sms= "NOT_ENOUGH_CREDITS";
                    break;
                case '-4':
                    $msg_sms= "NO_RECIPIENTS";
                    break;
                case '-5':
                    $msg_sms= "GENERAL_ERROR";
                    break;
                default:
                    $msg_sms= "SEND_OK";

                }
                $response['status']=  $msg_sms;
                $response['credits']=  $xdoc->getElementsByTagName('credits')->item(0)->nodeValue;
            }
            return $response;

        }



        private function sendSmsGet($mobileNumber,$message, $senderName, $options)
        {
            $optional_param='';
            $browser = $this->getBrowser();
            $uri= sfConfig::get('app_infobib_geturl');

            foreach($options as $key=>$val){
                $optional_param.= ",'".$key."'=>".$val;
            }


            $parameters= array('user'=>sfConfig::get('app_infobib_username'),'password'=>sfConfig::get('app_infobib_password'),'sender'=>$senderName,'SMSText'=>$message, 'GSM'=>$mobileNumber. $optional_param );
            $browser->get($uri,$parameters);
            $responce_code= $browser->getResponseText();

            switch($responce_code)
            {
                case '-1':
                    $msg_sms= "SEND_ERROR";
                    break;
                case '-2':
                    $msg_sms= "NOT_ENOUGHCREDITS";
                    break;
                case '-3':
                    $msg_sms= "NETWORK_NOTCOVERED";
                    break;
                case '-4':
                    $msg_sms= "SOCKET_EXCEPTION";
                    break;
                case '-5':
                    $msg_sms= "INVALID_USER_OR_PASS";
                    break;
                case '-6':
                    $msg_sms= "MISSING_DESTINATION_ADDRESS";
                    break;
                case '-7':
                    $msg_sms= "MISSING_SMSTEXT";
                    break;
                case '-8':
                    $msg_sms= "MISSING_SENDERNAME";
                    break;
                case '-9':
                    $msg_sms= "DESTADDR_INVALIDFORMAT";
                    break;
                case '-10':
                    $msg_sms= "MISSING_USERNAME";
                    break;
                case '-11':
                    $msg_sms= "MISSING_PASS";
                    break;
                case '-13':
                    $msg_sms= "INVALID_DESTINATION_ADDRESS";
                    break;
                default:
                    $msg_sms= "SEND_OK";

                }
                return $msg_sms;

            }

   /**
  * Silent posting of request XML with node value.
  *
  */
            private function getBrowser() {
                if(!@$this->browserInstance) {
                    $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
                        array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
                }
                return $this->browserInstance;
            }
        }
        ?>
