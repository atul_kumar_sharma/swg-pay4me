<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class EpPasswordPolicyManager
{
  public static function evaluateSiginPolicy($userId,$password=null){

    $returnObj = Doctrine::getTable('EpPasswordPolicy')->checkPasswordAndExpiry($userId, $password);
    if(is_object($returnObj)){

      $daysInExpiry = self::getDaysInExpiray($returnObj->getCreatedAt());
      if(sfConfig::get('app_pass_exp_days')>=$daysInExpiry){
        return (sfConfig::get('app_pass_exp_days') - $daysInExpiry);
      }else{
        return false;
      }
    }
  }

  public static function getDaysInExpiray($createdDate)
  {
    $dformat = '-';
    $beginDate = date('Y-m-d', strtotime($createdDate));
    $endDate = date('Y-m-d');
    $date_parts1=explode($dformat, $beginDate);
    $date_parts2=explode($dformat, $endDate);
    $start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
    $end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
    return  ($end_date - $start_date);
  }

  public static function doChangePassword($userId,$password){
    Doctrine::getTable('EpPasswordPolicy')->changePassword($userId, $password);

  }

  public static function checkPreviousPasswords($userId, $password, $salt, $algo){

    $password = call_user_func_array($algo, array($salt.$password));
    return Doctrine::getTable('EpPasswordPolicy')->checkPreviousPasswords($userId, $password);
  }
  public static function getOldPasswordLimit(){
    return sfConfig::get('app_number_of_password');
  }
  public static function getNumberOfDaysInExpiry(){
    return sfConfig::get('app_pass_exp_days');
  }

  public static function savePasswordForNewRegis($userId,$password){
    Doctrine::getTable('EpPasswordPolicy')->savePasswordForNewRegis($userId, $password);

  }

  public static function checkPasswordComplexity($password){
   //  if(preg_match('/[0-9A-Za-z]+[\!\#\$\%\&\'\*\+\-\_\/\=\?\^\`\{\|\}\~\]\.\)\(\}\{\@\|\\\"\;\:\,\?]/',$password)){
   if(preg_match('/.*^(?=.{8,20})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*\W).*$/',$password)){
      return true;
    }else   
    return false;
  }
}
?>
