#!/bin/bash


## Few variables of interest
# compress the log files after following days
COMPRESS_LOGS_AFTER_DAYS=2
# delete the logs files older than these many days
DELETE_LOGS_AFTER_DAYS=30

# execution count per iteration, after each iteration compression and
# cleanup activities will be performed
EXEC_COUNT_PER_ITR=200
# delay (in seconds) between two consecutive cmd executions
EXEC_REPEAT_DELAY=2

# File which contains the day of the month on which last cleanup occured
LAST_CLEANUP_DAY_FNAME="eplog-cleanup.day"

# Log file location and naming convention
LOG_FILE_PREFIX="run-sch"
LOG_FILE_EXT="log"
LOG_FILE_DIR_BASE="eplogs"

#### Usually you shouldn't have to edit anything below this line ######
########################################################################

pslist=/bin/ps
selectedPs=""
md5cmd=md5sum

if [ "$EXEC_COUNT_PER_ITR" -lt 1 ]; then
  echo "EXEC_COUNT_PER_ITR can not be less than 1"
  exit
fi

if [ $# -lt 2 ]; then
  echo "Usage: "
  echo "   $0 <php5-executable-path> <timezone>"
  echo " where "
  echo "  php5-executable-path: Absolute path of php5 executable"
  echo "  timezone: Timezone of your php web server environment"
  echo ""
  echo "It's important to set the timezone to match with your web php"
  echo "  environment as otherwise the tasks may execute earlier/later"
  echo "  due to diff between current system timezone and php-web timezone"
  echo ""
  echo "  example: $0 /usr/bin/php Africa/Lagos"
  echo ""
  exit 1
fi

## Change the path so that we have all required commands in path for script
export PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin/usr/local/sbin:$PATH

export TZ=$2

########################################################################
#### Following code heavily inspired from
#### http://stackoverflow.com/questions/7665/how-to-resolve-symbolic-links-in-a-shell-script
####
# get the absolute path of the executable
function getAbsResolvedPathAndName() {
  SELF_PATH=$(cd -P -- "$(dirname -- "$1")" && pwd -P) && SELF_PATH=$SELF_PATH/$(basename -- "$1")

  # resolve symlinks
  while [ -h $SELF_PATH ]; do
    # 1) cd to directory of the symlink
    # 2) cd to the directory of where the symlink points
    # 3) get the pwd
    # 4) append the basename
    DIR=$(dirname -- "$SELF_PATH")
    if [ "$SELF_PATH" == "" ]; then
      return
    fi
    SYM=$(readlink $SELF_PATH)
    SELF_PATH=$(cd $DIR && cd $(dirname -- "$SYM") && /bin/pwd -P)/$(basename -- "$SYM")
  done
}
########################################################################

function getPidFileName() {
  myPidFile="/tmp/$myBaseName.$myMd5"
}

function isSchedulerAlreadyRunning() {
  ## does the pid file exists
  if [ -f $myPidFile ]
  then
    echo "File exists"
    pidInFile=`cat $myPidFile`
    oldPidProcName=`$selectedPs --pid $pidInFile -o cmd --no-heading | cut -d " " -f2`
    if [ "$oldPidProcName" = "" ]
    then
      ## no process with this pid
      return 1;
    fi
    oldPidProcBaseName=$(basename -- $oldPidProcName)
    echo "$oldPidProcBaseName -- $oldPidProcName"
    if [ "$oldPidProcBaseName" = "$myBaseName" ] 
    then
      echo "Matched the process, sched already running"
      return 0
    fi
  fi
  return 1;
}

function createPidFile() {
  /bin/echo -n $$ > $myPidFile
  ## ensure that this file is deleted at end of script execution
  trap "rm -f $myPidFile >/dev/null 2>&1" 0
}

function isSuitablePs() {
  $1 --help | grep -- "--pid"
  return $?
}

function locatePs() {
  for aps in $pslist
  do
    isGnu=$(isSuitablePs $aps)
    if [ "$?" = "0" ]
    then
      selectedPs=$aps;
      return 0;
    fi
  done
  return 1;
}


function perform_compress_and_cleanup ()
{
  # compress the old log files now
  find . -name "$LOG_FILE_PREFIX-*.$LOG_FILE_EXT" -mtime +$COMPRESS_LOGS_AFTER_DAYS -exec bzip2 '{}' \;

  # delete the log files now
  find . -name "$LOG_FILE_PREFIX-*.$LOG_FILE_EXT.bz2" -mtime +$DELETE_LOGS_AFTER_DAYS -delete

  # keep a last cleanup day in file
  echo -n `date +%d` > $LAST_CLEANUP_DAY_FNAME
}

function compress_and_cleanup()
{
  old_wd=`pwd`
  cd $LOG_DIR
  if [ -f "$LAST_CLEANUP_DAY_FNAME" ]; then
    LAST_CDAY=`cat $LAST_CLEANUP_DAY_FNAME`
    if [ "$LAST_CDAY" != `date +%d` ]; then
      perform_compress_and_cleanup
    fi
  else
    perform_compress_and_cleanup
  fi
  cd $old_wd
}

function check_for_change_in_links()
{
  oldAbsPath=$myAbsPath
  getAbsResolvedPathAndName $invokedCmdLine
  newAbsPath=$SELF_PATH
  if [ "$oldAbsPath" != "$newAbsPath" ]; then
    echo "!!! Original script is no longer present at same path !!!"
    echo "!!! Looks like the symbolic links are changed !!!"
    echo "!!! Terminating myself now !!!"
    echo "!! New script shall invoke itself from cron if scheduled."
    exit 0
  fi
}

############################################################
#### All Functions definition Ends here                #####
#### Let us start the actual execution now             #####
############################################################

invokedCmdLine=$0

## Absolutely resolve the path and links of current file
getAbsResolvedPathAndName $invokedCmdLine

myAbsPath=$SELF_PATH
myBaseName=$(basename -- "$myAbsPath")

## Find out if we have md5sum installed or not
which md5sum >/dev/null
if [ "$?" -ne "0" ]
then
  echo "Unable to find md5sum command, please install it on this system first!!"
  exit 1
fi

## Find a suitable 'ps' command that we can use
locatePs

if [ "$?" -ne "0" ]
then
  echo "Unable to find suitable ps command!!"
  exit 1
fi

## calculate hash of my absolute path
myMd5=`echo $myAbsPath | $md5cmd - | cut -d " " -f1`

## Generate the pid file name for this scheduler
myPidFile="";
getPidFileName

## see if existing schedule already running or not
isSchedulerAlreadyRunning
if [ "$?" -eq "0" ]
then
  echo "Existing Schedule Running, aborting fresh launch"
  exit 0;
fi

## create the pid file first
createPidFile

## now do your job

## TODO: review following code for optimization ##
BASE_DIR=$(dirname -- "$myAbsPath")
BASE_DIR=$(dirname -- "$BASE_DIR")

BASE_BASE=$(basename -- "$BASE_DIR")
PG_BASE="epJobSchedulerPlugin"
if [ "$BASE_BASE" == "$PG_BASE" ]; then
  BASE_DIR=$(dirname -- "$BASE_DIR")
  BASE_DIR=$(dirname -- "$BASE_DIR")
fi

cd $BASE_DIR

SYMFONY_SCRIPT=$BASE_DIR/symfony
SYMFONY_ARGS="--trace epjobs:run-schedule"

LOG_DIR=$BASE_DIR/log/$LOG_FILE_DIR_BASE
mkdir -p $LOG_DIR

############################################################
##### This is from where all the php execution starts ######
#echo "Will execute $PHP_PATH $SYMFONY_SCRIPT $SYMFONY_ARGS"
############################################################

while `/bin/true`
do
  for ((a=1; a <= EXEC_COUNT_PER_ITR ; a++))
  do
    check_for_change_in_links
    CDATE=$(date +%d-%b-%y);
    LOG_FILE=$LOG_DIR/$LOG_FILE_PREFIX-$CDATE.$LOG_FILE_EXT
    $PHP_PATH $SYMFONY_SCRIPT $SYMFONY_ARGS >>$LOG_FILE 2>&1
    sleep $EXEC_REPEAT_DELAY
  done
  compress_and_cleanup
done
####### This is where execution of the schedule ends ######
############################################################

