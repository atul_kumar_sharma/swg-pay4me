<?php
class EpUnitTestCase {

  public static function checkScheduledCase($t){
    ### Unit Testing Case 1 ###
    $minutes = "32,52,56,58";
    $hours = "21,10,11,20,8";
    $dayofmonth ="15,21,23,25,30";
    $month = "1,2,3,4,5";
    $dayofweek = null;
    $nextSchDate = '2010-5-15 8:32:00';

    $obj = new EpCronScheduler($month, $dayofmonth, $dayofweek, $hours, $minutes);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate, $return, 'Condition is true');

    ### Unit Testing Case 2 ###
    $minutes1 = "46-55,20-23,25";
    $hours1 = "1-5,6,7";
    $dayofmonth1 ="15,10-16,25,30";
    $month1 = "4-6";
    $dayofweek1 = null;
    $nextSchDate1 = '2010-5-10 1:20:00';

    $obj = new EpCronScheduler($month1, $dayofmonth1, $dayofweek1, $hours1, $minutes1);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate1, $return, 'Condition is true');

    ### Unit Testing Case 3 ###
    $minutes3 = "46-55,25";
    $hours3 = "1-5,6,7";
    $dayofmonth3 ="15,10-16,25,30";
    $month3 = "4-6";
    $dayofweek3 = null;
    $nextSchDate3 = '2010-5-10 1:25:00';

    $obj = new EpCronScheduler($month3, $dayofmonth3, $dayofweek3, $hours3, $minutes3);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate3, $return, 'Condition is true');


    ### Unit Testing Case 4 Bundary values ###
    $minutes4 = "0-59";
    $hours4 = "0-23";
    $dayofmonth4 ="1-31";
    $month4 = "1-12";
    $dayofweek4 = null;
    $nextSchDate4 = '2010-5-4 11:3:00';


    $obj = new EpCronScheduler($month4, $dayofmonth4, $dayofweek4, $hours4, $minutes4);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate4, $return, 'Condition is true for bounday values');

    ### Unit Testing Case 5 for null valuee ###
    $minutes5 = "*";
    $hours5 = "*";
    $dayofmonth5 ="*";
    $month5 = "*";
    $dayofweek5 = null;
    $nextSchDate5 = '2010-5-3 11:3:00';

    $obj = new EpCronScheduler($month5, $dayofmonth5, $dayofweek5, $hours5, $minutes5);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate5, $return, 'Condition is true for null valuee');


    ### Unit Testing Case 6 for leap year ###
    $minutes6 = "25-45";
    $hours6 = "16-23";
    $dayofmonth6 ="29";
    $month6 = "2";
    $dayofweek6 = null;
    $nextSchDate6 = '2012-2-29 16:25:00';
    try{
      $obj = new EpCronScheduler($month6, $dayofmonth6, $dayofweek6, $hours6, $minutes6);
      $return = $obj->getNextScheduleTime();
      $t->fail('no code should be executed after throwing an exception');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }


    ### Unit Testing Case 7 for 31 days ###
    $minutes7 = "35-59";
    $hours7 = "19-23";
    $dayofmonth7 ="31";
    $month7 = "6-8";
    $dayofweek7 = null;
    $nextSchDate7 = '2010-7-31 19:35:00';

    $obj = new EpCronScheduler($month7, $dayofmonth7, $dayofweek7, $hours7, $minutes7);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate7, $return, 'Condition is true for 31 days');

    ### Unit Testing Case 8 for 30 days ###
    $minutes8 = "35-59";
    $hours8 = "19-23";
    $dayofmonth8 ="30";
    $month8 = "2,3";
    $dayofweek8 = null;
    $nextSchDate8 = '2011-3-30 19:35:00';

    $obj = new EpCronScheduler($month8, $dayofmonth8, $dayofweek8, $hours8, $minutes8);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate8, $return, 'Condition is true for 30 days');

    ### Unit Testing Case 9 for weekdays  ###
    $minutes9 = "35-59";
    $hours9 = "19-23";
    $dayofmonth9 =null;
    $month9 = "1-12";
    $dayofweek9 = "0-6";
    $nextSchDate9 = '2010-5-3 19:35:00';

    $obj = new EpCronScheduler($month9, $dayofmonth9, $dayofweek9, $hours9, $minutes9);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate9, $return, 'Condition is true for weekdays');

    ### Unit Testing Case 10 for weekdays(only for sunday and  thurusday) ###
    $minutes10 = "0-59";
    $hours10 = "0-23";
    $dayofmonth10 =null;
    $month10 = "1-12";
    $dayofweek10 = "0,4";
    $nextSchDate10 = '2010-5-6 0:0:00';

    $obj = new EpCronScheduler($month10, $dayofmonth10, $dayofweek10, $hours10, $minutes10);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate10, $return, 'Condition is true for weekdays (only for sunday and  thurusday)');

    ### Unit Testing Case 11 for weekdays(only for monday,tuesday and thurusday)###
    $minutes11 = "2-59,1";
    $hours11 = "5,12-23";
    $dayofmonth11 =null;
    $month11 = "1-12";
    $dayofweek11 = "4,1-2";
    $nextSchDate11 = '2010-5-3 12:1:00';

    $obj = new EpCronScheduler($month11, $dayofmonth11, $dayofweek11, $hours11, $minutes11);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate11, $return, 'Condition is true for weekdays (only for monday,tuesday and thurusday)');

    ### Unit Testing Case 12 for febuarary 2000###
    $minutes12 = "2-59,1";
    $hours12 = "5,12-23";
    $dayofmonth12 ="29";
    $month12 = "2";
    $dayofweek12 = null;
    $nextSchDate12 = '2012-2-29 5:1:00';
    try{
      $obj = new EpCronScheduler($month12, $dayofmonth12, $dayofweek12, $hours12, $minutes12);
      $return = $obj->getNextScheduleTime();
      $t->fail('no code should be executed after throwing an exception');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 13 for every minutes and every hours ###
    $minutes13 = "*";
    $hours13 = "*";
    $dayofmonth13 ="29,30-31";
    $month13 = "2-4";
    $dayofweek13 = null;
    $nextSchDate13 = '2011-3-29 0:0:00';

    $obj = new EpCronScheduler($month13, $dayofmonth13, $dayofweek13, $hours13, $minutes13);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate13, $return, 'Condition is true for every minutes and every hours');


    ### Unit Testing Case 14 for every minutes in defined hours ###
    $minutes14 = "*";
    $hours14 = "5-12,14-28,30";
    $dayofmonth14 ="29,30-31";
    $month14 = "2-4";
    $dayofweek14 = null;
    $nextSchDate14 = '2011-3-29 5:0:00';

    $obj = new EpCronScheduler($month14, $dayofmonth14, $dayofweek14, $hours14, $minutes14);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate14, $return, 'Condition is true for every minutes and every hours  in defined hours');

    ### Unit Testing Case 15 for every day and every minutes in defined hours ###
    $minutes15 = "*";
    $hours15 = "5-12,14-28,30";
    $dayofmonth15 ="*";
    $month15 = "2-4";
    $dayofweek15 = null;
    $nextSchDate15 = '2011-2-1 5:0:00';

    $obj = new EpCronScheduler($month15, $dayofmonth15, $dayofweek15, $hours15, $minutes15);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate15, $return, 'Condition is true for every day and every minutes in defined hours');


    ### Unit Testing Case 16 for every month and every hours in defined day of month ###
    $minutes16 = "20-23";
    $hours16 = "*";
    $dayofmonth16 ="28-31";
    $month16 = "*";
    $dayofweek16 = null;
    $nextSchDate16 = '2010-5-28 0:20:00';

    $obj = new EpCronScheduler($month16, $dayofmonth16, $dayofweek16, $hours16, $minutes16);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate16, $return, 'Condition is true for every month and every hours in defined day of month');

    ### Unit Testing Case 17 for every format ###
    $minutes17 = "20-23,56,2-8";
    $hours17 = "*";
    $dayofmonth17 =null;
    $month17 = "1-3,4,5-8";
    $dayofweek17 = "1-2,4";
    $nextSchDate17 = '2010-5-3 14:4:00';

    $obj = new EpCronScheduler($month17, $dayofmonth17, $dayofweek17, $hours17, $minutes17);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate17, $return, 'Condition is true for every format');

    ### Unit Testing Case 18 for every format ###
    $minutes18 = "20-23,56,2-8";
    $hours18 = "*";
    $dayofmonth18 =null;
    $month18 = "1-3,4,5-8";
    $dayofweek18 = "1-2,4,0-2";
    $nextSchDate18 = '2010-5-3 14:4:00';

    $obj = new EpCronScheduler($month18, $dayofmonth18, $dayofweek18, $hours18, $minutes18);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate18, $return, 'Condition is true for every format');


    ### Unit Testing Case 19 for every format ###
    $minutes19 = "8-10";
    $hours19 = "*";
    $dayofmonth19 =null;
    $month19 = "1-3,4,5-8";
    $dayofweek19 = "1-2,4,0-2";
    $nextSchDate19 = '2010-5-3 16:8:00';

    $obj = new EpCronScheduler($month19, $dayofmonth19, $dayofweek19, $hours19, $minutes19);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate19, $return, 'Condition is true for every format');


    ### Unit Testing Case 20 for every format ###
    $minutes20 = "8-10,4-25";
    $hours20 = "5-10,2-8";
    $dayofmonth20 =null;
    $month20 = "1-3,4,5-8";
    $dayofweek20 = "1-2,4,0-2";
    $nextSchDate20 = '2010-5-4 2:4:00';

    $obj = new EpCronScheduler($month20, $dayofmonth20, $dayofweek20, $hours20, $minutes20);
    $return = $obj->getNextScheduleTime();
    $t->is($nextSchDate20, $return, 'Condition is true for every format');


    ### Unit Testing Case 21 for every format ###
    $minutes21 = "8-10,25-4";
    $hours21 = "10-5,8-2";
    $dayofmonth21 =null;
    $month21 = "1-3,4,5-8";
    $dayofweek21 = "1-2,4,0-2";
    $nextSchDate21 = '2010-5-4 2:4:00';

    try{
      $obj = new EpCronScheduler($month21, $dayofmonth21, $dayofweek21, $hours21, $minutes21);
      $return = $obj->getNextScheduleTime();
      $t->fail('no code should be executed after throwing an exception');
    }
    catch(Exception $e){
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }



    ### Unit Testing Case 22 for every format ###
    $minutes22 = "8-10,4-25";
    $hours22 = "5-10";
    $dayofmonth22 ="15-25";
    $month22 = "1-3,4,5-8";
    $dayofweek22 = null;
    $nextSchDate22 = '2010-7-15 5:4:00';
    $startTime = "2010-7-1 11:3:00";
    try{
      $obj = new EpCronScheduler($month22, $dayofmonth22, $dayofweek22, $hours22, $minutes22);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate22, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 23 for every format ###
    $minutes23 = "8-10,4-25";
    $hours23 = "5-10";
    $dayofmonth23 ="15-25";
    $month23 = "1-3,4,5-8";
    $dayofweek23 = null;
    $nextSchDate23 = '2010-7-15 5:4:00';
    $startTime = "2012-7-1 11:3:00";
    try{
      $obj = new EpCronScheduler($month23, $dayofmonth23, $dayofweek23, $hours23, $minutes23);
      $return = $obj->getNextScheduleTime($startTime);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

  /*
   * Test Case for interval schedule time
   */
    ### Unit Testing Case 24 for every format ###
    $minutes24 = "*/5";
    $hours24 = "5-10";
    $dayofmonth24 ="15-25";
    $month24 = "1-3,4,5-8";
    $dayofweek24 = null;
    $nextSchDate24 = '2011-7-15 5:0:00';
    $startTime = "2011-7-15 11:3:00";
    try{
      $obj = new EpCronScheduler($month24, $dayofmonth24, $dayofweek24, $hours24, $minutes24);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate24, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 25 for every format ###
    $minutes25 = "*/7";
    $hours25 = "5-10";
    $dayofmonth25 ="15-25";
    $month25 = "1-3,4,5-8";
    $dayofweek25 = null;
    $nextSchDate25 = '2011-7-15 5:7:00';
    $startTime = "2011-7-15 5:3:00";
    try{
      $obj = new EpCronScheduler($month25, $dayofmonth25, $dayofweek25, $hours25, $minutes25);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate25, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 26 for every format ###
    $minutes26 = "*/7";
    $hours26 = "5-10";
    $dayofmonth26 ="15-25";
    $month26 = "1-3,4,5-8";
    $dayofweek26 = null;
    $nextSchDate26 = '2011-7-15 5:14:00';
    $startTime = "2011-7-15 5:10:00";
    try{
      $obj = new EpCronScheduler($month26, $dayofmonth26, $dayofweek26, $hours26, $minutes26);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate26, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 27 for every format ###
    $minutes27 = "*/7";
    $hours27 = "*/2";
    $dayofmonth27 ="15-25";
    $month27 = "1-3,4,5-8";
    $dayofweek27 = null;
    $nextSchDate27 = '2011-7-15 6:0:00';
    $startTime = "2011-7-15 5:10:00";
    try{
      $obj = new EpCronScheduler($month27, $dayofmonth27, $dayofweek27, $hours27, $minutes27);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate27, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 28 for every format ###
    $minutes28 = "*/7";
    $hours28 = "*/2";
    $dayofmonth28 ="15-25";
    $month28 = "1-3,4,5-8";
    $dayofweek28 = null;
    $nextSchDate28 = '2011-7-15 6:21:00';
    $startTime = "2011-7-15 6:15:00";
    try{
      $obj = new EpCronScheduler($month28, $dayofmonth28, $dayofweek28, $hours28, $minutes28);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate28, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 29 for every format ###
    $minutes29 = "*/7";
    $hours29 = "*/2";
    $dayofmonth29 ="*/8";
    $month29 = "1-3,4,5-8";
    $dayofweek29 = null;
    $nextSchDate29 = '2011-7-17 0:0:00';
    $startTime = "2011-7-15 6:15:00";
    try{
      $obj = new EpCronScheduler($month29, $dayofmonth29, $dayofweek29, $hours29, $minutes29);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate29, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 30 for every format ###
    $minutes30 = "*/7";
    $hours30 = "*/2";
    $dayofmonth30 ="*/8";
    $month30 = "1-3,4,5-8";
    $dayofweek30 = null;
    $nextSchDate30 = '2011-7-17 6:21:00';
    $startTime = "2011-7-17 6:15:00";
    try{
      $obj = new EpCronScheduler($month30, $dayofmonth30, $dayofweek30, $hours30, $minutes30);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate30, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 31 for every format ###
    $minutes31 = "*/7";
    $hours31 = "*/2";
    $dayofmonth31 ="*/8";
    $month31 = "1-3,4,5-8";
    $dayofweek31 = null;
    $nextSchDate31 = '2011-10-1 0:0:00';
    $startTime = "2011-7-17 6:15:00";
    try{
      $obj = new EpCronScheduler($month31, $dayofmonth31, $dayofweek31, $hours31, $minutes31);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate30, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 32 for every format ###
    $minutes32 = "*/7";
    $hours32 = "*/2";
    $dayofmonth32 ="*/8";
    $month32 = "*/3";
    $dayofweek32 = null;
    $nextSchDate32 = '2011-10-9 6:21:00';
    $startTime = "2011-10-9 6:15:00";
    try{
      $obj = new EpCronScheduler($month32, $dayofmonth32, $dayofweek32, $hours32, $minutes32);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate32, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 33 for every format ###
    $minutes33 = "*/7";
    $hours33 = "*/2";
    $dayofmonth33 =null;
    $month33 = "*/3";
    $dayofweek33 = "*/2";
    $nextSchDate33 = '2011-10-9 6:21:00';
    $startTime = "2011-10-9 6:15:00";
    try{
      $obj = new EpCronScheduler($month33, $dayofmonth33, $dayofweek33, $hours33, $minutes33);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate33, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 34 for every format ###
    $minutes34 = "*/7";
    $hours34 = "*/2";
    $dayofmonth34 =null;
    $month34 = "*/3";
    $dayofweek34 = "*/2";
    $nextSchDate34 = '2011-10-11 0:0:00';
    $startTime = "2011-10-10 6:15:00";
    try{
      $obj = new EpCronScheduler($month34, $dayofmonth34, $dayofweek34, $hours34, $minutes34);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate34, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

    ### Unit Testing Case 35 for every format ###
    $minutes35 = "*/7";
    $hours35 = "*/2";
    $dayofmonth35 =null;
    $month35 = "*/3";
    $dayofweek35 = "*/2";
    $nextSchDate35 = '2011-10-11 0:0:00';
    $startTime = "2012-10-10 6:15:00";
    try{
      $obj = new EpCronScheduler($month35, $dayofmonth35, $dayofweek35, $hours35, $minutes35);
      $return = $obj->getNextScheduleTime($startTime);
      $t->is($nextSchDate35, $return, 'Condition is true for every format');
    }
    catch(Exception $e){
      if($e->getMessage() == "No Valid Schedule Possible!!"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message ==".$e->getMessage());
      }

    }

  }
  public static function checkFormatCase($t) {
    ### Unit Testing Case 1 for null values ###
    $endtime = "2014-3-12 12:12:12";
    $minutes =null; //"46-55,32,52,56,58,20-23";//"10,32,52,56,58,[20-23]";0-59
    $hours = null; //"21,10,11,20,8";//0-23
    $dayofmonth =null; // "15,21,23,25,30";//1-12
    $month = null; //"4-6"; //1-31
    $dayofweek = null;

    $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
      $minutes, $hours, $dayofmonth, $month, $dayofweek);
    $t->is(true, $return, 'Condition is true for every null values');

    ### Unit Testing Case 2 ###

    $minutes1 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours1 = null; //"21,10,11,20,8";//0-23
    $dayofmonth1 =null; // "15,21,23,25,30";//1-12
    $month1 = null; //"4-6"; //1-31
    $dayofweek1 = null;

    $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
      $minutes1, $hours1, $dayofmonth1, $month1, $dayofweek1);
    $t->is(true, $return, 'Condition is true for every null values');

    ### Unit Testing Case 3 ###

    $minutes2 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours2 = "21,10,11,20,8";//0-23
    $dayofmonth2 =null; // "15,21,23,25,30";//1-12
    $month2 = null; //"4-6"; //1-31
    $dayofweek2 = null;

    $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime,  array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
      $minutes2, $hours2, $dayofmonth2, $month2, $dayofweek2);
    $t->is(true, $return, 'Condition is true for every null values');

    ### Unit Testing Case 4 ###

    $minutes3 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours3 = "21,10,11,20,8";//0-23
    $dayofmonth3 ="15,21,23,25,30";//1-12
    $month3 = null; //"4-6"; //1-31
    $dayofweek3 = null;

    $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',  $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
      $minutes3, $hours3, $dayofmonth3, $month3, $dayofweek3);
    $t->is(true, $return, 'Condition is true for every null values');

    ### Unit Testing Case 5 ###

    $minutes4 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours4 = "21,10,11,20,8";//0-23
    $dayofmonth4 ="15,21,23,25,30";//1-12
    $month4 = "4-6"; //1-31
    $dayofweek4 = null;

    $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
      $minutes4, $hours4, $dayofmonth4, $month4, $dayofweek4);
    $t->is(true, $return, 'Condition is true for every null values');

    ### Unit Testing Case 6 ###

    $minutes5 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours5 = "21,10,11,20,8";//0-23
    $dayofmonth5 ="15,21,23,25,30";//1-12
    $month5 = "1,3,4"; //1-31
    $dayofweek5 = "0,2,3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes5, $hours5, $dayofmonth5, $month5, $dayofweek5);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message");
      }

    }

    ### Unit Testing Case 7 ###

    $minutes6 ="32,52,56,58,20,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours6 = "21,10,11,20,8";//0-23
    $dayofmonth6 =null;//1-12
    $month6 = "1,3,4"; //1-31
    $dayofweek6 = "0,2,3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes6, $hours6, $dayofmonth6, $month6, $dayofweek6);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message");
      }

    }

    ### Unit Testing Case 8 ###

    $minutes7 ="0-59"; //"10,32,52,56,58,[20-23]";0-59
    $hours7 = "0-23";//0-23
    $dayofmonth7 =null;//1-12
    $month7 = "1-12"; //1-31
    $dayofweek7 = "0,2,3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes7, $hours7, $dayofmonth7, $month7, $dayofweek7);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message".$e->getMessage());
      }

    }

    ### Unit Testing Case 9 ###

    $minutes8 ="0-59,45,20"; //"10,32,52,56,58,[20-23]";0-59
    $hours8 = "0-23,12,4,3";//0-23
    $dayofmonth8 =null;//1-12
    $month8 = "1-12,10,9"; //1-31
    $dayofweek8 = "0-3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes8, $hours8, $dayofmonth8, $month8, $dayofweek8);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 10 ###

    $minutes9 ="0-59"; //"10,32,52,56,58,[20-23]";0-59
    $hours9 = "0-23,12,4,3";//0-23
    $dayofmonth9 =null;//1-12
    $month9 = "1-12,10,9"; //1-31
    $dayofweek9 = "0-3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes9, $hours9, $dayofmonth9, $month9, $dayofweek9);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 11 ###

    $minutes10 ="0-59"; //"10,32,52,56,58,[20-23]";0-59
    $hours10 = "0-23,12,4,3";//0-23
    $dayofmonth10 ="1,2,5,6,7";
    $month10 = "1-12,10,9"; //1-31
    $dayofweek10 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes10, $hours10, $dayofmonth10, $month10, $dayofweek10);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 12 ###

    $minutes11 ="0-59"; //"10,32,52,56,58,[20-23]";0-59
    $hours11 = "0-23,12,4,3";//0-23
    $dayofmonth11 ="1-25";
    $month11 = "1-12,10,9"; //1-31
    $dayofweek11 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',  $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes11, $hours11, $dayofmonth11, $month11, $dayofweek11);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 13 ###

    $minutes12 ="0-59"; //"10,32,52,56,58,[20-23]";0-59
    $hours12 = "0-23,12,4,3";//0-23
    $dayofmonth12 ="1-25,5,10";
    $month12 = "1-12,10,9"; //1-31
    $dayofweek12 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes12, $hours12, $dayofmonth12, $month12, $dayofweek12);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 14 ###

    $minutes13 ="0-59,1-6"; //"10,32,52,56,58,[20-23]";0-59
    $hours13 = "0-23,2-8";//0-23
    $dayofmonth13 ="1-25,5-10";
    $month13 = "1-12,10-9"; //1-31
    $dayofweek13 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes13, $hours13, $dayofmonth13, $month13, $dayofweek13);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass('Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)');
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 15 ###

    $minutes14 ="0-59,28,1-6"; //"10,32,52,56,58,[20-23]";0-59
    $hours14 = "0-23,13,2-8";//0-23
    $dayofmonth14 ="1-25,30,5-10";
    $month14 = "1-12,5,10-9"; //1-31
    $dayofweek14 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes14, $hours14, $dayofmonth14, $month14, $dayofweek14);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 16 ###

    $minutes15 ="0-59,28,1-6,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours15 = "0-23,13,2-8,21";//0-23
    $dayofmonth15 ="1-25,30,5-10,27";
    $month15 = "3-12,5,10-9,8"; //1-31
    $dayofweek15 = null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes15, $hours15, $dayofmonth15, $month15, $dayofweek15);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 17 ###

    $minutes16 ="0-59,28,1-6,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours16 = "0-23,13,2-8,21";//0-23
    $dayofmonth16 =null;//"1-25,30,5-10,27";
    $month16 = "3-12,5,10-9,8"; //1-31
    $dayofweek16 = "2-5,1,0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes16, $hours16, $dayofmonth16, $month16, $dayofweek16);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 18 ###

    $minutes17 ="0-59,28,1-6,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours17 = "0-23,13,2-8,21";//0-23
    $dayofmonth17 =null;//"1-25,30,5-10,27";
    $month17 = "3-12,5,10-9,8"; //1-31
    $dayofweek17 = "2-5,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes17, $hours17, $dayofmonth17, $month17, $dayofweek17);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specifid, first value should be less then second value !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 19 ###

    $minutes18 ="0-59,28,1-6,23"; //"10,32,52,56,58,[20-23]";0-59
    $hours18 = "0-23,13,2-8,21";//0-23
    $dayofmonth18 =null;//"1-25,30,5-10,27";
    $month18 = "3-12,5,9-10,8"; //1-31
    $dayofweek18 = "2-5,1,0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes18, $hours18, $dayofmonth18, $month18, $dayofweek18);
      $t->is(true, $return, 'Condition is true for every null values');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid day for this month 2"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 20 ###

    $minutes19 ="62,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours19 = "0-23,13,2-8,21";//0-23
    $dayofmonth19 =null;//"1-25,30,5-10,27";
    $month19 = "3-12,5,10-9,8"; //1-31
    $dayofweek19 = "5-2,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes19, $hours19, $dayofmonth19, $month19, $dayofweek19);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 21 ###

    $minutes20 ="-21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours20 = "0-23,13,2-8,21";//0-23
    $dayofmonth20 =null;//"1-25,30,5-10,27";
    $month20 = "3-12,5,10-9,8"; //1-31
    $dayofweek20 = "5-2,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes20, $hours20, $dayofmonth20, $month20, $dayofweek20);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 22 ###

    $minutes21 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours21 = "-23,13,2-8,21";//0-23
    $dayofmonth21 =null;//"1-25,30,5-10,27";
    $month21 = "3-12,5,10-9,8"; //1-31
    $dayofweek21 = "5-2,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes21, $hours21, $dayofmonth21, $month21, $dayofweek21);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 23 ###

    $minutes22 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours22 = "25,13,2-8,21";//0-23
    $dayofmonth22 =null;//"1-25,30,5-10,27";
    $month22 = "3-12,5,10-9,8"; //1-31
    $dayofweek22 = "5-2,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes22, $hours22, $dayofmonth22, $month22, $dayofweek22);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 24 ###

    $minutes24 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours24 = "13,2-8,21";//0-23
    $dayofmonth24 =null;//"1-25,30,5-10,27";
    $month24 = "33,3-12,5,9-10,8"; //1-31
    $dayofweek24 = "2-5,1,0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes24, $hours24, $dayofmonth24, $month24, $dayofweek24);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 25 ###

    $minutes25 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours25 = "20,13,2-8,21";//0-23
    $dayofmonth25 =null;//"1-25,30,5-10,27";
    $month25 = "-2,3-12,5,10-9,8"; //1-31
    $dayofweek25 = "5-2,1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes25, $hours25, $dayofmonth25, $month25, $dayofweek25);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 26 ###

    $minutes26 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours26 = "20,13,2-8,21";//0-23
    $dayofmonth26 =null;//"1-25,30,5-10,27";
    $month26 = "2,3-12,5,9-10,8"; //1-31
    $dayofweek26 = "8,2-5,1,0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes26, $hours26, $dayofmonth26, $month26, $dayofweek26);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 27 ###

    $minutes27 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours27 = "20,13,2-8,21";//0-23
    $dayofmonth27 =null;//"1-25,30,5-10,27";
    $month27 = "2,3-12,5,9-10,8"; //1-31
    $dayofweek27 = "2-5,/,-1,0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes27, $hours27, $dayofmonth27, $month27, $dayofweek27);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 28 ###

    $minutes28 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours28 = "20,13,2-8,21";//0-23
    $dayofmonth28 ="1-25,30,5-10,27,32";
    $month28 = "3-12,5,9-10,8"; //1-31
    $dayofweek28 = null; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes28, $hours28, $dayofmonth28, $month28, $dayofweek28);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 29 ###

    $minutes29 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours29 = "20,13,2-8,21";//0-23
    $dayofmonth29 ="1-25,/,-30,5-10,27";
    $month29 = "2,3-12,5,9-10,8"; //1-31
    $dayofweek29 = null; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes29, $hours29, $dayofmonth29, $month29, $dayofweek29);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 30 ###

    $minutes30 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours30 = "20,13,2-8,21";//0-23
    $dayofmonth30 ="0";
    $month30 = "2,3-12,5,9-10,8"; //1-31
    $dayofweek30 = null; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes30, $hours30, $dayofmonth30, $month30, $dayofweek30);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 31 ###

    $minutes31 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours31 = "20,13,2-8,21";//0-23
    $dayofmonth31 ="1-31";
    $month31 = "0"; //1-31
    $dayofweek31 = null; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes31, $hours31, $dayofmonth31, $month31, $dayofweek31);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid range for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 32 ###

    $minutes32 ="21,5,8,9"; //"10,32,52,56,58,[20-23]";0-59
    $hours32 = "20,13,2-8,21";//0-23
    $dayofmonth32 =null;
    $month32 = "1-12"; //1-31
    $dayofweek32 ="-2"; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',  $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes32, $hours32, $dayofmonth32, $month32, $dayofweek32);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 33 ###

    $minutes33 ="*"; //"10,32,52,56,58,[20-23]";0-59
    $hours33 = "20,13,2-8,21";//0-23
    $dayofmonth33 =null;
    $month33 = "1-12"; //1-31
    $dayofweek33 ="0-5"; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes33, $hours33, $dayofmonth33, $month33, $dayofweek33);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 34 ###

    $minutes34 ="*"; //"10,32,52,56,58,[20-23]";0-59
    $hours34 = "*";//0-23
    $dayofmonth34 =null;
    $month34 = "*"; //1-31
    $dayofweek34 ="*"; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes34, $hours34, $dayofmonth34, $month34, $dayofweek34);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 35 ###

    $minutes35 ="*"; //"10,32,52,56,58,[20-23]";0-59
    $hours35 = "*";//0-23
    $dayofmonth35 = "*";//null;
    $month35 = "*"; //1-31
    $dayofweek35 ="*"; //"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes35, $hours35, $dayofmonth35, $month35, $dayofweek35);
      $t->fail('no code should be executed after throwing an exception');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter only one parameter should be permitted (DayOfMonth or DayOfWeek)"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 36 ###

    $minutes36 ="*"; //"10,32,52,56,58,[20-23]";0-59
    $hours36 = "*";//0-23
    $dayofmonth36 ="*";// null;
    $month36 = "*"; //1-31
    $dayofweek36 =null;//"5-2,/,-1,4-0";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes36, $hours36, $dayofmonth36, $month36, $dayofweek36);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 37 ###

    $minutes37 ="*,*";
    $hours37 = "20";
    $dayofmonth37 =null;
    $month37 = "1-12,5";
    $dayofweek37 ="0-5";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes37, $hours37, $dayofmonth37, $month37, $dayofweek37);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specified ! only one * will be supported"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 38 ###

    $minutes38 ="1-5";
    $hours38 = "*,*";
    $dayofmonth38 =null;
    $month38 = "1-12,5";
    $dayofweek38 ="0-5";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime,  array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes38, $hours38, $dayofmonth38, $month38, $dayofweek38);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specified ! only one * will be supported"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 39 ###

    $minutes39 ="1-5";
    $hours39 = "2-20";
    $dayofmonth39 =null;
    $month39 = "*,*";
    $dayofweek39 ="0-5";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes39, $hours39, $dayofmonth39, $month39, $dayofweek39);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specified ! only one * will be supported"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 40 ###

    $minutes40 ="1-5";
    $hours40 = "2-20";
    $dayofmonth40 =null;
    $month40 = "1-5";
    $dayofweek40 ="*,*";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes40, $hours40, $dayofmonth40, $month40, $dayofweek40);
      $t->fail('no code should be executed after throwing an exception');

    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format specified ! only one * will be supported"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }


    ### Unit Testing Case 41 ###

    $minutes41 =",";
    $hours41 = "2-20";
    $dayofmonth41 =null;
    $month41 = "1-5";
    $dayofweek41 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes41, $hours41, $dayofmonth41, $month41, $dayofweek41);
      $t->fail('no code should be executed after throwing an exception');

    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter specifid, no value in this parameter !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 42 ###

    $minutes42 ="2-42";
    $hours42 = ",";
    $dayofmonth42 =null;
    $month42 = "1-5";
    $dayofweek42 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes42, $hours42, $dayofmonth42, $month42, $dayofweek42);
      $t->fail('no code should be executed after throwing an exception');

    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter specifid, no value in this parameter !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 43 ###

    $minutes43 =",,";
    $hours43 = "2-14";
    $dayofmonth43 =null;
    $month43 = "1-5";
    $dayofweek43 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes43, $hours43, $dayofmonth43, $month43, $dayofweek43);
      $t->fail('no code should be executed after throwing an exception');

    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter specifid, no value in this parameter !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 44 ###

    $minutes44 ="7,,8";
    $hours44 = "2-14";
    $dayofmonth44 =null;
    $month44 = "1-5";
    $dayofweek44 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',  $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes44, $hours44, $dayofmonth44, $month44, $dayofweek44);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter specifid, no value in this parameter !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 45 ###

    $minutes45 ="(";
    $hours45 = "2-14";
    $dayofmonth45 =null;
    $month45 = "1-5";
    $dayofweek45 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes45, $hours45, $dayofmonth45, $month45, $dayofweek45);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 46 ###

    $minutes46 ="(,[";
    $hours46 = "2-14";
    $dayofmonth46 =null;
    $month46 = "1-5";
    $dayofweek46 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes46, $hours46, $dayofmonth46, $month46, $dayofweek46);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 47 ###

    $minutes47 ="8>7";
    $hours47 = "2-14";
    $dayofmonth47 =null;
    $month47 = "1-5";
    $dayofweek47 ="0-4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes47, $hours47, $dayofmonth47, $month47, $dayofweek47);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 48 ###

    $minutes48 ="8,7";
    $hours48 = "2-14";
    $dayofmonth48 =null;
    $month48 = "1-5";
    $dayofweek48 ="0<4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1', $endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes48, $hours48, $dayofmonth48, $month48, $dayofweek48);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }


    ### Unit Testing Case 49 ###

    $minutes49 ="8,7";
    $hours49 = ",,";
    $dayofmonth49 =null;
    $month49 = "1-5";
    $dayofweek49 ="0,4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes49, $hours49, $dayofmonth49, $month49, $dayofweek49);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid parameter specifid, no value in this parameter !"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 50 ###

    $minutes50 =".";
    $hours50 = ",,";
    $dayofmonth50 =null;
    $month50 = "1-5";
    $dayofweek50 ="0,4";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',  $endtime,array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes50, $hours50, $dayofmonth50, $month50, $dayofweek50);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 51 ###
    $endtime51 = "2011-1-1 12:12:12";
    $minutes51 ="25-50";
    $hours51 = "16-23";
    $dayofmonth51 ="20-30";
    $month51 = "4";
    $dayofweek51 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime51, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes51, $hours51, $dayofmonth51, $month51, $dayofweek51);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (EpJobExpiredException $e)
    {
      if($e->getMessage() == "Next possible schedule time is beyond task end time"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 52 ###
    $endtime52 = null;
    $minutes52 ="25-50";
    $hours52 = "16-23";
    $dayofmonth52 ="20-30";
    $month52 = "4";
    $dayofweek52 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime52, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes52, $hours52, $dayofmonth52, $month52, $dayofweek52);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "End time is required for repeatable job shceduled"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    /*
     * Test Cases for Interval Time
     */
    ### Unit Testing Case 53 ###
    $endtime53 = "2014-3-12 12:12:12";
    $minutes53 ="*/5";
    $hours53 = "*/2";
    $dayofmonth53 ="*/3";
    $month53 = "*/2";
    $dayofweek53 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime53, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes53, $hours53, $dayofmonth53, $month53, $dayofweek53);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "End time is required for repeatable job shceduled"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 54 ###
    $endtime54 = "2014-3-12 12:12:12";
    $minutes54 ="**/5";
    $hours54 = "*/2";
    $dayofmonth54 = "*/3";
    $month54 = "*/2";
    $dayofweek54 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime54, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes54, $hours54, $dayofmonth54, $month54, $dayofweek54);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 55 ###
    $endtime55 = "2014-3-12 12:12:12";
    $minutes55 ="*//5";
    $hours55 = "*/2";
    $dayofmonth55 = "*/3";
    $month55 = "*/2";
    $dayofweek55 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime55, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes55, $hours55, $dayofmonth55, $month55, $dayofweek55);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 56 ###
    $endtime56 = "2014-3-12 12:12:12";
    $minutes56 ="*/45/5";
    $hours56 = "*/2";
    $dayofmonth56 = "*/3";
    $month56 = "*/2";
    $dayofweek56 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime56, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes56, $hours56, $dayofmonth56, $month56, $dayofweek56);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 57 ###
    $endtime57 = "2014-3-12 12:12:12";
    $minutes57 ="*/4555";
    $hours57 = "*/2";
    $dayofmonth57 = "*/3";
    $month57 = "*/2";
    $dayofweek57 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime57, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes57, $hours57, $dayofmonth57, $month57, $dayofweek57);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 58 ###
    $endtime58 = "2014-3-12 12:12:12";
    $minutes58 ="*/4";
    $hours58 = "*/2";
    $dayofmonth58 = null;
    $month58 = "*/2";
    $dayofweek58 ="*/3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime58, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes58, $hours58, $dayofmonth58, $month58, $dayofweek58);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }


    ### Unit Testing Case 59 ###
    $endtime59 = "2014-3-12 12:12:12";
    $minutes59 ="*/5";
    $hours59 = "./2";
    $dayofmonth59 ="*/3";
    $month59 = "*/2";
    $dayofweek59 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime59, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes59, $hours59, $dayofmonth59, $month59, $dayofweek59);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 60 ###
    $endtime60 = "2014-3-12 12:12:12";
    $minutes60 ="*/5";
    $hours60 = "**/2";
    $dayofmonth60 = "*/3";
    $month60 = "*/2";
    $dayofweek60 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime60, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes60, $hours60, $dayofmonth60, $month60, $dayofweek60);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 61 ###
    $endtime61 = "2014-3-12 12:12:12";
    $minutes61 ="*/5";
    $hours61 = "*//2";
    $dayofmonth61 = "*/3";
    $month61 = "*/2";
    $dayofweek61 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime61, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes61, $hours61, $dayofmonth61, $month61, $dayofweek61);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 62 ###
    $endtime62 = "2014-3-12 12:12:12";
    $minutes62 ="*/5";
    $hours62 = "*/45/2";
    $dayofmonth62 = "*/3";
    $month62 = "*/2";
    $dayofweek62 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime62, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes62, $hours62, $dayofmonth62, $month62, $dayofweek62);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 63 ###
    $endtime63 = "2014-3-12 12:12:12";
    $minutes63 ="*/4";
    $hours63 = "*/4542";
    $dayofmonth63 = "*/3";
    $month63 = "*/2";
    $dayofweek63 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime63, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes63, $hours63, $dayofmonth63, $month63, $dayofweek63);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Hours. Supported Characters are *,/,- and numbers within range 0 to 23"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 64 ###
    $endtime64 = "2014-3-12 12:12:12";
    $minutes64 ="*/4";
    $hours64 = "*/2";
    $dayofmonth64 = null;
    $month64 = "*/2";
    $dayofweek64 ="*/3";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime64, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes64, $hours64, $dayofmonth64, $month64, $dayofweek64);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Minutes. Supported Characters are *,/,- and numbers within range 0 to 59"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 65 ###
    $endtime65 = "2014-3-12 12:12:12";
    $minutes65 ="*/5";
    $hours65 = "*/2";
    $dayofmonth65 ="&/3";
    $month65 = "*/2";
    $dayofweek65 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime65, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes65, $hours65, $dayofmonth65, $month65, $dayofweek65);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 66 ###
    $endtime66 = "2014-3-12 12:12:12";
    $minutes66 ="*/5";
    $hours66 = "*/2";
    $dayofmonth66 = "**/3";
    $month66 = "*/2";
    $dayofweek66 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime66, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes66, $hours66, $dayofmonth66, $month66, $dayofweek66);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 67 ###
    $endtime67 = "2014-3-12 12:12:12";
    $minutes67 ="*/5";
    $hours67 = "*/2";
    $dayofmonth67 = "*//3";
    $month67 = "*/2";
    $dayofweek67 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime67, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes67, $hours67, $dayofmonth67, $month67, $dayofweek67);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 68 ###
    $endtime68 = "2014-3-12 12:12:12";
    $minutes68 ="*/5";
    $hours68 = "*/2";
    $dayofmonth68 = "*/*/3";
    $month68 = "*/2";
    $dayofweek68 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime68, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes68, $hours68, $dayofmonth68, $month68, $dayofweek68);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 69 ###
    $endtime69 = "2014-3-12 12:12:12";
    $minutes69 ="*/4";
    $hours69 = "*/4";
    $dayofmonth69 = "*/3455";
    $month69 = "*/2";
    $dayofweek69 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime69, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes69, $hours69, $dayofmonth69, $month69, $dayofweek69);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfMonth. Supported Characters are *,/,- and numbers within range 1 to 31"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 70 ###
    $endtime70 = "2014-3-12 12:12:12";
    $minutes70 ="*/5";
    $hours70 = "*/2";
    $dayofmonth70 ="*/3";
    $month70 = "^%/2";
    $dayofweek70 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime70, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes70, $hours70, $dayofmonth70, $month70, $dayofweek70);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 71 ###
    $endtime71 = "2014-3-12 12:12:12";
    $minutes71 ="*/5";
    $hours71 = "*/2";
    $dayofmonth71 = "*/3";
    $month71 = "*(*)/2";
    $dayofweek71 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime71, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes71, $hours71, $dayofmonth71, $month71, $dayofweek71);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 72 ###
    $endtime72 = "2014-3-12 12:12:12";
    $minutes72 ="*/5";
    $hours72 = "*/2";
    $dayofmonth72 = "*/3";
    $month72 = "*/*/*/2";
    $dayofweek72 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime72, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes72, $hours72, $dayofmonth72, $month72, $dayofweek72);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 73 ###
    $endtime73 = "2014-3-12 12:12:12";
    $minutes73 ="*/5";
    $hours73 = "*/2";
    $dayofmonth73 = "*/3";
    $month73 = "*//";
    $dayofweek73 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime73, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes73, $hours73, $dayofmonth73, $month73, $dayofweek73);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 74 ###
    $endtime74 = "2014-3-12 12:12:12";
    $minutes74 ="*/4";
    $hours74 = "*/4";
    $dayofmonth74 = "*/3";
    $month74 = "*/25644";
    $dayofweek74 =null;

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime74, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes74, $hours74, $dayofmonth74, $month74, $dayofweek74);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for Month. Supported Characters are *,/,- and numbers within range 1 to 12"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 75 ###
    $endtime75 = "2014-3-12 12:12:12";
    $minutes75 ="*/5";
    $hours75 = "*/2";
    $dayofmonth75 =null;
    $month75 = "*/2";
    $dayofweek75 ="^%/2";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime75, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes75, $hours75, $dayofmonth75, $month75, $dayofweek75);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 76 ###
    $endtime76 = "2014-3-12 12:12:12";
    $minutes76 ="*/5";
    $hours76 = "*/2";
    $dayofmonth76 = null;
    $month76 = "*/2";
    $dayofweek76 ="*(*)/2";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime76, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes76, $hours76, $dayofmonth76, $month76, $dayofweek76);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 77 ###
    $endtime77 = "2014-3-12 12:12:12";
    $minutes77 ="*/5";
    $hours77 = "*/2";
    $dayofmonth77 = null;
    $month77 = "*/2";
    $dayofweek77 ="*/*/*/2";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime77, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes77, $hours77, $dayofmonth77, $month77, $dayofweek77);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 78 ###
    $endtime78 = "2014-3-12 12:12:12";
    $minutes78 ="*/5";
    $hours78 = "*/2";
    $dayofmonth78 = null;
    $month78 = "*/2";
    $dayofweek78 ="*//";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime78, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes78, $hours78, $dayofmonth78, $month78, $dayofweek78);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }
    ### Unit Testing Case 79 ###
    $endtime79 = "2014-3-12 12:12:12";
    $minutes79 ="*/4";
    $hours79 = "*/4";
    $dayofmonth79 = null;
    $month79 = "*/2";
    $dayofweek79 ="*/25644";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime79, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes79, $hours79, $dayofmonth79, $month79, $dayofweek79);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

    ### Unit Testing Case 80 ###
    $endtime80 = "2014-3-12 12:12:12";
    $minutes80 ="*/4";
    $hours80 = "*/4";
    $dayofmonth80 = null;
    $month80 = "*/2";
    $dayofweek80 ="*/0-2";

    try
    {
      $return = EpjobsContext::getInstance()->addRepetableJob('TestJob', 'testRun/runName1',$endtime80, array('name' => 'test1'), $num_of_retry=-1, $app_name=null, $starttime=null,
        $minutes80, $hours80, $dayofmonth80, $month80, $dayofweek80);
      $t->is(true, $return, 'Condition is true for every format');
    }
    catch (Exception $e)
    {
      if($e->getMessage() == "Invalid format for DayOfWeek. Supported Characters are *,/,- and numbers within range 0 to 6"){
        $t->pass($e->getMessage());
      }else{
        $t->fail("Invalid exception message==".$e->getMessage());
      }

    }

  }

}