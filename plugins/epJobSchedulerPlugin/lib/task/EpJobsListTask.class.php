<?php

class EpJobsListTask extends sfDoctrineBaseTask {
  protected function configure() {
    $this->namespace        = 'epjobs';
    $this->name             = 'list-jobs';
    $this->briefDescription = 'List jobs in the jobqueue';
    $this->detailedDescription = <<<EOF
        The [epjobs:list-jobs|INFO] task manages the scheduled jobs.
Call it with:

  [php symfony epjobs:list-jobs|INFO]

You can enable selective listing by using the [state|COMMENT] option:

  [php symfony epjobs:list-jobs --state=running|INFO]

Possible values for [state|COMMENT] option are:
[ready|INFO]      List tasks which are ready for execution as of now (default)
[running|INFO]    List tasks which are currently running
[scheduled|INFO]  List tasks which are schedule for execution in future
EOF;
    $this->addOption('state', null, sfCommandOption::PARAMETER_OPTIONAL, 'State in which tasks should be', 'ready');
    $this->addOption('application', null,
      sfCommandOption::PARAMETER_OPTIONAL, 'The application name', EpJobConstants::$CFG_DEFAULT_APPLICATION);
    $this->addOption('env', null,
      sfCommandOption::PARAMETER_OPTIONAL, 'The environment', EpJobConstants::$CFG_DEFAULT_ENVIRONMENT);
  }

  protected function execute($arguments = array(), $options = array()) {
    $databaseManager = new sfDatabaseManager($this->configuration);
    $queyTable = Doctrine::getTable('EpJobQueue');
    $jobs = null;
    switch($options['state']) {
      case 'ready':
        $jobs = $queyTable->getReadyJobs(true);
        break;
      case 'running':
        $jobs = $queyTable->getRunningJobs(true);
        break;
      case 'scheduled':
        $jobs = $queyTable->getFutureJobs(true);
        break;
    }
    $this->printJobs($jobs);
    echo "Total Jobs: ".$jobs->count().PHP_EOL;
  }

  protected function printJobs(Doctrine_Collection $jobs) {
    //    foreach ($jobs as $aJob) {
    //      echo "QID: ".$aJob->getId().", Scheduled Time: "
    //          .$aJob->getScheduledStartTime().", Status: "
    //          .$aJob->getCurrentStatus().PHP_EOL;
    //    }
    foreach ($jobs->toArray() as $aJob) {
      echo "QID: ".$aJob['id'].", Scheduled Time: "
      .$aJob['scheduled_start_time'].", Type: "
      .$aJob['EpJob']['schedule_type'].", Status: "
      .$aJob['current_status'].PHP_EOL;
    }
  }
}