<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginEpJobTable extends Doctrine_Table
{
 /**
   * Finds out and return all the jobs URL for execution
   * right now.
   *
   * @return Doctrine_Collection collection of JobParameters objects
   */
   public function getJobsUrl($jobQueId) {
    $query = $this->createQuery()
    ->from('EpJob ej')
    ->leftJoin('ej.EpJobQueue ejq')
    ->where('ejq.id = :id', array('id' => $jobQueId));

     $result = $query->fetchOne();

    return $result;
  }

}