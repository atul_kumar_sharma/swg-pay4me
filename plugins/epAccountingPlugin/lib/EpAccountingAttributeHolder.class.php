<?php

class EpAccountingAttributeHolder {

  private $description='';
  private $isCleared=1;
  private $amount=0;
  private $account_id='';
  private $entry_type='credit';
  private $transaction_type='';

   public function EpAccountingAttributeHolder($description, $amount, $account_id, $isCleared, $entry_type,$transaction_type) {
    $this->amount= $amount;
    $this->account_id = $account_id;
    $this->description = $description;
    $this->isCleared = $isCleared;
    $this->entry_type= $entry_type;
    $this->transaction_type= $transaction_type;
    
  }

  public function getAmount() {
    return $this->amount;
  }


  public function getAccountId(){
    return $this->account_id;
  }

  public function getDescription(){
    return $this->description;
  }

  public function getIsCleared(){
    return $this->isCleared;
  }

  public function getEntryType(){
    return $this->entry_type;
  }

  public function getTransactionType(){
    return $this->transaction_type;
  }

  public function getAccountantId(){
    return $this->accountant_id;
  }

//  public function EpAccountingAttributeHolder($arr=array()) {
//    foreach($arr as $k=>$v){
//      $this->$k=$v;
//    }
//  }



 

}