INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`env` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'ErrorCodeInfo', 'Error Code Info', '27', 'admin_support_user', 'supportTool/errorCodeInfo', NULL , 'custom', '12', '', '', '0', NULL , NULL
);

ALTER TABLE `ipay4me_order` ADD `card_hash` VARCHAR( 40 ) NULL DEFAULT NULL AFTER `card_type` ; 