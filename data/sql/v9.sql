ALTER TABLE `merchant` ADD `address` VARCHAR( 255 ) NULL AFTER `homepage_url` ,
ADD `email` VARCHAR( 255 ) NULL AFTER `address` ,
ADD `contact_phone` VARCHAR( 255 ) NULL AFTER `email` ;