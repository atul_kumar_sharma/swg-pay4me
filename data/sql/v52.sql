CREATE TABLE IF NOT EXISTS `support_reason` (
  `id` bigint(20) NOT NULL auto_increment,
  `key` varchar(255) default NULL,
  `reason` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `support_reason`
--

INSERT INTO `support_reason` (`id`, `key`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Not able to attend interview', 'The applicant is not able to attend interview on date mentioned on report so either want to change interview date or want to cancel his report and asking for refund.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(2, 'No longer need appointment', 'The applicant now no longer need  appointment for Nigerian Consulate.So want to refund his money.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'Wrong information in application form', 'The applicant made a mistake in filling up the application form and now can not edit as he has made payment so now want to cancel the report to fill another application and want refund.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'Not get visa on time', 'The applicant didnt get Visa on time for his trip so now want to cancel the report and asking for refund.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 'want to refund his money', ' The applicant is directly asking for refund.(without any specific reason)', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 'Application was rejected due to long waiting list', 'The applicant''s application was rejected due to long waiting list and work load on the officers that visited the place. So want refund of his applications.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 'Could not interviewed', 'The applicant could not interviewed as the Officials ran out of time so applicant want his refund.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(8, 'payment sent to wrong place', 'Payment was sent to Benin Edo state instead of UK So want to transfer payment to UK.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 'wants his name on receipt', 'User wants his applicants name on the receipt sent to him.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 'Interview date reschedule', 'User wants his interview date to be re-scheduled', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 'wants to change processing embassy', 'User wants to change in processing embassy', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 'wants to changeprocessing country', 'User wants to change the processing country', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(13, 'mistakenly select wrong pasport type', 'Applicant has mistakenly selected wrong passport type.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(14, 'wants to know application status', 'User want to know status of his application id', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(15, 'Duplicate payment', 'If user does duplicate payment for same application and wants to refund his money.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);


INSERT INTO `fps_rule` (`description`, `deleted`) VALUES
('Admin Black List Card', 0);