INSERT INTO `ep_menu` ( `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Print Application', 'Print Application', 25, 'payment_user', 'nis/generatePrint', NULL, 'custom', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
('User Transaction Status', 'User Transaction Status', 27, 'refund_support_user', 'supportTool/userTransactionLimitStatus', NULL, 'custom', 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES ('Login User Details', 'Login User Details', '16', 'refund_support_user', 'userManagement/view', NULL, 'custom', '16', '', '', '0', NULL, NULL);
