--Files Need to be changed:
--apps/frontend/modules/paymentProcess/actions/actions.class.php
--apps/frontend/modules/configurationManager/actions/actions.class.php
--plugins/epNISPlugin/modules/passport/actions/actions.class.php
--plugins/epNISPlugin/modules/passport/config/security.yml
--apps/frontend/config/app.yml
--plugins/epNISPlugin/modules/passport/config/security.yml
--apps/frontend/modules/configurationManager/templates/checkVapPaymentStatusSuccess.php


INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'pos_manager', 'POS Manager', '2012-08-28 13:53:56', '2012-08-28 13:53:58');

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'pos_manager', 'POS Manager', '2012-08-28 13:54:48', '2012-08-28 13:54:50');

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'VOAP Configuration', 'VOAP Configuration', '0', NULL, NULL, NULL, 'custom', '13', '2012-08-28 14:10:58', '2012-08-28 14:11:00', '0', NULL, NULL);

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'Update VOAP', 'Update VOAP', '3', 'pos_manager', 'configurationManager/checkVapPaymentStatus', NULL, 'custom', '', '2012-08-28 14:09:52', '2012-08-28 14:09:54', '0', NULL, NULL);
