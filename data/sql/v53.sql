UPDATE `payment_mode` SET `display_name` = 'VISA (Inclusive of Verified by VISA)' WHERE `payment_mode`.`id` =5 LIMIT 1 ;

UPDATE `payment_mode` SET `display_name` = 'MasterCard (Inclusive of MasterCard 3D Secure)' WHERE `payment_mode`.`id` =6 LIMIT 1 ;

 CREATE TABLE `rollback_payment_details` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ,
`rollback_payment_id` BIGINT( 20 ) NOT NULL ,
`app_id` BIGINT( 20 ) NOT NULL ,
`app_type` VARCHAR( 20 ) NOT NULL ,
`amount` DOUBLE NOT NULL ,
`created_at` DATETIME NOT NULL ,
`created_by` BIGINT( 20 ) NOT NULL ,
`updated_at` DATETIME NOT NULL ,
`updated_by` BIGINT( 20 ) NOT NULL ,
PRIMARY KEY ( `id` ) ,
INDEX ( `rollback_payment_id` )
) ENGINE = InnoDB;

ALTER TABLE `rollback_payment_details` ADD FOREIGN KEY ( `rollback_payment_id` ) REFERENCES `rollback_payment` (
`id`
);

ALTER TABLE `rollback_payment` DROP `action`  ;

ALTER TABLE `rollback_payment_details` ADD `action` ENUM( 'refund', 'chargeback', 'refund and block' ) NOT NULL DEFAULT 'refund' AFTER `amount` ;

ALTER TABLE `rollback_payment_details` ADD `status` VARCHAR( 50 ) NOT NULL DEFAULT 'resuest sent' AFTER `action` ;