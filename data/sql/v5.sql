UPDATE `pay4meInt`.`merchant` SET `name` = 'NIS Passport' WHERE `merchant`.`id` =1 LIMIT 1 ;

UPDATE `sf_guard_group` SET `name` = 'payment_group',`description` = 'Payment Group' WHERE `sf_guard_group`.`id` =3;

--INSERT INTO `ep_menu` (
--`id` ,
--`name` ,
--`label` ,
--`parent_id` ,
--`sfguardperm` ,
--`url` ,
--`eval` ,
--`sequence` ,
--`created_at` ,
--`updated_at` ,
--`deleted` ,
--`created_by` ,
--`updated_by`
--)
--VALUES (
--NULL , 'Change Password', 'Change Password', '1', 'portal_admin', 'signup/changePassword', NULL , '1', now(), now(), '0', NULL , NULL
--);


-- Table structure for table `currency_value`
--

CREATE TABLE IF NOT EXISTS `currency_value` (
  `id` bigint(20) NOT NULL auto_increment,
  `value` float(18,2) NOT NULL,
  `currency_date` date NOT NULL,
  `conversion_type` enum('naira_to_dollor','dollor_to_naira') NOT NULL default 'dollor_to_naira',
  `description` varchar(128) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

