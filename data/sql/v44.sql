CREATE TABLE rollback_payment (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, action VARCHAR(255) DEFAULT 'refund' NOT NULL, comments VARCHAR(100) NOT NULL, UNIQUE INDEX unique_parameter_idx (order_number), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE `ep_gray_pay_request` ADD `transactionid` VARCHAR( 30 ) NOT NULL AFTER `ip_address` ,ADD `type` VARCHAR( 30 ) NOT NULL AFTER `transactionid` ;
ALTER TABLE `ep_gray_pay_request` CHANGE `order_number` `order_number` BIGINT( 20 ) NULL
ALTER TABLE `user_detail` ADD `payment_rights` INT( 2 ) NOT NULL DEFAULT '0' AFTER `approved_by` ;
ALTER TABLE `ep_gray_pay_request` CHANGE `type` `type` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'sale'


INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(6, 'support_user', 'Handle SWLL support request', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(6, 'refund_support_user', 'refund payment', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00')



INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(27, 'Support', 'Support', 0, '', '', NULL, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(28, 'Find Applications by Order Number', 'Find Applications by Order Number', 27, 'refund_support_user', 'supportTool/searchOrderNo', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(29, 'Search Application By Application Id', 'Search Application By Application Id', 27, 'refund_support_user', 'supportTool/searchApplication', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(30, 'Search Application By SWLLC Email Id', 'Search Application By SWLLC Email Id', 27, 'refund_support_user', 'supportTool/searchBySwLlcAccount', NULL, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(31, 'Refund SWLLC Order Number', 'Refund SWLLC Order Number', 27, 'refund_support_user', 'supportTool/orderAction?pAction=Refund', NULL, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(32, 'Refund SWLLC Order Number And Block', 'Refund SWLLC Order Number And Block', 27, 'refund_support_user', 'supportTool/orderAction?pAction=Refund and Block', NULL, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(33, 'Chargeback SWLLC Order Number', 'Chargeback SWLLC Order Number', 27, 'refund_support_user', 'supportTool/orderAction?pAction=Chargeback', NULL, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
