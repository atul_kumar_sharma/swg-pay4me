ALTER TABLE `order_request_details` ADD `card_first` INT( 11 ) NOT NULL AFTER `amount` ,
ADD `card_last` INT( 11 ) NOT NULL AFTER `card_first` ,
ADD `card_len` SMALLINT( 6 ) NULL DEFAULT NULL AFTER `card_last` ,
ADD `card_type` CHAR( 1 ) NULL DEFAULT NULL AFTER `card_len` ;
