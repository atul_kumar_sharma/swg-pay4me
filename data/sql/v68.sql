-----Query for NIS to block Applicant on Charge Back and refund block-----

CREATE TABLE tbl_block_applicant (id BIGINT AUTO_INCREMENT, first_name VARCHAR(150) NOT NULL, middle_name VARCHAR(150), last_name VARCHAR(150) NOT NULL, dob DATETIME NOT NULL, app_type VARCHAR(50) NOT NULL, blocked VARCHAR(255) DEFAULT 'yes', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;
 ALTER TABLE `tbl_block_applicant` CHANGE `blocked` `blocked` ENUM( 'yes', 'no' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'yes' 