TRUNCATE TABLE `country_payment_mode`;
INSERT INTO `country_payment_mode` (`id`, `country_code`, `card_type`, `cart_capacity`, `cart_amount_capacity`, `number_of_transaction`, `transaction_period`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'Vc', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(2, 'JP', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(3, 'US', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(4, 'CA', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(5, 'FR', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(6, 'SE', 'nmi_mcs,nmi_vbv,nmi_amx', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(7, 'AR', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(8, 'TT', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(9, 'IN', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(10, 'DE', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(11, 'AU', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(12, 'SG', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL),
(13, 'BR', 'A,M,V', 5, 800, 1, 43200, '2011-07-07 19:34:09', '2011-07-07 19:34:12', NULL, NULL);

TRUNCATE TABLE `country_based_mid_service`;
INSERT INTO `country_based_mid_service` (`id`, `country_code`, `service`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(1, 'ALL', 'MID2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(2, 'JP', 'MID1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(3, 'US', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(4, 'CA', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(5, 'FR', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL),
(6, 'SE', 'MID1', '2011-07-18 09:40:05', '2011-07-18 09:40:05', 0, NULL, NULL);


update country_payment_mode set card_type = 'nmi_mcs,nmi_vbv,nmi_amx,Mo' where country_code in ('US','CA') limit 2;
update country_payment_mode set card_type = 'nmi_mcs,nmi_vbv,nmi_amx' where country_code in ('FR','SE') limit 2;