ALTER TABLE `response` CHANGE `response_transaction_code` `response_transaction_code` BIGINT( 20 ) NULL DEFAULT NULL  ;
ALTER TABLE `response` ADD `response_authorization_code` VARCHAR( 10 ) NOT NULL AFTER `response_transaction_date` ;

CREATE TABLE IF NOT EXISTS `order_request_split` (
  `id` bigint(20) NOT NULL auto_increment,
  `order_request_id` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL COMMENT 'in cents',
  `merchant_code` int(11) NOT NULL,
  `merchant_id` bigint(20) NOT NULL,
  `pay_merchant_fee` enum('0','1') default '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `order_request_id_idx` (`order_request_id`),
  KEY `merchant_id_idx` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `order_request_split` ADD FOREIGN KEY ( `order_request_id` ) REFERENCES `order_request` (
`id`
);

ALTER TABLE `order_request_split` ADD FOREIGN KEY ( `merchant_id` ) REFERENCES `merchant` (
`id`
);


TRUNCATE TABLE `order_request`  ;
DROP TABLE order_request_split;
DROP TABLE response;
DROP TABLE request;
DROP TABLE order_request;


CREATE TABLE order_request (id BIGINT AUTO_INCREMENT, version VARCHAR(10) NOT NULL, merchant_id BIGINT NOT NULL, transaction_number BIGINT UNIQUE NOT NULL, amount FLOAT(18, 2) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (merchant_id, transaction_number), INDEX merchant_id_idx (merchant_id), PRIMARY KEY(id)) ENGINE = INNODB;


ALTER TABLE order_request ADD CONSTRAINT order_request_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

ALTER TABLE `order_request_details` ADD `paid_date` DATETIME NULL AFTER `payment_status` ;





CREATE TABLE order_request_details (id BIGINT AUTO_INCREMENT, order_request_id BIGINT NOT NULL, user_id INT, merchant_id BIGINT NOT NULL, retry_id BIGINT NOT NULL, name VARCHAR(100) NOT NULL, description TEXT NOT NULL, amount FLOAT(18, 2) NOT NULL, payment_status VARCHAR(255) DEFAULT '1', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX order_request_id_idx (order_request_id), INDEX merchant_id_idx (merchant_id), INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE order_request_details ADD CONSTRAINT order_request_details_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE order_request_details ADD CONSTRAINT order_request_details_order_request_id_order_request_id FOREIGN KEY (order_request_id) REFERENCES order_request(id) ON DELETE CASCADE;
ALTER TABLE order_request_details ADD CONSTRAINT order_request_details_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;



CREATE TABLE order_request_split (id BIGINT AUTO_INCREMENT, order_request_id BIGINT NOT NULL, order_detail_id BIGINT NOT NULL, amount BIGINT NOT NULL COMMENT 'Array', merchant_code INT NOT NULL, merchant_id BIGINT NOT NULL, pay_merchant_fee VARCHAR(255) DEFAULT '0', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX order_request_id_idx (order_request_id), INDEX order_detail_id_idx (order_detail_id), INDEX merchant_id_idx (merchant_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE order_request_split ADD CONSTRAINT order_request_split_order_request_id_order_request_id FOREIGN KEY (order_request_id) REFERENCES order_request(id) ON DELETE CASCADE;
ALTER TABLE order_request_split ADD CONSTRAINT order_request_split_order_detail_id_order_request_details_id FOREIGN KEY (order_detail_id) REFERENCES order_request_details(id) ON DELETE CASCADE;
ALTER TABLE order_request_split ADD CONSTRAINT order_request_split_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

CREATE TABLE request (id BIGINT AUTO_INCREMENT, order_detail_id BIGINT NOT NULL, request_order_number BIGINT NOT NULL, amount FLOAT(18, 2), card_details_first SMALLINT, card_details_last SMALLINT, cvv SMALLINT, exp_month SMALLINT, exp_year SMALLINT, card_type VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (request_order_number), INDEX order_detail_id_idx (order_detail_id), PRIMARY KEY(id)) ENGINE = INNODB;


CREATE TABLE response (id BIGINT AUTO_INCREMENT, request_id BIGINT NOT NULL, response_code TINYINT NOT NULL, response_reason VARCHAR(10), response_reason_txt VARCHAR(255), response_reason_sub_code TINYINT, response_transaction_code BIGINT, response_authorization_code VARCHAR(10), response_transaction_date DATETIME, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX request_id_idx (request_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE request ADD CONSTRAINT request_order_detail_id_order_request_details_id FOREIGN KEY (order_detail_id) REFERENCES order_request_details(id) ON DELETE CASCADE;
ALTER TABLE response ADD CONSTRAINT response_request_id_request_id FOREIGN KEY (request_id) REFERENCES request(id) ON DELETE CASCADE;

ALTER TABLE `order_request` DROP INDEX `transaction_number` ;

CREATE TABLE payment_response (id BIGINT AUTO_INCREMENT, request_order_number BIGINT NOT NULL, response_code TINYINT NOT NULL, response_reason VARCHAR(10), response_reason_txt VARCHAR(255), response_reason_sub_code TINYINT, response_transaction_code BIGINT, response_authorization_code VARCHAR(10), response_transaction_date DATETIME, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX request_order_number_idx (request_order_number), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE payment_response ADD CONSTRAINT prrr FOREIGN KEY (request_order_number) REFERENCES request(request_order_number) ON DELETE CASCADE;

ALTER TABLE `request`
  DROP `cvv`,
  DROP `exp_month`,
  DROP `exp_year`;

CREATE TABLE IF NOT EXISTS `ep_action_audit_event` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` int(11) default NULL,
  `username` varchar(128) default NULL,
  `ip_address` varchar(15) NOT NULL,
  `category` varchar(40) NOT NULL,
  `subcategory` varchar(40) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `ep_action_audit_event`
  ADD CONSTRAINT `ep_action_audit_event_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `sf_guard_user` (`id`) ON DELETE SET NULL;


CREATE TABLE IF NOT EXISTS `ep_action_audit_event_attributes` (
  `id` bigint(20) NOT NULL auto_increment,
  `audit_event_id` bigint(20) default NULL,
  `name` varchar(40) default NULL,
  `svalue` varchar(40) default NULL,
  `ivalue` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `audit_event_id_idx` (`audit_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `ep_action_audit_event_attributes`
  ADD CONSTRAINT `ep_action_audit_event_attributes_ibfk_1` FOREIGN KEY (`audit_event_id`) REFERENCES `ep_action_audit_event` (`id`) ON DELETE SET NULL;

 ALTER TABLE `request` DROP `card_type`  ;




---auditing menu sql---
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(5, 'Audit Trail', 'Audit Trail Report', 0, 'portal_admin', NULL, NULL, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(6, 'View Report', 'View Report', 5, 'portal_admin', 'epActionAuditEventReports/index', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
DELETE FROM `ep_menu` WHERE `ep_menu`.`id` = 3 LIMIT 1;
DELETE FROM `ep_menu` WHERE `ep_menu`.`id` = 4 LIMIT 1;
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(3, 'Reports', 'Reports', 0, '', '', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(4, 'Payment History', 'Payment History', 3, 'payment_user', 'report/paymentHistory', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

