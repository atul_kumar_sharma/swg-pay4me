INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:22:02', '2010-05-28 18:22:02'),
(2, 'User', 'user', '2010-05-28 18:22:48', '2010-05-28 18:22:48');


INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'portal_admin', 'Portal Admin', '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 'User', 'user', '2010-05-28 18:26:04', '2010-05-28 18:26:04');




INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:25:12', '2010-05-28 18:25:12'),
(2, 2, '2010-05-28 18:26:04', '2010-05-28 18:26:04');

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'sha1', 'd693cbac2f2984cc9f395f0f436ac5a9', '230b9e013cbd1ab8fa2cb102878c52438b4bf70e', 1, 1, '2010-05-28 18:49:18', '2010-05-28 18:28:47', '2010-05-28 18:49:18'),
(2, 'user', 'sha1', '638fa059a6124d0be0e7be2761b81c19', 'b77a3dfb67d4f5575c656be0a2a79c6d272b3428', 1, 0, '2010-05-28 19:24:13', '2010-05-28 19:23:45', '2010-05-28 19:24:13');

INSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45');

INSERT INTO `sf_guard_user_permission` (`user_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2010-05-28 18:28:47', '2010-05-28 18:28:47'),
(2, 2, '2010-05-28 19:23:45', '2010-05-28 19:23:45');


CREATE TABLE user_detail (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, name VARCHAR(255), last_name VARCHAR(255), dob DATE, address VARCHAR(255), email VARCHAR(255), mobile_no VARCHAR(20), work_phone VARCHAR(20), send_sms ENUM('gsmno', 'workphone', 'both', 'none') DEFAULT 'gsmno', failed_attempt SMALLINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE user_detail ADD CONSTRAINT user_detail_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;

INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'payment_group', 'Payment Group', now(), now());

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(3, 'payment_user', 'payment_user', now(), now());

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(3, 3, now(), now());
