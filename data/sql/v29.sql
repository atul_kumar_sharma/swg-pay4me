UPDATE `ep_menu` SET `deleted` = '1' WHERE `ep_menu`.`id` =24 LIMIT 1 ;
UPDATE `gateway` SET `display_name` = 'American Express' WHERE `gateway`.`id` =1 LIMIT 1 ;

UPDATE `gateway` SET `display_name` = 'Visa' WHERE `gateway`.`id` =4 LIMIT 1 ;

CREATE TABLE vault (id BIGINT AUTO_INCREMENT, vault_num BIGINT NOT NULL, order_number BIGINT NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE `ipay4me_recharge_order` ADD `validation_number` BIGINT NOT NULL AFTER `order_number` ;