
CREATE TABLE country_based_graypay_service (id BIGINT AUTO_INCREMENT, country_code VARCHAR(20), service VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

INSERT INTO `country_based_graypay_service` (`id` ,`country_code` ,`service` ,`created_at` ,`updated_at` ,`deleted` ,`created_by` ,`updated_by`)
VALUES (
NULL , 'ALL', 'G', '', '', '0', NULL , NULL),
(NULL , 'US', 'M', '', '', '0', NULL , NULL);

INSERT INTO `gateway` (`id` ,`name` ,`display_name` ,`merchant_id`)
VALUES (
'8', 'Mg', 'Maringateway', '7');

INSERT INTO `transaction_charges` (`id` ,`gateway_id` ,`transaction_charges` ,`split_type` ,`created_at` ,`updated_at` ,`deleted` ,`created_by` ,`updated_by` ,`version`)
VALUES
(NULL , '8', '4.00', 'percentage', '2010-10-19 15:11:51', '2010-10-19 15:11:51', '0', NULL , NULL , '0'),
(NULL , '9', '4.00', 'percentage', '2010-10-19 15:11:51', '2010-10-19 15:11:51', '0', NULL , NULL , '0');

ALTER TABLE `ep_gray_pay_request` ADD `gateway_id` BIGINT NOT NULL DEFAULT '1' AFTER `id` ;

ALTER TABLE `vault` ADD `gateway_id` BIGINT NOT NULL DEFAULT '1' AFTER `id` ;
