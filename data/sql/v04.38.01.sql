INSERT INTO `sf_guard_group` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'pos_report_manager', 'POS Report Manager', '2012-09-05 14:32:53', '2012-09-05 14:32:55');

INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES (NULL, 'pos_report_manager', 'POS Report Manager', '2012-09-05 14:33:49', '2012-09-05 14:33:51');

INSERT INTO `sf_guard_group_permission` (`group_id`, `permission_id`, `created_at`, `updated_at`) VALUES ('9', '10', '2012-09-05 15:08:59', '2012-09-05 15:09:00');

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'POS Reports', 'POS Reports', '0', NULL, NULL, NULL, 'custom', '13', '2012-09-05 14:10:58', '2012-09-05 14:11:00', '0', NULL, NULL);

INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES (NULL, 'POS Revenue Report', 'POS Revenue Report', '104', 'pos_report_manager', 'report/posPaymentReport', NULL, 'custom', '', '2012-09-05 14:09:52', '2012-09-05 14:09:54', '0', NULL, NULL);
