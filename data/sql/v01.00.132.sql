INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('UserTransactionLimit', 'User Transaction Limit', 27, 'admin_support_user', 'supportTool/userTransactionLimit', NULL, 'custom', 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

CREATE TABLE tbl_user_transaction_limit (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, cart_capacity BIGINT, cart_amount_capacity DOUBLE(18, 2), number_of_transaction BIGINT, transaction_period BIGINT, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE tbl_user_transaction_limit ADD CONSTRAINT tbl_user_transaction_limit_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;

ALTER TABLE `country_payment_mode` ADD `number_of_transaction` BIGINT( 20 ) NOT NULL AFTER `cart_amount_capacity` ;
ALTER TABLE `country_payment_mode` ADD `transaction_period` BIGINT( 20 ) NOT NULL AFTER `number_of_transaction` ;




INSERT INTO `payment_mode` (
`id` ,
`card_type` ,
`gateway_id` ,
`group_id` ,
`display_name` ,
`created_at` ,
`updated_at` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'nmi_amx', '5', '5.3', 'Amex', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL , NULL
);


UPDATE `country_payment_mode` SET `card_type` = 'Vc' WHERE `country_payment_mode`.`country_code` ='ALL' LIMIT 1 ;

UPDATE `country_payment_mode` SET `card_type` = 'nmi_mcs,nmi_vbv,nmi_amx,Mo' WHERE `country_payment_mode`.`country_code` in('US','CA') LIMIT 1 ;