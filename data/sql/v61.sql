CREATE TABLE tbl_nis_dollar_revenue (id BIGINT AUTO_INCREMENT, date DATE, amount DOUBLE(18, 2) NOT NULL, account_id BIGINT NOT NULL, remark VARCHAR(255), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX dateindex_idx (date, account_id), INDEX account_id_idx (account_id), PRIMARY KEY(id)) ENGINE = InnoDB;

TRUNCATE TABLE `tbl_nis_dollar_revenue` 