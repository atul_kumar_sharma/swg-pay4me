 DROP TABLE `ipay4me_order`  ;

CREATE TABLE ipay4me_order (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, gateway_id SMALLINT NOT NULL, order_request_detail_id BIGINT NOT NULL, payment_status VARCHAR(255) DEFAULT '1', response_code VARCHAR(30), response_text TEXT, payor_name VARCHAR(255), card_holder VARCHAR(255), card_first INT, card_last INT, card_len SMALLINT, card_type VARCHAR(10), address VARCHAR(255), phone VARCHAR(20), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (order_number), INDEX order_request_detail_id_idx (order_request_detail_id), INDEX gateway_id_idx (gateway_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE `order_request_details`
  DROP `card_first`,
  DROP `card_last`,
  DROP `card_len`,
  DROP `card_type`;




UPDATE `gateway` SET `name` = 'payEasy' WHERE `gateway`.`id` =2 LIMIT 1 ;

ALTER TABLE `transaction_charges` ADD `gateway_id` BIGINT( 20 ) NOT NULL AFTER `merchant_id` ;
ALTER TABLE `transaction_charges` DROP INDEX `merchant_id` ;
ALTER TABLE `transaction_charges` CHANGE `gateway_id` `gateway_id` SMALLINT( 6 ) NOT NULL ;
ALTER TABLE transaction_charges ADD CONSTRAINT transaction_charges_gateway_id_gateway_id FOREIGN KEY (gateway_id) REFERENCES gateway(id) ON DELETE CASCADE;
UPDATE `transaction_charges` SET `gateway_id` = '1' WHERE `transaction_charges`.`id` =1 LIMIT 1 ;

ALTER TABLE transaction_charges UNIQUE INDEX unique_parameter_idx (merchant_id, gateway_id);
ALTER TABLE `transaction_charges` ADD UNIQUE (`merchant_id`, gateway_id);
ALTER TABLE `transaction_charges` ADD INDEX ( `gateway_id` );


INSERT INTO `transaction_charges` (`id`, `merchant_id`, `gateway_id`, `transaction_charges`, `split_type`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(2, 1, 2, 6.00, 'percentage', now(),now(), 0, NULL, NULL);



INSERT INTO `sf_guard_group` (
`id` ,
`name` ,
`description` ,
`created_at` ,
`updated_at`
)
VALUES (4 , 'Merchant', 'Merchant Group', now(), now());


INSERT INTO `sf_guard_permission` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES (4, 'merchant', 'merchant', now(), now());

INSERT INTO `sf_guard_user` (`id`, `username`, `algorithm`, `salt`, `password`, `is_active`, `is_super_admin`, `last_login`, `created_at`, `updated_at`) VALUES
(3, 'nis', 'sha1', 'ad2f9d30d7c1e14719a52e6f33b8b711', '734682f071903f9b6d753b5e26582bf381c8ae53', 1, 0, now(), now(), now());

INSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`)
VALUES (3, 4, now(), now());

INSERT INTO `user_detail` (

`user_id` ,
`first_name` ,
`last_name` ,
`dob` ,
`address` ,
`email` ,
`mobile_phone` ,
`work_phone` ,
`failed_attempt` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
 '3', 'NIS', NULL , '2010-09-21', 'address1', 'a@a.com', NULL , NULL , '0', '', '', '0', NULL , NULL
);

INSERT INTO `sf_guard_group_permission` (
`group_id` ,
`permission_id` ,
`created_at` ,
`updated_at`
)
VALUES (
'4', '4', '', ''
);

CREATE TABLE merchant_user (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, merchant_id BIGINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), INDEX merchant_id_idx (merchant_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE merchant_user ADD CONSTRAINT merchant_user_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE merchant_user ADD CONSTRAINT merchant_user_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

INSERT INTO `merchant_user` (`user_id` ,`merchant_id` ,`created_at` ,`updated_at` ,`deleted` ,`created_by` ,`updated_by`)
VALUES ( '3', '1', '', '', '0', NULL , NULL);


CREATE TABLE transaction_charges_version (id INT, merchant_id BIGINT NOT NULL, gateway_id BIGINT NOT NULL, transaction_charges FLOAT(18, 2) DEFAULT 0 NOT NULL, split_type ENUM('flat', 'percentage') DEFAULT 'percentage', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, version BIGINT, PRIMARY KEY(id, version)) ENGINE = INNODB;
ALTER TABLE transaction_charges_version ADD CONSTRAINT transaction_charges_version_id_transaction_charges_id FOREIGN KEY (id) REFERENCES transaction_charges(id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE `transaction_charges` ADD `version` BIGINT NOT NULL ;




NSERT INTO `sf_guard_user_group` (`user_id`, `group_id`, `created_at`, `updated_at`)
VALUES (3, 4, now(), now());

INSERT INTO `user_detail` (

`user_id` ,
`first_name` ,
`last_name` ,
`dob` ,
`address` ,
`email` ,
`mobile_phone` ,
`work_phone` ,
`failed_attempt` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
'3', 'NIS', NULL , '2010-09-21', 'address1', 'a@a.com', NULL , NULL , '0', now(), now(), '0', NULL , NULL
);

INSERT INTO `sf_guard_group_permission` (
`group_id` ,
`permission_id` ,
`created_at` ,
`updated_at`
)
VALUES (
'4', '4', now(), now());

CREATE TABLE merchant_user (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, merchant_id BIGINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT, INDEX user_id_idx (user_id), INDEX merchant_id_idx (merchant_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE merchant_user ADD CONSTRAINT merchant_user_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;
ALTER TABLE merchant_user ADD CONSTRAINT merchant_user_merchant_id_merchant_id FOREIGN KEY (merchant_id) REFERENCES merchant(id) ON DELETE CASCADE;

INSERT INTO `merchant_user` (`user_id` ,`merchant_id` ,`created_at` ,`updated_at` ,`deleted` ,`created_by` ,`updated_by`)
VALUES ( '3', '1', '', '', '0', now() , now());


#Data migration code

INSERT INTO `ipay4me_order` (`order_number`, `gateway_id` ,`order_request_detail_id`) select cus_id, '2', order_request_detail_id from pay_easy_order;

update ipay4me_order i, ep_pay_easy_request e set i.payor_name=concat(e.`first_name`," ",e.`last_name`) ,i.card_holder=e.`card_holder`,i.card_first=e.`card_first`,i.card_last=e.`card_last`,i.card_len=e.`card_len`,i.card_type=e.`card_type`,i.address=CONCAT(e.`address1`,'~',e.`address2`,'~',e.`town`,'~',e.`state`,'~',e.`zip`,'~' ,e.`country`),i.phone=e.`phone` where i.order_number=e.cus_id;

update ipay4me_order i, ep_pay_easy_request e, ep_pay_easy_response r set i.response_code=r.response_code, i.response_text=r.response_text where i.order_number=e.cus_id and e.id=r.payeasy_req_id

update ipay4me_order set payment_status='0' where response_code='08';


--------------Menu SQl 0-----------------------------------
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(7, 'SWGlobal Report', 'SWGlobal Report', 3, 'portal_admin', 'report/swGlobalPaymentReport', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(9, 'Payment Report', 'Payment Report', 3, 'merchant', 'report/merchantPaymentReport', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(10, 'Settings', 'Settings', 0, NULL, NULL, NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(11, 'Transaction Charges', 'Transaction Charges', 10, 'portal_admin', 'settings/index', NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
------------------------------------


