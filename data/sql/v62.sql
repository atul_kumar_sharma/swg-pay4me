 DROP TABLE `support_reason`

CREATE TABLE IF NOT EXISTS `support_reason` (
  `id` bigint(20) NOT NULL auto_increment,
  `key` varchar(255) default NULL,
  `parent_id` int(10) NOT NULL default '0',
  `reason` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) default NULL,
  `updated_by` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `support_reason`
--

INSERT INTO `support_reason` (`id`, `key`, `parent_id`, `reason`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Request for Refund', 0, 'Request a refund if you have made a duplicate payment for an application. Please provide the duplicate order number and identify the duplicate application for faster processing of your request. In case of any other reason for refund, please provide the details in the message box.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(2, 'Request for Re-scheduling Interview date', 0, 'Please be advised that the interview date is controlled by the Nigeria Immigration Services. However we may be able to assist you if you want the interview date to be postponed further in future. Please provide desired interview date in the message box.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'Request for change Application Details', 0, 'In case you have made a mistake in providing any of the detail in application, you can request changes in the details by providing appropriate information below.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'Request for Application Details', 0, 'You can request the application status in case you have paid but the application is still being displayed as new.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 'Duplicate Payment', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 'Others', 1, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 'Name', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 'Date Of Birth', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 'Processing Office', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 'Others', 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);
