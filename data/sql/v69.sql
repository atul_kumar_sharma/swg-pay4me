
INSERT INTO `ep_menu` (`id`, `name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(42, 'RegisterCreditCard', 'RegisterCreditCard', 0, NULL, NULL, NULL, 'custom', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(43, 'RegisterCreditCard', 'Register Credit Card', 42, 'payment_user', 'userAuth/newContectDetails', NULL, 'custom', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL),
(44, 'CreditCardPendingList', 'Credit Card Pending List', 27, 'admin_support_user', 'supportTool/pendingUserAuthList', NULL, 'custom', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);


CREATE TABLE IF NOT EXISTS `applicant_vault` (
  `id` bigint(20) NOT NULL auto_increment,
  `first_name` varchar(30) default NULL,
  `last_name` varchar(30) default NULL,
  `card_first` int(11) NOT NULL,
  `card_last` int(11) NOT NULL,
  `card_hash` varchar(128) NOT NULL,
  `card_len` smallint(6) default NULL,
  `card_type` char(1) default NULL,
  `card_holder` varchar(255) NOT NULL,
  `expiry_month` varchar(5) NOT NULL,
  `expiry_year` varchar(10) NOT NULL,
  `cvv` varchar(5) default NULL,
  `address1` varchar(255) default NULL,
  `address2` varchar(255) default NULL,
  `town` varchar(255) default NULL,
  `state` varchar(255) default NULL,
  `zip` varchar(15) default NULL,
  `country` varchar(150) default NULL,
  `email` varchar(100) default NULL,
  `phone` varchar(20) default NULL,
  `uploaded_file` varchar(255) NOT NULL,
  `status` enum('New','Approved','Reject') NOT NULL default 'New',
  `approver_id` bigint(20) default NULL,
  `comments` varchar(255) default NULL,
  `failed_attempts` int(2) default NULL,
  `created_by` bigint(20) default NULL,
  `updated_by` bigint(20) default NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



DROP FUNCTION `fnc_country_name`//
CREATE DEFINER=`root`@`localhost` FUNCTION `fnc_country_name`(pcountry_id varchar(10)
																		 ) RETURNS varchar(200) CHARSET latin1
    READS SQL DATA
    DETERMINISTIC
BEGIN
declare cname varchar(200) ;
		select country_name into cname
		  from country_ip h
		 where  country_code  =pcountry_id limit 1;
 RETURN cname;
END
