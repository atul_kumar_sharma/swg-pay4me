INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'NIS Dollar Revenue Report', 'NIS Dollar Revenue Report', '3', 'portal_admin', 'report/nisDollarReport', NULL , '4', '', '', '0', NULL , NULL
);


INSERT INTO `ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'Daily Dollar Sweep Report', 'Daily Dollar Sweep Report', '3', 'portal_admin', 'report/nisDollarRevenue', NULL , '5', '', '', '0', NULL , NULL );

ALTER TABLE `tbl_nis_dollar_revenue` ADD UNIQUE (
`date`
);
