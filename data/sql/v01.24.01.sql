ALTER TABLE  `money_order` ADD  `deleted` TINYINT( 1 ) NOT NULL DEFAULT  '0';

ALTER TABLE  `money_order` ADD  `courier_flag` ENUM(  'Yes',  'No' ) NOT NULL DEFAULT  'No' AFTER  `comments` ,
ADD  `courier_service` VARCHAR( 255 ) NOT NULL AFTER  `courier_flag` ,
ADD  `waybill_trackingno` VARCHAR( 50 ) NOT NULL AFTER  `courier_service` ;


update ep_menu set deleted = 1 where id = 51 limit 1;
update ep_menu set label = 'Update Money Order' where id = 65 limit 1;
update ep_menu set label = 'Update Money Order' where id = 90 limit 1;
