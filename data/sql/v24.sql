
ALTER TABLE `recharge_order` ADD `item_fee` FLOAT( 18, 2 ) NOT NULL AFTER `amount` ,
ADD `service_charge` FLOAT( 18, 2 ) NOT NULL AFTER `item_fee` ;


 ALTER TABLE `ipay4me_order` CHANGE `card_first` `card_first` VARCHAR( 10 ) NULL DEFAULT NULL ,
CHANGE `card_last` `card_last` VARCHAR( 10 ) NULL DEFAULT NULL ;

ALTER TABLE `recharge_order` ADD `sms_charge` FLOAT( 18, 2 ) NOT NULL AFTER `service_charge` ;



ALTER TABLE `ep_pay_easy_request` ADD `created_at` DATETIME NOT NULL , ADD `updated_at` DATETIME NOT NULL;

ALTER TABLE `ep_pay_easy_response` ADD `created_at` DATETIME NOT NULL , ADD `updated_at` DATETIME NOT NULL;

ALTER TABLE `ep_gray_pay_request` ADD `created_at` DATETIME NOT NULL , ADD `updated_at` DATETIME NOT NULL;

ALTER TABLE `ep_gray_pay_response` ADD `created_at` DATETIME NOT NULL , ADD `updated_at` DATETIME NOT NULL;

CREATE TABLE order_authorize (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, trans_date DATETIME, authorize_status ENUM('0', '1') DEFAULT '1' COMMENT 'Array', response_code VARCHAR(30), response_text TEXT, paid_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, UNIQUE INDEX unique_parameter_idx (order_number), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE order_authorize ADD CONSTRAINT order_authorize_order_number_ipay4me_order_order_number FOREIGN KEY (order_number) REFERENCES ipay4me_order(order_number) ON DELETE CASCADE;


CREATE TABLE IF NOT EXISTS `gateway_response_details` (
  `id` int(11) NOT NULL auto_increment,
  `gateway_id` int(11) NOT NULL,
  `response_code` varchar(30) NOT NULL,
  `response_text` varchar(255) NOT NULL,
  `response_description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='gateway_response_details' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `gateway_response_details`
--

INSERT INTO `gateway_response_details` (`id`, `gateway_id`, `response_code`, `response_text`, `response_description`) VALUES
(1, 2, '8', 'Successful Transaction ', ''),
(2, 2, '20', 'Declined by issuer. Reason for decline not provided by the issuer ', ''),
(3, 2, '21', 'Declined with Refer to Issuer', ''),
(4, 2, '31', 'Invalid CVC Code', ''),
(5, 2, '32', 'Invalid Card', ''),
(6, 2, '33', 'Invalid Expiry Date', ''),
(7, 2, '34', ' Expired Card', ''),
(8, 2, '43', 'Suspected fraud. Same email address as being used to different card numbers. Rejected by PayEasy gateway. ', ''),
(9, 2, '45', ' Excessive use of Cards', ''),
(10, 2, '46', 'Excessive use of IP address', ''),
(11, 2, '74', 'Declined by GrayPay usually as duplicate record returns a REFID.', ''),
(12, 2, '86', 'Invalid parameter value passed. Usually length of field exceeded.', ''),
(13, 2, '96', ' Required parameter value not passed. Declined by PayEasy Gateway as required data not provided.', ''),
(14, 2, '98', 'System Error / Format Error (Have now been replaced by 86)', ''),
(15, 2, '99', 'Gateway Timeout connecting to GrayPay', '');

------------------------Fraud Prevention ---------------------------------
CREATE TABLE fps_detail (id BIGINT AUTO_INCREMENT, order_number BIGINT NOT NULL, order_request_detail_id BIGINT NOT NULL, gateway_id SMALLINT NOT NULL, ip_address VARCHAR(20), user_id BIGINT, card_num VARCHAR(40), email VARCHAR(50), trans_date DATETIME, payment_status ENUM('0', '1') DEFAULT '1', fps_rule_id BIGINT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted TINYINT(1) DEFAULT '0' NOT NULL, created_by INT, updated_by INT,  INDEX order_request_detail_id_idx (order_request_detail_id), INDEX gateway_id_idx (gateway_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE fps_rule (id BIGINT AUTO_INCREMENT, description TEXT, deleted TINYINT(1) DEFAULT '0' NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE fps_detail ADD CONSTRAINT fps_detail_order_request_detail_id_order_request_details_id FOREIGN KEY (order_request_detail_id) REFERENCES order_request_details(id) ON DELETE CASCADE;
ALTER TABLE fps_detail ADD CONSTRAINT fps_detail_gateway_id_gateway_id FOREIGN KEY (gateway_id) REFERENCES gateway(id);

INSERT INTO `fps_rule` (
`id` ,
`description` ,
`deleted`
)
VALUES (1 , 'IP Address', '0')
, (2 , 'Credit Card', '0')
, (3 , 'email', '0');
--------------------------------------------------------------------------
ALTER TABLE `ipay4me_order` ADD FOREIGN KEY ( `order_request_detail_id` ) REFERENCES `order_request_details` (`id`);

ALTER TABLE `ipay4me_order` ADD FOREIGN KEY ( `gateway_id` ) REFERENCES `gateway` (`id`);

ALTER TABLE `ipay4me_order` DROP FOREIGN KEY `ipay4me_order_ibfk_1` ;

ALTER TABLE `ipay4me_order` ADD FOREIGN KEY ( `order_request_detail_id` ) REFERENCES `order_request_details` (`id`);

