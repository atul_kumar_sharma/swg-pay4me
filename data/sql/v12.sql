ALTER TABLE `merchant` ADD `description` VARCHAR( 255 ) NULL AFTER `contact_phone` ,
ADD `abbr` VARCHAR( 150 ) NULL AFTER `description` ;

UPDATE `merchant` SET `name` = 'NIS Passport',
`address` = 'Nigeria Immigration Service Nnamdi Azikiwe Int’l Airport Road, Sauka, Abuja. P.M.B. 38, Garki, Abuja.',
`contact_phone` = '+234-9-7806771',
`description` = 'Nigerian Passport Service',
`abbr` = 'NIS' WHERE `merchant`.`id` =1 ;

UPDATE `merchant` SET `name` = 'NIS Entry Visa',
`address` = 'Nigeria Immigration Service Nnamdi Azikiwe Int’l Airport Road, Sauka, Abuja. P.M.B. 38, Garki, Abuja.',
`contact_phone` = '+234-9-7806771',
`description` = 'Nigerian Entry Visa',
`abbr` = 'NIS' WHERE `merchant`.`id` =2 ;

UPDATE `merchant` SET `name` = 'NIS Freezone',
`address` = 'Nigeria Immigration Service Nnamdi Azikiwe Int’l Airport Road, Sauka, Abuja. P.M.B. 38, Garki, Abuja.',
`contact_phone` = '+234-9-7806771',
`description` = 'Nigerian Freezone Service',
`abbr` = 'NIS' WHERE `merchant`.`id` =3 LIMIT 1 ;


UPDATE `merchant` SET `name` = 'NIS MRP',
`address` = 'Nigeria Immigration Service Nnamdi Azikiwe Int’l Airport Road, Sauka, Abuja. P.M.B. 38, Garki, Abuja.',
`contact_phone` = '+234-9-7806771',
`description` = 'Nigerian MRP Service',
`abbr` = 'NIS' WHERE `merchant`.`id` =5  ;


