
INSERT INTO `sf_guard_group` (
`id` ,
`name` ,
`description` ,
`created_at` ,
`updated_at`
)
VALUES (
NULL , 'configure_manager', 'Configure Manager', '2011-09-22 00:00:00', '2011-09-22 00:00:00'
);


INSERT INTO `sf_guard_permission` (
`id` ,
`name` ,
`description` ,
`created_at` ,
`updated_at`
)
VALUES (
NULL , 'configure_manager', 'Configure Manager', '2011-09-22 00:00:00', '2011-09-22 00:00:00'
);


INSERT INTO `sf_guard_group_permission` (
`group_id` ,
`permission_id` ,
`created_at` ,
`updated_at`
)
VALUES (
'8', '9', '2011-09-22 00:00:00', '2011-09-22 00:00:00'
);

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
(83, 'Configuration', 'Configuration', 0, 'configure_manager', NULL, NULL, 'custom', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);


INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES
('Mid Validation', 'Mid Validation', 78, 'configure_manager', 'configurationManager/midMasterValidations', NULL, 'custom', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);

-- For 15 mints break...
ALTER TABLE  `ipay4me_order` ADD  `jobId` BIGINT( 20 ) NOT NULL DEFAULT  '0' AFTER  `phone` ;

INSERT INTO `ep_menu` (`name`, `label`, `parent_id`, `sfguardperm`, `url`, `eval`, `env`, `sequence`, `created_at`, `updated_at`, `deleted`, `created_by`, `updated_by`) VALUES ('Black Listed User Report', 'Black Listed User Report', 3, 'admin_support_user', 'supportTool/blackListedReport', NULL, 'custom', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, NULL);
