CREATE TABLE ep_gray_pay_capture_data (id BIGINT AUTO_INCREMENT, gateway_id BIGINT NOT NULL, transactionid VARCHAR(30) NOT NULL, orderid VARCHAR(20) NOT NULL, amount DOUBLE(18, 2) NOT NULL, response_text VARCHAR(255), response_code VARCHAR(30), is_captured VARCHAR(255) DEFAULT 'no' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE `ep_gray_pay_capture_data` CHANGE `is_captured` `is_captured` ENUM( 'no', 'yes' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'no';

ALTER TABLE `ep_gray_pay_capture_data` CHANGE `is_captured` `is_captured` ENUM( 'no', 'yes', 'success', 'fail' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'no' ;

ALTER TABLE `ep_gray_pay_capture_data` ADD `midService` CHAR( 4 ) NOT NULL AFTER `response_code` ;