 ALTER TABLE `order_request_split` ADD `item_fee` BIGINT NOT NULL AFTER `amount` ;
 UPDATE `order_request_split` SET `transaction_charges` = round( `transaction_charges` ) WHERE 1;
 UPDATE `order_request_split` SET `item_fee` = `amount` - `transaction_charges` WHERE 1;

 ALTER TABLE `order_request_split` CHANGE `transaction_charges` `transaction_charges` BIGINT( 20 ) NULL DEFAULT NULL COMMENT 'cents';
 ALTER TABLE `order_request_split` CHANGE `amount` `amount` BIGINT( 20 ) NOT NULL COMMENT 'cents';
 ALTER TABLE `order_request_split` CHANGE `item_fee` `item_fee` BIGINT( 20 ) NOT NULL COMMENT 'cents' ;

ALTER TABLE `ep_gray_pay_request` DROP `cvv`;
 ALTER TABLE `ep_gray_pay_request` ADD `cvv` VARCHAR( 10 ) NOT NULL DEFAULT 'XXX' AFTER `card_holder`

ALTER TABLE `ipay4me_order` ADD `gray_pay_order_id` BIGINT( 20 ) NULL ;
 ALTER TABLE `ipay4me_order` ADD INDEX ( `gray_pay_order_id` )  ;
ALTER TABLE `ipay4me_order` ADD FOREIGN KEY ( `gray_pay_order_id` ) REFERENCES `ipay4me`.`ep_gray_pay_request` (`id`);

 ALTER TABLE `ep_gray_pay_response` DROP `amount`  ;