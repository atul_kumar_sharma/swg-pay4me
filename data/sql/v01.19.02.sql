CREATE TABLE country_based_register_card (id BIGINT AUTO_INCREMENT, country_code VARCHAR(20), is_registered VARCHAR(5) DEFAULT 'no' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, created_by INT, updated_by INT, PRIMARY KEY(id)) ENGINE = INNODB;

INSERT INTO `country_based_register_card` (
`country_code` ,
`is_registered` ,
`created_at` ,
`updated_at` ,
`created_by` ,
`updated_by`
)
VALUES (
'ALL', 'no', '2011-08-25 15:23:34', '', NULL , NULL
), (
'IT', 'yes', '2011-08-25 15:23:44', '', NULL , NULL
);