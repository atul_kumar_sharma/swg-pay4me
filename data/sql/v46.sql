UPDATE `pay4meInt`.`ep_menu` SET `deleted` = '1' WHERE `ep_menu`.`id` =31 LIMIT 1 ;

UPDATE `pay4meInt`.`ep_menu` SET `deleted` = '1' WHERE `ep_menu`.`id` =32 LIMIT 1 ;

UPDATE `pay4meInt`.`ep_menu` SET `deleted` = '1' WHERE `ep_menu`.`id` =33 LIMIT 1 ;


 CREATE TABLE `pay4meInt`.`vault_refund_list` (
`id` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ,
`vault_num` BIGINT( 20 ) NULL ,
`order_number` BIGINT( 20 ) NULL ,
`created_at` DATETIME NULL ,
`created_by` BIGINT( 20 ) NULL ,
`updated_at` DATETIME NULL ,
`updated_by` BIGINT( 20 ) NULL ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB ;

ALTER TABLE `rollback_payment` ADD `created_at` BIGINT( 20 ) NULL AFTER `comments` ,
ADD `updated_at` DATETIME NULL AFTER `created_at` ,
ADD `created_by` BIGINT( 20 ) NULL AFTER `updated_at` ,
ADD `updated_by` DATETIME NULL AFTER `created_by` ;


 INSERT INTO `pay4meInt`.`ep_menu` (
`id` ,
`name` ,
`label` ,
`parent_id` ,
`sfguardperm` ,
`url` ,
`eval` ,
`sequence` ,
`created_at` ,
`updated_at` ,
`deleted` ,
`created_by` ,
`updated_by`
)
VALUES (
NULL , 'New Support User', 'New Support User', '16', 'portal_admin', 'userAdmin/newSupportUser', NULL , '3', '', '', '0', NULL , NULL
);