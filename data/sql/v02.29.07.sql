ALTER TABLE  `transaction_service_charges` ADD  `app_convert_amount` FLOAT( 18, 2 ) NOT NULL AFTER  `app_amount` , ADD  `app_convert_service_charge` FLOAT( 18, 2 ) NOT NULL AFTER  `app_convert_amount`, ADD  `app_currency` ENUM(  'dollar',  'pound' ) NOT NULL AFTER  `app_convert_amount`;

ALTER TABLE  `transaction_service_charges` CHANGE  `service_charge`  `service_charge` FLOAT( 8, 2 ) NOT NULL , CHANGE  `app_amount`  `app_amount` FLOAT( 8, 2 ) NOT NULL;

ALTER TABLE `tbl_vap_application` MODIFY COLUMN `ref_no` bigint(20) AFTER `id`;