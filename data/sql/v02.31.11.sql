CREATE TABLE user_login_logs (id BIGINT AUTO_INCREMENT, user_id INT NOT NULL, login_mode VARCHAR(50), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX user_id_idx (user_id), PRIMARY KEY(id)) ENGINE = INNODB;

ALTER TABLE user_login_logs ADD CONSTRAINT user_login_logs_user_id_sf_guard_user_id FOREIGN KEY (user_id) REFERENCES sf_guard_user(id) ON DELETE CASCADE;

ALTER TABLE  `sf_guard_user` DROP INDEX  `idx_unq_username`;