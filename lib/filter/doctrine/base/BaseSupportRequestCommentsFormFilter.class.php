<?php

/**
 * SupportRequestComments filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSupportRequestCommentsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'), 'add_empty' => true)),
      'support_user_comments' => new sfWidgetFormFilterInput(),
      'customer_comments'     => new sfWidgetFormFilterInput(),
      'upload_document1'      => new sfWidgetFormFilterInput(),
      'upload_document2'      => new sfWidgetFormFilterInput(),
      'upload_document3'      => new sfWidgetFormFilterInput(),
      'upload_document4'      => new sfWidgetFormFilterInput(),
      'user_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'            => new sfWidgetFormFilterInput(),
      'updated_by'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'request_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SupportRequestList'), 'column' => 'id')),
      'support_user_comments' => new sfValidatorPass(array('required' => false)),
      'customer_comments'     => new sfValidatorPass(array('required' => false)),
      'upload_document1'      => new sfValidatorPass(array('required' => false)),
      'upload_document2'      => new sfValidatorPass(array('required' => false)),
      'upload_document3'      => new sfValidatorPass(array('required' => false)),
      'upload_document4'      => new sfValidatorPass(array('required' => false)),
      'user_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('support_request_comments_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SupportRequestComments';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'request_id'            => 'ForeignKey',
      'support_user_comments' => 'Text',
      'customer_comments'     => 'Text',
      'upload_document1'      => 'Text',
      'upload_document2'      => 'Text',
      'upload_document3'      => 'Text',
      'upload_document4'      => 'Text',
      'user_id'               => 'ForeignKey',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
      'created_by'            => 'Number',
      'updated_by'            => 'Number',
    );
  }
}
