<?php

/**
 * GatewayResponseDetails filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGatewayResponseDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => true)),
      'response_code'        => new sfWidgetFormFilterInput(),
      'response_text'        => new sfWidgetFormFilterInput(),
      'response_description' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'gateway_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Gateway'), 'column' => 'id')),
      'response_code'        => new sfValidatorPass(array('required' => false)),
      'response_text'        => new sfValidatorPass(array('required' => false)),
      'response_description' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('gateway_response_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GatewayResponseDetails';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'gateway_id'           => 'ForeignKey',
      'response_code'        => 'Text',
      'response_text'        => 'Text',
      'response_description' => 'Text',
    );
  }
}
