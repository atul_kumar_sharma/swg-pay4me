<?php

/**
 * VbvOrder filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVbvOrderFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpVbvRequest'), 'add_empty' => true)),
      'order_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => true)),
      'type'            => new sfWidgetFormChoice(array('choices' => array('' => '', 'pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->setValidators(array(
      'order_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpVbvRequest'), 'column' => 'id')),
      'order_detail_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('OrderRequestDetails'), 'column' => 'id')),
      'type'            => new sfValidatorChoice(array('required' => false, 'choices' => array('pay' => 'pay', 'recharge' => 'recharge'))),
    ));

    $this->widgetSchema->setNameFormat('vbv_order_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VbvOrder';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'order_id'        => 'ForeignKey',
      'order_detail_id' => 'ForeignKey',
      'type'            => 'Enum',
    );
  }
}
