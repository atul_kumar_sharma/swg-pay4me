<?php

/**
 * UserDetail filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUserDetailFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'first_name'         => new sfWidgetFormFilterInput(),
      'last_name'          => new sfWidgetFormFilterInput(),
      'dob'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'address'            => new sfWidgetFormFilterInput(),
      'email'              => new sfWidgetFormFilterInput(),
      'mobile_phone'       => new sfWidgetFormFilterInput(),
      'work_phone'         => new sfWidgetFormFilterInput(),
      'failed_attempt'     => new sfWidgetFormFilterInput(),
      'kyc_status'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'approved_by'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'disapprove_reason'  => new sfWidgetFormFilterInput(),
      'master_account_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'sms_charge'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'is_sms_charge_paid' => new sfWidgetFormChoice(array('choices' => array('' => '', 'yes' => 'yes', 'no' => 'no'))),
      'cart_items'         => new sfWidgetFormFilterInput(),
      'payment_rights'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'deleted'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_by'         => new sfWidgetFormFilterInput(),
      'updated_by'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'user_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'first_name'         => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(array('required' => false)),
      'dob'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'address'            => new sfValidatorPass(array('required' => false)),
      'email'              => new sfValidatorPass(array('required' => false)),
      'mobile_phone'       => new sfValidatorPass(array('required' => false)),
      'work_phone'         => new sfValidatorPass(array('required' => false)),
      'failed_attempt'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'kyc_status'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'approved_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'disapprove_reason'  => new sfValidatorPass(array('required' => false)),
      'master_account_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'sms_charge'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'is_sms_charge_paid' => new sfValidatorChoice(array('required' => false, 'choices' => array('yes' => 'yes', 'no' => 'no'))),
      'cart_items'         => new sfValidatorPass(array('required' => false)),
      'payment_rights'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'deleted'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('user_detail_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserDetail';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'user_id'            => 'ForeignKey',
      'first_name'         => 'Text',
      'last_name'          => 'Text',
      'dob'                => 'Date',
      'address'            => 'Text',
      'email'              => 'Text',
      'mobile_phone'       => 'Text',
      'work_phone'         => 'Text',
      'failed_attempt'     => 'Number',
      'kyc_status'         => 'Number',
      'approved_by'        => 'Number',
      'disapprove_reason'  => 'Text',
      'master_account_id'  => 'ForeignKey',
      'sms_charge'         => 'Number',
      'is_sms_charge_paid' => 'Enum',
      'cart_items'         => 'Text',
      'payment_rights'     => 'Number',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
      'deleted'            => 'Boolean',
      'created_by'         => 'Number',
      'updated_by'         => 'Number',
    );
  }
}
