<?php

/**
 * CartTrackingNumber filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCartTrackingNumberFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'cart_id'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tracking_number'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'order_number'            => new sfWidgetFormFilterInput(),
      'order_request_detail_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cart_amount'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'convert_amount'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'                => new sfWidgetFormFilterInput(),
      'user_id'                 => new sfWidgetFormFilterInput(),
      'associated_date'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Associated' => 'Associated', 'Paid' => 'Paid'))),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'              => new sfWidgetFormFilterInput(),
      'updated_by'              => new sfWidgetFormFilterInput(),
      'deleted'                 => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'cart_id'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tracking_number'         => new sfValidatorPass(array('required' => false)),
      'order_number'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'order_request_detail_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'cart_amount'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'convert_amount'          => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_id'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'associated_date'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'status'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Associated' => 'Associated', 'Paid' => 'Paid'))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'deleted'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('cart_tracking_number_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartTrackingNumber';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'cart_id'                 => 'Number',
      'tracking_number'         => 'Text',
      'order_number'            => 'Number',
      'order_request_detail_id' => 'Number',
      'cart_amount'             => 'Number',
      'convert_amount'          => 'Number',
      'currency'                => 'Number',
      'user_id'                 => 'Number',
      'associated_date'         => 'Number',
      'status'                  => 'Enum',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
      'created_by'              => 'Number',
      'updated_by'              => 'Number',
      'deleted'                 => 'Boolean',
    );
  }
}
