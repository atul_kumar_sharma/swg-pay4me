<?php

/**
 * CurrencyValue filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCurrencyValueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'value'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency_date'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'conversion_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'naira_to_dollor' => 'naira_to_dollor', 'dollor_to_naira' => 'dollor_to_naira'))),
      'description'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'value'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency_date'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'conversion_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('naira_to_dollor' => 'naira_to_dollor', 'dollor_to_naira' => 'dollor_to_naira'))),
      'description'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('currency_value_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyValue';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'value'           => 'Number',
      'currency_date'   => 'Date',
      'conversion_type' => 'Enum',
      'description'     => 'Text',
    );
  }
}
