<?php

/**
 * Wiretransfer filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseWiretransferFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'wire_transfer_number' => new sfWidgetFormFilterInput(),
      'amount'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'wire_transfer_date'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'paid_date'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'address'              => new sfWidgetFormFilterInput(),
      'wire_transfer_proof'  => new sfWidgetFormFilterInput(),
      'phone'                => new sfWidgetFormFilterInput(),
      'wire_transfer_office' => new sfWidgetFormFilterInput(),
      'comments'             => new sfWidgetFormFilterInput(),
      'status'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid'))),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'           => new sfWidgetFormFilterInput(),
      'updated_by'           => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'wire_transfer_number' => new sfValidatorPass(array('required' => false)),
      'amount'               => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'wire_transfer_date'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'paid_date'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'address'              => new sfValidatorPass(array('required' => false)),
      'wire_transfer_proof'  => new sfValidatorPass(array('required' => false)),
      'phone'                => new sfValidatorPass(array('required' => false)),
      'wire_transfer_office' => new sfValidatorPass(array('required' => false)),
      'comments'             => new sfValidatorPass(array('required' => false)),
      'status'               => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid'))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('wiretransfer_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Wiretransfer';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'wire_transfer_number' => 'Text',
      'amount'               => 'Number',
      'wire_transfer_date'   => 'Date',
      'paid_date'            => 'Date',
      'address'              => 'Text',
      'wire_transfer_proof'  => 'Text',
      'phone'                => 'Text',
      'wire_transfer_office' => 'Text',
      'comments'             => 'Text',
      'status'               => 'Enum',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
      'created_by'           => 'Number',
      'updated_by'           => 'Number',
    );
  }
}
