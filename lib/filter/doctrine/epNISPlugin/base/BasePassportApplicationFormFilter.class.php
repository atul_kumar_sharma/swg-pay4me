<?php

/**
 * PassportApplication filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'passporttype_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportAppType'), 'add_empty' => true)),
      'ref_no'                        => new sfWidgetFormFilterInput(),
      'title_id'                      => new sfWidgetFormChoice(array('choices' => array('' => '', 'MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'last_name'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'mid_name'                      => new sfWidgetFormFilterInput(),
      'email'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'occupation'                    => new sfWidgetFormFilterInput(),
      'gender_id'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_of_birth'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'color_eyes_id'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'color_hair_id'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'marital_status_id'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'ispaid'                        => new sfWidgetFormFilterInput(),
      'payment_trans_id'              => new sfWidgetFormFilterInput(),
      'maid_name'                     => new sfWidgetFormFilterInput(),
      'height'                        => new sfWidgetFormFilterInput(),
      'processing_country_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'processing_state_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'processing_embassy_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => true)),
      'processing_passport_office_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportOffice'), 'add_empty' => true)),
      'next_kin'                      => new sfWidgetFormFilterInput(),
      'next_kin_phone'                => new sfWidgetFormFilterInput(),
      'relation_with_kin'             => new sfWidgetFormFilterInput(),
      'next_kin_address_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportKinAddress'), 'add_empty' => true)),
      'passport_no'                   => new sfWidgetFormFilterInput(),
      'interview_date'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'status'                        => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'add_empty' => true)),
      'paid_dollar_amount'            => new sfWidgetFormFilterInput(),
      'local_currency_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LocalCurrencyType'), 'add_empty' => true)),
      'paid_local_currency_amount'    => new sfWidgetFormFilterInput(),
      'amount'                        => new sfWidgetFormFilterInput(),
      'currency_id'                   => new sfWidgetFormFilterInput(),
      'paid_at'                       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'is_email_valid'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'passporttype_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportAppType'), 'column' => 'id')),
      'ref_no'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title_id'                      => new sfValidatorChoice(array('required' => false, 'choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'first_name'                    => new sfValidatorPass(array('required' => false)),
      'last_name'                     => new sfValidatorPass(array('required' => false)),
      'mid_name'                      => new sfValidatorPass(array('required' => false)),
      'email'                         => new sfValidatorPass(array('required' => false)),
      'occupation'                    => new sfValidatorPass(array('required' => false)),
      'gender_id'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female', 'None' => 'None'))),
      'place_of_birth'                => new sfValidatorPass(array('required' => false)),
      'date_of_birth'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'color_eyes_id'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'color_hair_id'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'marital_status_id'             => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'ispaid'                        => new sfValidatorPass(array('required' => false)),
      'payment_trans_id'              => new sfValidatorPass(array('required' => false)),
      'maid_name'                     => new sfValidatorPass(array('required' => false)),
      'height'                        => new sfValidatorPass(array('required' => false)),
      'processing_country_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'processing_state_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('State'), 'column' => 'id')),
      'processing_embassy_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EmbassyMaster'), 'column' => 'id')),
      'processing_passport_office_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportOffice'), 'column' => 'id')),
      'next_kin'                      => new sfValidatorPass(array('required' => false)),
      'next_kin_phone'                => new sfValidatorPass(array('required' => false)),
      'relation_with_kin'             => new sfValidatorPass(array('required' => false)),
      'next_kin_address_id'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportKinAddress'), 'column' => 'id')),
      'passport_no'                   => new sfValidatorPass(array('required' => false)),
      'interview_date'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'status'                        => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentGatewayType'), 'column' => 'id')),
      'paid_dollar_amount'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'local_currency_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('LocalCurrencyType'), 'column' => 'id')),
      'paid_local_currency_amount'    => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount'                        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency_id'                   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_at'                       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'is_email_valid'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('passport_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplication';
  }

  public function getFields()
  {
    return array(
      'id'                            => 'Number',
      'passporttype_id'               => 'ForeignKey',
      'ref_no'                        => 'Number',
      'title_id'                      => 'Enum',
      'first_name'                    => 'Text',
      'last_name'                     => 'Text',
      'mid_name'                      => 'Text',
      'email'                         => 'Text',
      'occupation'                    => 'Text',
      'gender_id'                     => 'Enum',
      'place_of_birth'                => 'Text',
      'date_of_birth'                 => 'Date',
      'color_eyes_id'                 => 'Enum',
      'color_hair_id'                 => 'Enum',
      'marital_status_id'             => 'Enum',
      'ispaid'                        => 'Text',
      'payment_trans_id'              => 'Text',
      'maid_name'                     => 'Text',
      'height'                        => 'Text',
      'processing_country_id'         => 'ForeignKey',
      'processing_state_id'           => 'ForeignKey',
      'processing_embassy_id'         => 'ForeignKey',
      'processing_passport_office_id' => 'ForeignKey',
      'next_kin'                      => 'Text',
      'next_kin_phone'                => 'Text',
      'relation_with_kin'             => 'Text',
      'next_kin_address_id'           => 'ForeignKey',
      'passport_no'                   => 'Text',
      'interview_date'                => 'Date',
      'status'                        => 'Enum',
      'payment_gateway_id'            => 'ForeignKey',
      'paid_dollar_amount'            => 'Number',
      'local_currency_id'             => 'ForeignKey',
      'paid_local_currency_amount'    => 'Number',
      'amount'                        => 'Number',
      'currency_id'                   => 'Number',
      'paid_at'                       => 'Date',
      'is_email_valid'                => 'Boolean',
      'created_at'                    => 'Date',
      'updated_at'                    => 'Date',
    );
  }
}
