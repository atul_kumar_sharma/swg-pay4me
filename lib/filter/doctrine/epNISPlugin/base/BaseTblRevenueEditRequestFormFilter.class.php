<?php

/**
 * TblRevenueEditRequest filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblRevenueEditRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'current_amount' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'project_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblProjectName'), 'add_empty' => true)),
      'account_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblBankDetails'), 'add_empty' => true)),
      'comments'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'admin_comments' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'status'         => new sfWidgetFormChoice(array('choices' => array('' => '', 'Requested' => 'Requested', 'Approved' => 'Approved', 'Declined' => 'Declined'))),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'     => new sfWidgetFormFilterInput(),
      'updated_by'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'current_amount' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'project_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TblProjectName'), 'column' => 'id')),
      'account_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TblBankDetails'), 'column' => 'id')),
      'comments'       => new sfValidatorPass(array('required' => false)),
      'admin_comments' => new sfValidatorPass(array('required' => false)),
      'status'         => new sfValidatorChoice(array('required' => false, 'choices' => array('Requested' => 'Requested', 'Approved' => 'Approved', 'Declined' => 'Declined'))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('tbl_revenue_edit_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblRevenueEditRequest';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'current_amount' => 'Number',
      'date'           => 'Date',
      'project_id'     => 'ForeignKey',
      'account_id'     => 'ForeignKey',
      'comments'       => 'Text',
      'admin_comments' => 'Text',
      'status'         => 'Enum',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
      'created_by'     => 'Number',
      'updated_by'     => 'Number',
    );
  }
}
