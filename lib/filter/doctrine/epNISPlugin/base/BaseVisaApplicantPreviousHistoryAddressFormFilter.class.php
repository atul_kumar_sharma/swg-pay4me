<?php

/**
 * VisaApplicantPreviousHistoryAddress filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicantPreviousHistoryAddressFormFilter extends AddressMasterFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('visa_applicant_previous_history_address_filters[%s]');
  }

  public function getModelName()
  {
    return 'VisaApplicantPreviousHistoryAddress';
  }
}
