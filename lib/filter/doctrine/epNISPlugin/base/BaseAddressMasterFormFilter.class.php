<?php

/**
 * AddressMaster filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAddressMasterFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'address_1'  => new sfWidgetFormFilterInput(),
      'address_2'  => new sfWidgetFormFilterInput(),
      'city'       => new sfWidgetFormFilterInput(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'state'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'lga_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LGA'), 'add_empty' => true)),
      'district'   => new sfWidgetFormFilterInput(),
      'postcode'   => new sfWidgetFormFilterInput(),
      'var_type'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'address_1'  => new sfValidatorPass(array('required' => false)),
      'address_2'  => new sfValidatorPass(array('required' => false)),
      'city'       => new sfValidatorPass(array('required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Country'), 'column' => 'id')),
      'state'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('State'), 'column' => 'id')),
      'lga_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('LGA'), 'column' => 'id')),
      'district'   => new sfValidatorPass(array('required' => false)),
      'postcode'   => new sfValidatorPass(array('required' => false)),
      'var_type'   => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('address_master_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AddressMaster';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'address_1'  => 'Text',
      'address_2'  => 'Text',
      'city'       => 'Text',
      'country_id' => 'ForeignKey',
      'state'      => 'ForeignKey',
      'lga_id'     => 'ForeignKey',
      'district'   => 'Text',
      'postcode'   => 'Text',
      'var_type'   => 'Text',
    );
  }
}
