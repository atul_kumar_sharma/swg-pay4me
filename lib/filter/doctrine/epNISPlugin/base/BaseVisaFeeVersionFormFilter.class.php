<?php

/**
 * VisaFeeVersion filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaFeeVersionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'country_id'        => new sfWidgetFormFilterInput(),
      'visa_cat_id'       => new sfWidgetFormFilterInput(),
      'visa_type_id'      => new sfWidgetFormFilterInput(),
      'entry_type_id'     => new sfWidgetFormFilterInput(),
      'naira_amount'      => new sfWidgetFormFilterInput(),
      'dollar_amount'     => new sfWidgetFormFilterInput(),
      'is_fee_multiplied' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_gratis'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'        => new sfWidgetFormFilterInput(),
      'updated_by'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'country_id'        => new sfValidatorPass(array('required' => false)),
      'visa_cat_id'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visa_type_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'entry_type_id'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'naira_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_fee_multiplied' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_gratis'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('visa_fee_version_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaFeeVersion';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'country_id'        => 'Text',
      'visa_cat_id'       => 'Number',
      'visa_type_id'      => 'Number',
      'entry_type_id'     => 'Number',
      'naira_amount'      => 'Number',
      'dollar_amount'     => 'Number',
      'is_fee_multiplied' => 'Boolean',
      'is_gratis'         => 'Boolean',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'created_by'        => 'Number',
      'updated_by'        => 'Number',
      'version'           => 'Number',
    );
  }
}
