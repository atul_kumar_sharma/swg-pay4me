<?php

/**
 * CartItemsTransactions filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCartItemsTransactionsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'cart_id'            => new sfWidgetFormFilterInput(),
      'item_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentRequest'), 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentResponse'), 'add_empty' => true)),
      'created_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'cart_id'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'item_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('IPaymentRequest'), 'column' => 'id')),
      'transaction_number' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('IPaymentResponse'), 'column' => 'id')),
      'created_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('cart_items_transactions_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartItemsTransactions';
  }

  public function getFields()
  {
    return array(
      'id'                 => 'Number',
      'cart_id'            => 'Number',
      'item_id'            => 'ForeignKey',
      'transaction_number' => 'ForeignKey',
      'created_at'         => 'Date',
      'updated_at'         => 'Date',
    );
  }
}
