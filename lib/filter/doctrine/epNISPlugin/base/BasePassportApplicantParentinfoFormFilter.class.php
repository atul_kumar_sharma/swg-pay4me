<?php

/**
 * PassportApplicantParentinfo filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantParentinfoFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'application_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'father_name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'father_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('FatherCountry'), 'add_empty' => true)),
      'father_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportFatherAddress'), 'add_empty' => true)),
      'mother_name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'mother_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MotherCountry'), 'add_empty' => true)),
      'mother_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportMotherAddress'), 'add_empty' => true)),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'application_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportApplication'), 'column' => 'id')),
      'father_name'           => new sfValidatorPass(array('required' => false)),
      'father_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('FatherCountry'), 'column' => 'id')),
      'father_address_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportFatherAddress'), 'column' => 'id')),
      'mother_name'           => new sfValidatorPass(array('required' => false)),
      'mother_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MotherCountry'), 'column' => 'id')),
      'mother_address_id'     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PassportMotherAddress'), 'column' => 'id')),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_parentinfo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantParentinfo';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'application_id'        => 'ForeignKey',
      'father_name'           => 'Text',
      'father_nationality_id' => 'ForeignKey',
      'father_address_id'     => 'ForeignKey',
      'mother_name'           => 'Text',
      'mother_nationality_id' => 'ForeignKey',
      'mother_address_id'     => 'ForeignKey',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
    );
  }
}
