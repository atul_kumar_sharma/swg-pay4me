<?php

/**
 * Execution filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExecutionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'workflow_id'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_parent'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_started'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_variables'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_waiting_for'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_threads'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'execution_next_thread_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'workflow_id'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_parent'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_started'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'execution_variables'      => new sfValidatorPass(array('required' => false)),
      'execution_waiting_for'    => new sfValidatorPass(array('required' => false)),
      'execution_threads'        => new sfValidatorPass(array('required' => false)),
      'execution_next_thread_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('execution_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Execution';
  }

  public function getFields()
  {
    return array(
      'execution_id'             => 'Number',
      'workflow_id'              => 'Number',
      'execution_parent'         => 'Number',
      'execution_started'        => 'Number',
      'execution_variables'      => 'Text',
      'execution_waiting_for'    => 'Text',
      'execution_threads'        => 'Text',
      'execution_next_thread_id' => 'Number',
    );
  }
}
