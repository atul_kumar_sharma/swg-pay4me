<?php

/**
 * VisaApplication filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'visacategory_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'add_empty' => true)),
      'zone_type_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaZoneType'), 'add_empty' => true)),
      'ref_no'                  => new sfWidgetFormFilterInput(),
      'title'                   => new sfWidgetFormChoice(array('choices' => array('' => '', 'MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'middle_name'             => new sfWidgetFormFilterInput(),
      'other_name'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'gender'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'marital_status'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'place_of_birth'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'hair_color'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfWidgetFormChoice(array('choices' => array('' => '', 'Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfWidgetFormFilterInput(),
      'height'                  => new sfWidgetFormFilterInput(),
      'present_nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrentCountry'), 'add_empty' => true)),
      'previous_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PreviousCountry'), 'add_empty' => true)),
      'permanent_address_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaPermanentAddress'), 'add_empty' => true)),
      'perm_phone_no'           => new sfWidgetFormFilterInput(),
      'profession'              => new sfWidgetFormFilterInput(),
      'office_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOfficeAddress'), 'add_empty' => true)),
      'office_phone_no'         => new sfWidgetFormFilterInput(),
      'milltary_in'             => new sfWidgetFormFilterInput(),
      'military_dt_from'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'military_dt_to'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'ispaid'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'payment_trans_id'        => new sfWidgetFormFilterInput(),
      'term_chk_flg'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'interview_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'add_empty' => true)),
      'paid_dollar_amount'      => new sfWidgetFormFilterInput(),
      'paid_naira_amount'       => new sfWidgetFormFilterInput(),
      'amount'                  => new sfWidgetFormFilterInput(),
      'currency_id'             => new sfWidgetFormFilterInput(),
      'paid_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'is_email_valid'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'visacategory_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaCategory'), 'column' => 'id')),
      'zone_type_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaZoneType'), 'column' => 'id')),
      'ref_no'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'title'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfValidatorPass(array('required' => false)),
      'middle_name'             => new sfValidatorPass(array('required' => false)),
      'other_name'              => new sfValidatorPass(array('required' => false)),
      'gender'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfValidatorPass(array('required' => false)),
      'marital_status'          => new sfValidatorChoice(array('required' => false, 'choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'place_of_birth'          => new sfValidatorPass(array('required' => false)),
      'hair_color'              => new sfValidatorChoice(array('required' => false, 'choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfValidatorChoice(array('required' => false, 'choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfValidatorPass(array('required' => false)),
      'height'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'present_nationality_id'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CurrentCountry'), 'column' => 'id')),
      'previous_nationality_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PreviousCountry'), 'column' => 'id')),
      'permanent_address_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaPermanentAddress'), 'column' => 'id')),
      'perm_phone_no'           => new sfValidatorPass(array('required' => false)),
      'profession'              => new sfValidatorPass(array('required' => false)),
      'office_address_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisaOfficeAddress'), 'column' => 'id')),
      'office_phone_no'         => new sfValidatorPass(array('required' => false)),
      'milltary_in'             => new sfValidatorPass(array('required' => false)),
      'military_dt_from'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'military_dt_to'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'ispaid'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'payment_trans_id'        => new sfValidatorPass(array('required' => false)),
      'term_chk_flg'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'interview_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'status'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PaymentGatewayType'), 'column' => 'id')),
      'paid_dollar_amount'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'paid_naira_amount'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount'                  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency_id'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'paid_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'is_email_valid'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('visa_application_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplication';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'visacategory_id'         => 'ForeignKey',
      'zone_type_id'            => 'ForeignKey',
      'ref_no'                  => 'Number',
      'title'                   => 'Enum',
      'surname'                 => 'Text',
      'middle_name'             => 'Text',
      'other_name'              => 'Text',
      'gender'                  => 'Enum',
      'email'                   => 'Text',
      'marital_status'          => 'Enum',
      'date_of_birth'           => 'Date',
      'place_of_birth'          => 'Text',
      'hair_color'              => 'Enum',
      'eyes_color'              => 'Enum',
      'id_marks'                => 'Text',
      'height'                  => 'Number',
      'present_nationality_id'  => 'ForeignKey',
      'previous_nationality_id' => 'ForeignKey',
      'permanent_address_id'    => 'ForeignKey',
      'perm_phone_no'           => 'Text',
      'profession'              => 'Text',
      'office_address_id'       => 'ForeignKey',
      'office_phone_no'         => 'Text',
      'milltary_in'             => 'Text',
      'military_dt_from'        => 'Date',
      'military_dt_to'          => 'Date',
      'ispaid'                  => 'Boolean',
      'payment_trans_id'        => 'Text',
      'term_chk_flg'            => 'Boolean',
      'interview_date'          => 'Date',
      'status'                  => 'Enum',
      'payment_gateway_id'      => 'ForeignKey',
      'paid_dollar_amount'      => 'Number',
      'paid_naira_amount'       => 'Number',
      'amount'                  => 'Number',
      'currency_id'             => 'Number',
      'paid_at'                 => 'Date',
      'is_email_valid'          => 'Boolean',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
