<?php

/**
 * VApplicantFiveYearsTravelHistory filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVApplicantFiveYearsTravelHistoryFormFilter extends VisaApplicantTravelHistoryFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('v_applicant_five_years_travel_history_filters[%s]');
  }

  public function getModelName()
  {
    return 'VApplicantFiveYearsTravelHistory';
  }
}
