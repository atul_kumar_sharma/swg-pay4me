<?php

/**
 * ExecutionState filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExecutionStateFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'node_state'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'node_activated_from' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'node_thread_id'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'node_state'          => new sfValidatorPass(array('required' => false)),
      'node_activated_from' => new sfValidatorPass(array('required' => false)),
      'node_thread_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('execution_state_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ExecutionState';
  }

  public function getFields()
  {
    return array(
      'execution_id'        => 'Number',
      'node_id'             => 'Number',
      'node_state'          => 'Text',
      'node_activated_from' => 'Text',
      'node_thread_id'      => 'Number',
    );
  }
}
