<?php

/**
 * EpMasterLedger filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpMasterLedgerFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'master_account_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpMasterAccount'), 'add_empty' => true)),
      'entry_type'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 'credit' => 'credit', 'debit' => 'debit'))),
      'is_cleared'                 => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 1, 0 => 0))),
      'amount'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'payment_transaction_number' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'                 => new sfWidgetFormFilterInput(),
      'updated_by'                 => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'master_account_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpMasterAccount'), 'column' => 'id')),
      'entry_type'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('credit' => 'credit', 'debit' => 'debit'))),
      'is_cleared'                 => new sfValidatorChoice(array('required' => false, 'choices' => array(1 => 1, 0 => 0))),
      'amount'                     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'description'                => new sfValidatorPass(array('required' => false)),
      'transaction_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'payment_transaction_number' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_master_ledger_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpMasterLedger';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'master_account_id'          => 'ForeignKey',
      'entry_type'                 => 'Enum',
      'is_cleared'                 => 'Enum',
      'amount'                     => 'Number',
      'description'                => 'Text',
      'transaction_date'           => 'Date',
      'payment_transaction_number' => 'Number',
      'created_at'                 => 'Date',
      'updated_at'                 => 'Date',
      'created_by'                 => 'Number',
      'updated_by'                 => 'Number',
    );
  }
}
