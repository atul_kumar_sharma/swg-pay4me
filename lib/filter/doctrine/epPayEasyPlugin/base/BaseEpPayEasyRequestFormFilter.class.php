<?php

/**
 * EpPayEasyRequest filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpPayEasyRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'tran_date'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'issuer'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'request_ip'      => new sfWidgetFormFilterInput(),
      'tran_type'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_first'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_last'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_len'        => new sfWidgetFormFilterInput(),
      'card_type'       => new sfWidgetFormFilterInput(),
      'card_holder'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_month'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'expiry_year'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'cvv'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'customersupport' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address1'        => new sfWidgetFormFilterInput(),
      'address2'        => new sfWidgetFormFilterInput(),
      'town'            => new sfWidgetFormFilterInput(),
      'state'           => new sfWidgetFormFilterInput(),
      'zip'             => new sfWidgetFormFilterInput(),
      'country'         => new sfWidgetFormFilterInput(),
      'first_name'      => new sfWidgetFormFilterInput(),
      'last_name'       => new sfWidgetFormFilterInput(),
      'email'           => new sfWidgetFormFilterInput(),
      'phone'           => new sfWidgetFormFilterInput(),
      'cus_id'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ip_address'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'request_xml'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'tran_date'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'issuer'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'request_ip'      => new sfValidatorPass(array('required' => false)),
      'tran_type'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'currency'        => new sfValidatorPass(array('required' => false)),
      'amount'          => new sfValidatorPass(array('required' => false)),
      'card_first'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_last'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_len'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'card_type'       => new sfValidatorPass(array('required' => false)),
      'card_holder'     => new sfValidatorPass(array('required' => false)),
      'expiry_month'    => new sfValidatorPass(array('required' => false)),
      'expiry_year'     => new sfValidatorPass(array('required' => false)),
      'cvv'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'customersupport' => new sfValidatorPass(array('required' => false)),
      'address1'        => new sfValidatorPass(array('required' => false)),
      'address2'        => new sfValidatorPass(array('required' => false)),
      'town'            => new sfValidatorPass(array('required' => false)),
      'state'           => new sfValidatorPass(array('required' => false)),
      'zip'             => new sfValidatorPass(array('required' => false)),
      'country'         => new sfValidatorPass(array('required' => false)),
      'first_name'      => new sfValidatorPass(array('required' => false)),
      'last_name'       => new sfValidatorPass(array('required' => false)),
      'email'           => new sfValidatorPass(array('required' => false)),
      'phone'           => new sfValidatorPass(array('required' => false)),
      'cus_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ip_address'      => new sfValidatorPass(array('required' => false)),
      'request_xml'     => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_easy_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayEasyRequest';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'tran_date'       => 'Date',
      'issuer'          => 'Number',
      'request_ip'      => 'Text',
      'tran_type'       => 'Number',
      'currency'        => 'Text',
      'amount'          => 'Text',
      'card_first'      => 'Number',
      'card_last'       => 'Number',
      'card_len'        => 'Number',
      'card_type'       => 'Text',
      'card_holder'     => 'Text',
      'expiry_month'    => 'Text',
      'expiry_year'     => 'Text',
      'cvv'             => 'Number',
      'customersupport' => 'Text',
      'address1'        => 'Text',
      'address2'        => 'Text',
      'town'            => 'Text',
      'state'           => 'Text',
      'zip'             => 'Text',
      'country'         => 'Text',
      'first_name'      => 'Text',
      'last_name'       => 'Text',
      'email'           => 'Text',
      'phone'           => 'Text',
      'cus_id'          => 'Number',
      'ip_address'      => 'Text',
      'request_xml'     => 'Text',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
