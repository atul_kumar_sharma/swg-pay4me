<?php

/**
 * EpPayEasyResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpPayEasyResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'payeasy_req_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpPayEasyRequest'), 'add_empty' => true)),
      'time_taken'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'issuer'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'response_ip'    => new sfWidgetFormFilterInput(),
      'tran_type'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tran_id'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'response_code'  => new sfWidgetFormFilterInput(),
      'response_text'  => new sfWidgetFormFilterInput(),
      'auth_code'      => new sfWidgetFormFilterInput(),
      'status'         => new sfWidgetFormFilterInput(),
      'fraud_score'    => new sfWidgetFormFilterInput(),
      'fraud_message'  => new sfWidgetFormFilterInput(),
      'response_xml'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'payeasy_req_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpPayEasyRequest'), 'column' => 'id')),
      'time_taken'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'issuer'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'response_ip'    => new sfValidatorPass(array('required' => false)),
      'tran_type'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'currency'       => new sfValidatorPass(array('required' => false)),
      'amount'         => new sfValidatorPass(array('required' => false)),
      'tran_id'        => new sfValidatorPass(array('required' => false)),
      'response_code'  => new sfValidatorPass(array('required' => false)),
      'response_text'  => new sfValidatorPass(array('required' => false)),
      'auth_code'      => new sfValidatorPass(array('required' => false)),
      'status'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fraud_score'    => new sfValidatorPass(array('required' => false)),
      'fraud_message'  => new sfValidatorPass(array('required' => false)),
      'response_xml'   => new sfValidatorPass(array('required' => false)),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_pay_easy_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpPayEasyResponse';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'payeasy_req_id' => 'ForeignKey',
      'time_taken'     => 'Number',
      'issuer'         => 'Number',
      'response_ip'    => 'Text',
      'tran_type'      => 'Number',
      'currency'       => 'Text',
      'amount'         => 'Text',
      'tran_id'        => 'Text',
      'response_code'  => 'Text',
      'response_text'  => 'Text',
      'auth_code'      => 'Text',
      'status'         => 'Number',
      'fraud_score'    => 'Text',
      'fraud_message'  => 'Text',
      'response_xml'   => 'Text',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
