<?php

/**
 * EpGrayPayResponse filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'graypay_req_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpGrayPayRequest'), 'add_empty' => true)),
      'response'       => new sfWidgetFormFilterInput(),
      'response_text'  => new sfWidgetFormFilterInput(),
      'authcode'       => new sfWidgetFormFilterInput(),
      'transactionid'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'type'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'orderid'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'avsresponse'    => new sfWidgetFormFilterInput(),
      'cvvresponse'    => new sfWidgetFormFilterInput(),
      'response_code'  => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'graypay_req_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpGrayPayRequest'), 'column' => 'id')),
      'response'       => new sfValidatorPass(array('required' => false)),
      'response_text'  => new sfValidatorPass(array('required' => false)),
      'authcode'       => new sfValidatorPass(array('required' => false)),
      'transactionid'  => new sfValidatorPass(array('required' => false)),
      'type'           => new sfValidatorPass(array('required' => false)),
      'orderid'        => new sfValidatorPass(array('required' => false)),
      'amount'         => new sfValidatorPass(array('required' => false)),
      'avsresponse'    => new sfValidatorPass(array('required' => false)),
      'cvvresponse'    => new sfValidatorPass(array('required' => false)),
      'response_code'  => new sfValidatorPass(array('required' => false)),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_gray_pay_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayResponse';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'graypay_req_id' => 'ForeignKey',
      'response'       => 'Text',
      'response_text'  => 'Text',
      'authcode'       => 'Text',
      'transactionid'  => 'Text',
      'type'           => 'Text',
      'orderid'        => 'Text',
      'amount'         => 'Text',
      'avsresponse'    => 'Text',
      'cvvresponse'    => 'Text',
      'response_code'  => 'Text',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
