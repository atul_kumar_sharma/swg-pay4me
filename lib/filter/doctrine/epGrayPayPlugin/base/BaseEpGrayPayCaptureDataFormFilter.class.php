<?php

/**
 * EpGrayPayCaptureData filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayCaptureDataFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'gateway_id'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transactionid' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'orderid'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'response_text' => new sfWidgetFormFilterInput(),
      'response_code' => new sfWidgetFormFilterInput(),
      'midService'    => new sfWidgetFormFilterInput(),
      'is_captured'   => new sfWidgetFormChoice(array('choices' => array('' => '', 'no' => 'no', 'yes' => 'yes', 'success' => 'success', 'fail' => 'fail'))),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'gateway_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transactionid' => new sfValidatorPass(array('required' => false)),
      'orderid'       => new sfValidatorPass(array('required' => false)),
      'amount'        => new sfValidatorPass(array('required' => false)),
      'response_text' => new sfValidatorPass(array('required' => false)),
      'response_code' => new sfValidatorPass(array('required' => false)),
      'midService'    => new sfValidatorPass(array('required' => false)),
      'is_captured'   => new sfValidatorChoice(array('required' => false, 'choices' => array('no' => 'no', 'yes' => 'yes', 'success' => 'success', 'fail' => 'fail'))),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_gray_pay_capture_data_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayCaptureData';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'gateway_id'    => 'Number',
      'transactionid' => 'Text',
      'orderid'       => 'Text',
      'amount'        => 'Text',
      'response_text' => 'Text',
      'response_code' => 'Text',
      'midService'    => 'Text',
      'is_captured'   => 'Enum',
      'created_at'    => 'Date',
      'updated_at'    => 'Date',
    );
  }
}
