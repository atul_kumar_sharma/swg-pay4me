<?php

/**
 * EpVbvRequest filter form base class.
 *
 * @package    ama
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpVbvRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'order_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VbvOrder'), 'add_empty' => true)),
      'mer_id'          => new sfWidgetFormFilterInput(),
      'acqbin'          => new sfWidgetFormFilterInput(),
      'purchase_amount' => new sfWidgetFormFilterInput(),
      'visual_amount'   => new sfWidgetFormFilterInput(),
      'currency'        => new sfWidgetFormFilterInput(),
      'ret_url_approve' => new sfWidgetFormFilterInput(),
      'ret_url_decline' => new sfWidgetFormFilterInput(),
      'post_url'        => new sfWidgetFormFilterInput(),
      'user_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'order_id'        => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VbvOrder'), 'column' => 'id')),
      'mer_id'          => new sfValidatorPass(array('required' => false)),
      'acqbin'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'purchase_amount' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visual_amount'   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ret_url_approve' => new sfValidatorPass(array('required' => false)),
      'ret_url_decline' => new sfValidatorPass(array('required' => false)),
      'post_url'        => new sfValidatorPass(array('required' => false)),
      'user_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'created_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_vbv_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpVbvRequest';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'order_id'        => 'ForeignKey',
      'mer_id'          => 'Text',
      'acqbin'          => 'Number',
      'purchase_amount' => 'Number',
      'visual_amount'   => 'Number',
      'currency'        => 'Number',
      'ret_url_approve' => 'Text',
      'ret_url_decline' => 'Text',
      'post_url'        => 'Text',
      'user_id'         => 'ForeignKey',
      'created_at'      => 'Date',
      'updated_at'      => 'Date',
    );
  }
}
