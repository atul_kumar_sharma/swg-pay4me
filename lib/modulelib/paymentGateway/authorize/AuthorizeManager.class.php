<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class AuthorizeManager{

    public $browserInstance ;

    /**
    * Send a mail to user/s and/or all users of a group/s using secure mail.
    *
    *@author  Anurag
    * @param  string                     $card_num              Credit Card number
    * @param  integer                    $expiry_date           Expiry date of the credit card
    * @param  string                     $amount                Amount to be paid
    * @param  array                      $params                An Associative array which will be send as extra parameters to authorize.net for identifying the payment
    *
    * @return array
    * @example
    * <pre>
    * $card_num = $request->getPostParameter("card_num") ;
    * $expiry_date = $request->getPostParameter("expiry_date") ;
    * $amount = $request->getPostParameter("amount") ;
    * $params = array('transactionNumber' => 1234567) ;
    * $gateway = "authorize" ;
    * $paymentGatewayObj = PaymentGatewayServiceFactory::getService($gateway) ;
    * $paymentResponse = $paymentGatewayObj->paymentRequest($card_num, $expiry_date, $amount, $params) ;
    * </pre>
    */


    public function paymentRequest($type, $params='') {
        $paramsStr = '' ;
        $url = sfConfig::get('app_authorize_url'); // To Be Fetched from app.yml
        //$url = "https://test.authorize.net/gateway/transact.dll";
        $login_id = sfConfig::get('app_authorize_login_id'); ;
        //$login_id = "8vLAwF38qF";
        $transaction_key = sfConfig::get('app_authorize_transaction_key');
        //$transaction_key = "6sYgn3u3Nx98r5Tb";
        $version = "3.0" ;
        //$type = "AUTH_ONLY" ;
        $relay_response = "FALSE" ;
        $delim_data = "TRUE" ;
        if(count($params)){
            foreach($params as $key => $value)
            {
                $paramsStr .= "&".$key."=".$value ;
            }
        }
        if($version == "3.1")
        {
            $requestString = "x_login=" . $login_id ."&x_tran_key=".$transaction_key."&x_version=".$version."&x_type=".$type."&x_relay_response=".$relay_response."&x_delim_data=".$delim_data.$paramsStr ;
        }else{
            $requestString = "x_login=" . $login_id ."&x_tran_key=".$transaction_key."&x_type=".$type."&x_relay_response=". $relay_response."&x_delim_data=".$delim_data.$paramsStr  ;
        }


        $browser = $this->getBrowser() ; 
        $browser->post($url, $requestString);
        $code = $browser->getResponseCode();
        if ($code != 200 ) {
          $respMsg = $browser->getResponseMessage();
          $respText = $browser->getResponseText();
          throw new Exception("Error in sending payment request:".PHP_EOL.$code.' '.$respMsg.PHP_EOL.$respText);
        }
        $respText = $browser->getResponseText();


         
        $formattedResponse = $this->formatResponse($respText) ;
//          print "<pre>";
//          print_r($formattedResponse);exit;
        return $formattedResponse ;        
    }

    public function getBrowser() {
        if(!$this->browserInstance) {
          $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
              array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
        }
        return $this->browserInstance;
  }

    // Return Authorize.net response in associative array
    private function formatResponse($response) {
      //  $responseKeys = array() ;
       // $responseValues = array() ;
       $vals = explode(",",$response);
//       print "<pre>";
//       print_r($vals);
        $formattedResponse = array() ;
        list($formattedResponse['response_code'],$formattedResponse['response_subcode'],
            $formattedResponse['response_reason'],$formattedResponse['response_reason_text'],
            $formattedResponse['authorization_code'],$formattedResponse['avs_response'],
            $formattedResponse['transaction_id'],$formattedResponse['invoice_number'],
            $formattedResponse['description'],$formattedResponse['amount']) = explode(",", $response);
        return $formattedResponse ;
    }

   /* private function array_fill_key($target, $value = '') {
        if(is_array($target)) {
            foreach($target as $key => $val) {
                $filledArray[$val] = is_array($value) ? $value[$key] : $value;
            }
        }
        return $filledArray;
    }*/
}

?>
