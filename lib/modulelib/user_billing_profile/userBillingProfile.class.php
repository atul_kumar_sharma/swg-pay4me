<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of userBillingProfileclass
 *
 * @author spandey
 */
class UserBilling {

    public function insertUserBillingDetail($params,$strProfileName){

        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $BillingProfileObj = new UserBillingProfile();
        $concat_str = "~";
        $address = $params['ep_pay_easy_request']['address1'].$concat_str.$params['ep_pay_easy_request']['address2'].$concat_str.$params['ep_pay_easy_request']['town'].$concat_str.$params['ep_pay_easy_request']['state'].$concat_str.$params['ep_pay_easy_request']['zip'].$concat_str.$params['ep_pay_easy_request']['country'];
        $BillingProfileObj->setGatewayId($params['gateway_id']);
        $BillingProfileObj->setFirstName($params['ep_pay_easy_request']['first_name']);
        $BillingProfileObj->setLastName($params['ep_pay_easy_request']['last_name']);
        $BillingProfileObj->setAddress($address);
        $BillingProfileObj->setUserId($userId);
        $BillingProfileObj->setEmail($params['ep_pay_easy_request']['email']);
        $BillingProfileObj->setPhone($params['ep_pay_easy_request']['phone']);
        $BillingProfileObj->setProfile($strProfileName);

        ############## card info insert ######################
        $cardType = $params['ep_pay_easy_request']['card_type'];
        $cardNo = $params['ep_pay_easy_request']['card_Num'];
        $card_len = strlen($cardNo);
        $card_first = substr($cardNo, 0, 4);
        $card_last = substr($cardNo, -4, 4);
        $expiryMonth = $params['ep_pay_easy_request']['expiry_date']['month'];
        $expiryYear = $params['ep_pay_easy_request']['expiry_date']['year'];

        $BillingProfileObj->setCardFirst($card_first);
        $BillingProfileObj->setCardLast($card_last);
        $BillingProfileObj->setCardLen($card_len);
        $BillingProfileObj->setCardType($cardType);
        $BillingProfileObj->setExpiryMonth($expiryMonth);
        $BillingProfileObj->setExpiryYear($expiryYear);

        ######################################################
        $BillingProfileObj->save();

    }
    public function updatUserBillingDetail($params){


        $concat_str = "~";
        $address = $params['ep_pay_easy_request']['address1'].$concat_str.$params['ep_pay_easy_request']['address2'].$concat_str.$params['ep_pay_easy_request']['town'].$concat_str.$params['ep_pay_easy_request']['state'].$concat_str.$params['ep_pay_easy_request']['zip'].$concat_str.$params['ep_pay_easy_request']['country'];
        $BillingProfileObj = Doctrine::getTable('UserBillingProfile')->find($params['Profile']);
        if(!empty($params['gateway_id']))
        $BillingProfileObj->setGatewayId($params['gateway_id']);
        if(!empty($params['ep_pay_easy_request']['first_name']))
        $BillingProfileObj->setFirstName($params['ep_pay_easy_request']['first_name']);
        if(!empty($params['ep_pay_easy_request']['last_name']))
        $BillingProfileObj->setLastName($params['ep_pay_easy_request']['last_name']);
        if(!empty($address))
        $BillingProfileObj->setAddress($address);
        if(!empty($params['ep_pay_easy_request']['email']))
        $BillingProfileObj->setEmail($params['ep_pay_easy_request']['email']);
        if(!empty($params['ep_pay_easy_request']['phone']))
        $BillingProfileObj->setPhone($params['ep_pay_easy_request']['phone']);
        $BillingProfileObj->setUpdatedAt(date('Y-m-d H:i:s'));
        $BillingProfileObj->save();

    }



}
?>
