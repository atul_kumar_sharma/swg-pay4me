<?php
class EpAuditEvent {
  //category
  public static $CATEGORY_SECURITY = 'security';
  public static $CATEGORY_TRANSACTION = 'transaction';

  // subcategory for CATEGORY_SECURITY
  public static $SUBCATEGORY_SECURITY_INCORRECT_USERNAME = 'Incorrect Username';
  public static $SUBCATEGORY_SECURITY_LOGIN = 'Login';
  public static $SUBCATEGORY_SECURITY_LOGOUT = 'Logout';
  public static $SUBCATEGORY_SECURITY_TIMEOUT = 'Timeout';
  public static $SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER = 'Incorrect Credentials';
  public static $SUBCATEGORY_SECURITY_PASSWORD_CHANGE = 'Password Change';
  //public static $SUBCATEGORY_SECURITY_PASSWORD_RESET = 'Password Reset';
  //public static $SUBCATEGORY_SECURITY_INVALID_IP = 'Invalid IP';

  public static $SUBCATEGORY_TRANSACTION_USER_CREATE = 'User Create';
  public static $SUBCATEGORY_TRANSACTION_USER_UPDATE = 'User Update';
  public static $SUBCATEGORY_TRANSACTION_USER_DELETE = 'User Delete';
//  public static $SUBCATEGORY_TRANSACTION_USER_ACTIVATE = 'User Activation';
//  public static $SUBCATEGORY_TRANSACTION_USER_DEACTIVATE = 'User Deactivation';
  public static $SUBCATEGORY_TRANSACTION_USER_BLOCKED = 'User Blocked';
  public static $SUBCATEGORY_TRANSACTION_USER_UNBLOCKED  = 'User Unblocked';

  public static $SUBCATEGORY_TRANSACTION_AUTHORISE = 'Authorise for transaction';
  public static $SUBCATEGORY_TRANSACTION_PAYMENT = 'Payment';
  public static $SUBCATEGORY_TRANSACTION_RECHARGE = 'Recharge Ewallet';
  public static $SUBCATEGORY_TRANSACTION_REFUND = 'Refund';
  public static $SUBCATEGORY_TRANSACTION_BLOCK = 'Block';
  public static $SUBCATEGORY_TRANSACTION_CHARGEBACK = 'Chargeback';


  // These are attribute names
  public static $ATTR_P4MTXNID = 'RequestOrderNumber';
  public static $ATTR_USERINFO = 'UserDetail'; // User Details
  public static $ATTR_MERCHANTINFO = 'MerchantDetail';

  public static $SUBCATEGORY_TRANSACTION_CHARGE_CHANGE = "TransactionCharges";
  
  // These are messages templates
  public static $MSG_SUBCATEGORY_TRANSACTION_AUTHORISE = "Authorised for request order number '{reqOrderNumber}'";
  public static $MSG_SUBCATEGORY_TRANSACTION_PAYMENT = "Payment made for request order number '{reqOrderNumber}' with '{amount}'";
  public static $MSG_SUBCATEGORY_TRANSACTION_RECHARGE = "eWallet charged against Account Number '{accountNumber}'";
  public static $MSG_SUBCATEGORY_SECURITY_LOGOUT = "User Logged out";
  public static $MSG_SUBCATEGORY_SECURITY_TIMEOUT = "User Time out";
  public static $MSG_SUBCATEGORY_TRANSACTION_USER_BLOCKED = "'{username}' blocked - exceded number of Login Attempts";
  public static $MSG_SUBCATEGORY_TRANSACTION_USER_UNBLOCKED = "'{username}' unblocked";
  public static $MSG_SUBCATEGORY_SECURITY_FIRST_LOGIN = "'{username}' logged in for first time";
  public static $MSG_SUBCATEGORY_SECURITY_INCORRECT_USERNAME = "Username Incorrect - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_LOGIN = "Successfull Login - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_INVALID_IP = "Invalid IP Address - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER = "Invalid Password OR Security Answer - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_SEC_ANSWER = "Invalid Security Answer - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_FIRSTTIME_PASSWORD_CHANGE = "First time Login - Password Changed - '{username}'";
  public static $MSG_SUBCATEGORY_SECURITY_PASSWORD_CHANGE = "Password Changed for '{username}'";
  public static $MSG_SUBCATEGORY_TRANSACTION_CHARGE_CHANGE = "Transaction Charge change by '{username}'";
  public static $MSG_SUBCATEGORY_TRANSACTION_REFUND = "Payment refund to '{username}'";
  public static $MSG_SUBCATEGORY_TRANSACTION_BLOCK = "'{username}' payment rights blocked";
  public static $MSG_SUBCATEGORY_TRANSACTION_CHARGEBACK = "Payment chargeback to '{username}'";
  public static $ATTR_IPAY4MEINFO = 'iPay4me';
  
  public static function getFomattedMessage($message, $replaceVars) {
    $formattedMessage = $message;
    foreach ($replaceVars as $key => $value) {
      $formattedMessage = str_replace('{'.$key.'}', $value, $formattedMessage);
    }
    return $formattedMessage;
  }

  public static function getAllCategories() {
    return array('security'=>'Security', 'transaction'=>'Transaction');
  }

  public static function getAllSubcategory($category=null) {
    $class = __CLASS__;
    $rf = new ReflectionClass($class);
    $staticProperties = $rf->getProperties(ReflectionProperty::IS_STATIC);
    $subCategories = array();
    if(!empty($category)){
      foreach ($staticProperties as $aProp) {
//        if(empty($category)) {
//          $subCategories[] = $aProp->getValue();
//          continue;
//        }
        // see if it matches with our category
        // AUDIT_TRAIL_SUBCATEGORY_<all caps of $category>_*
        $name = $aProp->getName();
        $category_variable = "SUBCATEGORY_".strtoupper($category);

        if((strpos($name,$category_variable)!==false) && (strpos($name,"MSG") === FALSE)) {
          $subCategories[] = $aProp->getValue();
        }
      }
    }
    return $subCategories;
    //   print_r($subCategories);exit;

  }

}
?>
