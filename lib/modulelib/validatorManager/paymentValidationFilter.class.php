<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PaymentValidationFilter
{
  
  public function getCountryMidValidation($midService) {
    //get First Processing Country
    $paymentModeObj = new PaymentModeManager();
    $firstProcessingCountry = $paymentModeObj->getFirstProcessingCountry();

    //get coutnry payment option details
    $arrPaymentValidation = $this->getCountryPaymentOptions($firstProcessingCountry,$midService);
    //check for if payment user is privilege user
    $isPrivilegeUser = sfContext::getInstance()->getUser()->isPrivilegeUser();
    if($isPrivilegeUser){
      $arrPaymentValidation['countryValidation'] = "No";
    }
    //case 1: if country and MID doesn't have validation
    if((strtoupper($arrPaymentValidation['countryValidation']) == 'NO') && (strtoupper($arrPaymentValidation['midValidation']) == 'NO')){
      //return blank validation array
      $arrValidation = array('0'=>'NO');
      return $arrValidation;
      
    }
    //case 2: if country has no validation , use MID validation
    else if(strtoupper($arrPaymentValidation['countryValidation']) == 'NO' && (strtoupper($arrPaymentValidation['midValidation']) != 'NO'))
    {
      if(strpos($arrPaymentValidation['midValidation'],','))
      {
        $arrValidation = explode(',',$arrPaymentValidation['midValidation']);
      }else
      {
        $arrValidation1 = strtoupper($arrPaymentValidation['midValidation']);
        $arrValidation = array('0'=>$arrValidation1);
      }
        return $arrValidation;
    }
    //case 3: if MID has no validation , use country validation
    else if(strtoupper($arrPaymentValidation['countryValidation']) != 'NO' && (strtoupper($arrPaymentValidation['midValidation']) == 'NO'))
    {
      if(strpos($arrPaymentValidation['countryValidation'],','))
      {
        $arrValidation = explode(',',$arrPaymentValidation['countryValidation']);
      }else
      {
        $arrValidation1 = strtoupper($arrPaymentValidation['countryValidation']);
        $arrValidation = array('0'=>$arrValidation1);
      }
        return $arrValidation;
    }
    //case 4: if MID and Country both have validation
    else if(strtoupper($arrPaymentValidation['countryValidation']) != 'NO' && (strtoupper($arrPaymentValidation['midValidation']) != 'NO'))
    {
      if(strpos($arrPaymentValidation['countryValidation'],',') && strpos($arrPaymentValidation['midValidation'],','))
      {
        $arrCountryVal = explode(',',$arrPaymentValidation['countryValidation']);
        $arrMIDVal = explode(',',$arrPaymentValidation['midValidation']);
        
        //mearge both array
        $arrMearge = array_merge($arrCountryVal,$arrMIDVal);
        
        return array_unique($arrMearge);
      }else if(strpos($arrPaymentValidation['countryValidation'],','))
      {
        
        $arrCountryVal = explode(',',$arrPaymentValidation['countryValidation']);

        //push MID value
        array_push($arrCountryVal, strtoupper($arrPaymentValidation['midValidation']));
        return $arrCountryVal;
      }else if(strpos($arrPaymentValidation['midValidation'],','))
      {
        $arrMIDVal = explode(',',$arrPaymentValidation['midValidation']);

        //push country value
        array_push($arrMIDVal, strtoupper($arrPaymentValidation['countryValidation']));
        return $arrMIDVal;
        
      }else if(!strpos($arrPaymentValidation['countryValidation'],',') && !strpos($arrPaymentValidation['midValidation'],','))
      {
          $arrValidation = array('0'=>strtoupper($arrPaymentValidation['countryValidation']),'1'=>strtoupper($arrPaymentValidation['midValidation']));
          $arrUnique = array_unique($arrValidation);
          return $arrUnique;
      }
    }
  }

    public function getCountryPaymentOptions($firstProcessingCountry,$midService){

      //get country validation
      $countryValidation = Doctrine::getTable('CountryPaymentMode')->getCardType($firstProcessingCountry);

      if(!empty($countryValidation)){
        $countryValidation = $countryValidation;
      }else{
        $countryValidation = Doctrine::getTable('CountryPaymentMode')->getCardType('ALL');
      }
      //MID Validation
      $midValidation = Doctrine::getTable('MidMaster')->findOneBy('mid_service', strtoupper($midService));
      
      //return
      return array('countryValidation'=>$countryValidation[0]['validation'],'midValidation'=>$midValidation->getValidation());

    }
}

?>
