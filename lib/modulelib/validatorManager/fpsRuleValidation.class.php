<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * ////////////////////Fraud Prevention ////////////////////////////////////
 */

class FpsRuleValidation {

    public function chkFraudPrevention($params, $arrValidationRules) {

        if (in_array('IP', $arrValidationRules) || in_array('FPS', $arrValidationRules)) {
            
            /**
             * [WP: 070] => CR: 103
             * Excluding ip address from ip limit condition if exists...
             */
            ## This is being used to match all ip adresses which are going to exclude...
            $excluded_ip_addresses = Doctrine::getTable('IpExcludeConfiguration')->findByIpAddress($params['ip_address']);
            
            ## If exlucded IP array exists...
            if(count($excluded_ip_addresses) > 0){
                $GetAtempt = false;
            }else{
                $GetAtempt = $this->IsIpLimitExceeded($params['ip_address']);
            }
            
        } else {
            $GetAtempt = false;
        }

        //start:Kuldeep for check cart capacity
        $arrSameCardLimit = array();
        $cartValidationObj = new CartValidation();
        $arrCartCapacityLimit = $cartValidationObj->checkUserCartCapacity();
        if (!empty($arrCartCapacityLimit) && count($arrCartCapacityLimit)) {
            $arrSameCardLimit = $arrCartCapacityLimit;
        }
        //end:Kuldeep
        $maxNumReq = $arrSameCardLimit['number_of_transaction'];
        $timeInMinutes = $arrSameCardLimit['transaction_period'];
        $card_Num = md5($params['card_number']);

        $maxNumEmailReq = sfConfig::get('app_active_maxsame_email');
        $timeInEmailMinutes = sfConfig::get('app_active_maxemail_time_in_min');
        if (in_array('CCT', $arrValidationRules) || in_array('FPS', $arrValidationRules)) {
            $GetCardLimit = PaymentGatewayHelper::isSameCardlimitExcedded($card_Num, $maxNumReq, $timeInMinutes);
        } else {
            $GetCardLimit = false;
        }
        //$GetEmailLimit = PaymentGatewayHelper::isSameEmailWithCardlimitExcedded($card_Num, $maxNumEmailReq, $timeInEmailMinutes);
        $blackRuleId = 4;
        $refundAndBlockRuleId = 5;
        if (in_array('BLC', $arrValidationRules) || in_array('FPS', $arrValidationRules)) {
            $BlacklistCard = PaymentGatewayHelper::isCardBlackListed($card_Num, $blackRuleId);
        } else {
            $BlacklistCard = false;
        }

        if (in_array('BLCA', $arrValidationRules) || in_array('FPS', $arrValidationRules)) {
            $blacklistCardByAdmin = PaymentGatewayHelper::isCardBlackListed($card_Num, $refundAndBlockRuleId);
        } else {
            $blacklistCardByAdmin = false;
        }

        /**
         * [WP: 095] => CR: 136
         * If payment card is registered then BIN (CCC) rule will not be applicable anymore...
         */
        $registeredCardCount = Doctrine::getTable('ApplicantVault')->validateCardInApplicantVault($params['card_number']);
        if($registeredCardCount < 1){
            /**
             * [WP: 086] => CR:125
             * CCC means credit card country...
             */
            if (in_array('CCC', $arrValidationRules) || in_array('FPS', $arrValidationRules)) {
                $cardFirstSixDigit = substr($params['card_number'], 0, 6);
                $orderNumber = (isset($params['orderNumber']) && !empty($params['orderNumber']))?$params['orderNumber']:'';
                $cardCountryNotExist = PaymentGatewayHelper::isCreditCardCountryNotExist($cardFirstSixDigit, $orderNumber);
            } else {
                $cardCountryNotExist = false;
            }
        }else{
            $cardCountryNotExist = false;
        }
        

        $ruleId = '';
        if($cardCountryNotExist)
            $ruleId = 7;
        if ($blacklistCardByAdmin)
            $ruleId = 5;
        if ($BlacklistCard)
            $ruleId = 4;
        if ($GetCardLimit)
            $ruleId = 2;
        if ($GetAtempt)
            $ruleId = 1;

        $fpsId = $this->insertInFpsDetail($params, $ruleId);

        return array('fpsId' => $fpsId, 'ruleId' => $ruleId);
    }

    public function insertInFpsDetail($params, $ruleId) {
        $FpsDetailObj = new FpsDetail();
        $FpsDetailObj->setOrderRequestDetailId($params['requestId']);
        if (isset($params['orderNumber']) && !empty($params['orderNumber'])) {
            $FpsDetailObj->setOrderNumber($params['orderNumber']);
        }
        $FpsDetailObj->setGatewayId($params['gateway_id']);
        $FpsDetailObj->setIpAddress($params['request_ip']);
        $FpsDetailObj->setUserId(sfContext::getInstance()->getUser()->getGuardUser()->getId());
        $FpsDetailObj->setCardNum(md5($params['card_number']));
        $FpsDetailObj->setEmail($params['email']);
        $FpsDetailObj->setTransDate(date('Y-m-d H:i:s'));
        //       $FpsDetailObj->setPaymentStatus();
        $FpsDetailObj->setFpsRuleId($ruleId);
        $FpsDetailObj->save();

        return $FpsDetailObj->getId();
    }

    public function IsIpLimitExceeded($ip_address) {
        //$ip_address = $request->getRemoteAddress();
        $maxNumReq = sfConfig::get('app_active_maxip_request');
        $timeInMinutes = sfConfig::get('app_active_maxip_time_in_min');
        $GetAtempt = PaymentGatewayHelper::isMaxIPaddreslimitExcedded($ip_address, $maxNumReq, $timeInMinutes);
        return $GetAtempt;
    }
}
?>
