<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meLog
 *
 * @author sdutt
 */
class Pay4meIntLog {

  public function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
  {
    $path = $this->getLogPath($parentLogFolder);
    if($appendTime){
        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
    }else{
        $file_name = $path."/".$nameFormate.".txt";
    }
    if($append)
    {
        @file_put_contents($file_name, $xmlData, FILE_APPEND);
    }else{
    $i=1;
    while(file_exists($file_name)) {
      $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
      $i++;
     }
     @file_put_contents($file_name, $xmlData);
    }
  }


  public function getLogPath($parentLogFolder)
  {

    $logPath =$parentLogFolder==''?'/pay4melog':'/'.$parentLogFolder;
    $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;
  }

  
}
?>