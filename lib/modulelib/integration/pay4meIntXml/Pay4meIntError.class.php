<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Pay4meIntError {

  public static $PAYMENT_SUCCESS = 0;
  public static $NO_PAYMENT_ATTEMPT = 1;
  public static $ITEM_PAYMENT_ALREADY_DONE = 2;
  public static $INVALID_XML = 3;
  public static $INVALID_SERVICE = 4;
  public static $INVALID_MERCHANT = 5;
  public static $AUTHENTICATION_FAILURE = 6;
  public static $MANDATORY_PARAM_MISSING = 7;
  public static $INVALID_PARAM_INTEGER = 8;
  public static $INVALID_PARAM_STRING = 9;
  public static $UNSUPPORTED_PARAM = 10;
  public static $INVAID_ITEM_REQUEST = 11;
  public static $INVALID_SPLIT = 12;
  public static $INVALID_PAYFEE = 13;
  public static $INVALID_SHARE_MERCHANT = 14;
 
  

  public static $MSG_ERR_0 = 'Payment Successful';
  public static $MSG_ERR_1 = 'No payment attempt';
  public static $MSG_ERR_2 = 'Merchant Request – Payment has already been made for this Item';
  public static $MSG_ERR_3 = 'Merchant Request - Invalid XML Found';
  public static $MSG_ERR_4 = 'Merchant Request - Service Not Found';
  public static $MSG_ERR_5 = 'Merchant Validation Failed';
  public static $MSG_ERR_6 = 'Authentication Failure';
  public static $MSG_ERR_7 = '<parameter> mandatory field missing';
  public static $MSG_ERR_8 = 'Type mismatch. Expecting String. Found <parameter>';
  public static $MSG_ERR_9 = 'Type mismatch. Expecting Integer. Found <parameter>';
  public static $MSG_ERR_10 ='Found Unsupported parameters :  <parameter>';
  public static $MSG_ERR_11 ='No Request has been made for this item';
  public static $MSG_ERR_12 ='Split Information is not correct';
  public static $MSG_ERR_13 ='Only one pays-fee can be true';
  public static $MSG_ERR_14 ='Merchant validation failed on share merchant';




  public function error($code, array $params=NULL) {
    $logger=sfContext::getInstance()->getLogger();
    $flag = 1;
    //    $transactionObj = Doctrine::getTable('TransactionStatus')->findOneByCode($code);
    //    if($transactionObj){
    //      $description = $transactionObj->getDescription();
    //    }
    //    if(count($params)){
    $description  = $this->getFormattedDescription($code, $params);
    //    }
    if(!$flag) {
      $logger->info("Throwing the exception");
      throw new Exception($description);
    }
    if($flag) {
      $logger->info("Error found in XML. Sending Error Xml with Error Code - ".$code);

      $obj = new Pay4MeIntXMLV1();
      $errorXml = $obj->generateErrorNotification($code, $description);
      if($errorXml) {
        return $errorXml;
      //$this->createLog($errorXml, 'ErrorXml');
      //return $this->renderText($errorXml);
      }
    }
  }

  public function getFormattedDescription($code,array $params=NULL) {
    $desc_var = "MSG_ERR_".$code;
    $description = self::$$desc_var;
    if(count($params)) {
      foreach($params as $key=>$value) {
        $description = str_replace('<'.$key.'>', $value, $description);
      }
    }
    return $description;
  }


}

?>
