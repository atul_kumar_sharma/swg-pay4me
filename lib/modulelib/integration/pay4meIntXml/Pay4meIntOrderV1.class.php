<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderclass
 *
 * @author anurag
 */
class pay4meIntOrderV1{

    public $version = 'v1' ;
    public $merchant_id ;
    public $transaction_number ;
    public $name ;
    public $description ;
    public $currency ;
    public $temp_request;
    public $error_parameter;
    public $other_share = array();
    public $order_detail_id;
    public $revenue_share;
    public $revenue_pays_fee;
    public $merchant_code;
    public $isBackendPayment;

    public function __construct($xdoc,$version , $isBackendPayment = ''){
        $this->version = $version;
        $this->isBackendPayment = $isBackendPayment;
        $this->merchant_id = $xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id') ;
        $this->transaction_number = $xdoc->getElementsByTagName('item-number')->item(0)->nodeValue ;
        $this->retry_id = $xdoc->getElementsByTagName('retry-id')->item(0)->nodeValue ;
        $this->name = $xdoc->getElementsByTagName('name')->item(0)->nodeValue ;
        $this->description = $xdoc->getElementsByTagName('description')->item(0)->nodeValue ;
        $this->currency = $xdoc->getElementsByTagName('currency')->item(0)->nodeValue ;

        $this->revenue_share=$xdoc->getElementsByTagName('revenue-share')->item(0)->getAttribute('fixed-share');
        $mainMerchantArr=Doctrine::getTable('Merchant')->getMerchantDetailsById($this->merchant_id);
        $this->merchant_code=$mainMerchantArr[0]['merchant_code'];
        $pays_fee=$xdoc->getElementsByTagName('revenue-share')->item(0)->getAttribute('pays_fee');
        if('true'==$pays_fee)
             $this->revenue_pays_fee=1;
        else
             $this->revenue_pays_fee=0;

        $otherShareNodes = $xdoc->getElementsByTagName("other-share");

        $i=0;
        $otherArr = array();
        foreach($otherShareNodes  as $node) {
           $otherArr[$i]['amount']=$node->textContent ;
           $merchantCode=$node->getAttribute('merchant-code');
           $otherArr[$i]['merchant_code']= $merchantCode ;
           $merchantArr=Doctrine::getTable('Merchant')->getMerchantDetails($merchantCode);
           $otherArr[$i]['merchant_id']=$merchantArr['id'] ;
           if('true'==$node->getAttribute('pays-fee'))
                $payFee=1;
           else
                $payFee=0;
           $otherArr[$i]['pay_merchant_fee']=$payFee ;

           ++$i;
        }
        $this->other_share = $otherArr ;        
    }

    public function getDBObjectNew($pay4MeXMLOrderObj) {

        $logger=sfContext::getInstance()->getLogger();
        $merchant_id = $pay4MeXMLOrderObj->merchant_id;
        $transaction_number = $pay4MeXMLOrderObj->transaction_number;
        $order = Doctrine::getTable('OrderRequest')->chkOrder($merchant_id, $transaction_number);
        
        try{

            if(!$order) {
                $order = new OrderRequest();
                $order->setMerchantId($pay4MeXMLOrderObj->merchant_id) ;
                $order->setAmount($pay4MeXMLOrderObj->currency) ;
                $order->setTransactionNumber($pay4MeXMLOrderObj->transaction_number) ;
                $order->setVersion($this->version) ;
                 }
            else {
                $logger->info('in else');

                //chk if payment has been made for this transaction
                $order_id = $order->getId();
                $orderRequestObj = OrderRequestDetailsTable::getInstance();
                if($orderRequestObj->getPaymentStatus($order_id)) {
                  throw New Exception("ITEM_PAYMENT_ALREADY_DONE");
                }
                unset($orderRequestObj);
            }
//                $order->setDescription($this->description) ;
                $orderDetails = new OrderRequestDetails();
                $orderDetails->setName($this->name) ;
                $orderDetails->setRetryId($this->retry_id);
                $orderDetails->setMerchantId($pay4MeXMLOrderObj->merchant_id) ;
                $orderDetails->setDescription($this->description) ;
                $orderDetails->setOrderRequest($order) ;
                $orderDetails->setAmount($pay4MeXMLOrderObj->currency) ;
                $order->OrderRequestDetails[] = $orderDetails;
                $attrCount=-1;
                //////////////for revenue merchant ///////////////////
                    ++$attrCount;
                      $revenue = $orderDetails->OrderRequestSplit[$attrCount];
                      $revenue->setOrderRequestId($order);
                      $revenue->setAmount($this->revenue_share);
                      $revenue->setMerchantCode($this->merchant_code);
                      $revenue->setMerchantId($this->merchant_id);
                      $revenue->setPayMerchantFee($this->revenue_pays_fee);
                /////////////////////////////////////////////////////

                $attrCount=0;
                foreach($this->other_share as $value) {
                        ++$attrCount;
                      $OrderSplit = $orderDetails->OrderRequestSplit[$attrCount];
                      
                      $OrderSplit ->setAmount($value['amount']);

                      $OrderSplit->setOrderRequestId($order);
                      $OrderSplit->setMerchantCode($value['merchant_code']);
                      $OrderSplit->setMerchantId($value['merchant_id']);
                      $OrderSplit->setPayMerchantFee($value['pay_merchant_fee']);
                  }
                  try {
                $order->save();
                  }
                  catch (Exception $e) {
                    throw new Exception ($e->getMessage());
                  }

                $this->temp_request = $order;
                $this->order_detail_id = $orderDetails->getId();
                return  $order->getId();
           
        }catch(Exception $e){
            $logger->err($e->getMessage()) ;
            //$this->error_parameter =  ;
            throw $e;
        }
    }

    public function getRedirectObject()
    {
        $pay4meIntOrderRedirectV1Obj = new Pay4meIntOrderRedirectV1($this->temp_request,$this->order_detail_id,$this->version, $this->isBackendPayment) ;
        return $pay4meIntOrderRedirectV1Obj;
    }
}
?>
