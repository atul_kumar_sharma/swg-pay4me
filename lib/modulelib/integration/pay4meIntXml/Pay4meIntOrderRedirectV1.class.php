<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4meOrderRedirectV1
 *
 * @author anurag
 */
class Pay4meIntOrderRedirectV1 {

    public $redirectKey ;
    public $isBackendPayment ;

    public function __construct($order,$requestDetailId,$version='', $isBackendPayment='')
    {
        $transaction_num = $order->getTransactionNumber() ;
        $redirectKeyStr = $transaction_num.":".$requestDetailId.":".$version;

        $this->isBackendPayment = $isBackendPayment;
        $this->redirectKey = base64_encode($redirectKeyStr) ;
    }

    public function toXml()
    {
      //check the payment coming from backend or frontend if isbackend then will have a value
      if($this->isBackendPayment != '')
      $redirectUrl = "@order_pay_backend?order=".$this->redirectKey;
      else
      $redirectUrl = "@order_pay?order=".$this->redirectKey;
      
      $logger=sfContext::getInstance()->getLogger();
      //sfLoader::loadHelpers('Url');
      
      
    //  sfContext::getInstance()->getConfiguration()->loadHelpers(array('Helper', 'Url', 'Asset', 'Tag'));
      sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
     
//      $orderRedirectXml
       $obj = new Pay4MeIntXMLV1();
       $logger->info(url_for($redirectUrl,true));
       $orderRedirectXml = $obj->generatePay4MeIntOrderRedirectV1($this->xml_character_encode(url_for($redirectUrl,true)));
      $logger->info('test2');
//
//
//      $orderRedirectXml = "<order-redirect xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://www.pay4me.com/schema/pay4meorder'   xsi:schemaLocation='http://www.pay4me.com/schema/pay4meorder payformeorder.xsd'>" ;
//      $orderRedirectXml .= "<redirect-url>" ;
//      $orderRedirectXml .=  $this->xml_character_encode(url_for("@order_pay?order=".$this->redirectKey,true));
//      $orderRedirectXml .=  "</redirect-url>";
//      $orderRedirectXml .=  "</order-redirect>";


      $logger->info('sending redirect XML');
      $logger->log(print_r($orderRedirectXml, true)) ;
      return $orderRedirectXml;
    }

    protected function xml_character_encode($string, $trans='') {
      $trans = (is_array($trans)) ? $trans : get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
      foreach ($trans as $k=>$v)
        $trans[$k]= "&#".ord($k).";";

      return strtr($string, $trans);
    }
}
?>
