<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of validatePay4meXMLclass
 *
 * @author anurag
 */
class ValidatePay4meIntXML {
  public function validateXML($xdoc, $version) {

      //echo "<pre>"; print_r($xdoc->saveXML());

    $config_dir = sfConfig::get('sf_config_dir') ;

   $xmlschema = $config_dir . '/xmlschema/ipay4meorder'.strtoupper($version).'.xsd';
  //  $handle = @fopen($xmlschema, "r");

    try {
      if ($xdoc->schemaValidate($xmlschema)) {
        return true;
      }else {
        return false ;
      }
    }catch(Exception $ex)  {
      echo $ex->getMessage();
    }
  }

  
}
?>
