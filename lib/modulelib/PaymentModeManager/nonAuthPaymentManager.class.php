<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of nonAuthPaymentManagerclass
 *
 * @author akumar1
 */
class NonAuthPaymentManager
{

    public static function updateSplitAmount($orderRequestDetail_id,$gateway_id, $user_id) {

        $OrderSplitAllObj= Doctrine::getTable('OrderRequestSplit')->findByOrderDetailId($orderRequestDetail_id);
        // print "<pre>";
        // print_r($OrderSplitAllObj->toArray());exit;
        // $merchnat_id = $OrderSplitAllObj->getfirst()->getMerchantId();


        if($gateway_id != sfConfig::get('app_ewallet_gateway_id')) { //hardcoded to be changed to eWallet
            $transctionChargeObj= Doctrine::getTable('TransactionCharges')->findByGatewayId($gateway_id);



            if(is_object($transctionChargeObj)) {
                if("percentage" == $transctionChargeObj->getfirst()->getSplitType()) {
                    foreach($OrderSplitAllObj as $orderObj) {
                        $transCharge = round(($orderObj->getAmount() * $transctionChargeObj->getfirst()->getTransactionCharges())/100) ;
                        $itemFee = $orderObj->getAmount() - $transCharge;
                        $orderObj->setItemFee($itemFee);
                        $orderObj->setTransactionCharges($transCharge);
                        $orderObj->save();
                    }


                }
            }
        }
        else {
            $accountObj = new UserEwalletConsolidation();
            //$user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            $detailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetail_id);
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
            $payable_amount = convertToCents($detailsObj->getAmount());
            $splitArray =  $accountObj->getSplitConfiguration($payable_amount,$user_id) ;
            //                    print "<pre>";
            //                    print_R($splitArray);
            $transactionCharge = 0;
            $charge = 0;
            foreach($splitArray as $key=>$value) {
                $charge = $charge+$value['service_charge'] * $value['amount'];

            }
            $transactionCharge = $charge/$payable_amount;

            foreach($OrderSplitAllObj as $orderObj) {
                $transCharge = round(($orderObj->getAmount() * $transactionCharge)/100) ;
                $itemFee = $orderObj->getAmount() - $transCharge;
                $orderObj->setItemFee($itemFee);
                $orderObj->setTransactionCharges($transCharge);
                $orderObj->save();
            }
            $accountObj->updateEwalletConsolidationAcct($splitArray,$user_id);
            //      exit;


        }
        return $orderObj;
    }

    public static function paymentSuccess($order_number, $userId) {

        $orderDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($order_number);

        $updatePayment = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderDetailId, $userId);

        $notificationUrl = "notification/PaymentNotificationV1" ;
        $senMailUrl = "notification/paymentSuccessMail" ;
        $taskId = EpjobsContext::getInstance()->addJob('PaymentNoficationForOIS',$notificationUrl, array('order_detail_id' => $orderDetailId));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        NonAuthPaymentManager::paymentAuditPaymentTransaction($orderDetailId, $order_number);

        ## Add mail in job
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $url = url_for("report/getReceipt", true).'?receiptId='.Settings::encryptInput($order_number);
        $url1 = url_for("report/paymentHistory",true);

        $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMailForOIS',$senMailUrl, array('order_detail_id'=>$orderDetailId,'url'=>$url,'url1'=>$url1));
        sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");

        $result['status'] = 'success';
        $result['validation'] = $order_number;
        return $result;
    }

    public static function paymentAuditPaymentTransaction($orderDetailId, $cusId) {
        //Log audit Details
        $orderDetailsObj = Doctrine::getTable('OrderRequestDetails')->find($orderDetailId);
        $amount = $orderDetailsObj->getAmount();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
        $amount_formatted = format_amount($amount, 1);
        $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID, $orderDetailId, $orderDetailId));
        $eventHolder = new pay4meAuditEventHolder(
            EpAuditEvent::$CATEGORY_TRANSACTION,
            EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
            EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $cusId, 'amount' => $amount_formatted)),
            $applicationArr);

        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
    }

    public static function createOrder($request, $processingCountry) {
        $params = array();
        $params['requestId'] = $request->getParameter('requestId');             // order request detail id
        $params['cardNumber'] = '5555555555555555';
        $params['cardType'] = 'M';
        $params['billing-email'] = 'test@ois.com';
        $params['ip_address'] = '192.168.10.82';
        $params['fname'] = 'ois test';
        $params['lname'] = 'ois test';
        $params['card_holder'] = 'ois';
        $params['phone'] = '555555555555';
        $concat_str = "~";
        $params['address'] = $concat_str . $processingCountry;
        $params['payorName'] = 'ois';
        $params['card_len'] = strlen($params['cardNumber']);
        $params['card_first'] = substr($params['cardNumber'], 0, 4);
        $params['card_last'] = substr($params['cardNumber'], -4, 4);
        /* [WP: 103] => CR: 148 Inserting processing country in ipay4me_order table... */
        $params['processing_country'] = $processingCountry;
        $retArr = NonAuthPaymentManager::saveIp4mOrder($params);

        $params['gateway_id'] = $retArr['gatewayId'];
        $params['amount'] = $retArr['amount'];
        $params['orderNumber'] = $retArr['orderNumber'];
        $params['order_id'] = $retArr['order_id'];

        return $params;
    }

    public static function saveIp4mOrder($params) {
        $retArr = array();

        ## Getting payment gateway id...
        $intGatewayId = 11;

        $request_details = Doctrine::getTable('OrderRequestDetails')->find($params['requestId']);
        $amount = $request_details->getAmount();
        ## Order no
        $version = 'V1';
        $orderManagerObj = orderServiceFactory::getService($version);
        $orderNumber = $orderManagerObj->getOrderNumber();
        $saveOrderDetail = new ipay4meOrder();
        $saveOrderDetail->setOrderNumber($orderNumber);
        $saveOrderDetail->setGatewayId($intGatewayId);
        $saveOrderDetail->setOrderRequestDetailId($params['requestId']);
        /* [WP: 103] => CR: 148 Inserting processing country in ipay4me_order table... */
        $saveOrderDetail->setProcessingCountry($params['processing_country']);
        
        $saveOrderDetail->setPayorName($params['payorName']);
        $saveOrderDetail->setCardHolder($params['card_holder']);
        $saveOrderDetail->setAddress($params['address']);
        $saveOrderDetail->setPhone($params['phone']);
        $saveOrderDetail->setCardFirst($params['card_first']);
        $saveOrderDetail->setCardLast($params['card_last']);
        $saveOrderDetail->setCardLen($params['card_len']);
        $saveOrderDetail->setCardType($params['cardType']);
        $saveOrderDetail->setCardHash(md5($params['cardNumber']));
        $saveOrderDetail->save();
        $retArr['amount'] = $amount;
        $retArr['orderNumber'] = $orderNumber;
        $retArr['gatewayId'] = $intGatewayId;
        $retArr['order_id'] = $saveOrderDetail->getId();
        return $retArr;
    }



}

?>
