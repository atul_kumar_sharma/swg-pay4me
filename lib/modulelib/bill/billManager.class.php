<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class billManager {

    public function addBill($order_request_id){

        #$order_request_details_data =  Doctrine::getTable('OrderRequestDetails')->getLatestUnpaidOrderRequestDetails($order_id);
       if(Doctrine::getTable('Bill')->chkBillDuplicacy($order_request_id) == 0){
        //if($order_request_details_data != 0){
            $bill_number = $this->generateBillNumber();
            $user_id = sfContext::getInstance()->getUser()->getGuardUser()->getId();

            $ifBillExpiryActive = Settings::getBillExpiryActive();

            $billObj = new Bill();
            $billObj->setBillNumber($bill_number);
            $billObj->setUserId($user_id);
            $billObj->setOrderRequestId($order_request_id);
            if($ifBillExpiryActive){
               $expiry_date = date('Y-m-d h:i:s',mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
               $billObj->setExpiryDate($expiry_date);
            }
            $billObj->save();            
        //}
       }
       
    }

    public function generateBillNumber(){

        do {

              $bill_number = mt_rand(10000000,99999999);
              $duplicate_bill = $this->chkBillNumberDuplicacy($bill_number);
          } while ($duplicate_bill > 0);

          return $bill_number;
    }


    public function chkBillNumberDuplicacy($bill_number){
         try {
            return $bill_count = Doctrine::getTable('Bill')->chkBillNumberDuplicacy($bill_number);
        }catch(Exception $e ){
            echo("Problem found". $e->getMessage());die;
        }
    }

     public function addOISBill($order_request_id){

        #$order_request_details_data =  Doctrine::getTable('OrderRequestDetails')->getLatestUnpaidOrderRequestDetails($order_id);
       if(Doctrine::getTable('Bill')->chkBillDuplicacy($order_request_id) == 0){
        //if($order_request_details_data != 0){
            $bill_number = $this->generateBillNumber();
            $user_id = 1; //sfContext::getInstance()->getUser()->getGuardUser()->getId();

            $ifBillExpiryActive = Settings::getBillExpiryActive();

            $billObj = new Bill();
            $billObj->setBillNumber($bill_number);
            $billObj->setUserId($user_id);
            $billObj->setOrderRequestId($order_request_id);
            if($ifBillExpiryActive){
               $expiry_date = date('Y-m-d h:i:s',mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
               $billObj->setExpiryDate($expiry_date);
            }
            $billObj->save();
        //}
       }

    }
}




?>
