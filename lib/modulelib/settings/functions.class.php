<?php
/*
 * This class contains common functions...
 */

class Functions {

   /**
    * [WP: 085] => CR:124
    * @param <type> $input. convert input into encoded form...
    * @return <type>
    */

    public static function checkRequestIdTempering($requestId){

        $error = false;
        $sess_request_id = sfContext::getInstance()->getUser()->getAttribute('requestId');
        if(isset($sess_request_id)){
            if($sess_request_id != $requestId){
                $error = true;
            }
        }else{
            $error = true;
        }

        if($error){
            $errorMsg = Functions::temperingURLMessage();
            sfContext::getInstance()->getUser()->setFlash('notice', $errorMsg);
            sfContext::getInstance()->getController()->redirect('cart/list');
        }

    }


    public static function temperingURLMessage(){
        $errorObj = new ErrorMsg();
        return $errorObj->displayErrorMessage("E059", '001000');
    }

     /**
      * [WP: 086] => CR:125
      * This function return application processing country by application Id and application type...
      */
    public static function getProcessingCountryByAppIdAndType($appId='', $appType=''){

        $processingCountry = '';
        if($appId != '' && $appType != ''){
            $appType = strtolower($appType);
            if($appType == 'passport') {
                ## get Application details
                $processingCountry = Doctrine::getTable('PassportApplication')->getProcessingCountry($appId);
            }elseif ($appType == 'freezone') {
                $processingCountry = 'NG';
            }elseif ($appType == 'visa') {
                $processingCountry = Doctrine::getTable('VisaApplicantInfo')->getProcessingCountry($appId);
            }elseif ($appType == 'vap') {
                $processingCountry = Doctrine::getTable('VapApplication')->getProcessingCountry($appId);
            }
        }
        return $processingCountry;
    }


     /**
      * [WP: 087] => CR:126
      * @param <type> $appId
      * @param <type> $appType
      * @return <type> This function return application amount...
      *
      */
    public static function getApplicationFees($appId, $appType){        
        $appType = strtolower($appType);
        if ($appType == 'passport' || $appType == 'p') {
            $table = "PassportApplication";
        } elseif ($appType == 'visa' || $appType == 'freezone' || $appType == 'v' || $appType == 'f') {
            $table = "VisaApplication";
        } elseif ($appType == 'vap') {
            $table = "VapApplication";
        }        
        $app = Doctrine::getTable($table)->find($appId);        
        return $app->getDollarFee();
    }

    /**
     * [WP: 087] => CR:126
     * @param <type> $requestId
     * @return <type>
     * This function return
     */
    public static function getApplicationTypeByRequestId($requestId){

        $transactionObj = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($requestId);
        $appCount = count($transactionObj);
        if($appCount > 0){
          $appType = $transactionObj->getFirst()->getAppType();
        }else{
            $appType = '';
        }
        return strtolower($appType);

    }

    public static function getVapApplicationFeesForMO($appId, $appType){        
        $app = Doctrine::getTable('VapApplication')->find($appId);
        return $app->getMoDollarFeeForGB();
    }


    /**
     * [WP: 087] => CR: 126
     * @return <type>
     * If cart contains vap application then return true else false...
     */
    public static function isVapApplicationExistsInCart(){

        $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();        
        foreach ($arrCartItems as $items) {                                        
            if ($items->getType() == 'Vap') {
                return true;
                break;
            }
        }
        return false;
    }

    /**
     * [WP: 096]
     * @param <type> $appType
     * @param <type> $appId
     * @return <type>
     */
    public static function printApplicationURL($appType, $appId){
        $printUrl = '';
        $appType = strtolower($appType);
        switch($appType){
            case 'visa':
            case 'entryfreezone':
                ##$printUrl = url_for("visa/printVisaApp?id=".$appId);
                $printUrl = sfConfig::get('app_nis_portal_url').'visa/printForm/id/'.$appId.'/read/1/popup/true';
                break;
            case 'reentryfreezone':
                ##$printUrl = url_for("visa/printFreezone?id=".$appId);
                $printUrl = sfConfig::get('app_nis_portal_url').'visa/printReEntryForm/id/'.$appId.'/read/1/popup/true';
                break;
            case 'passport':
                ##$printUrl = url_for("passport/showPrintSuccess?id=".$appId);
                $printUrl = sfConfig::get('app_nis_portal_url').'passport/summary/id/'.$appId.'/read/1/popup/true';
                break;
            default:
                $printUrl = '';
                break;
        }
        return $printUrl;        
    }

    /**
     * [WP: 099]
     * @param <type> $xmlData
     * @param <type> $nameFormate
     * @param <type> $parentLogFolder
     * @param <type> $appendTime
     * @param <type> $append
     */
    public static function createLogData($xmlData,$nameFormate,$parentLogFolder='', $appendTime=true, $append = false)
    {
        $path = Functions::getLogPath($parentLogFolder);
        if($appendTime){
          $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
        }else{
          $file_name = $path."/".$nameFormate.".txt";
        }
        if($append)
        {
          @file_put_contents($file_name, $xmlData, FILE_APPEND);
        }else{
          $i=1;
          while(file_exists($file_name)) {
            $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
            $i++;
          }
          @file_put_contents($file_name, $xmlData);
        }
     }
     
     /**
      * [WP: 099]
      * @param <type> $parentLogFolder
      * @return <type> 
      */
     public static function getLogPath($parentLogFolder)
     {

        $logPath =$parentLogFolder==''?'/Temp/':'/'.$parentLogFolder.'/';
        $logPath =  sfConfig::get('sf_log_dir').$logPath.'/'.date('Y-m-d');

        if(is_dir($logPath)=='')
        {
          $dir_path=$logPath."/";
          mkdir($dir_path,0777,true);
          chmod($dir_path, 0777);
        }
        return $logPath;
     }


    /**
     * [WP: 102] => CR: 144
     * @param <type> $appId
     * @param <type> $appType
     * @return <type> 
     */
    public static function getVisaApplicationFeesWithoutAdditionalCharges($appId, $appType){
        $app = Doctrine::getTable('VisaApplication')->find($appId);
        ## $additional charges do not required this is why passing false...
        return $app->getDollarFee($additionalCharges = false);
    }


    /**
     * [WP: 102] => CR: 144
     * @param <type> $appId
     * @param <type> $appType
     * @param <type> $additionalCharges
     * @return <type> 
     */
    public static function getConvertedVisaAndAdditionalFees($appId, $appType, $additionalCharges){

        $appObj = Doctrine::getTable('VisaApplicantInfo')->findByApplicationId($appId);
        if(count($appObj) > 0){
            $processingCountryId = $appObj->getFirst()->getApplyingCountryId();
            $countryObj = Doctrine::getTable('Country')->find($processingCountryId);
            $from_currency = 1; //dollar
            $to_currency = 1; //dollar
            if(!empty($countryObj)){
                $to_currency = $countryObj->getCurrencyId();
            }
            if($from_currency != $to_currency){

                ## Converted Additional charges for vap...
                $additionalCharges = CurrencyManager::getConvertedAmount($additionalCharges, $from_currency, $to_currency);

                $appAmount = Functions::getVisaApplicationFeesWithoutAdditionalCharges($appId,$appType);
                $appAmount = CurrencyManager::getConvertedAmount($appAmount, $from_currency, $to_currency);
                //$amount = $appAmount + $additionalCharges ;

                return array('appAmount' => $appAmount, 'additional_charges' => $additionalCharges);

            }
        }//End of if(count($appObj) > 0){...

        return array();
        
    }

    /**
     * [WP: 102] => CR: 144
     * @param <type> $appId
     * @return <type>
     * This function return true if application is entry freezone or return false if application is not entry freezone...
     */
    public static function isVisaEntryFreezone($appId){
        $app = Doctrine::getTable('VisaApplication')->find($appId);
        if(!empty($app)){
            $visaCategoryId = $app->getVisacategoryId();
            ## 101 means Entry Freezone...
            if($visaCategoryId == 101){
                return true;
            }
        }
        return false;
    }

    /**
     * [WP: 102] => CR: 145
     * @param <type> $sdate
     * @param <type> $edate
     * @return <type>
     * Retrun weekly date range from given start date and end date...
     */
    public static function get_week_from_daterange($sdate, $edate) {

        list($sdate, $end_date) = Functions::get_week_range($sdate);
        list($start_date, $edate) = Functions::get_week_range($edate);

        $weeklyArray = array();
        while(strtotime($sdate) <= strtotime($edate)){

            $weeklyArray[] = Functions::get_week_range($sdate);

            list($sy, $sm, $sd) = explode("-", $sdate);
            list($ey, $em, $ed) = explode("-", $edate);

            $sdate = date('Y-m-d', mktime(0,0,0,$sm, $sd + 7, $sy));
        }

        return $weeklyArray;

    }

    /**
     * [WP: 102] => CR: 145
     * @param <type> $date
     * @return <type>
     * this function return start date of week and end date of week of given date...
     */
    public static function get_week_range($date) {
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        return array(date('Y-m-d', $start),
                     date('Y-m-d', strtotime('next saturday', $start)));
    }

    /**
     * [WP: 102] => CR: 144
     * @param <type> $amount
     * @param <type> $currencySymbol
     * @return <type>
     * This function return addtional text with currency symbol...
     */
    public static function getAdditionalText($amount, $currencySymbol){        
        return '<p></p><i><span style="font-size:10px;text-align:right"> ('.sfConfig::get('app_visa_additional_charges_text').': </span></i>'.html_entity_decode($currencySymbol).'<i><span style="font-size:10px;text-align:right">'.$amount.')</span></i>';
    }

    /**
     * [WP: 104] => CR: 149
     * If cart is empty then return to cart page with appropriate message...
     */
    public static function emptyCartMessage(){
        $arrCartItems = sfContext::getInstance()->getUser()->getCartItems();
        if (count($arrCartItems) == 0) {
            sfContext::getInstance()->getUser()->setFlash('notice', 'Cart is empty.');
            sfContext::getInstance()->getController()->redirect('cart/list');
        }
    }
    
    /**
     * [WP: 104] => CR: 149
     * Setting flag true for jna service to change address defined in bottom of the page...
     */
    public static function setFooterAddressFlag($countryId){       
        $countryPaymentMode = Doctrine::getTable('CountryPaymentMode')->getInfoByCountry($countryId);
        $processingService = $countryPaymentMode[0]['service'];
        $processingServiceArray = explode(',', $processingService);
        if(count($processingServiceArray) == 1 && strtolower($processingService) == 'jna1'){
            $processingCardType = $countryPaymentMode[0]['card_type'];
            $processingCardTypeArray = explode(',', $processingCardType);
            if(in_array('Mo', $processingCardTypeArray) || in_array('MO', $processingCardTypeArray) || in_array('mo', $processingCardTypeArray)){
                return false;
            }else{
                return true;
            }
        }else{
           return false;
        }//End of if(count($processingServiceArray) == 1 && strtolower($processingService) == 'jna1'){...
    }//End of public static function setFooterAddressFlag($countryId){...

    /**
     * [WP: 112] => CR: 158
     * @param <type> $array
     * @return <type>
     * This function remove white spaces from an array elements...
     */
    public static function removeWhiteSpacesInArray($array){

        $updateArray = array();
        foreach($array AS $key => $value){            
            if(is_array($value)){
                  $updateArray[$key] =  Functions::removeWhiteSpacesInArray($value);
            }else{
                $updateArray[$key] = trim($value);
            }
        }
        return $updateArray;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $orderRequestDetailId
     * @return <type> $applicationArray
     * This function return an application array from NIS...
     */
    public static function getApplicationsFromNIS($orderRequestDetailId){            

            $request_details = Doctrine::getTable('OrderRequestDetails')->find($orderRequestDetailId);
            $applicationsDetails = array();
            if(!empty($request_details)){
                $appDetails = CartItemsTransactionsTable::getInstance()->getRetryIdDetailsOnNis($request_details->getRetryId());                
                foreach ($appDetails as $value){
                   $applicationsDetails[] = $value;
                }
            }

            return $applicationsDetails;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $orderDetailId
     * Updating payment status set to 0...
     */
    public static function updateTransactionServiceChargesPaymentStatus($orderDetailId, $status = 0){
        
        $applicationDetailsFromNIS = Functions::getApplicationsFromNIS($orderDetailId);
        $transactionServiceObj = Doctrine::getTable('TransactionServiceCharges')->findByOrderRequestDetailId($orderDetailId);
        $applicationId = '';
        $applicationType = '';
        if(count($applicationDetailsFromNIS) != count($transactionServiceObj)){
            $applicationId = $applicationDetailsFromNIS[0]['id'];
            $applicationType = $applicationDetailsFromNIS[0]['app_type'];
        }
        Doctrine::getTable('TransactionServiceCharges')->updatePaymentStatus($orderDetailId, $status, $applicationId, $applicationType);
    }


    /**
     * [WP: 112] => CR: 158
     * @param <type> $applicationId
     * @param <type> $applicationType
     * @param <type> $orderNumber
     * @param <type> $status
     * This function updated application status in transaction service charges...
     */
     public static function updateApplicationPaymentStatus($applicationId, $applicationType, $orderNumber, $status = 0){
        if($applicationId != '' && $applicationType != '' && $orderNumber != ''){
            $orderRequestDetailId = Doctrine::getTable('Ipay4meOrder')->getOrderRequestDetailId($orderNumber);
            if($orderRequestDetailId != ''){
                Doctrine::getTable('TransactionServiceCharges')->updatePaymentStatus($orderRequestDetailId, $status, $applicationId, $applicationType);
            }
        }        
    }

}


?>
