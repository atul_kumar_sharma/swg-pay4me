<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Settings {

  public static function isEwalletPinActive() {
    return sfConfig::get('app_ewallet_pin_generation_flag');
  }

  public static function getMaxNoOfRetries() {
    return sfConfig::get('app_ewallet_max_no_of_retries');
  }
  public static function getEwalletPinActivatedTill() {
    return sfConfig::get('app_ewallet_pin_activated_till');
  }


  public static function getBillExpiryActive(){
    return sfConfig::get('app_bill_expiry_active');
  }


  public static function getIpay4meRechargeAmount() {
    return sfConfig::get('app_ipay4me_recharge_amount');
  }
  public static function getMaxNoOfPinRetries() {
    return sfConfig::get('app_ewallet_max_no_of_pin_retries');
  }
  public static function getEwalletGatewayId() {
    return sfConfig::get('app_ewallet_gateway_id');
  }
  public static function getActiveSmsGateway() {
    return sfConfig::get('app_active_gateway');
  }
  public static function per_smscharge_amtInCents() {
    return sfConfig::get('app_smscharge_amt_cents');
  }
  public static function getRechargeLimitFlag() {
    return sfConfig::get('app_recharge_limit_flag');
  }
  public static function getRechargeMaxAmount() {
    return sfConfig::get('app_recharge_max_amount');
  }
  public static function getRechargeNoOfDays() {
    return sfConfig::get('app_recharge_no_of_days');
  }
  public static function getRechargeGateway() {
    $gatewayarray = array();
    if(sfConfig::get('app_recharge_gateway_0')){
      $gateWay = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_recharge_gateway_0'))->toArray();
      $gatewayarray[$gateWay[0]['id']] = $gateWay[0]['display_name'];
    }
    if(sfConfig::get('app_recharge_gateway_1')){
      $gateWay = Doctrine::getTable('Gateway')->findByName(sfConfig::get('app_recharge_gateway_1'))->toArray();
      $gatewayarray[$gateWay[0]['id']] = $gateWay[0]['display_name'];
    }

    return $gatewayarray;
  }
  public static function getRechargeApproveurl() {
    return sfConfig::get('app_vbv_config_ret_url_recharge_approve');
  }
  public static function getRechargeDeclineurl() {
    return sfConfig::get('app_vbv_config_ret_url_recharge_decline');
  }
  public static function getPaymentApproveurl() {
    return sfConfig::get('app_vbv_config_ret_url_payment_approve');
  }
  public static function getPaymentDeclineurl() {
    return sfConfig::get('app_vbv_config_ret_url_payment_decline');
  }
  public static function getMerid() {
    return sfConfig::get('app_vbv_config_merchant_id');
  }
  public static function getAcqbin() {
    return sfConfig::get('app_vbv_config_acqbin');
  }
  public static function getPostUrl() {
    return sfConfig::get('app_vbv_config_posturl');
  }
  public static function getCurrency() {
    return sfConfig::get('app_vbv_config_currency');
  }
  public static function getCurrencyNaira() {
    return sfConfig::get('app_currency_naira');
  }
  public static function getCurrencyDollar() {
    return sfConfig::get('app_currency_dollar');
  }
  public static function getVbvGatewayId() {
    return sfConfig::get('app_vbv_gateway_id');
  }

  public static function getMaxBillingAddLimit() {
    return sfConfig::get('app_max_profile_address');
  }

  public static function isEwalletActive() {
    return sfConfig::get('app_is_ewallet_activated');
  }

  public static function isVbvActive() {
    return sfConfig::get('app_activate_vbv');
  }
  public static function maxcard_time_in_min() {
    return sfConfig::get('app_active_maxcard_time_in_min');
  }

  public static function maxcard_time($time='') {
      if(!empty($time)){
          $time = self::calculateTime($time);
      }

    return $time;
  }
  public static function maxip_time() {
    $time = self::calculateTime(sfConfig::get('app_active_maxip_time_in_min'));
    return $time;
  }
  public static function maxemail_time() {
    $time = self::calculateTime(sfConfig::get('app_active_maxemail_time_in_min'));
    return $time;
  }

  public static function isNmiActive() {
    return sfConfig::get('app_nmi_mid1_is_active');
  }

  public static function calculateTime($timeInMin)
  {
    $time = "";
    if($timeInMin >= 43200) //months
    {
      $month = round($timeInMin / 43200);
      $time .= ($month > 1) ? $month." Months." : "a Month.";
    }
    else if($timeInMin >= 1440){    //days
      $days = round($timeInMin / 1440);
      $time .= ($days > 1) ? $days." Days." : "a Day.";
    }
    else if($timeInMin >= 60){   //hours
      $hours = round($timeInMin / 60);
      $time .= ($hours > 1) ? $hours." Hours." : "an Hour.";
    }
    else{   //minutes
      $time .= ($timeInMin > 1) ? round($timeInMin)." Minutes." : "a Minute.";
    }
    return $time;
  }

  /**
   *
   * @return <type>
   * This function return site url (http path)
   */
  public static function getHTTPpath(){
    $host = sfContext::getInstance()->getRequest()->getUriPrefix();
    $url_root = sfContext::getInstance()->getRequest()->getPathInfoPrefix();
    $sitePath = $host . $url_root;
    return $sitePath;
  }

  /**
     * [WP: 069] => CR:102
     * @param <type> $username
     * @return <type>
     * return login type from where user came to login...
     */
  public static function getLoginType($username){
       $loginType = '';
       $urlArray = parse_url($username);
       if(isset($urlArray['host'])){
            $host = $urlArray['host'];
            if(!empty($host) && $host != ''){
                if($host == 'www.google.com'){
                    $loginType = 'Google';
                } else if($host == 'me.yahoo.com'){
                    $loginType = 'Yahoo';
                } else if($host == 'facebook.com'){
                    $loginType = 'Facebook';
                } else {
                    $loginType = 'OpenId';
                }
            }
        }else{
            $loginType = $username;
        }
        return $loginType;
   }

   public static function getNisUrl(){
       return sfConfig::get('app_nis_portal_url');
   }

   /**
    * [WP: 085] => CR:124
    * @param <type> $input. convert input into encoded form...
    * @return <type> 
    */
   public static function encryptInput($input){

          $input = SecureQueryString::ENCRYPT_DECRYPT($input);
          $finalInput = SecureQueryString::ENCODE($input);
         return $finalInput;
    }
    
   /**
    * [WP: 085] => CR:124
    * @param <type> $input. convert encoded input into normal form...
    * @return <type> 
    */
   public static function decryptInput($input){
       
       $input = SecureQueryString::DECODE($input);
       $finalInput = SecureQueryString::ENCRYPT_DECRYPT($input);
      return $finalInput;
      
    }


}



?>
