<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pay4meIntManagerclass
 *
 * @author sdutt
 */
class VbVManager {

  public function updatePayment($orderDetailId, $userId)
  {
    $updatePayment = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderDetailId, $userId);

    $notificationUrl = "notification/vbvPaymentNotification" ;
    $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('order_detail_id' => $orderDetailId));
    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    $this->auditPaymentTransaction($orderDetailId);

    return $updatePayment;
  }

  public function  auditPaymentTransaction($orderDetailId) {
    //Log audit Details
    $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$orderDetailId,$orderDetailId));
    $eventHolder = new pay4meAuditEventHolder(
      EpAuditEvent::$CATEGORY_TRANSACTION,
      EpAuditEvent::$SUBCATEGORY_TRANSACTION_PAYMENT,
      EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_PAYMENT, array('reqOrderNumber' => $orderDetailId)),
      $applicationArr);

    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
  }

  public function paymentVerify($orderId)
  {
    $paymentVerify = new PaymentVerify();
    $response = $paymentVerify->paymentVerifyOnBackend($orderId);
    return $response;
  }

  public function paymentByCron($numRecord)
  {
    $dbArr = DBInfo::getDSNInfo();
    mysql_connect($dbArr['host'],$dbArr['username'],$dbArr['password']) or die(mysql_error());
    mysql_select_db($dbArr['dbname']) or die(mysql_error());

    $sql = " SELECT ipay4me_order.id, ipay4me_order.order_number,ipay4me_order.gateway_id, ipay4me_order.created_by,order_request_details.amount,
              ipay4me_order.order_request_detail_id, ep_vbv_verify.order_id
              FROM ipay4me_order left join ep_vbv_verify on ipay4me_order.order_number = ep_vbv_verify.order_id
              left join order_request_details on (order_request_details.id = ipay4me_order.order_request_detail_id)
              where ep_vbv_verify.order_id is null and gateway_id = 4 and ipay4me_order.payment_status = 1 limit  $numRecord";

    $result = mysql_query($sql);

    if (mysql_num_rows($result) == 0) {
      return false ;
    }else{
      while ($row = mysql_fetch_assoc($result)) {
        $order_number  = $row['order_number'];
        $amount  = $row['amount'];
        $userId  = $row['created_by'];
        $orderReqDetailId  = $row['order_request_detail_id'];
        $this->processPayment($order_number, $orderReqDetailId, $userId, $amount);
      }
    }

  }

  public function processPayment($order_number, $orderReqDetailId, $userId, $amount)
  {
    $merchantId = Settings::getMerid();
    $paymentVerify = new PaymentVerify();
    $response = $paymentVerify->paymentVerifyOnBackend($order_number, $merchantId, $amount);
    if($response)
    {
      $this->updatePaymentForCron($order_number, $orderReqDetailId, $userId);
      return true;
    }
    return false;
  }

  private function updatePaymentForCron($orderNumber, $orderReqDetailId, $userId)
  {
    $updateIp4mOrder = Doctrine::getTable('ipay4meOrder')->updateIP4MOrderStatus($orderNumber);
    //update payment_status in order request detail table
    $updateOrderReqDetail = Doctrine::getTable('OrderRequestDetails')->updatePayment($orderReqDetailId,$userId);
    $gatewayId = 4;
    $payManagerObj = new PaymentManager();
    $updateOrder = $payManagerObj->updateSplitAmount($orderReqDetailId,$gatewayId);
    $notificationUrl = "notification/PaymentNotificationV1" ;
    $senMailUrl = "notification/paymentSuccessMail" ;
    $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('order_detail_id' => $orderReqDetailId));
    sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
    $this->auditPaymentTransaction($orderReqDetailId, $orderNumber);
    //add mail in job
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
    $url = url_for("report/getReceipt", true).'?receiptId='.$orderNumber;
    $url1 = url_for("report/paymentHistory",true);

    $mailTaskId = EpjobsContext::getInstance()->addJob('PaymentSuccessMail',$senMailUrl, array('order_detail_id'=>$orderReqDetailId,'url'=>$url,'url1'=>$url1));
    sfContext::getInstance()->getLogger()->debug("sceduled payment successful mail job with id: $mailTaskId");

  }

  public function StatusOfrequestId($requestId, $userId)
  {
    $status = '';
    $getAllOrderIds = ipay4meOrderTable::getInstance()->getAllOrderIds($requestId);
    if(count($getAllOrderIds))
    {
      for($i = 0; $i < count($getAllOrderIds); $i++)
      {
        $orderId = $getAllOrderIds[$i]['order_number'];
        $paymentVerify = new PaymentVerify();
        $response = $paymentVerify->paymentVerifyOnBackend($orderId);
        if($response)
        {
          $status = $orderId;
          $this->updatePaymentForCron($orderId, $requestId, $userId);
          break;
        }
      }
    }
    
    return $status;
  }
}
?>
