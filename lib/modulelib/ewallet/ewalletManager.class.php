<?php

class EwalletManager {

    public function getServiceCharge($gateway_id, $type, $amount){
        $serviceChargeSettings = Doctrine::getTable('ServiceCharge')->getServiceCharge($gateway_id, $type);
        if($serviceChargeSettings['charge_amount_type'] == 'flat'){
            return $serviceChargeSettings['charge_amount'] ;
        }elseif($serviceChargeSettings['charge_amount_type'] == 'percent'){
            $tmpServiceCharge = ($serviceChargeSettings['charge_amount']/100) * $amount ;
            if($tmpServiceCharge < $serviceChargeSettings['lower_limit'] ){
                return $serviceChargeSettings['lower_limit'] ;
            }elseif($tmpServiceCharge > $serviceChargeSettings['upper_limit'] ){
                return $serviceChargeSettings['upper_limit'] ;
            }else{
                return $tmpServiceCharge ;
            }
         }        
    }
    public function getRechargeLimit($userId){
        $limitRecharge = Doctrine::getTable('UserRechargeConfig')->findByUserId($userId);
        
        $rechargeArray = array();
        if(count($limitRecharge)){
            $rechargeArray['amount'] = $limitRecharge->getFirst()->getAmount();
            $rechargeArray['days'] = $limitRecharge->getFirst()->getDays();           
        }else{
            $rechargeArray['amount'] = Settings::getRechargeMaxAmount();
            $rechargeArray['days'] = Settings::getRechargeNoOfDays();
        }
        return $rechargeArray;
    }
}
?>
