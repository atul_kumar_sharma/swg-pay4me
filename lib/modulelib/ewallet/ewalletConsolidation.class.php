<?php

class UserEwalletConsolidation {

  public function getSplitConfiguration($payable_amount, $user_id) {

    $payorAccountObj = Doctrine::getTable('EwalletConsolidation')->getPayorAcct($payable_amount,$user_id);
    $split_collection = array();
    if($payorAccountObj) {
      $i=0;
      foreach($payorAccountObj as $accountObj) {
        $account_balance = $accountObj->getWalletAmount();
        $split_collection[$i]['amount'] = $payable_amount;
        $split_collection[$i]['gateway_id'] = $accountObj->getGatewayId();
        $split_collection[$i]['service_charge'] = $accountObj->getServiceCharge();
        $i++;
      }
    }
    else {
      $acctObj = Doctrine::getTable('EwalletConsolidation')->getCollectionAccounts($user_id);
      $bal = $payable_amount;
      $i=0;
      foreach($acctObj as $accountObj) {
        $account_balance = $accountObj->getWalletAmount();
        if($bal>0) {
          if(($bal >= $account_balance)) {
            $split_collection[$i]['amount'] = $account_balance;
            $split_collection[$i]['gateway_id'] = $accountObj->getGatewayId();
            $split_collection[$i]['service_charge'] = $accountObj->getServiceCharge();
          }
          else {
            $split_collection[$i]['amount'] = $bal;
            $split_collection[$i]['gateway_id'] = $accountObj->getGatewayId();
            $split_collection[$i]['service_charge'] = $accountObj->getServiceCharge();
          }
          $bal = $bal - $account_balance;
        }


        $i++;

      }
    }
    return $split_collection;

  }

  public function updateEwalletConsolidationAcct($split_array,$user_id) {
    foreach($split_array as $payment_array) {
      $amount = $payment_array['amount'];
      $gateway_id = $payment_array['gateway_id'];
      $service_charge = $payment_array['service_charge'];
      ////update the ewallet user consolidation acct
      $walletObj = Doctrine::getTable('EwalletConsolidation')->updatePayment($service_charge, $gateway_id, $amount, $user_id);
    }
  }

  public function updateEwalletAccount($gatewayId, $userId, $actualAmount,$ewalletAmount, $entry='credit') {
    sfContext::getInstance()->getConfiguration()->loadHelpers(array('EPortal'));
    $actualAmount = convertToCents($actualAmount);
    $ewalletAmount = convertToCents($ewalletAmount);
    $userObj = Doctrine::getTable('UserDetail')->findByMasterAccountId($userId);
    $gatewayObj = Doctrine::getTable('TransactionCharges')->findByGatewayId($gatewayId)->getFirst();
    $serviceCharge = $gatewayObj->getTransactionCharges();

    $recordObj = Doctrine::getTable('EwalletConsolidation')->fetchDetails($userId, $gatewayId,$serviceCharge);
    if(!$recordObj) {
      $walletObj = new EwalletConsolidation();
      $walletObj->setGatewayId($gatewayId);
      $walletObj->setUserId($userId);
      $walletObj->setServiceCharge($serviceCharge);
    }
    else {
      $id = $recordObj->getFirst()->getId();
      $walletObj = Doctrine::getTable('EwalletConsolidation')->find(array($id));
      $current_actual_amount = $recordObj->getFirst()->getActualAmount();
      $current_wallet_amount = $recordObj->getFirst()->getWalletAmount();

      if($entry == 'credit') {
        $actualAmount = $current_actual_amount+$actualAmount;
        $ewalletAmount = $current_wallet_amount+$ewalletAmount;
      }
      else if($entry == 'debit') {
          $actualAmount = $current_actual_amount-$actualAmount;
          $ewalletAmount = $current_wallet_amount-$ewalletAmount;
        }
    }
    $walletObj->setActualAmount($actualAmount);
    $walletObj->setWalletAmount($ewalletAmount);
    $walletObj->save();
  }
}
?>
