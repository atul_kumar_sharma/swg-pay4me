<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class cartManager {

  public function addToCart($type, $id) {
    if ($type == 'p') {
      $table = "PassportApplication";
    } elseif ($type == 'v') {
      $table = "VisaApplication";
    } elseif ($type == 'vap') {
      $table = "VapApplication";
    } else {
      return;
    }

    $user = sfContext::getInstance()->getUser();

    $app = Doctrine::getTable($table)->find($id);
    $item = $app->getCartItem();
//    echo "<pre>";print_r($item);die;
    $added = $user->addCartItem($item);
    
    if($added == "duplicate") {
      return $added;
    }
    
    if($added == "application_edit") {
      return $added;
    }
    if($added == "only_vap_allowed") {
      return $added;
    }
    if($added == "capacity_full") {
      return $added;
    }
    if($added == "amount_capacity_full") {
      return $added;
    }
    if($added == "common_opt_not_available"){
      return $added;
    }
    if($added == "application_rejected"){
      return $added;
    }
    if($added == "gratis"){
      return $added;
    }
  }

  public function emptyCart() {
    $userObj = sfContext::getInstance()->getUser();
    $userObj->clearCart();
  }

  public function removeCart($id, $type) {
    $userObj = sfContext::getInstance()->getUser();
    $userObj->removeCartItemById($id,$type);
  }

  public function getCartCurrValue(){
       $userObj = sfContext::getInstance()->getUser();
       return $userObj->getCartValue() ;
  }
}

?>
