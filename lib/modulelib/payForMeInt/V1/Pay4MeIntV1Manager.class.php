<?php
class Pay4MeIntV1Manager implements pay4MeIntService {
    public $error_parameter;
    public function generatePaymentNotificationXMLV1($orderDetailId)
    {
      $Pay4MeIntXMLV1 = new Pay4MeIntXMLV1();
     $xmlData = $Pay4MeIntXMLV1->generatePaymentNotificationXMLV1($orderDetailId);
     return $xmlData;
    }

    public function getNotificationTransactionDetails($request_order_number)
    {
        $returnRecordSet =  Doctrine::getTable('Response')->getOrderDetails($request_order_number);
        return $returnRecordSet;
    }

    public function getNotificationUrl($request_order_number){
        $returnRecordArr =  Doctrine::getTable('Request')->getNotificationUrlbyOrderNo($request_order_number);
        return $returnRecordArr['notification_url'];
        
    }
    public function processOrder($request, $isBackendPayment=''){
      

      //print_r($request->);
        //order processing logic
        sfContext::getInstance()->getResponse()->setContentType('text/xml') ;
        $xmldata = file_get_contents('php://input'); //$request->getRawBody() ;
       
        $xdoc = new DomDocument('1.0');
        $xdoc->LoadXML($xmldata);

        //log creation
        Pay4MeIntUtility::setLog($xmldata,'NewOrderItem');
        
        
        try {
            
            $merchant_param_name = sfConfig::get('app_merchant_url_param_name') ;
            if($request->getParameter($merchant_param_name))
                $merchant_code = $request->getParameter($merchant_param_name) ;
            else
                throw New Exception("AUTHENTICATION_FAILURE");

            $xmlValidatorObj = new ValidatePay4meIntXML() ;
            $isValid = $xmlValidatorObj->validateXML($xdoc, 'V1') ;
            if(!$isValid) // is a Valid P4M XML //isValidMerchantServiceId
            throw New Exception("INVALID_XML");

            $isValidMrchnt = $this->isValidOtherMerchant($xdoc) ;
            if(!$isValidMrchnt)
                throw New Exception("INVALID_SHARE_MERCHANT");

            $Pay4meIntXMLOrderObj =  new pay4meIntOrderV1($xdoc, 'V1', $isBackendPayment);
            
            $merchantTableObj = MerchantTable::getInstance();
            if(!$merchantTableObj->isValidMerchant($merchant_code))
            throw New Exception("INVALID_MERCHANT");
            unset($merchantTableObj);
           
            $isMerchantAuthenticate = $this->authenticateMerchantRequest($request) ;
            if(!$isMerchantAuthenticate)
            throw New Exception("AUTHENTICATION_FAILURE");

            $isValidSplit = $this->isValidSplit($xdoc) ;
            if(!$isValidSplit)
                throw New Exception("INVALID_SPLIT");

            $isValidPayFee = $this->isValidPayFee($xdoc) ;
            if(!$isValidPayFee)
                throw New Exception("INVALID_PAYFEE");

 

           
            $created = $Pay4meIntXMLOrderObj->getDBObjectNew($Pay4meIntXMLOrderObj) ; // try - catch
           if(!ctype_digit($created)){ //checks if the merchant_request_id is returned
                throw New Exception($created);
            }
            ##$this->logMessage("logging")  ;
            $redirectXML = $Pay4meIntXMLOrderObj->getRedirectObject() ;

            $xml = $redirectXML->toXml() ;
            //log creation
            Pay4MeIntUtility::setLog($xml,'RedirectXml');

            $Pay4meIntXMLV1Obj = new Pay4meIntXMLV1() ;
            $Pay4meIntXMLV1Obj->setResponseHeader($xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id'));
            return $xml;
        } catch (Exception $ex) {
            unset($merchantTableObj);
            if(isset($Pay4meIntXMLOrderObj))
                $this->error_parameter = $Pay4meIntXMLOrderObj->error_parameter;
            $msg = $ex->getMessage();
            return $returnVal = $this->error($msg,$xdoc,$xmldata,$merchant_code);
        }
    }

     public function isValidOtherMerchant($xdoc) {
         
        $otherShareNodes = $xdoc->getElementsByTagName("other-share");

        $merchantTableObj = MerchantTable::getInstance();
        foreach($otherShareNodes  as $node) {
           $merchant_code=$node->getAttribute('merchant-code') ;
            if(!$merchantTableObj->isValidMerchant($merchant_code))
              return false;
        }
          return true;
      
    }
     public function isValidPayFee($xdoc) {

        $flag=0;
        $fixedshare = $xdoc->getElementsByTagName("revenue-share")->item(0)->getAttribute('pays-fee');
        if('true'==$fixedshare)
            ++$flag;

        $otherShareNodes = $xdoc->getElementsByTagName("other-share");
        $otherShare =0;
        foreach($otherShareNodes  as $node) {

           $payFee=$node->getAttribute('pays-fee') ;
           if('true'==$payFee)
              ++$flag; 
           if(2==$flag)
             break;
        }
        
       if($flag ==1 )
          return true;
        else
          return false;

    }

    public function isValidSplit($xdoc) {
        $fixedshare = $xdoc->getElementsByTagName("revenue-share")->item(0)->getAttribute('fixed-share');
        
        $otherShareNodes = $xdoc->getElementsByTagName("other-share");
        $otherShare =0;


        foreach($otherShareNodes  as $node) {
           $otherShare = $otherShare + $node->textContent ;
           
        }
        $allSplit = $fixedshare + $otherShare ;
        
        $TotShare=($xdoc->getElementsByTagName("currency")->item(0)->nodeValue) *100;
        if($TotShare == $allSplit)
          return true;
        else
          return false;
        
    }
    public function error($error_msg,$xdoc,$xmlData="",$merchant_code="") {

        if($error_msg != "INVALID_XML" && $error_msg != "INVALID_MERCHANT" && $error_msg != "AUTHENTICATION_FAILURE") {
            $merchant_id = $xdoc->getElementsByTagName('merchant')->item(0)->getAttribute('id');
        }else {
            $merchant_id = "";
        }
        $vals = array();

        if(property_exists('pay4meIntError', $error_msg)) {
            $error_code = Pay4meIntError::$$error_msg;
            $errObj = new Pay4meIntError();
            $errorXml = $errObj->error($error_code,$this->error_parameter);
        }
        else {
            $error_code = 400;
            $obj = new Pay4MeIntXMLV1();
            $errorXml = $obj->generateErrorNotification($error_code, $error_msg);
        }

        if($errorXml) {
            Pay4MeIntUtility::setLog($errorXml, 'ErrorXml');
            if($merchant_id) {
                $Pay4meIntXMLV1Obj = new Pay4meIntXMLV1() ;
                $Pay4meIntXMLV1Obj->setResponseHeader($merchant_id);
            }
            return $errorXml;
        }
    }


    public function authenticateMerchantRequest($request)
    {
        $merchant_code = $request->getHttpHeader('AUTH_USER','PHP') ;
        $merchant_key = $request->getHttpHeader('AUTH_PW','PHP') ;
        $merchant_param_name = sfConfig::get('app_merchant_url_param_name') ;
        $request_merchant_id = $request->getParameter($merchant_param_name) ;
    

        if($request_merchant_id != $merchant_code) {
            return false;
        }

        ##$logger = sfContext::getInstance()->getLogger();
        ##$logger->debug("OBJ: $merchant_key ".print_r($merchant_code,true));
        $merchantTableObj = MerchantTable::getInstance();
        $merchant_details =  $merchantTableObj->getMerchantDetails($request_merchant_id) ;
        unset($merchantTableObj);
        ##$logger->debug("OBJ: ".print_r($merchant_details,true));
        if(($merchant_details['merchant_code'] == $merchant_code) && ($merchant_details['merchant_key'] == $merchant_key)){
            return true ;
        }

        return false ;
    }

    public function saveOrderRequest($request_order_id,$card_num,$month,$year,$card_type,$cvv_number) {
      try {
        $orderRequestObj = Doctrine::getTable('OrderRequestDetails')->find($request_order_id);
        $order_request_id = $orderRequestObj->getOrderRequestId();
        if($orderRequestObj) {
         $paymentStatus = Doctrine::getTable('OrderRequestDetails')->getPaymentStatus($order_request_id);
        if($paymentStatus==0) {
          //chk if the Order Request has already been placed and the card has been authorized i.e. AUTH_ONLY
        $requestDuplicacy = Doctrine::getTable('Request')->chkResponse($request_order_id);
         if(!$requestDuplicacy) {
       //   $paymentStatus = 1;
          
            $con = Doctrine_Manager::connection();
            try {
              $con->beginTransaction();
              $userObj = sfContext::getInstance()->getUser();
              if($userObj->isAuthenticated()) {
                $card_num = str_replace("-", "", $card_num);
                $card_num = str_replace(" ", "", $card_num);
                $card_array = str_split( $card_num, 4 );

                $amount = $orderRequestObj->getAmount() ;
                $order_number = mt_rand(1000, 99999) ; //to be set

                $requestObj = new Request();
                $requestObj->setOrderDetailId($request_order_id);
                $requestObj->setRequestOrderNumber($order_number);
                $requestObj->setAmount($amount);
                $requestObj->setCardDetailsFirst($card_array[0]);
                $requestObj->setCardDetailsLast($card_array[3]);
              //  $requestObj->setCvv($cvv_number);
              //  $requestObj->setExpMonth($month);
              //  $requestObj->setExpYear($year);
                //$requestObj->setCardType($card_type);
                $requestObj->save();
                //$requestObj->save();
                $con->commit();

                $gateway = "authorize" ;
                $expiry_date = $month.$year ;
                $params['x_invoice_num'] = $order_number ;
                $params['x_description'] = "NIS - NIS Passport" ;
                $params['x_card_num'] = $card_num;
                $params['x_exp_date']=$expiry_date;
                $params['x_card_code']=$cvv_number;
                $params['x_amount']=$amount;
                $paymentGatewayObj = PaymentGatewayServiceFactory::getService($gateway) ;
                $paymentResponse = $paymentGatewayObj->paymentRequest('AUTH_ONLY', $params ) ;
                $paymentRes = Doctrine::getTable('Response')->saveResponse($requestObj->getId(),$paymentResponse);
                if(($paymentResponse['response_code'] == 1) && ($paymentResponse['response_reason']==1)) {
                  $notificationUrl = "notification/paymentNotification" ;
                  $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('request_order_number' => $order_number));
                  sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
                  $this->auditPaymentTransaction($order_number);
                  return $order_number ;

                }else {
                  throw New Exception($paymentResponse['response_reason_text']);
                }


              }
              else {
                throw New Exception("Session Expired");
              }
            } catch (Exception $e ) {
              $con->rollback();
              throw New Exception();
            }          
        }
        else {
          throw New Exception ("Please wait...While we process your transaction");
        }
       }else {
            throw New Exception ("The payment has already been made against this order »");
          }
        }
        else {
          throw New Exception ("No Order Request found!!");
        }
      }catch (Exception $e ) {
        throw New Exception($e->getMessage());
      }

    }





    public function makePayment($request_id) {

      try {

      // Generate Validation number and test it to be unique mt_rand(10000000, 9999999999) ;
        $notificationUrl = "notification/paymentNotification" ;
        $taskId = EpjobsContext::getInstance()->addJob('PaymentNofication',$notificationUrl, array('p4m_transaction_id' => $txnId));
        sfContext::getInstance()->getLogger()->debug("sceduled job with id: $taskId");
        //Log audit Details
        //            $this->auditPaymentTransaction($txnId);
        return $validationNo;

      }
      catch (Exception $e) {

        throw new Exception($e->getMessage());
      }

    }
    
 public function  auditPaymentTransaction($order_number) {
    //Log audit Details
    $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_P4MTXNID,$order_number,$order_number));
    $eventHolder = new pay4meAuditEventHolder(
      EpAuditEvent::$CATEGORY_TRANSACTION,
      EpAuditEvent::$SUBCATEGORY_TRANSACTION_AUTHORISE,
      EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TRANSACTION_AUTHORISE, array('reqOrderNumber' => $order_number)),
      $applicationArr);

    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
  }
}
?>
