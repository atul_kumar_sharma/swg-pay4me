<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


class rawDocPager extends sfPager
{
  protected $resultsetArray = array();

  public function __construct($class = null, $maxPerPage = 10)
  {
    parent::__construct($class, $maxPerPage);
  }

  public function setStatement($statement)
  {
    $this->statement = $statement;
  }

  public function init()
  {
   $res = $q = Doctrine_Manager::getInstance()->getCurrentConnection()->execute($this->statement);

    $resultArr = $res->fetchAll();
    //$this->statement->execute();

    $this->setNbResults(count($resultArr));

    if (($this->getPage() == 0 || $this->getMaxPerPage() == 0))
    {
     $this->setLastPage(0);
    }
    else
    {
     $this->setLastPage(ceil($this->getNbResults() / $this->getMaxPerPage()));
    }
  }

  public function getResults()
  {

    if (($this->getPage() == 0 || $this->getMaxPerPage() == 0))
    {
      $this->setLastPage(0);
    }
    else
    {
      $this->setLastPage(ceil($this->getNbResults() / $this->getMaxPerPage()));
    }

    $row_num = 1;

    $res = $q = Doctrine_Manager::getInstance()->getCurrentConnection()->execute($this->statement);
    $resultArr = $res->fetchAll();

     foreach ($resultArr as $resultset)
//    while ($resultset = $this->statement->fetch(PDO::FETCH_OBJ))
    {
      if ($row_num > $this->getMaxPerPage()*($this->getPage()-1)
      and $row_num <= ($this->getPage()*$this->getMaxPerPage() ))
      {
        $this->resultsetArray[] = $resultset;
      }
      $row_num++;
    }

    return $this->resultsetArray;
  }

  public function retrieveObject($offset)
  {
    return $this->resultsetArray[$offset];

  }
}

?>