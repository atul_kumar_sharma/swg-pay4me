<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function generateCrumb($module_name, $action_name)
{    
    $pattern = '/([A-Z]+)/';
    $replacement = ' ${1}';

    $module_name = str_replace('_', ' ', $module_name);

    $action_name = str_replace('_form', 'New', $action_name);

    return ucfirst(preg_replace($pattern, $replacement, $module_name)).' &raquo; '.ucfirst(preg_replace($pattern, $replacement, $action_name));
}
?>