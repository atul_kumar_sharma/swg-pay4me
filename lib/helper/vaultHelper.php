<?php

class VaultHelper{

 public function is_processed_with_vault($cardtype, $amount) {

    //Do not change pattern array key
    $cardPattern = array('A'=>'1__', 'M'=>'_1_', 'V'=>'__1');

    //change according to clubbed
    $clubbedCard = array();  //array('M'=>'?11', 'V'=>'?11');
    
    if(isset($clubbedCard[$cardtype] ))
        $cardPattern = $clubbedCard[$cardtype];
    else
        $cardPattern = $cardPattern[$cardtype];

    // get CST DATE
            $estTime =  date('Y-m-d H:i:s',mktime(date('H')-6 ,date('i') , date('s'), date('m'), date('d'), date('Y')));
//            if( (date('H')-6) > 18 )
//            $estDate =  date('Y-m-d',mktime(date('H'),date('i') , date('s'), date('m'), date('d') + 1, date('Y')));
            $estDate =  date('Y-m-d');

    /// get record of this date with respect to card pattern
    $getVaultConfig = Doctrine::getTable('VaultConfig')->getResultByCard($cardPattern,$estDate);
    if(!$getVaultConfig)
     {
            //  if not found get last record
            $LastVaultConfig = Doctrine::getTable('VaultConfig')->getResultByCard($cardPattern);
            if($LastVaultConfig)
            {
                //insert the all last  record with current date
                $vaultConfigObj = new VaultConfig();
                $vaultConfigObj->setIsVaultActive('no');
                $vaultConfigObj->setTransDate($estDate);
                $vaultConfigObj->setAmount($LastVaultConfig['amount']);
                $vaultConfigObj->setCardType($LastVaultConfig['card_type']);
                $vaultConfigObj->save();
                
                return $this->is_processed_with_vault($cardtype, $amount);
            }
            else
               return false;   

     }
     else
     {  
         //checking is vault active for this card if active return  true
          $hour = date('H')-6;
         if('yes' == $getVaultConfig['is_vault_active'])// && $hour <18 )
         {
             return true;
         }
         else
         {
             // get NGN time
             $NgnToTime  = date('Y-m-d H:i:s',mktime(gmdate('H') +1 ,gmdate('i'), gmdate('s'), gmdate('m'), gmdate('d'), gmdate('Y')));
             if($hour<18)
             {
                //$NgnFromTime = date('Y-m-d',mktime(gmdate('H') -1 ,gmdate('i'), gmdate('s'), gmdate('m'), gmdate('d')-1, gmdate('Y')))." 00:00:00";
                $NgnFromTime = date('Y-m-d')." 00:00:00";
                
             }
             else
             {
                $NgnFromTime = date('Y-m-d',mktime(gmdate('H') +1 ,gmdate('i'), gmdate('s'), gmdate('m'), gmdate('d'), gmdate('Y')))." 00:00:00";
                
             }

             $NgnToTime = date('Y-m-d H:i:s');
             $NgnFromTime = date('Y-m-d')." 00:00:00";

             // get successful paid amount with out vault
             $OrdDetails = Doctrine::getTable('OrderRequestDetails')->getTotAmountBetDate($NgnFromTime , $NgnToTime );
             $res = $q = Doctrine_Manager::getInstance()->getCurrentConnection()->execute($OrdDetails );

             $resultArr = $res->fetchAll();
          
            $totalPaidAmount = $resultArr[0]['TotAmount'];
//                 echo ($totalPaidAmount +  $amount);die;
             //  check thershold amount exceeding the limit
             if( $getVaultConfig['amount']  < ($totalPaidAmount +  $amount))
             {
                //  activate  vault
                $vaultConfigObj = Doctrine::getTable('VaultConfig')->find($getVaultConfig['id']);
                $vaultConfigObj->setIsVaultActive('yes');
                $vaultConfigObj->save();
                return true;
             }
             else
                return false;
         }
    
            
            

     }
   }

   public function isOverLimit( $amount) {

             $getThresholdAmt =  Settings::getThresholdAmt();
             $NgnToTime = date('Y-m-d H:i:s');
             $NgnFromTime = date('Y-m-d')." 00:00:00";

             // get successful paid amount with out vault
             $OrdDetails = Doctrine::getTable('OrderRequestDetails')->getTotAmountBetDate($NgnFromTime , $NgnToTime );
             $res = $q = Doctrine_Manager::getInstance()->getCurrentConnection()->execute($OrdDetails );

             $resultArr = $res->fetchAll();

            $totalPaidAmount = $resultArr[0]['TotAmount'];
//                 echo ($totalPaidAmount +  $amount);die;
             //  check thershold amount exceeding the limit
             if( $getThresholdAmt  < ($totalPaidAmount +  $amount))
             {
                //  activate  vault
                $vaultConfigObj = Doctrine::getTable('VaultConfig')->find($getVaultConfig['id']);
                $vaultConfigObj->setIsVaultActive('yes');
                $vaultConfigObj->save();
                return true;
             }
             else
                return false;
     
   }

   /*return Date Time
    * if current time is less than today's 2:00:00 am then it will return yesterday's 2:00:00 am else
    * it will return today's 2:00:00 am 
    */
   function getDateTime(){

        $calculateTime  = date("Y-m-d G:i:s") ;
        $todayDate = date("Y-m-d");
        $times = explode("-", $todayDate) ;

        $currTime = time() ;
        $todayStartTime = mktime(2, 0, 0, $times[1], $times[2], $times[0]) ;

        $yesterdayStartTime = mktime(2, 0, 0, date("m")  , date("d")-1, date("Y"));

        if($currTime > $todayStartTime){
            $calculateTime = date("Y-m-d G:i:s", $todayStartTime) ;
        }else{
            $calculateTime = date("Y-m-d G:i:s",$yesterdayStartTime) ;
        }

        return $calculateTime ;
    }
}
?>
