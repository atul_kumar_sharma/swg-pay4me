<?php
/* 
 *Switch case created to show the Reason for payment process failure
 *
 */

function fps_error($fpsId)
{
  $time = Settings::maxip_time();
  switch ($fpsId)
      {
        case "1" :
          $reason = " You have reached the maximum allowed number of transactions form this computer at the current time. Please try again after ".$time;
          break;
        case "2":
          $reason = " You have reached the maximum allowed number of transactions from this card at this time. Please try again after ".$time;
          break;

        case "3":
          $reason = "Due to anti fraud reasons this transaction can not be processed currently. Please try again after ".$time;
          break;
        case "4":
          $reason = "Your card has been declined by the Processor, please use another card.";
          break;
        case "5":
          $reason = "This card is black listed by Admin, please use another card.";
        default :
          $reason = "--";

           
        }
        return $reason;
}

function fps_error_admin($fpsId)
{
  $time = Settings::maxip_time();
  switch ($fpsId)
      {
        case "0" :
          $reason = " ";
          break;
        case "1" :
          $reason = "Customer has reached maximum allowed number of transactions form the computer at the current time. Customer should try again after ".$time;
          break;
        case "2":
          $reason = "Customer has reached the maximum allowed number of transactions from the card at this time. Customer should try again after ".$time;
          break;

        case "3":
          $reason = "Due to anti fraud reasons the transaction can not be processed currently. Customer should try again after ".$time;
          break;
        default :
          $reason = "--";

        }
        return $reason;
}

?>
