<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class WidgetHelpers {
  public static function getDateRanges($start = 99, $end = 15) {
    $years = range(date('Y') - $start, date('Y') + $end );
    return array_combine($years, $years);
  }
}