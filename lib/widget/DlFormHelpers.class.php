<?php

/**
 * Project Defination List form Formatter.
 *
 * @package    Form Render Formating
 * @path       /lib/Widget/DlFormHelpers.php
 */
interface iUIGroup {
	const BLOCK = 0;
	const FIELD = 1;
	const WEBFORM =2 ;

	/** add a new UI Elenment to Group */
	public function addElement($uigroups);

	/** Returns all the embedded uigroup elements*/
	public function getElements();

	public function setLabel($label);

	public function getLabel();

	/**
	* Returns the type of this group - this
	* will be one of the constants defined
	* in this interface.
	*/
	public function getType();
}

abstract class UIGroupBase implements iUIGroup {
	protected $elements = array();
	protected $label;

  public function UIGroupBase($baseLabel = "") {
    $this->label = $baseLabel;
  }

	public function addElement($uigroups) {
    if(is_array($uigroups)) {
      foreach ($uigroups as $grp) {
        $this->elements[] = $grp;
      }
    } else {
      //    $this->elements = array_merge($this->elements, $uigroups);
      $this->elements[] = $uigroups;
    }
	}

  public function getElements() {
		return $this->elements;
	}

	public function setLabel($label) {
	  $this->label = $label;
	}

	public function getLabel() {
		return $this->label;
	}

}

/*
* TODO:
* In all the folllowing classes - add basic validation in
* addElement method so that a page can't be added to group and
* likewise for lower level hierarchy.
*/

class Dlform extends UIGroupBase {
	public function getType() {
		return iUIGroup::WEBFORM;
	}
}
class FormBlock extends UIGroupBase {
  protected $fieldsName = array();

	public function getType() {
		return iUIGroup::BLOCK;
	}

  public function getFieldsName() {
    return $this->fieldsName;
  }

  public function appendFieldName($fieldName) {
    $this->fieldsName[] = $fieldName;
  }

  public function setFieldsName ($newFieldsName) {
    $this->fieldsName = $newFieldsName;
  }
}

//class FIELD extends UIGroupBase {
//	public function getType() {
//		return iUIGroup::FIELD;
//	}
//}

// --- This is where page rendering starts

//function printElement(iUIGroup $element) {
//	if ($element->getType() == iUIGroup::WEBFORM) {
//		echo "\n<h1>Page: ".$element->getLabel()."</h1>";
//	} else if ($element->getType() == iUIGroup::BLOCK) {
//		echo "\n<fieldset class='multiForm'><legend>Page: ".$element->getLabel()."</legend>";
//	} else if ($element->getType() == iUIGroup::FIELD) {
//		echo "\n<dl><dt><label>".$element->getLabel()."</label></dt><dd><input type='text' /></dd></dl>";
//	}
//
//	foreach ($element->getElements() as $elem)
//	{
//		printElement($elem);
//	}
//
//	echo ($element->getType() == iUIGroup::BLOCK)?"</fieldset>":'';
//
//}
