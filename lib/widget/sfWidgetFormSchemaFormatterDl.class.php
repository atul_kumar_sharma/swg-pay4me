<?php

/*
 * This file is part of the symfony package.
 * (c) Fabien Potencier <fabien.potencier@symfony-project.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * 
 *
 * @package    symfony
 * @subpackage widget
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfWidgetFormSchemaFormatterTable.class.php 5995 2007-11-13 15:50:03Z fabien $
 */
class sfWidgetFormSchemaFormatterDl extends sfWidgetFormSchemaFormatter
{
  protected
    $rowFormat                = "<dl id='%widgetName%_row'>\n  <dt>%label%</dt>\n  <dd><ul class='fcol'><li class='fElement'>%field%</li><li class='help'>%help%</li><li class='error'>%error%</li><li class='hidden'>%hidden_fields%</li></ul></dd>\n</dl>\n",
    $errorRowFormat           = "<div class='error_list'>%errors%</div>\n",
    //$errorRowFormat           = "<dl class='error_list'>\n  <dt>%label%</dt>\n  <dd>%field%%error%<caption>%help%</caption>%hidden_fields%</dd>\n</dl>\n",
    $helpFormat               = "\n %help%\n",
    $decoratorFormat          = "<fieldset>\n  %content%</fieldset>",
    $errorListFormatInARow    = "%errors%",
    $errorRowFormatInARow     = "%error%\n",
    $namedErrorRowFormatInARow = '%name%: %error% \n ';

  public function formatRow($label, $field, $errors = array(), $help = '', $hiddenFields = null)
  {
    if (!preg_match('<label for="([^"]+)">',$label,$matches)) {
     $matches[1] = '';
    }
    return strtr($this->getRowFormat(), array(
      '%widgetName%'                => $matches[1],
      '%label%'         => $label,
      '%field%'         => $field,
      '%error%'         => $this->formatErrorsForRow($errors),
      '%help%'          => $this->formatHelp($help),
      '%hidden_fields%' => is_null($hiddenFields) ? '%hidden_fields%' : $hiddenFields,
    ));
  }


//  public function __construct(sfWidgetFormSchema $widgetSchema)
//  {
//    $this->setWidgetSchema($widgetSchema);
//   // $this->getWidgetSchema()->setRowFormat("<dl class=".$this->getWidgetSchema()->getGenerateID()."><pre>".print_r(get_class_methods($this->getWidgetSchema()))."<br>\n  <dt>%label%</dt>\n  <dd><ul class='fcol'><li class='fElement'>%field%</li><li class='error'>%error%</li><li class='help'>%help%</li><li class='hidden'>%hidden_fields%</li></ul></dd>\n</dl>\n") ;
//   $this->rowFormat = "<dl class=".$this->getWidgetSchema()->generateName()."><pre>".print_r(get_class_methods($this->getWidgetSchema()))."<br>\n  <dt>%label%</dt>\n  <dd><ul class='fcol'><li class='fElement'>%field%</li><li class='error'>%error%</li><li class='help'>%help%</li><li class='hidden'>%hidden_fields%</li></ul></dd>\n</dl>\n" ;
//  }


}
