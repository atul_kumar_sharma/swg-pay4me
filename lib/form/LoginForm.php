<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class LoginForm extends sfGuardFormSignin {


    private function _setValidators() {
        //   $this->validateUsername = new sfValidatorAnd(
        //                              array(array('required'=>true),array('required'=>'Username can\'t be empty')),
        //
        //   );

        $this->validateUsername = new sfValidatorAnd(
            array(
                new sfValidatorString(array('min_length' => 3, 'max_length' => 50),
                    array('min_length' => 'The username should be at least three characters long',
                                                                    'max_length' => 'The username should be fifty characters long at most',
                    )
                ),
                new sfValidatorCallback(array('callback'  => array($this, 'validateUser')
                    )
                ),

            ),
            array('required'   => TRUE),
            array('required'   => 'Please enter username')
        );

        $this->validatePassword = new sfValidatorString(array('required' => TRUE),array('required'   => 'Please enter password'));


    }

    private function _setWidgets() {
        $this->username = new sfWidgetFormInput(array(), array('id' => 'username','maxlength'=>25, 'size' => 25,'autocomplete' => 'off' ,'class' => 'username_input' ));
        $this->password     = new sfWidgetFormInputPassword(array(), array('id' => 'password', 'maxlength'=>25, 'size' => 25,'autocomplete' => 'off', 'class' => 'username_input'));
    }

    public function configure()     {
        $this->_setValidators();
        $this->_setWidgets();

           /*
         * Set validators
         */
        $this->setValidators(array('username' => $this->validateUsername,
                                   'password'     => $this->validatePassword,
            )
        );

        /*
         * Set widgets
         */
        $this->setWidgets(array('username' => $this->username,
                                'password'     => $this->password,
            )
        );

        $this->widgetSchema->setNameFormat('signin[%s]');
        //     $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());



         /*
         * Set decorator
         */
        //        $oDecorator = new sfWidgetFormSchemaFormatterDiv($this->getWidgetSchema());
        //        $this->getWidgetSchema()->addFormFormatter('div', $oDecorator);
        //        $this->getWidgetSchema()->setFormFormatterName('div');




    }

    public function validateUser($validator, $values) {

        $username = $_REQUEST['signin']['username'];
        $isActive=1;
        $password = $_REQUEST['signin']['password'];

        $user = Doctrine::getTable('sfGuardUser')->retrieveByUsername($username,$isActive);


        $error = new sfValidatorError($validator,'The username and/or password is invalid.');
        //$user = $query->fetchOne();
        if(!$user) {
            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_SECURITY,
                EpAuditEvent::$SUBCATEGORY_SECURITY_INCORRECT_USERNAME,
                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_INCORRECT_USERNAME, array('username'=>$username)),
                NULL,
                NULL,
                $username
            );
            sfcontext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            throw new sfValidatorErrorSchema($validator,array($error));
        }
        if(!($user->getIsActive() && $user->checkPassword($password))) {

            $eventHolder = new pay4meAuditEventHolder(
                EpAuditEvent::$CATEGORY_SECURITY,
                EpAuditEvent::$SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER,
                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER, array('username'=>$username)),
                NULL,
                $user->getId(),
                $username
            );
            sfcontext::getInstance()->getEventDispatcher()->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            throw new sfValidatorErrorSchema($validator,array($error));
        }
        //print_r($_REQUEST);
        //  $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());
        return $values;
    }


    //            public function bind(array $taintedValues = null, array $taintedFiles = array()) {
    //        $request = sfContext::getInstance()->getRequest();
    //
    //        if ($request->hasParameter(self::$CSRFFieldName))         {
    //            $taintedValues[self::$CSRFFieldName] = $request->getParameter(self::$CSRFFieldName);
    //        }
    //
    //        parent::bind($taintedValues, $taintedFiles);
    //    }


/*public function configure() {
  parent::configure();

  unset( $this['remember']);

  $this->setValidators(array(
      'username' => new sfValidatorString(array('required'=>true),array('required'=>'Username can\'t be empty')),
      'password' => new sfValidatorString(array('required'=>true),array('required'=>'Password can\'t be empty')),
     // 'remember' => new sfValidatorBoolean(),
      ));

    $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());
  }*/
}

?>
