<?php

/**
 * CurrencyMaster form base class.
 *
 * @method CurrencyMaster getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCurrencyMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'from_currency_id' => new sfWidgetFormInputText(),
      'to_currency_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('GlobalMaster'), 'add_empty' => true)),
      'exchange_rate'    => new sfWidgetFormInputText(),
      'exchange_dt'      => new sfWidgetFormDate(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'from_currency_id' => new sfValidatorInteger(array('required' => false)),
      'to_currency_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('GlobalMaster'), 'required' => false)),
      'exchange_rate'    => new sfValidatorNumber(array('required' => false)),
      'exchange_dt'      => new sfValidatorDate(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('currency_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyMaster';
  }

}
