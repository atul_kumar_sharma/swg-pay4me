<?php

/**
 * ExecutionState form base class.
 *
 * @method ExecutionState getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseExecutionStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'execution_id'        => new sfWidgetFormInputHidden(),
      'node_id'             => new sfWidgetFormInputHidden(),
      'node_state'          => new sfWidgetFormTextarea(),
      'node_activated_from' => new sfWidgetFormTextarea(),
      'node_thread_id'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'execution_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'execution_id', 'required' => false)),
      'node_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'node_id', 'required' => false)),
      'node_state'          => new sfValidatorString(),
      'node_activated_from' => new sfValidatorString(),
      'node_thread_id'      => new sfValidatorInteger(),
    ));

    $this->widgetSchema->setNameFormat('execution_state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ExecutionState';
  }

}
