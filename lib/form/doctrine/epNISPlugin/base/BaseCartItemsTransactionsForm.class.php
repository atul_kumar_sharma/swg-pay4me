<?php

/**
 * CartItemsTransactions form base class.
 *
 * @method CartItemsTransactions getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCartItemsTransactionsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'cart_id'            => new sfWidgetFormInputText(),
      'item_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentRequest'), 'add_empty' => true)),
      'transaction_number' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentResponse'), 'add_empty' => true)),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'cart_id'            => new sfValidatorInteger(array('required' => false)),
      'item_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentRequest'), 'required' => false)),
      'transaction_number' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IPaymentResponse'), 'required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('cart_items_transactions[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartItemsTransactions';
  }

}
