<?php

/**
 * VisaOfficeAddress form base class.
 *
 * @method VisaOfficeAddress getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaOfficeAddressForm extends AddressMasterInterNationalForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('visa_office_address[%s]');
  }

  public function getModelName()
  {
    return 'VisaOfficeAddress';
  }

}
