<?php

/**
 * VApplicantFiveYearsTravelHistory form base class.
 *
 * @method VApplicantFiveYearsTravelHistory getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVApplicantFiveYearsTravelHistoryForm extends VisaApplicantTravelHistoryForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('v_applicant_five_years_travel_history[%s]');
  }

  public function getModelName()
  {
    return 'VApplicantFiveYearsTravelHistory';
  }

}
