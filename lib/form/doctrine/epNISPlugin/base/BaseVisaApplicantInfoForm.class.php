<?php

/**
 * VisaApplicantInfo form base class.
 *
 * @method VisaApplicantInfo getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicantInfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                 => new sfWidgetFormInputHidden(),
      'issusing_govt'                      => new sfWidgetFormInputText(),
      'passport_number'                    => new sfWidgetFormInputText(),
      'date_of_issue'                      => new sfWidgetFormDate(),
      'date_of_exp'                        => new sfWidgetFormDate(),
      'place_of_issue'                     => new sfWidgetFormInputText(),
      'visatype_id'                        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeId'), 'add_empty' => false)),
      'applying_country_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ApplyingCountry'), 'add_empty' => false)),
      'embassy_of_pref_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => false)),
      'purpose_of_journey'                 => new sfWidgetFormInputText(),
      'entry_type_id'                      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => false)),
      'no_of_re_entry_type'                => new sfWidgetFormInputText(),
      'stay_duration_days'                 => new sfWidgetFormInputText(),
      'proposed_date_of_travel'            => new sfWidgetFormDate(),
      'mode_of_travel'                     => new sfWidgetFormInputText(),
      'money_in_hand'                      => new sfWidgetFormInputText(),
      'application_id'                     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'applied_nigeria_visa'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'nigeria_visa_applied_place'         => new sfWidgetFormInputText(),
      'applied_nigeria_visa_status'        => new sfWidgetFormChoice(array('choices' => array('Granted' => 'Granted', 'Rejected' => 'Rejected'))),
      'applied_nigeria_visa_reject_reason' => new sfWidgetFormInputText(),
      'have_visited_nigeria'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'visited_reason_type_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeReason'), 'add_empty' => true)),
      'applying_country_duration'          => new sfWidgetFormInputText(),
      'contagious_disease'                 => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'police_case'                        => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'narcotic_involvement'               => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_status'                    => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'deported_county_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('DeportedCountry'), 'add_empty' => true)),
      'visa_fraud_status'                  => new sfWidgetFormChoice(array('choices' => array('Yes' => 'Yes', 'No' => 'No'))),
      'authority_id'                       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'add_empty' => true)),
      'created_at'                         => new sfWidgetFormDateTime(),
      'updated_at'                         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'issusing_govt'                      => new sfValidatorString(array('max_length' => 64)),
      'passport_number'                    => new sfValidatorString(array('max_length' => 64)),
      'date_of_issue'                      => new sfValidatorDate(),
      'date_of_exp'                        => new sfValidatorDate(),
      'place_of_issue'                     => new sfValidatorString(array('max_length' => 64)),
      'visatype_id'                        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeId'))),
      'applying_country_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ApplyingCountry'))),
      'embassy_of_pref_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'))),
      'purpose_of_journey'                 => new sfValidatorString(array('max_length' => 255)),
      'entry_type_id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'))),
      'no_of_re_entry_type'                => new sfValidatorInteger(array('required' => false)),
      'stay_duration_days'                 => new sfValidatorInteger(),
      'proposed_date_of_travel'            => new sfValidatorDate(array('required' => false)),
      'mode_of_travel'                     => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'money_in_hand'                      => new sfValidatorInteger(),
      'application_id'                     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'applied_nigeria_visa'               => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'nigeria_visa_applied_place'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'applied_nigeria_visa_status'        => new sfValidatorChoice(array('choices' => array(0 => 'Granted', 1 => 'Rejected'), 'required' => false)),
      'applied_nigeria_visa_reject_reason' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'have_visited_nigeria'               => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'visited_reason_type_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaTypeReason'), 'required' => false)),
      'applying_country_duration'          => new sfValidatorInteger(array('required' => false)),
      'contagious_disease'                 => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'police_case'                        => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'narcotic_involvement'               => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'deported_status'                    => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'deported_county_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('DeportedCountry'), 'required' => false)),
      'visa_fraud_status'                  => new sfValidatorChoice(array('choices' => array(0 => 'Yes', 1 => 'No'), 'required' => false)),
      'authority_id'                       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'required' => false)),
      'created_at'                         => new sfValidatorDateTime(),
      'updated_at'                         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visa_applicant_info[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicantInfo';
  }

}
