<?php

/**
 * TblBlockApplicant form base class.
 *
 * @method TblBlockApplicant getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblBlockApplicantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'first_name'         => new sfWidgetFormInputText(),
      'middle_name'        => new sfWidgetFormInputText(),
      'last_name'          => new sfWidgetFormInputText(),
      'dob'                => new sfWidgetFormDateTime(),
      'card_first'         => new sfWidgetFormInputText(),
      'card_last'          => new sfWidgetFormInputText(),
      'app_id'             => new sfWidgetFormInputText(),
      'ref_no'             => new sfWidgetFormInputText(),
      'app_type'           => new sfWidgetFormInputText(),
      'card_holder_name'   => new sfWidgetFormInputText(),
      'email'              => new sfWidgetFormInputText(),
      'processing_country' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'embassy'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'add_empty' => true)),
      'blocked'            => new sfWidgetFormChoice(array('choices' => array('yes' => 'yes', 'no' => 'no'))),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'first_name'         => new sfValidatorPass(),
      'middle_name'        => new sfValidatorPass(array('required' => false)),
      'last_name'          => new sfValidatorPass(),
      'dob'                => new sfValidatorDateTime(),
      'card_first'         => new sfValidatorString(array('max_length' => 5)),
      'card_last'          => new sfValidatorString(array('max_length' => 5)),
      'app_id'             => new sfValidatorInteger(),
      'ref_no'             => new sfValidatorInteger(array('required' => false)),
      'app_type'           => new sfValidatorPass(),
      'card_holder_name'   => new sfValidatorString(array('max_length' => 255)),
      'email'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'processing_country' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'embassy'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EmbassyMaster'), 'required' => false)),
      'blocked'            => new sfValidatorChoice(array('choices' => array(0 => 'yes', 1 => 'no'), 'required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('tbl_block_applicant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblBlockApplicant';
  }

}
