<?php

/**
 * VisaProcessingCentre form base class.
 *
 * @method VisaProcessingCentre getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaProcessingCentreForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'centre_name'        => new sfWidgetFormInputText(),
      'office_capacity'    => new sfWidgetFormInputText(),
      'desk_active_status' => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'anchored_to'        => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'centre_name'        => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'office_capacity'    => new sfValidatorInteger(array('required' => false)),
      'desk_active_status' => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'anchored_to'        => new sfValidatorInteger(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visa_processing_centre[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaProcessingCentre';
  }

}
