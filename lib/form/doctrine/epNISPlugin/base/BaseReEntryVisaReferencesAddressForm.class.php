<?php

/**
 * ReEntryVisaReferencesAddress form base class.
 *
 * @method ReEntryVisaReferencesAddress getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReEntryVisaReferencesAddressForm extends AddressMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('re_entry_visa_references_address[%s]');
  }

  public function getModelName()
  {
    return 'ReEntryVisaReferencesAddress';
  }

}
