<?php

/**
 * PassportFatherAddress form base class.
 *
 * @method PassportFatherAddress getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportFatherAddressForm extends AddressMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('passport_father_address[%s]');
  }

  public function getModelName()
  {
    return 'PassportFatherAddress';
  }

}
