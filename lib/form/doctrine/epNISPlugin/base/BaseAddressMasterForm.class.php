<?php

/**
 * AddressMaster form base class.
 *
 * @method AddressMaster getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAddressMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'address_1'  => new sfWidgetFormInputText(),
      'address_2'  => new sfWidgetFormInputText(),
      'city'       => new sfWidgetFormInputText(),
      'country_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'add_empty' => true)),
      'state'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'add_empty' => true)),
      'lga_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LGA'), 'add_empty' => true)),
      'district'   => new sfWidgetFormInputText(),
      'postcode'   => new sfWidgetFormInputText(),
      'var_type'   => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'address_1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address_2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'country_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Country'), 'required' => false)),
      'state'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('State'), 'required' => false)),
      'lga_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('LGA'), 'required' => false)),
      'district'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'postcode'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'var_type'   => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('address_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AddressMaster';
  }

}
