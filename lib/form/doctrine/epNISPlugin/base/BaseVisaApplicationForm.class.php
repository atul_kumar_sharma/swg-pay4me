<?php

/**
 * VisaApplication form base class.
 *
 * @method VisaApplication getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'visacategory_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'add_empty' => true)),
      'zone_type_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaZoneType'), 'add_empty' => true)),
      'ref_no'                  => new sfWidgetFormInputText(),
      'title'                   => new sfWidgetFormChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR'))),
      'surname'                 => new sfWidgetFormInputText(),
      'middle_name'             => new sfWidgetFormInputText(),
      'other_name'              => new sfWidgetFormInputText(),
      'gender'                  => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'email'                   => new sfWidgetFormInputText(),
      'marital_status'          => new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced', 'None' => 'None'))),
      'date_of_birth'           => new sfWidgetFormDate(),
      'place_of_birth'          => new sfWidgetFormInputText(),
      'hair_color'              => new sfWidgetFormChoice(array('choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray', 'None' => 'None'))),
      'eyes_color'              => new sfWidgetFormChoice(array('choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray', 'None' => 'None'))),
      'id_marks'                => new sfWidgetFormInputText(),
      'height'                  => new sfWidgetFormInputText(),
      'present_nationality_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CurrentCountry'), 'add_empty' => true)),
      'previous_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PreviousCountry'), 'add_empty' => true)),
      'permanent_address_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaPermanentAddress'), 'add_empty' => true)),
      'perm_phone_no'           => new sfWidgetFormInputText(),
      'profession'              => new sfWidgetFormInputText(),
      'office_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOfficeAddress'), 'add_empty' => true)),
      'office_phone_no'         => new sfWidgetFormInputText(),
      'milltary_in'             => new sfWidgetFormInputText(),
      'military_dt_from'        => new sfWidgetFormDate(),
      'military_dt_to'          => new sfWidgetFormDate(),
      'ispaid'                  => new sfWidgetFormInputCheckbox(),
      'payment_trans_id'        => new sfWidgetFormInputText(),
      'term_chk_flg'            => new sfWidgetFormInputCheckbox(),
      'interview_date'          => new sfWidgetFormDate(),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Paid' => 'Paid', 'Vetted' => 'Vetted', 'Approved' => 'Approved', 'Rejected by Vetter' => 'Rejected by Vetter', 'Rejected by Approver' => 'Rejected by Approver'))),
      'payment_gateway_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'add_empty' => true)),
      'paid_dollar_amount'      => new sfWidgetFormInputText(),
      'paid_naira_amount'       => new sfWidgetFormInputText(),
      'amount'                  => new sfWidgetFormInputText(),
      'currency_id'             => new sfWidgetFormInputText(),
      'paid_at'                 => new sfWidgetFormDate(),
      'is_email_valid'          => new sfWidgetFormInputCheckbox(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'visacategory_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaCategory'), 'required' => false)),
      'zone_type_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaZoneType'), 'required' => false)),
      'ref_no'                  => new sfValidatorInteger(array('required' => false)),
      'title'                   => new sfValidatorChoice(array('choices' => array(0 => 'MR', 1 => 'MRS', 2 => 'MISS', 3 => 'DR'))),
      'surname'                 => new sfValidatorString(array('max_length' => 50)),
      'middle_name'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'other_name'              => new sfValidatorString(array('max_length' => 50)),
      'gender'                  => new sfValidatorChoice(array('choices' => array(0 => 'Male', 1 => 'Female'))),
      'email'                   => new sfValidatorString(array('max_length' => 100)),
      'marital_status'          => new sfValidatorChoice(array('choices' => array(0 => 'Single', 1 => 'Married', 2 => 'Widowed', 3 => 'Divorced', 4 => 'None'), 'required' => false)),
      'date_of_birth'           => new sfValidatorDate(),
      'place_of_birth'          => new sfValidatorString(array('max_length' => 100)),
      'hair_color'              => new sfValidatorChoice(array('choices' => array(0 => 'Black', 1 => 'Brown', 2 => 'White', 3 => 'Gray', 4 => 'None'), 'required' => false)),
      'eyes_color'              => new sfValidatorChoice(array('choices' => array(0 => 'Brown', 1 => 'Blue', 2 => 'Green', 3 => 'Gray', 4 => 'None'), 'required' => false)),
      'id_marks'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'height'                  => new sfValidatorInteger(array('required' => false)),
      'present_nationality_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CurrentCountry'), 'required' => false)),
      'previous_nationality_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PreviousCountry'), 'required' => false)),
      'permanent_address_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaPermanentAddress'), 'required' => false)),
      'perm_phone_no'           => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'profession'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'office_address_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOfficeAddress'), 'required' => false)),
      'office_phone_no'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'milltary_in'             => new sfValidatorString(array('max_length' => 45, 'required' => false)),
      'military_dt_from'        => new sfValidatorDate(array('required' => false)),
      'military_dt_to'          => new sfValidatorDate(array('required' => false)),
      'ispaid'                  => new sfValidatorBoolean(array('required' => false)),
      'payment_trans_id'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'term_chk_flg'            => new sfValidatorBoolean(array('required' => false)),
      'interview_date'          => new sfValidatorDate(array('required' => false)),
      'status'                  => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Paid', 2 => 'Vetted', 3 => 'Approved', 4 => 'Rejected by Vetter', 5 => 'Rejected by Approver'), 'required' => false)),
      'payment_gateway_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PaymentGatewayType'), 'required' => false)),
      'paid_dollar_amount'      => new sfValidatorNumber(array('required' => false)),
      'paid_naira_amount'       => new sfValidatorNumber(array('required' => false)),
      'amount'                  => new sfValidatorNumber(array('required' => false)),
      'currency_id'             => new sfValidatorInteger(array('required' => false)),
      'paid_at'                 => new sfValidatorDate(array('required' => false)),
      'is_email_valid'          => new sfValidatorBoolean(array('required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visa_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplication';
  }

}
