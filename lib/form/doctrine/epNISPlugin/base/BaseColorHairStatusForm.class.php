<?php

/**
 * ColorHairStatus form base class.
 *
 * @method ColorHairStatus getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseColorHairStatusForm extends GlobalMasterForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('color_hair_status[%s]');
  }

  public function getModelName()
  {
    return 'ColorHairStatus';
  }

}
