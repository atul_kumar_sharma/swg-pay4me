<?php

/**
 * ReEntryVisaApplication form base class.
 *
 * @method ReEntryVisaApplication getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReEntryVisaApplicationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'issusing_govt'             => new sfWidgetFormInputText(),
      'passport_number'           => new sfWidgetFormInputText(),
      'date_of_issue'             => new sfWidgetFormDate(),
      'date_of_exp'               => new sfWidgetFormDate(),
      'place_of_issue'            => new sfWidgetFormInputText(),
      'address_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaAddress'), 'add_empty' => true)),
      'profession'                => new sfWidgetFormInputText(),
      'reason_for_visa_requiring' => new sfWidgetFormInputText(),
      'last_arrival_in_nigeria'   => new sfWidgetFormDate(),
      'proposeddate'              => new sfWidgetFormDate(),
      're_entry_category'         => new sfWidgetFormChoice(array('choices' => array(3 => '3', 6 => '6', 12 => '12'))),
      're_entry_type'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'), 'add_empty' => false)),
      'no_of_re_entry_type'       => new sfWidgetFormInputText(),
      'employer_name'             => new sfWidgetFormInputText(),
      'employer_phone'            => new sfWidgetFormInputText(),
      'employer_address_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaEmployerAddress'), 'add_empty' => true)),
      'cerpa_quota'               => new sfWidgetFormInputText(),
      'cerpac_issuing_state'      => new sfWidgetFormInputText(),
      'cerpac_date_of_issue'      => new sfWidgetFormDate(),
      'cerpac_exp_date'           => new sfWidgetFormDate(),
      'issuing_cerpac_office'     => new sfWidgetFormInputText(),
      'visa_type_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'add_empty' => true)),
      'visa_state_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProceesingState'), 'add_empty' => true)),
      'visa_office_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOffice'), 'add_empty' => true)),
      'application_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'processing_centre_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'add_empty' => true)),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'issusing_govt'             => new sfValidatorString(array('max_length' => 64)),
      'passport_number'           => new sfValidatorString(array('max_length' => 64)),
      'date_of_issue'             => new sfValidatorDate(),
      'date_of_exp'               => new sfValidatorDate(),
      'place_of_issue'            => new sfValidatorString(array('max_length' => 64)),
      'address_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaAddress'), 'required' => false)),
      'profession'                => new sfValidatorString(array('max_length' => 100)),
      'reason_for_visa_requiring' => new sfValidatorString(array('max_length' => 255)),
      'last_arrival_in_nigeria'   => new sfValidatorDate(),
      'proposeddate'              => new sfValidatorDate(array('required' => false)),
      're_entry_category'         => new sfValidatorChoice(array('choices' => array(0 => '3', 1 => '6', 2 => '12'), 'required' => false)),
      're_entry_type'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EntryType'))),
      'no_of_re_entry_type'       => new sfValidatorInteger(array('required' => false)),
      'employer_name'             => new sfValidatorString(array('max_length' => 70, 'required' => false)),
      'employer_phone'            => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'employer_address_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaEmployerAddress'), 'required' => false)),
      'cerpa_quota'               => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'cerpac_issuing_state'      => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'cerpac_date_of_issue'      => new sfValidatorDate(array('required' => false)),
      'cerpac_exp_date'           => new sfValidatorDate(array('required' => false)),
      'issuing_cerpac_office'     => new sfValidatorString(array('max_length' => 64, 'required' => false)),
      'visa_type_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaType'), 'required' => false)),
      'visa_state_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProceesingState'), 'required' => false)),
      'visa_office_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaOffice'), 'required' => false)),
      'application_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'processing_centre_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaProcessingCentre'), 'required' => false)),
      'created_at'                => new sfValidatorDateTime(),
      'updated_at'                => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_application[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaApplication';
  }

}
