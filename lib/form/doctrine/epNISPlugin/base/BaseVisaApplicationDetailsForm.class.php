<?php

/**
 * VisaApplicationDetails form base class.
 *
 * @method VisaApplicationDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseVisaApplicationDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'application_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'employer_name'                => new sfWidgetFormInputText(),
      'position_occupied'            => new sfWidgetFormInputText(),
      'job_description'              => new sfWidgetFormInputText(),
      'relative_employer_name'       => new sfWidgetFormInputText(),
      'relative_employer_phone'      => new sfWidgetFormInputText(),
      'relative_employer_address_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaRelativeEmployerAddress'), 'add_empty' => true)),
      'relative_nigeria_leaving_mth' => new sfWidgetFormInputText(),
      'intended_address_nigeria_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaIntendedAddressNigeriaAddress'), 'add_empty' => true)),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'employer_name'                => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'position_occupied'            => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'job_description'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'relative_employer_name'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'relative_employer_phone'      => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'relative_employer_address_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaRelativeEmployerAddress'), 'required' => false)),
      'relative_nigeria_leaving_mth' => new sfValidatorInteger(array('required' => false)),
      'intended_address_nigeria_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaIntendedAddressNigeriaAddress'), 'required' => false)),
      'created_at'                   => new sfValidatorDateTime(),
      'updated_at'                   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('visa_application_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisaApplicationDetails';
  }

}
