<?php

/**
 * Country form base class.
 *
 * @method Country getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCountryForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'country_name'           => new sfWidgetFormInputText(),
      'is_ecowas'              => new sfWidgetFormInputCheckbox(),
      'is_visa_fee_applicable' => new sfWidgetFormInputCheckbox(),
      'is_african'             => new sfWidgetFormInputCheckbox(),
      'currency_id'            => new sfWidgetFormInputText(),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'country_name'           => new sfValidatorString(array('max_length' => 150)),
      'is_ecowas'              => new sfValidatorBoolean(array('required' => false)),
      'is_visa_fee_applicable' => new sfValidatorBoolean(array('required' => false)),
      'is_african'             => new sfValidatorBoolean(array('required' => false)),
      'currency_id'            => new sfValidatorInteger(array('required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('country[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Country';
  }

}
