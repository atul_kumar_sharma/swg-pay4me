<?php

/**
 * ReEntryVisaReferences form base class.
 *
 * @method ReEntryVisaReferences getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseReEntryVisaReferencesForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'application_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'add_empty' => true)),
      'name_of_refree'       => new sfWidgetFormInputText(),
      'phone_of_refree'      => new sfWidgetFormInputText(),
      'address_of_refree_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaReferencesAddress'), 'add_empty' => true)),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisaApplication'), 'required' => false)),
      'name_of_refree'       => new sfValidatorString(array('max_length' => 100)),
      'phone_of_refree'      => new sfValidatorString(array('max_length' => 20)),
      'address_of_refree_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ReEntryVisaReferencesAddress'), 'required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('re_entry_visa_references[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ReEntryVisaReferences';
  }

}
