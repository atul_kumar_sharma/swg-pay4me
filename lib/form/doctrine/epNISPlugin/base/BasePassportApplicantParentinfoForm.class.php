<?php

/**
 * PassportApplicantParentinfo form base class.
 *
 * @method PassportApplicantParentinfo getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BasePassportApplicantParentinfoForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'application_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'add_empty' => true)),
      'father_name'           => new sfWidgetFormInputText(),
      'father_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('FatherCountry'), 'add_empty' => false)),
      'father_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportFatherAddress'), 'add_empty' => true)),
      'mother_name'           => new sfWidgetFormInputText(),
      'mother_nationality_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MotherCountry'), 'add_empty' => false)),
      'mother_address_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PassportMotherAddress'), 'add_empty' => true)),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'application_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportApplication'), 'required' => false)),
      'father_name'           => new sfValidatorString(array('max_length' => 70)),
      'father_nationality_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('FatherCountry'))),
      'father_address_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportFatherAddress'), 'required' => false)),
      'mother_name'           => new sfValidatorString(array('max_length' => 70)),
      'mother_nationality_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MotherCountry'))),
      'mother_address_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PassportMotherAddress'), 'required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('passport_applicant_parentinfo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PassportApplicantParentinfo';
  }

}
