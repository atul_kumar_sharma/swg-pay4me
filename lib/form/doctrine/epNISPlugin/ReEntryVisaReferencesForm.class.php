<?php

/**
 * ReEntryVisaReferences form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ReEntryVisaReferencesForm extends PluginReEntryVisaReferencesForm
{
    public function configure()
    {
        //Unset Field Name
        unset($this['created_at'],$this['updated_at'],$this['application_id']);

        //Set Field Lable
        $this->widgetSchema->setLabels(
            array(
                  'name_of_refree'    => 'Name of Referee',
                  'phone_of_refree'    => 'Phone No.',
                //'address_of_refree'    => 'Address'
            ));

        //Set Field Type
        // $this->widgetSchema['address_of_refree'] =  new sfWidgetFormTextarea(array('label' => 'Address'));

        $this->validatorSchema['name_of_refree']    = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'),'max_length' => 20),array('required' => 'Referee name is required.',
      'max_length'=>'Referee name can not be more than 20 characters','invalid'=>'Name of refree is not valid'));
        // $this->validatorSchema['address_of_refree'] = new sfValidatorString(array('max_length' => 255),array('required' => 'Referee address is required.','max_length'=>'Referee address can not be more than 255 characters'));
        $this->validatorSchema['phone_of_refree'] = new sfValidatorAnd(array(
                new sfValidatorString(array('max_length' => 20,'min_length' => 6),array('required' => 'Referee phone is required.','min_length'=>'Phone number will be minimum 6 digits.','max_length'=>'Phone number can not be more than 20 digits.')),
                new sfValidatorRegex(array('pattern' => '/^[0-9+-]*$/'),array('invalid' => 'Phone number is not valid.'))
            ),
            array('halt_on_error' => true),
            array('required' => 'Referee phone is required')
        );
    }
}
