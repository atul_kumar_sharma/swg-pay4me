<?php

class EditVapApplicationForm extends VapApplicationForm
{
    public function configure()
    {
        unset ($this['id'],$this['boarding_place'],$this['flight_number'],$this['flight_carrier'],$this['arrival_date'],$this['ref_no'],$this['previous_nationality_id'],$this['applicant_type'],$this['interview_date'],$this['permanent_address_id'],$this['perm_phone_no'],$this['email'],$this['occupation'],$this['place_of_birth'],$this['ispaid'],$this['payment_trans_id'],$this['height'],$this['processing_country_id'],$this['processing_center_id'],$this['next_kin_address_id'],$this['status'],$this['payment_gateway_id'],$this['paid_dollar_amount'],$this['is_email_valid'],$this['created_at'],$this['updated_at'],$this['present_nationality_id'],$this['id_marks'],$this['office_address_id'],$this['office_phone_no'],$this['vap_company_id'],$this['customer_service_number'],$this['document_1'],$this['document_2'],$this['document_3'],$this['document_4'],$this['document_5'],$this['issusing_govt'],$this['passport_number'],$this['date_of_issue'],$this['date_of_exp'],$this['place_of_issue'],$this['applying_country_id'],$this['type_of_visa'],$this['trip_money'],$this['local_company_name'],$this['local_company_address_id'],$this['contagious_disease'],$this['police_case'],$this['narcotic_involvement'],$this['deported_status'],$this['deported_county_id'],$this['visa_fraud_status'],$this['paid_naira_amount'],$this['paid_date'],$this['profession'],$this['office_address_id'],$this['office_address_id'],$this['applied_nigeria_visa'],$this['nigeria_visa_applied_place'],$this['applied_nigeria_visa_status'],$this['applied_nigeria_visa_reject_reason'],$this['have_visited_nigeria'],$this['visited_reason_type_id'],$this['applying_country_duration'],$this['business_address'],$this['sponsore_type']);


        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
        $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid', 'invalid' => 'Invalid date of birth.'));

        $hairColor = array('Black' => 'Black', 'Brown' => 'Brown', 'Gray' => 'Gray', 'Red' => 'Red', 'White' => 'White');
        $eyesColor = array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray');
        $this->widgetSchema['hair_color'] = new sfWidgetFormChoice(array('choices' => $hairColor));
        $this->widgetSchema['eyes_color'] = new sfWidgetFormChoice(array('choices' => $eyesColor));
        $this->widgetSchema['marital_status'] = new sfWidgetFormChoice(array('choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));


        $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YY)',
                                          'first_name' => 'First Name',
                                          'middle_name' => 'Middle Name',
                                          'surname' => 'Last Name',
            ));

        $this->validatorSchema['hair_color'] = new sfValidatorChoice(array('choices' => $hairColor, 'required' => false));

    }//End of public function configure()...

}//End of class EditVapApplicationForm ...