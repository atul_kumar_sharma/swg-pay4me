<?php

class EditPassportApplicationForm extends EstandardPassportForm
{
  public function configure()
  {
      unset ($this['id'],$this['interview_date'],$this['passporttype_id'],$this['ref_no'],$this['email'],$this['occupation'],$this['place_of_birth'],$this['ispaid'],$this['payment_trans_id'],$this['maid_name'],$this['height'],$this['processing_country_id'],$this['processing_state_id'],$this['processing_embassy_id'],$this['processing_passport_office_id'],$this['next_kin'],$this['next_kin_phone'],$this['relation_with_kin'],$this['next_kin_address_id'],$this['passport_no'],$this['status'],$this['payment_gateway_id'],$this['paid_dollar_amount'],$this['local_currency_id'],$this['paid_local_currency_amount'],$this['paid_at'],$this['is_email_valid'],$this['created_at'],$this['updated_at'], $this['interview_date']);


    $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()));
    $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid', 'invalid' => 'Invalid date of birth.'));
    $this->widgetSchema['gender_id'] = new sfWidgetFormChoice(array('label' => 'Gender ','choices' => array('Male' => 'Male', 'Female' => 'Female')));
    $this->widgetSchema['color_eyes_id'] = new sfWidgetFormChoice(array('label' => 'Color of eyes ','choices' => array('Brown' => 'Brown', 'Blue' => 'Blue', 'Green' => 'Green', 'Gray' => 'Gray')));
    $this->widgetSchema['color_hair_id'] = new sfWidgetFormChoice(array('label' => 'Color of Fair ','choices' => array('Black' => 'Black', 'Brown' => 'Brown', 'White' => 'White', 'Gray' => 'Gray')));
    $this->widgetSchema['marital_status_id'] = new sfWidgetFormChoice(array('label' => 'Marital status ','choices' => array('Single' => 'Single', 'Married' => 'Married', 'Widowed' => 'Widowed', 'Divorced' => 'Divorced')));

    $this->widgetSchema->setLabels(array('date_of_birth' => 'Date of Birth(DD/MM/YY)',
              'first_name' => 'First Name',
              'mid_name' => 'Middle Name',
              'last_name' => 'Last Name',
              'color_hair_id'   => 'Color of Hair '
          ));

//    $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(new sfValidatorCallback(array('callback' => array($this, 'checInterviewDate'))))));
      

  }

//  public function  checInterviewDate($validator, $values) {
//
//  $date =  strtotime($values['interview_date']);
//     if($date != '' && !empty($date)){
//         if($date < strtotime(date('Y-m-d'))){
//             $error = new sfValidatorError($validator,"Interview Date should not be past date");
//             throw new sfValidatorErrorSchema($validator,array('interview_date' => $error));
//
//         }
//     }
//     return $values;
// }

}