<?php
class splitForm extends BaseMerchantForm {
    public function configure() {
        sfProjectConfiguration::getActive()->loadHelpers('Asset');

        $i18n = sfContext::getInstance()->getI18N();
        $user = sfContext::getInstance()->getUser();

        $culture = $user->getCulture();
        if (strlen($culture) > 2) {
            $culture = substr($culture, 0, 2);
        }

        $sfWidgetFormI18nJQueryDateOptions = array(
      'culture' => $culture,
      'image'   => image_path('calendar.png'),
      'config'  => "{ duration: '' }"
        );

        $this->setWidgets(array(
        //'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => 'Merchant', 'add_empty' => 'Please select Merchant')),
//        'from_date'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
//        'to_date'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
//        'from_date'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
//        'to_date'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
        'from_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(split_from_date)','class'=>'txt-input')),
        'to_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(split_to_date)','class'=>'txt-input')),


            ));

        $this->setValidators(array(
        //'merchant_id' => new sfValidatorDoctrineChoice(array('model' => 'Merchant', 'required' => true),array('required' => 'Please select Merchant')),
       'from_date'  =>new sfValidatorString(array('required' => true),array('required' => 'Please enter Start Date.')),
       'to_date'    =>new sfValidatorString(array('required' => true),array('required' => 'Please enter End Date.'))
//        'from_date'  =>new sfValidatorString(array('required' => true),array('required' => 'Please enter Start Date.')),
//        'to_date'    =>new sfValidatorString(array('required' => true),array('required' => 'Please enter End Date.')),
//        'from_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the From Date')),
//        'to_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the To Date')),
            ));
     if(isset($_REQUEST['split']['to_date']) && $_REQUEST['split']['to_date']!=""){
        $this->validatorSchema->setPostValidator(

             new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => true),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date ("%right_field%")')
                        )
                    ))

            );

      }
        $this->widgetSchema->setLabels(array(
         // 'merchant_id' => 'Merchant',
          'from_date'   =>  'From Date',
          'to_date'   =>  'To Date',
            ));

        $this->widgetSchema->setNameFormat('split[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
}

?>