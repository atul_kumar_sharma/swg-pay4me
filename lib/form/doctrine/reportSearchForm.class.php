<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of userPaymentreportForm
 *
 * @author spandey
 */
class reportSearchForm extends Baseipay4meOrderForm{
    
    protected static $status = array(''=>'Select Status',  '0'=>'Paid', '1'=>'Unpaid');
    public function configure() {

        $gateway = Doctrine::getTable('Gateway')->findGateway();
        $gatewayarr = Array();
        foreach($gateway as $val){
            $gatewayarr[$val['id']] =  $val['name'];
            
        }      

        $this->setWidgets(array(
        'gateway' => new sfWidgetFormSelect(array('choices' => $gatewayarr), array('lang'=>'blank', 'title'=>'Gateway')),
        'order_number'=> new sfWidgetFormInputText(array(),array('class'=>'txt-input')),
        'from_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(report_from_date)','class'=>'txt-input')),
        'to_date' => new sfWidgetFormInput(array(),array('maxlength'=>10, 'readonly'=>'true','onfocus'=>'showCalendarControl(report_to_date)','class'=>'txt-input')),
        'status'=> new sfWidgetFormSelect(array('choices' => self::$status), array('lang'=>'blank', 'title'=>'Status')),
       
            ));

        $this->setValidators(array(
        'gateway'  =>new sfValidatorString(array('required' => false)),
        'from_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the From Date')),
        'to_date'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter the To Date')),       
        'order_number'=> new sfValidatorString(array('required' => false)),
        'status'=>new sfValidatorString(array('required' => false)),
            ));
        if(isset($_REQUEST['report']['to_date']) && $_REQUEST['report']['to_date']!=""){
            $this->validatorSchema->setPostValidator(

                new sfValidatorAnd(array(
                        new sfValidatorSchemaCompare('from_date', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'to_date',
                            array('throw_global_error' => true),
                            array('invalid' => 'The From Date ("%left_field%") cannot be greater than To Date (%right_field%)')
                        )
                    ))

            );

        }
        $this->widgetSchema->setLabels(array(
                
          'from_date'   =>  'From Date',
          'to_date'   =>  'To Date',
          'bill_number'=>'Bill No',
          'order_number'=>'Order No'
            ));

        $this->widgetSchema->setNameFormat('report[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
}
?>
