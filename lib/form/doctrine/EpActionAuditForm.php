<?php
class EpActionAuditForm extends BaseEpActionAuditEventForm
{
    public function configure()
    {

        $category =  EpAuditEvent::getAllCategories();
        $category[''] = '-- All Categories --';
        $result = array_reverse($category);


        $subSecCatList = EpAuditEvent::getAllSubcategory('security');
        $subTransCatList = EpAuditEvent::getAllSubcategory('transaction');
        $subCatList = array(' ' => '-- All Subcategories --');
        $subCatArray = array_merge((array)$subCatList, (array)$subSecCatList, (array)$subTransCatList);

        //echo "<pre>"; print_r($subCatArray);

        $this->setWidgets(array(
            'startDate'=>new sfWidgetFormInput(array(),array('maxlength'=>30,'readonly'=>'true','onfocus'=>'showCalendarControl(audit_startDate)','class'=>'txt-input')),
            'endDate'=>new sfWidgetFormInput(array(),array('maxlength'=>30,'readonly'=>'true','onfocus'=>'showCalendarControl(audit_endDate)','class'=>'txt-input')),
            'category'=>new sfWidgetFormChoice(array( 'choices'  =>$result, 'expanded' => false, )),
            'subCategory'=>new sfWidgetFormChoice(array('choices'  =>array( '' => '-- All Subcategories --'),'expanded' => false,   ))

            ));

        $this->widgetSchema->setNameFormat('audit[%s]');


        $this->setValidators(array(
      'startDate'    => new sfValidatorString(array('required' => true),array('required' => 'Please enter Start Date.')),
      'endDate'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter End Date.')),
      'category' => new sfValidatorChoice(array( 'choices'  =>array_keys($result),'required' => false)),
      'subCategory' => new sfValidatorChoice(array( 'choices'=>$subCatArray,'required' => false)),


            ));
        
        if(isset($_REQUEST['audit']['endDate']) && $_REQUEST['audit']['endDate']!=""){
        $this->validatorSchema->setPostValidator(

            new sfValidatorSchemaCompare('startDate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endDate',
                array('throw_global_error' => true),
                array('invalid' => 'The start date (%left_field%) cannot be greater than end date (%right_field%)')
            ));
        }

        $this->widgetSchema->setLabels(array(
                'startDate'    => 'Start Date',
                'endDate' => 'End Date',
                'category'=>'Category',
                'subCategory'=>'Sub-Category'
            ));


    }
}
?>
