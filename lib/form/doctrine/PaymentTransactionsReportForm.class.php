<?php
class PaymentTransactionsReportForm extends sfForm
{/*
    static public $card_types = array(
    ''=> 'Please select',
    'V' => 'VISA',
    'M' => 'MASTER',
    'A' => 'AMEX'
    );
    static public $duration = array(
    ''=> 'Please select',
    '30' => '30 Minutes',
    '45' => '45 Minutes',
    '1' => '1 Hour',
    '6' => '6 Hours',
    '1' => '1 day',
    );*/
    
    public function configure()
    {
        $card_types = array(''=> 'Please select','V' => 'VISA','M' => 'MASTER','A' => 'AMEX');
        $duration = array(''=> 'Please select','30' => '30 Minutes','45' => '45 Minutes','1' => '1 Hour', '6' => '6 Hours'
            , '1' => '1 day');
        
        $this->setWidgets(array(
            'startDate'=>new sfWidgetFormInput(array(),array('maxlength'=>30,'readonly'=>'true','onfocus'=>'showCalendarControl(paymentTransaction_startDate)','class'=>'txt-input')),
            'endDate'=>new sfWidgetFormInput(array(),array('maxlength'=>30,'readonly'=>'true','onfocus'=>'showCalendarControl(paymentTransaction_endDate)','class'=>'txt-input')),
            'card_type'   => new sfWidgetFormChoice(array('choices' => $card_types)),
            'duration'=>new sfWidgetFormChoice(array('choices' => $duration),array('onchange'=>'setDurationForDate()'))
           ));

        $this->widgetSchema->setNameFormat('paymentTransaction[%s]');


        $this->setValidators(array(
      'startDate'    => new sfValidatorString(array('required' => true),array('required' => 'Please enter Start Date.')),
      'endDate'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter End Date.')),
     // 'card_type'=>new sfValidatorString(array('required' => false)),
     // 'duration'=>new sfValidatorString(array('required' => true),array('required' => 'Please enter Duration.')),
      'card_type'   => new sfValidatorChoice(array('choices' => array_keys($card_types),'required' => false)),
      'duration'   => new sfValidatorChoice(array('choices' => array_keys($duration)),array('required'=>'Please select Duration')),

            ));

        if(isset($_REQUEST['paymentTransaction']['endDate']) && $_REQUEST['paymentTransaction']['endDate']!=""){
        $this->validatorSchema->setPostValidator(

            new sfValidatorSchemaCompare('startDate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endDate',
                array('throw_global_error' => true),
                array('invalid' => 'The start date (%left_field%) cannot be greater than end date (%right_field%)')
            ));
        }

        $this->widgetSchema->setLabels(array(
                'startDate'    => 'Start Date',
                'endDate' => 'End Date',
                'card_type'=>'Card Type',
                'duration'=>'Duration'
            ));

        $this->widgetSchema->setLabels(array('startDate' => 'Start Date <font color=red>*</font>','endDate' => 'End Date <font color=red>*</font>','duration' => 'Duration <font color=red>*</font>'));
       }
}
?>
