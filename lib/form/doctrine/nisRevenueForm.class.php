<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class NisRevenueForm extends sfForm
{

    public function configure()
    {


       // echo '<pre>';print_r($this->getOptions());die;die;
        $revDate = sfContext::getInstance()->getRequest()->getAttribute('revDate');
        $revAmount = sfContext::getInstance()->getRequest()->getAttribute('revAmount');
        $revRemark = sfContext::getInstance()->getRequest()->getAttribute('remark');

        $this->widgetSchema['date'] = new sfWidgetFormDateCal(array('years'=>WidgetHelpers::getDateRanges()),array('disabled'=>'disabled'));
        if(!empty($revRemark)){
          $this->widgetSchema['amount'] = new sfWidgetFormInput(array(),array('maxlength'=>10,'title'=>'Amount','class'=>'txt-input','readonly'=>'readonly'));
        }else{
          $this->widgetSchema['amount'] = new sfWidgetFormInput(array(),array('maxlength'=>10,'title'=>'Amount','class'=>'txt-input'));
        }
        
       // $this->widgetSchema['holiday'] =  new sfWidgetFormInputCheckbox(array(),array());

        $this->setValidators(array(
        //'holiday'            => new sfValidatorBoolean(array('required'=>true),array('required'=>'Please agree to Terms & Conditions')),
        'amount'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Amount.')),
            ));
        $this->setDefault('date',$revDate);
        $this->setDefault('amount',$revAmount);
        //$this->setDefault('holiday',$revAmount);
        $this->validatorSchema['date'] =   new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid'));





        $this->widgetSchema->setLabels(array(

          'amount'    => 'Amount <font color=red>*</font>',
          //'holiday'    => 'Holiday',
         
          'date'   => 'Date of Transaction<font color=red>*</font>',
            ));


        $this->widgetSchema->setNameFormat('order[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }

    


}
?>
