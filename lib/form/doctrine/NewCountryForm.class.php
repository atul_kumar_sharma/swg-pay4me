<?php

/**
 * Country Validation form.
 *
 * @package    ipay4me
 * @subpackage form
 * @author     Varun
 * @version    
 */
class NewCountryForm extends sfForm
{
    public function configure()
    {

        //arrays to be showen
        $countryId = $this->getOption('countryId');
        //get all card type
        $cardTypeArr = Doctrine::getTable('PaymentMode')->findAll();
        foreach($cardTypeArr as $val){
          $arrCardType[$val->getCardType()] = $val->getDisplayName();
        }
        //get All Services
        $serviceArr = Doctrine::getTable('MidMaster')->findAll();
        foreach($serviceArr as $val){
          $arrService[$val->getMidService()] = $val->getMidService();
        }
        //validation array
        $arrValidation = array('NO'=>'No','FPS'=>'FPS Validation','SN'=>'Surname Validation','IP'=>'IP Check' , 'CCT'=>'Credit Card transaction Limit' , 'BLC'=>'Blacklist card check' ,'BLCA'=>'Admin Blacklist card check');

        $this->widgetSchema['country_code'] = new sfWidgetFormChoice(array('choices' => $countryId,'multiple'=>false));
        $this->widgetSchema['card_type'] = new sfWidgetFormChoice(array('choices' => $arrCardType,'multiple'=>true),array(),array('maxlength'=>50));
        $this->widgetSchema['service'] = new sfWidgetFormChoice(array('choices' => $arrService,'multiple'=>true),array());
        $this->widgetSchema['validation'] = new sfWidgetFormChoice(array('choices' => $arrValidation,'multiple'=>true),array());
        $this->widgetSchema['cart_capacity'] = new sfWidgetFormInput(array(),array('maxlength'=>11,'title'=>'Cart Capacity','class'=>'txt-input'));
        $this->widgetSchema['cart_amount_capacity'] = new sfWidgetFormInput(array(),array('maxlength'=>11,'title'=>'Cart Amount Capacity','class'=>'txt-input'));
        $this->widgetSchema['number_of_transaction'] = new sfWidgetFormInput(array(),array('maxlength'=>20,'title'=>'Number of Transaction','class'=>'txt-input'));
        $this->widgetSchema['transaction_period'] = new sfWidgetFormInput(array(),array('maxlength'=>20,'title'=>'Transaction Period','class'=>'txt-input'));

        $this->setValidators(array(
        'country_code'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Country Code.')),
        'card_type'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Card Type.')),
        'service'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Service.')),
        'validation'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Validation.')),
        'cart_capacity'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Cart Capacity.')),
        'cart_amount_capacity'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Cart Amount Capacity.')),
        'number_of_transaction'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Number of Transaction.')),
        'transaction_period'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Transaction Period.')),
            ));

        $this->widgetSchema->setLabels(array(
          'country_code'    => 'Country<font color=red>*</font>',
          'card_type'    => 'Card Type <font color=red>*</font>',
          'service'   => 'Service<font color=red>*</font>',
          'validation'    => 'Validation <font color=red>*</font>',
          'cart_capacity'   => 'Cart Capacity<font color=red>*</font>',
          'cart_amount_capacity'   => 'Cart Amount Capacity<font color=red>*</font>',
          'number_of_transaction'   => 'Number of Transaction<font color=red>*</font>',
          'transaction_period'   => 'Transaction Period<font color=red>*</font>',
            ));


        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }




}
