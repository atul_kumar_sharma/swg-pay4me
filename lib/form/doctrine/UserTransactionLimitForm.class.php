<?php

/**
 * UserTransactionLimit form.
 *
 * @package    ipay4me
 * @subpackage form
 * @author     Vchandwani
 * @version    SVN: $Id: sfDoctrineFormTemplate.php $
 */
class UserTransactionLimitForm extends BaseUserTransactionLimitForm {

    public function configure() {
        unset(
                $this['created_at'], $this['updated_at'], $this['deleted'], $this['created_by'], $this['updated_by'], $this['user_id']
        );
        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            'user_id' => new sfWidgetFormInputHidden(),
            'Update' => new sfWidgetFormInputHidden(),
            'Save' => new sfWidgetFormInputHidden(),
            'cart_capacity' => new sfWidgetFormInputText(array(), array('class' => 'txt-input', 'maxlength' => 5)),
            'cart_amount_capacity' => new sfWidgetFormInputText(array(), array('class' => 'txt-input', 'maxlength' => 8)),
            'number_of_transaction' => new sfWidgetFormInputText(array(), array('class' => 'txt-input', 'maxlength' => 5)),
            'transaction_period' => new sfWidgetFormInputText(array(), array('class' => 'txt-input', 'maxlength' => 10)),
        ));
        $this->setValidators(array(
            'user_id' => new sfValidatorNumber(array('required'=>false)),
            'Update' => new sfValidatorString(array('required'=>false)),
            'Save' => new sfValidatorString(array('required'=>false)),
            'cart_capacity' => new sfValidatorInteger(array('min' => 0, 'max' => 99,'required'=>false), array('invalid' => 'Invalid Cart Capacity', 'min' => 'Cart Capacity should be greater than 0', 'max' => 'Cart Capacity should be less than or equal to 99')),

            'cart_amount_capacity' => new sfValidatorNumber(array('min' => 0, 'max' => 99999,'required'=>false), array('invalid' => 'Invalid Cart Amount Capacity', 'min' => 'Cart Amount Capacity should be greater than 0', 'max' => 'Cart Amount Capacity should be less than or equal to 99999')),
            'number_of_transaction' => new sfValidatorInteger(array('min' => 0, 'max' => 99,'required'=>false), array('invalid' => 'Invalid Number of Transaction(s)', 'min' => 'Number of Transaction(s) should be greater than 0', 'max' => 'Number of Transaction(s) should be less than or equal to 99')),
            'transaction_period' => new sfValidatorInteger(array('min' => 0, 'max' => 9999999,'required'=>false), array('invalid' => 'Invalid Transaction Period', 'min' => 'Transaction Period should be greater than 0', 'max' => 'Transaction Period should be less than or equal to 999999')),
        ));
//        $this->setValidators(array(
//            'user_id' => new sfValidatorNumber(array('required'=>false)),
//            'Update' => new sfValidatorString(array('required'=>false)),
//            'Save' => new sfValidatorString(array('required'=>false)),
//            'cart_capacity' => new sfValidatorAnd(array(
//                     new sfValidatorRegex(array('pattern' => '/^[0-9]*$/'),array('invalid' => 'First Name is Invalid.','required' => 'First Name is required.')),
//                     new sfValidatorNumber(array('min' => 0, 'max' => 99), array( 'min' => 'Cart Capacity should be greater than 0', 'max' => 'Cart Capacity should be less than or equal to 99'))
//             )),
//
//            'cart_amount_capacity' => new sfValidatorNumber(array('min' => 0, 'max' => 99999,'required'=>false), array('invalid' => 'Invalid Cart Amount Capacity', 'min' => 'Cart Amount Capacity should be greater than 0', 'max' => 'Cart Amount Capacity should be less than or equal to 99999')),
//            'number_of_transaction' => new sfValidatorNumber(array('min' => 0, 'max' => 99,'required'=>false), array('invalid' => 'Invalid Number of Transaction(s)', 'min' => 'Number of Transaction(s) should be greater than 0', 'max' => 'Number of Transaction(s) should be less than or equal to 99')),
//            'transaction_period' => new sfValidatorNumber(array('min' => 0, 'max' => 9999999,'required'=>false), array('invalid' => 'Invalid Transaction Period', 'min' => 'Transaction Period should be greater than 0', 'max' => 'Transaction Period should be less than or equal to 999999')),
//        ));




        $this->widgetSchema->setLabels(array(
            'cart_capacity' => 'Cart capacity',
            'cart_amount_capacity' => 'Cart Amount capacity',
            'number_of_transaction' => 'Allowed Number of Transaction(s)',
            'transaction_period' => 'Transaction Period',
        ));
    }

}
