<?php

/**
 * EwalletPin form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class VerifyMobileNumberForm extends sfForm
{

    public static function getMobileNumber(){
        $user = sfContext::getInstance()->getUser();
        $userId = $user->getGuardUser()->getUserDetail()->getId();
        $userDetails = Doctrine::getTable('UserDetail')->find($userId);
        return $userDetails->getMobilePhone();
    }

   
    public function configure()
    {
        $this->widgetSchema['mobile_number'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Mobile Number','class'=>'txt-input', 'value' => self::getMobileNumber()));
   
        $this->setValidators(array(
        'mobile_number'   => new sfValidatorDate(array('required' => true),array('required' => 'Please enter Mobile Number.')),
            ));
        
        
         $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'isMobileNumberExists')))
            );



        $this->widgetSchema->setLabels(array(
            'mobile_number'    => 'Mobile Number ',
            ));


        $this->widgetSchema->setNameFormat('mobile[%s]');

    }


     function isMobileNumberExists($validator, $values){ 
         $error = '';
         $user = sfContext::getInstance()->getUser();
         $userId = $user->getGuardUser()->getUserDetail()->getId();

    
        $countMobile = Doctrine::getTable('UserDetail')->checkUniqueMobileNumber($userId,$_POST['mobile']['mobile_number']);
        

        if ($countMobile > 0){
            $error = new sfValidatorError($validator,'This Mobile Number already exists.');
            throw new sfValidatorErrorSchema($validator,array('mobile_number' => $error));
        }
    }


}
