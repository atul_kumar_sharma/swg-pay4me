<?php

/**
 * Privilige User Value form.
 *
 * @package    ipay4me
 * @subpackage form
 * @author     Varun
 * @version
 */
class NewPriviligeUserValueForm extends sfForm
{
    public function configure()
    {
        //validation array
        $arrStatus = array('active'=>'Active','Inactive'=>'Inactive');
         //get all card type
        $cardTypeArr = Doctrine::getTable('PaymentMode')->findAll();
        foreach($cardTypeArr as $val){
          $arrCardType[$val->getCardType()] = $val->getDisplayName();
        }
        //get All Services
        $serviceArr = Doctrine::getTable('MidMaster')->findAll();
        foreach($serviceArr as $val){
          $arrService[$val->getMidService()] = $val->getMidService();
        }

        $this->widgetSchema['status'] = new sfWidgetFormChoice(array('choices' => $arrStatus,'multiple'=>false),array('include-custom'=>'Plese select'));
        $this->widgetSchema['card_type'] = new sfWidgetFormChoice(array('choices' => $arrCardType,'multiple'=>true),array(),array('maxlength'=>50));
        $this->widgetSchema['mid_service'] = new sfWidgetFormChoice(array('choices' => $arrService,'multiple'=>true),array());


        $this->setValidators(array(
        'status'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Service.')),
        'mid_service'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Cart Capacity.')),
        'card_type'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Cart Amount Capacity.')),
            ));

        $this->widgetSchema->setLabels(array(
          'status'   => 'Status<font color=red>*</font>',
          'mid_service'   => 'Mid Service<font color=red>*</font>',
          'card_type'   => 'Card Type<font color=red>*</font>',
            ));
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }

}
