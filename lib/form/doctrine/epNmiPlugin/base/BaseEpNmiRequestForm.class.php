<?php

/**
 * EpNmiRequest form base class.
 *
 * @method EpNmiRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpNmiRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'gateway_id'     => new sfWidgetFormInputText(),
      'order_id'       => new sfWidgetFormInputText(),
      'transaction_id' => new sfWidgetFormInputText(),
      'amount'         => new sfWidgetFormInputText(),
      'type'           => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'created_by'     => new sfWidgetFormInputText(),
      'updated_by'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'     => new sfValidatorInteger(array('required' => false)),
      'order_id'       => new sfValidatorString(array('max_length' => 100)),
      'transaction_id' => new sfValidatorPass(array('required' => false)),
      'amount'         => new sfValidatorNumber(array('required' => false)),
      'type'           => new sfValidatorString(array('max_length' => 30)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'created_by'     => new sfValidatorInteger(array('required' => false)),
      'updated_by'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_nmi_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpNmiRequest';
  }

}
