<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class NewSupportUserForm extends BasesfGuardUserForm
{

    public function configure()
    {
        // do unsetting
        unset(
            $this['algorithm'],$this['salt'],$this['is_super_admin'],
            $this['last_login'],$this['updated_at'],$this['created_at'],
            $this['permissions_list'],$this['updated_by'],$this['created_by']
        );

    /*$this->widgetSchema->setLabels(
      array('username'=>'Username *', 'is_active' => 'Desktop Active'));*/

        $this->widgetSchema['password']= new sfWidgetFormInputPassword();
        $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword();

        $groupQuery = sfGuardGroupTable::getInstance()->getGroupQuery();
//        $this->validatorSchema->setPostValidator(
//            new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
//                array(),
//                array('invalid' => 'Password do not match, please try again')
//            ));

        $this->widgetSchema['groups_list']      = new sfWidgetFormDoctrineChoice(array('multiple' => false, 'model' => 'sfGuardGroup','query'=>$groupQuery,"add_empty"=>true,"label"=>"User Group"));
        $this->widgetSchema['groups_list']->setOption("add_empty"," -- Please Select -- ");
        // setup details form now
        $detailsForm = new UserDetailForm($this->getObject()->getUserDetail());

             
    $detailsForm->validatorSchema['first_name']  = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'First Name is required.','max_length'=>'First name can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First Name is Invalid.','required' => 'First Name is required.'))
      ),
      array('halt_on_error' => true),
      array('required' => 'First Name is required')
    );
    $detailsForm->validatorSchema['last_name']  = new sfValidatorAnd(array(
        new sfValidatorString(array('max_length' => 20),array('required' => 'First Name is required.','max_length'=>'First name can not be more than 20 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'First Name is Invalid.','required' => 'First Name is required.'))
      ),
      array('halt_on_error' => false,'required'=>false),
      array('required' => 'First Name is required')
    );

              $detailsForm->validatorSchema['email']         = new sfValidatorEmail(array('required'=>true), array('required'=>'Email is Required','invalid' => 'The email address is invalid.'));
        

        $this->widgetSchema->setLabels(
            array('password'=>'Password', 'username'=>'Username', 'confirm_password'=>'Confirm Password', 'is_active' => 'Desktop Active'));
        $detailsForm->getWidget('first_name')->setLabel('First Name');
        $detailsForm->getWidget('last_name')->setLabel('Last Name');
        $detailsForm->getWidget('email')->setLabel('Email');
        //$detailsForm->getWidget('rank')->setLabel('Rank');
        //$detailsForm->getWidget('service_number')->setLabel('NIS-Service Number');
        $detailsForm->getValidator('first_name')->setOption('required',true);
        $detailsForm->getValidator('email')->setOption('required',true);
        $detailsForm->getValidator('email')->setMessages(array('required'=>"Email is required","invalid"=>"invalid email"));
    $this->validatorSchema['username'] = new sfValidatorAnd(array(
        new sfValidatorString(array('min_length' => 3,'max_length' => 20),array('required' => 'Username is required.','max_length'=>'Username can not be more than 20 characters.','min_length'=>'Username can not be less than 3 characters.')),
        new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')),array('invalid' => 'Invalid Username.','required' => 'Username is required.')),
        new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('username')),array('invalid' => 'Username already exist.'))),
      array('halt_on_error' => true),
      array('required' => 'Username is required')
    );

        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('min_length' => 'Password can be less than 8 characters  .', 'max_length'=>'Password can not  be more than 20 characters.','required' => 'Password is required'));
        $this->validatorSchema['password'] = new sfValidatorCallback(array('callback'  => 'NewSupportUserForm::matchNewAndConfirmPasswordCallBack'));

        $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => true),array('max_length'=>'Confirm password can not  be more than 20 characters.','required' => 'Confirm password is required.'));
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->validatorSchema['groups_list']      = new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'sfGuardGroup', 'required' => true),array("required"=>"Please select User Group"));
        $this->embedForm('details', $detailsForm);

        //$this->renderGlobalErrors();
    }


    public function configureGroups() {
        $this->uiGroup = new Dlform();
        //$this->uiGroup->setLabel('Portal User');

        $newUserDetails = new FormBlock('New User Details');
        $this->uiGroup->addElement($newUserDetails);

        $newUserDetails->addElement(array('details:first_name','details:last_name',
      'details:email','username','password','confirm_password','is_active','groups_list'));

    }

     public static function matchNewAndConfirmPasswordCallBack($validator, $value, $arguments){
    if(!preg_match('/[^\s+]/' ,$_REQUEST['sf_guard_user']['password']) ){
      throw new sfValidatorError($validator, 'White spaces are not allowed.');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) < 8){
      throw new sfValidatorError($validator, 'Minimum 8 characters required.');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) > 20){
      throw new sfValidatorError($validator, 'Password can not be more than 20 characters.');
    }
    if($_REQUEST['sf_guard_user']['password'] != $_REQUEST['sf_guard_user']['confirm_password']){
      throw new sfValidatorError($validator, 'Password and Confirm Password do not match.');
    }
    return $value;

  }
}
