<?php

/**
 * OrderSearch form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderSearchForm extends sfForm
{
    public function configure()
    {
        $this->widgetSchema['order_number'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Order Number','class'=>'txt-input'));

        $this->setValidators(array(
        'order_number'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Order Number.')),
            ));


        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'isOrderNumberExists')))
              );



        $this->widgetSchema->setLabels(array('order_number'    => 'Order Number <font color=red>*</font>',
            ));


        $this->widgetSchema->setNameFormat('order[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }


     function isOrderNumberExists($validator, $values){
        $error = '';
        if($_POST['order']['order_number'] != '' && $_POST['order']['order_number'] > 0){
            $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($_POST['order']['order_number']);
            //echo $_POST['order']['order_number'].'==='.count($ipay4meOrder);
            if (count($ipay4meOrder) > 0){
                $ipay4meOrderNumber = $ipay4meOrder->getFirst()->getOrderNumber();
                $requestId = $ipay4meOrder->getFirst()->getOrderRequestDetailId();

                $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                if($request_details){
                    $orderPaymentStatus = $request_details->getPaymentStatus();
                }
                $gatewayId = $ipay4meOrder->getFirst()->getGatewayId();
                if($_POST['order']['order_number'] == $ipay4meOrderNumber && $orderPaymentStatus == 0) {
                    $error = new sfValidatorError($validator,'Payment has already been made for this order.');
                    throw new sfValidatorErrorSchema($validator,array('order_number' => $error));
                }
                if($_POST['order']['order_number'] == $ipay4meOrderNumber && $gatewayId == 3) {
                    $error = new sfValidatorError($validator,'Payment can not be done for eWallet Order.');
                    throw new sfValidatorErrorSchema($validator,array('order_number' => $error));
                }
            }else{
                $error = new sfValidatorError($validator,'Invalid Order Number.');
                throw new sfValidatorErrorSchema($validator,array('order_number' => $error));
            }
        }
    }


}
