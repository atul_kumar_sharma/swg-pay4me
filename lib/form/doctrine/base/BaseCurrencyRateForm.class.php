<?php

/**
 * CurrencyRate form base class.
 *
 * @method CurrencyRate getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCurrencyRateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'country_id'                 => new sfWidgetFormInputText(),
      'currency'                   => new sfWidgetFormInputText(),
      'passport'                   => new sfWidgetFormInputText(),
      'visa'                       => new sfWidgetFormInputText(),
      'visa_arrival'               => new sfWidgetFormInputText(),
      'additional_charges_for_vap' => new sfWidgetFormInputText(),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
      'created_by'                 => new sfWidgetFormInputText(),
      'updated_by'                 => new sfWidgetFormInputText(),
      'version'                    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'country_id'                 => new sfValidatorPass(),
      'currency'                   => new sfValidatorPass(),
      'passport'                   => new sfValidatorNumber(),
      'visa'                       => new sfValidatorNumber(),
      'visa_arrival'               => new sfValidatorNumber(),
      'additional_charges_for_vap' => new sfValidatorNumber(),
      'created_at'                 => new sfValidatorDateTime(),
      'updated_at'                 => new sfValidatorDateTime(),
      'created_by'                 => new sfValidatorInteger(array('required' => false)),
      'updated_by'                 => new sfValidatorInteger(array('required' => false)),
      'version'                    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('currency_rate[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CurrencyRate';
  }

}
