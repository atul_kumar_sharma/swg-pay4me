<?php

/**
 * MidMaster form base class.
 *
 * @method MidMaster getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMidMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'mid_service'         => new sfWidgetFormInputText(),
      'validation'          => new sfWidgetFormInputText(),
      'visa_amount_limit'   => new sfWidgetFormInputText(),
      'master_amount_limit' => new sfWidgetFormInputText(),
      'cart_amount_limit'   => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'mid_service'         => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'validation'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'visa_amount_limit'   => new sfValidatorInteger(),
      'master_amount_limit' => new sfValidatorInteger(),
      'cart_amount_limit'   => new sfValidatorInteger(),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('mid_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MidMaster';
  }

}
