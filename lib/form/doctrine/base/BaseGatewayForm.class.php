<?php

/**
 * Gateway form base class.
 *
 * @method Gateway getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGatewayForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'name'         => new sfWidgetFormInputText(),
      'display_name' => new sfWidgetFormInputText(),
      'merchant_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'name'         => new sfValidatorString(array('max_length' => 50)),
      'display_name' => new sfValidatorString(array('max_length' => 50)),
      'merchant_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
    ));

    $this->widgetSchema->setNameFormat('gateway[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Gateway';
  }

}
