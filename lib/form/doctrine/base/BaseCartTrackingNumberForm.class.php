<?php

/**
 * CartTrackingNumber form base class.
 *
 * @method CartTrackingNumber getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCartTrackingNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'cart_id'                 => new sfWidgetFormInputText(),
      'tracking_number'         => new sfWidgetFormInputText(),
      'order_number'            => new sfWidgetFormInputText(),
      'order_request_detail_id' => new sfWidgetFormInputText(),
      'cart_amount'             => new sfWidgetFormInputText(),
      'convert_amount'          => new sfWidgetFormInputText(),
      'currency'                => new sfWidgetFormInputText(),
      'user_id'                 => new sfWidgetFormInputText(),
      'associated_date'         => new sfWidgetFormInputText(),
      'status'                  => new sfWidgetFormChoice(array('choices' => array('New' => 'New', 'Associated' => 'Associated', 'Paid' => 'Paid'))),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
      'created_by'              => new sfWidgetFormInputText(),
      'updated_by'              => new sfWidgetFormInputText(),
      'deleted'                 => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'cart_id'                 => new sfValidatorInteger(),
      'tracking_number'         => new sfValidatorString(array('max_length' => 50)),
      'order_number'            => new sfValidatorInteger(array('required' => false)),
      'order_request_detail_id' => new sfValidatorInteger(),
      'cart_amount'             => new sfValidatorNumber(),
      'convert_amount'          => new sfValidatorNumber(),
      'currency'                => new sfValidatorInteger(array('required' => false)),
      'user_id'                 => new sfValidatorInteger(array('required' => false)),
      'associated_date'         => new sfValidatorNumber(),
      'status'                  => new sfValidatorChoice(array('choices' => array(0 => 'New', 1 => 'Associated', 2 => 'Paid'), 'required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
      'created_by'              => new sfValidatorInteger(array('required' => false)),
      'updated_by'              => new sfValidatorInteger(array('required' => false)),
      'deleted'                 => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('cart_tracking_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CartTrackingNumber';
  }

}
