<?php

/**
 * SupportRequestAssignment form base class.
 *
 * @method SupportRequestAssignment getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSupportRequestAssignmentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'request_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'), 'add_empty' => false)),
      'assigned_to'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'assigned_from' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestAssignment'), 'add_empty' => true)),
      'assign_active' => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
      'created_by'    => new sfWidgetFormInputText(),
      'updated_by'    => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'))),
      'assigned_to'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'assigned_from' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestAssignment'), 'required' => false)),
      'assign_active' => new sfValidatorPass(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
      'created_by'    => new sfValidatorInteger(array('required' => false)),
      'updated_by'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('support_request_assignment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SupportRequestAssignment';
  }

}
