<?php

/**
 * ServiceCharge form base class.
 *
 * @method ServiceCharge getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseServiceChargeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                 => new sfWidgetFormInputHidden(),
      'gateway_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'type'               => new sfWidgetFormInputText(),
      'charge_amount'      => new sfWidgetFormInputText(),
      'charge_amount_type' => new sfWidgetFormInputText(),
      'lower_limit'        => new sfWidgetFormInputText(),
      'upper_limit'        => new sfWidgetFormInputText(),
      'created_at'         => new sfWidgetFormDateTime(),
      'updated_at'         => new sfWidgetFormDateTime(),
      'created_by'         => new sfWidgetFormInputText(),
      'updated_by'         => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'type'               => new sfValidatorString(array('max_length' => 20)),
      'charge_amount'      => new sfValidatorNumber(array('required' => false)),
      'charge_amount_type' => new sfValidatorString(array('max_length' => 20)),
      'lower_limit'        => new sfValidatorNumber(array('required' => false)),
      'upper_limit'        => new sfValidatorNumber(array('required' => false)),
      'created_at'         => new sfValidatorDateTime(),
      'updated_at'         => new sfValidatorDateTime(),
      'created_by'         => new sfValidatorInteger(array('required' => false)),
      'updated_by'         => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('service_charge[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ServiceCharge';
  }

}
