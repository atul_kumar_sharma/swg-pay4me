<?php

/**
 * FpsDetail form base class.
 *
 * @method FpsDetail getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseFpsDetailForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'order_number'            => new sfWidgetFormInputText(),
      'order_request_detail_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'), 'add_empty' => false)),
      'gateway_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'), 'add_empty' => false)),
      'ip_address'              => new sfWidgetFormInputText(),
      'user_id'                 => new sfWidgetFormInputText(),
      'card_num'                => new sfWidgetFormInputText(),
      'email'                   => new sfWidgetFormInputText(),
      'trans_date'              => new sfWidgetFormDateTime(),
      'payment_status'          => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'fps_rule_id'             => new sfWidgetFormInputText(),
      'isverified'              => new sfWidgetFormChoice(array('choices' => array('no' => 'no', 'yes' => 'yes'))),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
      'deleted'                 => new sfWidgetFormInputCheckbox(),
      'created_by'              => new sfWidgetFormInputText(),
      'updated_by'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_number'            => new sfValidatorInteger(),
      'order_request_detail_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('OrderRequestDetails'))),
      'gateway_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Gateway'))),
      'ip_address'              => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'user_id'                 => new sfValidatorInteger(array('required' => false)),
      'card_num'                => new sfValidatorPass(array('required' => false)),
      'email'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'trans_date'              => new sfValidatorDateTime(array('required' => false)),
      'payment_status'          => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'fps_rule_id'             => new sfValidatorInteger(),
      'isverified'              => new sfValidatorChoice(array('choices' => array(0 => 'no', 1 => 'yes'), 'required' => false)),
      'created_at'              => new sfValidatorDateTime(),
      'updated_at'              => new sfValidatorDateTime(),
      'deleted'                 => new sfValidatorBoolean(array('required' => false)),
      'created_by'              => new sfValidatorInteger(array('required' => false)),
      'updated_by'              => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fps_detail[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FpsDetail';
  }

}
