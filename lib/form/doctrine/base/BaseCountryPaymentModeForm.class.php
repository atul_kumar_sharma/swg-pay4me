<?php

/**
 * CountryPaymentMode form base class.
 *
 * @method CountryPaymentMode getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCountryPaymentModeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'country_code'          => new sfWidgetFormInputText(),
      'card_type'             => new sfWidgetFormInputText(),
      'service'               => new sfWidgetFormInputText(),
      'validation'            => new sfWidgetFormInputText(),
      'cart_capacity'         => new sfWidgetFormInputText(),
      'cart_amount_capacity'  => new sfWidgetFormInputText(),
      'number_of_transaction' => new sfWidgetFormInputText(),
      'transaction_period'    => new sfWidgetFormInputText(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'country_code'          => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'card_type'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'service'               => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'validation'            => new sfValidatorString(array('max_length' => 25, 'required' => false)),
      'cart_capacity'         => new sfValidatorInteger(),
      'cart_amount_capacity'  => new sfValidatorInteger(),
      'number_of_transaction' => new sfValidatorInteger(),
      'transaction_period'    => new sfValidatorInteger(),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('country_payment_mode[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CountryPaymentMode';
  }

}
