<?php

/**
 * CardCountryList form base class.
 *
 * @method CardCountryList getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCardCountryListForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'bin'           => new sfWidgetFormInputText(),
      'processor_bin' => new sfWidgetFormInputText(),
      'region'        => new sfWidgetFormInputText(),
      'country'       => new sfWidgetFormInputText(),
      'unknown'       => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'bin'           => new sfValidatorInteger(),
      'processor_bin' => new sfValidatorInteger(),
      'region'        => new sfValidatorInteger(),
      'country'       => new sfValidatorString(array('max_length' => 5)),
      'unknown'       => new sfValidatorString(array('max_length' => 5)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('card_country_list[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CardCountryList';
  }

}
