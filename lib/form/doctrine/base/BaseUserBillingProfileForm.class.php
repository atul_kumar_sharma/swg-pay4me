<?php

/**
 * UserBillingProfile form base class.
 *
 * @method UserBillingProfile getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUserBillingProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'user_id'      => new sfWidgetFormInputText(),
      'profile'      => new sfWidgetFormChoice(array('choices' => array('p1' => 'p1', 'p2' => 'p2', 'p3' => 'p3', 'p4' => 'p4', 'p5' => 'p5'))),
      'gateway_id'   => new sfWidgetFormInputText(),
      'first_name'   => new sfWidgetFormInputText(),
      'last_name'    => new sfWidgetFormInputText(),
      'address'      => new sfWidgetFormInputText(),
      'email'        => new sfWidgetFormInputText(),
      'phone'        => new sfWidgetFormInputText(),
      'card_first'   => new sfWidgetFormInputText(),
      'card_last'    => new sfWidgetFormInputText(),
      'card_len'     => new sfWidgetFormInputText(),
      'card_type'    => new sfWidgetFormInputText(),
      'expiry_month' => new sfWidgetFormInputText(),
      'expiry_year'  => new sfWidgetFormInputText(),
      'vault_id'     => new sfWidgetFormInputText(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'      => new sfValidatorInteger(),
      'profile'      => new sfValidatorChoice(array('choices' => array(0 => 'p1', 1 => 'p2', 2 => 'p3', 3 => 'p4', 4 => 'p5'))),
      'gateway_id'   => new sfValidatorInteger(),
      'first_name'   => new sfValidatorString(array('max_length' => 30)),
      'last_name'    => new sfValidatorString(array('max_length' => 30)),
      'address'      => new sfValidatorString(array('max_length' => 255)),
      'email'        => new sfValidatorString(array('max_length' => 100)),
      'phone'        => new sfValidatorString(array('max_length' => 20)),
      'card_first'   => new sfValidatorInteger(),
      'card_last'    => new sfValidatorInteger(),
      'card_len'     => new sfValidatorInteger(array('required' => false)),
      'card_type'    => new sfValidatorPass(array('required' => false)),
      'expiry_month' => new sfValidatorString(array('max_length' => 5)),
      'expiry_year'  => new sfValidatorString(array('max_length' => 10)),
      'vault_id'     => new sfValidatorInteger(),
      'created_at'   => new sfValidatorDateTime(),
      'updated_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('user_billing_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserBillingProfile';
  }

}
