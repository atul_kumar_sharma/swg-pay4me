<?php

/**
 * OrderAuthorize form base class.
 *
 * @method OrderAuthorize getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseOrderAuthorizeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'order_number'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ipay4meOrder'), 'add_empty' => false)),
      'gateway_id'       => new sfWidgetFormInputText(),
      'trans_date'       => new sfWidgetFormDateTime(),
      'authorize_status' => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'response_code'    => new sfWidgetFormInputText(),
      'response_text'    => new sfWidgetFormTextarea(),
      'paid_date'        => new sfWidgetFormDateTime(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'order_number'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ipay4meOrder'))),
      'gateway_id'       => new sfValidatorInteger(),
      'trans_date'       => new sfValidatorDateTime(array('required' => false)),
      'authorize_status' => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'response_code'    => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'response_text'    => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'paid_date'        => new sfValidatorDateTime(),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'OrderAuthorize', 'column' => array('order_number')))
    );

    $this->widgetSchema->setNameFormat('order_authorize[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'OrderAuthorize';
  }

}
