<?php

/**
 * UserTransactionLimit form base class.
 *
 * @method UserTransactionLimit getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseUserTransactionLimitForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'user_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'cart_capacity'         => new sfWidgetFormInputText(),
      'cart_amount_capacity'  => new sfWidgetFormInputText(),
      'number_of_transaction' => new sfWidgetFormInputText(),
      'transaction_period'    => new sfWidgetFormInputText(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
      'deleted'               => new sfWidgetFormInputCheckbox(),
      'created_by'            => new sfWidgetFormInputText(),
      'updated_by'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'cart_capacity'         => new sfValidatorInteger(),
      'cart_amount_capacity'  => new sfValidatorPass(),
      'number_of_transaction' => new sfValidatorInteger(),
      'transaction_period'    => new sfValidatorInteger(),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
      'deleted'               => new sfValidatorBoolean(array('required' => false)),
      'created_by'            => new sfValidatorInteger(array('required' => false)),
      'updated_by'            => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_transaction_limit[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserTransactionLimit';
  }

}
