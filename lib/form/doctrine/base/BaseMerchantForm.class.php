<?php

/**
 * Merchant form base class.
 *
 * @method Merchant getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMerchantForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'name'             => new sfWidgetFormInputText(),
      'merchant_code'    => new sfWidgetFormInputText(),
      'merchant_key'     => new sfWidgetFormInputText(),
      'notification_url' => new sfWidgetFormInputText(),
      'homepage_url'     => new sfWidgetFormInputText(),
      'address'          => new sfWidgetFormInputText(),
      'email'            => new sfWidgetFormInputText(),
      'contact_phone'    => new sfWidgetFormInputText(),
      'description'      => new sfWidgetFormInputText(),
      'abbr'             => new sfWidgetFormInputText(),
      'user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'deleted'          => new sfWidgetFormInputCheckbox(),
      'created_by'       => new sfWidgetFormInputText(),
      'updated_by'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'name'             => new sfValidatorString(array('max_length' => 100)),
      'merchant_code'    => new sfValidatorInteger(),
      'merchant_key'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'notification_url' => new sfValidatorString(array('max_length' => 255)),
      'homepage_url'     => new sfValidatorString(array('max_length' => 255)),
      'address'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'contact_phone'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'description'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'abbr'             => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'user_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'deleted'          => new sfValidatorBoolean(array('required' => false)),
      'created_by'       => new sfValidatorInteger(array('required' => false)),
      'updated_by'       => new sfValidatorInteger(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Merchant', 'column' => array('name')))
    );

    $this->widgetSchema->setNameFormat('merchant[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Merchant';
  }

}
