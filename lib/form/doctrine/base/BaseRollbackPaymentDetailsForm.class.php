<?php

/**
 * RollbackPaymentDetails form base class.
 *
 * @method RollbackPaymentDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseRollbackPaymentDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'rollback_payment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('RollbackPayment'), 'add_empty' => false)),
      'app_id'              => new sfWidgetFormInputText(),
      'app_type'            => new sfWidgetFormInputText(),
      'amount'              => new sfWidgetFormInputText(),
      'action'              => new sfWidgetFormChoice(array('choices' => array('refund' => 'refund', 'chargeback' => 'chargeback', 'refund and block' => 'refund and block'))),
      'status'              => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'rollback_payment_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('RollbackPayment'))),
      'app_id'              => new sfValidatorInteger(),
      'app_type'            => new sfValidatorInteger(),
      'amount'              => new sfValidatorInteger(),
      'action'              => new sfValidatorChoice(array('choices' => array(0 => 'refund', 1 => 'chargeback', 2 => 'refund and block'), 'required' => false)),
      'status'              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('rollback_payment_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'RollbackPaymentDetails';
  }

}
