<?php

/**
 * SupportRequestApplicationDetails form base class.
 *
 * @method SupportRequestApplicationDetails getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseSupportRequestApplicationDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'request_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'), 'add_empty' => false)),
      'app_id'      => new sfWidgetFormInputText(),
      'app_type'    => new sfWidgetFormInputText(),
      'request_for' => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
      'created_by'  => new sfWidgetFormInputText(),
      'updated_by'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SupportRequestList'))),
      'app_id'      => new sfValidatorInteger(),
      'app_type'    => new sfValidatorPass(),
      'request_for' => new sfValidatorPass(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
      'created_by'  => new sfValidatorInteger(array('required' => false)),
      'updated_by'  => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('support_request_application_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SupportRequestApplicationDetails';
  }

}
