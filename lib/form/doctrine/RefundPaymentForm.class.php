<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class RefundPaymentForm extends sfForm {

    public function configure() {

        $reasonArr = Doctrine::getTable('SupportReason')->getParentReason();
        $reasonChoice[''] = "---Please Select Reason---";
        foreach ($reasonArr as $reason) {
            $reasonChoice[$reason['key']] = $reason['key'];
            // $reasonSubChoice[$reason['key']]=$reason['key'];
        }
        $onlyEmbassyCountry[''] = "---Please Select Country---";
       
        $countryArray = Doctrine_Core::getTable('Country')->getOnlyEmbassyCountry();
        foreach ($countryArray as $country) {
            $onlyEmbassyCountry[$country['id']] = $country['country_name'];
            // $reasonSubChoice[$reason['key']]=$reason['key'];
        }
//        echo "<pre>";print_r($onlyEmbassyCountry);die;

        $name = sfContext::getInstance()->getRequest()->getAttribute('name');

        $this->widgetSchema['reason'] = new sfWidgetFormChoice(array('choices' => $reasonChoice), array('onchange' => 'getDescription(),getSubSupportCategory()', 'size' => "5"));
        if (isset($name) && $name != '') {
            $this->setDefault('reason', $name);
            $this->widgetSchema['reason']->setAttribute('disabled', 'disabled');
        }
        $this->widgetSchema['order_number'] = new sfWidgetFormInput(array(), array('maxlength' => 15, 'title' => 'Order Number', 'class' => 'txt-input'));
        $this->widgetSchema['Message'] = new sfWidgetFormTextarea(array(), array('maxlength' => 255, 'title' => 'Message', 'class' => 'txt-input'));
        $this->widgetSchema['date_of_birth'] = new sfWidgetFormDateCal(array('years' => WidgetHelpers::getDateRanges()));

        $this->widgetSchema['processing_country_id'] = new sfWidgetFormChoice(array('choices'=>$onlyEmbassyCountry), array("onChange" => "getEmbassy()"));


        //$this->widgetSchema['processing_embassy_id']  = new sfWidgetFormDoctrineChoice(array('model' => 'EmbassyMaster', 'add_empty' => false));
        $this->widgetSchema['processing_center_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'VisaProcessingCentre', 'add_empty' => false));
        $request = sfContext::getInstance()->getRequest();
        if ($request->hasParameter("countryId")) {
            $countryId = $request->getParameter("countryId");
        }
//        $this->widgetSchema['processing_country_id']->setOption('add_empty', "-- Please Select --");
        $this->widgetSchema['user_proof']   = new sfWidgetFormInputFile(array(),array('class'=>'txt-input','maxlength'=>20,'min_length'=>3));

        //$this->widgetSchema['processing_embassy_id']->setOption('add_empty','-- Please Select --');


//        $this->setValidators(array(
            $this->validatorSchema['reason'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter a reason for Refund.'));
            $this->validatorSchema['order_number'] = new sfValidatorString(array('required' => true), array('required' => 'Please enter Order Number.'));
            $this->validatorSchema['user_proof'] = new sfValidatorFile(array(
                'required' => false,
                'path' => sfConfig::get('sf_upload_dir').'/support_request',
                'mime_types' => array('image/jpeg','image/png','image/gif'),
                'max_size' => '410623',
                    ), array('required' => "Please upload user proof.", "mime_types" => "Please upload JPG/GIF/PNG/PDF images only.", "max_size" => "User proof can not be more then 400 KB."));
        

        $this->validatorSchema['date_of_birth'] = new sfValidatorDate(array('max' => time()), array('required' => 'Date of birth is required.', 'max' => 'Date of birth is invalid'));
        $this->validatorSchema['order_number'] =
                new sfValidatorCallback(array('callback' => 'RefundPaymentForm::isOrderNumberExists'));

        $this->validatorSchema['processing_country_id'] = new sfValidatorString(array('required' => true), array('required' => 'Processing Country is required'));
        $this->validatorSchema['processing_center_id'] = new sfValidatorDoctrineChoice(array('model' => 'VisaProcessingCentre'), array('required' => 'Processing Center is required'));
        $this->validatorSchema['title_id'] = new sfValidatorChoice(array('choices' => array('MR' => 'MR', 'MRS' => 'MRS', 'MISS' => 'MISS', 'DR' => 'DR')), array('required' => 'Title is required.'));
        $this->validatorSchema['first_name'] = new sfValidatorAnd(array(
                    new sfValidatorString(array('max_length' => 20), array('required' => 'First Name is required.', 'max_length' => 'First name can not be more than 20 characters.')),
                    new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')), array('invalid' => 'First Name is Invalid.', 'required' => 'First Name is required.'))
                        ),
                        array('halt_on_error' => true),
                        array('required' => 'First Name is required')
        );
        $this->validatorSchema['mid_name'] = new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern'), 'max_length' => 20,
                    'required' => false), array('max_length' => 'Middle Name can not  be more than 20 characters.',
                    'invalid' => 'Middle name is invalid.'));
        $this->validatorSchema['last_name'] = new sfValidatorAnd(array(
                    new sfValidatorString(array('max_length' => 20), array('required' => 'Last Name (<i>Surname</i>) is required.', 'max_length' => 'Last name (<i>Surname</i>) can not be more than 20 characters.')),
                    new sfValidatorRegex(array('pattern' => sfConfig::get('app_name_exception_pattern')), array('invalid' => 'Last Name (<i>Surname</i>) is Invalid.', 'required' => 'Last Name (<i>Surname</i>) is required.'))
                        ),
                        array('halt_on_error' => true),
                        array('required' => 'Last Name (<i>Surname</i>) is required')
        );
       
        $this->widgetSchema->setLabels(array(
            'reason' => 'Support Category<font color=red>*</font>',
            'title_id' => 'Title',
            'first_name' => 'First name ',
            'last_name' => 'Last name (<i>Surname</i>) ',
            'mid_name' => 'Middle name',
            'sub_reason' => 'Support Sub Category<font color=red>*</font>',
            'order_number' => 'Duplicate Order Number <font color=red>*</font>',
            'Message' => 'Message <font color=red>*</font>',
            'date_of_birth' => 'Date of birth (dd-mm-yyyy)<font color=red>*</font>',
            'user_proof' => 'User Proof',
        ));


        $this->widgetSchema->setNameFormat('order[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);
    }

    function isOrderNumberExists($validator, $values, $arguments) {
        $error = '';
        if ($_POST['order']['order_number'] != '' && $_POST['order']['order_number'] > 0) {
            $ipay4meOrder = Doctrine::getTable('ipay4meOrder')->findByOrderNumber($_POST['order']['order_number']);
            //echo $_POST['order']['order_number'].'==='.count($ipay4meOrder);
            if (count($ipay4meOrder) > 0) {
                $ipay4meOrderNumber = $ipay4meOrder->getFirst()->getOrderNumber();
                $requestId = $ipay4meOrder->getFirst()->getOrderRequestDetailId();

                $request_details = Doctrine::getTable('OrderRequestDetails')->find($requestId);
                if ($request_details) {
                    $orderPaymentStatus = $request_details->getPaymentStatus();
                }
                $gatewayId = $ipay4meOrder->getFirst()->getGatewayId();
                if ($_POST['order']['order_number'] == $ipay4meOrderNumber && $orderPaymentStatus == 0) {
                    $error = new sfValidatorError($validator, 'Payment has already been made for this order.');
                    throw new sfValidatorErrorSchema($validator, array('order_number' => $error));
                }
                if ($_POST['order']['order_number'] == $ipay4meOrderNumber && $gatewayId == 3) {
                    $error = new sfValidatorError($validator, 'Payment can not be done for eWallet Order.');
                    throw new sfValidatorErrorSchema($validator, array('order_number' => $error));
                }
            } else {
                $error = new sfValidatorError($validator, 'Invalid Order Number.');
                throw new sfValidatorErrorSchema($validator, array('order_number' => $error));
            }
        }
    }

   
}

?>
