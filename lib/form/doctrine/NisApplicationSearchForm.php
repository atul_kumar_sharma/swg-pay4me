<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class NisApplicationSearchForm extends sfForm
{

//  static public $application_types = array(
//    ''=> 'Please select',
//    'v' => 'Visa/Free Zone ',
//    'p' => 'Passport',
//    'vap' => 'Visa on Arrival&nbsp;&nbsp;'
//  );
    public function configure()
    {
       if(sfConfig::get('app_visa_arrival_enable')=='true')
        {
            $appArray = array(
            ''=> 'Please select',
            'v' => 'Visa/Free Zone ',
            'p' => 'Passport',
            'vap' => 'Visa on Arrival&nbsp;&nbsp;'
            );
        } else {
            $appArray = array(
            ''=> 'Please select',
            'v' => 'Visa/Free Zone ',
            'p' => 'Passport');
        }

      sfWidgetFormSchema::setDefaultFormFormatterName('dl');

     $this->setWidgets(array(

                'application_type'   => new sfWidgetFormChoice(array('choices' => $appArray)),
                'application_id'=> new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>10)),

            ));



        $this->setValidators(array(

                'application_type'    =>new sfValidatorString(array('required' => true),array('required' =>'Pelese select Application Type')),
                'application_id' => new sfValidatorNumber(array('min'=>1,'required'=>true),array('invalid'=>'Please enter valid Application Id','required'=>'Please enter Application Id','min'=>'Please enter valid Application Id ')),
//                'application_id'  =>new sfValidatorString(array('required' => true),array('required' => 'Please enter Application Id')),
            ));

        

        $this->widgetSchema->setLabels(array(
                'application_type'=>'Application Type <font color=red>*</font>',
                'application_id'    => 'Application Id <font color=red>*</font>',

            ));

        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

          $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'verifySearch'))),

                ))

        );


       $this->widgetSchema->setNameFormat('search[%s]');



     
     }

     private function isAppProcessed($application_id, $application_type)
     {
         $arrIPay4MeRequest = CartItemsTransactionsTable::getInstance()->getPaymentRequest($application_id, $application_type);

         $arrItemId = array();
         for($i=0;$i<count($arrIPay4MeRequest);$i++)
         {
             $arrItemId[$i] = $arrIPay4MeRequest[$i]['id'];
         }
         
         $arrCartDetail = array();
         if(!empty($arrItemId))
         {
            $arrCartDetail = IPaymentRequestTable::getInstance()->getCartDetails($arrItemId);
         }

         $arrCartIds = array();
         for($j=0;$j<count($arrCartDetail);$j++)
         {
             $arrCartIds[$j] = $arrCartDetail[$j]['cart_id'];
         }

         $arrTrackingDeatil = CartTrackingNumberTable::getInstance()->getTrackingNumber($arrCartIds);
         if(!empty($arrTrackingDeatil))
         {
             //if not empty means app is processed
             return true;
         }
         else
         {
             //if not empty means app is processed
             $arrTrackingDeatilWT = WireTrackingNumberTable::getInstance()->getTrackingNumber($arrCartIds);
             if(!empty($arrTrackingDeatilWT))
             {
                 //if not empty means app is processed
                 return true;
             }
             else
             {
                return false;
             }
         }
     }

        public function verifySearch($validator, $values) {
        $err = '';
        $application_type =  $values['application_type'];
        $application_id = $values['application_id'];

        if(($application_type!="") && ($application_id!="")) {
          if($application_type == 'p') {
            $table = "PassportApplication";
          }
          else if ($application_type == 'v') {
            $table = "VisaApplication";
          }
          else if ($application_type == 'vap') {
            $table = "VapApplication";
        }
        }
        if ((!$application_type) || (!$application_id)) {
          return $values;
        }
//
//        if($application_type == "") {
//          $error = new sfValidatorError($validator,'Please select Application type');
//          throw new sfValidatorErrorSchema($validator,array('application_type' => $error));
//        }
//
//        if($application_id == "") {
//          $error = new sfValidatorError($validator,'Please select application Id');
//          throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
//        }

        $app = Doctrine::getTable($table)->find($application_id);
        if(!$app) {
          $error = new sfValidatorError($validator,'Application not found');
          throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
        }
        
        
        //  Commented by aswani as all tracking checks are moving to when user try to generate tracking number... *
        //start - by vineet to add new validation for Money order
        if($this->isAppProcessed($application_id, $application_type))
        {
            $error = new sfValidatorError($validator,'Application has already been associated with money order.');
            throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
        }
        //end - by vineet to add new validation for Money order

         //* End Here... **/
            
        $payment_status = $app->getIspaid();
        if($payment_status) {
          $error = new sfValidatorError($validator,'Application has already been paid');
          throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
        }

        

          ## this is updated by ashwani kumar bug ID :34864
          ## $isRolledBack = RollbackPaymentDetailsTable::getInstance()->findByAppId($application_id);
          ## if($isRolledBack->count()) {
          $RollbackAction = RollbackPaymentDetailsTable::getInstance()->isRefunded($application_id, $application_type);
          if(count($RollbackAction) > 0){
            $error = new sfValidatorError($validator,'<br />This application can not be added as '.$RollbackAction[0]['action'].' is already initiated for this application');
            ## throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
          }
          ## }

        /**
         * [WP: 097] => CR: 139 
         */
        if(strtolower($application_type) == 'v'){
          $nairaPayCountry = sfconfig::get('app_nigeriaOnlyCountries_Visa');
        }else if(strtolower($application_type) == 'p'){
          $nairaPayCountry = sfconfig::get('app_nigeriaOnlyCountries_Passport');
        }else if(strtolower($application_type) == 'vap'){
          $nairaPayCountry = sfconfig::get('app_nigeriaOnlyCountries_Vap');
        }else{
          $nairaPayCountry = sfconfig::get('app_nigeriaOnlyCountries');
        }

        if($application_type == 'v'){
          if(VisaCategoryTable::getInstance()->isFreshVisa($app->getVisacategoryId())){
            $processingCountry = $app->getVisaApplicantInfo()->getApplyingCountryId();
          }else{
            if($app->getVisacategoryId() == VisaCategoryTable::getInstance()->getReEntryId()){
               $error = new sfValidatorError($validator,"Invalid Application");
              throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
              return   $values;
            }else{
              $processingCountry = "IN";
            }
          }
          $nationality = $app->getPresentNationalityId();
        }else if($application_type == "p"){
          $processingCountry = $app->getProcessingCountryId();
        }else if($application_type == "vap"){
          $processingCountry = $app->getApplyingCountryId();
        }
//        echo $processingCountry;
//        echo "<pre>";print_r($nairaPayCountry);die;
        if(in_array($processingCountry, $nairaPayCountry)){
          $error = new sfValidatorError($validator,"Invalid Application");
          throw new sfValidatorErrorSchema($validator,array('application_id' => $error));
        }

        return $values;
    }
}

?>