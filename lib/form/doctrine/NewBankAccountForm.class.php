<?php

/**
 * NisDollarRevenue form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NewBankAccountForm extends sfForm
{
    public function configure()
    {


        $account_name = sfContext::getInstance()->getRequest()->getAttribute('account_name');
        $account_number = sfContext::getInstance()->getRequest()->getAttribute('account_number');
        $bank_name = sfContext::getInstance()->getRequest()->getAttribute('bank_name');
        $city = sfContext::getInstance()->getRequest()->getAttribute('city');
        $state = sfContext::getInstance()->getRequest()->getAttribute('state');
        $country = sfContext::getInstance()->getRequest()->getAttribute('country');


        
        $this->widgetSchema['account_name'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Account Name','class'=>'txt-input'));
        $this->widgetSchema['account_number'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Account Number','class'=>'txt-input'));
        $this->widgetSchema['bank_name'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Bank Name','class'=>'txt-input'));
        $this->widgetSchema['city'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'City','class'=>'txt-input'));
        $this->widgetSchema['state'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'State','class'=>'txt-input'));
        $this->widgetSchema['country'] = new sfWidgetFormInput(array(),array('maxlength'=>15,'title'=>'Country','class'=>'txt-input'));
        // $this->widgetSchema['holiday'] =  new sfWidgetFormInputCheckbox(array(),array());

        $this->setValidators(array(
                //'holiday'            => new sfValidatorBoolean(array('required'=>true),array('required'=>'Please agree to Terms & Conditions')),
        'account_name'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Account Name.')),
        'account_number'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Account Number.')),
        'bank_name'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Bank Name.')),
        'city'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Order City.')),
        'state'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Order State.')),
        'country'   => new sfValidatorString(array('required' => true),array('required' => 'Please enter Order country.')),
            ));

        $this->setDefault('account_name',$account_name);
        $this->setDefault('account_number',$account_number);
        $this->setDefault('bank_name',$bank_name);
        $this->setDefault('city',$city);
        $this->setDefault('state',$state);
        $this->setDefault('country',$country);
        
        //$this->validatorSchema['date'] =   new sfValidatorDate(array('max' => time()),array('required' => 'Date of birth is required.','max'=>'Date of birth is invalid'));





        $this->widgetSchema->setLabels(array(
          'account_name'    => 'Account Name <font color=red>*</font>',
          'account_number'    => 'Account Number <font color=red>*</font>',
          'bank_name'   => 'Bank Name<font color=red>*</font>',
          'city'    => 'City <font color=red>*</font>',
          'state'   => 'State<font color=red>*</font>',
          'country'   => 'Country<font color=red>*</font>',
            ));


        $this->widgetSchema->setNameFormat('bank[%s]');
        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }




}
