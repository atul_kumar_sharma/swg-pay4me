<?php
class ActionGatewayResponseForm extends sfForm
{
    public function configure()
    {

     sfProjectConfiguration::getActive()->loadHelpers('Asset');
     $this->setWidgets(array(
              
                'gateway'   => new sfWidgetFormDoctrineChoice(array('model' => 'Gateway', 'add_empty' => 'Please select Gateway')),
                'startDate'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
                'endDate'=>new widgetFormDateTimeCal(array(),array('maxlength'=>30,'readonly'=>'true','class'=>'txt-input')),
             
            ));

        $this->widgetSchema->setNameFormat('report[%s]');


        $this->setValidators(array(

                'gateway'    =>new sfValidatorDoctrineChoice(array('model' => 'Gateway'),array('required' =>'Pelese select Gateway')),
                'startDate'  =>new sfValidatorString(array('required' => true),array('required' => 'Please enter Start Date.')),
                'endDate'    =>new sfValidatorString(array('required' => true),array('required' => 'Please enter End Date.'))
            ));

        if(isset($_REQUEST['report']['endDate']) && $_REQUEST['report']['endDate']!=""){
            $this->validatorSchema->setPostValidator(

               new sfValidatorSchemaCompare('startDate', sfValidatorSchemaCompare::LESS_THAN_EQUAL, 'endDate',
                    array('throw_global_error' => true),
                    array('invalid' => 'The start date (%left_field%) cannot be greater than end date (%right_field%)')
                ));
        }

        $this->widgetSchema->setLabels(array(
                'gateway'=>'Gateway <font color=red>*</font>',
                'startDate'    => 'Start Date <font color=red>*</font>',
                'endDate' => 'End Date <font color=red>*</font>'

            ));

        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    }
}
?>
