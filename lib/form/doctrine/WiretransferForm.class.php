<?php

/**
 * Wiretransfer form.
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class WiretransferForm extends BaseWiretransferForm
{
  public function configure()
  {

    unset(
      $this['created_at'],$this['created_by'],$this['updated_at'],$this['updated_by'],$this['paid_date'],
      $this['phone'],$this['address'],$this['wire_transfer_office'],$this['paid_date']
    );
    $this->widgetSchema['wire_transfer_number'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>30,'min_length'=>3,'autocomplete'=>'off'));
    $this->widgetSchema['re_wire_transfer_number'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>30,'min_length'=>3,'autocomplete'=>'off'));
    $this->widgetSchema['amount'] =  new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    $this->widgetSchema['wire_transfer_date'] =  new sfWidgetFormDateCal(array(),array('class'=>'txt-input','maxlength'=>32,'min_length'=>3));
    $this->validatorSchema['amount'] = new sfValidatorInteger(array('required'=>true,'min'=>1,'max'=>1000000),array('required'=>'Wire Transfer number is required','min'=>'Amount cannot be less than $1.','max'=>'Amount cannot be more than $1000000'));
    $this->validatorSchema['wire_transfer_date'] = new sfValidatorDate(array('required'=>true,'max'=>time()),array('required'=>'Wire Transfer issuing date is required','max'=>'Wire Transfer issuing date can not be future date'));
    

    $this->validatorSchema['wire_transfer_number'] =
     new sfValidatorAnd(array(
         new sfValidatorRegex(array('required'=>true,'pattern' => '/^[\-0-9a-zA-Z]+$/','required'=>true),array('required'=>'Wire Transfer number is required','invalid' => 'White spaces and special characters are not allowed.')),
         new sfValidatorCallback(array('required'=>true,
        'callback'  => 'WiretransferForm::findDuplicateWT')
      )))
      ;
    $this->validatorSchema['re_wire_transfer_number'] =
     new sfValidatorAnd(array(
         new sfValidatorRegex(array('required'=>true,'pattern' => '/^[\-0-9a-zA-Z]+$/','required'=>true),array('required'=>'Wire Transfer number is required','invalid' => 'White spaces and special characters are not allowed.')),
         new sfValidatorCallback(array('required'=>true,
        'callback'  => 'WiretransferForm::findDuplicateWT')
      )))
      ;
    
    /**
     *
     *
     * Set label
     */
    $this->widgetSchema->setLabels(array(
         'wire_transfer_number'    => 'Wire Transfer Number',
         're_wire_transfer_number'    => 'Confirm Wire Transfere Number',
         'amount'    => 'Wire Transfer Amount',
         'paid_date'    => 'Wire Transfer paid date',
         'address'    => 'Address',
         'phone'    => 'Phone Number',
         'wire_transfer_office'    => 'Wire Transfer Office',
         'wire_transfer_proof'      => 'Wire Transfer Receipt',
      )
    );

     $this->widgetSchema['wire_transfer_proof'] = new sfWidgetFormInputFile(array(),array('class'=>'txt-input'));


     $userID = $this->getOption('userId');

     ##$userID = sfContext::getInstance()->getUser()->getGuarduser()->getId();
     $wire_transfer_receipt = sfConfig::get('sf_upload_dir').'/wire_transfer_receipt/';

      ## Creating Path if not exist...
      if(!file_exists($wire_transfer_receipt)){
         mkdir($wire_transfer_receipt);
         chmod($wire_transfer_receipt, 0777);
      }//End of if(!file_exists($address_scan)){...
      $path = $wire_transfer_receipt.$userID.'/';
      ## Creating Path if not exist...
      if(!file_exists($path)){
         mkdir($path);
         chmod($path, 0777);
      }//End of if(!file_exists($path)){...
      

     $this->validatorSchema['wire_transfer_proof']  = new sfValidatorFile(array(
                            'required'   => true,
                            'path'       => $path,
                            'mime_types' => 'web_images',
                            'max_size' => '513000',
                            ),array("required" => "Wire Transfer receipt required.", "mime_types"=>"Please upload JPG/GIF/PNG images only - (DO NO UPLOAD IMAGE LARGER THAN 500 KB).","max_size"=>"<br/>Wire Transfer receipt can not be more than 500 KB."));



     $this->validatorSchema->setOption('allow_extra_fields', true);
  }

  public static function findDuplicateWT($validator, $value){

    $wireTransferNumber =  $_REQUEST['wiretransfer']['wire_transfer_number'];
    $rewireTransferNumber =  $_REQUEST['wiretransfer']['re_wire_transfer_number'];

    if($wireTransferNumber != $rewireTransferNumber){
        throw new sfValidatorError($validator, 'Wire Transfer serial number and Confirm Wire Transfer serial number do not match.');
    }else{
        $isWireTransferNumber = WiretransferTable::getInstance()->findByWireTransferNumber($wireTransferNumber);        
        if(count($isWireTransferNumber) > 0){
          throw new sfValidatorError($validator, 'The Wire Transfer Serial Number entered is already used');
        }
        else
        {
          return $value;
        }
    }

  }
}
