<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/
class RechargeForm extends BaseRequestForm
{
    public function configure()
    {
        $this->setWidgets(array(
        'rechargeAmt'  => new sfWidgetFormInputText(array(),array('class'=>'txt-input','maxlength'=>9)),
            ));

        $this->setValidators(array(
         'rechargeAmt' => new sfValidatorNumber(array('min'=>0.01,'required'=>true),array('invalid'=>'Please enter valid amount','required'=>'Please enter amount','min'=>'Amount should be greater than "0" ')),
            ));

        $this->widgetSchema->setLabels(array(
        'rechargeAmt'=> 'Amount',
            ));
        $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'verifyRechargeAmount')))

                ))

        );

        $this->widgetSchema->setNameFormat('recharge[%s]');
    }
    public function verifyRechargeAmount($validator, $values) {
        $err = '';
        $uid =  sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $ewalletManagerObj = new EwalletManager();
        $rechargeLimit = $ewalletManagerObj->getRechargeLimit($uid);       
        $transactionCharge =  $values['rechargeAmt'];
        $maxRechargeLimit = $rechargeLimit['amount'];

        if($transactionCharge<=$maxRechargeLimit){
            if(strstr($transactionCharge,'.')){
            $tran = explode('.',$transactionCharge);
            if( strlen($tran[1])>=3){
                $error = new sfValidatorError($validator,'There should be two digits after decimal point');
                throw new sfValidatorErrorSchema($validator,array('rechargeAmt' => $error));
            }}
        }else{
            $error = new sfValidatorError($validator,'Amount should not be greater than '.number_format($maxRechargeLimit));
            throw new sfValidatorErrorSchema($validator,array('rechargeAmt' => $error));
        }


        return $values;
    }
}
?>
