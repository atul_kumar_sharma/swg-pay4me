<?php

/**
 * EpGrayPayCaptureData form base class.
 *
 * @method EpGrayPayCaptureData getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayCaptureDataForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'gateway_id'    => new sfWidgetFormInputText(),
      'transactionid' => new sfWidgetFormInputText(),
      'orderid'       => new sfWidgetFormInputText(),
      'amount'        => new sfWidgetFormInputText(),
      'response_text' => new sfWidgetFormInputText(),
      'response_code' => new sfWidgetFormInputText(),
      'midService'    => new sfWidgetFormInputText(),
      'is_captured'   => new sfWidgetFormChoice(array('choices' => array('no' => 'no', 'yes' => 'yes', 'success' => 'success', 'fail' => 'fail'))),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'    => new sfValidatorInteger(),
      'transactionid' => new sfValidatorString(array('max_length' => 30)),
      'orderid'       => new sfValidatorString(array('max_length' => 20)),
      'amount'        => new sfValidatorPass(),
      'response_text' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'response_code' => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'midService'    => new sfValidatorString(array('max_length' => 4, 'required' => false)),
      'is_captured'   => new sfValidatorChoice(array('choices' => array(0 => 'no', 1 => 'yes', 2 => 'success', 3 => 'fail'), 'required' => false)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_gray_pay_capture_data[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayCaptureData';
  }

}
