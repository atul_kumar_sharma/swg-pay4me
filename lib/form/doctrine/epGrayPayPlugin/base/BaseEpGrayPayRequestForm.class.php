<?php

/**
 * EpGrayPayRequest form base class.
 *
 * @method EpGrayPayRequest getObject() Returns the current form's model object
 *
 * @package    ama
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpGrayPayRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'gateway_id'    => new sfWidgetFormInputText(),
      'tran_date'     => new sfWidgetFormDateTime(),
      'request_ip'    => new sfWidgetFormInputText(),
      'amount'        => new sfWidgetFormInputText(),
      'card_first'    => new sfWidgetFormInputText(),
      'card_last'     => new sfWidgetFormInputText(),
      'card_len'      => new sfWidgetFormInputText(),
      'card_type'     => new sfWidgetFormInputText(),
      'cvv'           => new sfWidgetFormInputText(),
      'card_holder'   => new sfWidgetFormInputText(),
      'expiry_month'  => new sfWidgetFormInputText(),
      'expiry_year'   => new sfWidgetFormInputText(),
      'address1'      => new sfWidgetFormInputText(),
      'address2'      => new sfWidgetFormInputText(),
      'city'          => new sfWidgetFormInputText(),
      'state'         => new sfWidgetFormInputText(),
      'zip'           => new sfWidgetFormInputText(),
      'country'       => new sfWidgetFormInputText(),
      'first_name'    => new sfWidgetFormInputText(),
      'last_name'     => new sfWidgetFormInputText(),
      'email'         => new sfWidgetFormInputText(),
      'phone'         => new sfWidgetFormInputText(),
      'order_number'  => new sfWidgetFormInputText(),
      'ip_address'    => new sfWidgetFormInputText(),
      'transactionid' => new sfWidgetFormInputText(),
      'type'          => new sfWidgetFormInputText(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'gateway_id'    => new sfValidatorInteger(array('required' => false)),
      'tran_date'     => new sfValidatorDateTime(array('required' => false)),
      'request_ip'    => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'amount'        => new sfValidatorPass(),
      'card_first'    => new sfValidatorInteger(),
      'card_last'     => new sfValidatorInteger(),
      'card_len'      => new sfValidatorInteger(array('required' => false)),
      'card_type'     => new sfValidatorPass(array('required' => false)),
      'cvv'           => new sfValidatorString(array('max_length' => 5, 'required' => false)),
      'card_holder'   => new sfValidatorString(array('max_length' => 255)),
      'expiry_month'  => new sfValidatorString(array('max_length' => 5)),
      'expiry_year'   => new sfValidatorString(array('max_length' => 10)),
      'address1'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address2'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'city'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'state'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'zip'           => new sfValidatorString(array('max_length' => 15, 'required' => false)),
      'country'       => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'first_name'    => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'last_name'     => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'email'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone'         => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'order_number'  => new sfValidatorInteger(),
      'ip_address'    => new sfValidatorString(array('max_length' => 20)),
      'transactionid' => new sfValidatorString(array('max_length' => 30)),
      'type'          => new sfValidatorString(array('max_length' => 30)),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'EpGrayPayRequest', 'column' => array('order_number')))
    );

    $this->widgetSchema->setNameFormat('ep_gray_pay_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpGrayPayRequest';
  }

}
