<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EditFirstProfileForm extends BasesfGuardUserForm {
  public static function matchNewAndConfirmPasswordCallBack($validator, $value, $arguments){
    if(!preg_match('/[^\s+]/' ,$_REQUEST['sf_guard_user']['password']) ){
      throw new sfValidatorError($validator, 'White spaces are not allowed');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) < 8){
      throw new sfValidatorError($validator, 'Minimum 8 characters required');
    }
    if(strlen($_REQUEST['sf_guard_user']['password']) > 20){
      throw new sfValidatorError($validator, 'Password can not be more than 20 characters');
    }
    if($_REQUEST['sf_guard_user']['password'] != $_REQUEST['sf_guard_user']['confirm_password']){
      throw new sfValidatorError($validator, 'New Password and Confirm Password do not match');
    }
    return $value;

  }
  public function configure() {

  // do unsetting
    unset(
        $this['algorithm'],$this['salt'],$this['is_super_admin'],
        $this['updated_at'],$this['created_at'],
        $this['groups_list'],$this['permissions_list'],$this['is_active']
    );

    $userGroup = sfContext::getInstance()->getUser()->getGroupNames();

    $this->widgetSchema['last_login']= new sfWidgetFormInputHidden();

    //$this->validatorSchema['last_login'] = new sfValidatorString(array('required'=>false));
      $this->widgetSchema['password']= new sfWidgetFormInputPassword(array(),array('class'=>'txt-input','title'=>'Password','maxlength'=>20, 'autocomplete' => 'off','accept' => 'blank'));
   // if(!in_array(sfConfig::get('app_pfm_role_ewallet_user'),$userGroup)) { //ewallet user will no be prompted to change his password

        $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword(array(),array('maxlength'=>10, 'autocomplete' => 'off'));
       // $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 8,'required' => true),array('max_length' => 'Password can not be more than 20 characters', 'min_length'=>'Password should be atleast 8 characters'));
        

        $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => true));
    //}
//    $this->validatorSchema['password'] = new sfValidatorString(array('required' => true));
//    $this->validatorSchema['password'] = new sfValidatorCallback(array(
//        'callback'  => 'EditFirstProfileForm::matchNewAndConfirmPasswordCallBack'));
//        $this->validatorSchema['old_pin']->setOption('required',true);
//        $this->validatorSchema['old_pin']->setMessage('required','Current Pin is required');
        $this->validatorSchema['password'] = new sfValidatorAnd(array(new sfValidatorString(array( 'required' => true)),
        new sfValidatorCallback(array(
        'callback'  => 'EditFirstProfileForm::matchNewAndConfirmPasswordCallBack'))
            ));
//   $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//          new sfValidatorCallback(array('callback' => array($this, 'matchNewAndConfirmPasswordCallBack')))
//        )
//      ));
  


    $this->widgetSchema->setLabels(array('password'=>'New Password', 'confirm_password'=>'Confirm New Password'));

    if($this->offsetExists('username'))
    {
        $this->widgetSchema['username']->setLabel('Username');
    }

    $attr = array( 'onpaste'=>"return false",'ondrop'=>"return false", 'ondrag'=>"return false", 'oncopy'=>"return false", 'autocomplete'=>"off",'class'=>'txt-input','title'=>'Confirm Password','accept' => 'blank');


//    if(!in_array(sfConfig::get('app_pfm_role_ewallet_user'),$userGroup)) { //ewallet user will no be prompted to change his password
        $this->widgetSchema['confirm_password']->setAttributes($attr);
//      $this->validatorSchema->setPostValidator(
//          new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
//          array(),
//          array('invalid' => 'Password do not match, please try again')
//      ));

      #set defaults
      $this->getWidget('password')->setDefault(NULL);
      $this->object->setPassword('');
  //  }
    // setup details form now
    //$detailsForm = new UserDetailsForm($this->getObject()->getUserDetails());
    //    $detailsForm->getWidget('rank')->setLabel('Rank');
    //    $detailsForm->getWidget('service_number')->setLabel('NIS-Service Number');
    if($this->offsetExists('username'))
    {
      $attrb = array( 'readonly'=>'readonly','class'=>'txt-input','title'=>'username');
      $this->widgetSchema['username']->setAttributes($attrb);
      $this->validatorSchema['username'] = new sfValidatorString(array('required' => false));
    }    
    //    $detailsForm->getWidget('email')->setAttribute('readonly','readonly');
    //$detailsForm->getWidget('first_name')->setAttribute('readonly','readonly');
    //$detailsForm->getWidget('last_name')->setAttribute('readonly','readonly');
    //$titleWidget = new sfWidgetFormInput(array(), array('readonly'=>'readonly'));
    //$detailsForm->setWidget('title', $titleWidget);

    // $this->embedForm('details', $detailsForm);
    
    $this->widgetSchema->setHelps(array(
            'password'  => ' <br />Password Strength <span id="strength"></span>'
        ));



  }
  public function configureGroups() {
    $this->uiGroup = new Dlform();
    $userGroup = sfContext::getInstance()->getUser()->getGroupNames();
    if($this->offsetExists('username'))
    {
        $newUserDetails = new FormBlock('Update your credentials');
        $this->uiGroup->addElement($newUserDetails);
       // if(in_array(sfConfig::get('app_pfm_role_ewallet_user'),$userGroup)) {//ewallet user will no be prompted to change his password
//          $newUserDetails->addElement(array('username'));
//        }
//        else {
          $newUserDetails->addElement(array('username','password','confirm_password'));
//        }
    }
  }

}