<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class EditUserForm extends BasesfGuardUserForm
{
  protected function doSave($con = null)
  {
    parent::doSave($con);

//    $this->savecategoryList($con);

  }

  public function savecategoryList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->embeddedForms['details']->widgetSchema['category_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (is_null($con))
    {
      $con = $this->getConnection();
    }
    $obj = $this->object->UserDetails;
    $existing = $obj->category->getPrimaryKeys();
    $vl = $this->getValue('details');
    $values = $vl['category_list'];
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $obj->unlink('category', array_values($unlink));

    }
    $link = array_diff($values, $existing);
    if (count($link))
    {
        $obj->link('category', array_values($link));
    }
  }
    public function configure()
  {
    // do unsetting
    unset(
      $this['algorithm'],$this['salt'],$this['is_super_admin'],
      $this['last_login'],$this['updated_at'],$this['created_at'],
      $this['groups_list'],$this['permissions_list']
    );

    $this->widgetSchema->setLabels(
      array( 'is_active' => 'Is Active'));

    $this->widgetSchema['password']= new sfWidgetFormInputPassword(array(),array('title'=>'Password','maxlength'=>10, 'autocomplete' => 'off','accept' => 'blank'));
    $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword(array(),array('title'=>'Confirm Password','maxlength'=>10, 'autocomplete' => 'off','accept' => 'blank'));
    $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20,'required' => false),array('max_length' => 'Password can not be more than 20 characters'));
    $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'required' => false),array('max_length' => 'Password can not be more than 20 characters'));

    $this->widgetSchema->setLabels(
      array('password'=>'Password', 'confirm_password'=>'Confirm Password', 'username'=>'Username'));

    $this->validatorSchema->setPostValidator(
      new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
        array(),
        array('invalid' => 'Password do not match, please try again')
      ));

    $this->widgetSchema['username']->setAttribute('readonly','readonly');
    $this->validatorSchema['username']->setOption('required',false);
    // setup details form now
    $detailsForm = new UserDetailsForm($this->getObject()->getUserDetails());
//    echo "<pre>";
//    print_r($detailsForm[]['group_id']);die;
//    $detailsForm->getWidget('email')->setAttribute('readonly','readonly');
//    $detailsForm->getWidget('first_name')->setAttribute('readonly','readonly');
//    $detailsForm->getWidget('last_name')->setAttribute('readonly','readonly');
//    $titleWidget = new sfWidgetFormInput(array(), array('readonly'=>'readonly'));
//    $titleWidget = new sfWidgetFormInput();
    if(sfContext::getInstance()->getUser()->getAttribute('is_super_admin')){
        $detailsForm->getWidget('ministry_id')->setLabel('Ministry Id');
    }
     $detailsForm->getWidget('group_id')->setLabel('Role');
     $detailsForm->getWidget('category_list')->setLabel('Categories');
//    $detailsForm->setWidget('title', $titleWidget);
//    $detailsForm->setDefault('group_id', 2);
    $this->embedForm('details', $detailsForm);
  }


  public function configureGroups() {
    $this->uiGroup = new Dlform();
    //$this->uiGroup->setLabel('Portal User');

    $newUserDetails = new FormBlock('Edit User Details');
    $this->uiGroup->addElement($newUserDetails);
//    $newUserDetails->addElement(array('details:title','details:first_name','details:last_name','details:ministry_id',
//      'details:email','username','password','confirm_password','is_active'));
    if(sfContext::getInstance()->getUser()->getAttribute('is_super_admin')){
        if(sfContext::getInstance()->getUser()->getGuardUser()->getId() == sfContext::getInstance()->getRequest()->getParameter('id')){
//            super admin assign categories edit user
            if(sfContext::getInstance()->getRequest()->getParameter('id')){
                $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','is_active'));
            }else{
                $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:ministry_id','details:group_id','details:category_list'));

            }
//            $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:ministry_id','details:group_id'));
        }else{
 //            super admin assign categories edit user
            $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:ministry_id','details:group_id','details:category_list','is_active'));
//            $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:ministry_id','details:group_id','is_active'));
        }
    }else{
        if(sfContext::getInstance()->getUser()->getGuardUser()->getId() == sfContext::getInstance()->getRequest()->getParameter('id')){
            if(sfContext::getInstance()->getRequest()->getParameter('id')){
                $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','is_active'));
            }else{
                $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:group_id','details:category_list'));
            }
        }else{
            $newUserDetails->addElement(array('username','details:title','details:first_name','details:last_name','details:email','details:group_id','details:category_list','is_active'));
        }
    }

  }

}