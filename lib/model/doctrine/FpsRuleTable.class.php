<?php


class FpsRuleTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('FpsRule');
    }
     public function getPfsRule() {
      $q = $this->createQuery('r')
      ->select('r.description');

      return $q->execute(array(), DOCTRINE::HYDRATE_ARRAY);
    }
}