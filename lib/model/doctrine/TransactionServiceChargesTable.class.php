<?php


class TransactionServiceChargesTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('TransactionServiceCharges');
    }

    public function updateConvertedAmount($orderRequestDetailId, $appType, $appId, $appAmount, $currency, $serviceCharge=''){
        try{
            $statusData = Doctrine_Query::create()
            ->update("TransactionServiceCharges")
            ->set("app_convert_amount", "?", $appAmount)
            ->set("app_currency", "?", $currency);
            if($serviceCharge != ''){
                $statusData->set("app_convert_service_charge", "?", $serviceCharge);
            }
            $statusData->where("app_id = ?",$appId)
            ->andWhere("app_type = ?",$appType)
            ->andWhere("order_request_detail_id = ?", $orderRequestDetailId)
            ->execute();
            return true;
        }catch(Exception $e){
            return false;
        }
    }
    /**
     *
     * @param <type> $appId
     * @param <type> $appType
     * @param <type> $orderRequestDetailId
     * @return <type>
     */
    public function getApplicationDetails($appId, $appType, $orderRequestDetailId){

        try{
            if(!empty($appId) && !empty($appType) && !empty($orderRequestDetailId)){

                $query = Doctrine_Query::create()
                ->select('t.*')
                ->from('TransactionServiceCharges t')
                ->where('t.app_id = ?',$appId)
                ->andWhere('t.app_type = ?',$appType)
                ->andWhere('t.order_request_detail_id = ?',$orderRequestDetailId)
                ->execute();
                return $query;
            }else{
                return false;
            }
        }catch(Exception $e){
            return false;
        }

    }

     /**
       *
       * @param <type> $orderNumber
       * Fetching application details from order number...
       */
    public function getAppDetailFromOrderNumber($orderNumber){
        try{
            $orderRequestDetailId = Doctrine::getTable('ipay4meOrder')->getOrderRequestDetails($orderNumber);
            if($orderRequestDetailId > 0){
                $query = Doctrine_Query::create()
                ->select('t.*')
                ->from('TransactionServiceCharges t')
                ->where('t.order_request_detail_id = ?',$orderRequestDetailId);
                $result = $query->execute();
                return $result;
            }else{
                return false;
            }
        }catch(Exception $e){
            return false;
        }
    }

      /**
       *
       * @param <type> $appId
       * @param <type> $appType
       * @return <type>
       */
    public function getOrderRequestDetailId($appId, $appType){

        try{
            $query = Doctrine_Query::create()
            ->select('t.*')
            ->from('TransactionServiceCharges t')
            ->where('t.app_id = ?',$appId)
            ->andWhere('t.app_type = ?',$appType);
            $result = $query->execute();
            return $result;

        }catch(Exception $e){
            return false;
        }

    }

    /**
     * [WP: 082] => CR:120
     * [WP: 113] => CR: 159 => Adding $status parameter
     * @param <type> $orderRequestDetailId
     * @return <type>
     * This function return applications assosciated with given order request details id...
     */
    public function getApplicationDetailsByORDId($orderRequestDetailId, $status = ''){
        try{            
            $query = Doctrine_Query::create()
            ->select('t.*')
            ->from('TransactionServiceCharges t')
            ->where('t.order_request_detail_id = ?',$orderRequestDetailId);
            /**
             * [WP: 113] => CR: 159
             * If status will come then condition will be true...
             */
            if($status != ''){
                $query->andWhere('t.status=?',$status);
            }
            $result = $query->execute();
            return $result;

        }catch(Exception $e){
            return false;
        }
    }

    public function getVisaArrivalReport($startDate,$endDate){
        $detailArray = array();
        $i = 0;
        $query = "SELECT DATE_FORMAT(ts.created_at, '%Y') AS year FROM transaction_service_charges ts left join ipay4me_order ip on ts.order_request_detail_id = ip.order_request_detail_id  WHERE ts.created_at >= '".$startDate." 00:00:00' AND ts.created_at <= '".$endDate." 23:59:59' and ts.app_type = 'vap' and ip.payment_status = 0 GROUP BY year";
        $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");
        $res = $db->execute($query)->fetchAll();
        $i = 0;
        if(count($res) > 0){
            foreach($res AS $records){
                $query = "SELECT DATE_FORMAT(ts.created_at, '%M') AS month, DATE_FORMAT(ts.created_at, '%Y') AS year, count(ts.id) as count,sum(service_charge) as service_charge,sum(app_amount) as app_amount FROM transaction_service_charges ts left join ipay4me_order ip on ts.order_request_detail_id = ip.order_request_detail_id  WHERE ts.created_at >= '".$startDate." 00:00:00' AND ts.created_at <= '".$endDate." 23:59:59' and ts.app_type = 'vap' and ip.payment_status = 0 GROUP BY month HAVING year = '".$records['year']."' ORDER BY ts.created_at ASC";
                $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");
                $appRes = $db->execute($query)->fetchAll();
                foreach($appRes AS $appRecords){
                    $detailArray[$i]['month'] = $appRecords['month'];
                    $detailArray[$i]['year'] = $appRecords['year'];
                    $detailArray[$i]['count'] = $appRecords['count'] ;
                    $detailArray[$i]['app_amount'] = $appRecords['app_amount'] - $appRecords['service_charge'];
                    $detailArray[$i]['service_charge'] = $appRecords['service_charge'];
                    $detailArray[$i]['total_amount'] = ($appRecords['app_amount']);
                    $i++;
                }
            }
        }

        return $detailArray;
    }

    /**
     * [WP: 092] => CR:132
     * @param <type> $startDate
     * @param <type> $endDate
     * @return <type>
     *
     */

    public function getPOSPaymentReport($startDate,$endDate){
        $detailArray = array();
        $query = "SELECT DATE_FORMAT(ts.created_at, '%Y') AS year FROM transaction_service_charges ts left join ipay4me_order ip on ts.order_request_detail_id = ip.order_request_detail_id  WHERE ts.created_at >= '".$startDate." 00:00:00' AND ts.created_at <= '".$endDate." 23:59:59' and ts.app_type = 'vap' and ip.payment_status = 0 and ip.card_first = '4444' and card_last = '4444' and card_holder = 'pos' GROUP BY year";
        $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");
        $res = $db->execute($query)->fetchAll();
        $i = 0;
        if(count($res) > 0){
            foreach($res AS $records){
                $query = "SELECT DATE_FORMAT(ts.created_at, '%M') AS month, DATE_FORMAT(ts.created_at, '%Y') AS year, count(ts.id) as count,sum(service_charge) as service_charge,sum(app_amount) as app_amount FROM transaction_service_charges ts left join ipay4me_order ip on ts.order_request_detail_id = ip.order_request_detail_id  WHERE ts.created_at >= '".$startDate." 00:00:00' AND ts.created_at <= '".$endDate." 23:59:59' and ts.app_type = 'vap' and ip.payment_status = 0 and ip.card_first = '4444' and card_last = '4444' and card_holder = 'pos' GROUP BY month HAVING year = '".$records['year']."' ORDER BY ts.created_at ASC";
                $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");
                $appRes = $db->execute($query)->fetchAll();
                foreach($appRes AS $appRecords){
                    $detailArray[$i]['month'] = $appRecords['month'];
                    $detailArray[$i]['year'] = $appRecords['year'];
                    $detailArray[$i]['count'] = $appRecords['count'] ;
                    $detailArray[$i]['app_amount'] = $appRecords['app_amount']  - $appRecords['service_charge'];
                    $detailArray[$i]['service_charge'] = $appRecords['service_charge'];
                    $detailArray[$i]['total_amount'] = $appRecords['app_amount'];
                    $i++;
                }
            }
        }

        return $detailArray;
    }


    public function getSingleRecordByAppIdAndAppType($appId, $appType){

        try{
            $query = Doctrine_Query::create()
            ->select('t.*')
            ->from('TransactionServiceCharges t')
            ->where('t.app_id = ?',$appId)
            ->andWhere('t.app_type = ?',$appType)
            ->orderBy('t.id DESC')
            ->limit(1);
            $result = $query->execute();
            return $result;

        }catch(Exception $e){
            return false;
        }

    }


    /**
     * [WP: 102] => CR: 145
     * @param <type> $startDate
     * @param <type> $endDate
     * @return <type>
     */
    public function getVisaWeeklyReport($weeklyArray = array()){

        $detailArray = array();
        $i = 0;

        foreach($weeklyArray AS $week){
            $startDate = $week[0].' 00:00:00';
            $endDate = $week[1].' 23:59:59';

            $query = Doctrine_Query::create()
            ->select("ts.id, DATE_FORMAT(ts.updated_at, '%M') AS month, DATE_FORMAT(ts.updated_at, '%Y') AS year, COUNT(ts.id) as count, SUM(service_charge) as service_charge, SUM(app_amount) as app_amount, SUM(app_convert_service_charge) as convert_service_charge, SUM(app_convert_amount) as app_convert_amount, ts.app_currency")
            ->from('TransactionServiceCharges ts')
            ->where('ts.updated_at >= ?', $startDate)
            ->andWhere('ts.updated_at <= ?', $endDate)
            ->andWhere('ts.status = ?', 0)
            ->andWhere('ts.app_type = ?', 'Visa')
            ->groupBy("year, month")
            ->orderBy('year, month ASC');

            $appRes = $query->execute();

            $k = 1;
            foreach($appRes AS $appRecords){

                if($k > 1){
                    
                    $detailArray[$i]['week_start'] = $week[0];
                    $detailArray[$i]['week_end'] = $week[1];
                    $detailArray[$i]['month'] = $appRecords['month'];
                    $detailArray[$i]['year'] = $appRecords['year'];
                    $detailArray[$i]['count'] += $appRecords['count'] ;
                    $detailArray[$i]['app_amount'] += ($appRecords['app_amount'] - $appRecords['service_charge']);
                    $detailArray[$i]['service_charge'] += $appRecords['service_charge'];
                    $detailArray[$i]['total_amount'] += ($appRecords['app_amount']);
                    $detailArray[$i]['app_currency'] = ($appRecords['app_currency']);
                    $detailArray[$i]['app_convert_amount'] += $appRecords['app_convert_amount'] - $appRecords['convert_service_charge'];
                    $detailArray[$i]['app_convert_service_charge'] += $appRecords['convert_service_charge'];
                    $detailArray[$i]['total_convert_amount'] += $appRecords['app_convert_amount'];

                    if($k == count($appRes)){
                        $i++;
                    }
                    
                }

                if($k == 1){
                    $detailArray[$i]['week_start'] = $week[0];
                    $detailArray[$i]['week_end'] = $week[1];
                    $detailArray[$i]['month'] = $appRecords['month'];
                    $detailArray[$i]['year'] = $appRecords['year'];
                    $detailArray[$i]['count'] = $appRecords['count'] ;
                    $detailArray[$i]['app_amount'] = $appRecords['app_amount'] - $appRecords['service_charge'];
                    $detailArray[$i]['service_charge'] = $appRecords['service_charge'];
                    $detailArray[$i]['total_amount'] = ($appRecords['app_amount']);
                    $detailArray[$i]['app_currency'] = ($appRecords['app_currency']);
                    $detailArray[$i]['app_convert_amount'] = $appRecords['app_convert_amount'] - $appRecords['convert_service_charge'];
                    $detailArray[$i]['app_convert_service_charge'] = $appRecords['convert_service_charge'];
                    $detailArray[$i]['total_convert_amount'] = $appRecords['app_convert_amount'];

                    if(count($appRes) == 1){
                        $i++;
                    }

                    $k++;

                }
                
            }//End of foreach($appRes AS $appRecords){...
        }//End of foreach($weeklyArray AS $week){...

        return $detailArray;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $startDate
     * @param <type> $endDate
     * @return <type> 
     */
    public function getVisaMonthlyReport($startDate, $endDate){

        $detailArray = array();

        $startDate = $startDate.' 00:00:00';
        $endDate = $endDate.' 23:59:59';

        $query = Doctrine_Query::create()
        ->select("ts.id, DATE_FORMAT(ts.created_at, '%M') AS month, DATE_FORMAT(ts.created_at, '%Y') AS year, COUNT(ts.id) as count, SUM(service_charge) as service_charge, SUM(app_amount) as app_amount, SUM(app_convert_service_charge) as convert_service_charge, SUM(app_convert_amount) as app_convert_amount, ts.app_currency")
        ->from('TransactionServiceCharges ts')
        ->where('ts.updated_at >= ?', $startDate)
        ->andWhere('ts.updated_at <= ?', $endDate)
        ->andWhere('ts.status = ?', 0)
        ->andWhere('ts.app_type = ?', 'Visa')
        ->groupBy('year, month')
        ->orderBy('ts.id, year, month ASC');

        $appRes = $query->execute();
        $i=0;
        foreach($appRes AS $appRecords){

            $detailArray[$i]['week_start'] = $week[0];
            $detailArray[$i]['week_end'] = $week[1];
            $detailArray[$i]['month'] = $appRecords['month'];
            $detailArray[$i]['year'] = $appRecords['year'];
            $detailArray[$i]['count'] = $appRecords['count'] ;
            $detailArray[$i]['app_amount'] = $appRecords['app_amount'] - $appRecords['service_charge'];
            $detailArray[$i]['service_charge'] = $appRecords['service_charge'];
            $detailArray[$i]['total_amount'] = ($appRecords['app_amount']);
            $detailArray[$i]['app_currency'] = ($appRecords['app_currency']);
            $detailArray[$i]['app_convert_amount'] = $appRecords['app_convert_amount'] - $appRecords['convert_service_charge'];
            $detailArray[$i]['app_convert_service_charge'] = $appRecords['convert_service_charge'];
            $detailArray[$i]['total_convert_amount'] = $appRecords['app_convert_amount'];
            $i++;
        }//End of foreach($appRes AS $appRecords){...

        return $detailArray;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $startDate
     * @param <type> $endDate
     * @param <type> $country
     * @return <type> 
     */
    public function getVisaMonthlyReportByCountry($startDate, $endDate, $country = array()){

        $detailArray = array();

        $startDate = $startDate.' 00:00:00';
        $endDate = $endDate.' 23:59:59';


        $cntArray = '';
        foreach($country AS $cnt){
            $cntArray .= "'".$cnt."',";
        }
        $cntArray = RTRIM($cntArray, ',');

        ## Creating Ipay4me connection...
        $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");

        foreach($country AS $cnt){
            
            $query = "SELECT io.id, io.processing_country, ts.id, DATE_FORMAT(ts.created_at, '%M') AS month, DATE_FORMAT(ts.created_at, '%Y') AS year, COUNT(ts.id) as count, SUM(service_charge) as service_charge, SUM(app_amount) as app_amount, SUM(app_convert_service_charge) as convert_service_charge, SUM(app_convert_amount) as app_convert_amount, ts.app_currency
              FROM ipay4me_order io LEFT JOIN transaction_service_charges ts ON (io.order_request_detail_id = ts.order_request_detail_id )
              WHERE ts.updated_at >= '".$startDate."' AND ts.updated_at <= '".$endDate."'
              AND ts.status = '0' AND ts.app_type = 'Visa' AND io.processing_country = '".$cnt."' AND io.payment_status IN (0,2)
              GROUP BY year, month
              ORDER BY ts.id, year, month DESC";

            $statement = $db->prepare($query);
            $statement->execute();
            $appRes = $statement->fetchAll(PDO::FETCH_OBJ);
            $i=0;
            foreach($appRes AS $appRecords){

                $detailArray[$cnt][$i]['week_start'] = $week[0];
                $detailArray[$cnt][$i]['week_end'] = $week[1];
                $detailArray[$cnt][$i]['month'] = $appRecords->month;
                $detailArray[$cnt][$i]['year'] = $appRecords->year;
                $detailArray[$cnt][$i]['count'] = $appRecords->count;
                $detailArray[$cnt][$i]['app_amount'] = $appRecords->app_amount - $appRecords->service_charge;
                $detailArray[$cnt][$i]['service_charge'] = $appRecords->service_charge;
                $detailArray[$cnt][$i]['total_amount'] = $appRecords->app_amount;
                $detailArray[$cnt][$i]['app_currency'] = $appRecords->app_currency;
                $detailArray[$cnt][$i]['app_convert_amount'] = $appRecords->app_convert_amount - $appRecords->convert_service_charge;
                $detailArray[$cnt][$i]['app_convert_service_charge'] = $appRecords->convert_service_charge;
                $detailArray[$cnt][$i]['total_convert_amount'] = $appRecords->app_convert_amount;
                $detailArray[$cnt][$i]['processing_country'] = $appRecords->processing_country;
                $i++;
                
            }//End of foreach($appRes AS $appRecords){...
            
        }//End of foreach($country AS $cnt){...

        return $detailArray;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $weeklyArray
     * @param <type> $country
     * @return <type>
     */
    public function getVisaWeeklyReportByCountry($weeklyArray = array(), $country = array()){

        $cntArray = '';
        foreach($country AS $cnt){
            $cntArray .= "'".$cnt."',";
        }
        $cntArray = RTRIM($cntArray, ',');

        $detailArray = array();
        $i = 0;
        /* Creating Ipay4me connection... */
        $db = Doctrine_Manager::connection()->getManager()->getConnection("connection_ipay4me");


        foreach($country AS $cnt){

                foreach($weeklyArray AS $week){
                    $startDate = $week[0].' 00:00:00';
                    $endDate = $week[1].' 23:59:59';

                    $query = "SELECT io.id, io.processing_country, ts.id, DATE_FORMAT(ts.created_at, '%M') AS month, DATE_FORMAT(ts.created_at, '%Y') AS year, COUNT(ts.id) as count, SUM(service_charge) as service_charge, SUM(app_amount) as app_amount, SUM(app_convert_service_charge) as convert_service_charge, SUM(app_convert_amount) as app_convert_amount, ts.app_currency
                          FROM ipay4me_order io LEFT JOIN transaction_service_charges ts ON (io.order_request_detail_id = ts.order_request_detail_id )
                          WHERE ts.updated_at >= '".$startDate."' AND ts.updated_at <= '".$endDate."'
                          AND ts.status = '0' AND ts.app_type = 'Visa' AND io.processing_country = '".$cnt."' AND io.payment_status IN (0,2)
                          GROUP BY '".$week[0]."', '".$week[1]."', year, month
                          ORDER BY ts.created_at, year, month ASC";

                    $statement = $db->prepare($query);
                    $statement->execute();
                    $appRes = $statement->fetchAll(PDO::FETCH_OBJ);


                    $k = 1;
                    foreach($appRes AS $appRecords){

                        if($k > 1){

                            $detailArray[$cnt][$i]['week_start'] = $week[0];
                            $detailArray[$cnt][$i]['week_end'] = $week[1];
                            $detailArray[$cnt][$i]['month'] = $appRecords->month;
                            $detailArray[$cnt][$i]['year'] = $appRecords->year;
                            $detailArray[$cnt][$i]['count'] += $appRecords->count;
                            $detailArray[$cnt][$i]['app_amount'] += $appRecords->app_amount - $appRecords->service_charge;
                            $detailArray[$cnt][$i]['service_charge'] += $appRecords->service_charge;
                            $detailArray[$cnt][$i]['total_amount'] += $appRecords->app_amount;
                            $detailArray[$cnt][$i]['app_currency'] = $appRecords->app_currency;
                            $detailArray[$cnt][$i]['app_convert_amount'] += $appRecords->app_convert_amount - $appRecords->convert_service_charge;
                            $detailArray[$cnt][$i]['app_convert_service_charge'] += $appRecords->convert_service_charge;
                            $detailArray[$cnt][$i]['total_convert_amount'] += $appRecords->app_convert_amount;
                            $detailArray[$cnt][$i]['processing_country'] = $appRecords->processing_country;

                            if($k == count($appRes)){
                                $i++;
                            }
                            
                        }

                        if($k == 1){
                            $detailArray[$cnt][$i]['week_start'] = $week[0];
                            $detailArray[$cnt][$i]['week_end'] = $week[1];
                            $detailArray[$cnt][$i]['month'] = $appRecords->month;
                            $detailArray[$cnt][$i]['year'] = $appRecords->year;
                            $detailArray[$cnt][$i]['count'] = $appRecords->count;
                            $detailArray[$cnt][$i]['app_amount'] = $appRecords->app_amount - $appRecords->service_charge;
                            $detailArray[$cnt][$i]['service_charge'] = $appRecords->service_charge;
                            $detailArray[$cnt][$i]['total_amount'] = $appRecords->app_amount;
                            $detailArray[$cnt][$i]['app_currency'] = $appRecords->app_currency;
                            $detailArray[$cnt][$i]['app_convert_amount'] = $appRecords->app_convert_amount - $appRecords->convert_service_charge;
                            $detailArray[$cnt][$i]['app_convert_service_charge'] = $appRecords->convert_service_charge;
                            $detailArray[$cnt][$i]['total_convert_amount'] = $appRecords->app_convert_amount;
                            $detailArray[$cnt][$i]['processing_country'] = $appRecords->processing_country;

                            if(count($appRes) == 1){
                                $i++;
                            }

                            $k++;
                        }                        
                        
                    }//End of foreach($appRes AS $appRecords){...
                    
                }//End of foreach($weeklyArray AS $week){...
            
        }//End of foreach($country AS $cnt){...
        
        return $detailArray;
    }

    /**
     * [WP: 112] => CR: 158
     * @param <type> $orderRequestDetailId
     * @param <type> $applicationId
     * @param <type> $applicationType
     * This function update payment status...
     */
    public function updatePaymentStatus($orderRequestDetailId, $status, $applicationId='', $applicationType=''){

        try {

            $q = $this->createQuery('d')
            ->update()
            ->set('d.status','?', $status)
            ->where('d.order_request_detail_id = ?',$orderRequestDetailId);

            if($applicationId != '' && $applicationType != ''){
                $q->andWhere('d.app_id = ?', $applicationId);
                $q->andWhere('d.app_type = ?', $applicationType);
            }

            $result = $q->execute();

        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }

}