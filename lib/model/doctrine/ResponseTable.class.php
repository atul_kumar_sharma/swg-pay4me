<?php


class ResponseTable extends Doctrine_Table {

  public static function getInstance() {
    return Doctrine_Core::getTable('Response');
  }

  public function saveResponse($requestId, $paymentResponse) {//echo "->>>>>>>>>".$paymentResponse['response_reason_text'];die;
    $response = new Response();
    $response->setRequestId($requestId);
    $response->setResponseCode($paymentResponse['response_code']);
    $response->setResponseReason($paymentResponse['response_reason']);
    $response->setResponseReasonTxt($paymentResponse['response_reason_text']);
    $response->setResponseReasonSubCode($paymentResponse['response_subcode']);
    $response->setResponseTransactionCode($paymentResponse['transaction_id']);
    $response->setResponseAuthorizationCode($paymentResponse['authorization_code']);
    $response->setResponseTransactionDate(date("Y-m-d H:m:s"));
    $response->save();
    return true ;
  }

  public function getOrderDetails($request_order_number) {
    try {

      $q = $this->createQuery('r')
          ->select('r.*,res.*,d.*,t.*')
          ->innerJoin('r.Request res')
          ->innerJoin('res.OrderRequestDetails d')
          ->innerJoin('d.OrderRequest t')
          ->where('res.request_order_number=?',$request_order_number );
       //   print $q->getSqlQuery();

      $res = $q->execute();

//      print "<pre>";
//      print_r($res->toArray());

      if($res->count() == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }



  }




}