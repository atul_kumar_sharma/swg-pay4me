<?php

/**
 * BaseOrderAuthorize
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $order_number
 * @property integer $gateway_id
 * @property timestamp $trans_date
 * @property enum $authorize_status
 * @property string $response_code
 * @property string $response_text
 * @property timestamp $paid_date
 * @property ipay4meOrder $ipay4meOrder
 * 
 * @method integer        getOrderNumber()      Returns the current record's "order_number" value
 * @method integer        getGatewayId()        Returns the current record's "gateway_id" value
 * @method timestamp      getTransDate()        Returns the current record's "trans_date" value
 * @method enum           getAuthorizeStatus()  Returns the current record's "authorize_status" value
 * @method string         getResponseCode()     Returns the current record's "response_code" value
 * @method string         getResponseText()     Returns the current record's "response_text" value
 * @method timestamp      getPaidDate()         Returns the current record's "paid_date" value
 * @method ipay4meOrder   getIpay4meOrder()     Returns the current record's "ipay4meOrder" value
 * @method OrderAuthorize setOrderNumber()      Sets the current record's "order_number" value
 * @method OrderAuthorize setGatewayId()        Sets the current record's "gateway_id" value
 * @method OrderAuthorize setTransDate()        Sets the current record's "trans_date" value
 * @method OrderAuthorize setAuthorizeStatus()  Sets the current record's "authorize_status" value
 * @method OrderAuthorize setResponseCode()     Sets the current record's "response_code" value
 * @method OrderAuthorize setResponseText()     Sets the current record's "response_text" value
 * @method OrderAuthorize setPaidDate()         Sets the current record's "paid_date" value
 * @method OrderAuthorize setIpay4meOrder()     Sets the current record's "ipay4meOrder" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseOrderAuthorize extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('order_authorize');
        $this->hasColumn('order_number', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('gateway_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('trans_date', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => false,
             ));
        $this->hasColumn('authorize_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 0,
              1 => 1,
             ),
             'default' => 1,
             'comment' => 
             array(
              0 => '0=capture',
              1 => '1=authorize',
             ),
             ));
        $this->hasColumn('response_code', 'string', 30, array(
             'type' => 'string',
             'length' => 30,
             ));
        $this->hasColumn('response_text', 'string', 1000, array(
             'type' => 'string',
             'length' => 1000,
             ));
        $this->hasColumn('paid_date', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => true,
             ));


        $this->index('unique_parameter', array(
             'fields' => 
             array(
              0 => 'order_number',
             ),
             'type' => 'unique',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('ipay4meOrder', array(
             'local' => 'order_number',
             'foreign' => 'order_number',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}