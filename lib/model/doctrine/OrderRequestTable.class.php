<?php


class OrderRequestTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('OrderRequest');
    }



    public function getReceipt($receiptId) {
        $orview = $this->createQuery('or')
        ->select('or.merchant_id, ORD.id,
                 ORD.amount as amount  as Amount,
                 mr.address as Address,mr.email as Email,mr.contact_phone as Phone,
                 ORD.paid_date as TransactionDate, ORD.card_first AS CardFirst,ORD.card_last AS CardLast,ORD.card_len AS CardLenght,ORD.payment_status as paymentStatus ,PEO.order_number as orderId, mr.name as Name')
        ->leftJoin('or.Merchant mr')
        ->leftJoin('or.OrderRequestDetails ORD')
        ->leftjoin('ORD.ipay4meOrder PEO')
        ->Where('ORD.id= ?',$receiptId)
        ->andWhere('ORD.payment_status = 0')
        ->execute(array(), Doctrine::HYDRATE_ARRAY);       
        if(count($orview) > 0)
        {
            return $orview[0];
        }

        return false;
    }

    public function getPurchaseHistory($userId){
        $phis = $this->createQuery('ph')
        ->select('ph.id, ORD.id as request_order_number, ORD.amount as amount,
                    ORD.paid_date as response_transaction_date,
                    mr.name as mname')
        ->leftJoin('ph.OrderRequestDetails ORD')
        ->leftJoin('ph.Merchant mr')
        ->where('ORD.user_id = ?',$userId)
        ->andWhere('ORD.payment_status = 0');
        return $phis;
    }

    /*public function getPaymentStatus($transaction_id,$merchant_id){
        $query = Doctrine_Query::create()
        ->from('OrderRequest or')
        ->leftJoin('or.Request req')
        ->leftJoin('req.Response res')
        ->andwhere('or.merchant_id = ?', $merchant_id)
        ->andwhere('or.transaction_number = ?',$transaction_id)
        ->andwhere('res.response_code = ?',1)->count();
        return $query;
    }*/

    public function findRequest($requestId){
        $q   = Doctrine_Query::create()
        ->select('or.*,mr.*')
        ->from('OrderRequest or')
        ->leftJoin('or.Merchant mr')
        ->Where('or.id= ?',$requestId);
        return $res = $q->fetchOne();
    }

    public function chkOrder($merchant_id, $transaction_number) {

        $query = $this->createQuery('o')
        ->select('*')
        ->Where('merchant_id = ?', $merchant_id)
        ->andWhere('transaction_number = ?', $transaction_number);

        $res = $query->execute();
        if($res->count() == 0) {
            return 0;
        }
        return $res->getFirst();

    }
}
