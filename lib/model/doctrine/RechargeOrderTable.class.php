<?php


class RechargeOrderTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('RechargeOrder');
    }

    public function findTotSuccesfulPayment($userId)
    {
        $q = $this->createQuery('s')
              ->select('*')
              ->where('s.user_id = ?',$userId)
              ->andWhere('s.payment_status = ?',0);
        $result = count($q->execute(array(),Doctrine::HYDRATE_ARRAY));
        return $result;
    }

}