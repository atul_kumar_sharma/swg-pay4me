<?php


class TrackingMoneyorderTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('TrackingMoneyorder');
  }

  public function getMoneyOrderDetails($moneyOrderId){
    if(isset ($moneyOrderId) && $moneyOrderId!=''){
      $query = Doctrine_Query::create()
      ->select('t.id,t.moneyorder_id as moneyorder_id,s.id,s.order_number as order_number,s.cart_amount as cart_amount, s.convert_amount as convert_amount, s.currency as currency, s.tracking_number as tracking_number,s.order_request_detail_id as order_request_detail_id')
      ->from('TrackingMoneyorder t')
      ->leftJoin('t.CartTrackingNumber s')
      ->where("t.moneyorder_id=?",$moneyOrderId)
      ->execute()->toArray();
      return $query;
    }
  }

  public function getTrackingMoneyOrderDetailByMoneyOrderId($moneyorderId){
    if(isset ($moneyorderId) && $moneyorderId!=''){
      $query = Doctrine_Query::create()
      ->select('t.*')
      ->from('TrackingMoneyorder t')
      ->where("t.moneyorder_id =?",$moneyorderId)
      ->execute()->toArray();
      return $query;
    }
  }
  public function deleteDetailByMoneyOrderId($moneyorderId){
    if(isset ($moneyorderId) && $moneyorderId!=''){
      $query = Doctrine_Query::create()
      ->delete('t.*')
      ->from('TrackingMoneyorder t')
      ->where('t.moneyorder_id =?',$moneyorderId);
      $query->execute();
    }
  }

  public function deleteDetailByMoneyOrderIdCartTrackId($moneyOrderDetailId,$tracking_money_order_id){
    if(isset ($moneyOrderDetailId) && $moneyOrderDetailId!='' && isset ($tracking_money_order_id) && $tracking_money_order_id!=''){
      $query = Doctrine_Query::create()
      ->delete('t.*')
      ->from('TrackingMoneyorder t')
      ->where("t.moneyorder_id =?",$moneyOrderDetailId)
      ->andWhere("t.cart_track_id =?",$tracking_money_order_id)
      ->execute();      
    }
  }

}