<?php


class RequestTable extends Doctrine_Table {

  public static function getInstance() {
    return Doctrine_Core::getTable('Request');
  }

  public static function getNotificationUrlbyOrderNo($request_order_number) {
    try {

      $res = array();

      $q   = Doctrine_Query::create()
          ->select('RQ.id, 0R.id, MT.id, MT.notification_url as notification_url')
          ->from('Request RQ')
          ->leftJoin('RQ.OrderRequestDetails 0R')
          ->leftJoin('0R.Merchant MT')
          ->where('RQ.request_order_number=?',$request_order_number);

      $res = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(count($res)>0) {
        return $res[0];
      }
      return $res;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function chkResponse($request_order_id) {
    try {
      $q = $this->createQuery('r')
          ->innerJoin('r.Response res')
          ->where('r.order_detail_id=?',$request_order_id )
          ->andWhere('res.response_code=?',1)
          ->andWhere('res.response_reason_sub_code=?',1);

      $res = $q->execute();

      if($res->count() == 0) {
        return 0;
      }
      return $res;
    }
    catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }



}