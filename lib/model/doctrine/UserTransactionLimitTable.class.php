<?php

class UserTransactionLimitTable extends Doctrine_Table {

  public static function getInstance() {
    return Doctrine_Core::getTable('UserTransactionLimit');
  }

  public function checkUserId($user_id) {
    try {
      $q = $this->createQuery('s')
      ->select('count(*)')
      ->where('s.user_id = ?', $user_id);

      return $q->count();
    } catch (Exception $e) {
      echo("Problem found" . $e->getMessage());
      die;
    }
  }
  /**
   * Function to retrive data by user_id
   * @param <type> $user_id
   * @return <type>
   */
  public function getDetailByUserId($user_id) {
    try {
      $q = $this->createQuery('s')
      ->select('s.*')
      ->where('s.user_id = ?', $user_id);
      $res = $q->execute();

      return $res;
    } catch (Exception $e) {
      echo("Problem found" . $e->getMessage());
      die;
    }
  }
  /**function addUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent)
   *@purpose :add enteries in table
   *@param : N/A
   *@return :  list of all active users
   *@author : Vchandwani
   *@date : 31-05-2011
   */
  public function addUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent) {

    try {
      $q = new UserTransactionLimit();
      if(($cart_capacity) !='')
      {
        $q->setCartCapacity($cart_capacity);
      }
      if(($cart_amount_capacity) !='')
      {
        $q->setCartAmountCapacity($cart_amount_capacity);
      }
      if(($number_of_transaction) !='')
      {
        $q->setNumberOfTransaction($number_of_transaction);
      }
      if(($transaction_period) !='')
      {
        $q->setTransactionPeriod($transaction_period);
      }
      $q->setUserId($user_id);
      $q->save();
      $res = $q;


      if ($res) {
        return true;
      } else {
        return false;
      }
    } catch (Exception $e) {
      return "Exception when retriving updating user detail.";
    }
  }
   /**function updateUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent)
   *@purpose :update table for edited emteries
   *@param : N/A
   *@return :  list of all active users
   *@author : Vchandwani
   *@date : 31-05-2011
   */
  public function updateUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent) {
    try {
      $query = Doctrine_Query::create();
      $query->update('UserTransactionLimit');
      if(trim($cart_capacity) != '')
      {
        $query->set('cart_capacity', '?', $cart_capacity);
      }
      else
      {
        $query->set("cart_capacity",'NULL');
      }
      if(trim($cart_amount_capacity) != '')
      {
        $query->set('cart_amount_capacity', '?', $cart_amount_capacity);
      }
      else
      {
        $query->set('cart_amount_capacity','NULL');
      }
      if(trim($number_of_transaction) != '')
      {
        $query->set('number_of_transaction', '?', $number_of_transaction);
      }
      else
      {
        $query->set('number_of_transaction','NULL');
      }
      if(trim($transaction_period) != '')
      {
        $query->set('transaction_period', '?', $transaction_period);
      }
      else
      {
        $query->set('transaction_period','NULL');
      }
      $query->where('user_id =?', $user_id);
      //echo $query->getSqlQuery();die;
      $res = $query->execute();
      if ($res) {
        return true;
      } else {
        return false;
      }
    } catch (Exception $e) {
      return "Exception when retriving updating user detail.";
    }
  }


   /**function updateUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent)
   *@purpose :update table for edited emteries
   *@param : N/A
   *@return :  list of all active users
   *@author : Vchandwani
   *@date : 31-05-2011
   */
  public function updateUserTransactionLimitRegisterCard($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction, $transaction_period, $ifLimitPresent) {
    try {
      $query = Doctrine_Query::create();
      $query->update('UserTransactionLimit');
      if(trim($cart_capacity) != '')
      {
        $query->set('cart_capacity', '?', $cart_capacity);
      }
      else
      {
        $query->set("cart_capacity",'NULL');
      }
      if(trim($cart_amount_capacity) != '')
      {
        $query->set('cart_amount_capacity', '?', $cart_amount_capacity);
      }
      else
      {
        $query->set('cart_amount_capacity','NULL');
      }
      if(trim($number_of_transaction) != '')
      {
        $query->set('number_of_transaction', '?', $number_of_transaction);
      }
      else
      {
        $query->set('number_of_transaction','NULL');
      }
      if(trim($transaction_period) != '')
      {
        $query->set('transaction_period', '?', $transaction_period);
      }
      
      $query->where('user_id =?', $user_id);
      //echo $query->getSqlQuery();die;
      $res = $query->execute();
      if ($res) {
        return true;
      } else {
        return false;
      }
    } catch (Exception $e) {
      return "Exception when retriving updating user detail.";
    }
  }

    /**function getUserCartLimit($userId)
   *@purpose :get user's cart capacity variables
   *@param : N/A
   *@return :  list of all active users
   *@author : KSingh
   *@date : 02-05-2011
   */

  public function getUserCartLimit($userId){

    $q = $this->createQuery('utl')
    ->select('utl.*')
    ->where('utl.user_id =?',$userId)
    ->execute(array(),Doctrine::HYDRATE_ARRAY);

    return $q;
  }

  public function saveUserTransactionLimit($user_id, $cart_capacity, $cart_amount_capacity, $number_of_transaction)
  {
    if(trim($cart_capacity) == "" && trim($cart_amount_capacity) == "" && trim($number_of_transaction) == "")
    {
      return;
    }
    $objUserLimit = new UserTransactionLimit();
    if(trim($cart_capacity) != "")
    $objUserLimit->setCartCapacity($cart_capacity);
    if(trim($cart_amount_capacity) != "")
    $objUserLimit->setCartAmountCapacity($cart_amount_capacity);
    if(trim($number_of_transaction) != "")
    $objUserLimit->setNumberOfTransaction($number_of_transaction);
    $objUserLimit->setUserId($user_id);
    $objUserLimit->save();
    return;
  }

  public function getAllUser($fname='',$lname='',$email='',$paymentGroupId=''){
        $query = $this->createQuery('ud')
        ->select('ud.*,u.id,ae.username as username,ae.created_at as last_login,ug.user_id,utl.user_id as user_id_user_transaction')
        ->leftJoin('ud.sfGuardUser u')
        ->leftJoin('u.sfGuardUserGroup ug')
        ->leftJoin('u.EpActionAuditEvent ae')
        ->leftJoin('u.UserTransactionLimit utl')
        ->Where('ud.deleted = ?','0')
        ->andWhere('ug.group_id = ?',$paymentGroupId)
        ->andWhere('ae.subcategory = ?','Login');
        if(isset($fname) && !empty($fname)){
          $query->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $query->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $query->andWhere('ud.email=?',$email);
        }
        $query->orderBy('ae.created_at DESC');

        return $query;
    }

    public function getAllUsers($fname='',$lname='',$email='',$paymentGroupId=''){
        $query = $this->createQuery('utl')
        ->select('ud.*, utl.id, u.id, u.username AS username, utl.created_at AS last_login, utl.user_id AS user_id_user_transaction')
        ->leftJoin('utl.sfGuardUser u')
        ->leftJoin('u.UserDetail ud')
        ->Where('ud.deleted = ?','0');        
        
        if(isset($fname) && !empty($fname)){
          $query->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $query->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $query->andWhere('ud.email=?',$email);
        }
        $query->orderBy('utl.created_at DESC');

        return $query;
    }

}