<?php


class CartTrackingNumberTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('CartTrackingNumber');
  }

  public function getTrackingNumbersByStaus($userId,$status){;
    if(isset ($userId) && $userId!='' && isset ($status) && $status!=''){
      $trackingData = Doctrine_Query::create()
      ->select('t.id,t.cart_id,t.tracking_number,t.cart_amount, t.status, t.created_at')
      ->from('CartTrackingNumber t')
      ->where("t.status='$status'")
      ->andWhere("t.user_id=?",$userId)
      ->orderBy("t.id DESC")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(isset ($trackingData)){
        return $trackingData;
      }else{
        return '';
      }
    }
  }

  public function changeTrackingSataus($trackingId, $status, $orderNumber){
    try{
      $statusData = Doctrine_Query::create()
      ->update("CartTrackingNumber")
      ->set("status", "?", $status)
      ->set("associated_date", "?", date('Y-m-d'))
      ->set("order_number", "?", $orderNumber)
      ->where("id=?",$trackingId)
      ->execute();
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  /*
  * @param <type> $orderNumber,$orderReqDetailId
  * @return <type>
  * To get retry id for the order number passsed.
  */

  public function updateCartTrackingDetail($orderNumber,$orderReqDetailId){
    try{
      $q = Doctrine_Query::create()
      ->update('CartTrackingNumber')
      ->set('status','?','Paid')
      ->where('order_request_detail_id =?',$orderReqDetailId)
      ->andWhere('order_number =?',$orderNumber);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when updating cart";
    }
  }

  /*
   * Added by vineet to see the entry of cart in this table
   *
   */
  public function getTrackingNumber($arrCartId,$status = '')
  {
    if(!empty($arrCartId))
    {
      $trackingData = Doctrine_Query::create();
      $trackingData->select('t.*');
      $trackingData->from('CartTrackingNumber t');
      $trackingData->whereIN("t.cart_id",$arrCartId);

      if(!empty($status))
      {
          $trackingData->andWhere("t.status=?",$status);
      }

      $arrTrackingData = $trackingData->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(isset ($arrTrackingData)){
        return $arrTrackingData;
      }else{
        return '';
      }
    }

  }
  public function setTrackingNumberStatus($finalArray){
    try{
      if(!empty($finalArray))
      {
        $q = Doctrine_Query::create()
        ->update('CartTrackingNumber')
        ->set('status','?','New')
        ->set('order_number','?','')
        ->whereIn('id',$finalArray);
        $res = $q->execute();
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when updating cart";
    }
  }

  public function getTrackingNumberById($finalArray){

    if(isset ($finalArray) && $finalArray!=''){
      $query = Doctrine_Query::create()
      ->select('t.*')
      ->from('CartTrackingNumber t')
      ->whereIn('t.id',$finalArray)
      ->execute()->toArray();
      return $query;
    }
  }


  /*
   * Added by vineet to see the entry of cart in this table
   *
   */
  public function getAllTrackingNumberId($arrCartId)
  {
      if(!empty($arrCartId))
      {
          $trackingData = Doctrine_Query::create()
          ->select('t.*')
          ->from('CartTrackingNumber t')
          ->whereIN("t.cart_id",$arrCartId)
          ->execute(array(),Doctrine::HYDRATE_ARRAY);
          if(isset ($trackingData)){
              return $trackingData;
          }else{
              return '';
          }
      }

  }

  public function setTrackingNumberDeleteStatus($cartId, $status=0){
    try{
      if($cartId !='' && $status != '')
      {
        $q = Doctrine_Query::create()
        ->update('CartTrackingNumber')
        ->set('deleted','?',$status)        
        ->where('cart_id = ?',$cartId);
        //die("Query: ".$q->getSqlQuery()."<br />CartId: ".$cartId."<br />Status: ".$status);
        $res = $q->execute();
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when updating cart";
    }
  }
 
 /**
  *
  * This function return data deleted or not...
  * @param <type> $arrCartId
  * @param <type> $tracking_number
  * @param <type> $status
  * @return <type> 
  */

  public function getUnbindTrackingNumber($arrCartId=array(), $tracking_number = '', $status = '')
  {
    if(!empty($arrCartId) || !empty($tracking_number))
    {
      $trackingData = Doctrine_Query::create();
      $trackingData->select('t.*');
      $trackingData->from('CartTrackingNumber t');

      if(!empty($arrCartId)){
        $trackingData->whereIN("t.cart_id",$arrCartId);
      }else{
        $trackingData->where("t.tracking_number=?",$tracking_number);
      }
      
      if(!empty($status))
      {
          $trackingData->andWhere("t.status=?",$status);
      }

      $trackingData->andWhereIn("t.deleted",array(0,1));
      $trackingData->orderBy("t.id DESC");

      $arrTrackingData = $trackingData->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(isset ($arrTrackingData)){
        return $arrTrackingData;
      }else{
        return '';
      }
    }else{
        return array();
    }
  }

}