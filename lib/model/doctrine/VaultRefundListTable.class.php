<?php


class VaultRefundListTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('VaultRefundList');
  }

  public function isDontRefundGraypayVault($orderNumber, $amount){
    if(isset ($orderNumber) && $orderNumber!=''){
      $vaultPaymentArr = $this->isExixtGraypayVault($orderNumber);
      if(isset ($vaultPaymentArr) && is_array($vaultPaymentArr) && count($vaultPaymentArr)>0){

        $checkEntry = $this->checkEntry($orderNumber);
        if(count($checkEntry)){
          $lastRefundedAmount = $checkEntry[0]['amount'];
          $upadtedAmount = $lastRefundedAmount + $amount;
          $update = Doctrine_Query::create()
          ->update('VaultRefundList')
          ->set('amount', '?',$upadtedAmount)
          ->where('order_number =?', $orderNumber)
          ->execute();
        }else{
          $newObj = new VaultRefundList();
          $newObj->setVaultNum($vaultPaymentArr[0]['vault_num']);
          $newObj->setOrderNumber($vaultPaymentArr[0]['order_number']);
          $newObj->setAmount($amount);
          $newObj->save();
        }
        return true;
      }else{
        return false;
      }
    }
  }

  public function isExixtGraypayVault($orderNumber){
    return $detailArr = Doctrine_Query::create()
    ->select("t.*")
    ->from("Vault t")
    ->where("t.order_number='$orderNumber'")
    ->andWhere("t.payment_status!='2'")
    ->execute(array(),Doctrine::HYDRATE_ARRAY);
  }
  public function checkEntry($orderNumber)
  {
    $q = Doctrine_Query::create()
    ->select('amount')
    ->from('VaultRefundList')
    ->where('order_number =?', $orderNumber)
    ->fetchArray();
//           echo "<pre>";print_r($q);die;
    return $q;
  }
    public function getOrderAmount($orderNumber){
        return $detailArr = Doctrine_Query::create()
        ->select("t.*")
        ->from("VaultRefundList t")
        ->where("t.order_number='$orderNumber'")
        //->andWhere("t.payment_status!='2'")
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
    }
}