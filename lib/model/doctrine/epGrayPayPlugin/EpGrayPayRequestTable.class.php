<?php


class EpGrayPayRequestTable extends PluginEpGrayPayRequestTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpGrayPayRequest');
    }

    public function getCardDetail($cusId) {
        $orview = $this->createQuery('per')
        ->select('per.card_first AS CardFirst,per.card_last AS CardLast,per.card_len AS CardLenght, per.card_type as CardType, per.card_holder as name, per.city as town, per.address1 as add1, per.state as state, per.zip as zip, per.country as country, per.phone as phone ')
        ->Where('per.order_number= ?',$cusId)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(count($orview) > 0)
        {
            return $orview[0];
        }

        return false;
    }

   public function getRequestbetTimeDiff($ipAddress,$timeFrameMinutes) {
        $orview = $this->createQuery('gp')
        ->select('gp.id ')
        ->Where('gp.ip_address = ?',$ipAddress)
        ->andWhere('gp.tran_date < ?',date('Y-m-d H:i:s'))
        ->andWhere('gp.tran_date > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeFrameMinutes, date('s'), date('m'), date('d'), date('Y'))))
        ;
  //echo $ipAddress."    ==============      ".date('Y-m-d H:i:s')."  ======== ".date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeFrameMinutes, date('s'), date('m'), date('d'), date('Y')));
        return count($orview);
    }

  public function getOrderAmount($order_number){
        $orview = $this->createQuery('gp')
        ->select('gp.amount')
        ->Where('gp.order_number = ?',$order_number)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(count($orview) > 0)
        {
            return $orview[0]['amount'];
        }

        return false;
      
  }
}