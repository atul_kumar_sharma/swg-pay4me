<?php


class EpGrayPayResponseTable extends PluginEpGrayPayResponseTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpGrayPayResponse');
    }

    public function getTransactionId($orderid)
    {
        $orview = $this->createQuery('per')
        ->select('per.transactionid')
        ->Where('per.orderid= ?',$orderid)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);

        if(count($orview) > 0)
        {
            
            return $orview[0]['transactionid'];
        }

        return false;
    }
}