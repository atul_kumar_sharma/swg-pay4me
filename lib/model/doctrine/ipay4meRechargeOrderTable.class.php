<?php


class ipay4meRechargeOrderTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('ipay4meRechargeOrder');
    }

    public function getTotalRechargeAmount($days)
    {        
        $userId = sfContext::getInstance()->getUser()->getGuardUser()->getId();
        $endDate=date('Y-m-d',mktime(0, 0, 0, date("m")  , date("d")-$days, date("Y")));

        $query=Doctrine_Query::create()
        ->select('iro.id,sum(ro.amount)')
        ->from('ipay4meRechargeOrder iro')
        ->leftJoin('iro.RechargeOrder ro')
        ->where('iro.created_by=?',$userId)
        ->andWhere('date(iro.created_at)>=?',$endDate)
        ->andWhere('date(iro.created_at)<=?',date('Y-m-d'))
        ->andWhere("payment_status = '0'")
        ->groupBy('iro.created_by')
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        if(count($query))
         return $query['0'];
         else
         return 0;
    }
   public function getOrderRequestDetails($order_number) {
        try {
            $q = $this->createQuery('r')
            ->select('r.*,t.*,m.*')
            ->innerJoin('r.RechargeOrder t')
            ->where('r.order_number =?',$order_number ) ;
            $result = $q->fetchArray() ;
            //      $res = $q->execute();

            if(count($result) == 0) {
                return 0;
            }
            return $result[0];
        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }


     public function checkDuplicacy($order_number) {
        try {
            $q = $this->createQuery('s')
            ->select('count(*)')
            ->where('s.order_number = ?',$order_number);

            return  $q->count();
        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }
}