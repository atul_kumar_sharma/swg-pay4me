<?php


class CardCountryListTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CardCountryList');
    }

    public function getAllRecords(){
        $query = Doctrine_Query::create()
                ->select('ccl.*')
                ->from('CardCountryList ccl')
                ->orderBy('ccl.country ASC');
        return $query;
    }
}