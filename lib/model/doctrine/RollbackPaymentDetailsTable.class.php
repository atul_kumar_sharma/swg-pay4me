<?php


class RollbackPaymentDetailsTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('RollbackPaymentDetails');
  }

  public function setRollbackDetail($appId,$appType,$appAmount,$isRollbackUpdated,$action,$status){
    $response = new RollbackPaymentDetails();
    $response->setAppId($appId);
    $response->setAppType($appType);
    $response->setAmount($appAmount);
    $response->setAction($action);
    $response->setStatus($status);
    $response->setRollbackPaymentId($isRollbackUpdated);
    $response->save();
    return true;
  }

  public function isRefunded($application_id, $application_type)
  {
    $q = Doctrine_Query::create()
    ->select('id')
    ->from('RollbackPaymentDetails')
    ->where('app_id =?', $application_id);

    if($application_type == 'p')
    {
      $q->andWhere('app_type =?', 'passport');
    }
    if($application_type == 'v')
    {
      $q->andWhere('app_type !=?', 'passport');
    }
    if($application_type == 'vap')
    {
      $q->andWhere('app_type =?', 'vap');
    }

      $count = $q->execute();
    return $count;
  }

  public function getRefundType($appId, $appType)
  {
    $q = Doctrine_Query::create()
    ->select('id, action, created_at')
    ->from('RollbackPaymentDetails')
    ->where('app_id =?', $appId)
    ->andWhere('app_type =?', $appType)
    ->fetchArray();
    return $q;
  }

  public function setRollbackAction($appId, $appType, $rollbackId, $status){

        try{
            if($appId != '' && $appType != '' && $rollbackId!= '' && $status != ''){
                $q = Doctrine_Query::create()
                    ->update('RollbackPaymentDetails')
                    ->set('action', '?', $status)
                    ->where('app_id = ?',$appId)
                    ->andWhere('app_type = ?',$appType)
                    ->andWhere('rollback_payment_id = ?',$rollbackId)
                    ->limit(1);
                $res = $q->execute();
                if($res){
                   return true;
                }else{
                   return false;
                }
            }else{
                return false;
            }
            
        }catch (Exception $e) {            
            throw New Exception ($e->getMessage());
        }        
     }
}