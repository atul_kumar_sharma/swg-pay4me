<?php

class SupportRequestCommentsTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('SupportRequestComments');
    }

     public function getRequestDetails($requestId){
       try {
       $query = $this->createQuery('a')
               ->select('*')
               ->where('a.request_id = ?',$requestId);

       $requestDetails = $query->execute()->toArray();
       return $requestDetails ;
       } catch(Exception $e){
            echo ("Problem Found ".$e.getError());die;
       }
   }

    public function getUpdatedComments($requestId) {

        $query = $this->createQuery('a')
                        ->select('*')
                        ->orderBy('a.id DESC')
                        ->where('a.request_id = ?', $requestId)
                        ->limit(1);

        $requestDetails = $query->execute()->toArray();
        return $requestDetails;
    }

    public function setSupportUserComments($param, $latestId) {

        $set = Doctrine_Query::create()
        ->update('SupportRequestComments');
        if (isset($param['txt_support_close_cmt']) && $param['txt_support_close_cmt']!='') {
            $set->set('support_user_comments', '?', $param['txt_support_close_cmt']);
        }
        if (isset($param['txt_support_assign_cmt']) && $param['txt_support_assign_cmt']!='') {
            $set->set('support_user_comments', '?', $param['txt_support_assign_cmt']);
        }
        if (isset($param['txt_support_respond_cmt']) && $param['txt_support_respond_cmt']!='') {
            $set->set('support_user_comments', '?', $param['txt_support_respond_cmt']);
        }
        $set->where('request_id = ?', $param['hdn_requestId'])
        ->andWhere('id =?', $latestId)
        ->execute();
    }

}