<?php


class EpJobQueueTable extends PluginEpJobQueueTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpJobQueue');
    }
}