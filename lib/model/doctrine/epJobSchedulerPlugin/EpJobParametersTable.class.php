<?php


class EpJobParametersTable extends PluginEpJobParametersTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpJobParameters');
    }
}