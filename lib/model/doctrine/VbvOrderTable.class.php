<?php


class VbvOrderTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VbvOrder');
    }

      public function saveOrder($order_id, $tarnsaNo, $type)
  {
    $obj = new VbvOrder();
    $obj->setOrderId($order_id);
    $obj->setOrderDetailId($tarnsaNo);
    $obj->setType($type);
    $obj->save();
    return true;
  }

  public function getOrderDetailId($order_id, $type='')
  {

    $q = Doctrine_Query::create()
            ->select('v.*,e.*')
            ->from('VbvOrder v')
            ->leftJoin('v.EpVbvRequest e')
            ->where('v.order_id = ?',$order_id);

            $result = $q->execute();
    if($result->count()){

            return $q->fetchArray();
    }
    return 0;
  }
}