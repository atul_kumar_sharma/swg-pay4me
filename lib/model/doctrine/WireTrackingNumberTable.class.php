<?php


class WireTrackingNumberTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('WireTrackingNumber');
    }

    public function getTrackingNumbersByStaus($userId,$status){;
    if(isset ($userId) && $userId!='' && isset ($status) && $status!=''){
      $trackingData = Doctrine_Query::create()
      ->select('id, cart_id, tracking_number, cart_amount, status, created_at')
      ->from('WireTrackingNumber w')
      ->where("status=?",$status)
      ->andWhere("user_id=?",$userId)
      ->orderBy("id DESC")
      ->execute(array(),Doctrine::HYDRATE_ARRAY);
      if(isset ($trackingData)){
        return $trackingData;
      }else{
        return '';
      }
    }
  }

  /**
   *
   * @param <type> $trackingId
   * @param <type> $status
   * @param <type> $orderNumber
   * @param <type> $wiretransferId
   * @return <type>
   * THIS FUNCTION IS USED TO UPDATE WIRE_TRACKING_NUMBER
   * UPDATING STATUS, ORDERNUMBER
   */

  public function changeTrackingSataus($trackingId, $status, $orderNumber, $wiretransferId){
    try{
      $statusData = Doctrine_Query::create()
      ->update("WireTrackingNumber")
      ->set("status", "?", $status)
      ->set("associated_date", "?", date('Y-m-d'))
      ->set("order_number", "?", $orderNumber)
      ->set("wire_tracking_id", "?", $wiretransferId)
      ->where("id=?",$trackingId)
      ->execute();
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  /*
  * @param <type> $orderNumber,$orderReqDetailId
  * @return <type>
  * To get retry id for the order number passsed.
  */

  public function updateCartTrackingDetail($orderNumber,$orderReqDetailId){
    try{
      $q = Doctrine_Query::create()
      ->update('WireTrackingNumber')
      ->set('status','?','Paid')
      ->where('order_request_detail_id =?',$orderReqDetailId)
      ->andWhere('order_number =?',$orderNumber);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when updating cart";
    }
  }

  /*
   * Added by vineet to see the entry of cart in this table
   *
   */
  public function getTrackingNumber($arrCartId,$status = '')
  {
    if(!empty($arrCartId))
    {
      $trackingData = Doctrine_Query::create();
      $trackingData->select('t.*');
      $trackingData->from('WireTrackingNumber t');
      $trackingData->whereIN("t.cart_id",$arrCartId);

      if(!empty($status))
      {
          $trackingData->andWhere("t.status=?",$status);
      }

      $arrTrackingData = $trackingData->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(isset ($arrTrackingData)){
        return $arrTrackingData;
      }else{
        return '';
      }
    }

  }


  public function setTrackingNumberStatus($finalArray){
    try{
      if(!empty($finalArray))
      {
        $q = Doctrine_Query::create();
        $q->update('WireTrackingNumber');
        $q->set('status','?','New');
        $q->set('order_number','?','');
        $q->set('wire_tracking_id','NULL');
        $q->whereIn('id',$finalArray);
        $res = $q->execute();        
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when updating cart";
    }
  }

  public function getTrackingNumberById($wireTransferId){
  
    if(isset ($wireTransferId) && $wireTransferId != ''){
      $query = Doctrine_Query::create()
      ->select('t.*')
      ->from('WireTrackingNumber t')
      ->where('t.wire_tracking_id =?',$wireTransferId)
      ->execute()->toArray();
      return $query;
    }
  }


  /*
   * Added by vineet to see the entry of cart in this table
   *
   */
  public function getAllTrackingNumberId($arrCartId)
  {
      if(!empty($arrCartId))
      {
          $trackingData = Doctrine_Query::create()
          ->select('t.*')
          ->from('WireTrackingNumber t')
          ->whereIN("t.cart_id",$arrCartId)
          ->execute(array(),Doctrine::HYDRATE_ARRAY);
          if(isset ($trackingData)){
              return $trackingData;
          }else{
              return '';
          }
      }
  }

  public function getWireTransferDetails($wiretransferId){
    if(isset ($wiretransferId) && $wiretransferId!=''){
      $query = Doctrine_Query::create()
      ->select('t.id,t.wire_tracking_id,t.order_number,t.cart_amount,t.tracking_number,t.order_request_detail_id')
      ->from('WireTrackingNumber t')      
      ->where("t.wire_tracking_id=?",$wiretransferId)
      ->execute()->toArray();
      return $query;
    }
  }

  public function getTrackingWireTransferDetailByWireTransferId($wireTransferDetailId){
    if(isset ($wireTransferDetailId) && $wireTransferDetailId!=''){
      $query = Doctrine_Query::create()
      ->select('wtn.*')
      ->from('WireTrackingNumber wtn')
      ->where("wtn.wire_tracking_id =?",$wireTransferDetailId)
      ->execute()->toArray();
      return $query;
    }
  }


}