<?php


class EpMasterAccountTable extends PluginEpMasterAccountTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpMasterAccount');
    }
}