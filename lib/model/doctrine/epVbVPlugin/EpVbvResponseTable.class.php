<?php


class EpVbvResponseTable extends PluginEpVbvResponseTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpVbvResponse');
    }

    public function getDetailsByApprovalCode($approvalCode){
        $query = Doctrine_Query::create()
        ->select("t.*")
        ->from("EpVbvResponse t")
        ->where("t.approval_code =?",$approvalCode);
        $query = $query->execute(array(),Doctrine::HYDRATE_ARRAY);                
        return $query;        
    }
}