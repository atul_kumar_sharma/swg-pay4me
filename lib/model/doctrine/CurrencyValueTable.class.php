<?php


class CurrencyValueTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CurrencyValue');
    }

    public function getUsdInNaira()
    {
        $today = date('Y-m-d');
        $conversionType = 'dollor_to_naira';

        $q = $this->createQuery('c')
            ->select('c.value')
            ->where('date(c.currency_date) = ?',$today)
            ->andWhere('c.conversion_type = ?', $conversionType)
            ->orderBy('c.currency_date DESC')
            ->execute(array(), Doctrine::HYDRATE_ARRAY);

         if(count($q) > 0)
        {
            return $q[0]['value'];
        }

        return false;
            
    }
}