<?php


class BatchProcessRequestTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('BatchProcessRequest');
  }

  public function getVaultProcessedTxnNo($orderNumber)
  {
    $q = Doctrine_Query::create()
    ->select('req.id as reqid, res.transactionid as transactionid')
    ->from('BatchProcessRequest req')
    ->leftJoin('req.BatchProcessResponse res')
    ->where('res.orderid=?',$orderNumber)
    ->andWhere('res.response_code = 100')
    ->fetchArray();

    if(count($q) > 0)
    {
      return $q[0]['transactionid'];
    }else{
      return '';
    }
  }
}