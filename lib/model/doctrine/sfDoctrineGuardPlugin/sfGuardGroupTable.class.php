<?php


class sfGuardGroupTable extends PluginsfGuardGroupTable
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('sfGuardGroup');
  }

  public static function getGroup($roleName) {
    $q = Doctrine_Query::create();
    $q->from('sfGuardGroup')
    ->where('name = ? ', $roleName);
    return $q->execute()->getFirst();
  }

  public function getGroupQuery(){
    $roleNameArr = array("payment_group","Merchant","admin")  ;
    $q = Doctrine_Query::create();
    $q->from('sfGuardGroup')
    ->whereNotIn('name', $roleNameArr);
    return $q;
  }
}