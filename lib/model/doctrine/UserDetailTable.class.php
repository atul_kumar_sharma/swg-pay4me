<?php


class UserDetailTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('UserDetail');
    }

    public function chkEmailId($email,$id = null){
        $query = $this->createQuery('ud')
        ->select('email')
        ->where('ud.email=?',$email);
        if($id)
        {
            $query->andWhere('ud.user_id!=?',$id);
        }
        $result = $query->execute();
        return $result->count();
    }
   

    public function createUser($username,$email,$fname,$lname,$mob,$address){
        $regObj = new UserDetail();

        if($fname != null){
            $regObj->setFirstName($fname);
        }
        if($lname != null){
            $regObj->setLastName($lname);
        }

        $regObj->setEmail($email);
        if($mob != null){
            $mob  = trim($mob);//[\]+()-]
            $mob  = str_replace("[","",$mob);
            $mob  = str_replace("]","",$mob);
            $mob  = str_replace("+","",$mob);
            $mob  = str_replace("(","",$mob);
            $mob  = str_replace(")","",$mob);
            $mob  = str_replace("-","",$mob);
            $mob  = str_replace(" ","",$mob);
            $mob  = "+".$mob  ;
            $regObj->setMobilePhone($mob);
        }
        if($address != null){
            $regObj->setAddress($address);
        }
        $regObj->setEmail($email);
        if($mob != null){
            $regObj->setMobilePhone($mob);
        }
        if($address != null){
            $regObj->setAddress($address);
        }

        $sfUserObj = $regObj->getSfGuardUser();
        $sfUserObj->setUsername($username);
        $sfUserObj->setPassword('123456');

        try{
            Doctrine_Manager::getInstance()->getCurrentConnection()->beginTransaction();
            $regObj->save(Doctrine_Manager::getInstance()->getCurrentConnection());
            $role = sfGuardGroupTable::getGroup(sfConfig::get('app_pay4me_role_user'));
            $roleIds[] = $role['id'];
            $sfUserObj->link('groups', $roleIds);
            $sfUserObj->save();
            $regId = $regObj->getUserId();
            Doctrine_Manager::getInstance()->getCurrentConnection()->commit();
            return $regId;
        }catch(Exception $e){
            $logger = sfContext::getInstance()->getLogger();
            $logger->err($e);
            Doctrine_Manager::getInstance()->getCurrentConnection()->rollback();
            throw $e;
        }

    }
    public function getFailedAttempts($uname)
    {
        //,DATEDIFF(NOW(),u.updated_at) as sub_time
        $sfObj = $this->createQuery('ud')
        ->select('u.*,ud.*')
        ->leftJoin('ud.sfGuardUser u')
        ->where('u.username=?',$uname)
        ->andWhere('u.is_active=?',1)
        //->andWhere('u.deleted=?',0)->getQuery();
        ->fetchOne();

       /*
        if($sfObj!=false && $sfObj->getSubTime()>1)
        {
          $sfObj->setFailedAttempt(0);
          $sfObj->save();
        }
        *
        */
        return $sfObj;
    }
    public function getNewEwallet($from_date,$to_date)
    {
        $q = $this->createQuery('ud')
        ->select('ud.id, ud.user_id as uid,ud.created_at,ud.first_name,ud.last_name,  GU.id , GU.username AS uname')
        ->innerJoin('ud.sfGuardUser GU')
        ->where('ud.kyc_status = ?',2);

        if($from_date != "") {
            $from_date=$from_date." 00:00:00";
            $q = $q->andWhere('ud.updated_at  >= ?', $from_date);
        }

        if($to_date != "") {
            $to_date=$to_date." 23:59:59";
            $q = $q->andWhere('ud.updated_at  <= ?', $to_date);
        }
        //$q->execute();echo "<pre>";print_r($q->fetchArray());
        return $q;
    }


    public function checkUniqueMobileNumber($userId, $mobileNumber){
        $q = $this->createQuery('ud')
        ->select('ud.id')
        ->Where('ud.mobile_phone = ?',$mobileNumber);
        if($userId != '' || $userId != NULL){
            $q->andWhere('ud.id != ?',$userId);
        }

        $result = $q->execute();
        return $result->count();
    }
    public function getUserDetail($username='', $email=''){
        $q = $this->createQuery('ud')
        ->select('ud.*,u.username,c.amount,c.days')
        ->leftJoin('ud.sfGuardUser u')
        ->leftjoin('u.UserRechargeConfig c');
        if(''!=$username){
            $q->andWhere('u.username = ?',$username);
        }

        if(''!=$email){
            $q->andWhere('ud.email = ?',$email);
        }
        return $q;
        //         $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
        //         return $result[0];
        // echo "<pre>";print_r($result);die;

    }

    //for block user's transaction rights
    public function setBlockPaymentRights($user_id){
        $q = Doctrine_Query::create()
        ->update('UserDetail')
        ->set('payment_rights', 1)
        ->where('user_id =?',$user_id);
        $res = $q->execute();
        if($res){
            return true;
        }else{
            return false;
        }

    }
    //for unblock user's transaction rights
    public function setUnblockPaymentRights($user_id){
        try{
            $q = Doctrine_Query::create()
                ->update('UserDetail')
                ->set('payment_rights',0)
                ->where('user_id =?',$user_id);
            $res = $q->execute();
            if($res){
                return true;
            }else{
                return false;
            }
            
        }catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }
/**
 *
 * @param <type> $userId
 * 
 */
    public function getDetailByUserId($userId){
    try{
      $query= $this->createQuery('ud')
      ->select('ud.*')      
      ->where('ud.user_id = ?', $userId);
       $res = $query->execute(array(),Doctrine::HYDRATE_ARRAY);

      if(count($res) == 0) {
        return 0;
      }
      return $res;
    }
      catch (Exception $e) {
      throw New Exception ($e->getMessage());
    }
  }

    public function checkUniqueEmail($userId, $email){
        $q = $this->createQuery('ud')
        ->select('ud.id')
        ->Where('ud.email = ?',$email);
        if($userId != '' || $userId != NULL){
            $q->andWhere('ud.id != ?',$userId);
        }

        $result = $q->execute();
        return $result->count();
    }

    /**function getAllActiveUser()
   *@purpose :get all active user from db
   *@param : $fname='',$lname='',$email=''
   *@return :  list of all active users
   *@author : KSingh
   *@date : 29-04-2011
   */

    public function getAllActiveUser($fname='',$lname='',$email='',$paymentGroupId='',$status = ''){
        $q = $this->createQuery('ud')
        ->select('ud.*,u.id,ae.username as username,ae.created_at as last_login,ug.user_id')
        ->leftJoin('ud.sfGuardUser u')
        ->leftJoin('u.sfGuardUserGroup ug')
        ->leftJoin('u.EpActionAuditEvent ae')
        ->leftJoin('u.PaymentPrivilageUser ppu')
        ->Where('ud.deleted = ?','0')
        ->andWhere('ug.group_id = ?',$paymentGroupId)
        ->andWhere('ae.subcategory = ?','Login');
        if(isset($fname) && !empty($fname)){
          $q->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $q->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $q->andWhere('ud.email=?',$email);
        }
        if(isset($status) && !empty($status)){
          $q->andWhere('ppu.status=?',$status);
        }
        $q->orderBy('ae.created_at DESC');
        
        return $q;
    }
        /**
         * @purpose :get all active user from db having user id of user_transaction_limit table
         * @param <type> $fname
         * @param <type> $lname
         * @param <type> $email
         * @param <type> $paymentGroupId
         * @return $query
         * @author : VChandwani
         * @date : 31-05-2011
         */
        public function getAllUser($fname='',$lname='',$email='',$paymentGroupId=''){
        $query = $this->createQuery('ud')
        ->select('ud.*,u.id,ae.username as username,ae.created_at as last_login,ug.user_id,utl.user_id as user_id_user_transaction')
        ->leftJoin('ud.sfGuardUser u')
        ->leftJoin('u.sfGuardUserGroup ug')
        ->leftJoin('u.EpActionAuditEvent ae')
        ->leftJoin('u.UserTransactionLimit utl')
        ->Where('ud.deleted = ?','0')
        ->andWhere('ug.group_id = ?',$paymentGroupId)
        ->andWhere('ae.subcategory = ?','Login');
        if(isset($fname) && !empty($fname)){
          $query->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $query->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $query->andWhere('ud.email=?',$email);
        }
        $query->orderBy('ae.created_at DESC');

        return $query;
    }
        public function getSearchedUser($email){
        $query = $this->createQuery('ud')
        ->select('ud.*');
        if(isset($email) && !empty($email)){
          $query->andWhere('ud.email=?',$email);
        }
        return $query;
    }

     public function getUserDetailList($email='', $phone='', $userId='')
      {
        $query = Doctrine_Query::create()
        ->select("t.*,sf.username")
        ->from("UserDetail t")
        ->leftJoin('t.sfGuardUser sf')
        ->where('t.user_id !=?','');
        if($email != '')
        {
          $query->andWhere("t.email LIKE '%".$email."%'");
        }
        if($phone != '')
        $query->andWhere("t.mobile_phone LIKE '%".$phone."%'");
        if($userId != '')
        $query->andWhere('t.user_id =?', $userId);

        $query->orderBy("t.id desc");
    //          echo "<pre>";print_r($query->execute()->toArray());die;
        return $query;
      }
    public function checkUniqueEmailWhileUpdating($userId,$email){
        $query = Doctrine_Query::create()
        ->select('t.*')
        ->from('UserDetail t')
        ->where('t.email = ?',$email)
        ->andWhere('t.user_id !=?',$userId)
        ->execute()->toArray();
        if(!empty($query) && $query != ''){
            return $query;
        }else{
            return false;
        }
    }
    public function checkUniquePhoneWhileUpdating($userId,$phone){
        $query = Doctrine_Query::create()
        ->select('t.*')
        ->from('UserDetail t')
        ->where('t.mobile_phone = ?',$phone)
        ->andWhere('t.user_id !=?',$userId)
        ->execute()->toArray();
        if(!empty($query) && $query != ''){
            return $query;
        }else{
            return false;
        }
    }

    public function getPaymentUserByEmail($email = ''){

        if(!empty($email)){
            $query = $this->createQuery('ud')
                    ->select('ud.*, u.*, ug.*')
                    ->leftJoin('ud.sfGuardUser u')
                    ->leftJoin('u.sfGuardUserGroup ug')                    
                    ->where('ug.group_id = ?',3)
                    ->andWhere('ud.email = ?', $email);
            //die($query->getSqlQuery());
            $result = $query->execute();
            return $result;
        }else{
            return false;
        }
        
    }

     public function getPaymentUserFieldsByEmail($email = '', $fields=''){

        if(!empty($email)){
            $userData = Doctrine::getTable('UserDetail')->createQuery('u')
                ->select($fields)
                ->where('u.email = ?',$email)
               ->execute(array(),Doctrine::HYDRATE_ARRAY);
            return $userData;
        }else{
            return false;
        }

    }

    /**
    * [WP: 107] => CR: 152
    * @return <type>
    * This function will return query for all active payment users...
    */
    public function getAllActivePaymentUserQuery($fname='', $lname='', $email=''){
        $q = $this->createQuery('ud')
        ->select('ud.*,u.id, u.username,u.last_login')
        ->leftJoin('ud.sfGuardUser u')
        ->leftJoin('u.sfGuardUserGroup ug')
        ->where('ug.group_id = ?',3);
        
        if(isset($fname) && !empty($fname)){
          $q->andWhere('ud.first_name=?',$fname);
        }
        if(isset($lname) && !empty($lname)){
          $q->andWhere('ud.last_name=?',$lname);
        }
        if(isset($email) && !empty($email)){
          $q->andWhere('ud.email=?',$email);
        }
        
        $q->orderBy('ud.updated_at DESC');

        return $q;
        
    }//End of public function getAllActivePaymentUserQuery($fname='', $lname='', $email=''){...

}