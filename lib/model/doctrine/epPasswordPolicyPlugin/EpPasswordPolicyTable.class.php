<?php


class EpPasswordPolicyTable extends PluginEpPasswordPolicyTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpPasswordPolicy');
    }
}