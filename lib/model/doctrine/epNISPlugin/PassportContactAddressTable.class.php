<?php


class PassportContactAddressTable extends PluginPassportContactAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportContactAddress');
    }
}