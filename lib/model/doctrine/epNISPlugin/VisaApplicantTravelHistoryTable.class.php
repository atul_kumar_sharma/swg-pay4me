<?php


class VisaApplicantTravelHistoryTable extends PluginVisaApplicantTravelHistoryTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplicantTravelHistory');
    }

    public function getTravelHistory($getAppId){
      $visa_application=Doctrine_Query::create()
      ->select("vat.*,vpc.*")
      ->from('VisaApplicantTravelHistory vat')
      ->leftJoin('vat.Country vpc')
      ->where("vat.application_id =". $getAppId)
      ->execute()->toArray(true);
      return $visa_application;
    }

}