<?php


class IPaymentResponseTable extends PluginIPaymentResponseTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('IPaymentResponse');
    }


    // Delete Entry from IPaymentResponse Table for Unpay Visa & Passport Applications
    public function deleteRecord($order_no)
    {
        try{
            if(isset ($order_no) && $order_no!=''){
                $query = Doctrine_Query::create()
                        ->delete('ipr.*')
                        ->from('IPaymentResponse ipr')
                        ->where('ipr.order_number=?',$order_no)->limit(1);
                $order_no = $query->execute();
                if($order_no){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());            
        }
    }
}