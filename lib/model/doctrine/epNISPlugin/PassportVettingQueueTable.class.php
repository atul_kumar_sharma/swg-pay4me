<?php


class PassportVettingQueueTable extends PluginPassportVettingQueueTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportVettingQueue');
    }

    // Delete Entry from PassportVettingQueue Table for Unpay Visa & Passport Applications
    public function deletePassportRecord($app_id)
    {
        try{
            if(isset ($app_id) && $app_id!=''){
                $query = Doctrine_Query::create()
                       ->delete('pv.*')
                       ->from('PassportVettingQueue pv')
                       ->where('pv.application_id =?',$app_id)->limit(1);
                $app_id = $query->execute();
                if($app_id){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());
        }
    }
}