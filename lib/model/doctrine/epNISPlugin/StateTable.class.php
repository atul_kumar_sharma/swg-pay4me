<?php


class StateTable extends PluginStateTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('State');
    }
}