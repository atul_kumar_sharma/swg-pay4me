<?php


class ExecutionStateTable extends PluginExecutionStateTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('ExecutionState');
    }

    // Delete Entry from ExecutionState Table for Unpay Visa & Passport Applications
    public function deleteRecord($execution_id)
    {
        try{
            if(isset ($execution_id) && $execution_id!=''){
                $query = Doctrine_Query::create()
                ->delete('e.*')
                ->from('ExecutionState e')
                ->where('e.execution_id=?',$execution_id)->limit(1);
                $execution_id = $query->execute();
                if($execution_id){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());
        }
    }
}