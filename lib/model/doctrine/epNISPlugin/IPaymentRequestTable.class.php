<?php


class IPaymentRequestTable extends PluginIPaymentRequestTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('IPaymentRequest');
    }

    public function getId($appID,$refNo,$appType)
        {
          if(!empty($appID))
          {
          if($appType == 'visa')
          {
              
             $q = Doctrine_Query::create()
               ->select('*')
               ->from('IPaymentRequest')
               ->where("visa_id='$appID'")
               ->execute(array(),Doctrine::HYDRATE_ARRAY);
               if(!empty($q))
               {
                   return $q;
               }
               else
               {
                   return false;
               }
             

               
              
               return $q;
          }

          if($appType == 'passport')
          {
             $q = Doctrine_Query::create()
               ->select('t.id')
               ->from('IPaymentRequest t')
               ->where('t.passport_id =?',$appID)
               ->execute(array(),Doctrine::HYDRATE_ARRAY);
               return $q;
          }
          if($appType == 'freezone')
          {
             $q = Doctrine_Query::create()
               ->select('t.id')
               ->from('IPaymentRequest t')
               ->where('t.freezone_id =?',$appID)
               ->execute(array(),Doctrine::HYDRATE_ARRAY);
               return $q;
          }

            if($appType == 'vap')
            {

                $q = Doctrine_Query::create()
                    ->select('*')
                    ->from('IPaymentRequest')
                    ->where("visa_arrival_program_id='$appID'")
                    ->execute(array(),Doctrine::HYDRATE_ARRAY);
                if(!empty($q))
                {
                    return $q;
          }
                else
                {
                    return false;
        }




                return $q;
            }
        }
    }

        public function getAppId($itemId)
        {
            $q = Doctrine_Query::create()
               ->select('t.*')
               ->from('IPaymentRequest t')
               ->where('t.id =?',$itemId)
               ->execute(array(),Doctrine::HYDRATE_ARRAY);
                return $q;
        }
}