<?php


class VisaApprovalRecommendationTable extends PluginVisaApprovalRecommendationTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApprovalRecommendation');
    }
}