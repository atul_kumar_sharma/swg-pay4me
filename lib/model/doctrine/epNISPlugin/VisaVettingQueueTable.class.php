<?php


class VisaVettingQueueTable extends PluginVisaVettingQueueTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaVettingQueue');
    }

    // Delete Entry from VisaVettingQueue Table for Unpay Visa & Passport Applications
    public function deleteVisaRecord($app_id)
    {
        try{
            if(isset ($app_id) && $app_id!=''){
                $query = Doctrine_Query::create()
                ->delete('pv.*')
                ->from('VisaVettingQueue vq')
                ->where('vq.application_id =?',$app_id)->limit(1);
                $app_id = $query->execute();
                if($app_id){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());
        }
    }
}