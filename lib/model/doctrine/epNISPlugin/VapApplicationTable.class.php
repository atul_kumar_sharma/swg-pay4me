<?php


class VapApplicationTable extends PluginVapApplicationTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('VapApplication');
    }

    public function checkDuplicateApplication($first_name, $last_name, $gender_id, $date_of_birth, $place_of_birth, $email)
    {
        if($first_name != '' && $last_name != '' && $gender_id != '' && $date_of_birth != ''  && $place_of_birth != '' && $email != ''){
            $query = Doctrine_Query::create()
            ->select("pa.id AS appId, pa.ref_no AS ref_no, pa.status AS status, pa.contagious_disease AS contagious_disease, pa.police_case AS police_case, pa.narcotic_involvement AS narcotic_involvement, pa.paid_date, pa.remarks")
            ->from("VapApplication pa");

            $query->where("pa.first_name = ?", strtolower($first_name))
            ->andWhere("pa.surname = ?", strtolower($last_name))
            ->andWhere("pa.gender = ?", $gender_id)
            ->andWhere("pa.email = ?", $email)
            ->andWhere("pa.date_of_birth = ?", $date_of_birth)
            ->andWhere("pa.place_of_birth = ?", strtolower($place_of_birth));
            $query->orderBy('pa.id DESC');
            $query->limit(1);

            $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

            if(count($result) > 0){
                return $result;
            }else{
                return array();
            }
        }else{
            return 0;
        }
    }
    public function getVisaFreshRecordSummery($id)
    {
        $visa_application=Doctrine_Query::create()
        ->select("VA.*,voa.*,voc.*,vpc.*,C.*")
        ->from('VapApplication VA')
        //      ->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
        //->leftJoin('VA.PreviousCountry pc','pc.id = VA.previous_nationality_id')
        //->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
        //->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
        //->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
        ->leftJoin('VA.VapVisaOfficeAddress voa')
        //->leftJoin('VA.VapVisaPermanentAddress vpa')
        //      ->leftJoin('VA.VisaApplicationDetails vad')
        //      ->leftJoin('vad.VisaIntendedAddressNigeriaAddress vna')
        //      ->leftJoin('vad.VisaRelativeEmployerAddress vrea')
        //      ->leftJoin('vrea.Country vrc')
        ->leftJoin('voa.Country voc')
        //->leftJoin('vpa.Country vpc')
        //      ->leftJoin('VAI.VisaProcessingCentre vcp')
        //      ->leftJoin('vna.State s')
        //      ->leftJoin('vna.LGA l')
        //      ->leftJoin('VAI.VisaTypeReason vtr')
        //      ->leftJoin('VAI.DeportedCountry dc')
        //      ->leftJoin('VAI.EntryType et')
        //      ->leftJoin('VAI.ApplyingCountry vaic')
        ->where("VA.id =". $id)
        ->execute()->toArray(true);
        return $visa_application;
    }

    public function getVisaFreshRecord($id)
    {
        $visa_application=Doctrine_Query::create()
        ->select("VA.*,C.country_name")
        ->from('VapApplication VA')
        //->leftJoin('VA.VisaApplicantInfo VAI')
        ->leftJoin('VA.CurrentCountry C','C.id = VA.present_nationality_id')
        //->leftJoin('VA.VisaCategory VC','VC.id = VA.visacategory_id')
        //->leftJoin('VAI.EmbassyMaster EM','EM.id = VAI.embassy_of_pref_id')
        //->leftJoin('VAI.VisaTypeId VT','VT.id = VAI.visatype_id')
        ->where("VA.id =". $id)
        ->execute()->toArray(true);
        return $visa_application;
    }

    public function getProcessingCountry($appId) {
        $Result = Doctrine_Query::create()
        ->select("*")
        ->from('VapApplication a')
        //->leftJoin('a.VisaProcessingCentreTable b')
        ->where('a.id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);

        if(count($Result) > 0){
            return $Result[0]['applying_country_id'];
        }
        return false;
        //return $Result['0']['applying_country_id'];
    }
    public function getVisaAppIdRefId($appId, $refId)
    {
        $q = $this->createQuery('j')
        ->select('id')
        ->where('j.id = ?', $appId)
        ->addwhere('j.ref_no = ?', $refId)->execute()->toArray();

        if(count($q) > 0) {
            return $q[0]['id'];
        }
        return false;
    }

    public function getVisaArrivalInfo($appId,$refNo='')
    {
        if($refNo=='')
        {
            $Result = $this->createQuery('pa')
            ->select("pa.*")
            ->where("pa.id = ".$appId)
            ->execute(array(), Doctrine::HYDRATE_ARRAY);
        }
        else
        {
            $Result = $this->createQuery('pa')
            ->select("pa.*")
            ->where("pa.id = ".$appId)
            ->andWhere("pa.ref_no = ".$refNo)
            ->execute(array(), Doctrine::HYDRATE_ARRAY);
        }

        if(count($Result) > 0)
        return $Result;
        else
        return false;
    }



    public function getVisaInfoById($visaId)
    {
        $query = Doctrine_Query::create()
        ->select("pa.id,pa.ref_no,pa.title, pa.first_name,pa.middle_name,pa.surname,pa.date_of_birth,pa.email,pa.status,pa.paid_dollar_amount,pa.paid_date")
        ->from("VapApplication pa")
        ->where('pa.id= ?',$visaId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);
        return $query;

    }
    public function findDuplicateVapApplication($firstName = null, $dob = null,$appType = null,$id = null)
    {
        $payObj = new paymentHelper();
        $ipay4meGateWayId = $payObj->getiPayformeGatewayTypeId();
        $query = Doctrine_Query::create()
        ->select("pa.id,t.id,crt.id,pa.id as app_id,pa.ref_no as ref_no,concat(pa.title,' ' , pa.first_name,' ' ,pa.middle_name,' ' ,pa.surname) as name,pa.date_of_birth as dob,pa.status as status,pa.paid_date as paid_on,group_concat( cast( ifnull( ips.order_number, 0 ) AS char( 50 ) ) ORDER BY ifnull( ips.order_number, 0 ) DESC ) AS order_no")
        ->from("VapApplication pa");
        $query->leftJoin("pa.IPaymentRequest t on t.visa_arrival_program_id = pa.id")
        ->leftJoin("t.CartItemsTransactions crt")
        ->leftJoin("crt.IPaymentResponse ips");
        if(isset ($firstName) && $firstName!='' && isset ($dob) && $dob!='')
        {
            $query->where("pa.first_name=?",$firstName)
            ->andWhere("pa.date_of_birth=?",$dob);
        }else if(isset ($id) && $id!=''){
            $query->where("pa.id=$id");
            $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")->groupBy("pa.id");
            return $data = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
        }
        $query->andWhere("pa.status IN ('New') OR ips.transaction_number IS NOT NULL OR pa.payment_gateway_id !=$ipay4meGateWayId")
        ->groupBy("pa.id");
        return $query;
        //      $query = $query->execute(array(),Doctrine::HYDRATE_ARRAY);
        //            echo "<pre>";print_r($query);die;
    }

    public function getPaidAmount($appId) {
        $fee = $this->createQuery('a')
        ->where('a.id = ?', $appId)
        ->execute(array(),Doctrine::HYDRATE_ARRAY);


        return $fee['0']['paid_dollar_amount'];
    }

    public function getVapApplicationDetails($appId,$refNo){
        $appArray = Doctrine_Query::create()
        ->select('VAP.*,VPC.var_value as processing_center_name')
        ->from('VapApplication VAP')
        ->leftJoin('VAP.VapProcessingCentre VPC')
        ->where('VAP.id = ?',$appId)
        ->andWhere('VAP.ref_no = ?',$refNo)
        ->execute()->toArray();
        //                              echo "<pre>";print_r($appArray);die;
        if(isset($appArray) && $appArray != ''){
            return $appArray;

        }else{
            return $a = array();
        }

    }

    /**
     *
     * @return <type>
     * This function return applications whose status is 'Paid' and and paid date lies between last 5 days except today.
     */
    public function getVisaArrivalApplication(){        
        $no_of_months =  (int)sfConfig::get('app_visa_arrival_flight_update_limit')/30;
        $expire_days_range = sfConfig::get('app_visa_arrival_expire_days_range');        
        $expiry_start_date = date('Y-m-d', mktime(0,0,0,date("m")-$no_of_months,date("d")-$expire_days_range,date("y")));
        $expiry_end_date = date('Y-m-d', mktime(0,0,0,date("m")-$no_of_months,date("d"),date("y")));        
        $query = Doctrine_Query::create()
                  ->select('vap.id AS id, vap.paid_date')
                  ->from('VapApplication vap')
                  ->where('vap.status = ?', 'Paid')
                  ->andWhere('vap.paid_date BETWEEN ? AND ?',array('0'=>$expiry_start_date,'1'=> $expiry_end_date));
        
        $result = $query->execute();
        return $result;
    }

    // Update Entry from VisaApplication Table for Unpay Visa & Passport Applications
  public function updateVapApp($app_id){
    try {
      $q = Doctrine_Query::create()
      ->update('VapApplication v')
      ->set('v.payment_gateway_id', 'NULL')
      ->set('v.status', '?', 'New')
      ->set('paid_date', 'NULL')
      ->set('payment_trans_id', 'NULL')
      ->set('paid_dollar_amount', 'NULL')
      ->set('paid_naira_amount ', 'NULL')
      ->set('ispaid', 'NULL')
      ->where("v.id = ? ", $app_id)
      ->limit(1);
      $res = $q->execute();

      if($res){
        return true;
      }else{
        return false;
      }

    }catch(Exception $e){
      throw New Exception ($e->getMessage());     
    }
  }
   
}