<?php


class CurrencyMasterTable extends PluginCurrencyMasterTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CurrencyMaster');
    }
}