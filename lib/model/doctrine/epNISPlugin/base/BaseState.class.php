<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('State', 'connection_nis');

/**
 * BaseState
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $state_name
 * @property string $country_id
 * @property boolean $is_mrp_state
 * @property boolean $is_official_state
 * @property boolean $is_mrp_seamans_state
 * @property Doctrine_Collection $PassportOffice
 * @property Doctrine_Collection $PassportApplicantContactinfo
 * @property Doctrine_Collection $PassportApplication
 * @property Doctrine_Collection $PassportApplicationDetails
 * @property Doctrine_Collection $ReEntryVisaApplication
 * @property Doctrine_Collection $AddressMaster
 * @property Doctrine_Collection $VisaOffice
 * 
 * @method integer             getId()                           Returns the current record's "id" value
 * @method string              getStateName()                    Returns the current record's "state_name" value
 * @method string              getCountryId()                    Returns the current record's "country_id" value
 * @method boolean             getIsMrpState()                   Returns the current record's "is_mrp_state" value
 * @method boolean             getIsOfficialState()              Returns the current record's "is_official_state" value
 * @method boolean             getIsMrpSeamansState()            Returns the current record's "is_mrp_seamans_state" value
 * @method Doctrine_Collection getPassportOffice()               Returns the current record's "PassportOffice" collection
 * @method Doctrine_Collection getPassportApplicantContactinfo() Returns the current record's "PassportApplicantContactinfo" collection
 * @method Doctrine_Collection getPassportApplication()          Returns the current record's "PassportApplication" collection
 * @method Doctrine_Collection getPassportApplicationDetails()   Returns the current record's "PassportApplicationDetails" collection
 * @method Doctrine_Collection getReEntryVisaApplication()       Returns the current record's "ReEntryVisaApplication" collection
 * @method Doctrine_Collection getAddressMaster()                Returns the current record's "AddressMaster" collection
 * @method Doctrine_Collection getVisaOffice()                   Returns the current record's "VisaOffice" collection
 * @method State               setId()                           Sets the current record's "id" value
 * @method State               setStateName()                    Sets the current record's "state_name" value
 * @method State               setCountryId()                    Sets the current record's "country_id" value
 * @method State               setIsMrpState()                   Sets the current record's "is_mrp_state" value
 * @method State               setIsOfficialState()              Sets the current record's "is_official_state" value
 * @method State               setIsMrpSeamansState()            Sets the current record's "is_mrp_seamans_state" value
 * @method State               setPassportOffice()               Sets the current record's "PassportOffice" collection
 * @method State               setPassportApplicantContactinfo() Sets the current record's "PassportApplicantContactinfo" collection
 * @method State               setPassportApplication()          Sets the current record's "PassportApplication" collection
 * @method State               setPassportApplicationDetails()   Sets the current record's "PassportApplicationDetails" collection
 * @method State               setReEntryVisaApplication()       Sets the current record's "ReEntryVisaApplication" collection
 * @method State               setAddressMaster()                Sets the current record's "AddressMaster" collection
 * @method State               setVisaOffice()                   Sets the current record's "VisaOffice" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseState extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_state');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('state_name', 'string', 150, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 150,
             ));
        $this->hasColumn('country_id', 'string', 2, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 2,
             ));
        $this->hasColumn('is_mrp_state', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('is_official_state', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('is_mrp_seamans_state', 'boolean', null, array(
             'type' => 'boolean',
             ));

        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('PassportOffice', array(
             'local' => 'id',
             'foreign' => 'office_state_id'));

        $this->hasMany('PassportApplicantContactinfo', array(
             'local' => 'id',
             'foreign' => 'state_of_origin'));

        $this->hasMany('PassportApplication', array(
             'local' => 'id',
             'foreign' => 'processing_state_id'));

        $this->hasMany('PassportApplicationDetails', array(
             'local' => 'id',
             'foreign' => 'stateoforigin'));

        $this->hasMany('ReEntryVisaApplication', array(
             'local' => 'id',
             'foreign' => 'visa_state_id'));

        $this->hasMany('AddressMaster', array(
             'local' => 'id',
             'foreign' => 'state'));

        $this->hasMany('VisaOffice', array(
             'local' => 'id',
             'foreign' => 'office_state_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}