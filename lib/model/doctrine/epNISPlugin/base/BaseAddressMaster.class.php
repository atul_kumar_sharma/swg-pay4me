<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('AddressMaster', 'connection_nis');

/**
 * BaseAddressMaster
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $country_id
 * @property integer $state
 * @property integer $lga_id
 * @property string $district
 * @property string $postcode
 * @property string $var_type
 * @property LGA $LGA
 * @property State $State
 * @property Country $Country
 * 
 * @method integer       getId()         Returns the current record's "id" value
 * @method string        getAddress1()   Returns the current record's "address_1" value
 * @method string        getAddress2()   Returns the current record's "address_2" value
 * @method string        getCity()       Returns the current record's "city" value
 * @method string        getCountryId()  Returns the current record's "country_id" value
 * @method integer       getState()      Returns the current record's "state" value
 * @method integer       getLgaId()      Returns the current record's "lga_id" value
 * @method string        getDistrict()   Returns the current record's "district" value
 * @method string        getPostcode()   Returns the current record's "postcode" value
 * @method string        getVarType()    Returns the current record's "var_type" value
 * @method LGA           getLGA()        Returns the current record's "LGA" value
 * @method State         getState()      Returns the current record's "State" value
 * @method Country       getCountry()    Returns the current record's "Country" value
 * @method AddressMaster setId()         Sets the current record's "id" value
 * @method AddressMaster setAddress1()   Sets the current record's "address_1" value
 * @method AddressMaster setAddress2()   Sets the current record's "address_2" value
 * @method AddressMaster setCity()       Sets the current record's "city" value
 * @method AddressMaster setCountryId()  Sets the current record's "country_id" value
 * @method AddressMaster setState()      Sets the current record's "state" value
 * @method AddressMaster setLgaId()      Sets the current record's "lga_id" value
 * @method AddressMaster setDistrict()   Sets the current record's "district" value
 * @method AddressMaster setPostcode()   Sets the current record's "postcode" value
 * @method AddressMaster setVarType()    Sets the current record's "var_type" value
 * @method AddressMaster setLGA()        Sets the current record's "LGA" value
 * @method AddressMaster setState()      Sets the current record's "State" value
 * @method AddressMaster setCountry()    Sets the current record's "Country" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAddressMaster extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_address');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('address_1', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('address_2', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('city', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('country_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('state', 'integer', 20, array(
             'type' => 'integer',
             'length' => 20,
             ));
        $this->hasColumn('lga_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('district', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('postcode', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('var_type', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));

        $this->option('type', 'InnoDB');

        $this->setSubClasses(array(
             'VisaApplicantPreviousHistoryAddress' => 
             array(
              'var_type' => 'visa_applicant_previous_history_address',
             ),
             'VisaIntendedAddressNigeriaAddress' => 
             array(
              'var_type' => 'visa_intended_nigeria_address',
             ),
             'PassportFatherAddress' => 
             array(
              'var_type' => 'passport_father_address',
             ),
             'PassportMotherAddress' => 
             array(
              'var_type' => 'passport_mother_address',
             ),
             'PassportReferenceAddress' => 
             array(
              'var_type' => 'passport_reference_address',
             ),
             'PassportKinAddress' => 
             array(
              'var_type' => 'passport_kin_address',
             ),
             'PassportPermanentAddress' => 
             array(
              'var_type' => 'passport_permanent_address',
             ),
             'ReEntryVisaReferencesAddress' => 
             array(
              'var_type' => 'reentry_visa_references_address',
             ),
             'VapLocalComanyAddress' => 
             array(
              'var_type' => 'vap_visa_local_company_address',
             ),
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('LGA', array(
             'local' => 'lga_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('State', array(
             'local' => 'state',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('Country', array(
             'local' => 'country_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));
    }
}