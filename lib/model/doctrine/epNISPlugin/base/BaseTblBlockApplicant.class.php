<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('TblBlockApplicant', 'connection_nis');

/**
 * BaseTblBlockApplicant
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property varchar $first_name
 * @property varchar $middle_name
 * @property varchar $last_name
 * @property timestamp $dob
 * @property string $card_first
 * @property string $card_last
 * @property integer $app_id
 * @property integer $ref_no
 * @property varchar $app_type
 * @property string $card_holder_name
 * @property string $email
 * @property string $processing_country
 * @property integer $embassy
 * @property enum $blocked
 * @property EmbassyMaster $EmbassyMaster
 * @property Country $Country
 * 
 * @method varchar           getFirstName()          Returns the current record's "first_name" value
 * @method varchar           getMiddleName()         Returns the current record's "middle_name" value
 * @method varchar           getLastName()           Returns the current record's "last_name" value
 * @method timestamp         getDob()                Returns the current record's "dob" value
 * @method string            getCardFirst()          Returns the current record's "card_first" value
 * @method string            getCardLast()           Returns the current record's "card_last" value
 * @method integer           getAppId()              Returns the current record's "app_id" value
 * @method integer           getRefNo()              Returns the current record's "ref_no" value
 * @method varchar           getAppType()            Returns the current record's "app_type" value
 * @method string            getCardHolderName()     Returns the current record's "card_holder_name" value
 * @method string            getEmail()              Returns the current record's "email" value
 * @method string            getProcessingCountry()  Returns the current record's "processing_country" value
 * @method integer           getEmbassy()            Returns the current record's "embassy" value
 * @method enum              getBlocked()            Returns the current record's "blocked" value
 * @method EmbassyMaster     getEmbassyMaster()      Returns the current record's "EmbassyMaster" value
 * @method Country           getCountry()            Returns the current record's "Country" value
 * @method TblBlockApplicant setFirstName()          Sets the current record's "first_name" value
 * @method TblBlockApplicant setMiddleName()         Sets the current record's "middle_name" value
 * @method TblBlockApplicant setLastName()           Sets the current record's "last_name" value
 * @method TblBlockApplicant setDob()                Sets the current record's "dob" value
 * @method TblBlockApplicant setCardFirst()          Sets the current record's "card_first" value
 * @method TblBlockApplicant setCardLast()           Sets the current record's "card_last" value
 * @method TblBlockApplicant setAppId()              Sets the current record's "app_id" value
 * @method TblBlockApplicant setRefNo()              Sets the current record's "ref_no" value
 * @method TblBlockApplicant setAppType()            Sets the current record's "app_type" value
 * @method TblBlockApplicant setCardHolderName()     Sets the current record's "card_holder_name" value
 * @method TblBlockApplicant setEmail()              Sets the current record's "email" value
 * @method TblBlockApplicant setProcessingCountry()  Sets the current record's "processing_country" value
 * @method TblBlockApplicant setEmbassy()            Sets the current record's "embassy" value
 * @method TblBlockApplicant setBlocked()            Sets the current record's "blocked" value
 * @method TblBlockApplicant setEmbassyMaster()      Sets the current record's "EmbassyMaster" value
 * @method TblBlockApplicant setCountry()            Sets the current record's "Country" value
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseTblBlockApplicant extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_block_applicant');
        $this->hasColumn('first_name', 'varchar', 150, array(
             'type' => 'varchar',
             'notnull' => true,
             'length' => 150,
             ));
        $this->hasColumn('middle_name', 'varchar', 150, array(
             'type' => 'varchar',
             'notnull' => false,
             'length' => 150,
             ));
        $this->hasColumn('last_name', 'varchar', 150, array(
             'type' => 'varchar',
             'notnull' => true,
             'length' => 150,
             ));
        $this->hasColumn('dob', 'timestamp', null, array(
             'type' => 'timestamp',
             'notnull' => true,
             ));
        $this->hasColumn('card_first', 'string', 5, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 5,
             ));
        $this->hasColumn('card_last', 'string', 5, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 5,
             ));
        $this->hasColumn('app_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('ref_no', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('app_type', 'varchar', 50, array(
             'type' => 'varchar',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('card_holder_name', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 255,
             ));
        $this->hasColumn('email', 'string', 100, array(
             'type' => 'string',
             'notnull' => false,
             'length' => 100,
             ));
        $this->hasColumn('processing_country', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('embassy', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('blocked', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'yes',
              1 => 'no',
             ),
             'default' => 'yes',
             ));

        $this->option('type:', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('EmbassyMaster', array(
             'local' => 'embassy',
             'foreign' => 'id'));

        $this->hasOne('Country', array(
             'local' => 'processing_country',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $sftrackable0 = new sfTrackable();
        $this->actAs($timestampable0);
        $this->actAs($sftrackable0);
    }
}