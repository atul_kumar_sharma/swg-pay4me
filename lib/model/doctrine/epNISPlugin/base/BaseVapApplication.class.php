<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('VapApplication', 'connection_nis');

/**
 * BaseVapApplication
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $ref_no
 * @property enum $title
 * @property string $first_name
 * @property string $surname
 * @property string $middle_name
 * @property enum $gender
 * @property enum $marital_status
 * @property string $email
 * @property date $date_of_birth
 * @property string $place_of_birth
 * @property string $present_nationality_id
 * @property enum $hair_color
 * @property enum $eyes_color
 * @property string $id_marks
 * @property integer $height
 * @property integer $office_address_id
 * @property string $office_phone_no
 * @property enum $applicant_type
 * @property integer $vap_company_id
 * @property string $customer_service_number
 * @property string $document_1
 * @property string $document_2
 * @property string $document_3
 * @property string $document_4
 * @property string $document_5
 * @property string $boarding_place
 * @property string $flight_carrier
 * @property string $flight_number
 * @property string $issusing_govt
 * @property string $passport_number
 * @property date $date_of_issue
 * @property date $date_of_exp
 * @property string $place_of_issue
 * @property string $applying_country_id
 * @property string $type_of_visa
 * @property decimal $trip_money
 * @property string $local_company_name
 * @property integer $local_company_address_id
 * @property enum $contagious_disease
 * @property enum $police_case
 * @property enum $narcotic_involvement
 * @property enum $deported_status
 * @property string $deported_county_id
 * @property enum $visa_fraud_status
 * @property string $processing_country_id
 * @property integer $processing_center_id
 * @property boolean $ispaid
 * @property boolean $is_email_valid
 * @property integer $payment_gateway_id
 * @property decimal $paid_dollar_amount
 * @property decimal $paid_naira_amount
 * @property date $paid_date
 * @property date $arrival_date
 * @property enum $status
 * @property string $payment_trans_id
 * @property string $business_address
 * @property enum $sponsore_type
 * @property text $remarks
 * @property VapCompany $VapCompany
 * @property VapVisaOfficeAddress $VapVisaOfficeAddress
 * @property VapVisaPermanentAddress $VapVisaPermanentAddress
 * @property VapLocalComanyAddress $VapLocalComanyAddress
 * @property VapProcessingCentre $VapProcessingCentre
 * @property VisaType $VapVisaTypeReason
 * @property Country $VapApplyingCountry
 * @property Country $VapDeportedCountry
 * @property Country $CurrentCountry
 * @property Country $ProcessingCountry
 * @property PaymentGatewayType $PaymentGatewayType
 * @property Doctrine_Collection $IPaymentRequest
 * 
 * @method integer                 getId()                       Returns the current record's "id" value
 * @method integer                 getRefNo()                    Returns the current record's "ref_no" value
 * @method enum                    getTitle()                    Returns the current record's "title" value
 * @method string                  getFirstName()                Returns the current record's "first_name" value
 * @method string                  getSurname()                  Returns the current record's "surname" value
 * @method string                  getMiddleName()               Returns the current record's "middle_name" value
 * @method enum                    getGender()                   Returns the current record's "gender" value
 * @method enum                    getMaritalStatus()            Returns the current record's "marital_status" value
 * @method string                  getEmail()                    Returns the current record's "email" value
 * @method date                    getDateOfBirth()              Returns the current record's "date_of_birth" value
 * @method string                  getPlaceOfBirth()             Returns the current record's "place_of_birth" value
 * @method string                  getPresentNationalityId()     Returns the current record's "present_nationality_id" value
 * @method enum                    getHairColor()                Returns the current record's "hair_color" value
 * @method enum                    getEyesColor()                Returns the current record's "eyes_color" value
 * @method string                  getIdMarks()                  Returns the current record's "id_marks" value
 * @method integer                 getHeight()                   Returns the current record's "height" value
 * @method integer                 getOfficeAddressId()          Returns the current record's "office_address_id" value
 * @method string                  getOfficePhoneNo()            Returns the current record's "office_phone_no" value
 * @method enum                    getApplicantType()            Returns the current record's "applicant_type" value
 * @method integer                 getVapCompanyId()             Returns the current record's "vap_company_id" value
 * @method string                  getCustomerServiceNumber()    Returns the current record's "customer_service_number" value
 * @method string                  getDocument1()                Returns the current record's "document_1" value
 * @method string                  getDocument2()                Returns the current record's "document_2" value
 * @method string                  getDocument3()                Returns the current record's "document_3" value
 * @method string                  getDocument4()                Returns the current record's "document_4" value
 * @method string                  getDocument5()                Returns the current record's "document_5" value
 * @method string                  getBoardingPlace()            Returns the current record's "boarding_place" value
 * @method string                  getFlightCarrier()            Returns the current record's "flight_carrier" value
 * @method string                  getFlightNumber()             Returns the current record's "flight_number" value
 * @method string                  getIssusingGovt()             Returns the current record's "issusing_govt" value
 * @method string                  getPassportNumber()           Returns the current record's "passport_number" value
 * @method date                    getDateOfIssue()              Returns the current record's "date_of_issue" value
 * @method date                    getDateOfExp()                Returns the current record's "date_of_exp" value
 * @method string                  getPlaceOfIssue()             Returns the current record's "place_of_issue" value
 * @method string                  getApplyingCountryId()        Returns the current record's "applying_country_id" value
 * @method string                  getTypeOfVisa()               Returns the current record's "type_of_visa" value
 * @method decimal                 getTripMoney()                Returns the current record's "trip_money" value
 * @method string                  getLocalCompanyName()         Returns the current record's "local_company_name" value
 * @method integer                 getLocalCompanyAddressId()    Returns the current record's "local_company_address_id" value
 * @method enum                    getContagiousDisease()        Returns the current record's "contagious_disease" value
 * @method enum                    getPoliceCase()               Returns the current record's "police_case" value
 * @method enum                    getNarcoticInvolvement()      Returns the current record's "narcotic_involvement" value
 * @method enum                    getDeportedStatus()           Returns the current record's "deported_status" value
 * @method string                  getDeportedCountyId()         Returns the current record's "deported_county_id" value
 * @method enum                    getVisaFraudStatus()          Returns the current record's "visa_fraud_status" value
 * @method string                  getProcessingCountryId()      Returns the current record's "processing_country_id" value
 * @method integer                 getProcessingCenterId()       Returns the current record's "processing_center_id" value
 * @method boolean                 getIspaid()                   Returns the current record's "ispaid" value
 * @method boolean                 getIsEmailValid()             Returns the current record's "is_email_valid" value
 * @method integer                 getPaymentGatewayId()         Returns the current record's "payment_gateway_id" value
 * @method decimal                 getPaidDollarAmount()         Returns the current record's "paid_dollar_amount" value
 * @method decimal                 getPaidNairaAmount()          Returns the current record's "paid_naira_amount" value
 * @method date                    getPaidDate()                 Returns the current record's "paid_date" value
 * @method date                    getArrivalDate()              Returns the current record's "arrival_date" value
 * @method enum                    getStatus()                   Returns the current record's "status" value
 * @method string                  getPaymentTransId()           Returns the current record's "payment_trans_id" value
 * @method string                  getBusinessAddress()          Returns the current record's "business_address" value
 * @method enum                    getSponsoreType()             Returns the current record's "sponsore_type" value
 * @method text                    getRemarks()                  Returns the current record's "remarks" value
 * @method VapCompany              getVapCompany()               Returns the current record's "VapCompany" value
 * @method VapVisaOfficeAddress    getVapVisaOfficeAddress()     Returns the current record's "VapVisaOfficeAddress" value
 * @method VapVisaPermanentAddress getVapVisaPermanentAddress()  Returns the current record's "VapVisaPermanentAddress" value
 * @method VapLocalComanyAddress   getVapLocalComanyAddress()    Returns the current record's "VapLocalComanyAddress" value
 * @method VapProcessingCentre     getVapProcessingCentre()      Returns the current record's "VapProcessingCentre" value
 * @method VisaType                getVapVisaTypeReason()        Returns the current record's "VapVisaTypeReason" value
 * @method Country                 getVapApplyingCountry()       Returns the current record's "VapApplyingCountry" value
 * @method Country                 getVapDeportedCountry()       Returns the current record's "VapDeportedCountry" value
 * @method Country                 getCurrentCountry()           Returns the current record's "CurrentCountry" value
 * @method Country                 getProcessingCountry()        Returns the current record's "ProcessingCountry" value
 * @method PaymentGatewayType      getPaymentGatewayType()       Returns the current record's "PaymentGatewayType" value
 * @method Doctrine_Collection     getIPaymentRequest()          Returns the current record's "IPaymentRequest" collection
 * @method VapApplication          setId()                       Sets the current record's "id" value
 * @method VapApplication          setRefNo()                    Sets the current record's "ref_no" value
 * @method VapApplication          setTitle()                    Sets the current record's "title" value
 * @method VapApplication          setFirstName()                Sets the current record's "first_name" value
 * @method VapApplication          setSurname()                  Sets the current record's "surname" value
 * @method VapApplication          setMiddleName()               Sets the current record's "middle_name" value
 * @method VapApplication          setGender()                   Sets the current record's "gender" value
 * @method VapApplication          setMaritalStatus()            Sets the current record's "marital_status" value
 * @method VapApplication          setEmail()                    Sets the current record's "email" value
 * @method VapApplication          setDateOfBirth()              Sets the current record's "date_of_birth" value
 * @method VapApplication          setPlaceOfBirth()             Sets the current record's "place_of_birth" value
 * @method VapApplication          setPresentNationalityId()     Sets the current record's "present_nationality_id" value
 * @method VapApplication          setHairColor()                Sets the current record's "hair_color" value
 * @method VapApplication          setEyesColor()                Sets the current record's "eyes_color" value
 * @method VapApplication          setIdMarks()                  Sets the current record's "id_marks" value
 * @method VapApplication          setHeight()                   Sets the current record's "height" value
 * @method VapApplication          setOfficeAddressId()          Sets the current record's "office_address_id" value
 * @method VapApplication          setOfficePhoneNo()            Sets the current record's "office_phone_no" value
 * @method VapApplication          setApplicantType()            Sets the current record's "applicant_type" value
 * @method VapApplication          setVapCompanyId()             Sets the current record's "vap_company_id" value
 * @method VapApplication          setCustomerServiceNumber()    Sets the current record's "customer_service_number" value
 * @method VapApplication          setDocument1()                Sets the current record's "document_1" value
 * @method VapApplication          setDocument2()                Sets the current record's "document_2" value
 * @method VapApplication          setDocument3()                Sets the current record's "document_3" value
 * @method VapApplication          setDocument4()                Sets the current record's "document_4" value
 * @method VapApplication          setDocument5()                Sets the current record's "document_5" value
 * @method VapApplication          setBoardingPlace()            Sets the current record's "boarding_place" value
 * @method VapApplication          setFlightCarrier()            Sets the current record's "flight_carrier" value
 * @method VapApplication          setFlightNumber()             Sets the current record's "flight_number" value
 * @method VapApplication          setIssusingGovt()             Sets the current record's "issusing_govt" value
 * @method VapApplication          setPassportNumber()           Sets the current record's "passport_number" value
 * @method VapApplication          setDateOfIssue()              Sets the current record's "date_of_issue" value
 * @method VapApplication          setDateOfExp()                Sets the current record's "date_of_exp" value
 * @method VapApplication          setPlaceOfIssue()             Sets the current record's "place_of_issue" value
 * @method VapApplication          setApplyingCountryId()        Sets the current record's "applying_country_id" value
 * @method VapApplication          setTypeOfVisa()               Sets the current record's "type_of_visa" value
 * @method VapApplication          setTripMoney()                Sets the current record's "trip_money" value
 * @method VapApplication          setLocalCompanyName()         Sets the current record's "local_company_name" value
 * @method VapApplication          setLocalCompanyAddressId()    Sets the current record's "local_company_address_id" value
 * @method VapApplication          setContagiousDisease()        Sets the current record's "contagious_disease" value
 * @method VapApplication          setPoliceCase()               Sets the current record's "police_case" value
 * @method VapApplication          setNarcoticInvolvement()      Sets the current record's "narcotic_involvement" value
 * @method VapApplication          setDeportedStatus()           Sets the current record's "deported_status" value
 * @method VapApplication          setDeportedCountyId()         Sets the current record's "deported_county_id" value
 * @method VapApplication          setVisaFraudStatus()          Sets the current record's "visa_fraud_status" value
 * @method VapApplication          setProcessingCountryId()      Sets the current record's "processing_country_id" value
 * @method VapApplication          setProcessingCenterId()       Sets the current record's "processing_center_id" value
 * @method VapApplication          setIspaid()                   Sets the current record's "ispaid" value
 * @method VapApplication          setIsEmailValid()             Sets the current record's "is_email_valid" value
 * @method VapApplication          setPaymentGatewayId()         Sets the current record's "payment_gateway_id" value
 * @method VapApplication          setPaidDollarAmount()         Sets the current record's "paid_dollar_amount" value
 * @method VapApplication          setPaidNairaAmount()          Sets the current record's "paid_naira_amount" value
 * @method VapApplication          setPaidDate()                 Sets the current record's "paid_date" value
 * @method VapApplication          setArrivalDate()              Sets the current record's "arrival_date" value
 * @method VapApplication          setStatus()                   Sets the current record's "status" value
 * @method VapApplication          setPaymentTransId()           Sets the current record's "payment_trans_id" value
 * @method VapApplication          setBusinessAddress()          Sets the current record's "business_address" value
 * @method VapApplication          setSponsoreType()             Sets the current record's "sponsore_type" value
 * @method VapApplication          setRemarks()                  Sets the current record's "remarks" value
 * @method VapApplication          setVapCompany()               Sets the current record's "VapCompany" value
 * @method VapApplication          setVapVisaOfficeAddress()     Sets the current record's "VapVisaOfficeAddress" value
 * @method VapApplication          setVapVisaPermanentAddress()  Sets the current record's "VapVisaPermanentAddress" value
 * @method VapApplication          setVapLocalComanyAddress()    Sets the current record's "VapLocalComanyAddress" value
 * @method VapApplication          setVapProcessingCentre()      Sets the current record's "VapProcessingCentre" value
 * @method VapApplication          setVapVisaTypeReason()        Sets the current record's "VapVisaTypeReason" value
 * @method VapApplication          setVapApplyingCountry()       Sets the current record's "VapApplyingCountry" value
 * @method VapApplication          setVapDeportedCountry()       Sets the current record's "VapDeportedCountry" value
 * @method VapApplication          setCurrentCountry()           Sets the current record's "CurrentCountry" value
 * @method VapApplication          setProcessingCountry()        Sets the current record's "ProcessingCountry" value
 * @method VapApplication          setPaymentGatewayType()       Sets the current record's "PaymentGatewayType" value
 * @method VapApplication          setIPaymentRequest()          Sets the current record's "IPaymentRequest" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVapApplication extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_vap_application');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('ref_no', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('title', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'MR',
              1 => 'MRS',
              2 => 'MISS',
              3 => 'DR',
             ),
             ));
        $this->hasColumn('first_name', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('surname', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('middle_name', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('gender', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Male',
              1 => 'Female',
             ),
             ));
        $this->hasColumn('marital_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Single',
              1 => 'Married',
              2 => 'Widowed',
              3 => 'Divorced',
              4 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('email', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('date_of_birth', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('place_of_birth', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('present_nationality_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('hair_color', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Black',
              1 => 'Brown',
              2 => 'White',
              3 => 'Red',
              4 => 'Gray',
              5 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('eyes_color', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Brown',
              1 => 'Blue',
              2 => 'Green',
              3 => 'Gray',
              4 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('id_marks', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('height', 'integer', 3, array(
             'type' => 'integer',
             'length' => 3,
             ));
        $this->hasColumn('office_address_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('office_phone_no', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('applicant_type', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'GB',
              1 => 'PB',
             ),
             'comment' => 'GB-Government Business, PB-Private Business',
             ));
        $this->hasColumn('vap_company_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('customer_service_number', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('document_1', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('document_2', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('document_3', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('document_4', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('document_5', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('boarding_place', 'string', 150, array(
             'type' => 'string',
             'length' => 150,
             ));
        $this->hasColumn('flight_carrier', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('flight_number', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('issusing_govt', 'string', 192, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 192,
             ));
        $this->hasColumn('passport_number', 'string', 192, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 192,
             ));
        $this->hasColumn('date_of_issue', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('date_of_exp', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('place_of_issue', 'string', 192, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 192,
             ));
        $this->hasColumn('applying_country_id', 'string', 2, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 2,
             ));
        $this->hasColumn('type_of_visa', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('trip_money', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('local_company_name', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('local_company_address_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('contagious_disease', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Yes',
              1 => 'No',
             ),
             ));
        $this->hasColumn('police_case', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Yes',
              1 => 'No',
             ),
             ));
        $this->hasColumn('narcotic_involvement', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Yes',
              1 => 'No',
             ),
             ));
        $this->hasColumn('deported_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Yes',
              1 => 'No',
             ),
             ));
        $this->hasColumn('deported_county_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('visa_fraud_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Yes',
              1 => 'No',
             ),
             ));
        $this->hasColumn('processing_country_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('processing_center_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('ispaid', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('is_email_valid', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('payment_gateway_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('paid_dollar_amount', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('paid_naira_amount', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('paid_date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('arrival_date', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'New',
              1 => 'Paid',
              2 => 'Vetted',
              3 => 'Approved',
              4 => 'Rejected',
              5 => 'Expired',
             ),
             'default' => 'New',
             ));
        $this->hasColumn('payment_trans_id', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('business_address', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('sponsore_type', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'SS',
              1 => 'CS',
             ),
             'comment' => 'SS - Self Sponsored, CS - Company Sponsored',
             ));
        $this->hasColumn('remarks', 'text', null, array(
             'type' => 'text',
             'notnull' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('VapCompany', array(
             'local' => 'vap_company_id',
             'foreign' => 'id'));

        $this->hasOne('VapVisaOfficeAddress', array(
             'local' => 'office_address_id',
             'foreign' => 'id'));

        $this->hasOne('VapVisaPermanentAddress', array(
             'local' => 'permanent_address_id',
             'foreign' => 'id'));

        $this->hasOne('VapLocalComanyAddress', array(
             'local' => 'local_company_address_id',
             'foreign' => 'id'));

        $this->hasOne('VapProcessingCentre', array(
             'local' => 'processing_center_id',
             'foreign' => 'id'));

        $this->hasOne('VisaType as VapVisaTypeReason', array(
             'local' => 'visited_reason_type_id',
             'foreign' => 'id'));

        $this->hasOne('Country as VapApplyingCountry', array(
             'local' => 'applying_country_id',
             'foreign' => 'id'));

        $this->hasOne('Country as VapDeportedCountry', array(
             'local' => 'deported_county_id',
             'foreign' => 'id'));

        $this->hasOne('Country as CurrentCountry', array(
             'local' => 'present_nationality_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('Country as ProcessingCountry', array(
             'local' => 'processing_country_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('PaymentGatewayType', array(
             'local' => 'payment_gateway_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasMany('IPaymentRequest', array(
             'local' => 'id',
             'foreign' => 'visa_arrival_program_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}