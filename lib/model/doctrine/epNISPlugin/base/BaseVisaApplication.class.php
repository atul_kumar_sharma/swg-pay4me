<?php
// Connection Component Binding
Doctrine_Manager::getInstance()->bindComponent('VisaApplication', 'connection_nis');

/**
 * BaseVisaApplication
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $visacategory_id
 * @property integer $zone_type_id
 * @property integer $ref_no
 * @property enum $title
 * @property string $surname
 * @property string $middle_name
 * @property string $other_name
 * @property enum $gender
 * @property string $email
 * @property enum $marital_status
 * @property date $date_of_birth
 * @property string $place_of_birth
 * @property enum $hair_color
 * @property enum $eyes_color
 * @property string $id_marks
 * @property integer $height
 * @property string $present_nationality_id
 * @property string $previous_nationality_id
 * @property integer $permanent_address_id
 * @property string $perm_phone_no
 * @property string $profession
 * @property integer $office_address_id
 * @property string $office_phone_no
 * @property string $milltary_in
 * @property date $military_dt_from
 * @property date $military_dt_to
 * @property boolean $ispaid
 * @property string $payment_trans_id
 * @property boolean $term_chk_flg
 * @property date $interview_date
 * @property enum $status
 * @property integer $payment_gateway_id
 * @property decimal $paid_dollar_amount
 * @property decimal $paid_naira_amount
 * @property decimal $amount
 * @property integer $currency_id
 * @property date $paid_at
 * @property boolean $is_email_valid
 * @property VisaOfficeAddress $VisaOfficeAddress
 * @property VisaPermanentAddress $VisaPermanentAddress
 * @property VisaCategory $VisaCategory
 * @property VisaZoneType $VisaZoneType
 * @property ReEntryVisaApplication $ReEntryVisaApplication
 * @property VisaApplicationDetails $VisaApplicationDetails
 * @property VisaApplicantInfo $VisaApplicantInfo
 * @property VisaVettingQueue $VisaVettingQueue
 * @property Country $PreviousCountry
 * @property Country $CurrentCountry
 * @property PaymentGatewayType $PaymentGatewayType
 * @property Doctrine_Collection $TravelHistory5y
 * @property Doctrine_Collection $TravelHistory1y
 * @property Doctrine_Collection $PreviousVisits
 * @property Doctrine_Collection $VisaReferences
 * @property Doctrine_Collection $IPaymentRequest
 * 
 * @method integer                getId()                      Returns the current record's "id" value
 * @method integer                getVisacategoryId()          Returns the current record's "visacategory_id" value
 * @method integer                getZoneTypeId()              Returns the current record's "zone_type_id" value
 * @method integer                getRefNo()                   Returns the current record's "ref_no" value
 * @method enum                   getTitle()                   Returns the current record's "title" value
 * @method string                 getSurname()                 Returns the current record's "surname" value
 * @method string                 getMiddleName()              Returns the current record's "middle_name" value
 * @method string                 getOtherName()               Returns the current record's "other_name" value
 * @method enum                   getGender()                  Returns the current record's "gender" value
 * @method string                 getEmail()                   Returns the current record's "email" value
 * @method enum                   getMaritalStatus()           Returns the current record's "marital_status" value
 * @method date                   getDateOfBirth()             Returns the current record's "date_of_birth" value
 * @method string                 getPlaceOfBirth()            Returns the current record's "place_of_birth" value
 * @method enum                   getHairColor()               Returns the current record's "hair_color" value
 * @method enum                   getEyesColor()               Returns the current record's "eyes_color" value
 * @method string                 getIdMarks()                 Returns the current record's "id_marks" value
 * @method integer                getHeight()                  Returns the current record's "height" value
 * @method string                 getPresentNationalityId()    Returns the current record's "present_nationality_id" value
 * @method string                 getPreviousNationalityId()   Returns the current record's "previous_nationality_id" value
 * @method integer                getPermanentAddressId()      Returns the current record's "permanent_address_id" value
 * @method string                 getPermPhoneNo()             Returns the current record's "perm_phone_no" value
 * @method string                 getProfession()              Returns the current record's "profession" value
 * @method integer                getOfficeAddressId()         Returns the current record's "office_address_id" value
 * @method string                 getOfficePhoneNo()           Returns the current record's "office_phone_no" value
 * @method string                 getMilltaryIn()              Returns the current record's "milltary_in" value
 * @method date                   getMilitaryDtFrom()          Returns the current record's "military_dt_from" value
 * @method date                   getMilitaryDtTo()            Returns the current record's "military_dt_to" value
 * @method boolean                getIspaid()                  Returns the current record's "ispaid" value
 * @method string                 getPaymentTransId()          Returns the current record's "payment_trans_id" value
 * @method boolean                getTermChkFlg()              Returns the current record's "term_chk_flg" value
 * @method date                   getInterviewDate()           Returns the current record's "interview_date" value
 * @method enum                   getStatus()                  Returns the current record's "status" value
 * @method integer                getPaymentGatewayId()        Returns the current record's "payment_gateway_id" value
 * @method decimal                getPaidDollarAmount()        Returns the current record's "paid_dollar_amount" value
 * @method decimal                getPaidNairaAmount()         Returns the current record's "paid_naira_amount" value
 * @method decimal                getAmount()                  Returns the current record's "amount" value
 * @method integer                getCurrencyId()              Returns the current record's "currency_id" value
 * @method date                   getPaidAt()                  Returns the current record's "paid_at" value
 * @method boolean                getIsEmailValid()            Returns the current record's "is_email_valid" value
 * @method VisaOfficeAddress      getVisaOfficeAddress()       Returns the current record's "VisaOfficeAddress" value
 * @method VisaPermanentAddress   getVisaPermanentAddress()    Returns the current record's "VisaPermanentAddress" value
 * @method VisaCategory           getVisaCategory()            Returns the current record's "VisaCategory" value
 * @method VisaZoneType           getVisaZoneType()            Returns the current record's "VisaZoneType" value
 * @method ReEntryVisaApplication getReEntryVisaApplication()  Returns the current record's "ReEntryVisaApplication" value
 * @method VisaApplicationDetails getVisaApplicationDetails()  Returns the current record's "VisaApplicationDetails" value
 * @method VisaApplicantInfo      getVisaApplicantInfo()       Returns the current record's "VisaApplicantInfo" value
 * @method VisaVettingQueue       getVisaVettingQueue()        Returns the current record's "VisaVettingQueue" value
 * @method Country                getPreviousCountry()         Returns the current record's "PreviousCountry" value
 * @method Country                getCurrentCountry()          Returns the current record's "CurrentCountry" value
 * @method PaymentGatewayType     getPaymentGatewayType()      Returns the current record's "PaymentGatewayType" value
 * @method Doctrine_Collection    getTravelHistory5y()         Returns the current record's "TravelHistory5y" collection
 * @method Doctrine_Collection    getTravelHistory1y()         Returns the current record's "TravelHistory1y" collection
 * @method Doctrine_Collection    getPreviousVisits()          Returns the current record's "PreviousVisits" collection
 * @method Doctrine_Collection    getVisaReferences()          Returns the current record's "VisaReferences" collection
 * @method Doctrine_Collection    getIPaymentRequest()         Returns the current record's "IPaymentRequest" collection
 * @method VisaApplication        setId()                      Sets the current record's "id" value
 * @method VisaApplication        setVisacategoryId()          Sets the current record's "visacategory_id" value
 * @method VisaApplication        setZoneTypeId()              Sets the current record's "zone_type_id" value
 * @method VisaApplication        setRefNo()                   Sets the current record's "ref_no" value
 * @method VisaApplication        setTitle()                   Sets the current record's "title" value
 * @method VisaApplication        setSurname()                 Sets the current record's "surname" value
 * @method VisaApplication        setMiddleName()              Sets the current record's "middle_name" value
 * @method VisaApplication        setOtherName()               Sets the current record's "other_name" value
 * @method VisaApplication        setGender()                  Sets the current record's "gender" value
 * @method VisaApplication        setEmail()                   Sets the current record's "email" value
 * @method VisaApplication        setMaritalStatus()           Sets the current record's "marital_status" value
 * @method VisaApplication        setDateOfBirth()             Sets the current record's "date_of_birth" value
 * @method VisaApplication        setPlaceOfBirth()            Sets the current record's "place_of_birth" value
 * @method VisaApplication        setHairColor()               Sets the current record's "hair_color" value
 * @method VisaApplication        setEyesColor()               Sets the current record's "eyes_color" value
 * @method VisaApplication        setIdMarks()                 Sets the current record's "id_marks" value
 * @method VisaApplication        setHeight()                  Sets the current record's "height" value
 * @method VisaApplication        setPresentNationalityId()    Sets the current record's "present_nationality_id" value
 * @method VisaApplication        setPreviousNationalityId()   Sets the current record's "previous_nationality_id" value
 * @method VisaApplication        setPermanentAddressId()      Sets the current record's "permanent_address_id" value
 * @method VisaApplication        setPermPhoneNo()             Sets the current record's "perm_phone_no" value
 * @method VisaApplication        setProfession()              Sets the current record's "profession" value
 * @method VisaApplication        setOfficeAddressId()         Sets the current record's "office_address_id" value
 * @method VisaApplication        setOfficePhoneNo()           Sets the current record's "office_phone_no" value
 * @method VisaApplication        setMilltaryIn()              Sets the current record's "milltary_in" value
 * @method VisaApplication        setMilitaryDtFrom()          Sets the current record's "military_dt_from" value
 * @method VisaApplication        setMilitaryDtTo()            Sets the current record's "military_dt_to" value
 * @method VisaApplication        setIspaid()                  Sets the current record's "ispaid" value
 * @method VisaApplication        setPaymentTransId()          Sets the current record's "payment_trans_id" value
 * @method VisaApplication        setTermChkFlg()              Sets the current record's "term_chk_flg" value
 * @method VisaApplication        setInterviewDate()           Sets the current record's "interview_date" value
 * @method VisaApplication        setStatus()                  Sets the current record's "status" value
 * @method VisaApplication        setPaymentGatewayId()        Sets the current record's "payment_gateway_id" value
 * @method VisaApplication        setPaidDollarAmount()        Sets the current record's "paid_dollar_amount" value
 * @method VisaApplication        setPaidNairaAmount()         Sets the current record's "paid_naira_amount" value
 * @method VisaApplication        setAmount()                  Sets the current record's "amount" value
 * @method VisaApplication        setCurrencyId()              Sets the current record's "currency_id" value
 * @method VisaApplication        setPaidAt()                  Sets the current record's "paid_at" value
 * @method VisaApplication        setIsEmailValid()            Sets the current record's "is_email_valid" value
 * @method VisaApplication        setVisaOfficeAddress()       Sets the current record's "VisaOfficeAddress" value
 * @method VisaApplication        setVisaPermanentAddress()    Sets the current record's "VisaPermanentAddress" value
 * @method VisaApplication        setVisaCategory()            Sets the current record's "VisaCategory" value
 * @method VisaApplication        setVisaZoneType()            Sets the current record's "VisaZoneType" value
 * @method VisaApplication        setReEntryVisaApplication()  Sets the current record's "ReEntryVisaApplication" value
 * @method VisaApplication        setVisaApplicationDetails()  Sets the current record's "VisaApplicationDetails" value
 * @method VisaApplication        setVisaApplicantInfo()       Sets the current record's "VisaApplicantInfo" value
 * @method VisaApplication        setVisaVettingQueue()        Sets the current record's "VisaVettingQueue" value
 * @method VisaApplication        setPreviousCountry()         Sets the current record's "PreviousCountry" value
 * @method VisaApplication        setCurrentCountry()          Sets the current record's "CurrentCountry" value
 * @method VisaApplication        setPaymentGatewayType()      Sets the current record's "PaymentGatewayType" value
 * @method VisaApplication        setTravelHistory5y()         Sets the current record's "TravelHistory5y" collection
 * @method VisaApplication        setTravelHistory1y()         Sets the current record's "TravelHistory1y" collection
 * @method VisaApplication        setPreviousVisits()          Sets the current record's "PreviousVisits" collection
 * @method VisaApplication        setVisaReferences()          Sets the current record's "VisaReferences" collection
 * @method VisaApplication        setIPaymentRequest()         Sets the current record's "IPaymentRequest" collection
 * 
 * @package    ama
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVisaApplication extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('tbl_visa_application');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('visacategory_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('zone_type_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('ref_no', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('title', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'MR',
              1 => 'MRS',
              2 => 'MISS',
              3 => 'DR',
             ),
             'notnull' => true,
             ));
        $this->hasColumn('surname', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('middle_name', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('other_name', 'string', 50, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 50,
             ));
        $this->hasColumn('gender', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Male',
              1 => 'Female',
             ),
             'notnull' => true,
             ));
        $this->hasColumn('email', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('marital_status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Single',
              1 => 'Married',
              2 => 'Widowed',
              3 => 'Divorced',
              4 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('date_of_birth', 'date', null, array(
             'type' => 'date',
             'notnull' => true,
             ));
        $this->hasColumn('place_of_birth', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('hair_color', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Black',
              1 => 'Brown',
              2 => 'White',
              3 => 'Gray',
              4 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('eyes_color', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'Brown',
              1 => 'Blue',
              2 => 'Green',
              3 => 'Gray',
              4 => 'None',
             ),
             'default' => 'None',
             ));
        $this->hasColumn('id_marks', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('height', 'integer', 3, array(
             'type' => 'integer',
             'length' => 3,
             ));
        $this->hasColumn('present_nationality_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('previous_nationality_id', 'string', 2, array(
             'type' => 'string',
             'length' => 2,
             ));
        $this->hasColumn('permanent_address_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('perm_phone_no', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('profession', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('office_address_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('office_phone_no', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('milltary_in', 'string', 45, array(
             'type' => 'string',
             'length' => 45,
             ));
        $this->hasColumn('military_dt_from', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('military_dt_to', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('ispaid', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('payment_trans_id', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('term_chk_flg', 'boolean', null, array(
             'type' => 'boolean',
             ));
        $this->hasColumn('interview_date', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('status', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'New',
              1 => 'Paid',
              2 => 'Vetted',
              3 => 'Approved',
              4 => 'Rejected by Vetter',
              5 => 'Rejected by Approver',
             ),
             'default' => 'New',
             ));
        $this->hasColumn('payment_gateway_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('paid_dollar_amount', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('paid_naira_amount', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('amount', 'decimal', 18, array(
             'type' => 'decimal',
             'scale' => 2,
             'length' => 18,
             ));
        $this->hasColumn('currency_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('paid_at', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('is_email_valid', 'boolean', null, array(
             'type' => 'boolean',
             ));


        $this->index('visacategoryid_idx', array(
             'fields' => 
             array(
              0 => 'visacategory_id',
             ),
             ));
        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('VisaOfficeAddress', array(
             'local' => 'office_address_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('VisaPermanentAddress', array(
             'local' => 'permanent_address_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('VisaCategory', array(
             'local' => 'visacategory_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('VisaZoneType', array(
             'local' => 'zone_type_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('ReEntryVisaApplication', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasOne('VisaApplicationDetails', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasOne('VisaApplicantInfo', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasOne('VisaVettingQueue', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasOne('Country as PreviousCountry', array(
             'local' => 'previous_nationality_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('Country as CurrentCountry', array(
             'local' => 'present_nationality_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasOne('PaymentGatewayType', array(
             'local' => 'payment_gateway_id',
             'foreign' => 'id',
             'onDelete' => 'restrict',
             'onUpdate' => 'restrict'));

        $this->hasMany('VApplicantFiveYearsTravelHistory as TravelHistory5y', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasMany('VApplicantOneYearsTravelHistory as TravelHistory1y', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasMany('VisaApplicantPreviousHistory as PreviousVisits', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasMany('ReEntryVisaReferences as VisaReferences', array(
             'local' => 'id',
             'foreign' => 'application_id'));

        $this->hasMany('IPaymentRequest', array(
             'local' => 'id',
             'foreign' => 'freezone_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}