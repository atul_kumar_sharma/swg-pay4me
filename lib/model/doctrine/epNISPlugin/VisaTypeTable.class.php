<?php


class VisaTypeTable extends PluginVisaTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaType');
    }
}