<?php


class TblBlockApplicantTable extends PluginTblBlockApplicantTable
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('TblBlockApplicant');
  }

  public function blockApplicant($details)
  {
    $blockObj = new TblBlockApplicant();
    $blockObj->setFirstName($details['fname']);
    $blockObj->setMiddleName($details['mname']);
    $blockObj->setLastName($details['lname']);
    $blockObj->setDob($details['dob']);
    $blockObj->setAppType($details['app_type']);
    $blockObj->setAppId($details['id']);
    $blockObj->setRefNo($details['ref_no']);
    $blockObj->setCardFirst($details['card_first']);
    $blockObj->setCardLast($details['card_last']);
    $blockObj->setCardHolderName($details['card_holder']);
    $blockObj->setEmail($details['email']);
    $blockObj->setProcessingCountry($details['processing_country']);
    $blockObj->setEmbassy($details['processing_embassy']);
    $blockObj->setBlocked('yes');
    $blockObj->save();
  }

  public function isApplicantBlocked($arrForVerification)
  {
   
    if(is_array($arrForVerification['dob']) && is_array($arrForVerification['dob']) && is_array($arrForVerification['dob']) && $arrForVerification['dob']['day'] !='' && $arrForVerification['dob']['month'] !='' && $arrForVerification['dob']['year'] !=''){
        $day = $arrForVerification['dob']['day'];
        $month = $arrForVerification['dob']['month'];
        $year = $arrForVerification['dob']['year'];
        $dob = date('Y-m-d H:i:s', strtotime("$year-$month-$day 00:00:00"));
    }else{
        $dob = $arrForVerification['dob'];
    }
    
    $q = Doctrine_Query::create()
    ->select('count(*)')
    ->from('TblBlockApplicant')
    ->where('first_name =?', $arrForVerification['fname'])
    ->andWhere('middle_name =?', $arrForVerification['mname'])
    ->andWhere('last_name =?', $arrForVerification['lname'])
    ->andWhere('dob =?', $dob)
    ->andWhere('blocked =?', 'yes')
    
    ->count();
   
    return $q;
  }

}