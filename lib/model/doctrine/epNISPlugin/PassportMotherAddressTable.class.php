<?php


class PassportMotherAddressTable extends PluginPassportMotherAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportMotherAddress');
    }
}