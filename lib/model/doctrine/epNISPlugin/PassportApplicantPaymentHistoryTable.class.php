<?php


class PassportApplicantPaymentHistoryTable extends PluginPassportApplicantPaymentHistoryTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportApplicantPaymentHistory');
    }
}