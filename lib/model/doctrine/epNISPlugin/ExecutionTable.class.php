<?php


class ExecutionTable extends PluginExecutionTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('Execution');
    }

    // Delete Entry from Workpool Table for Unpay Visa & Passport Applications
    public function deleteRecord($execution_id)
    {
        try{
            if(isset ($execution_id) && $execution_id!=''){
                $query = Doctrine_Query::create()
                ->delete('e.*')
                ->from('Execution e')
                ->where('e.execution_id=?',$execution_id)->limit(1);
                $execution_id = $query->execute();
                if($execution_id){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());
        }
    }
}