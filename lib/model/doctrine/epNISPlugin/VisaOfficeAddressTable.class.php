<?php


class VisaOfficeAddressTable extends PluginVisaOfficeAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaOfficeAddress');
    }
}