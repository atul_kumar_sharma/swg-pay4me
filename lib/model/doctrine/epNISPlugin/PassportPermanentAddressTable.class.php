<?php


class PassportPermanentAddressTable extends PluginPassportPermanentAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportPermanentAddress');
    }
}