<?php


class PassportOfficeTable extends PluginPassportOfficeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PassportOffice');
    }
}