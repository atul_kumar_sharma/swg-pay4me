<?php


class VisaApplicationDetailsTable extends PluginVisaApplicationDetailsTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplicationDetails');
    }
}