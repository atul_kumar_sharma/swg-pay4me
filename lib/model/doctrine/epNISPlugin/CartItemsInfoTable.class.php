<?php


class CartItemsInfoTable extends PluginCartItemsInfoTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CartItemsInfo');
    }

    public function getCartItemInfo($payment_request_id){
        if($payment_request_id != ''){
           $q = Doctrine_Query::create()
            ->select('cii.cart_id')
            ->from('CartItemsInfo cii')
            ->where('cii.item_id = ?',$payment_request_id);

             //echo $payment_request_id;
             //print_r($q->getSqlQuery()); die();            
            return $q->execute(array(),Doctrine::HYDRATE_ARRAY);

        }else{
            return ;
        }
    }

    
}