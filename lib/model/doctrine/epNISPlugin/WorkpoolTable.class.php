<?php


class WorkpoolTable extends PluginWorkpoolTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('Workpool');
    }

    // Delete Entry from Workpool Table for Unpay Visa & Passport Applications
    public function deleteRecord($execution_id)
    {
        try{
            if(isset ($execution_id) && $execution_id!=''){
                $query = Doctrine_Query::create()
                ->delete('wp.*')
                ->from('Workpool wp')
                ->where('wp.execution_id=?',$execution_id)->limit(1);
                $execution_id = $query->execute();
                if($execution_id){
                    return true;
                }
                else {
                    return false;
                }
            }else {
                return false;
            }
        }
        catch (Exception $e)
        {
            throw New Exception ($e->getMessage());
        }
    }
}
