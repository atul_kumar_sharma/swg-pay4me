<?php


class VisaApplicantPreviousHistoryTable extends PluginVisaApplicantPreviousHistoryTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplicantPreviousHistory');
    }

    public function getPreviousHistory($getAppId){
      $visa_application=Doctrine_Query::create()
      ->select("vaph.*,ha.*,l.*,s.*")
      ->from('VisaApplicantPreviousHistory vaph')
      ->leftJoin('vaph.VisaApplicantPreviousHistoryAddress ha')
      ->leftJoin('ha.LGA l')
      ->leftJoin('ha.State s')
      ->where("vaph.application_id =". $getAppId)
      ->execute()->toArray(true);
      return $visa_application;
    }
}