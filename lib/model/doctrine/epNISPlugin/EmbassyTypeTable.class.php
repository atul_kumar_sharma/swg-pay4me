<?php


class EmbassyTypeTable extends PluginEmbassyTypeTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EmbassyType');
    }
}