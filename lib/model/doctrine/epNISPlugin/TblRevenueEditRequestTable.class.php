<?php


class TblRevenueEditRequestTable extends PluginTblRevenueEditRequestTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('TblRevenueEditRequest');
    }

    public function getPreviousRequest($hdnDate,$hdnAccountId,$hdnProjectId){

        $orview = $this->createQuery('gp')
        ->select('gp.* ')
        ->Where('gp.date = ?',$hdnDate)
        ->andWhere('gp.project_id=?',$hdnProjectId)
        ->andWhere('gp.account_id = ?',$hdnAccountId)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        return $orview;
    }

    public function updateEditRequest($amount,$hdnDate,$hdnAccountId,$comments,$hdnProjectId,$editRequestId){
        try {
            $q = Doctrine_Query::create()
            ->update('TblRevenueEditRequest')
            ->set('date', '?', $hdnDate)
            ->set('project_id', '?', $hdnProjectId)
            ->set('current_amount', '?', $amount)
            ->set('account_id', '?', $hdnAccountId)
            ->set('comments', '?', $comments)
            ->where("id =?", $editRequestId)
            ->execute();
            return true;
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getRecord(){

      try {
            $q = $this->createQuery()
            ->select('o.*,pn.name,bd.account_name')
            ->from('TblRevenueEditRequest o')
            ->leftJoin('o.TblProjectName pn')
            ->leftJoin('o.TblBankDetails bd')            
            ->orderBy('status,created_at DESC');
            $q->fetchArray();

      return $q;
        }catch(Exception $e){
            echo("Problem found". $e->getMessage());die;
        }
    }

    public function getEditRecord($id){
      try {
        $q = $this->createQuery()
        ->select('*')
        ->from('TblRevenueEditRequest')
        ->where('id=?',$id)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);

        return $q;
      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }
      
    }

    public function getAccountRecord($accountId,$date){
      try {
        $q = $this->createQuery()
        ->select('*')
        ->from('TblRevenueEditRequest')
        ->where('account_id=?',$accountId)
        ->andWhere('date=?',$date)
        ->orderBy('date')
        ->execute(array(), Doctrine::HYDRATE_ARRAY);

        return $q;
      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }

    }

    public function updateAdminComments($action,$comments,$hdn_id){
      try {
        $q = Doctrine_Query::create()
        ->update('TblRevenueEditRequest')
        ->set('status', '?', $action)
        ->set('admin_comments', '?', $comments)
        ->where("id =?", $hdn_id)
        ->execute();
        return true;
      }catch(Exception $e){
        echo("Problem found". $e->getMessage());die;
      }
    }

}