<?php


class VisaApplicantPreviousHistoryAddressTable extends PluginVisaApplicantPreviousHistoryAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApplicantPreviousHistoryAddress');
    }
}