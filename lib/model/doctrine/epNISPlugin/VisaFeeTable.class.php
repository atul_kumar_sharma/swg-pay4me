<?php


class VisaFeeTable extends PluginVisaFeeTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaFee');
    }

    /*
     * Added by ashwani for visa payment receipt...
     */
    public function getVisaFee($cnt_id,$catId, $visaType,$entType,$noOfEntries=1,$zoneType=null, $additionalCharges)
    {
        /**
         * [WP : 102] => CR: 144
         * Addding additional charges in case of Visa and Entry Freezone...
         * 29 is Visa and 101 Entry Freezone...
         */
        $addition_fee = 0;
        if($additionalCharges){
            $visa_additional_charges_flag = sfConfig::get('app_visa_additional_charges_flag');            
            if($visa_additional_charges_flag){
                if($catId == 29 || $catId == 101){
                    $addition_fee = sfConfig::get('app_visa_additional_charges');
                }
            }
        }        

        $fZoneId = Doctrine::getTable('VisaZoneType')->getFreeZoneId();
        $freezoneCatId = Doctrine::getTable('VisaCategory')->getFreshEntryFreezoneId();
        $freshCatId = Doctrine::getTable('VisaCategory')->getFreshEntryId();
        $visaFee = array('naira_amount' => 0,'dollar_amount' => 0);

        //   if($zoneType == $fZoneId){ //If Application is freeZone then fee will be set for single entry and multiple entry
        //     return $this->freeZoneFeee($entType);
        //   }else{ //Else fee will be applicable from visa fee table

        if($catId==$freezoneCatId){ 
            $catIdNew=$freshCatId;
        }else if($catId==$freshCatId){
            $catIdNew=$freshCatId;
        }else{
            $catIdNew=$catId;
        }

        $fee = $this->createQuery('j')
        ->where('j.country_id = ?', $cnt_id)
        ->addwhere('j.visa_cat_id = ?', $catIdNew)
        ->addwhere('j.visa_type_id = ?', $visaType)
        ->addwhere('j.entry_type_id = ?', $entType)
        ->execute()->getFirst();
        if (!$fee) { 
            // fee doesn't exist
            $visaFee['naira_amount'] = null;
            $visaFee['dollar_amount'] = null;
            return $visaFee;
        }
        if ($fee->getIsGratis()) {
            // return 0 0
            return $visaFee;
        }
        if ($fee->getIsFeeMultiplied()) { 
            // call this method itself with entType as singleEntry
            if($noOfEntries == 0)
            {
                $noOfEntries = 1;
            }
            $visaFee = $this->getVisaFee($cnt_id,$catIdNew, $visaType,Doctrine::getTable('EntryType')->getSingleEntryType());
            $finalFee['naira_amount'] = ($visaFee['naira_amount'] * $noOfEntries) + $addition_fee;
            $finalFee['dollar_amount'] = ($visaFee['dollar_amount'] * $noOfEntries) + $addition_fee;
            return $finalFee;
        }
        $visaFee['naira_amount'] = $fee->getNairaAmount() + $addition_fee;
        $visaFee['dollar_amount'] = $fee->getDollarAmount() + $addition_fee;
        return $visaFee;
        //    }

    }
      /**
       *
       * @param <type> $cnt_id
       * @return <type>
       * This function return visa arrival fee for defined visa cat, visa type and entry type...
       */
    public function getVisaArrivalFee($cnt_id, $visaType)
    {
        $addition_fee = sfConfig::get('app_visa_arrival_additional_charges');

        $visaFee = array('naira_amount' => 0,'dollar_amount' => 0);

        $fee = $this->createQuery('j')
        ->where('j.country_id = ?', $cnt_id)
        ->addwhere('j.visa_cat_id = ?', sfConfig::get('app_visa_arrival_visa_cat'))
        ->addwhere('j.visa_type_id = ?', $visaType) //sfConfig::get('app_visa_arrival_visa_type')
        ->addwhere('j.entry_type_id = ?', sfConfig::get('app_visa_arrival_entry_type'))
        ->execute()->getFirst();
        if (!$fee) {
            // fee doesn't exist
            $visaFee['naira_amount'] = null;
            $visaFee['dollar_amount'] = null;
            return $visaFee;
        }else if ($fee->getIsGratis()) {
            // return 0 0
            return $visaFee;
        }else{
            $visaFee['naira_amount'] = $fee->getNairaAmount() + $addition_fee;
            $visaFee['dollar_amount'] = $fee->getDollarAmount() + $addition_fee;
            return $visaFee;
        }

    }


    public function getMoVisaArrivalFeeForGB($cnt_id, $visaType)
    {
        $visaFee = array('naira_amount' => 0,'dollar_amount' => 0);

        $fee = $this->createQuery('j')
        ->where('j.country_id = ?', $cnt_id)
        ->addwhere('j.visa_cat_id = ?', sfConfig::get('app_visa_arrival_visa_cat'))
        ->addwhere('j.visa_type_id = ?', $visaType) //sfConfig::get('app_visa_arrival_visa_type')
        ->addwhere('j.entry_type_id = ?', sfConfig::get('app_visa_arrival_entry_type'))
        ->execute()->getFirst();
        if (!$fee) {
            // fee doesn't exist
            $visaFee['naira_amount'] = null;
            $visaFee['dollar_amount'] = null;
            return $visaFee;
        }else if ($fee->getIsGratis()) {
            // return 0 0
            return $visaFee;
        }else{
            $visaFee['naira_amount'] = $fee->getNairaAmount();
            $visaFee['dollar_amount'] = $fee->getDollarAmount();
            return $visaFee;
        }

    }
}