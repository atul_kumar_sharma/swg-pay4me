<?php


class VapVisaOfficeAddressTable extends PluginVapVisaOfficeAddressTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VapVisaOfficeAddress');
    }
}