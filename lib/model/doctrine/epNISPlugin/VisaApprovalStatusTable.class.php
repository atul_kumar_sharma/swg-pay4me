<?php


class VisaApprovalStatusTable extends PluginVisaApprovalStatusTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VisaApprovalStatus');
    }
}