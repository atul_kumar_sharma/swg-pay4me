<?php


class MidMasterTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('MidMaster');
    }

    public function  getMidMasterList()
    {
        try {
        $q = $this->createQuery('m')
                ->select('*')
                ->orderBy('id');        
            return $q;
        } catch (Exception $e) {
            echo ("Problem Found " . $e . getError());
            die;
        }

    }

    public function setValidations($id,$validation,$visa,$master,$cart)
    {
        try{
           $set = Doctrine_Query::create()
                    ->update('MidMaster');
                    if(isset($validation) && $validation!='')
                    {
                        $set->set('validation','?',$validation);
                    }
                    $set->set('visa_amount_limit','?',$visa);
                    $set->set('master_amount_limit','?',$master);
                    $set->set('cart_amount_limit','?',$cart);
                    $set->where('id = ?', $id)
                    ->execute();
                    return true;
        }catch(Exception $e)
        {
            echo ("Problem found ".$e.getError());
            die;
        }
    }
}