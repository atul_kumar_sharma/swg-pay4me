<?php


class CountryPaymentModeTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('CountryPaymentMode');
  }

  public function getCardType($arrCountry){
    $query = Doctrine_Query::create()
    ->select('cpm.*')
    ->from('CountryPaymentMode cpm')
    ->where('cpm.country_code=?',$arrCountry)
    ->execute()->toArray();
    return $query;
  }

    /**
     * @param <type> $card_type
     * @return array
     */
  public function getCountryMoList($card_type){
    $q = Doctrine_Query::create()
    ->select('country_code')
    ->from('CountryPaymentMode')
    ->where('card_type LIKE ?', '%'.$card_type.'%');

    //echo $q->getSqlQuery();
    //echo "<pre>"; print_r($q->fetchArray()); die();
    //return $q->execute(array(), DOCTRINE::HYDRATE_ARRAY);
    return $result = $q->fetchArray();
  }

  public function getCountryWtList($card_type){
    $q = Doctrine_Query::create()
    ->select('country_code')
    ->from('CountryPaymentMode')
    ->where('card_type LIKE ?', '%'.$card_type.'%');

    //echo $q->getSqlQuery();
    //echo "<pre>"; print_r($q->fetchArray()); die();
    //return $q->execute(array(), DOCTRINE::HYDRATE_ARRAY);
    return $result = $q->fetchArray();
  }

  public function getPaymentOptions($arrCountry){
    $query = Doctrine_Query::create()
    ->select('cpm.card_type')
    ->from('CountryPaymentMode cpm')
    ->where('cpm.country_code=?',$arrCountry)
    ->execute()->toArray();
    if(count($query) > 0)
    {
      $option = $query[0]['card_type'];
    }else{
      $option = 0;
    }
    //    echo "<pre>"; print_r($option); die();
    return $option;
  }

  public function checkCountryCode($countryCode)
  {
      try {
            $query = Doctrine_Query::create()
                            ->select("count(*)")
                            ->from("CountryPaymentMode c")
                            ->where('c.country_code =?', $countryCode)
                            ->count();
            //    echo "<pre>";print_r($query);die;
            return $query;
        } catch (Exception $e) {
            echo ("Problem Found " . $e . getError());
            die;
        }
  }
  public function getCountryPaymentModeList(){
      try {
        $q = $this->createQuery('c')
                ->select('*')
                ->orderBy('id');
            return $q;
        } catch (Exception $e) {
            echo ("Problem Found " . $e . getError());
            die;
        }
  }

  public function setValidations($id,$cardType,$service,$validation,$cartCapacity,$cartAmountCapacity,$numberOfTransaction,$transactionPeriod)
  {
      try{
           $set = Doctrine_Query::create()
                    ->update('CountryPaymentMode');
                    if(isset($cardType) && $cardType!='')
                    {
                        $set->set('card_type','?',$cardType);
                    }
                    if(isset($service) && $service!='')
                    {
                        $set->set('service','?',$service);
                    }
                    if(isset($validation) && $validation!='')
                    {
                        $set->set('validation','?',$validation);
                    }
                    if(isset($cartCapacity) && $cartCapacity!='')
                    {
                        $set->set('cart_capacity','?',$cartCapacity);
                    }
                    if(isset($cartAmountCapacity) && $cartAmountCapacity!='')
                    {
                        $set->set('cart_amount_capacity','?',$cartAmountCapacity);
                    }
                    if(isset($numberOfTransaction) && $numberOfTransaction!='')
                    {
                        $set->set('number_of_transaction','?',$numberOfTransaction);
                    }
                    if(isset($transactionPeriod) && $transactionPeriod!='')
                    {
                        $set->set('transaction_period','?',$transactionPeriod);
                    }
                    
                    $set->where('id = ?', $id)
                    ->execute();
                    return true;
        }catch(Exception $e)
        {
            echo ("Problem found ".$e.getError());
            die;
        }
  }

  public function removeValidations($id)
  {
      try{
        if(isset ($id) && $id!=''){
         $query = Doctrine_Query::create()
        ->delete('cp.*')
        ->from('CountryPaymentMode cp')
         ->where('cp.id =?',$id);
         $id = $query->execute();
        if($id){
                return true;
        }
        else {
            return false;
          }
        }
      }
    catch (Exception $e)
        {
            echo ("Problem found ".$e.getError());
            die;
        }
  }

  /**
   * [WP: 104] => CR: 149
   * @param <type> $countryId
   * @return <type>
   */
  public function getInfoByCountry($countryId){
    $result = Doctrine_Query::create()
    ->select('cpm.*')
    ->from('CountryPaymentMode cpm')
    ->where('cpm.country_code=?',$countryId)
    ->execute()->toArray();
    if(count($result)){
        return $result;
    }else{
        $result = Doctrine_Query::create()
                ->select('cpm.*')
                ->from('CountryPaymentMode cpm')
                ->where('cpm.country_code=?','All')
                ->execute()->toArray();
        return $result;
    }
  }
    

}