<?php


class UserBillingProfileTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('UserBillingProfile');
    }


       public function getLastAddressPaymentOrderProfile($user_id,$gateway_id='') {
        try{

           $q = $this->createQuery('u')
            ->select('u.*')
            ->from('UserBillingProfile u')
            ->where('u.user_id =?',$user_id)
            ->andWhere('u.gateway_id =?',$gateway_id)
            ->orderBy('u.updated_at desc')
            ->limit(1)
            ->execute();
             return  $q->toArray();

        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
       }


       public function getAllAddressProfile($user_id,$requestId='',$gateway_id='') {
        try{


            $q = Doctrine_Query::create()
            ->select('u.*')
            ->from('UserBillingProfile u')
             ->where('u.user_id =?',$user_id);
            if(!empty($gateway_id))
            $q->andWhere('u.gateway_id =?',$gateway_id);
            if(!empty($requestId)){
                $q->where('u.id =?',$requestId);
            }

            $result= $q->execute()->toArray();
            return $result;


        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
       }



        public function getUserBillingProfileCount($user_id,$gateway_id,$address='',$first_name='',$last_name='',$email='',$phone='') {
        try{


            $q = Doctrine_Query::create()
            ->select('u.*')
            ->from('UserBillingProfile u')
            ->where('u.user_id =?',$user_id)
            ->andWhere('u.gateway_id =?',$gateway_id);
            if(!empty($address)){
                $q->andWhere('u.address =?',$address);
            }
            if(!empty($address)){
                $q->andWhere('u.address =?',$address);
            }
            if(!empty($first_name)){
                $q->andWhere('u.first_name =?',$first_name);
            }
            if(!empty($last_name)){
                $q->andWhere('u.last_name =?',$last_name);
            }
            if(!empty($email)){
                $q->andWhere('u.email =?',$email);
            }
            if(!empty($phone)){
                $q->andWhere('u.phone =?',$phone);
            }
            $result= $q->execute();
             return  $result->count() ;


        }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
       }






}