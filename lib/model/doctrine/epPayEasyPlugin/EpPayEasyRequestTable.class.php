<?php


class EpPayEasyRequestTable extends PluginEpPayEasyRequestTable
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpPayEasyRequest');
    }

    ########### function for get card detail #################
    public function getCardDetail($cusId) {
        $orview = $this->createQuery('per')
        ->select('per.card_first AS CardFirst,per.card_last AS CardLast,per.card_len AS CardLenght, per.card_type as CardType, per.first_name as fname,
              per.last_name as lname, per.town as town, per.address1 as add1, per.state as state, per.zip as zip, per.country as country, per.phone as phone ')
        ->Where('per.cus_id= ?',$cusId)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(count($orview) > 0)
        {
            return $orview[0];
        }

        return false;
    }
   public function getRequestbetTimeDiff($ipAddress,$timeFrameMinutes) {
        $orview = $this->createQuery('gp')
        ->select('gp.id ')
        ->Where('gp.ip_address = ?',$ipAddress)
        ->andWhere('gp.tran_date < ?',date('Y-m-d H:i:s'))
        ->andWhere('gp.tran_date > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeFrameMinutes, date('s'), date('m'), date('d'), date('Y'))))
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        ;
   
        return count($orview);
    }

    public function getRequestDetails($fromDate, $toDate) {
        try {
            $inner = new Doctrine_Query();
            $inner->select('s.payeasy_req_id');
            $inner->from('EpPayEasyResponse s');
            $resArray = $inner->execute();

            foreach($resArray as $id)
            {
                $myArr[] = $id['payeasy_req_id'];
               
            }
            $comma_separated = implode(",", $myArr);
            $q = $this->createQuery('r')
                ->select('r.*')
                ->where('r.id not in ('.$comma_separated.')')
                ->andWhere('r.tran_date between ? and ?',array($fromDate,$toDate));
                
            //$res = $q->execute();
            //echo $q->getSqlQuery(); exit;
            //if($res->count() == 0) {
              //  return 0;
            //}
            return $q;
            

        }
        catch (Exception $e) {
            throw New Exception ($e->getMessage());
        }
    }
}