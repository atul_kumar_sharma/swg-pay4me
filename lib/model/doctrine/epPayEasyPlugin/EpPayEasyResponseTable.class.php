<?php


class EpPayEasyResponseTable extends PluginEpPayEasyResponseTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpPayEasyResponse');
    }
}