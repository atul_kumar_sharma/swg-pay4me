<?php


class MoneyorderTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('Moneyorder');
  }

  public function getAllTrackingNumber($moneyOrder)
  {
    $sql = "Select ctn.tracking_number as tracking_number,
                ctn.order_number as order_number,
                mo.amount as amount,
                mo.convert_amount as convert_amount,
                mo.currency as currency,
                ctn.associated_date as associated_date,
                mo.id as id,
                ctn.order_request_detail_id as order_request_detail_id,
                ctn.user_id as user_id,
                mo.moneyorder_number as moneyorder_number,
                mo.phone as phone,
                mo.address as address,
                mo.courier_service as courier_service,
                mo.courier_flag as courier_flag,
                mo.waybill_trackingno as waybill_trackingno
                from money_order mo
                left join tracking_money_order tmo on (mo.id = tmo.moneyorder_id)
                left join cart_tracking_number ctn on (tmo.cart_track_id = ctn.id)
                where (ctn.status = 'Associated' AND mo.deleted = 0 AND ctn.deleted = 0)";
      if(trim($moneyOrder)!=''){
          $sql .= " and mo.moneyorder_number = '".addslashes($moneyOrder)."'";
      }
      $sql .= " group by mo.moneyorder_number order by mo.updated_at desc ";

      if(trim($moneyOrder)!=''){
          $sql .= " limit 1";
      }

    return $sql;
  }

  public function getAllMODetails($moneyOrder){
    $query = Doctrine_Query::create()
    ->select("mo.*,tmo.*,ctn.*")
    ->from('Moneyorder mo')
    ->leftJoin('mo.TrackingMoneyorder tmo')
    ->leftJoin('tmo.CartTrackingNumber ctn')

    ->where("mo.id =?",$moneyOrder)
    ->andWhere("ctn.status =?",'Associated');

    $res = $query->execute()->toArray();
    return $res;
    //echo '<pre>';print_r($res);die;
  }

     /**
 *
 * @param <type> $orderNumber
 * @return <type>
 * To get retry id for the order number passsed.
 */

  public function updateMODetail($MOId,$comments){
    try{
      $q = Doctrine_Query::create()
      ->update('Moneyorder')
      ->set('paid_date',"'".date('Y-m-d H:i:s')."'")
      ->set('status','?','Paid')
      ->set('comments ','?',$comments)
      ->where('id =?',$MOId);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }


  public function getTrackingNumberByStatus($moneyOrderStatus='',$moneyOrder='',$trackingNumber='',$userId='',$order_number='',$currency = '', $fdate='',$tdate=''){
    $query = Doctrine_Query::create()
    ->select("mo.*,tmo.*,ctn.*")
    ->from('CartTrackingNumber ctn')
    ->leftJoin('ctn.TrackingMoneyorder tmo')
    ->leftJoin('tmo.Moneyorder mo');
    if(!empty($moneyOrderStatus)){
      $query->where("ctn.status =?",$moneyOrderStatus);
    }

    if(!empty ($moneyOrder)){
      $query->andWhere("mo.moneyorder_number =?",$moneyOrder);
    }
    if(!empty ($trackingNumber)){
      $query->andWhere("ctn.tracking_number =?",$trackingNumber);
    }
    if(!empty ($userId)){
      $query->andWhere("ctn.user_id =?",$userId);
    }
    if(!empty ($order_number)){
      $query->andWhere("ctn.order_number =?",$order_number);
    }
    if(!empty ($currency)){
      $query->andWhere("ctn.currency =?",$currency);
    }
    
    if(!empty ($fdate) && !empty ($tdate)){
      $query->andWhere("date(ctn.updated_at) >= ?",$fdate);
      $query->andWhere("date(ctn.updated_at) <= ?",$tdate);
    }

    $query->orderBy('ctn.id DESC');

    return $query;
  }
  public function getMoneyOrderDetailByTrackingNumber($trackingNumberId){
    $query = Doctrine_Query::create()
    ->select("mo.*,tmo.*,ctn.*")
    ->from('Moneyorder mo')
    ->leftJoin('mo.TrackingMoneyorder tmo')
    ->leftJoin('tmo.CartTrackingNumber ctn')
    ->where("ctn.id =?",$trackingNumberId);


    return $query->execute()->toArray();
  }

  public function getMoneyOrderDetailByMoneyOrderNumber($moneyOrderNumber){
    $query = Doctrine_Query::create()
    ->select("mo.*")
    ->from('Moneyorder mo')
    ->where("mo.moneyorder_number =?",$moneyOrderNumber);
    return $query->execute()->toArray();
  }

  public function deleteDetailById($moneyorderId){
    if(isset ($moneyorderId) && $moneyorderId!=''){
      $query = Doctrine_Query::create()
      ->delete('mo.*')
      ->from('MoneyOrder mo')
      ->where('mo.id =?',$moneyorderId);
      $query->execute();
    }
  }

  public function setMoneyOrderDetailByMoneyOrderId($moneyOrderDetailId,$moneyOrderAmount,$moneyOrderNumber){
    try{
      if(!empty($moneyOrderDetailId)&&!empty($moneyOrderAmount)&&!empty($moneyOrderNumber))
      {
        $q = Doctrine_Query::create()
        ->update('Moneyorder')
        ->set('moneyorder_number','?',$moneyOrderNumber)
        //->set('amount','?',$moneyOrderAmount) // commented by ashwani kumar...
        ->where('id =?',$moneyOrderDetailId);
        $res = $q->execute();
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }


  public function getMoneyOrderNumberId($moneyOrderNumber,$moneyOrderDetailId){
    try{
      $q = Doctrine_Query::create()
      ->select('mo.*')
      ->from('Moneyorder mo')
      ->where('mo.moneyorder_number =?',$moneyOrderNumber)
      ->andWhere('mo.id !=?',$moneyOrderDetailId)
      ->count();
      return $q;

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }


  public function setMoneyOrderDeleteStatus($moneyOrderId, $status=0){
    try{
      if($moneyOrderId !='' && $status != '')
      {
        $q = Doctrine_Query::create()
        ->update('Moneyorder')
        ->set('deleted','?',$status)
        ->where('id = ?',$moneyOrderId);
        //die("Query: ".$q->getSqlQuery()."<br />CartId: ".$cartId."<br />Status: ".$status);
        $res = $q->execute();
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when updating Money Order";
    }
  }

}