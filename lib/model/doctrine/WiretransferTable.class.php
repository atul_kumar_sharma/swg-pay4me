<?php


class WiretransferTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Wiretransfer');
    }
    public function getAllTrackingNumber($wireTransfer)
  {
    $sql = "SELECT wtn.tracking_number as tracking_number,
                wtn.order_number as order_number,
                wt.amount as amount,
                wtn.associated_date as associated_date,
                wt.id as id,
                wt.wire_transfer_proof as wire_transfer_proof,
                wtn.order_request_detail_id as order_request_detail_id,
                wtn.user_id as user_id,
                wt.wire_transfer_number as wire_transfer_number
            FROM wire_transfer wt
              left join wire_tracking_number wtn on (wt.id = wtn.wire_tracking_id )
            WHERE (wtn.status = 'Associated')

              ";
    if($wireTransfer != '')
    {
      $sql .= " and wt.wire_transfer_number = '".addslashes($wireTransfer)."'";
    }
    $sql .= " group by wt.wire_transfer_number ORDER BY wt.updated_at desc";
    return $sql;
  }

  public function getAllWTDetails($wireTransfer){
    $query = Doctrine_Query::create()
    ->select("wt.*,wtn.*")
    ->from('Wiretransfer wt')
    ->leftJoin('wt.WireTrackingNumber wtn')

    ->where("wt.id =?",$wireTransfer)
    ->andWhere("wtn.status =?",'Associated');

    $res = $query->execute()->toArray();
    return $res;
    //echo '<pre>';print_r($res);die;
  }

     /**
 *
 * @param <type> $orderNumber
 * @return <type>
 * To get retry id for the order number passsed.
 */

  public function updateWTDetail($WTId,$comments){
    try{
      $q = Doctrine_Query::create()
      ->update('Wiretransfer')
      ->set('paid_date',"'".date('Y-m-d H:i:s')."'")
      ->set('status','?','Paid')
      ->set('comments ','?',$comments)
      ->where('id =?',$WTId);
      $res = $q->execute();
      if($res){
        return true;
      }else{
        return false;
      }
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }


  public function getTrackingNumberByStatus($wireTransferStatus='',$wireTransfer='',$trackingNumber='',$userId='',$order_number=''){
    $query = Doctrine_Query::create()
    ->select("wt.*,wtn.*")
    ->from('WireTrackingNumber wtn')
    ->leftJoin('wtn.Wiretransfer wt');
    
    if(!empty($wireTransferStatus)){
      $query->where("wtn.status =?",$wireTransferStatus);
    }

    if(!empty ($wireTransfer)){
      $query->andWhere("wt.wire_transfer_number =?",$wireTransfer);
    }
    if(!empty ($trackingNumber)){
      $query->andWhere("wtn.tracking_number =?",$trackingNumber);
    }
    if(!empty ($userId)){
      $query->andWhere("wtn.user_id =?",$userId);
    }
    if(!empty ($order_number)){
      $query->andWhere("wtn.order_number =?",$order_number);
    }

    $query->orderBy('wtn.id DESC');

    return $query;
  }
  
  public function getWireTransferDetailByTrackingNumber($trackingNumberId){
    $query = Doctrine_Query::create()
    ->select("wt.*, wtn.*")
    ->from('Wiretransfer wt')
    ->leftJoin('wt.WireTrackingNumber wtn')
    ->where("wtn.id =?",$trackingNumberId);

    return $query->execute()->toArray();
  }

  public function getWireTransferDetailByWireTransferNumber($wireTransferNumber){
    $query = Doctrine_Query::create()
    ->select("wt.*")
    ->from('Wiretransfer wt')
    ->where("wt.wire_transfer_number =?",$wireTransferNumber);
    return $query->execute()->toArray();
  }

  public function deleteDetailById($wireTransferId){
    if(isset ($wireTransferId) && $wireTransferId!=''){
      $query = Doctrine_Query::create()
      ->delete('wt.*')
      ->from('Wiretransfer wt')
      ->where('wt.id =?',$wireTransferId);
      $query->execute();
    }
  }

  public function setWireTransferDetailByWireTransferId($wireTransferDetailId,$wireTransferAmount,$wireTransferNumber){
    try{
      if(!empty($wireTransferDetailId)&&!empty($wireTransferAmount)&&!empty($wireTransferNumber))
      {
        $q = Doctrine_Query::create()
        ->update('Wiretransfer')
        ->set('wire_transfer_number','?',$wireTransferNumber)
        ->set('amount','?',$wireTransferAmount)
        ->where('id =?',$wireTransferDetailId);
        $res = $q->execute();
        if($res){
          return true;
        }else{
          return false;
        }
      }
      return false;
    }catch(Exception $e){
      return "Exception when retriving NIS Appliations";
    }
  }


  public function getWireTransferNumberId($wireTransferNumber,$wireTransferDetailId){
    try{
      $q = Doctrine_Query::create()
      ->select('wt.*')
      ->from('Wiretransfer wt')
      ->where('wt.wire_transfer_number =?',$wireTransferNumber)
      ->andWhere('wt.id !=?',$wireTransferDetailId)
      ->count();
      return $q;

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }
}