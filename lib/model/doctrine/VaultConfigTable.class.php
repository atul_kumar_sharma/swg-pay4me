<?php


class VaultConfigTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('VaultConfig');
    }
    public function getResultByCard($cardPattern,$cstDate = '')
    {
        
         try{
                $q = Doctrine_Query::create()
                ->select('VC.id, VC.is_vault_active, VC.trans_date , VC.amount, VC.card_type ')
                ->from('VaultConfig VC')
                ->where('VC.card_type LIKE "'.$cardPattern.'"');
                if( '' != $cstDate)
                    $q->andWhere(" date(VC.trans_date) = ?",$cstDate );
                    $q->orderBy('VC.trans_date DESC') ;
                   
                $result = $q->execute(array(),Doctrine::HYDRATE_ARRAY);
                if(count($result) > 0)
                    return $result[0];
                else
                    return false;
            }catch(Exception $e) {
            echo("Problem found". $e->getMessage());die;
        }
    }
}