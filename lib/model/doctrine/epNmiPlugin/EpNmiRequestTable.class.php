<?php


class EpNmiRequestTable extends PluginEpNmiRequestTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpNmiRequest');
    }

    public function setRequest()
    {
        $obj = new EpNmiRequest();
        $obj->setOrderId($order_id);
        $obj->setTransactionId($tarnsaNo);
        $obj->setAmount($type);
        $obj->save();
        return $obj;
    }
    public function getNmiRequestId($transactionId){
        $q = $this->createQuery('r')
        ->select('r.*')
        ->where('r.transaction_id=?',$transactionId)
        ->execute()->toArray();
        return $q;


    }

    public function getOrderAmount($order_number){
        $orview = $this->createQuery('gp')
        ->select('gp.amount')
        ->Where('gp.order_id = ?',$order_number)
        ->execute(array(), Doctrine::HYDRATE_ARRAY);
        if(count($orview) > 0)
        {
            return $orview[0]['amount'];
        }
        return false;
     }
     
}