<?php


class FpsDetailTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('FpsDetail');
  }
  public function  getFpsDeatil($from_date='',$to_date=''){
    $phis = $this->createQuery('p')
    ->select('count(*),  fps_rule_id')
    ->groupBy('p.fps_rule_id');

    if(''!=$from_date){
      $phis->andWhere('p.trans_date  >= ?',$from_date);
    }
    if(''!=$to_date){
      $phis->andWhere('p.trans_date <= ?',$to_date);

    }

    return $phis->execute(array(), DOCTRINE::HYDRATE_ARRAY);
  }

  public function getRequestbetTimeDiff($ipAddress,$timeFrameMinutes) {
    $orview = $this->createQuery('gp')
    ->select('gp.id ')
    ->Where('gp.ip_address = ?',$ipAddress)
    ->andWhere('gp.trans_date < ?',date('Y-m-d H:i:s'))
    ->andWhere('gp.trans_date > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeFrameMinutes, date('s'), date('m'), date('d'), date('Y'))))
    ->andWhere('gp.fps_rule_id = ?', 0);        
    $orview = $orview->execute(array(), Doctrine::HYDRATE_ARRAY);
    return count($orview);
  }

  public function getNoOfCardUsed($card_Num, $timeInMinutes){
    try{

      $query= $this->createQuery('ip')
      ->select('ip.id')
      ->where('ip.card_num = ?',$card_Num)
      ->andWhere('ip.trans_date < ?',date('Y-m-d H:i:s'))
      ->andWhere('ip.trans_date > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeInMinutes, date('s'), date('m'), date('d'), date('Y'))))
      ->andWhere('ip.fps_rule_id = ?', 0)
      ->andWhere('ip.payment_status = ?', 0)
      ->execute(array(), Doctrine::HYDRATE_ARRAY);

      return count($query);
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
  public function getNoEmailUsed($card_Num, $timeInMinutes){
    try{

      $query= $this->createQuery('ip')
      ->select('ip.email')
      ->where('ip.card_num = ?',$card_Num)
      ->andWhere('ip.trans_date < ?',date('Y-m-d H:i:s'))
      ->andWhere('ip.trans_date > ?',date('Y-m-d H:i:s',mktime(date('H'),date('i') - $timeInMinutes, date('s'), date('m'), date('d'), date('Y'))))
      ->andWhere('ip.fps_rule_id = ?', 0)
      //                    ->andWhere('ip.payment_status = ?', 1);;
      ->execute(array(), Doctrine::HYDRATE_ARRAY);

      return $query;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getBlackListCard($card_Num, $ruleId){
    try{

      $query= $this->createQuery('ip')
      ->select('ip.id')
      ->where('ip.card_num = ?',$card_Num)
      ->andWhere('ip.fps_rule_id = ?',$ruleId)
      ->execute(array(), Doctrine::HYDRATE_ARRAY);

      return count($query);
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

   public function getBlackListCardDetails($email = '',$cardhash = ''){
    try{
      $userId = Doctrine::getTable('UserDetail')->getPaymentUserFieldsByEmail($email,'user_id'); 
      if(count($userId) > 0){
          $userId = $userId[0]['user_id'];
      }  else {
          $userId = '';
      }  
      $sql = "SELECT fps.id as fps_id, fps.user_id as user_id, fps.email as email,ord.id as order_id, ord.card_first as card_first, ord.card_last as card_last,
              ord.card_len as card_len, usr.first_name as fname, usr.last_name as lname, fps.order_number as fpsorder, fps.fps_rule_id as rule_id,fps.updated_at as blocked_date,ord.order_number,ord.payor_name
              FROM `fps_detail` fps
              INNER JOIN user_detail usr on (usr.user_id=fps.user_id)
              LEFT JOIN ipay4me_order ord on (ord.order_number=fps.order_number)
              WHERE (fps.fps_rule_id = 4 or fps.fps_rule_id = 5 )
              ";
      if($email != '')
      {
        $sql .= " AND ( fps.email LIKE '$email%'";
      }
      if($userId != ''){
          $sql .= " OR fps.user_id = ".$userId.")";
      }else if($cardhash == ''){
          $sql .= ")";
      }
      if($cardhash != ''){
          $sql .= "  AND fps.card_num= '".$cardhash."'";
      }
      
      $sql .= " ORDER BY usr.user_id";
//            echo $sql;die;
      return $sql;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }
   public function getBlackListUserDetails($email = '',$fdate = '',$tdate = '',$cardFirst = '',$cardLast = ''){
    try{



//      $sql = "Select fps.id as fps_id, fps.user_id as user_id, fps.email as email,ord.id as order_id, ord.card_first as card_first, ord.card_last as card_last,
//              ord.card_len as card_len, usr.first_name as fname, usr.last_name as lname, fps.order_number as fpsorder, fps.fps_rule_id as rule_id,fps.updated_at as blocked_date,ord.order_number,ord.payor_name,ord.card_holder
//              from `fps_detail` fps
//              inner join user_detail usr on (usr.user_id=fps.user_id)
//              left join ipay4me_order ord on (ord.order_number=fps.order_number)
//              where (fps.fps_rule_id = 5)";

      $sql = "SELECT *, ipo.updated_at as blocked_date FROM ipay4me_order ipo WHERE payment_status = '3'";

//      if($email != '')
//      {
//        $sql .= " and fps.email LIKE '$email%'";
//      }
      if($fdate != '' && $tdate != ''){

         $sql .= " and date(ipo.created_at) >= '".$fdate."' and date(ipo.created_at) <= '".$tdate."'";
      }
      if($cardFirst != '' and $cardLast != ''){
          $sql .= "and ipo.card_first LIKE '".$cardFirst."%' and ipo.card_last = '".$cardLast."'";
      }

      $sql .= " order by ipo.id";
            //echo $sql;die;
      return $sql;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function UnblockCards($orderNumber)
  {
    $cardNum = $this->getBlackListCardNumber($orderNumber);
    try {
      $q = Doctrine_Query::create()
      ->update('FpsDetail')
      ->set('fps_rule_id', '?', 0)
      ->where("card_num =?", $cardNum)
      ->execute();
      return true;
    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function UnblockCardsByCardNum($cardNum){
      if($cardNum != ''){
          try {
              $q = Doctrine_Query::create()
              ->update('FpsDetail')
              ->set('fps_rule_id', '?', 0)
              ->where("card_num =?", $cardNum)
              ->execute();
              return true;
            }catch(Exception $e){
              throw New Exception ($e->getMessage());
            }
      }
  }

  public function getBlackListCardNumber($orderNumber){
    try{
      $query= $this->createQuery('ip')
      ->select('ip.id,ip.card_num')
      ->where('ip.order_number = ?',$orderNumber)
      ->execute()->toArray();
      if(count($query) > 0)
      return $query[0]['card_num'];
      else
      return 0;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function setBlockUserCard($orderNumber){
    try {
      $q = Doctrine_Query::create()
      ->update('FpsDetail')
      ->set('fps_rule_id', '?', 5)
      ->where("order_number=?", $orderNumber);
      $res = $q->execute();

      if($res){
        return true;
      }else{
        return false;
      }

    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function getAllOrderNumber($cardNumber,$transactionDate){

    try{
      $query= $this->createQuery('ip')
      ->select('*')
      ->where('ip.card_num = ?',"$cardNumber")
      ->andWhere('ip.trans_date > ?',"$transactionDate")
      ->execute()->toArray();
      if(count($query) > 0)
      return $query;
      else
      return 0;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function findByOrderNumber($orderNumber)
  {
    try{
      $query= $this->createQuery('ip')
      ->select('*')
      ->where('ip.order_number = ?',$orderNumber)
      ->execute()
      ->toArray();
      if(count($query)>0)
      {
        return $query;
      }
      else
      {
        return 0;
      }
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function updateFpsDetailForNmi($orderNumber, $status, $verified,$arrValidationRules)
  {
    try {
      if($status == 'success')
      {
        $ruleId = 0;
        $pstatus = 0;
      }else{
          if(is_array($arrValidationRules) && (in_array('BLC',$arrValidationRules) || in_array('FPS',$arrValidationRules))) {
                  $ruleId = 4;
                  $pstatus = 1;

                  $allowedCardFailureTries =  sfConfig::get('app_maxsame_card_failure_tries');
                  $getLastTwoPaymentStatus = $this->getLastTwoPaymentStatus($orderNumber);
                  if(count($getLastTwoPaymentStatus) > 0 )
                  {
                      $count = $getLastTwoPaymentStatus['count'] + 1;
                      $ruleId = ($allowedCardFailureTries == $count) ? 4 : 0;
                  }

              }else{
                  $ruleId = 0;
                  $pstatus = 1;
              }
          }

      $q = Doctrine_Query::create()
      ->update('FpsDetail')
      ->set('fps_rule_id', '?', $ruleId)
      ->set('payment_status', '?', $pstatus)
      ->set('isverified', '?', $verified)
      ->where("order_number=?", $orderNumber);
      $res = $q->execute();

      if($res){
        return true;
      }else{
        return false;
      }

    }catch(Exception $e){
      echo("Problem found". $e->getMessage());die;
    }
  }

  public function is3dVerified($cardNumber)
  {
    try{
      $query= $this->createQuery('ip')
      ->select('count(*)')
      ->where('ip.isverified = ?', 'yes')
      ->andWhere('ip.card_num = ?', md5($cardNumber))
      ->count();
      return $query;
    }catch(Exception $e) {
      echo("Problem found". $e->getMessage());die;
    }
  }

  private function getLastTwoPaymentStatus($orderNumber)
  {
    $returnArr = array();
    $q = $this->createQuery('ip')
    ->select('id, card_num')
    ->where('ip.order_number = ?', $orderNumber)
    ->fetchArray();

    if(count($q) > 0)
    {
      $card_num = $q[0]['card_num'];
      $returnArr['card_num'] = $card_num;
      $count = 0;
      $query = $this->createQuery('ip')
      ->select('id, card_num, payment_status')
      ->where('ip.card_num = ?', $card_num)
      ->orderBy('ip.id desc')
      ->offset(1)
      ->limit(2)
      ->fetchArray();

      foreach ($query as $record)
      {
        if($record['payment_status'] == 1)
        $count ++;
      }
      $returnArr['count'] = $count;
    }
    return $returnArr;
  }

  public function updateFpsDetailForGraypay($fpsId,$status,$arrValidationRules)
  {
    $FpsDetailObj = Doctrine::getTable('FpsDetail')->find($fpsId);
    if(!empty($status['validation']))
    $FpsDetailObj->setOrderNumber($status['validation']);

    if($status['status'] == 'success')
    {
      $FpsDetailObj->setPaymentStatus('0');
    }
    else{
        if(is_array($arrValidationRules) && (in_array('BLC',$arrValidationRules) || in_array('FPS',$arrValidationRules))) {
          
              $orderNumber = $status['validation'];
              $allowedCardFailureTries =  sfConfig::get('app_maxsame_card_failure_tries');
              $getLastTwoPaymentStatus = $this->getLastTwoPaymentStatusForGraypay($FpsDetailObj->getCardNum());
              if(count($getLastTwoPaymentStatus) > 0 )
              {
                   $count = $getLastTwoPaymentStatus['count'] + 1;
                  ($count >=$allowedCardFailureTries ) ? $FpsDetailObj->setFpsRuleId('4') : $FpsDetailObj->setFpsRuleId('0');
              }
          }else{
              $FpsDetailObj->setFpsRuleId('0');
          }
    }
    $FpsDetailObj->save();
  }

  private function getLastTwoPaymentStatusForGraypay($card_num)
  {
    $returnArr = array();
    $count = 0;
    $query = $this->createQuery('ip')
    ->select('id, card_num, payment_status')
    ->where('ip.card_num = ?', $card_num)
    ->orderBy('ip.id desc')
    ->offset(1)
    ->limit(2)
    ->fetchArray();

    foreach ($query as $record)
    {
      if($record['payment_status'] == 1)
      $count ++;
    }
    $returnArr['count'] = $count;

    return $returnArr;
  }
  /**
   *
   * @param <type> $orderRequestDetailId
   * @return <type>
   * Added for Bug Id: 28992
   */
  public function getFpsDetailbyOrderRequestDetailId($orderRequestDetailId) {

    if(isset($orderRequestDetailId) && $orderRequestDetailId!='')
    {
      $query = Doctrine_Query::create()
      ->select("t.*")
      ->from("FpsDetail t")
      ->where("t.order_request_detail_id =?",$orderRequestDetailId);
      return $query;
    }
  }

  public function findByCardNum($cardHash)
  {
     if(isset($cardHash) && $cardHash!='')
    {
      $query = Doctrine_Query::create()
      ->select('t.*')
      ->from('FpsDetail t')
      ->where('t.card_num =?',$cardHash)
      ->orderBy('t.id desc')
      ->execute()->toArray();
      if(!empty($query))
      {
      return $query;
      }
      else
      {
          return false;
      }
    }
  }
    public function findLastFailureDate($cardHash)
  {
     if(isset($cardHash) && $cardHash!='')
    {
      $var = '1';
      $query = Doctrine_Query::create()
      ->select('t.*')
      ->from('FpsDetail t')
      ->where('t.card_num =?',$cardHash)
      ->andWhere('t.payment_status =?',$var)
      ->orderBy('t.id desc');
      return $query;
    }
  }

  public function getCardhash($timePeriod,$currentDate,$userId)
  {      
   if(isset($timePeriod) && $timePeriod!='' && isset($currentDate) && $currentDate!='')
    {
      $currentDate=$currentDate." 23:59:59";
      $timePeriod=$timePeriod." 00:00:00";
      $query= $this->createQuery('t');
      $query->select('DISTINCT(t.card_num)');
      $query->where('t.created_at >=?', $timePeriod);
      $query->andWhere('t.created_at <=?', $currentDate);
      $query->andWhere('t.user_id =?',$userId);
      return $query;
    }
  }

  public function getDistinctCardSuccessTrans($cardHash,$timePeriod,$currentDate)
  {
      $currentDate=$currentDate." 23:59:59";
      $timePeriod=$timePeriod." 00:00:00";
      try{
      $q = Doctrine_Query::create()
      ->select('t.*')
      ->from('FpsDetail t')
      ->where('t.created_at >= ?', $timePeriod)
      ->andWhere('t.created_at <= ?', $currentDate)
      ->andWhere('t.card_num =?',$cardHash)
      ->andWhereIn('t.payment_status',array('0','2','3'))
      ->orderBy('t.id DESC');
      return $q;

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }
  public function getDistinctCardFailTrans($cardHash,$timePeriod,$currentDate)
  {
      $currentDate=$currentDate." 23:59:59";
      $timePeriod=$timePeriod." 00:00:00";
      try{
      $q = Doctrine_Query::create()
      ->select('t.*')
      ->from('FpsDetail t')
      ->where('t.created_at >= ?', $timePeriod)
      ->andWhere('t.created_at <= ?', $currentDate)
      ->andWhere('t.card_num =?',$cardHash)
      ->andWhere('t.payment_status =?','1')
      ->count();
      return $q;

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }

  public function getDistinctCardBlock($cardHash,$timePeriod,$currentDate)
  {
      $currentDate=$currentDate." 23:59:59";
      $timePeriod=$timePeriod." 00:00:00";
      try{
      $q = Doctrine_Query::create()
      ->select('t.*')
      ->from('FpsDetail t')
      ->where('t.created_at >= ?', $timePeriod)
      ->andWhere('t.created_at <= ?', $currentDate)
      ->andWhere('t.card_num =?',$cardHash)
      ->andWhereIn('t.fps_rule_id',array('4','5'))
      ->count();
      return $q;;

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }
  public function getUnpaidRecordByRequestId($orderRequestDetailId){
    if(isset($orderRequestDetailId) && $orderRequestDetailId !='')
    {
      $query = Doctrine_Query::create()
      ->select("t.*")
      ->from("FpsDetail t")
      ->where("t.order_request_detail_id =?",$orderRequestDetailId)
      ->andWhere("t.payment_status = ?",1)
      ->execute()->toArray();
      if($query){
          return $query;
      }else{
          return false;
      }
    }

  }

  /**
   * [WP: 110] => CR: 156
   * @param <type> $userId
   * @param <type> $timePeriod
   * @param <type> $currentDate
   * @return <type> 
   */
  public function getSuccessTransactions($userId, $timePeriod, $currentDate, $cardNum = '')
  {
      $currentDate = $currentDate." 23:59:59";
      $timePeriod = $timePeriod." 00:00:00";
      try{
          $q = Doctrine_Query::create()
          ->select('t.*')
          ->from('FpsDetail t')
          ->where('t.created_at >= ?', $timePeriod)
          ->andWhere('t.created_at <= ?', $currentDate)
          ->andWhere('t.user_id =?',$userId)
          ->andWhere('t.payment_status=?','0');
          if($cardNum != ''){
            $q->andWhere('t.card_num = ?', $cardNum);
          }
          $q->orderBy('t.id DESC')->execute();
          return $q;
          //echo $q->getSqlQuery();

    }catch(Exception $e){
      return "Exception when retriving Data";
    }
  }//End of public function getSuccessTransactions($userId, $timePeriod, $currentDate)...


    /**
     * [WP: 110] => CR: 156
     * @param <type> $userId
     * @param <type> $timePeriod
     * @param <type> $currentDate
     * @param <type> $txtValue
     * @return <type> 
     */
    public function resetUserTransactions($userId, $timePeriod, $currentDate, $txtValue, $cardNum=''){

      $currentDate = $currentDate." 23:59:59";
      $timePeriod = $timePeriod." 00:00:00";
      try{
            if($userId != '' && $timePeriod != '' && $currentDate != ''){
              $q = Doctrine_Query::create()
                ->update('FpsDetail t')
                ->set('t.deleted', '?','1')
                ->where('t.user_id =?',$userId)
                ->where('t.deleted =?',0)
                ->andWhere('t.created_at >= ?', $timePeriod)
                ->andWhere('t.created_at <= ?', $currentDate);
                if($cardNum != ''){
                    $q->andWhere('t.card_num = ?', $cardNum);
                }
                $q->andWhere('t.payment_status = ?',0)
                ->orderBy('t.id DESC')
                ->limit($txtValue);
              $res = $q->execute();
              return true;
          }else{
              return false;
          }
      }catch(Exception $e){          
           return false;
      }       

    }//End of public function resetUserTransactions($userId, $timePeriod, $currentDate, $txtValue){...
  
}