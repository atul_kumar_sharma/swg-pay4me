<?php


class BillTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Bill');
    }


      public function chkBillNumberDuplicacy($bill_number)
      {
         $q = $this->createQuery('b')
                ->select('count(*)')
                ->addWhere("bill_number = '".$bill_number."'");

            return  $q->count();

      }


      public function chkBillDuplicacy($order_request_id )
      {
         $q = $this->createQuery('b')
                ->select('count(*)')
                ->addWhere("order_request_id  = '".$order_request_id."'");

            return  $q->count();

      }

      public function getUnpaidBillRecords($user_id){

          $q = $this->createQuery('b')
               ->select('b.*, or.amount')
               ->leftJoin('b.OrderRequest or')
               ->where('b.status =?','unpaid')
               ->addWhere('b.user_id =?',$user_id);
               //->addWhere('b.order_request_id != (select b.order_request_id from bill b where b.status =?)','paid');

           return $q;

      }


}