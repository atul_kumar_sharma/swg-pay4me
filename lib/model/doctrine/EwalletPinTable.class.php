<?php


class EwalletPinTable extends Doctrine_Table
{

    public static function getInstance()
    {
        return Doctrine_Core::getTable('EwalletPin');
    }
    function setPin($pin,$userId)
    {
        if(isset($pin) && isset($userId) && !empty($pin) && !empty($userId) && is_numeric($userId) && $userId>0)
        {
            $pinObj=new pin();
            $activatedTill=$pinObj->getActivatedTill();
            $ob = new EwalletPin();
            $ob->set('user_id', $userId);
            $ob->set('pin_number', $pin);
            $ob->set('activated_till', $activatedTill);
            $ob->set('no_of_retries', 1);
            $ob->save();
        }else
        {
            return false;
        }

    }
}