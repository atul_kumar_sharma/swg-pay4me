<?php


class CountryBasedRegisterCardTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CountryBasedRegisterCard');
    }
    public function getTotalRecord(){
        $sql = $this->createQuery()
        ->select('*');

        return $sql;
    }

    public function removeRowById($id){ 
       $query = Doctrine_Query::create()
               ->delete('cbr.*')
               ->from('CountryBasedRegisterCard cbr')
               ->where('cbr.id =?',$id)
               ->andWhere('cbr.country_code !=?','ALL');
       $res =   $query->execute();
        if($res){
            return true;
        }else{
            return false;
        }

    }

    public function reomveMultipleRow($idArray){
       if(count($idArray) > 0){
            $query = Doctrine_Query::create()
                   ->delete('cbr.*')
                   ->from('CountryBasedRegisterCard cbr')
                   ->whereIN('cbr.id',$idArray)
                   ->andWhere('cbr.country_code !=?','ALL');;
           $res =   $query->execute();
            if($res){
                return true;
            }else{
                return false;
            }
       }else{
          return false;
       }
       
    }
    public function changeAllStatus($id,$isRegister){
        try {
        $query = Doctrine_Query::create()
               ->update('countryBasedRegisterCard')
               ->set('is_registered','?',$isRegister)
               ->where('id =?',$id);
       $res =   $query->execute();
        if($res){
            return true;
        }else{
            return false;
        }
    }catch(Exception $e){
        return "Error ";
    }
   }

}