<?php


class CountryBasedZipTable extends Doctrine_Table
{

  public static function getInstance()
  {
    return Doctrine_Core::getTable('CountryBasedZip');
  }

  public function isCountryHasZipCheck($country_code)
  {
    $q = Doctrine_Query::create()
    ->select('count(*)')
    ->from('CountryBasedZip')
    ->where('zip =?', 'yes')
    ->andWhere('country_code =?', $country_code)
    ->count();
    return $q;
  }

//start - added by vineet for fetching all country code for ZIP
   public function getAllCountryCode()
  {
    $q = Doctrine_Query::create()
    ->select('*')
    ->from('CountryBasedZip')
    ->where('zip =?', 'yes')
    ->execute()->toArray();
    return $q;
  }
  //end - added by vineet for fetching all country code for ZIP
}