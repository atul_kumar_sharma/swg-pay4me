<?php
/**
 * Class that adds
 */
class sfTrackable extends Doctrine_Template {
    protected $_options = array('created' =>  array('disabled'      =>  false),
                                'updated' =>  array('disabled'      =>  false));

    /**
     * __construct
     *
     * @param string $array
     * @return void
     */
    public function __construct(array $options = array())
    {
        $this->_options = Doctrine_Lib::arrayDeepMerge($this->_options, $options);
    }

    public function setTableDefinition()
    {
      if( ! $this->_options['created']['disabled']) {
        $this->hasColumn('created_by','integer',4);
      }
      if( ! $this->_options['updated']['disabled']) {
        $this->hasColumn('updated_by','integer',4);
      }
      $this->addListener(new sfTrackableListener($this->_options));
    }
}