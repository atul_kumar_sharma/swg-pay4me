<?php
// auto-generated by sfSecurityConfigHandler
// date: 2013/09/25 11:49:25
$this->security = array (
  'searchappformo' => 
  array (
    'is_secure' => 'on',
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'admin_support_user',
        1 => 'admin',
        2 => 'portal_admin',
        3 => 'support_user',
        4 => 'refund_support_user',
        5 => 'money_order_menu',
        6 => 'support_only',
      ),
    ),
  ),
  'unbindmoneyorder' => 
  array (
    'is_secure' => 'on',
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'admin_support_user',
        1 => 'admin',
        2 => 'portal_admin',
        3 => 'support_user',
        4 => 'refund_support_user',
        5 => 'money_order_menu',
        6 => 'support_only',
        7 => 'money_order_menu_report',
      ),
    ),
  ),
  'all' => 
  array (
    'is_secure' => 'on',
  ),
);
